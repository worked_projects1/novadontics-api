
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v2.routes
// @DATE:Fri Jun 19 20:14:07 IST 2020

package v2

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HealthCheckController_20: javax.inject.Provider[controllers.v1.HealthCheckController],
  // @LINE:9
  SessionController_9: javax.inject.Provider[controllers.v1.SessionController],
  // @LINE:13
  PasswordController_19: javax.inject.Provider[controllers.v1.PasswordController],
  // @LINE:19
  CategoriesController_5: javax.inject.Provider[controllers.v2.CategoriesController],
  // @LINE:25
  FavouritesController_10: javax.inject.Provider[controllers.v2.FavouritesController],
  // @LINE:28
  VendorController_7: javax.inject.Provider[controllers.v2.VendorController],
  // @LINE:35
  OrdersController_6: javax.inject.Provider[controllers.v1.OrdersController],
  // @LINE:41
  TreatmentsController_4: javax.inject.Provider[controllers.v1.TreatmentsController],
  // @LINE:48
  PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
  // @LINE:51
  ConsultationsController_11: javax.inject.Provider[controllers.v1.ConsultationsController],
  // @LINE:57
  UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
  // @LINE:63
  StaffController_18: javax.inject.Provider[controllers.v1.StaffController],
  // @LINE:70
  ProfileController_15: javax.inject.Provider[controllers.v2.ProfileController],
  // @LINE:73
  PracticeController_21: javax.inject.Provider[controllers.v1.PracticeController],
  // @LINE:79
  CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
  // @LINE:85
  CoursesController_8: javax.inject.Provider[controllers.v1.CoursesController],
  // @LINE:91
  UploadsController_12: javax.inject.Provider[controllers.v1.UploadsController],
  // @LINE:93
  EmailMappingController_22: javax.inject.Provider[controllers.v2.EmailMappingController],
  // @LINE:101
  ShippingController_13: javax.inject.Provider[controllers.v2.ShippingController],
  // @LINE:107
  FormsController_3: javax.inject.Provider[controllers.v2.FormsController],
  // @LINE:112
  PrescriptionController_16: javax.inject.Provider[controllers.v2.PrescriptionController],
  // @LINE:117
  ResourceCategoriesController_17: javax.inject.Provider[controllers.v2.ResourceCategoriesController],
  // @LINE:124
  EducationRequestController_14: javax.inject.Provider[controllers.v2.EducationRequestController],
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HealthCheckController_20: javax.inject.Provider[controllers.v1.HealthCheckController],
    // @LINE:9
    SessionController_9: javax.inject.Provider[controllers.v1.SessionController],
    // @LINE:13
    PasswordController_19: javax.inject.Provider[controllers.v1.PasswordController],
    // @LINE:19
    CategoriesController_5: javax.inject.Provider[controllers.v2.CategoriesController],
    // @LINE:25
    FavouritesController_10: javax.inject.Provider[controllers.v2.FavouritesController],
    // @LINE:28
    VendorController_7: javax.inject.Provider[controllers.v2.VendorController],
    // @LINE:35
    OrdersController_6: javax.inject.Provider[controllers.v1.OrdersController],
    // @LINE:41
    TreatmentsController_4: javax.inject.Provider[controllers.v1.TreatmentsController],
    // @LINE:48
    PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
    // @LINE:51
    ConsultationsController_11: javax.inject.Provider[controllers.v1.ConsultationsController],
    // @LINE:57
    UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
    // @LINE:63
    StaffController_18: javax.inject.Provider[controllers.v1.StaffController],
    // @LINE:70
    ProfileController_15: javax.inject.Provider[controllers.v2.ProfileController],
    // @LINE:73
    PracticeController_21: javax.inject.Provider[controllers.v1.PracticeController],
    // @LINE:79
    CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
    // @LINE:85
    CoursesController_8: javax.inject.Provider[controllers.v1.CoursesController],
    // @LINE:91
    UploadsController_12: javax.inject.Provider[controllers.v1.UploadsController],
    // @LINE:93
    EmailMappingController_22: javax.inject.Provider[controllers.v2.EmailMappingController],
    // @LINE:101
    ShippingController_13: javax.inject.Provider[controllers.v2.ShippingController],
    // @LINE:107
    FormsController_3: javax.inject.Provider[controllers.v2.FormsController],
    // @LINE:112
    PrescriptionController_16: javax.inject.Provider[controllers.v2.PrescriptionController],
    // @LINE:117
    ResourceCategoriesController_17: javax.inject.Provider[controllers.v2.ResourceCategoriesController],
    // @LINE:124
    EducationRequestController_14: javax.inject.Provider[controllers.v2.EducationRequestController]
  ) = this(errorHandler, HealthCheckController_20, SessionController_9, PasswordController_19, CategoriesController_5, FavouritesController_10, VendorController_7, OrdersController_6, TreatmentsController_4, PatientsController_2, ConsultationsController_11, UsersController_0, StaffController_18, ProfileController_15, PracticeController_21, CoursesController_1, CoursesController_8, UploadsController_12, EmailMappingController_22, ShippingController_13, FormsController_3, PrescriptionController_16, ResourceCategoriesController_17, EducationRequestController_14, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    v2.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HealthCheckController_20, SessionController_9, PasswordController_19, CategoriesController_5, FavouritesController_10, VendorController_7, OrdersController_6, TreatmentsController_4, PatientsController_2, ConsultationsController_11, UsersController_0, StaffController_18, ProfileController_15, PracticeController_21, CoursesController_1, CoursesController_8, UploadsController_12, EmailMappingController_22, ShippingController_13, FormsController_3, PrescriptionController_16, ResourceCategoriesController_17, EducationRequestController_14, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """healthcheck""", """@controllers.v1.HealthCheckController@.check"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.check"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logIn"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logOut"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forgot/""" + "$" + """email<[^/]+>""", """@controllers.v1.PasswordController@.forgot(email:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.check(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.reset(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """password""", """@controllers.v1.PasswordController@.change"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.readAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.bulkUpdate"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.add(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.remove(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.archive(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>/unarchive""", """@controllers.v2.VendorController@.unarchive(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.TreatmentsController@.pdf(id:Long, step:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients""", """@controllers.v1.PatientsController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients/""" + "$" + """id<[^/]+>""", """@controllers.v1.PatientsController@.read(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.readAll(practice:Option[Long])"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.update(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/udid""", """@controllers.v1.StaffController@.setUDID"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.read"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents""", """@controllers.v3.CoursesController@.readAll(resource:String = "resource")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.read(resource:String = "resource", id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents""", """@controllers.v3.CoursesController@.create(resource:String = "resource")"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.update(resource:String = "resource", id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.delete(resource:String = "resource", id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos""", """@controllers.v1.CoursesController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.read(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos""", """@controllers.v1.CoursesController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signature/""" + "$" + """name<[^/]+>""", """@controllers.v1.UploadsController@.signature(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/add""", """@controllers.v2.EmailMappingController@.addUser(id:Long, userId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/rem""", """@controllers.v2.EmailMappingController@.removeUser(id:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forms""", """@controllers.v2.FormsController@.readForms"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.readAllConsents"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents/""" + "$" + """id<[^/]+>""", """@controllers.v2.FormsController@.updateConsent(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.addConsent"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.create"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions/""" + "$" + """id<[^/]+>""", """@controllers.v2.PrescriptionController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescription/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v2.PrescriptionController@.generatePdf(id:Long, treatment:String, signature:String, timezone:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.readAll(resourceType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.read(resourceType:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.create(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.bulkUpdate(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.update(resourceType:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.delete(resourceType:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """education""", """@controllers.v2.EducationRequestController@.create"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_v1_HealthCheckController_check0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("healthcheck")))
  )
  private[this] lazy val controllers_v1_HealthCheckController_check0_invoker = createInvoker(
    HealthCheckController_20.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.HealthCheckController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """healthcheck"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_v1_SessionController_check1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_check1_invoker = createInvoker(
    SessionController_9.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.SessionController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_v1_SessionController_logIn2_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logIn2_invoker = createInvoker(
    SessionController_9.get.logIn,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.SessionController",
      "logIn",
      Nil,
      "PUT",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_v1_SessionController_logOut3_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logOut3_invoker = createInvoker(
    SessionController_9.get.logOut,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.SessionController",
      "logOut",
      Nil,
      "DELETE",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_v1_PasswordController_forgot4_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forgot/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_forgot4_invoker = createInvoker(
    PasswordController_19.get.forgot(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PasswordController",
      "forgot",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """forgot/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_v1_PasswordController_check5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_check5_invoker = createInvoker(
    PasswordController_19.get.check(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PasswordController",
      "check",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:15
  private[this] lazy val controllers_v1_PasswordController_reset6_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_reset6_invoker = createInvoker(
    PasswordController_19.get.reset(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PasswordController",
      "reset",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_v1_PasswordController_change7_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("password")))
  )
  private[this] lazy val controllers_v1_PasswordController_change7_invoker = createInvoker(
    PasswordController_19.get.change,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PasswordController",
      "change",
      Nil,
      "PUT",
      """""",
      this.prefix + """password"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_v2_CategoriesController_readAll8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_readAll8_invoker = createInvoker(
    CategoriesController_5.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.CategoriesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate9_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate9_invoker = createInvoker(
    CategoriesController_5.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.CategoriesController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_v2_CategoriesController_update10_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_update10_invoker = createInvoker(
    CategoriesController_5.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.CategoriesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_v2_CategoriesController_delete11_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_delete11_invoker = createInvoker(
    CategoriesController_5.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.CategoriesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_v2_FavouritesController_add12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_add12_invoker = createInvoker(
    FavouritesController_10.get.add(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FavouritesController",
      "add",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_v2_FavouritesController_remove13_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_remove13_invoker = createInvoker(
    FavouritesController_10.get.remove(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FavouritesController",
      "remove",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_v2_VendorController_readAll14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_readAll14_invoker = createInvoker(
    VendorController_7.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_v2_VendorController_create15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_create15_invoker = createInvoker(
    VendorController_7.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_v2_VendorController_read16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_read16_invoker = createInvoker(
    VendorController_7.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_v2_VendorController_update17_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_update17_invoker = createInvoker(
    VendorController_7.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_v2_VendorController_archive18_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_archive18_invoker = createInvoker(
    VendorController_7.get.archive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "archive",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_v2_VendorController_unarchive19_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true), StaticPart("/unarchive")))
  )
  private[this] lazy val controllers_v2_VendorController_unarchive19_invoker = createInvoker(
    VendorController_7.get.unarchive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.VendorController",
      "unarchive",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>/unarchive"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_v1_OrdersController_readAll20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_readAll20_invoker = createInvoker(
    OrdersController_6.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.OrdersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_v1_OrdersController_create21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_create21_invoker = createInvoker(
    OrdersController_6.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.OrdersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_v1_OrdersController_read22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_read22_invoker = createInvoker(
    OrdersController_6.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.OrdersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_v1_OrdersController_update23_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_update23_invoker = createInvoker(
    OrdersController_6.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.OrdersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_v1_OrdersController_delete24_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_delete24_invoker = createInvoker(
    OrdersController_6.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.OrdersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_v1_TreatmentsController_readAll25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_readAll25_invoker = createInvoker(
    TreatmentsController_4.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:42
  private[this] lazy val controllers_v1_TreatmentsController_create26_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_create26_invoker = createInvoker(
    TreatmentsController_4.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:43
  private[this] lazy val controllers_v1_TreatmentsController_read27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_read27_invoker = createInvoker(
    TreatmentsController_4.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:44
  private[this] lazy val controllers_v1_TreatmentsController_update28_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_update28_invoker = createInvoker(
    TreatmentsController_4.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_v1_TreatmentsController_delete29_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_delete29_invoker = createInvoker(
    TreatmentsController_4.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_v1_TreatmentsController_pdf30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_pdf30_invoker = createInvoker(
    TreatmentsController_4.get.pdf(fakeValue[Long], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.TreatmentsController",
      "pdf",
      Seq(classOf[Long], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:48
  private[this] lazy val controllers_v1_PatientsController_readAll31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients")))
  )
  private[this] lazy val controllers_v1_PatientsController_readAll31_invoker = createInvoker(
    PatientsController_2.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PatientsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """patients"""
    )
  )

  // @LINE:49
  private[this] lazy val controllers_v1_PatientsController_read32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PatientsController_read32_invoker = createInvoker(
    PatientsController_2.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PatientsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """patients/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:51
  private[this] lazy val controllers_v1_ConsultationsController_readAll33_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_readAll33_invoker = createInvoker(
    ConsultationsController_11.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.ConsultationsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_v1_ConsultationsController_create34_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_create34_invoker = createInvoker(
    ConsultationsController_11.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.ConsultationsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:53
  private[this] lazy val controllers_v1_ConsultationsController_read35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_read35_invoker = createInvoker(
    ConsultationsController_11.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.ConsultationsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_v1_ConsultationsController_update36_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_update36_invoker = createInvoker(
    ConsultationsController_11.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.ConsultationsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:55
  private[this] lazy val controllers_v1_ConsultationsController_delete37_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_delete37_invoker = createInvoker(
    ConsultationsController_11.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.ConsultationsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_v1_UsersController_readAll38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_readAll38_invoker = createInvoker(
    UsersController_0.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UsersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_v1_UsersController_create39_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_create39_invoker = createInvoker(
    UsersController_0.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UsersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:59
  private[this] lazy val controllers_v1_UsersController_read40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_read40_invoker = createInvoker(
    UsersController_0.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UsersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:60
  private[this] lazy val controllers_v1_UsersController_update41_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_update41_invoker = createInvoker(
    UsersController_0.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UsersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_v1_UsersController_delete42_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_delete42_invoker = createInvoker(
    UsersController_0.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UsersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:63
  private[this] lazy val controllers_v1_StaffController_readAll43_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_readAll43_invoker = createInvoker(
    StaffController_18.get.readAll(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "readAll",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:64
  private[this] lazy val controllers_v1_StaffController_create44_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_create44_invoker = createInvoker(
    StaffController_18.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:65
  private[this] lazy val controllers_v1_StaffController_read45_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_read45_invoker = createInvoker(
    StaffController_18.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:66
  private[this] lazy val controllers_v1_StaffController_update46_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_update46_invoker = createInvoker(
    StaffController_18.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:67
  private[this] lazy val controllers_v1_StaffController_setUDID47_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/udid")))
  )
  private[this] lazy val controllers_v1_StaffController_setUDID47_invoker = createInvoker(
    StaffController_18.get.setUDID,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "setUDID",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/udid"""
    )
  )

  // @LINE:68
  private[this] lazy val controllers_v1_StaffController_delete48_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_delete48_invoker = createInvoker(
    StaffController_18.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.StaffController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:70
  private[this] lazy val controllers_v2_ProfileController_read49_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_read49_invoker = createInvoker(
    ProfileController_15.get.read,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ProfileController",
      "read",
      Nil,
      "GET",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:71
  private[this] lazy val controllers_v2_ProfileController_update50_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_update50_invoker = createInvoker(
    ProfileController_15.get.update,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ProfileController",
      "update",
      Nil,
      "PUT",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:73
  private[this] lazy val controllers_v1_PracticeController_readAll51_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_readAll51_invoker = createInvoker(
    PracticeController_21.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PracticeController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:74
  private[this] lazy val controllers_v1_PracticeController_create52_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_create52_invoker = createInvoker(
    PracticeController_21.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PracticeController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:75
  private[this] lazy val controllers_v1_PracticeController_read53_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_read53_invoker = createInvoker(
    PracticeController_21.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PracticeController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:76
  private[this] lazy val controllers_v1_PracticeController_update54_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_update54_invoker = createInvoker(
    PracticeController_21.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PracticeController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:77
  private[this] lazy val controllers_v1_PracticeController_delete55_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_delete55_invoker = createInvoker(
    PracticeController_21.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.PracticeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_v3_CoursesController_readAll56_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents")))
  )
  private[this] lazy val controllers_v3_CoursesController_readAll56_invoker = createInvoker(
    CoursesController_1.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v3.CoursesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """documents"""
    )
  )

  // @LINE:80
  private[this] lazy val controllers_v3_CoursesController_read57_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_read57_invoker = createInvoker(
    CoursesController_1.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v3.CoursesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:81
  private[this] lazy val controllers_v3_CoursesController_create58_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents")))
  )
  private[this] lazy val controllers_v3_CoursesController_create58_invoker = createInvoker(
    CoursesController_1.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v3.CoursesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """documents"""
    )
  )

  // @LINE:82
  private[this] lazy val controllers_v3_CoursesController_update59_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_update59_invoker = createInvoker(
    CoursesController_1.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v3.CoursesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:83
  private[this] lazy val controllers_v3_CoursesController_delete60_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_delete60_invoker = createInvoker(
    CoursesController_1.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v3.CoursesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:85
  private[this] lazy val controllers_v1_CoursesController_readAll61_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos")))
  )
  private[this] lazy val controllers_v1_CoursesController_readAll61_invoker = createInvoker(
    CoursesController_8.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.CoursesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """videos"""
    )
  )

  // @LINE:86
  private[this] lazy val controllers_v1_CoursesController_read62_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_read62_invoker = createInvoker(
    CoursesController_8.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.CoursesController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:87
  private[this] lazy val controllers_v1_CoursesController_create63_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos")))
  )
  private[this] lazy val controllers_v1_CoursesController_create63_invoker = createInvoker(
    CoursesController_8.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.CoursesController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """videos"""
    )
  )

  // @LINE:88
  private[this] lazy val controllers_v1_CoursesController_update64_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_update64_invoker = createInvoker(
    CoursesController_8.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.CoursesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:89
  private[this] lazy val controllers_v1_CoursesController_delete65_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_delete65_invoker = createInvoker(
    CoursesController_8.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.CoursesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:91
  private[this] lazy val controllers_v1_UploadsController_signature66_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signature/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UploadsController_signature66_invoker = createInvoker(
    UploadsController_12.get.signature(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v1.UploadsController",
      "signature",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """signature/""" + "$" + """name<[^/]+>"""
    )
  )

  // @LINE:93
  private[this] lazy val controllers_v2_EmailMappingController_readAll67_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_readAll67_invoker = createInvoker(
    EmailMappingController_22.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:94
  private[this] lazy val controllers_v2_EmailMappingController_create68_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_create68_invoker = createInvoker(
    EmailMappingController_22.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:95
  private[this] lazy val controllers_v2_EmailMappingController_read69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_read69_invoker = createInvoker(
    EmailMappingController_22.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:96
  private[this] lazy val controllers_v2_EmailMappingController_update70_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_update70_invoker = createInvoker(
    EmailMappingController_22.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:97
  private[this] lazy val controllers_v2_EmailMappingController_delete71_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_delete71_invoker = createInvoker(
    EmailMappingController_22.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:98
  private[this] lazy val controllers_v2_EmailMappingController_addUser72_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/add")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_addUser72_invoker = createInvoker(
    EmailMappingController_22.get.addUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "addUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/add"""
    )
  )

  // @LINE:99
  private[this] lazy val controllers_v2_EmailMappingController_removeUser73_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/rem")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_removeUser73_invoker = createInvoker(
    EmailMappingController_22.get.removeUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EmailMappingController",
      "removeUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/rem"""
    )
  )

  // @LINE:101
  private[this] lazy val controllers_v2_ShippingController_readAll74_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_readAll74_invoker = createInvoker(
    ShippingController_13.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ShippingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:102
  private[this] lazy val controllers_v2_ShippingController_create75_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_create75_invoker = createInvoker(
    ShippingController_13.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ShippingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:103
  private[this] lazy val controllers_v2_ShippingController_read76_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_read76_invoker = createInvoker(
    ShippingController_13.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ShippingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:104
  private[this] lazy val controllers_v2_ShippingController_update77_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_update77_invoker = createInvoker(
    ShippingController_13.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ShippingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:105
  private[this] lazy val controllers_v2_ShippingController_delete78_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_delete78_invoker = createInvoker(
    ShippingController_13.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ShippingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:107
  private[this] lazy val controllers_v2_FormsController_readForms79_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forms")))
  )
  private[this] lazy val controllers_v2_FormsController_readForms79_invoker = createInvoker(
    FormsController_3.get.readForms,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FormsController",
      "readForms",
      Nil,
      "GET",
      """""",
      this.prefix + """forms"""
    )
  )

  // @LINE:108
  private[this] lazy val controllers_v2_FormsController_readAllConsents80_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_readAllConsents80_invoker = createInvoker(
    FormsController_3.get.readAllConsents,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FormsController",
      "readAllConsents",
      Nil,
      "GET",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:109
  private[this] lazy val controllers_v2_FormsController_updateConsent81_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_FormsController_updateConsent81_invoker = createInvoker(
    FormsController_3.get.updateConsent(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FormsController",
      "updateConsent",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """consents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:110
  private[this] lazy val controllers_v2_FormsController_addConsent82_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_addConsent82_invoker = createInvoker(
    FormsController_3.get.addConsent,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.FormsController",
      "addConsent",
      Nil,
      "POST",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:112
  private[this] lazy val controllers_v2_PrescriptionController_readAll83_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_readAll83_invoker = createInvoker(
    PrescriptionController_16.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.PrescriptionController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:113
  private[this] lazy val controllers_v2_PrescriptionController_create84_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_create84_invoker = createInvoker(
    PrescriptionController_16.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.PrescriptionController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:114
  private[this] lazy val controllers_v2_PrescriptionController_delete85_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionController_delete85_invoker = createInvoker(
    PrescriptionController_16.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.PrescriptionController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """prescriptions/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:115
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf86_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescription/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf86_invoker = createInvoker(
    PrescriptionController_16.get.generatePdf(fakeValue[Long], fakeValue[String], fakeValue[String], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.PrescriptionController",
      "generatePdf",
      Seq(classOf[Long], classOf[String], classOf[String], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """prescription/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:117
  private[this] lazy val controllers_v2_ResourceCategoriesController_readAll87_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_readAll87_invoker = createInvoker(
    ResourceCategoriesController_17.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:118
  private[this] lazy val controllers_v2_ResourceCategoriesController_read88_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_read88_invoker = createInvoker(
    ResourceCategoriesController_17.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:119
  private[this] lazy val controllers_v2_ResourceCategoriesController_create89_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_create89_invoker = createInvoker(
    ResourceCategoriesController_17.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:120
  private[this] lazy val controllers_v2_ResourceCategoriesController_bulkUpdate90_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_bulkUpdate90_invoker = createInvoker(
    ResourceCategoriesController_17.get.bulkUpdate(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "bulkUpdate",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:121
  private[this] lazy val controllers_v2_ResourceCategoriesController_update91_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_update91_invoker = createInvoker(
    ResourceCategoriesController_17.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:122
  private[this] lazy val controllers_v2_ResourceCategoriesController_delete92_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_delete92_invoker = createInvoker(
    ResourceCategoriesController_17.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.ResourceCategoriesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:124
  private[this] lazy val controllers_v2_EducationRequestController_create93_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("education")))
  )
  private[this] lazy val controllers_v2_EducationRequestController_create93_invoker = createInvoker(
    EducationRequestController_14.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v2",
      "controllers.v2.EducationRequestController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """education"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_v1_HealthCheckController_check0_route(params) =>
      call { 
        controllers_v1_HealthCheckController_check0_invoker.call(HealthCheckController_20.get.check)
      }
  
    // @LINE:9
    case controllers_v1_SessionController_check1_route(params) =>
      call { 
        controllers_v1_SessionController_check1_invoker.call(SessionController_9.get.check)
      }
  
    // @LINE:10
    case controllers_v1_SessionController_logIn2_route(params) =>
      call { 
        controllers_v1_SessionController_logIn2_invoker.call(SessionController_9.get.logIn)
      }
  
    // @LINE:11
    case controllers_v1_SessionController_logOut3_route(params) =>
      call { 
        controllers_v1_SessionController_logOut3_invoker.call(SessionController_9.get.logOut)
      }
  
    // @LINE:13
    case controllers_v1_PasswordController_forgot4_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_PasswordController_forgot4_invoker.call(PasswordController_19.get.forgot(email))
      }
  
    // @LINE:14
    case controllers_v1_PasswordController_check5_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_check5_invoker.call(PasswordController_19.get.check(secret))
      }
  
    // @LINE:15
    case controllers_v1_PasswordController_reset6_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_reset6_invoker.call(PasswordController_19.get.reset(secret))
      }
  
    // @LINE:16
    case controllers_v1_PasswordController_change7_route(params) =>
      call { 
        controllers_v1_PasswordController_change7_invoker.call(PasswordController_19.get.change)
      }
  
    // @LINE:19
    case controllers_v2_CategoriesController_readAll8_route(params) =>
      call { 
        controllers_v2_CategoriesController_readAll8_invoker.call(CategoriesController_5.get.readAll)
      }
  
    // @LINE:20
    case controllers_v2_CategoriesController_bulkUpdate9_route(params) =>
      call { 
        controllers_v2_CategoriesController_bulkUpdate9_invoker.call(CategoriesController_5.get.bulkUpdate)
      }
  
    // @LINE:21
    case controllers_v2_CategoriesController_update10_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_update10_invoker.call(CategoriesController_5.get.update(id))
      }
  
    // @LINE:22
    case controllers_v2_CategoriesController_delete11_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_delete11_invoker.call(CategoriesController_5.get.delete(id))
      }
  
    // @LINE:25
    case controllers_v2_FavouritesController_add12_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_add12_invoker.call(FavouritesController_10.get.add(id))
      }
  
    // @LINE:26
    case controllers_v2_FavouritesController_remove13_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_remove13_invoker.call(FavouritesController_10.get.remove(id))
      }
  
    // @LINE:28
    case controllers_v2_VendorController_readAll14_route(params) =>
      call { 
        controllers_v2_VendorController_readAll14_invoker.call(VendorController_7.get.readAll)
      }
  
    // @LINE:29
    case controllers_v2_VendorController_create15_route(params) =>
      call { 
        controllers_v2_VendorController_create15_invoker.call(VendorController_7.get.create)
      }
  
    // @LINE:30
    case controllers_v2_VendorController_read16_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_read16_invoker.call(VendorController_7.get.read(id))
      }
  
    // @LINE:31
    case controllers_v2_VendorController_update17_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_update17_invoker.call(VendorController_7.get.update(id))
      }
  
    // @LINE:32
    case controllers_v2_VendorController_archive18_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_archive18_invoker.call(VendorController_7.get.archive(id))
      }
  
    // @LINE:33
    case controllers_v2_VendorController_unarchive19_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_unarchive19_invoker.call(VendorController_7.get.unarchive(id))
      }
  
    // @LINE:35
    case controllers_v1_OrdersController_readAll20_route(params) =>
      call { 
        controllers_v1_OrdersController_readAll20_invoker.call(OrdersController_6.get.readAll)
      }
  
    // @LINE:36
    case controllers_v1_OrdersController_create21_route(params) =>
      call { 
        controllers_v1_OrdersController_create21_invoker.call(OrdersController_6.get.create)
      }
  
    // @LINE:37
    case controllers_v1_OrdersController_read22_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_read22_invoker.call(OrdersController_6.get.read(id))
      }
  
    // @LINE:38
    case controllers_v1_OrdersController_update23_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_update23_invoker.call(OrdersController_6.get.update(id))
      }
  
    // @LINE:39
    case controllers_v1_OrdersController_delete24_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_delete24_invoker.call(OrdersController_6.get.delete(id))
      }
  
    // @LINE:41
    case controllers_v1_TreatmentsController_readAll25_route(params) =>
      call { 
        controllers_v1_TreatmentsController_readAll25_invoker.call(TreatmentsController_4.get.readAll)
      }
  
    // @LINE:42
    case controllers_v1_TreatmentsController_create26_route(params) =>
      call { 
        controllers_v1_TreatmentsController_create26_invoker.call(TreatmentsController_4.get.create)
      }
  
    // @LINE:43
    case controllers_v1_TreatmentsController_read27_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_read27_invoker.call(TreatmentsController_4.get.read(id))
      }
  
    // @LINE:44
    case controllers_v1_TreatmentsController_update28_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_update28_invoker.call(TreatmentsController_4.get.update(id))
      }
  
    // @LINE:45
    case controllers_v1_TreatmentsController_delete29_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_delete29_invoker.call(TreatmentsController_4.get.delete(id))
      }
  
    // @LINE:46
    case controllers_v1_TreatmentsController_pdf30_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Option[String]]("step", None)) { (id, step) =>
        controllers_v1_TreatmentsController_pdf30_invoker.call(TreatmentsController_4.get.pdf(id, step))
      }
  
    // @LINE:48
    case controllers_v1_PatientsController_readAll31_route(params) =>
      call { 
        controllers_v1_PatientsController_readAll31_invoker.call(PatientsController_2.get.readAll)
      }
  
    // @LINE:49
    case controllers_v1_PatientsController_read32_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_read32_invoker.call(PatientsController_2.get.read(id))
      }
  
    // @LINE:51
    case controllers_v1_ConsultationsController_readAll33_route(params) =>
      call { 
        controllers_v1_ConsultationsController_readAll33_invoker.call(ConsultationsController_11.get.readAll)
      }
  
    // @LINE:52
    case controllers_v1_ConsultationsController_create34_route(params) =>
      call { 
        controllers_v1_ConsultationsController_create34_invoker.call(ConsultationsController_11.get.create)
      }
  
    // @LINE:53
    case controllers_v1_ConsultationsController_read35_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_read35_invoker.call(ConsultationsController_11.get.read(id))
      }
  
    // @LINE:54
    case controllers_v1_ConsultationsController_update36_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_update36_invoker.call(ConsultationsController_11.get.update(id))
      }
  
    // @LINE:55
    case controllers_v1_ConsultationsController_delete37_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_delete37_invoker.call(ConsultationsController_11.get.delete(id))
      }
  
    // @LINE:57
    case controllers_v1_UsersController_readAll38_route(params) =>
      call { 
        controllers_v1_UsersController_readAll38_invoker.call(UsersController_0.get.readAll)
      }
  
    // @LINE:58
    case controllers_v1_UsersController_create39_route(params) =>
      call { 
        controllers_v1_UsersController_create39_invoker.call(UsersController_0.get.create)
      }
  
    // @LINE:59
    case controllers_v1_UsersController_read40_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_read40_invoker.call(UsersController_0.get.read(id))
      }
  
    // @LINE:60
    case controllers_v1_UsersController_update41_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_update41_invoker.call(UsersController_0.get.update(id))
      }
  
    // @LINE:61
    case controllers_v1_UsersController_delete42_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_delete42_invoker.call(UsersController_0.get.delete(id))
      }
  
    // @LINE:63
    case controllers_v1_StaffController_readAll43_route(params) =>
      call(params.fromQuery[Option[Long]]("practice", None)) { (practice) =>
        controllers_v1_StaffController_readAll43_invoker.call(StaffController_18.get.readAll(practice))
      }
  
    // @LINE:64
    case controllers_v1_StaffController_create44_route(params) =>
      call { 
        controllers_v1_StaffController_create44_invoker.call(StaffController_18.get.create)
      }
  
    // @LINE:65
    case controllers_v1_StaffController_read45_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_read45_invoker.call(StaffController_18.get.read(id))
      }
  
    // @LINE:66
    case controllers_v1_StaffController_update46_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_update46_invoker.call(StaffController_18.get.update(id))
      }
  
    // @LINE:67
    case controllers_v1_StaffController_setUDID47_route(params) =>
      call { 
        controllers_v1_StaffController_setUDID47_invoker.call(StaffController_18.get.setUDID)
      }
  
    // @LINE:68
    case controllers_v1_StaffController_delete48_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_delete48_invoker.call(StaffController_18.get.delete(id))
      }
  
    // @LINE:70
    case controllers_v2_ProfileController_read49_route(params) =>
      call { 
        controllers_v2_ProfileController_read49_invoker.call(ProfileController_15.get.read)
      }
  
    // @LINE:71
    case controllers_v2_ProfileController_update50_route(params) =>
      call { 
        controllers_v2_ProfileController_update50_invoker.call(ProfileController_15.get.update)
      }
  
    // @LINE:73
    case controllers_v1_PracticeController_readAll51_route(params) =>
      call { 
        controllers_v1_PracticeController_readAll51_invoker.call(PracticeController_21.get.readAll)
      }
  
    // @LINE:74
    case controllers_v1_PracticeController_create52_route(params) =>
      call { 
        controllers_v1_PracticeController_create52_invoker.call(PracticeController_21.get.create)
      }
  
    // @LINE:75
    case controllers_v1_PracticeController_read53_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_read53_invoker.call(PracticeController_21.get.read(id))
      }
  
    // @LINE:76
    case controllers_v1_PracticeController_update54_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_update54_invoker.call(PracticeController_21.get.update(id))
      }
  
    // @LINE:77
    case controllers_v1_PracticeController_delete55_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_delete55_invoker.call(PracticeController_21.get.delete(id))
      }
  
    // @LINE:79
    case controllers_v3_CoursesController_readAll56_route(params) =>
      call(Param[String]("resource", Right("resource"))) { (resource) =>
        controllers_v3_CoursesController_readAll56_invoker.call(CoursesController_1.get.readAll(resource))
      }
  
    // @LINE:80
    case controllers_v3_CoursesController_read57_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_read57_invoker.call(CoursesController_1.get.read(resource, id))
      }
  
    // @LINE:81
    case controllers_v3_CoursesController_create58_route(params) =>
      call(Param[String]("resource", Right("resource"))) { (resource) =>
        controllers_v3_CoursesController_create58_invoker.call(CoursesController_1.get.create(resource))
      }
  
    // @LINE:82
    case controllers_v3_CoursesController_update59_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_update59_invoker.call(CoursesController_1.get.update(resource, id))
      }
  
    // @LINE:83
    case controllers_v3_CoursesController_delete60_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_delete60_invoker.call(CoursesController_1.get.delete(resource, id))
      }
  
    // @LINE:85
    case controllers_v1_CoursesController_readAll61_route(params) =>
      call { 
        controllers_v1_CoursesController_readAll61_invoker.call(CoursesController_8.get.readAll)
      }
  
    // @LINE:86
    case controllers_v1_CoursesController_read62_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_read62_invoker.call(CoursesController_8.get.read(id))
      }
  
    // @LINE:87
    case controllers_v1_CoursesController_create63_route(params) =>
      call { 
        controllers_v1_CoursesController_create63_invoker.call(CoursesController_8.get.create)
      }
  
    // @LINE:88
    case controllers_v1_CoursesController_update64_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_update64_invoker.call(CoursesController_8.get.update(id))
      }
  
    // @LINE:89
    case controllers_v1_CoursesController_delete65_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_delete65_invoker.call(CoursesController_8.get.delete(id))
      }
  
    // @LINE:91
    case controllers_v1_UploadsController_signature66_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_v1_UploadsController_signature66_invoker.call(UploadsController_12.get.signature(name))
      }
  
    // @LINE:93
    case controllers_v2_EmailMappingController_readAll67_route(params) =>
      call { 
        controllers_v2_EmailMappingController_readAll67_invoker.call(EmailMappingController_22.get.readAll)
      }
  
    // @LINE:94
    case controllers_v2_EmailMappingController_create68_route(params) =>
      call { 
        controllers_v2_EmailMappingController_create68_invoker.call(EmailMappingController_22.get.create)
      }
  
    // @LINE:95
    case controllers_v2_EmailMappingController_read69_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_read69_invoker.call(EmailMappingController_22.get.read(id))
      }
  
    // @LINE:96
    case controllers_v2_EmailMappingController_update70_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_update70_invoker.call(EmailMappingController_22.get.update(id))
      }
  
    // @LINE:97
    case controllers_v2_EmailMappingController_delete71_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_delete71_invoker.call(EmailMappingController_22.get.delete(id))
      }
  
    // @LINE:98
    case controllers_v2_EmailMappingController_addUser72_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_addUser72_invoker.call(EmailMappingController_22.get.addUser(id, userId))
      }
  
    // @LINE:99
    case controllers_v2_EmailMappingController_removeUser73_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_removeUser73_invoker.call(EmailMappingController_22.get.removeUser(id, userId))
      }
  
    // @LINE:101
    case controllers_v2_ShippingController_readAll74_route(params) =>
      call { 
        controllers_v2_ShippingController_readAll74_invoker.call(ShippingController_13.get.readAll)
      }
  
    // @LINE:102
    case controllers_v2_ShippingController_create75_route(params) =>
      call { 
        controllers_v2_ShippingController_create75_invoker.call(ShippingController_13.get.create)
      }
  
    // @LINE:103
    case controllers_v2_ShippingController_read76_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_read76_invoker.call(ShippingController_13.get.read(id))
      }
  
    // @LINE:104
    case controllers_v2_ShippingController_update77_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_update77_invoker.call(ShippingController_13.get.update(id))
      }
  
    // @LINE:105
    case controllers_v2_ShippingController_delete78_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_delete78_invoker.call(ShippingController_13.get.delete(id))
      }
  
    // @LINE:107
    case controllers_v2_FormsController_readForms79_route(params) =>
      call { 
        controllers_v2_FormsController_readForms79_invoker.call(FormsController_3.get.readForms)
      }
  
    // @LINE:108
    case controllers_v2_FormsController_readAllConsents80_route(params) =>
      call { 
        controllers_v2_FormsController_readAllConsents80_invoker.call(FormsController_3.get.readAllConsents)
      }
  
    // @LINE:109
    case controllers_v2_FormsController_updateConsent81_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_v2_FormsController_updateConsent81_invoker.call(FormsController_3.get.updateConsent(id))
      }
  
    // @LINE:110
    case controllers_v2_FormsController_addConsent82_route(params) =>
      call { 
        controllers_v2_FormsController_addConsent82_invoker.call(FormsController_3.get.addConsent)
      }
  
    // @LINE:112
    case controllers_v2_PrescriptionController_readAll83_route(params) =>
      call { 
        controllers_v2_PrescriptionController_readAll83_invoker.call(PrescriptionController_16.get.readAll)
      }
  
    // @LINE:113
    case controllers_v2_PrescriptionController_create84_route(params) =>
      call { 
        controllers_v2_PrescriptionController_create84_invoker.call(PrescriptionController_16.get.create)
      }
  
    // @LINE:114
    case controllers_v2_PrescriptionController_delete85_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_PrescriptionController_delete85_invoker.call(PrescriptionController_16.get.delete(id))
      }
  
    // @LINE:115
    case controllers_v2_PrescriptionController_generatePdf86_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[String]("treatment", None), params.fromQuery[String]("signature", None), params.fromQuery[Option[String]]("timezone", None)) { (id, treatment, signature, timezone) =>
        controllers_v2_PrescriptionController_generatePdf86_invoker.call(PrescriptionController_16.get.generatePdf(id, treatment, signature, timezone))
      }
  
    // @LINE:117
    case controllers_v2_ResourceCategoriesController_readAll87_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_readAll87_invoker.call(ResourceCategoriesController_17.get.readAll(resourceType))
      }
  
    // @LINE:118
    case controllers_v2_ResourceCategoriesController_read88_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_read88_invoker.call(ResourceCategoriesController_17.get.read(resourceType, id))
      }
  
    // @LINE:119
    case controllers_v2_ResourceCategoriesController_create89_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_create89_invoker.call(ResourceCategoriesController_17.get.create(resourceType))
      }
  
    // @LINE:120
    case controllers_v2_ResourceCategoriesController_bulkUpdate90_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_bulkUpdate90_invoker.call(ResourceCategoriesController_17.get.bulkUpdate(resourceType))
      }
  
    // @LINE:121
    case controllers_v2_ResourceCategoriesController_update91_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_update91_invoker.call(ResourceCategoriesController_17.get.update(resourceType, id))
      }
  
    // @LINE:122
    case controllers_v2_ResourceCategoriesController_delete92_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_delete92_invoker.call(ResourceCategoriesController_17.get.delete(resourceType, id))
      }
  
    // @LINE:124
    case controllers_v2_EducationRequestController_create93_route(params) =>
      call { 
        controllers_v2_EducationRequestController_create93_invoker.call(EducationRequestController_14.get.create)
      }
  }
}
