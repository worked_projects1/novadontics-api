
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:67
package controllers.v3 {

  // @LINE:67
  class ReverseCoursesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:70
    def update(id:Long): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("resource", "resource")))
      Call("PUT", _prefix + { _defaultPrefix } + "documents/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:71
    def delete(id:Long): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("resource", "resource")))
      Call("DELETE", _prefix + { _defaultPrefix } + "documents/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:68
    def read(id:Long): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("resource", "resource")))
      Call("GET", _prefix + { _defaultPrefix } + "documents/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:67
    def readAll(): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("resource", "resource")))
      Call("GET", _prefix + { _defaultPrefix } + "documents")
    }
  
    // @LINE:69
    def create(): Call = {
      implicit val _rrc = new ReverseRouteContext(Map(("resource", "resource")))
      Call("POST", _prefix + { _defaultPrefix } + "documents")
    }
  
  }


}
