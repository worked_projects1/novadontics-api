
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:67
package controllers.v3.javascript {
  import ReverseRouteContext.empty

  // @LINE:67
  class ReverseCoursesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:70
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v3.CoursesController.update",
      """
        function(id1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "documents/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:71
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v3.CoursesController.delete",
      """
        function(id1) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "documents/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:68
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v3.CoursesController.read",
      """
        function(id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "documents/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:67
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v3.CoursesController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "documents"})
        }
      """
    )
  
    // @LINE:69
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v3.CoursesController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "documents"})
        }
      """
    )
  
  }


}
