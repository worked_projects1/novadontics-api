
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

package controllers.v3;

import v1.RoutesPrefix;

public class routes {
  
  public static final controllers.v3.ReverseCoursesController CoursesController = new controllers.v3.ReverseCoursesController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.v3.javascript.ReverseCoursesController CoursesController = new controllers.v3.javascript.ReverseCoursesController(RoutesPrefix.byNamePrefix());
  }

}
