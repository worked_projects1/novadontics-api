
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

package controllers.v1;

import v1.RoutesPrefix;

public class routes {
  
  public static final controllers.v1.ReverseStaffController StaffController = new controllers.v1.ReverseStaffController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseCategoriesController CategoriesController = new controllers.v1.ReverseCategoriesController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseTreatmentsController TreatmentsController = new controllers.v1.ReverseTreatmentsController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReversePasswordController PasswordController = new controllers.v1.ReversePasswordController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseUsersController UsersController = new controllers.v1.ReverseUsersController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseOrdersController OrdersController = new controllers.v1.ReverseOrdersController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseUploadsController UploadsController = new controllers.v1.ReverseUploadsController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReversePracticeController PracticeController = new controllers.v1.ReversePracticeController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseCoursesController CoursesController = new controllers.v1.ReverseCoursesController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReversePatientsController PatientsController = new controllers.v1.ReversePatientsController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseHealthCheckController HealthCheckController = new controllers.v1.ReverseHealthCheckController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseConsultationsController ConsultationsController = new controllers.v1.ReverseConsultationsController(RoutesPrefix.byNamePrefix());
  public static final controllers.v1.ReverseSessionController SessionController = new controllers.v1.ReverseSessionController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.v1.javascript.ReverseStaffController StaffController = new controllers.v1.javascript.ReverseStaffController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseCategoriesController CategoriesController = new controllers.v1.javascript.ReverseCategoriesController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseTreatmentsController TreatmentsController = new controllers.v1.javascript.ReverseTreatmentsController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReversePasswordController PasswordController = new controllers.v1.javascript.ReversePasswordController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseUsersController UsersController = new controllers.v1.javascript.ReverseUsersController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseOrdersController OrdersController = new controllers.v1.javascript.ReverseOrdersController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseUploadsController UploadsController = new controllers.v1.javascript.ReverseUploadsController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReversePracticeController PracticeController = new controllers.v1.javascript.ReversePracticeController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseCoursesController CoursesController = new controllers.v1.javascript.ReverseCoursesController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReversePatientsController PatientsController = new controllers.v1.javascript.ReversePatientsController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseHealthCheckController HealthCheckController = new controllers.v1.javascript.ReverseHealthCheckController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseConsultationsController ConsultationsController = new controllers.v1.javascript.ReverseConsultationsController(RoutesPrefix.byNamePrefix());
    public static final controllers.v1.javascript.ReverseSessionController SessionController = new controllers.v1.javascript.ReverseSessionController(RoutesPrefix.byNamePrefix());
  }

}
