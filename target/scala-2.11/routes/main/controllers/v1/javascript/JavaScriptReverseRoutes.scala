
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers.v1.javascript {
  import ReverseRouteContext.empty

  // @LINE:55
  class ReverseStaffController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:59
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.StaffController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:56
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.StaffController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts"})
        }
      """
    )
  
    // @LINE:57
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.StaffController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:58
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.StaffController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:55
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.StaffController.readAll",
      """
        function(practice0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts" + _qS([(""" + implicitly[QueryStringBindable[Option[Long]]].javascriptUnbind + """)("practice", practice0)])})
        }
      """
    )
  
  }

  // @LINE:19
  class ReverseCategoriesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def bulkUpdate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CategoriesController.bulkUpdate",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "categories"})
        }
      """
    )
  
    // @LINE:21
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CategoriesController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:22
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CategoriesController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:19
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CategoriesController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories"})
        }
      """
    )
  
  }

  // @LINE:31
  class ReverseTreatmentsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def pdf: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.pdf",
      """
        function(id0,step1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/pdf" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("step", step1)])})
        }
      """
    )
  
    // @LINE:35
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:31
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments"})
        }
      """
    )
  
    // @LINE:32
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments"})
        }
      """
    )
  
    // @LINE:33
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:34
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "treatments/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:41
    def readForms: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.TreatmentsController.readForms",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "forms"})
        }
      """
    )
  
  }

  // @LINE:13
  class ReversePasswordController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def forgot: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PasswordController.forgot",
      """
        function(email0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "forgot/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("email", encodeURIComponent(email0))})
        }
      """
    )
  
    // @LINE:14
    def check: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PasswordController.check",
      """
        function(secret0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("secret", encodeURIComponent(secret0))})
        }
      """
    )
  
    // @LINE:16
    def change: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PasswordController.change",
      """
        function() {
        
          if (true) {
            return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "password"})
          }
        
        }
      """
    )
  
    // @LINE:15
    def reset: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PasswordController.reset",
      """
        function(secret0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "reset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("secret", encodeURIComponent(secret0))})
        }
      """
    )
  
  }

  // @LINE:49
  class ReverseUsersController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:53
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UsersController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "users/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:49
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UsersController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
        }
      """
    )
  
    // @LINE:50
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UsersController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
        }
      """
    )
  
    // @LINE:51
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UsersController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:52
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UsersController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "users/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:25
  class ReverseOrdersController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.OrdersController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:25
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.OrdersController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders"})
        }
      """
    )
  
    // @LINE:26
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.OrdersController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "orders"})
        }
      """
    )
  
    // @LINE:27
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.OrdersController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:28
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.OrdersController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:79
  class ReverseUploadsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:79
    def signature: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.UploadsController.signature",
      """
        function(name0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "signature/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name0))})
        }
      """
    )
  
  }

  // @LINE:61
  class ReversePracticeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:65
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PracticeController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "practices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:61
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PracticeController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "practices"})
        }
      """
    )
  
    // @LINE:62
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PracticeController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "practices"})
        }
      """
    )
  
    // @LINE:63
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PracticeController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "practices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:64
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PracticeController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "practices/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:73
  class ReverseCoursesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:77
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CoursesController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "videos/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:73
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CoursesController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "videos"})
        }
      """
    )
  
    // @LINE:75
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CoursesController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "videos"})
        }
      """
    )
  
    // @LINE:74
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CoursesController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "videos/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:76
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.CoursesController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "videos/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:38
  class ReversePatientsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:38
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PatientsController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "patients"})
        }
      """
    )
  
    // @LINE:39
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.PatientsController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "patients/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseHealthCheckController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def check: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.HealthCheckController.check",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "healthcheck"})
        }
      """
    )
  
  }

  // @LINE:43
  class ReverseConsultationsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:47
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.ConsultationsController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "consultations/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:43
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.ConsultationsController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "consultations"})
        }
      """
    )
  
    // @LINE:44
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.ConsultationsController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "consultations"})
        }
      """
    )
  
    // @LINE:45
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.ConsultationsController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "consultations/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:46
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.ConsultationsController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "consultations/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseSessionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def logIn: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.SessionController.logIn",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "session"})
        }
      """
    )
  
    // @LINE:9
    def check: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.SessionController.check",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "session"})
        }
      """
    )
  
    // @LINE:11
    def logOut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v1.SessionController.logOut",
      """
        function() {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "session"})
        }
      """
    )
  
  }


}
