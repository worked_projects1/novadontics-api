
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v2.routes
// @DATE:Fri Jun 19 20:14:07 IST 2020

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:19
package controllers.v2.javascript {
  import ReverseRouteContext.empty

  // @LINE:117
  class ReverseResourceCategoriesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:121
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.update",
      """
        function(resourceType0,id1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:122
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.delete",
      """
        function(resourceType0,id1) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:120
    def bulkUpdate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.bulkUpdate",
      """
        function(resourceType0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0))})
        }
      """
    )
  
    // @LINE:118
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.read",
      """
        function(resourceType0,id1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0)) + "/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id1)})
        }
      """
    )
  
    // @LINE:117
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.readAll",
      """
        function(resourceType0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0))})
        }
      """
    )
  
    // @LINE:119
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ResourceCategoriesController.create",
      """
        function(resourceType0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "resource-categories/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("resourceType", encodeURIComponent(resourceType0))})
        }
      """
    )
  
  }

  // @LINE:112
  class ReversePrescriptionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:113
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.PrescriptionController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prescriptions"})
        }
      """
    )
  
    // @LINE:114
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.PrescriptionController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "prescriptions/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:112
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.PrescriptionController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "prescriptions"})
        }
      """
    )
  
    // @LINE:115
    def generatePdf: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.PrescriptionController.generatePdf",
      """
        function(id0,treatment1,signature2,timezone3) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "prescription/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/pdf" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("treatment", treatment1), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("signature", signature2), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("timezone", timezone3)])})
        }
      """
    )
  
  }

  // @LINE:19
  class ReverseCategoriesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def bulkUpdate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.CategoriesController.bulkUpdate",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "categories"})
        }
      """
    )
  
    // @LINE:21
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.CategoriesController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:22
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.CategoriesController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:19
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.CategoriesController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories"})
        }
      """
    )
  
  }

  // @LINE:124
  class ReverseEducationRequestController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:124
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EducationRequestController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "education"})
        }
      """
    )
  
  }

  // @LINE:107
  class ReverseFormsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:110
    def addConsent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FormsController.addConsent",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "consents"})
        }
      """
    )
  
    // @LINE:107
    def readForms: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FormsController.readForms",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "forms"})
        }
      """
    )
  
    // @LINE:109
    def updateConsent: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FormsController.updateConsent",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "consents/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id0))})
        }
      """
    )
  
    // @LINE:108
    def readAllConsents: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FormsController.readAllConsents",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "consents"})
        }
      """
    )
  
  }

  // @LINE:93
  class ReverseEmailMappingController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:97
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:93
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings"})
        }
      """
    )
  
    // @LINE:99
    def removeUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.removeUser",
      """
        function(id0,userId1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/rem" + _qS([(""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("userId", userId1)])})
        }
      """
    )
  
    // @LINE:94
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings"})
        }
      """
    )
  
    // @LINE:95
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:96
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:98
    def addUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.EmailMappingController.addUser",
      """
        function(id0,userId1) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "emailMappings/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/add" + _qS([(""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("userId", userId1)])})
        }
      """
    )
  
  }

  // @LINE:70
  class ReverseProfileController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:71
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ProfileController.update",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "profile"})
        }
      """
    )
  
    // @LINE:70
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ProfileController.read",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile"})
        }
      """
    )
  
  }

  // @LINE:25
  class ReverseFavouritesController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:26
    def remove: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FavouritesController.remove",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "products/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/favourite"})
        }
      """
    )
  
    // @LINE:25
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.FavouritesController.add",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "products/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/favourite"})
        }
      """
    )
  
  }

  // @LINE:101
  class ReverseShippingController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:105
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ShippingController.delete",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "shipping/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:101
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ShippingController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "shipping"})
        }
      """
    )
  
    // @LINE:102
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ShippingController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "shipping"})
        }
      """
    )
  
    // @LINE:103
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ShippingController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "shipping/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:104
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.ShippingController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "shipping/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:28
  class ReverseVendorController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def readAll: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.readAll",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors"})
        }
      """
    )
  
    // @LINE:33
    def unarchive: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.unarchive",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0) + "/unarchive"})
        }
      """
    )
  
    // @LINE:29
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.create",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors"})
        }
      """
    )
  
    // @LINE:30
    def read: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.read",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:31
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.update",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:32
    def archive: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.v2.VendorController.archive",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "vendors/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }


}
