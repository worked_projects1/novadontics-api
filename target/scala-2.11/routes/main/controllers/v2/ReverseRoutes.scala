
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v2.routes
// @DATE:Fri Jun 19 20:14:07 IST 2020

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:19
package controllers.v2 {

  // @LINE:117
  class ReverseResourceCategoriesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:121
    def update(resourceType:String, id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)) + "/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:122
    def delete(resourceType:String, id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)) + "/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:120
    def bulkUpdate(resourceType:String): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)))
    }
  
    // @LINE:118
    def read(resourceType:String, id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)) + "/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:117
    def readAll(resourceType:String): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)))
    }
  
    // @LINE:119
    def create(resourceType:String): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "resource-categories/" + implicitly[PathBindable[String]].unbind("resourceType", dynamicString(resourceType)))
    }
  
  }

  // @LINE:112
  class ReversePrescriptionController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:113
    def create(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "prescriptions")
    }
  
    // @LINE:114
    def delete(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "prescriptions/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:112
    def readAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "prescriptions")
    }
  
    // @LINE:115
    def generatePdf(id:Long, treatment:String, signature:String, timezone:Option[String]): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "prescription/" + implicitly[PathBindable[Long]].unbind("id", id) + "/pdf" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("treatment", treatment)), Some(implicitly[QueryStringBindable[String]].unbind("signature", signature)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("timezone", timezone)))))
    }
  
  }

  // @LINE:19
  class ReverseCategoriesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def bulkUpdate(): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "categories")
    }
  
    // @LINE:21
    def update(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "categories/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:22
    def delete(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "categories/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:19
    def readAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "categories")
    }
  
  }

  // @LINE:124
  class ReverseEducationRequestController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:124
    def create(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "education")
    }
  
  }

  // @LINE:107
  class ReverseFormsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:110
    def addConsent(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "consents")
    }
  
    // @LINE:107
    def readForms(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "forms")
    }
  
    // @LINE:109
    def updateConsent(id:String): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "consents/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
    }
  
    // @LINE:108
    def readAllConsents(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "consents")
    }
  
  }

  // @LINE:93
  class ReverseEmailMappingController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:97
    def delete(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "emailMappings/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:93
    def readAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "emailMappings")
    }
  
    // @LINE:99
    def removeUser(id:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "emailMappings/" + implicitly[PathBindable[Long]].unbind("id", id) + "/rem" + queryString(List(Some(implicitly[QueryStringBindable[Long]].unbind("userId", userId)))))
    }
  
    // @LINE:94
    def create(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "emailMappings")
    }
  
    // @LINE:95
    def read(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "emailMappings/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:96
    def update(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "emailMappings/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:98
    def addUser(id:Long, userId:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "emailMappings/" + implicitly[PathBindable[Long]].unbind("id", id) + "/add" + queryString(List(Some(implicitly[QueryStringBindable[Long]].unbind("userId", userId)))))
    }
  
  }

  // @LINE:70
  class ReverseProfileController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:71
    def update(): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "profile")
    }
  
    // @LINE:70
    def read(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "profile")
    }
  
  }

  // @LINE:25
  class ReverseFavouritesController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:26
    def remove(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "products/" + implicitly[PathBindable[Long]].unbind("id", id) + "/favourite")
    }
  
    // @LINE:25
    def add(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "products/" + implicitly[PathBindable[Long]].unbind("id", id) + "/favourite")
    }
  
  }

  // @LINE:101
  class ReverseShippingController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:105
    def delete(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "shipping/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:101
    def readAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "shipping")
    }
  
    // @LINE:102
    def create(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "shipping")
    }
  
    // @LINE:103
    def read(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "shipping/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:104
    def update(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "shipping/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
  }

  // @LINE:28
  class ReverseVendorController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def readAll(): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "vendors")
    }
  
    // @LINE:33
    def unarchive(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "vendors/" + implicitly[PathBindable[Long]].unbind("id", id) + "/unarchive")
    }
  
    // @LINE:29
    def create(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "vendors")
    }
  
    // @LINE:30
    def read(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "vendors/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:31
    def update(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("PUT", _prefix + { _defaultPrefix } + "vendors/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
    // @LINE:32
    def archive(id:Long): Call = {
      import ReverseRouteContext.empty
      Call("DELETE", _prefix + { _defaultPrefix } + "vendors/" + implicitly[PathBindable[Long]].unbind("id", id))
    }
  
  }


}
