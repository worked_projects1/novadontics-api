
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v2.routes
// @DATE:Fri Jun 19 20:14:07 IST 2020

package controllers.v2;

import v2.RoutesPrefix;

public class routes {
  
  public static final controllers.v2.ReverseResourceCategoriesController ResourceCategoriesController = new controllers.v2.ReverseResourceCategoriesController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReversePrescriptionController PrescriptionController = new controllers.v2.ReversePrescriptionController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseCategoriesController CategoriesController = new controllers.v2.ReverseCategoriesController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseEducationRequestController EducationRequestController = new controllers.v2.ReverseEducationRequestController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseFormsController FormsController = new controllers.v2.ReverseFormsController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseEmailMappingController EmailMappingController = new controllers.v2.ReverseEmailMappingController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseProfileController ProfileController = new controllers.v2.ReverseProfileController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseFavouritesController FavouritesController = new controllers.v2.ReverseFavouritesController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseShippingController ShippingController = new controllers.v2.ReverseShippingController(RoutesPrefix.byNamePrefix());
  public static final controllers.v2.ReverseVendorController VendorController = new controllers.v2.ReverseVendorController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.v2.javascript.ReverseResourceCategoriesController ResourceCategoriesController = new controllers.v2.javascript.ReverseResourceCategoriesController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReversePrescriptionController PrescriptionController = new controllers.v2.javascript.ReversePrescriptionController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseCategoriesController CategoriesController = new controllers.v2.javascript.ReverseCategoriesController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseEducationRequestController EducationRequestController = new controllers.v2.javascript.ReverseEducationRequestController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseFormsController FormsController = new controllers.v2.javascript.ReverseFormsController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseEmailMappingController EmailMappingController = new controllers.v2.javascript.ReverseEmailMappingController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseProfileController ProfileController = new controllers.v2.javascript.ReverseProfileController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseFavouritesController FavouritesController = new controllers.v2.javascript.ReverseFavouritesController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseShippingController ShippingController = new controllers.v2.javascript.ReverseShippingController(RoutesPrefix.byNamePrefix());
    public static final controllers.v2.javascript.ReverseVendorController VendorController = new controllers.v2.javascript.ReverseVendorController(RoutesPrefix.byNamePrefix());
  }

}
