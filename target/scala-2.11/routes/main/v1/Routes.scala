
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v1.routes
// @DATE:Fri Jun 19 20:14:08 IST 2020

package v1

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HealthCheckController_12: javax.inject.Provider[controllers.v1.HealthCheckController],
  // @LINE:9
  SessionController_6: javax.inject.Provider[controllers.v1.SessionController],
  // @LINE:13
  PasswordController_11: javax.inject.Provider[controllers.v1.PasswordController],
  // @LINE:19
  CategoriesController_8: javax.inject.Provider[controllers.v1.CategoriesController],
  // @LINE:25
  OrdersController_4: javax.inject.Provider[controllers.v1.OrdersController],
  // @LINE:31
  TreatmentsController_3: javax.inject.Provider[controllers.v1.TreatmentsController],
  // @LINE:38
  PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
  // @LINE:43
  ConsultationsController_7: javax.inject.Provider[controllers.v1.ConsultationsController],
  // @LINE:49
  UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
  // @LINE:55
  StaffController_10: javax.inject.Provider[controllers.v1.StaffController],
  // @LINE:61
  PracticeController_13: javax.inject.Provider[controllers.v1.PracticeController],
  // @LINE:67
  CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
  // @LINE:73
  CoursesController_5: javax.inject.Provider[controllers.v1.CoursesController],
  // @LINE:79
  UploadsController_9: javax.inject.Provider[controllers.v1.UploadsController],
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HealthCheckController_12: javax.inject.Provider[controllers.v1.HealthCheckController],
    // @LINE:9
    SessionController_6: javax.inject.Provider[controllers.v1.SessionController],
    // @LINE:13
    PasswordController_11: javax.inject.Provider[controllers.v1.PasswordController],
    // @LINE:19
    CategoriesController_8: javax.inject.Provider[controllers.v1.CategoriesController],
    // @LINE:25
    OrdersController_4: javax.inject.Provider[controllers.v1.OrdersController],
    // @LINE:31
    TreatmentsController_3: javax.inject.Provider[controllers.v1.TreatmentsController],
    // @LINE:38
    PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
    // @LINE:43
    ConsultationsController_7: javax.inject.Provider[controllers.v1.ConsultationsController],
    // @LINE:49
    UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
    // @LINE:55
    StaffController_10: javax.inject.Provider[controllers.v1.StaffController],
    // @LINE:61
    PracticeController_13: javax.inject.Provider[controllers.v1.PracticeController],
    // @LINE:67
    CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
    // @LINE:73
    CoursesController_5: javax.inject.Provider[controllers.v1.CoursesController],
    // @LINE:79
    UploadsController_9: javax.inject.Provider[controllers.v1.UploadsController]
  ) = this(errorHandler, HealthCheckController_12, SessionController_6, PasswordController_11, CategoriesController_8, OrdersController_4, TreatmentsController_3, PatientsController_2, ConsultationsController_7, UsersController_0, StaffController_10, PracticeController_13, CoursesController_1, CoursesController_5, UploadsController_9, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    v1.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HealthCheckController_12, SessionController_6, PasswordController_11, CategoriesController_8, OrdersController_4, TreatmentsController_3, PatientsController_2, ConsultationsController_7, UsersController_0, StaffController_10, PracticeController_13, CoursesController_1, CoursesController_5, UploadsController_9, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """healthcheck""", """@controllers.v1.HealthCheckController@.check"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.check"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logIn"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logOut"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forgot/""" + "$" + """email<[^/]+>""", """@controllers.v1.PasswordController@.forgot(email:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.check(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.reset(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """password""", """@controllers.v1.PasswordController@.change"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """password""", """@controllers.v1.PasswordController@.change"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v1.CategoriesController@.readAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v1.CategoriesController@.bulkUpdate"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v1.CategoriesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v1.CategoriesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.TreatmentsController@.pdf(id:Long, step:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients""", """@controllers.v1.PatientsController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients/""" + "$" + """id<[^/]+>""", """@controllers.v1.PatientsController@.read(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forms""", """@controllers.v1.TreatmentsController@.readForms"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.readAll(practice:Option[Long])"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents""", """@controllers.v3.CoursesController@.readAll(resource:String = "resource")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.read(resource:String = "resource", id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents""", """@controllers.v3.CoursesController@.create(resource:String = "resource")"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.update(resource:String = "resource", id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """documents/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.delete(resource:String = "resource", id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos""", """@controllers.v1.CoursesController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.read(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos""", """@controllers.v1.CoursesController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """videos/""" + "$" + """id<[^/]+>""", """@controllers.v1.CoursesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signature/""" + "$" + """name<[^/]+>""", """@controllers.v1.UploadsController@.signature(name:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_v1_HealthCheckController_check0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("healthcheck")))
  )
  private[this] lazy val controllers_v1_HealthCheckController_check0_invoker = createInvoker(
    HealthCheckController_12.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.HealthCheckController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """healthcheck"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_v1_SessionController_check1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_check1_invoker = createInvoker(
    SessionController_6.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.SessionController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_v1_SessionController_logIn2_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logIn2_invoker = createInvoker(
    SessionController_6.get.logIn,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.SessionController",
      "logIn",
      Nil,
      "PUT",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_v1_SessionController_logOut3_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logOut3_invoker = createInvoker(
    SessionController_6.get.logOut,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.SessionController",
      "logOut",
      Nil,
      "DELETE",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_v1_PasswordController_forgot4_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forgot/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_forgot4_invoker = createInvoker(
    PasswordController_11.get.forgot(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PasswordController",
      "forgot",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """forgot/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_v1_PasswordController_check5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_check5_invoker = createInvoker(
    PasswordController_11.get.check(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PasswordController",
      "check",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:15
  private[this] lazy val controllers_v1_PasswordController_reset6_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_reset6_invoker = createInvoker(
    PasswordController_11.get.reset(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PasswordController",
      "reset",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_v1_PasswordController_change7_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("password")))
  )
  private[this] lazy val controllers_v1_PasswordController_change7_invoker = createInvoker(
    PasswordController_11.get.change,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PasswordController",
      "change",
      Nil,
      "PUT",
      """""",
      this.prefix + """password"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_v1_PasswordController_change8_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("password")))
  )
  private[this] lazy val controllers_v1_PasswordController_change8_invoker = createInvoker(
    PasswordController_11.get.change,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PasswordController",
      "change",
      Nil,
      "PUT",
      """""",
      this.prefix + """password"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_v1_CategoriesController_readAll9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v1_CategoriesController_readAll9_invoker = createInvoker(
    CategoriesController_8.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CategoriesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_v1_CategoriesController_bulkUpdate10_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v1_CategoriesController_bulkUpdate10_invoker = createInvoker(
    CategoriesController_8.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CategoriesController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_v1_CategoriesController_update11_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CategoriesController_update11_invoker = createInvoker(
    CategoriesController_8.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CategoriesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_v1_CategoriesController_delete12_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CategoriesController_delete12_invoker = createInvoker(
    CategoriesController_8.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CategoriesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_v1_OrdersController_readAll13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_readAll13_invoker = createInvoker(
    OrdersController_4.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.OrdersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_v1_OrdersController_create14_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_create14_invoker = createInvoker(
    OrdersController_4.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.OrdersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_v1_OrdersController_read15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_read15_invoker = createInvoker(
    OrdersController_4.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.OrdersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_v1_OrdersController_update16_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_update16_invoker = createInvoker(
    OrdersController_4.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.OrdersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_v1_OrdersController_delete17_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_delete17_invoker = createInvoker(
    OrdersController_4.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.OrdersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_v1_TreatmentsController_readAll18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_readAll18_invoker = createInvoker(
    TreatmentsController_3.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_v1_TreatmentsController_create19_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_create19_invoker = createInvoker(
    TreatmentsController_3.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_v1_TreatmentsController_read20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_read20_invoker = createInvoker(
    TreatmentsController_3.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_v1_TreatmentsController_update21_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_update21_invoker = createInvoker(
    TreatmentsController_3.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_v1_TreatmentsController_delete22_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_delete22_invoker = createInvoker(
    TreatmentsController_3.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_v1_TreatmentsController_pdf23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_pdf23_invoker = createInvoker(
    TreatmentsController_3.get.pdf(fakeValue[Long], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "pdf",
      Seq(classOf[Long], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_v1_PatientsController_readAll24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients")))
  )
  private[this] lazy val controllers_v1_PatientsController_readAll24_invoker = createInvoker(
    PatientsController_2.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PatientsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """patients"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_v1_PatientsController_read25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PatientsController_read25_invoker = createInvoker(
    PatientsController_2.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PatientsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """patients/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_v1_TreatmentsController_readForms26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forms")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_readForms26_invoker = createInvoker(
    TreatmentsController_3.get.readForms,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.TreatmentsController",
      "readForms",
      Nil,
      "GET",
      """""",
      this.prefix + """forms"""
    )
  )

  // @LINE:43
  private[this] lazy val controllers_v1_ConsultationsController_readAll27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_readAll27_invoker = createInvoker(
    ConsultationsController_7.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.ConsultationsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:44
  private[this] lazy val controllers_v1_ConsultationsController_create28_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_create28_invoker = createInvoker(
    ConsultationsController_7.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.ConsultationsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_v1_ConsultationsController_read29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_read29_invoker = createInvoker(
    ConsultationsController_7.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.ConsultationsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_v1_ConsultationsController_update30_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_update30_invoker = createInvoker(
    ConsultationsController_7.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.ConsultationsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:47
  private[this] lazy val controllers_v1_ConsultationsController_delete31_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_delete31_invoker = createInvoker(
    ConsultationsController_7.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.ConsultationsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:49
  private[this] lazy val controllers_v1_UsersController_readAll32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_readAll32_invoker = createInvoker(
    UsersController_0.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UsersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:50
  private[this] lazy val controllers_v1_UsersController_create33_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_create33_invoker = createInvoker(
    UsersController_0.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UsersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:51
  private[this] lazy val controllers_v1_UsersController_read34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_read34_invoker = createInvoker(
    UsersController_0.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UsersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_v1_UsersController_update35_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_update35_invoker = createInvoker(
    UsersController_0.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UsersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:53
  private[this] lazy val controllers_v1_UsersController_delete36_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_delete36_invoker = createInvoker(
    UsersController_0.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UsersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:55
  private[this] lazy val controllers_v1_StaffController_readAll37_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_readAll37_invoker = createInvoker(
    StaffController_10.get.readAll(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.StaffController",
      "readAll",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:56
  private[this] lazy val controllers_v1_StaffController_create38_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_create38_invoker = createInvoker(
    StaffController_10.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.StaffController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_v1_StaffController_read39_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_read39_invoker = createInvoker(
    StaffController_10.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.StaffController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_v1_StaffController_update40_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_update40_invoker = createInvoker(
    StaffController_10.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.StaffController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:59
  private[this] lazy val controllers_v1_StaffController_delete41_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_delete41_invoker = createInvoker(
    StaffController_10.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.StaffController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_v1_PracticeController_readAll42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_readAll42_invoker = createInvoker(
    PracticeController_13.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PracticeController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:62
  private[this] lazy val controllers_v1_PracticeController_create43_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_create43_invoker = createInvoker(
    PracticeController_13.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PracticeController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:63
  private[this] lazy val controllers_v1_PracticeController_read44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_read44_invoker = createInvoker(
    PracticeController_13.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PracticeController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:64
  private[this] lazy val controllers_v1_PracticeController_update45_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_update45_invoker = createInvoker(
    PracticeController_13.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PracticeController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:65
  private[this] lazy val controllers_v1_PracticeController_delete46_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_delete46_invoker = createInvoker(
    PracticeController_13.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.PracticeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:67
  private[this] lazy val controllers_v3_CoursesController_readAll47_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents")))
  )
  private[this] lazy val controllers_v3_CoursesController_readAll47_invoker = createInvoker(
    CoursesController_1.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v3.CoursesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """documents"""
    )
  )

  // @LINE:68
  private[this] lazy val controllers_v3_CoursesController_read48_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_read48_invoker = createInvoker(
    CoursesController_1.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v3.CoursesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:69
  private[this] lazy val controllers_v3_CoursesController_create49_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents")))
  )
  private[this] lazy val controllers_v3_CoursesController_create49_invoker = createInvoker(
    CoursesController_1.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v3.CoursesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """documents"""
    )
  )

  // @LINE:70
  private[this] lazy val controllers_v3_CoursesController_update50_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_update50_invoker = createInvoker(
    CoursesController_1.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v3.CoursesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:71
  private[this] lazy val controllers_v3_CoursesController_delete51_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("documents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_delete51_invoker = createInvoker(
    CoursesController_1.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v3.CoursesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """documents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:73
  private[this] lazy val controllers_v1_CoursesController_readAll52_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos")))
  )
  private[this] lazy val controllers_v1_CoursesController_readAll52_invoker = createInvoker(
    CoursesController_5.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CoursesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """videos"""
    )
  )

  // @LINE:74
  private[this] lazy val controllers_v1_CoursesController_read53_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_read53_invoker = createInvoker(
    CoursesController_5.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CoursesController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:75
  private[this] lazy val controllers_v1_CoursesController_create54_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos")))
  )
  private[this] lazy val controllers_v1_CoursesController_create54_invoker = createInvoker(
    CoursesController_5.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CoursesController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """videos"""
    )
  )

  // @LINE:76
  private[this] lazy val controllers_v1_CoursesController_update55_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_update55_invoker = createInvoker(
    CoursesController_5.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CoursesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:77
  private[this] lazy val controllers_v1_CoursesController_delete56_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("videos/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_CoursesController_delete56_invoker = createInvoker(
    CoursesController_5.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.CoursesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """videos/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_v1_UploadsController_signature57_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signature/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UploadsController_signature57_invoker = createInvoker(
    UploadsController_9.get.signature(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v1",
      "controllers.v1.UploadsController",
      "signature",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """signature/""" + "$" + """name<[^/]+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_v1_HealthCheckController_check0_route(params) =>
      call { 
        controllers_v1_HealthCheckController_check0_invoker.call(HealthCheckController_12.get.check)
      }
  
    // @LINE:9
    case controllers_v1_SessionController_check1_route(params) =>
      call { 
        controllers_v1_SessionController_check1_invoker.call(SessionController_6.get.check)
      }
  
    // @LINE:10
    case controllers_v1_SessionController_logIn2_route(params) =>
      call { 
        controllers_v1_SessionController_logIn2_invoker.call(SessionController_6.get.logIn)
      }
  
    // @LINE:11
    case controllers_v1_SessionController_logOut3_route(params) =>
      call { 
        controllers_v1_SessionController_logOut3_invoker.call(SessionController_6.get.logOut)
      }
  
    // @LINE:13
    case controllers_v1_PasswordController_forgot4_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_PasswordController_forgot4_invoker.call(PasswordController_11.get.forgot(email))
      }
  
    // @LINE:14
    case controllers_v1_PasswordController_check5_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_check5_invoker.call(PasswordController_11.get.check(secret))
      }
  
    // @LINE:15
    case controllers_v1_PasswordController_reset6_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_reset6_invoker.call(PasswordController_11.get.reset(secret))
      }
  
    // @LINE:16
    case controllers_v1_PasswordController_change7_route(params) =>
      call { 
        controllers_v1_PasswordController_change7_invoker.call(PasswordController_11.get.change)
      }
  
    // @LINE:17
    case controllers_v1_PasswordController_change8_route(params) =>
      call { 
        controllers_v1_PasswordController_change8_invoker.call(PasswordController_11.get.change)
      }
  
    // @LINE:19
    case controllers_v1_CategoriesController_readAll9_route(params) =>
      call { 
        controllers_v1_CategoriesController_readAll9_invoker.call(CategoriesController_8.get.readAll)
      }
  
    // @LINE:20
    case controllers_v1_CategoriesController_bulkUpdate10_route(params) =>
      call { 
        controllers_v1_CategoriesController_bulkUpdate10_invoker.call(CategoriesController_8.get.bulkUpdate)
      }
  
    // @LINE:21
    case controllers_v1_CategoriesController_update11_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CategoriesController_update11_invoker.call(CategoriesController_8.get.update(id))
      }
  
    // @LINE:22
    case controllers_v1_CategoriesController_delete12_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CategoriesController_delete12_invoker.call(CategoriesController_8.get.delete(id))
      }
  
    // @LINE:25
    case controllers_v1_OrdersController_readAll13_route(params) =>
      call { 
        controllers_v1_OrdersController_readAll13_invoker.call(OrdersController_4.get.readAll)
      }
  
    // @LINE:26
    case controllers_v1_OrdersController_create14_route(params) =>
      call { 
        controllers_v1_OrdersController_create14_invoker.call(OrdersController_4.get.create)
      }
  
    // @LINE:27
    case controllers_v1_OrdersController_read15_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_read15_invoker.call(OrdersController_4.get.read(id))
      }
  
    // @LINE:28
    case controllers_v1_OrdersController_update16_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_update16_invoker.call(OrdersController_4.get.update(id))
      }
  
    // @LINE:29
    case controllers_v1_OrdersController_delete17_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_delete17_invoker.call(OrdersController_4.get.delete(id))
      }
  
    // @LINE:31
    case controllers_v1_TreatmentsController_readAll18_route(params) =>
      call { 
        controllers_v1_TreatmentsController_readAll18_invoker.call(TreatmentsController_3.get.readAll)
      }
  
    // @LINE:32
    case controllers_v1_TreatmentsController_create19_route(params) =>
      call { 
        controllers_v1_TreatmentsController_create19_invoker.call(TreatmentsController_3.get.create)
      }
  
    // @LINE:33
    case controllers_v1_TreatmentsController_read20_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_read20_invoker.call(TreatmentsController_3.get.read(id))
      }
  
    // @LINE:34
    case controllers_v1_TreatmentsController_update21_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_update21_invoker.call(TreatmentsController_3.get.update(id))
      }
  
    // @LINE:35
    case controllers_v1_TreatmentsController_delete22_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_delete22_invoker.call(TreatmentsController_3.get.delete(id))
      }
  
    // @LINE:36
    case controllers_v1_TreatmentsController_pdf23_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Option[String]]("step", None)) { (id, step) =>
        controllers_v1_TreatmentsController_pdf23_invoker.call(TreatmentsController_3.get.pdf(id, step))
      }
  
    // @LINE:38
    case controllers_v1_PatientsController_readAll24_route(params) =>
      call { 
        controllers_v1_PatientsController_readAll24_invoker.call(PatientsController_2.get.readAll)
      }
  
    // @LINE:39
    case controllers_v1_PatientsController_read25_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_read25_invoker.call(PatientsController_2.get.read(id))
      }
  
    // @LINE:41
    case controllers_v1_TreatmentsController_readForms26_route(params) =>
      call { 
        controllers_v1_TreatmentsController_readForms26_invoker.call(TreatmentsController_3.get.readForms)
      }
  
    // @LINE:43
    case controllers_v1_ConsultationsController_readAll27_route(params) =>
      call { 
        controllers_v1_ConsultationsController_readAll27_invoker.call(ConsultationsController_7.get.readAll)
      }
  
    // @LINE:44
    case controllers_v1_ConsultationsController_create28_route(params) =>
      call { 
        controllers_v1_ConsultationsController_create28_invoker.call(ConsultationsController_7.get.create)
      }
  
    // @LINE:45
    case controllers_v1_ConsultationsController_read29_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_read29_invoker.call(ConsultationsController_7.get.read(id))
      }
  
    // @LINE:46
    case controllers_v1_ConsultationsController_update30_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_update30_invoker.call(ConsultationsController_7.get.update(id))
      }
  
    // @LINE:47
    case controllers_v1_ConsultationsController_delete31_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_delete31_invoker.call(ConsultationsController_7.get.delete(id))
      }
  
    // @LINE:49
    case controllers_v1_UsersController_readAll32_route(params) =>
      call { 
        controllers_v1_UsersController_readAll32_invoker.call(UsersController_0.get.readAll)
      }
  
    // @LINE:50
    case controllers_v1_UsersController_create33_route(params) =>
      call { 
        controllers_v1_UsersController_create33_invoker.call(UsersController_0.get.create)
      }
  
    // @LINE:51
    case controllers_v1_UsersController_read34_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_read34_invoker.call(UsersController_0.get.read(id))
      }
  
    // @LINE:52
    case controllers_v1_UsersController_update35_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_update35_invoker.call(UsersController_0.get.update(id))
      }
  
    // @LINE:53
    case controllers_v1_UsersController_delete36_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_delete36_invoker.call(UsersController_0.get.delete(id))
      }
  
    // @LINE:55
    case controllers_v1_StaffController_readAll37_route(params) =>
      call(params.fromQuery[Option[Long]]("practice", None)) { (practice) =>
        controllers_v1_StaffController_readAll37_invoker.call(StaffController_10.get.readAll(practice))
      }
  
    // @LINE:56
    case controllers_v1_StaffController_create38_route(params) =>
      call { 
        controllers_v1_StaffController_create38_invoker.call(StaffController_10.get.create)
      }
  
    // @LINE:57
    case controllers_v1_StaffController_read39_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_read39_invoker.call(StaffController_10.get.read(id))
      }
  
    // @LINE:58
    case controllers_v1_StaffController_update40_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_update40_invoker.call(StaffController_10.get.update(id))
      }
  
    // @LINE:59
    case controllers_v1_StaffController_delete41_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_delete41_invoker.call(StaffController_10.get.delete(id))
      }
  
    // @LINE:61
    case controllers_v1_PracticeController_readAll42_route(params) =>
      call { 
        controllers_v1_PracticeController_readAll42_invoker.call(PracticeController_13.get.readAll)
      }
  
    // @LINE:62
    case controllers_v1_PracticeController_create43_route(params) =>
      call { 
        controllers_v1_PracticeController_create43_invoker.call(PracticeController_13.get.create)
      }
  
    // @LINE:63
    case controllers_v1_PracticeController_read44_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_read44_invoker.call(PracticeController_13.get.read(id))
      }
  
    // @LINE:64
    case controllers_v1_PracticeController_update45_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_update45_invoker.call(PracticeController_13.get.update(id))
      }
  
    // @LINE:65
    case controllers_v1_PracticeController_delete46_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_delete46_invoker.call(PracticeController_13.get.delete(id))
      }
  
    // @LINE:67
    case controllers_v3_CoursesController_readAll47_route(params) =>
      call(Param[String]("resource", Right("resource"))) { (resource) =>
        controllers_v3_CoursesController_readAll47_invoker.call(CoursesController_1.get.readAll(resource))
      }
  
    // @LINE:68
    case controllers_v3_CoursesController_read48_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_read48_invoker.call(CoursesController_1.get.read(resource, id))
      }
  
    // @LINE:69
    case controllers_v3_CoursesController_create49_route(params) =>
      call(Param[String]("resource", Right("resource"))) { (resource) =>
        controllers_v3_CoursesController_create49_invoker.call(CoursesController_1.get.create(resource))
      }
  
    // @LINE:70
    case controllers_v3_CoursesController_update50_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_update50_invoker.call(CoursesController_1.get.update(resource, id))
      }
  
    // @LINE:71
    case controllers_v3_CoursesController_delete51_route(params) =>
      call(Param[String]("resource", Right("resource")), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_delete51_invoker.call(CoursesController_1.get.delete(resource, id))
      }
  
    // @LINE:73
    case controllers_v1_CoursesController_readAll52_route(params) =>
      call { 
        controllers_v1_CoursesController_readAll52_invoker.call(CoursesController_5.get.readAll)
      }
  
    // @LINE:74
    case controllers_v1_CoursesController_read53_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_read53_invoker.call(CoursesController_5.get.read(id))
      }
  
    // @LINE:75
    case controllers_v1_CoursesController_create54_route(params) =>
      call { 
        controllers_v1_CoursesController_create54_invoker.call(CoursesController_5.get.create)
      }
  
    // @LINE:76
    case controllers_v1_CoursesController_update55_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_update55_invoker.call(CoursesController_5.get.update(id))
      }
  
    // @LINE:77
    case controllers_v1_CoursesController_delete56_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_CoursesController_delete56_invoker.call(CoursesController_5.get.delete(id))
      }
  
    // @LINE:79
    case controllers_v1_UploadsController_signature57_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_v1_UploadsController_signature57_invoker.call(UploadsController_9.get.signature(name))
      }
  }
}
