
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v4.routes
// @DATE:Fri Jun 19 20:14:06 IST 2020

package v4

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HealthCheckController_35: javax.inject.Provider[controllers.v1.HealthCheckController],
  // @LINE:9
  SessionController_12: javax.inject.Provider[controllers.v1.SessionController],
  // @LINE:16
  PasswordController_33: javax.inject.Provider[controllers.v1.PasswordController],
  // @LINE:21
  AnalyticsController_11: javax.inject.Provider[controllers.v3.AnalyticsController],
  // @LINE:23
  CategoriesController_7: javax.inject.Provider[controllers.v2.CategoriesController],
  // @LINE:28
  ProductsController_14: javax.inject.Provider[controllers.v3.ProductsController],
  // @LINE:39
  FavouritesController_13: javax.inject.Provider[controllers.v2.FavouritesController],
  // @LINE:45
  VendorController_10: javax.inject.Provider[controllers.v2.VendorController],
  // @LINE:66
  OrdersController_8: javax.inject.Provider[controllers.v1.OrdersController],
  // @LINE:76
  TreatmentsController_6: javax.inject.Provider[controllers.v1.TreatmentsController],
  // @LINE:127
  PatientsController_4: javax.inject.Provider[controllers.v1.PatientsController],
  // @LINE:133
  ConsultationsController_15: javax.inject.Provider[controllers.v1.ConsultationsController],
  // @LINE:139
  UsersController_1: javax.inject.Provider[controllers.v1.UsersController],
  // @LINE:145
  StaffController_29: javax.inject.Provider[controllers.v1.StaffController],
  // @LINE:164
  ProfileController_21: javax.inject.Provider[controllers.v2.ProfileController],
  // @LINE:167
  PracticeController_36: javax.inject.Provider[controllers.v1.PracticeController],
  // @LINE:173
  CoursesController_2: javax.inject.Provider[controllers.v3.CoursesController],
  // @LINE:179
  UploadsController_17: javax.inject.Provider[controllers.v1.UploadsController],
  // @LINE:181
  EmailMappingController_37: javax.inject.Provider[controllers.v2.EmailMappingController],
  // @LINE:189
  ShippingController_19: javax.inject.Provider[controllers.v2.ShippingController],
  // @LINE:195
  FormsController_5: javax.inject.Provider[controllers.v2.FormsController],
  // @LINE:200
  PrescriptionController_23: javax.inject.Provider[controllers.v2.PrescriptionController],
  // @LINE:206
  PrescriptionHistoryController_0: javax.inject.Provider[controllers.v2.PrescriptionHistoryController],
  // @LINE:216
  ResourceCategoriesController_9: javax.inject.Provider[controllers.v3.ResourceCategoriesController],
  // @LINE:224
  EducationRequestController_20: javax.inject.Provider[controllers.v2.EducationRequestController],
  // @LINE:230
  CartController_16: javax.inject.Provider[controllers.v3.CartController],
  // @LINE:236
  ResourceSubCategoriesController_32: javax.inject.Provider[controllers.v3.ResourceSubCategoriesController],
  // @LINE:242
  ClinicalNotesController_30: javax.inject.Provider[controllers.v3.ClinicalNotesController],
  // @LINE:250
  OperatoriesController_18: javax.inject.Provider[controllers.v3.OperatoriesController],
  // @LINE:255
  ProvidersController_24: javax.inject.Provider[controllers.v3.ProvidersController],
  // @LINE:260
  ProcedureController_22: javax.inject.Provider[controllers.v3.ProcedureController],
  // @LINE:270
  NonWorkingDaysController_34: javax.inject.Provider[controllers.v3.NonWorkingDaysController],
  // @LINE:275
  WorkingHoursController_28: javax.inject.Provider[controllers.v3.WorkingHoursController],
  // @LINE:279
  AppointmentController_25: javax.inject.Provider[controllers.v3.AppointmentController],
  // @LINE:305
  TreatmentPlanController_3: javax.inject.Provider[controllers.v3.TreatmentPlanController],
  // @LINE:310
  TreatmentPlanDetailsController_26: javax.inject.Provider[controllers.v3.TreatmentPlanDetailsController],
  // @LINE:316
  StaffMembersController_31: javax.inject.Provider[controllers.v3.StaffMembersController],
  // @LINE:321
  SharedPatientController_27: javax.inject.Provider[controllers.v3.SharedPatientController],
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HealthCheckController_35: javax.inject.Provider[controllers.v1.HealthCheckController],
    // @LINE:9
    SessionController_12: javax.inject.Provider[controllers.v1.SessionController],
    // @LINE:16
    PasswordController_33: javax.inject.Provider[controllers.v1.PasswordController],
    // @LINE:21
    AnalyticsController_11: javax.inject.Provider[controllers.v3.AnalyticsController],
    // @LINE:23
    CategoriesController_7: javax.inject.Provider[controllers.v2.CategoriesController],
    // @LINE:28
    ProductsController_14: javax.inject.Provider[controllers.v3.ProductsController],
    // @LINE:39
    FavouritesController_13: javax.inject.Provider[controllers.v2.FavouritesController],
    // @LINE:45
    VendorController_10: javax.inject.Provider[controllers.v2.VendorController],
    // @LINE:66
    OrdersController_8: javax.inject.Provider[controllers.v1.OrdersController],
    // @LINE:76
    TreatmentsController_6: javax.inject.Provider[controllers.v1.TreatmentsController],
    // @LINE:127
    PatientsController_4: javax.inject.Provider[controllers.v1.PatientsController],
    // @LINE:133
    ConsultationsController_15: javax.inject.Provider[controllers.v1.ConsultationsController],
    // @LINE:139
    UsersController_1: javax.inject.Provider[controllers.v1.UsersController],
    // @LINE:145
    StaffController_29: javax.inject.Provider[controllers.v1.StaffController],
    // @LINE:164
    ProfileController_21: javax.inject.Provider[controllers.v2.ProfileController],
    // @LINE:167
    PracticeController_36: javax.inject.Provider[controllers.v1.PracticeController],
    // @LINE:173
    CoursesController_2: javax.inject.Provider[controllers.v3.CoursesController],
    // @LINE:179
    UploadsController_17: javax.inject.Provider[controllers.v1.UploadsController],
    // @LINE:181
    EmailMappingController_37: javax.inject.Provider[controllers.v2.EmailMappingController],
    // @LINE:189
    ShippingController_19: javax.inject.Provider[controllers.v2.ShippingController],
    // @LINE:195
    FormsController_5: javax.inject.Provider[controllers.v2.FormsController],
    // @LINE:200
    PrescriptionController_23: javax.inject.Provider[controllers.v2.PrescriptionController],
    // @LINE:206
    PrescriptionHistoryController_0: javax.inject.Provider[controllers.v2.PrescriptionHistoryController],
    // @LINE:216
    ResourceCategoriesController_9: javax.inject.Provider[controllers.v3.ResourceCategoriesController],
    // @LINE:224
    EducationRequestController_20: javax.inject.Provider[controllers.v2.EducationRequestController],
    // @LINE:230
    CartController_16: javax.inject.Provider[controllers.v3.CartController],
    // @LINE:236
    ResourceSubCategoriesController_32: javax.inject.Provider[controllers.v3.ResourceSubCategoriesController],
    // @LINE:242
    ClinicalNotesController_30: javax.inject.Provider[controllers.v3.ClinicalNotesController],
    // @LINE:250
    OperatoriesController_18: javax.inject.Provider[controllers.v3.OperatoriesController],
    // @LINE:255
    ProvidersController_24: javax.inject.Provider[controllers.v3.ProvidersController],
    // @LINE:260
    ProcedureController_22: javax.inject.Provider[controllers.v3.ProcedureController],
    // @LINE:270
    NonWorkingDaysController_34: javax.inject.Provider[controllers.v3.NonWorkingDaysController],
    // @LINE:275
    WorkingHoursController_28: javax.inject.Provider[controllers.v3.WorkingHoursController],
    // @LINE:279
    AppointmentController_25: javax.inject.Provider[controllers.v3.AppointmentController],
    // @LINE:305
    TreatmentPlanController_3: javax.inject.Provider[controllers.v3.TreatmentPlanController],
    // @LINE:310
    TreatmentPlanDetailsController_26: javax.inject.Provider[controllers.v3.TreatmentPlanDetailsController],
    // @LINE:316
    StaffMembersController_31: javax.inject.Provider[controllers.v3.StaffMembersController],
    // @LINE:321
    SharedPatientController_27: javax.inject.Provider[controllers.v3.SharedPatientController]
  ) = this(errorHandler, HealthCheckController_35, SessionController_12, PasswordController_33, AnalyticsController_11, CategoriesController_7, ProductsController_14, FavouritesController_13, VendorController_10, OrdersController_8, TreatmentsController_6, PatientsController_4, ConsultationsController_15, UsersController_1, StaffController_29, ProfileController_21, PracticeController_36, CoursesController_2, UploadsController_17, EmailMappingController_37, ShippingController_19, FormsController_5, PrescriptionController_23, PrescriptionHistoryController_0, ResourceCategoriesController_9, EducationRequestController_20, CartController_16, ResourceSubCategoriesController_32, ClinicalNotesController_30, OperatoriesController_18, ProvidersController_24, ProcedureController_22, NonWorkingDaysController_34, WorkingHoursController_28, AppointmentController_25, TreatmentPlanController_3, TreatmentPlanDetailsController_26, StaffMembersController_31, SharedPatientController_27, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    v4.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HealthCheckController_35, SessionController_12, PasswordController_33, AnalyticsController_11, CategoriesController_7, ProductsController_14, FavouritesController_13, VendorController_10, OrdersController_8, TreatmentsController_6, PatientsController_4, ConsultationsController_15, UsersController_1, StaffController_29, ProfileController_21, PracticeController_36, CoursesController_2, UploadsController_17, EmailMappingController_37, ShippingController_19, FormsController_5, PrescriptionController_23, PrescriptionHistoryController_0, ResourceCategoriesController_9, EducationRequestController_20, CartController_16, ResourceSubCategoriesController_32, ClinicalNotesController_30, OperatoriesController_18, ProvidersController_24, ProcedureController_22, NonWorkingDaysController_34, WorkingHoursController_28, AppointmentController_25, TreatmentPlanController_3, TreatmentPlanDetailsController_26, StaffMembersController_31, SharedPatientController_27, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """healthcheck""", """@controllers.v1.HealthCheckController@.check"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.check"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logIn"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logOut"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session/signUpRequest""", """@controllers.v1.SessionController@.signUpRequest"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getAuthorizeNetDetails""", """@controllers.v1.SessionController@.getAuthorizeNetDetails"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forgot/""" + "$" + """email<[^/]+>""", """@controllers.v1.PasswordController@.forgot(email:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.check(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.reset(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """password""", """@controllers.v1.PasswordController@.change"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """analytics""", """@controllers.v3.AnalyticsController@.read"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.readAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.bulkUpdate"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/generateCSV""", """@controllers.v3.ProductsController@.generateCSV"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.update(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.bulkUpdate"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/generatePdf/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v3.ProductsController@.generatePdf(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """findProducts""", """@controllers.v3.ProductsController@.findProducts"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getCategories""", """@controllers.v3.ProductsController@.getCategories"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.add(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.remove(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """webService/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.webService(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.update(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stateTax""", """@controllers.v2.VendorController@.stateTaxReadAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stateTax/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.stateTaxUpdate(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stateTax/findSaleTax/""" + "$" + """shippingId<[^/]+>""", """@controllers.v2.VendorController@.findSaleTax(shippingId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """saleTax/""" + "$" + """vendorId<[^/]+>""", """@controllers.v2.VendorController@.saleTaxReadAll(vendorId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """saleTax""", """@controllers.v2.VendorController@.saleTaxCreate"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """saleTax/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.saleTaxUpdate(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """state""", """@controllers.v2.VendorController@.stateReadAll"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.archive(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>/unarchive""", """@controllers.v2.VendorController@.unarchive(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/increasePrice/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.increasePrice(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/priceChangeHistory/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.getPriceChangeHisotry(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/deleteAllProducts/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.deleteAllProducts(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/generateCSV""", """@controllers.v1.OrdersController@.generateCSV(status:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """catalogOrders""", """@controllers.v1.OrdersController@.catalogOrders"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """updateGrandTotal""", """@controllers.v1.OrdersController@.updateGrandTotal"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/updateVendorPayment/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.updateVendorPayment(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """referralSourcePDF""", """@controllers.v1.TreatmentsController@.generateReferralSourcePdf(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """referralSourceCSV""", """@controllers.v1.TreatmentsController@.generateReferralSourceCsv(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """referralSource""", """@controllers.v1.TreatmentsController@.referralSourceList(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patientList""", """@controllers.v1.TreatmentsController@.patientList"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """merge1b1c""", """@controllers.v1.TreatmentsController@.mergeAll1B1C"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """merge3b3c""", """@controllers.v1.TreatmentsController@.mergeAll3B3C"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """mergeForms""", """@controllers.v1.TreatmentsController@.mergeForms(sourceForm:String, targetForm:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """migrateTreatment""", """@controllers.v1.TreatmentsController@.migrateTreatment"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """migrateMedicalCondtionSelected""", """@controllers.v1.TreatmentsController@.migrateMedicalCondtionSelected"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentAdditionalData/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.getTreatmentAdditionalData(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """newPatient""", """@controllers.v1.TreatmentsController@.newPatientsList(fromDate:Option[String], toDate:Option[String])"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.TreatmentsController@.pdf(id:Long, step:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/testpdf/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.TreatmentsController@.testpdf(id:Long, step:Option[String])"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateCtScan/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateCtScan(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateContracts/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateContracts(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/recallPeriod/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.recallPeriod(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateScratchPad/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateScratchPad(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """measurementHistory/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.getMeasurementHisotry(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateToothChartData/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateToothChartData(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/getToothChartData/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.getToothChartData(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/getPerioChartData/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.getPerioChartData(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updatePerioChartData/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updatePerioChartData(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateXray/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateXray(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateToothChartDate/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateToothChartDate(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """migrateAllMedicalCondtionLog""", """@controllers.v1.TreatmentsController@.migrateAllMedicalCondtionLog"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """updatePerioChartDataFormat""", """@controllers.v1.TreatmentsController@.updatePerioChartDataFormat"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """newPatientPDF""", """@controllers.v1.TreatmentsController@.generateNewPatientPDF(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """newPatientCSV""", """@controllers.v1.TreatmentsController@.generateNewPatientCSV(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientTypePDF""", """@controllers.v1.TreatmentsController@.patientTypePdf(patientType:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientTypeCSV""", """@controllers.v1.TreatmentsController@.patientTypeCSV(patientType:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientBalancePDF""", """@controllers.v1.TreatmentsController@.generatePatientsOutstandingBalancePdf"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientBalanceCSV""", """@controllers.v1.TreatmentsController@.generatePatientsOutstandingBalanceCsv"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientBalance""", """@controllers.v1.TreatmentsController@.patientsOutstandingBalanceList"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """outstandingInsurancePDF""", """@controllers.v1.TreatmentsController@.outstandingInsurancePDF"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """outstandingInsuranceCSV""", """@controllers.v1.TreatmentsController@.outstandingInsuranceCSV"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """outstandingInsurance""", """@controllers.v1.TreatmentsController@.outstandingInsurance"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """totalCollectedAmountPDF""", """@controllers.v1.TreatmentsController@.totalCollectedAmountPDF(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """totalCollectedAmountCSV""", """@controllers.v1.TreatmentsController@.totalCollectedAmountCSV(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """totalCollectedAmount""", """@controllers.v1.TreatmentsController@.totalCollectedAmount(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientStatementPDF""", """@controllers.v1.TreatmentsController@.generatePatientsStatementPdf(treatmentPlanId:Option[Long])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patientEstimatePDF""", """@controllers.v1.TreatmentsController@.patientEstimatePDF(treatmentPlanId:Option[Long])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """PatientType""", """@controllers.v1.TreatmentsController@.patientTypeList(patientType:Option[String])"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shareToAdmin/""" + "$" + """patientId<[^/]+>""", """@controllers.v1.TreatmentsController@.shareToAdmin(patientId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients""", """@controllers.v1.PatientsController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients/""" + "$" + """id<[^/]+>""", """@controllers.v1.PatientsController@.read(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generateHealthHistory/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.PatientsController@.generateHealthHistoryForm(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generateConsentForm/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.PatientsController@.generateConsentForm(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.readAll(practice:Option[Long])"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.update(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/updateAuthorizeCustId""", """@controllers.v1.StaffController@.updateAuthorizeCustId"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/udid""", """@controllers.v1.StaffController@.setUDID"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/udid1""", """@controllers.v1.StaffController@.setUDID1"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>/extend""", """@controllers.v1.StaffController@.extendExpirationDate(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>/extend1""", """@controllers.v1.StaffController@.extendExpirationDate1(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/clearUDID/""" + "$" + """email<[^/]+>""", """@controllers.v1.StaffController@.clearUDID(email:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """roleToSquareAccessMapping""", """@controllers.v1.StaffController@.roleToSquareAccessMapping"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """renewalDetails/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.readAllRenewalDetails(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """renewalDetails/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.updateRenewalDetails(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """staffImport""", """@controllers.v1.StaffController@.bulkUpdate"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.read"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>""", """@controllers.v3.CoursesController@.readAll(resource:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.read(resource:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>""", """@controllers.v3.CoursesController@.create(resource:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.update(resource:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.delete(resource:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signature/""" + "$" + """name<[^/]+>""", """@controllers.v1.UploadsController@.signature(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/add""", """@controllers.v2.EmailMappingController@.addUser(id:Long, userId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/rem""", """@controllers.v2.EmailMappingController@.removeUser(id:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forms""", """@controllers.v2.FormsController@.readForms"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.readAllConsents"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents/""" + "$" + """id<[^/]+>""", """@controllers.v2.FormsController@.updateConsent(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.addConsent"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions/""" + "$" + """id<[^/]+>""", """@controllers.v2.PrescriptionController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions/""" + "$" + """id<[^/]+>""", """@controllers.v2.PrescriptionController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptionHistory/Pdf""", """@controllers.v2.PrescriptionHistoryController@.generatePdf(patientId:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescription/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v2.PrescriptionController@.generatePdf(id:Long, treatment:String, signature:String, timezone:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """samplePDF""", """@controllers.v2.PrescriptionHistoryController@.samplePDF(patientId:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptionHistory""", """@controllers.v2.PrescriptionHistoryController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptionHistory/""" + "$" + """patientId<[^/]+>""", """@controllers.v2.PrescriptionHistoryController@.getPatient(patientId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptionHistory""", """@controllers.v2.PrescriptionHistoryController@.create"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptionHistory/""" + "$" + """id<[^/]+>""", """@controllers.v2.PrescriptionHistoryController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.readAll(resourceType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.read(resourceType:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/category/""" + "$" + """categoryId<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.readSubCategories(resourceType:String, categoryId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.create(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.bulkUpdate(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.update(resourceType:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceCategoriesController@.delete(resourceType:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests""", """@controllers.v2.EducationRequestController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests""", """@controllers.v2.EducationRequestController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart""", """@controllers.v3.CartController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cartWithProducts""", """@controllers.v3.CartController@.getCartWithProducts"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart""", """@controllers.v3.CartController@.update"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart/""" + "$" + """id<[^/]+>""", """@controllers.v3.CartController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """categoryId<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.readAll(resourceType:String, categoryId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/subCategory/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.read(resourceType:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.create(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.update(resourceType:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.delete(resourceType:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """clinicalNotes/""" + "$" + """id<[^/]+>""", """@controllers.v3.ClinicalNotesController@.readAll(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """clinicalNotes/""" + "$" + """id<[^/]+>""", """@controllers.v3.ClinicalNotesController@.create(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """clinicalNotes/""" + "$" + """id<[^/]+>""", """@controllers.v3.ClinicalNotesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """clinicalNotes/""" + "$" + """id<[^/]+>""", """@controllers.v3.ClinicalNotesController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """updateLock/""" + "$" + """id<[^/]+>""", """@controllers.v3.ClinicalNotesController@.updateLock(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """clinicalNotesMigration""", """@controllers.v3.ClinicalNotesController@.clinicalNotesMigration"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """operatories""", """@controllers.v3.OperatoriesController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """operatories""", """@controllers.v3.OperatoriesController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """operatories/""" + "$" + """id<[^/]+>""", """@controllers.v3.OperatoriesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """operatories/""" + "$" + """id<[^/]+>""", """@controllers.v3.OperatoriesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providers""", """@controllers.v3.ProvidersController@.readAll(practiceId:Option[Long])"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providers""", """@controllers.v3.ProvidersController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providers/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProvidersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providers/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProvidersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insuranceCompany""", """@controllers.v3.ProcedureController@.companyReadAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insuranceFee""", """@controllers.v3.ProcedureController@.feeReadAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """insuranceFee""", """@controllers.v3.ProcedureController@.updateInsuranceFee"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """procedure""", """@controllers.v3.ProcedureController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """procedure/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProcedureController@.read(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """procedure""", """@controllers.v3.ProcedureController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """procedure/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProcedureController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """procedure/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProcedureController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonWorkingDays""", """@controllers.v3.NonWorkingDaysController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonWorkingDays""", """@controllers.v3.NonWorkingDaysController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonWorkingDays/""" + "$" + """id<[^/]+>""", """@controllers.v3.NonWorkingDaysController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """nonWorkingDays/""" + "$" + """id<[^/]+>""", """@controllers.v3.NonWorkingDaysController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """workingHours""", """@controllers.v3.WorkingHoursController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """workingHours""", """@controllers.v3.WorkingHoursController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """workingHours/""" + "$" + """practiceId<[^/]+>""", """@controllers.v3.WorkingHoursController@.update(practiceId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointmentReason""", """@controllers.v3.AppointmentController@.appointmentReasonReadAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providerAppointmentPDF""", """@controllers.v3.AppointmentController@.generatePdf(fromDate:Option[String], toDate:Option[String], providerId:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providerAppointmentCSV""", """@controllers.v3.AppointmentController@.appointmentCSV(fromDate:Option[String], toDate:Option[String], providerId:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cancelReportPDF""", """@controllers.v3.AppointmentController@.generateCancelReportPdf(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providerAppointment""", """@controllers.v3.AppointmentController@.providerAppointmentList(fromDate:Option[String], toDate:Option[String], providerId:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cancelReport""", """@controllers.v3.AppointmentController@.cancelReportList(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """recallReport""", """@controllers.v3.AppointmentController@.recallReportList(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cancelReportCSV""", """@controllers.v3.AppointmentController@.generateCancelReportCsv(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/availableOperatories""", """@controllers.v3.AppointmentController@.availableOperatories(date:Option[String], startTime:Option[String], endTime:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """recallReportCSV""", """@controllers.v3.AppointmentController@.recallReportCSV(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """recallReportPDF""", """@controllers.v3.AppointmentController@.recallReportPDF(fromDate:Option[String], toDate:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providerFilter""", """@controllers.v3.AppointmentController@.getProviders"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/readAppointment""", """@controllers.v3.AppointmentController@.readAppointment(date:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/dailyBrief""", """@controllers.v3.AppointmentController@.dailyBrief(date:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/dailyBriefPDF""", """@controllers.v3.AppointmentController@.dailyBriefPdf(date:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/""" + "$" + """patientId<[^/]+>""", """@controllers.v3.AppointmentController@.patientAppointments(patientId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment""", """@controllers.v3.AppointmentController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """role""", """@controllers.v3.AppointmentController@.roleReadAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """migratePatientName""", """@controllers.v3.AppointmentController@.migratePatientName"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """checkAppointmentSetup""", """@controllers.v3.AppointmentController@.checkAppointmentSetup"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """providerFilter""", """@controllers.v3.AppointmentController@.updateProviders"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment""", """@controllers.v3.AppointmentController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/""" + "$" + """id<[^/]+>""", """@controllers.v3.AppointmentController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """appointment/""" + "$" + """id<[^/]+>""", """@controllers.v3.AppointmentController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """listOptionsReadAll/""" + "$" + """listType<[^/]+>""", """@controllers.v3.AppointmentController@.listOptionsReadAll(listType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlan/""" + "$" + """patientId<[^/]+>""", """@controllers.v3.TreatmentPlanController@.readAll(patientId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlan""", """@controllers.v3.TreatmentPlanController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlan/""" + "$" + """id<[^/]+>""", """@controllers.v3.TreatmentPlanController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlan/""" + "$" + """id<[^/]+>""", """@controllers.v3.TreatmentPlanController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlanDetails/""" + "$" + """planId<[^/]+>""", """@controllers.v3.TreatmentPlanDetailsController@.readAll(planId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlanDetails""", """@controllers.v3.TreatmentPlanDetailsController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlanDetails/""" + "$" + """id<[^/]+>""", """@controllers.v3.TreatmentPlanDetailsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlanDetails/""" + "$" + """id<[^/]+>""", """@controllers.v3.TreatmentPlanDetailsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatmentPlanDetails/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v3.TreatmentPlanDetailsController@.generatePdf(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """staffMembers""", """@controllers.v3.StaffMembersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """staffMembers""", """@controllers.v3.StaffMembersController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """staffMembers/""" + "$" + """id<[^/]+>""", """@controllers.v3.StaffMembersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """staffMembers/""" + "$" + """id<[^/]+>""", """@controllers.v3.StaffMembersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sharedPatient/""" + "$" + """patientId<[^/]+>""", """@controllers.v3.SharedPatientController@.sharedList(patientId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shareToAdmin/""" + "$" + """patientId<[^/]+>""", """@controllers.v3.SharedPatientController@.shareToAdminList(patientId:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sharedPatient""", """@controllers.v3.SharedPatientController@.create"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sharedPatient/""" + "$" + """id<[^/]+>""", """@controllers.v3.SharedPatientController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sharedPatient/""" + "$" + """id<[^/]+>""", """@controllers.v3.SharedPatientController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getDoctorsList""", """@controllers.v3.SharedPatientController@.getDoctors"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """mainSquareList""", """@controllers.v3.SharedPatientController@.mainSquareList"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_v1_HealthCheckController_check0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("healthcheck")))
  )
  private[this] lazy val controllers_v1_HealthCheckController_check0_invoker = createInvoker(
    HealthCheckController_35.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.HealthCheckController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """healthcheck"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_v1_SessionController_check1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_check1_invoker = createInvoker(
    SessionController_12.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.SessionController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_v1_SessionController_logIn2_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logIn2_invoker = createInvoker(
    SessionController_12.get.logIn,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.SessionController",
      "logIn",
      Nil,
      "PUT",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_v1_SessionController_logOut3_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logOut3_invoker = createInvoker(
    SessionController_12.get.logOut,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.SessionController",
      "logOut",
      Nil,
      "DELETE",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_v1_SessionController_signUpRequest4_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session/signUpRequest")))
  )
  private[this] lazy val controllers_v1_SessionController_signUpRequest4_invoker = createInvoker(
    SessionController_12.get.signUpRequest,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.SessionController",
      "signUpRequest",
      Nil,
      "PUT",
      """""",
      this.prefix + """session/signUpRequest"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_v1_SessionController_getAuthorizeNetDetails5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getAuthorizeNetDetails")))
  )
  private[this] lazy val controllers_v1_SessionController_getAuthorizeNetDetails5_invoker = createInvoker(
    SessionController_12.get.getAuthorizeNetDetails,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.SessionController",
      "getAuthorizeNetDetails",
      Nil,
      "GET",
      """""",
      this.prefix + """getAuthorizeNetDetails"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_v1_PasswordController_forgot6_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forgot/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_forgot6_invoker = createInvoker(
    PasswordController_33.get.forgot(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PasswordController",
      "forgot",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """forgot/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_v1_PasswordController_check7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_check7_invoker = createInvoker(
    PasswordController_33.get.check(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PasswordController",
      "check",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_v1_PasswordController_reset8_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_reset8_invoker = createInvoker(
    PasswordController_33.get.reset(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PasswordController",
      "reset",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_v1_PasswordController_change9_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("password")))
  )
  private[this] lazy val controllers_v1_PasswordController_change9_invoker = createInvoker(
    PasswordController_33.get.change,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PasswordController",
      "change",
      Nil,
      "PUT",
      """""",
      this.prefix + """password"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_v3_AnalyticsController_read10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("analytics")))
  )
  private[this] lazy val controllers_v3_AnalyticsController_read10_invoker = createInvoker(
    AnalyticsController_11.get.read,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AnalyticsController",
      "read",
      Nil,
      "GET",
      """""",
      this.prefix + """analytics"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_v2_CategoriesController_readAll11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_readAll11_invoker = createInvoker(
    CategoriesController_7.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.CategoriesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate12_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate12_invoker = createInvoker(
    CategoriesController_7.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.CategoriesController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_v2_CategoriesController_update13_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_update13_invoker = createInvoker(
    CategoriesController_7.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.CategoriesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_v2_CategoriesController_delete14_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_delete14_invoker = createInvoker(
    CategoriesController_7.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.CategoriesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_v3_ProductsController_generateCSV15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/generateCSV")))
  )
  private[this] lazy val controllers_v3_ProductsController_generateCSV15_invoker = createInvoker(
    ProductsController_14.get.generateCSV,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "generateCSV",
      Nil,
      "GET",
      """""",
      this.prefix + """products/generateCSV"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_v3_ProductsController_readAll16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_readAll16_invoker = createInvoker(
    ProductsController_14.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_v3_ProductsController_create17_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_create17_invoker = createInvoker(
    ProductsController_14.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_v3_ProductsController_read18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_read18_invoker = createInvoker(
    ProductsController_14.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_v3_ProductsController_update19_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_update19_invoker = createInvoker(
    ProductsController_14.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_v3_ProductsController_bulkUpdate20_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_bulkUpdate20_invoker = createInvoker(
    ProductsController_14.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_v3_ProductsController_delete21_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_delete21_invoker = createInvoker(
    ProductsController_14.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_v3_ProductsController_generatePdf22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/generatePdf/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v3_ProductsController_generatePdf22_invoker = createInvoker(
    ProductsController_14.get.generatePdf(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "generatePdf",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """products/generatePdf/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_v3_ProductsController_findProducts23_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("findProducts")))
  )
  private[this] lazy val controllers_v3_ProductsController_findProducts23_invoker = createInvoker(
    ProductsController_14.get.findProducts,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "findProducts",
      Nil,
      "POST",
      """""",
      this.prefix + """findProducts"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_v3_ProductsController_getCategories24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getCategories")))
  )
  private[this] lazy val controllers_v3_ProductsController_getCategories24_invoker = createInvoker(
    ProductsController_14.get.getCategories,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "getCategories",
      Nil,
      "POST",
      """""",
      this.prefix + """getCategories"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_v2_FavouritesController_add25_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_add25_invoker = createInvoker(
    FavouritesController_13.get.add(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FavouritesController",
      "add",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:40
  private[this] lazy val controllers_v2_FavouritesController_remove26_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_remove26_invoker = createInvoker(
    FavouritesController_13.get.remove(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FavouritesController",
      "remove",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:42
  private[this] lazy val controllers_v3_ProductsController_webService27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("webService/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_webService27_invoker = createInvoker(
    ProductsController_14.get.webService(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProductsController",
      "webService",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """webService/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_v2_VendorController_readAll28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_readAll28_invoker = createInvoker(
    VendorController_10.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_v2_VendorController_create29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_create29_invoker = createInvoker(
    VendorController_10.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:47
  private[this] lazy val controllers_v2_VendorController_read30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_read30_invoker = createInvoker(
    VendorController_10.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:48
  private[this] lazy val controllers_v2_VendorController_update31_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_update31_invoker = createInvoker(
    VendorController_10.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:50
  private[this] lazy val controllers_v2_VendorController_stateTaxReadAll32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stateTax")))
  )
  private[this] lazy val controllers_v2_VendorController_stateTaxReadAll32_invoker = createInvoker(
    VendorController_10.get.stateTaxReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "stateTaxReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """stateTax"""
    )
  )

  // @LINE:51
  private[this] lazy val controllers_v2_VendorController_stateTaxUpdate33_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stateTax/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_stateTaxUpdate33_invoker = createInvoker(
    VendorController_10.get.stateTaxUpdate(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "stateTaxUpdate",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """stateTax/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_v2_VendorController_findSaleTax34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stateTax/findSaleTax/"), DynamicPart("shippingId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_findSaleTax34_invoker = createInvoker(
    VendorController_10.get.findSaleTax(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "findSaleTax",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """stateTax/findSaleTax/""" + "$" + """shippingId<[^/]+>"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_v2_VendorController_saleTaxReadAll35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("saleTax/"), DynamicPart("vendorId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_saleTaxReadAll35_invoker = createInvoker(
    VendorController_10.get.saleTaxReadAll(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "saleTaxReadAll",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """saleTax/""" + "$" + """vendorId<[^/]+>"""
    )
  )

  // @LINE:55
  private[this] lazy val controllers_v2_VendorController_saleTaxCreate36_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("saleTax")))
  )
  private[this] lazy val controllers_v2_VendorController_saleTaxCreate36_invoker = createInvoker(
    VendorController_10.get.saleTaxCreate,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "saleTaxCreate",
      Nil,
      "POST",
      """""",
      this.prefix + """saleTax"""
    )
  )

  // @LINE:56
  private[this] lazy val controllers_v2_VendorController_saleTaxUpdate37_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("saleTax/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_saleTaxUpdate37_invoker = createInvoker(
    VendorController_10.get.saleTaxUpdate(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "saleTaxUpdate",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """saleTax/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_v2_VendorController_stateReadAll38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("state")))
  )
  private[this] lazy val controllers_v2_VendorController_stateReadAll38_invoker = createInvoker(
    VendorController_10.get.stateReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "stateReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """state"""
    )
  )

  // @LINE:60
  private[this] lazy val controllers_v2_VendorController_archive39_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_archive39_invoker = createInvoker(
    VendorController_10.get.archive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "archive",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_v2_VendorController_unarchive40_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true), StaticPart("/unarchive")))
  )
  private[this] lazy val controllers_v2_VendorController_unarchive40_invoker = createInvoker(
    VendorController_10.get.unarchive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "unarchive",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>/unarchive"""
    )
  )

  // @LINE:62
  private[this] lazy val controllers_v2_VendorController_increasePrice41_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/increasePrice/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_increasePrice41_invoker = createInvoker(
    VendorController_10.get.increasePrice(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "increasePrice",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/increasePrice/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:63
  private[this] lazy val controllers_v2_VendorController_getPriceChangeHisotry42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/priceChangeHistory/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_getPriceChangeHisotry42_invoker = createInvoker(
    VendorController_10.get.getPriceChangeHisotry(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "getPriceChangeHisotry",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """vendors/priceChangeHistory/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:64
  private[this] lazy val controllers_v2_VendorController_deleteAllProducts43_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/deleteAllProducts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_deleteAllProducts43_invoker = createInvoker(
    VendorController_10.get.deleteAllProducts(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.VendorController",
      "deleteAllProducts",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """vendors/deleteAllProducts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:66
  private[this] lazy val controllers_v1_OrdersController_generateCSV44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/generateCSV")))
  )
  private[this] lazy val controllers_v1_OrdersController_generateCSV44_invoker = createInvoker(
    OrdersController_8.get.generateCSV(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "generateCSV",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """orders/generateCSV"""
    )
  )

  // @LINE:67
  private[this] lazy val controllers_v1_OrdersController_readAll45_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_readAll45_invoker = createInvoker(
    OrdersController_8.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:68
  private[this] lazy val controllers_v1_OrdersController_catalogOrders46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("catalogOrders")))
  )
  private[this] lazy val controllers_v1_OrdersController_catalogOrders46_invoker = createInvoker(
    OrdersController_8.get.catalogOrders,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "catalogOrders",
      Nil,
      "GET",
      """""",
      this.prefix + """catalogOrders"""
    )
  )

  // @LINE:69
  private[this] lazy val controllers_v1_OrdersController_create47_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_create47_invoker = createInvoker(
    OrdersController_8.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:70
  private[this] lazy val controllers_v1_OrdersController_read48_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_read48_invoker = createInvoker(
    OrdersController_8.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:71
  private[this] lazy val controllers_v1_OrdersController_update49_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_update49_invoker = createInvoker(
    OrdersController_8.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:72
  private[this] lazy val controllers_v1_OrdersController_delete50_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_delete50_invoker = createInvoker(
    OrdersController_8.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:73
  private[this] lazy val controllers_v1_OrdersController_updateGrandTotal51_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("updateGrandTotal")))
  )
  private[this] lazy val controllers_v1_OrdersController_updateGrandTotal51_invoker = createInvoker(
    OrdersController_8.get.updateGrandTotal,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "updateGrandTotal",
      Nil,
      "PUT",
      """""",
      this.prefix + """updateGrandTotal"""
    )
  )

  // @LINE:74
  private[this] lazy val controllers_v1_OrdersController_updateVendorPayment52_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/updateVendorPayment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_updateVendorPayment52_invoker = createInvoker(
    OrdersController_8.get.updateVendorPayment(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.OrdersController",
      "updateVendorPayment",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/updateVendorPayment/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:76
  private[this] lazy val controllers_v1_TreatmentsController_generateReferralSourcePdf53_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("referralSourcePDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generateReferralSourcePdf53_invoker = createInvoker(
    TreatmentsController_6.get.generateReferralSourcePdf(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generateReferralSourcePdf",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """referralSourcePDF"""
    )
  )

  // @LINE:77
  private[this] lazy val controllers_v1_TreatmentsController_generateReferralSourceCsv54_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("referralSourceCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generateReferralSourceCsv54_invoker = createInvoker(
    TreatmentsController_6.get.generateReferralSourceCsv(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generateReferralSourceCsv",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """referralSourceCSV"""
    )
  )

  // @LINE:78
  private[this] lazy val controllers_v1_TreatmentsController_referralSourceList55_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("referralSource")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_referralSourceList55_invoker = createInvoker(
    TreatmentsController_6.get.referralSourceList(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "referralSourceList",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """referralSource"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_v1_TreatmentsController_readAll56_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_readAll56_invoker = createInvoker(
    TreatmentsController_6.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:80
  private[this] lazy val controllers_v1_TreatmentsController_create57_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_create57_invoker = createInvoker(
    TreatmentsController_6.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:81
  private[this] lazy val controllers_v1_TreatmentsController_patientList58_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patientList")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientList58_invoker = createInvoker(
    TreatmentsController_6.get.patientList,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientList",
      Nil,
      "GET",
      """""",
      this.prefix + """patientList"""
    )
  )

  // @LINE:82
  private[this] lazy val controllers_v1_TreatmentsController_read59_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_read59_invoker = createInvoker(
    TreatmentsController_6.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:83
  private[this] lazy val controllers_v1_TreatmentsController_mergeAll1B1C60_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("merge1b1c")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_mergeAll1B1C60_invoker = createInvoker(
    TreatmentsController_6.get.mergeAll1B1C,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "mergeAll1B1C",
      Nil,
      "PUT",
      """""",
      this.prefix + """merge1b1c"""
    )
  )

  // @LINE:84
  private[this] lazy val controllers_v1_TreatmentsController_mergeAll3B3C61_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("merge3b3c")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_mergeAll3B3C61_invoker = createInvoker(
    TreatmentsController_6.get.mergeAll3B3C,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "mergeAll3B3C",
      Nil,
      "PUT",
      """""",
      this.prefix + """merge3b3c"""
    )
  )

  // @LINE:85
  private[this] lazy val controllers_v1_TreatmentsController_mergeForms62_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("mergeForms")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_mergeForms62_invoker = createInvoker(
    TreatmentsController_6.get.mergeForms(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "mergeForms",
      Seq(classOf[String], classOf[String]),
      "PUT",
      """""",
      this.prefix + """mergeForms"""
    )
  )

  // @LINE:86
  private[this] lazy val controllers_v1_TreatmentsController_migrateTreatment63_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("migrateTreatment")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_migrateTreatment63_invoker = createInvoker(
    TreatmentsController_6.get.migrateTreatment,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "migrateTreatment",
      Nil,
      "PUT",
      """""",
      this.prefix + """migrateTreatment"""
    )
  )

  // @LINE:87
  private[this] lazy val controllers_v1_TreatmentsController_migrateMedicalCondtionSelected64_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("migrateMedicalCondtionSelected")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_migrateMedicalCondtionSelected64_invoker = createInvoker(
    TreatmentsController_6.get.migrateMedicalCondtionSelected,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "migrateMedicalCondtionSelected",
      Nil,
      "PUT",
      """""",
      this.prefix + """migrateMedicalCondtionSelected"""
    )
  )

  // @LINE:89
  private[this] lazy val controllers_v1_TreatmentsController_getTreatmentAdditionalData65_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentAdditionalData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_getTreatmentAdditionalData65_invoker = createInvoker(
    TreatmentsController_6.get.getTreatmentAdditionalData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "getTreatmentAdditionalData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatmentAdditionalData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:90
  private[this] lazy val controllers_v1_TreatmentsController_newPatientsList66_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("newPatient")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_newPatientsList66_invoker = createInvoker(
    TreatmentsController_6.get.newPatientsList(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "newPatientsList",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """newPatient"""
    )
  )

  // @LINE:92
  private[this] lazy val controllers_v1_TreatmentsController_update67_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_update67_invoker = createInvoker(
    TreatmentsController_6.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:93
  private[this] lazy val controllers_v1_TreatmentsController_delete68_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_delete68_invoker = createInvoker(
    TreatmentsController_6.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:94
  private[this] lazy val controllers_v1_TreatmentsController_pdf69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_pdf69_invoker = createInvoker(
    TreatmentsController_6.get.pdf(fakeValue[Long], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "pdf",
      Seq(classOf[Long], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:95
  private[this] lazy val controllers_v1_TreatmentsController_testpdf70_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/testpdf/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_testpdf70_invoker = createInvoker(
    TreatmentsController_6.get.testpdf(fakeValue[Long], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "testpdf",
      Seq(classOf[Long], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """treatments/testpdf/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:96
  private[this] lazy val controllers_v1_TreatmentsController_updateCtScan71_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateCtScan/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateCtScan71_invoker = createInvoker(
    TreatmentsController_6.get.updateCtScan(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateCtScan",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateCtScan/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:97
  private[this] lazy val controllers_v1_TreatmentsController_updateContracts72_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateContracts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateContracts72_invoker = createInvoker(
    TreatmentsController_6.get.updateContracts(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateContracts",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateContracts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:98
  private[this] lazy val controllers_v1_TreatmentsController_recallPeriod73_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/recallPeriod/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_recallPeriod73_invoker = createInvoker(
    TreatmentsController_6.get.recallPeriod(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "recallPeriod",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/recallPeriod/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:99
  private[this] lazy val controllers_v1_TreatmentsController_updateScratchPad74_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateScratchPad/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateScratchPad74_invoker = createInvoker(
    TreatmentsController_6.get.updateScratchPad(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateScratchPad",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateScratchPad/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:100
  private[this] lazy val controllers_v1_TreatmentsController_getMeasurementHisotry75_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("measurementHistory/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_getMeasurementHisotry75_invoker = createInvoker(
    TreatmentsController_6.get.getMeasurementHisotry(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "getMeasurementHisotry",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """measurementHistory/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:101
  private[this] lazy val controllers_v1_TreatmentsController_updateToothChartData76_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateToothChartData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateToothChartData76_invoker = createInvoker(
    TreatmentsController_6.get.updateToothChartData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateToothChartData",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """treatments/updateToothChartData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:102
  private[this] lazy val controllers_v1_TreatmentsController_getToothChartData77_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/getToothChartData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_getToothChartData77_invoker = createInvoker(
    TreatmentsController_6.get.getToothChartData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "getToothChartData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/getToothChartData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:103
  private[this] lazy val controllers_v1_TreatmentsController_getPerioChartData78_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/getPerioChartData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_getPerioChartData78_invoker = createInvoker(
    TreatmentsController_6.get.getPerioChartData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "getPerioChartData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/getPerioChartData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:104
  private[this] lazy val controllers_v1_TreatmentsController_updatePerioChartData79_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updatePerioChartData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updatePerioChartData79_invoker = createInvoker(
    TreatmentsController_6.get.updatePerioChartData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updatePerioChartData",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """treatments/updatePerioChartData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:105
  private[this] lazy val controllers_v1_TreatmentsController_updateXray80_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateXray/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateXray80_invoker = createInvoker(
    TreatmentsController_6.get.updateXray(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateXray",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateXray/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:106
  private[this] lazy val controllers_v1_TreatmentsController_updateToothChartDate81_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateToothChartDate/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateToothChartDate81_invoker = createInvoker(
    TreatmentsController_6.get.updateToothChartDate(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updateToothChartDate",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateToothChartDate/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:107
  private[this] lazy val controllers_v1_TreatmentsController_migrateAllMedicalCondtionLog82_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("migrateAllMedicalCondtionLog")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_migrateAllMedicalCondtionLog82_invoker = createInvoker(
    TreatmentsController_6.get.migrateAllMedicalCondtionLog,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "migrateAllMedicalCondtionLog",
      Nil,
      "PUT",
      """""",
      this.prefix + """migrateAllMedicalCondtionLog"""
    )
  )

  // @LINE:108
  private[this] lazy val controllers_v1_TreatmentsController_updatePerioChartDataFormat83_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("updatePerioChartDataFormat")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updatePerioChartDataFormat83_invoker = createInvoker(
    TreatmentsController_6.get.updatePerioChartDataFormat,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "updatePerioChartDataFormat",
      Nil,
      "PUT",
      """""",
      this.prefix + """updatePerioChartDataFormat"""
    )
  )

  // @LINE:109
  private[this] lazy val controllers_v1_TreatmentsController_generateNewPatientPDF84_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("newPatientPDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generateNewPatientPDF84_invoker = createInvoker(
    TreatmentsController_6.get.generateNewPatientPDF(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generateNewPatientPDF",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """newPatientPDF"""
    )
  )

  // @LINE:110
  private[this] lazy val controllers_v1_TreatmentsController_generateNewPatientCSV85_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("newPatientCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generateNewPatientCSV85_invoker = createInvoker(
    TreatmentsController_6.get.generateNewPatientCSV(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generateNewPatientCSV",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """newPatientCSV"""
    )
  )

  // @LINE:111
  private[this] lazy val controllers_v1_TreatmentsController_patientTypePdf86_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientTypePDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientTypePdf86_invoker = createInvoker(
    TreatmentsController_6.get.patientTypePdf(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientTypePdf",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """PatientTypePDF"""
    )
  )

  // @LINE:112
  private[this] lazy val controllers_v1_TreatmentsController_patientTypeCSV87_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientTypeCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientTypeCSV87_invoker = createInvoker(
    TreatmentsController_6.get.patientTypeCSV(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientTypeCSV",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """PatientTypeCSV"""
    )
  )

  // @LINE:113
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsOutstandingBalancePdf88_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientBalancePDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsOutstandingBalancePdf88_invoker = createInvoker(
    TreatmentsController_6.get.generatePatientsOutstandingBalancePdf,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generatePatientsOutstandingBalancePdf",
      Nil,
      "GET",
      """""",
      this.prefix + """PatientBalancePDF"""
    )
  )

  // @LINE:114
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsOutstandingBalanceCsv89_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientBalanceCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsOutstandingBalanceCsv89_invoker = createInvoker(
    TreatmentsController_6.get.generatePatientsOutstandingBalanceCsv,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generatePatientsOutstandingBalanceCsv",
      Nil,
      "GET",
      """""",
      this.prefix + """PatientBalanceCSV"""
    )
  )

  // @LINE:115
  private[this] lazy val controllers_v1_TreatmentsController_patientsOutstandingBalanceList90_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientBalance")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientsOutstandingBalanceList90_invoker = createInvoker(
    TreatmentsController_6.get.patientsOutstandingBalanceList,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientsOutstandingBalanceList",
      Nil,
      "GET",
      """""",
      this.prefix + """PatientBalance"""
    )
  )

  // @LINE:116
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsurancePDF91_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("outstandingInsurancePDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsurancePDF91_invoker = createInvoker(
    TreatmentsController_6.get.outstandingInsurancePDF,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "outstandingInsurancePDF",
      Nil,
      "GET",
      """""",
      this.prefix + """outstandingInsurancePDF"""
    )
  )

  // @LINE:117
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsuranceCSV92_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("outstandingInsuranceCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsuranceCSV92_invoker = createInvoker(
    TreatmentsController_6.get.outstandingInsuranceCSV,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "outstandingInsuranceCSV",
      Nil,
      "GET",
      """""",
      this.prefix + """outstandingInsuranceCSV"""
    )
  )

  // @LINE:118
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsurance93_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("outstandingInsurance")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_outstandingInsurance93_invoker = createInvoker(
    TreatmentsController_6.get.outstandingInsurance,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "outstandingInsurance",
      Nil,
      "GET",
      """""",
      this.prefix + """outstandingInsurance"""
    )
  )

  // @LINE:119
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmountPDF94_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("totalCollectedAmountPDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmountPDF94_invoker = createInvoker(
    TreatmentsController_6.get.totalCollectedAmountPDF(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "totalCollectedAmountPDF",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """totalCollectedAmountPDF"""
    )
  )

  // @LINE:120
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmountCSV95_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("totalCollectedAmountCSV")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmountCSV95_invoker = createInvoker(
    TreatmentsController_6.get.totalCollectedAmountCSV(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "totalCollectedAmountCSV",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """totalCollectedAmountCSV"""
    )
  )

  // @LINE:121
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmount96_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("totalCollectedAmount")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_totalCollectedAmount96_invoker = createInvoker(
    TreatmentsController_6.get.totalCollectedAmount(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "totalCollectedAmount",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """totalCollectedAmount"""
    )
  )

  // @LINE:122
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsStatementPdf97_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientStatementPDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_generatePatientsStatementPdf97_invoker = createInvoker(
    TreatmentsController_6.get.generatePatientsStatementPdf(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "generatePatientsStatementPdf",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """PatientStatementPDF"""
    )
  )

  // @LINE:123
  private[this] lazy val controllers_v1_TreatmentsController_patientEstimatePDF98_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patientEstimatePDF")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientEstimatePDF98_invoker = createInvoker(
    TreatmentsController_6.get.patientEstimatePDF(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientEstimatePDF",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """patientEstimatePDF"""
    )
  )

  // @LINE:124
  private[this] lazy val controllers_v1_TreatmentsController_patientTypeList99_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("PatientType")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_patientTypeList99_invoker = createInvoker(
    TreatmentsController_6.get.patientTypeList(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "patientTypeList",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """PatientType"""
    )
  )

  // @LINE:125
  private[this] lazy val controllers_v1_TreatmentsController_shareToAdmin100_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shareToAdmin/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_shareToAdmin100_invoker = createInvoker(
    TreatmentsController_6.get.shareToAdmin(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.TreatmentsController",
      "shareToAdmin",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """shareToAdmin/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:127
  private[this] lazy val controllers_v1_PatientsController_readAll101_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients")))
  )
  private[this] lazy val controllers_v1_PatientsController_readAll101_invoker = createInvoker(
    PatientsController_4.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PatientsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """patients"""
    )
  )

  // @LINE:128
  private[this] lazy val controllers_v1_PatientsController_read102_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PatientsController_read102_invoker = createInvoker(
    PatientsController_4.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PatientsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """patients/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:129
  private[this] lazy val controllers_v1_PatientsController_generateHealthHistoryForm103_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generateHealthHistory/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_PatientsController_generateHealthHistoryForm103_invoker = createInvoker(
    PatientsController_4.get.generateHealthHistoryForm(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PatientsController",
      "generateHealthHistoryForm",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """generateHealthHistory/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:130
  private[this] lazy val controllers_v1_PatientsController_generateConsentForm104_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generateConsentForm/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_PatientsController_generateConsentForm104_invoker = createInvoker(
    PatientsController_4.get.generateConsentForm(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PatientsController",
      "generateConsentForm",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """generateConsentForm/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:133
  private[this] lazy val controllers_v1_ConsultationsController_readAll105_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_readAll105_invoker = createInvoker(
    ConsultationsController_15.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.ConsultationsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:134
  private[this] lazy val controllers_v1_ConsultationsController_create106_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_create106_invoker = createInvoker(
    ConsultationsController_15.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.ConsultationsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:135
  private[this] lazy val controllers_v1_ConsultationsController_read107_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_read107_invoker = createInvoker(
    ConsultationsController_15.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.ConsultationsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:136
  private[this] lazy val controllers_v1_ConsultationsController_update108_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_update108_invoker = createInvoker(
    ConsultationsController_15.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.ConsultationsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:137
  private[this] lazy val controllers_v1_ConsultationsController_delete109_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_delete109_invoker = createInvoker(
    ConsultationsController_15.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.ConsultationsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:139
  private[this] lazy val controllers_v1_UsersController_readAll110_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_readAll110_invoker = createInvoker(
    UsersController_1.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UsersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:140
  private[this] lazy val controllers_v1_UsersController_create111_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_create111_invoker = createInvoker(
    UsersController_1.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UsersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:141
  private[this] lazy val controllers_v1_UsersController_read112_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_read112_invoker = createInvoker(
    UsersController_1.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UsersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:142
  private[this] lazy val controllers_v1_UsersController_update113_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_update113_invoker = createInvoker(
    UsersController_1.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UsersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:143
  private[this] lazy val controllers_v1_UsersController_delete114_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_delete114_invoker = createInvoker(
    UsersController_1.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UsersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:145
  private[this] lazy val controllers_v1_StaffController_readAll115_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_readAll115_invoker = createInvoker(
    StaffController_29.get.readAll(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "readAll",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:146
  private[this] lazy val controllers_v1_StaffController_create116_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_create116_invoker = createInvoker(
    StaffController_29.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:147
  private[this] lazy val controllers_v1_StaffController_read117_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_read117_invoker = createInvoker(
    StaffController_29.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:148
  private[this] lazy val controllers_v1_StaffController_update118_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_update118_invoker = createInvoker(
    StaffController_29.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:149
  private[this] lazy val controllers_v1_StaffController_updateAuthorizeCustId119_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/updateAuthorizeCustId")))
  )
  private[this] lazy val controllers_v1_StaffController_updateAuthorizeCustId119_invoker = createInvoker(
    StaffController_29.get.updateAuthorizeCustId,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "updateAuthorizeCustId",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/updateAuthorizeCustId"""
    )
  )

  // @LINE:150
  private[this] lazy val controllers_v1_StaffController_setUDID120_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/udid")))
  )
  private[this] lazy val controllers_v1_StaffController_setUDID120_invoker = createInvoker(
    StaffController_29.get.setUDID,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "setUDID",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/udid"""
    )
  )

  // @LINE:151
  private[this] lazy val controllers_v1_StaffController_setUDID1121_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/udid1")))
  )
  private[this] lazy val controllers_v1_StaffController_setUDID1121_invoker = createInvoker(
    StaffController_29.get.setUDID1,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "setUDID1",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/udid1"""
    )
  )

  // @LINE:152
  private[this] lazy val controllers_v1_StaffController_delete122_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_delete122_invoker = createInvoker(
    StaffController_29.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:153
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate123_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true), StaticPart("/extend")))
  )
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate123_invoker = createInvoker(
    StaffController_29.get.extendExpirationDate(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "extendExpirationDate",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>/extend"""
    )
  )

  // @LINE:154
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate1124_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true), StaticPart("/extend1")))
  )
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate1124_invoker = createInvoker(
    StaffController_29.get.extendExpirationDate1(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "extendExpirationDate1",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>/extend1"""
    )
  )

  // @LINE:155
  private[this] lazy val controllers_v1_StaffController_clearUDID125_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/clearUDID/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_clearUDID125_invoker = createInvoker(
    StaffController_29.get.clearUDID(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "clearUDID",
      Seq(classOf[String]),
      "DELETE",
      """""",
      this.prefix + """accounts/clearUDID/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:156
  private[this] lazy val controllers_v1_StaffController_roleToSquareAccessMapping126_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("roleToSquareAccessMapping")))
  )
  private[this] lazy val controllers_v1_StaffController_roleToSquareAccessMapping126_invoker = createInvoker(
    StaffController_29.get.roleToSquareAccessMapping,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "roleToSquareAccessMapping",
      Nil,
      "PUT",
      """""",
      this.prefix + """roleToSquareAccessMapping"""
    )
  )

  // @LINE:158
  private[this] lazy val controllers_v1_StaffController_readAllRenewalDetails127_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("renewalDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_readAllRenewalDetails127_invoker = createInvoker(
    StaffController_29.get.readAllRenewalDetails(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "readAllRenewalDetails",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """renewalDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:159
  private[this] lazy val controllers_v1_StaffController_updateRenewalDetails128_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("renewalDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_updateRenewalDetails128_invoker = createInvoker(
    StaffController_29.get.updateRenewalDetails(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "updateRenewalDetails",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """renewalDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:161
  private[this] lazy val controllers_v1_StaffController_bulkUpdate129_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("staffImport")))
  )
  private[this] lazy val controllers_v1_StaffController_bulkUpdate129_invoker = createInvoker(
    StaffController_29.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.StaffController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """staffImport"""
    )
  )

  // @LINE:164
  private[this] lazy val controllers_v2_ProfileController_read130_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_read130_invoker = createInvoker(
    ProfileController_21.get.read,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ProfileController",
      "read",
      Nil,
      "GET",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:165
  private[this] lazy val controllers_v2_ProfileController_update131_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_update131_invoker = createInvoker(
    ProfileController_21.get.update,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ProfileController",
      "update",
      Nil,
      "PUT",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:167
  private[this] lazy val controllers_v1_PracticeController_readAll132_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_readAll132_invoker = createInvoker(
    PracticeController_36.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PracticeController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:168
  private[this] lazy val controllers_v1_PracticeController_create133_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_create133_invoker = createInvoker(
    PracticeController_36.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PracticeController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:169
  private[this] lazy val controllers_v1_PracticeController_read134_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_read134_invoker = createInvoker(
    PracticeController_36.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PracticeController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:170
  private[this] lazy val controllers_v1_PracticeController_update135_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_update135_invoker = createInvoker(
    PracticeController_36.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PracticeController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:171
  private[this] lazy val controllers_v1_PracticeController_delete136_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_delete136_invoker = createInvoker(
    PracticeController_36.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.PracticeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:173
  private[this] lazy val controllers_v3_CoursesController_readAll137_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_readAll137_invoker = createInvoker(
    CoursesController_2.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CoursesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>"""
    )
  )

  // @LINE:174
  private[this] lazy val controllers_v3_CoursesController_read138_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_read138_invoker = createInvoker(
    CoursesController_2.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CoursesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:175
  private[this] lazy val controllers_v3_CoursesController_create139_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_create139_invoker = createInvoker(
    CoursesController_2.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CoursesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>"""
    )
  )

  // @LINE:176
  private[this] lazy val controllers_v3_CoursesController_update140_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_update140_invoker = createInvoker(
    CoursesController_2.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CoursesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:177
  private[this] lazy val controllers_v3_CoursesController_delete141_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_delete141_invoker = createInvoker(
    CoursesController_2.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CoursesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:179
  private[this] lazy val controllers_v1_UploadsController_signature142_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signature/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UploadsController_signature142_invoker = createInvoker(
    UploadsController_17.get.signature(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v1.UploadsController",
      "signature",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """signature/""" + "$" + """name<[^/]+>"""
    )
  )

  // @LINE:181
  private[this] lazy val controllers_v2_EmailMappingController_readAll143_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_readAll143_invoker = createInvoker(
    EmailMappingController_37.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:182
  private[this] lazy val controllers_v2_EmailMappingController_create144_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_create144_invoker = createInvoker(
    EmailMappingController_37.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:183
  private[this] lazy val controllers_v2_EmailMappingController_read145_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_read145_invoker = createInvoker(
    EmailMappingController_37.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:184
  private[this] lazy val controllers_v2_EmailMappingController_update146_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_update146_invoker = createInvoker(
    EmailMappingController_37.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:185
  private[this] lazy val controllers_v2_EmailMappingController_delete147_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_delete147_invoker = createInvoker(
    EmailMappingController_37.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:186
  private[this] lazy val controllers_v2_EmailMappingController_addUser148_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/add")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_addUser148_invoker = createInvoker(
    EmailMappingController_37.get.addUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "addUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/add"""
    )
  )

  // @LINE:187
  private[this] lazy val controllers_v2_EmailMappingController_removeUser149_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/rem")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_removeUser149_invoker = createInvoker(
    EmailMappingController_37.get.removeUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EmailMappingController",
      "removeUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/rem"""
    )
  )

  // @LINE:189
  private[this] lazy val controllers_v2_ShippingController_readAll150_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_readAll150_invoker = createInvoker(
    ShippingController_19.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ShippingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:190
  private[this] lazy val controllers_v2_ShippingController_create151_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_create151_invoker = createInvoker(
    ShippingController_19.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ShippingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:191
  private[this] lazy val controllers_v2_ShippingController_read152_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_read152_invoker = createInvoker(
    ShippingController_19.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ShippingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:192
  private[this] lazy val controllers_v2_ShippingController_update153_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_update153_invoker = createInvoker(
    ShippingController_19.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ShippingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:193
  private[this] lazy val controllers_v2_ShippingController_delete154_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_delete154_invoker = createInvoker(
    ShippingController_19.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.ShippingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:195
  private[this] lazy val controllers_v2_FormsController_readForms155_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forms")))
  )
  private[this] lazy val controllers_v2_FormsController_readForms155_invoker = createInvoker(
    FormsController_5.get.readForms,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FormsController",
      "readForms",
      Nil,
      "GET",
      """""",
      this.prefix + """forms"""
    )
  )

  // @LINE:196
  private[this] lazy val controllers_v2_FormsController_readAllConsents156_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_readAllConsents156_invoker = createInvoker(
    FormsController_5.get.readAllConsents,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FormsController",
      "readAllConsents",
      Nil,
      "GET",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:197
  private[this] lazy val controllers_v2_FormsController_updateConsent157_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_FormsController_updateConsent157_invoker = createInvoker(
    FormsController_5.get.updateConsent(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FormsController",
      "updateConsent",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """consents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:198
  private[this] lazy val controllers_v2_FormsController_addConsent158_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_addConsent158_invoker = createInvoker(
    FormsController_5.get.addConsent,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.FormsController",
      "addConsent",
      Nil,
      "POST",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:200
  private[this] lazy val controllers_v2_PrescriptionController_readAll159_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_readAll159_invoker = createInvoker(
    PrescriptionController_23.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:201
  private[this] lazy val controllers_v2_PrescriptionController_create160_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_create160_invoker = createInvoker(
    PrescriptionController_23.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:202
  private[this] lazy val controllers_v2_PrescriptionController_update161_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionController_update161_invoker = createInvoker(
    PrescriptionController_23.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """prescriptions/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:203
  private[this] lazy val controllers_v2_PrescriptionController_delete162_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionController_delete162_invoker = createInvoker(
    PrescriptionController_23.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """prescriptions/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:206
  private[this] lazy val controllers_v2_PrescriptionHistoryController_generatePdf163_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptionHistory/Pdf")))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_generatePdf163_invoker = createInvoker(
    PrescriptionHistoryController_0.get.generatePdf(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "generatePdf",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """prescriptionHistory/Pdf"""
    )
  )

  // @LINE:207
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf164_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescription/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf164_invoker = createInvoker(
    PrescriptionController_23.get.generatePdf(fakeValue[Long], fakeValue[String], fakeValue[String], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionController",
      "generatePdf",
      Seq(classOf[Long], classOf[String], classOf[String], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """prescription/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:208
  private[this] lazy val controllers_v2_PrescriptionHistoryController_samplePDF165_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("samplePDF")))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_samplePDF165_invoker = createInvoker(
    PrescriptionHistoryController_0.get.samplePDF(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "samplePDF",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """samplePDF"""
    )
  )

  // @LINE:210
  private[this] lazy val controllers_v2_PrescriptionHistoryController_readAll166_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptionHistory")))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_readAll166_invoker = createInvoker(
    PrescriptionHistoryController_0.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """prescriptionHistory"""
    )
  )

  // @LINE:211
  private[this] lazy val controllers_v2_PrescriptionHistoryController_getPatient167_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptionHistory/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_getPatient167_invoker = createInvoker(
    PrescriptionHistoryController_0.get.getPatient(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "getPatient",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """prescriptionHistory/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:212
  private[this] lazy val controllers_v2_PrescriptionHistoryController_create168_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptionHistory")))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_create168_invoker = createInvoker(
    PrescriptionHistoryController_0.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """prescriptionHistory"""
    )
  )

  // @LINE:213
  private[this] lazy val controllers_v2_PrescriptionHistoryController_delete169_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptionHistory/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionHistoryController_delete169_invoker = createInvoker(
    PrescriptionHistoryController_0.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.PrescriptionHistoryController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """prescriptionHistory/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:216
  private[this] lazy val controllers_v3_ResourceCategoriesController_readAll170_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_readAll170_invoker = createInvoker(
    ResourceCategoriesController_9.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:217
  private[this] lazy val controllers_v3_ResourceCategoriesController_read171_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_read171_invoker = createInvoker(
    ResourceCategoriesController_9.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:218
  private[this] lazy val controllers_v3_ResourceCategoriesController_readSubCategories172_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/category/"), DynamicPart("categoryId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_readSubCategories172_invoker = createInvoker(
    ResourceCategoriesController_9.get.readSubCategories(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "readSubCategories",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/category/""" + "$" + """categoryId<[^/]+>"""
    )
  )

  // @LINE:219
  private[this] lazy val controllers_v3_ResourceCategoriesController_create173_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_create173_invoker = createInvoker(
    ResourceCategoriesController_9.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:220
  private[this] lazy val controllers_v3_ResourceCategoriesController_bulkUpdate174_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_bulkUpdate174_invoker = createInvoker(
    ResourceCategoriesController_9.get.bulkUpdate(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "bulkUpdate",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:221
  private[this] lazy val controllers_v3_ResourceCategoriesController_update175_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_update175_invoker = createInvoker(
    ResourceCategoriesController_9.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:222
  private[this] lazy val controllers_v3_ResourceCategoriesController_delete176_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceCategoriesController_delete176_invoker = createInvoker(
    ResourceCategoriesController_9.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceCategoriesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:224
  private[this] lazy val controllers_v2_EducationRequestController_readAll177_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests")))
  )
  private[this] lazy val controllers_v2_EducationRequestController_readAll177_invoker = createInvoker(
    EducationRequestController_20.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EducationRequestController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """educationRequests"""
    )
  )

  // @LINE:225
  private[this] lazy val controllers_v2_EducationRequestController_read178_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_read178_invoker = createInvoker(
    EducationRequestController_20.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EducationRequestController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:226
  private[this] lazy val controllers_v2_EducationRequestController_update179_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_update179_invoker = createInvoker(
    EducationRequestController_20.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EducationRequestController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:227
  private[this] lazy val controllers_v2_EducationRequestController_delete180_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_delete180_invoker = createInvoker(
    EducationRequestController_20.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EducationRequestController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:228
  private[this] lazy val controllers_v2_EducationRequestController_create181_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests")))
  )
  private[this] lazy val controllers_v2_EducationRequestController_create181_invoker = createInvoker(
    EducationRequestController_20.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v2.EducationRequestController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """educationRequests"""
    )
  )

  // @LINE:230
  private[this] lazy val controllers_v3_CartController_readAll182_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart")))
  )
  private[this] lazy val controllers_v3_CartController_readAll182_invoker = createInvoker(
    CartController_16.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CartController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """cart"""
    )
  )

  // @LINE:231
  private[this] lazy val controllers_v3_CartController_getCartWithProducts183_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cartWithProducts")))
  )
  private[this] lazy val controllers_v3_CartController_getCartWithProducts183_invoker = createInvoker(
    CartController_16.get.getCartWithProducts,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CartController",
      "getCartWithProducts",
      Nil,
      "GET",
      """""",
      this.prefix + """cartWithProducts"""
    )
  )

  // @LINE:233
  private[this] lazy val controllers_v3_CartController_update184_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart")))
  )
  private[this] lazy val controllers_v3_CartController_update184_invoker = createInvoker(
    CartController_16.get.update,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CartController",
      "update",
      Nil,
      "POST",
      """""",
      this.prefix + """cart"""
    )
  )

  // @LINE:234
  private[this] lazy val controllers_v3_CartController_delete185_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CartController_delete185_invoker = createInvoker(
    CartController_16.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.CartController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """cart/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:236
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_readAll186_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("categoryId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_readAll186_invoker = createInvoker(
    ResourceSubCategoriesController_32.get.readAll(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceSubCategoriesController",
      "readAll",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """categoryId<[^/]+>"""
    )
  )

  // @LINE:237
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_read187_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/subCategory/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_read187_invoker = createInvoker(
    ResourceSubCategoriesController_32.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceSubCategoriesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/subCategory/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:238
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_create188_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_create188_invoker = createInvoker(
    ResourceSubCategoriesController_32.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceSubCategoriesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:239
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_update189_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_update189_invoker = createInvoker(
    ResourceSubCategoriesController_32.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceSubCategoriesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:240
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_delete190_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_delete190_invoker = createInvoker(
    ResourceSubCategoriesController_32.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ResourceSubCategoriesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:242
  private[this] lazy val controllers_v3_ClinicalNotesController_readAll191_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("clinicalNotes/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_readAll191_invoker = createInvoker(
    ClinicalNotesController_30.get.readAll(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "readAll",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """clinicalNotes/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:243
  private[this] lazy val controllers_v3_ClinicalNotesController_create192_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("clinicalNotes/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_create192_invoker = createInvoker(
    ClinicalNotesController_30.get.create(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "create",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """clinicalNotes/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:244
  private[this] lazy val controllers_v3_ClinicalNotesController_update193_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("clinicalNotes/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_update193_invoker = createInvoker(
    ClinicalNotesController_30.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """clinicalNotes/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:245
  private[this] lazy val controllers_v3_ClinicalNotesController_delete194_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("clinicalNotes/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_delete194_invoker = createInvoker(
    ClinicalNotesController_30.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """clinicalNotes/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:246
  private[this] lazy val controllers_v3_ClinicalNotesController_updateLock195_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("updateLock/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_updateLock195_invoker = createInvoker(
    ClinicalNotesController_30.get.updateLock(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "updateLock",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """updateLock/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:247
  private[this] lazy val controllers_v3_ClinicalNotesController_clinicalNotesMigration196_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("clinicalNotesMigration")))
  )
  private[this] lazy val controllers_v3_ClinicalNotesController_clinicalNotesMigration196_invoker = createInvoker(
    ClinicalNotesController_30.get.clinicalNotesMigration,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ClinicalNotesController",
      "clinicalNotesMigration",
      Nil,
      "PUT",
      """""",
      this.prefix + """clinicalNotesMigration"""
    )
  )

  // @LINE:250
  private[this] lazy val controllers_v3_OperatoriesController_readAll197_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("operatories")))
  )
  private[this] lazy val controllers_v3_OperatoriesController_readAll197_invoker = createInvoker(
    OperatoriesController_18.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.OperatoriesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """operatories"""
    )
  )

  // @LINE:251
  private[this] lazy val controllers_v3_OperatoriesController_create198_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("operatories")))
  )
  private[this] lazy val controllers_v3_OperatoriesController_create198_invoker = createInvoker(
    OperatoriesController_18.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.OperatoriesController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """operatories"""
    )
  )

  // @LINE:252
  private[this] lazy val controllers_v3_OperatoriesController_update199_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("operatories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_OperatoriesController_update199_invoker = createInvoker(
    OperatoriesController_18.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.OperatoriesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """operatories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:253
  private[this] lazy val controllers_v3_OperatoriesController_delete200_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("operatories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_OperatoriesController_delete200_invoker = createInvoker(
    OperatoriesController_18.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.OperatoriesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """operatories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:255
  private[this] lazy val controllers_v3_ProvidersController_readAll201_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providers")))
  )
  private[this] lazy val controllers_v3_ProvidersController_readAll201_invoker = createInvoker(
    ProvidersController_24.get.readAll(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProvidersController",
      "readAll",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """providers"""
    )
  )

  // @LINE:256
  private[this] lazy val controllers_v3_ProvidersController_create202_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providers")))
  )
  private[this] lazy val controllers_v3_ProvidersController_create202_invoker = createInvoker(
    ProvidersController_24.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProvidersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """providers"""
    )
  )

  // @LINE:257
  private[this] lazy val controllers_v3_ProvidersController_update203_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProvidersController_update203_invoker = createInvoker(
    ProvidersController_24.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProvidersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """providers/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:258
  private[this] lazy val controllers_v3_ProvidersController_delete204_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProvidersController_delete204_invoker = createInvoker(
    ProvidersController_24.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProvidersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """providers/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:260
  private[this] lazy val controllers_v3_ProcedureController_companyReadAll205_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insuranceCompany")))
  )
  private[this] lazy val controllers_v3_ProcedureController_companyReadAll205_invoker = createInvoker(
    ProcedureController_22.get.companyReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "companyReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """insuranceCompany"""
    )
  )

  // @LINE:261
  private[this] lazy val controllers_v3_ProcedureController_feeReadAll206_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insuranceFee")))
  )
  private[this] lazy val controllers_v3_ProcedureController_feeReadAll206_invoker = createInvoker(
    ProcedureController_22.get.feeReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "feeReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """insuranceFee"""
    )
  )

  // @LINE:262
  private[this] lazy val controllers_v3_ProcedureController_updateInsuranceFee207_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("insuranceFee")))
  )
  private[this] lazy val controllers_v3_ProcedureController_updateInsuranceFee207_invoker = createInvoker(
    ProcedureController_22.get.updateInsuranceFee,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "updateInsuranceFee",
      Nil,
      "PUT",
      """""",
      this.prefix + """insuranceFee"""
    )
  )

  // @LINE:264
  private[this] lazy val controllers_v3_ProcedureController_readAll208_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("procedure")))
  )
  private[this] lazy val controllers_v3_ProcedureController_readAll208_invoker = createInvoker(
    ProcedureController_22.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """procedure"""
    )
  )

  // @LINE:265
  private[this] lazy val controllers_v3_ProcedureController_read209_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("procedure/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProcedureController_read209_invoker = createInvoker(
    ProcedureController_22.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """procedure/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:266
  private[this] lazy val controllers_v3_ProcedureController_create210_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("procedure")))
  )
  private[this] lazy val controllers_v3_ProcedureController_create210_invoker = createInvoker(
    ProcedureController_22.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """procedure"""
    )
  )

  // @LINE:267
  private[this] lazy val controllers_v3_ProcedureController_update211_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("procedure/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProcedureController_update211_invoker = createInvoker(
    ProcedureController_22.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """procedure/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:268
  private[this] lazy val controllers_v3_ProcedureController_delete212_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("procedure/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProcedureController_delete212_invoker = createInvoker(
    ProcedureController_22.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.ProcedureController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """procedure/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:270
  private[this] lazy val controllers_v3_NonWorkingDaysController_readAll213_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonWorkingDays")))
  )
  private[this] lazy val controllers_v3_NonWorkingDaysController_readAll213_invoker = createInvoker(
    NonWorkingDaysController_34.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.NonWorkingDaysController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """nonWorkingDays"""
    )
  )

  // @LINE:271
  private[this] lazy val controllers_v3_NonWorkingDaysController_create214_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonWorkingDays")))
  )
  private[this] lazy val controllers_v3_NonWorkingDaysController_create214_invoker = createInvoker(
    NonWorkingDaysController_34.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.NonWorkingDaysController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """nonWorkingDays"""
    )
  )

  // @LINE:272
  private[this] lazy val controllers_v3_NonWorkingDaysController_update215_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonWorkingDays/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_NonWorkingDaysController_update215_invoker = createInvoker(
    NonWorkingDaysController_34.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.NonWorkingDaysController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """nonWorkingDays/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:273
  private[this] lazy val controllers_v3_NonWorkingDaysController_delete216_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("nonWorkingDays/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_NonWorkingDaysController_delete216_invoker = createInvoker(
    NonWorkingDaysController_34.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.NonWorkingDaysController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """nonWorkingDays/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:275
  private[this] lazy val controllers_v3_WorkingHoursController_readAll217_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("workingHours")))
  )
  private[this] lazy val controllers_v3_WorkingHoursController_readAll217_invoker = createInvoker(
    WorkingHoursController_28.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.WorkingHoursController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """workingHours"""
    )
  )

  // @LINE:276
  private[this] lazy val controllers_v3_WorkingHoursController_create218_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("workingHours")))
  )
  private[this] lazy val controllers_v3_WorkingHoursController_create218_invoker = createInvoker(
    WorkingHoursController_28.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.WorkingHoursController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """workingHours"""
    )
  )

  // @LINE:277
  private[this] lazy val controllers_v3_WorkingHoursController_update219_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("workingHours/"), DynamicPart("practiceId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_WorkingHoursController_update219_invoker = createInvoker(
    WorkingHoursController_28.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.WorkingHoursController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """workingHours/""" + "$" + """practiceId<[^/]+>"""
    )
  )

  // @LINE:279
  private[this] lazy val controllers_v3_AppointmentController_appointmentReasonReadAll220_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointmentReason")))
  )
  private[this] lazy val controllers_v3_AppointmentController_appointmentReasonReadAll220_invoker = createInvoker(
    AppointmentController_25.get.appointmentReasonReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "appointmentReasonReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """appointmentReason"""
    )
  )

  // @LINE:280
  private[this] lazy val controllers_v3_AppointmentController_generatePdf221_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providerAppointmentPDF")))
  )
  private[this] lazy val controllers_v3_AppointmentController_generatePdf221_invoker = createInvoker(
    AppointmentController_25.get.generatePdf(fakeValue[Option[String]], fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "generatePdf",
      Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """providerAppointmentPDF"""
    )
  )

  // @LINE:281
  private[this] lazy val controllers_v3_AppointmentController_appointmentCSV222_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providerAppointmentCSV")))
  )
  private[this] lazy val controllers_v3_AppointmentController_appointmentCSV222_invoker = createInvoker(
    AppointmentController_25.get.appointmentCSV(fakeValue[Option[String]], fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "appointmentCSV",
      Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """providerAppointmentCSV"""
    )
  )

  // @LINE:282
  private[this] lazy val controllers_v3_AppointmentController_generateCancelReportPdf223_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cancelReportPDF")))
  )
  private[this] lazy val controllers_v3_AppointmentController_generateCancelReportPdf223_invoker = createInvoker(
    AppointmentController_25.get.generateCancelReportPdf(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "generateCancelReportPdf",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """cancelReportPDF"""
    )
  )

  // @LINE:283
  private[this] lazy val controllers_v3_AppointmentController_providerAppointmentList224_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providerAppointment")))
  )
  private[this] lazy val controllers_v3_AppointmentController_providerAppointmentList224_invoker = createInvoker(
    AppointmentController_25.get.providerAppointmentList(fakeValue[Option[String]], fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "providerAppointmentList",
      Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """providerAppointment"""
    )
  )

  // @LINE:284
  private[this] lazy val controllers_v3_AppointmentController_cancelReportList225_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cancelReport")))
  )
  private[this] lazy val controllers_v3_AppointmentController_cancelReportList225_invoker = createInvoker(
    AppointmentController_25.get.cancelReportList(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "cancelReportList",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """cancelReport"""
    )
  )

  // @LINE:285
  private[this] lazy val controllers_v3_AppointmentController_recallReportList226_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("recallReport")))
  )
  private[this] lazy val controllers_v3_AppointmentController_recallReportList226_invoker = createInvoker(
    AppointmentController_25.get.recallReportList(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "recallReportList",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """recallReport"""
    )
  )

  // @LINE:286
  private[this] lazy val controllers_v3_AppointmentController_generateCancelReportCsv227_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cancelReportCSV")))
  )
  private[this] lazy val controllers_v3_AppointmentController_generateCancelReportCsv227_invoker = createInvoker(
    AppointmentController_25.get.generateCancelReportCsv(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "generateCancelReportCsv",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """cancelReportCSV"""
    )
  )

  // @LINE:287
  private[this] lazy val controllers_v3_AppointmentController_availableOperatories228_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/availableOperatories")))
  )
  private[this] lazy val controllers_v3_AppointmentController_availableOperatories228_invoker = createInvoker(
    AppointmentController_25.get.availableOperatories(fakeValue[Option[String]], fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "availableOperatories",
      Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """appointment/availableOperatories"""
    )
  )

  // @LINE:288
  private[this] lazy val controllers_v3_AppointmentController_recallReportCSV229_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("recallReportCSV")))
  )
  private[this] lazy val controllers_v3_AppointmentController_recallReportCSV229_invoker = createInvoker(
    AppointmentController_25.get.recallReportCSV(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "recallReportCSV",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """recallReportCSV"""
    )
  )

  // @LINE:289
  private[this] lazy val controllers_v3_AppointmentController_recallReportPDF230_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("recallReportPDF")))
  )
  private[this] lazy val controllers_v3_AppointmentController_recallReportPDF230_invoker = createInvoker(
    AppointmentController_25.get.recallReportPDF(fakeValue[Option[String]], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "recallReportPDF",
      Seq(classOf[Option[String]], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """recallReportPDF"""
    )
  )

  // @LINE:290
  private[this] lazy val controllers_v3_AppointmentController_getProviders231_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providerFilter")))
  )
  private[this] lazy val controllers_v3_AppointmentController_getProviders231_invoker = createInvoker(
    AppointmentController_25.get.getProviders,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "getProviders",
      Nil,
      "GET",
      """""",
      this.prefix + """providerFilter"""
    )
  )

  // @LINE:291
  private[this] lazy val controllers_v3_AppointmentController_readAppointment232_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/readAppointment")))
  )
  private[this] lazy val controllers_v3_AppointmentController_readAppointment232_invoker = createInvoker(
    AppointmentController_25.get.readAppointment(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "readAppointment",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """appointment/readAppointment"""
    )
  )

  // @LINE:292
  private[this] lazy val controllers_v3_AppointmentController_dailyBrief233_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/dailyBrief")))
  )
  private[this] lazy val controllers_v3_AppointmentController_dailyBrief233_invoker = createInvoker(
    AppointmentController_25.get.dailyBrief(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "dailyBrief",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """appointment/dailyBrief"""
    )
  )

  // @LINE:293
  private[this] lazy val controllers_v3_AppointmentController_dailyBriefPdf234_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/dailyBriefPDF")))
  )
  private[this] lazy val controllers_v3_AppointmentController_dailyBriefPdf234_invoker = createInvoker(
    AppointmentController_25.get.dailyBriefPdf(fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "dailyBriefPdf",
      Seq(classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """appointment/dailyBriefPDF"""
    )
  )

  // @LINE:294
  private[this] lazy val controllers_v3_AppointmentController_patientAppointments235_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_AppointmentController_patientAppointments235_invoker = createInvoker(
    AppointmentController_25.get.patientAppointments(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "patientAppointments",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """appointment/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:295
  private[this] lazy val controllers_v3_AppointmentController_readAll236_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment")))
  )
  private[this] lazy val controllers_v3_AppointmentController_readAll236_invoker = createInvoker(
    AppointmentController_25.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """appointment"""
    )
  )

  // @LINE:296
  private[this] lazy val controllers_v3_AppointmentController_roleReadAll237_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("role")))
  )
  private[this] lazy val controllers_v3_AppointmentController_roleReadAll237_invoker = createInvoker(
    AppointmentController_25.get.roleReadAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "roleReadAll",
      Nil,
      "GET",
      """""",
      this.prefix + """role"""
    )
  )

  // @LINE:297
  private[this] lazy val controllers_v3_AppointmentController_migratePatientName238_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("migratePatientName")))
  )
  private[this] lazy val controllers_v3_AppointmentController_migratePatientName238_invoker = createInvoker(
    AppointmentController_25.get.migratePatientName,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "migratePatientName",
      Nil,
      "PUT",
      """""",
      this.prefix + """migratePatientName"""
    )
  )

  // @LINE:298
  private[this] lazy val controllers_v3_AppointmentController_checkAppointmentSetup239_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("checkAppointmentSetup")))
  )
  private[this] lazy val controllers_v3_AppointmentController_checkAppointmentSetup239_invoker = createInvoker(
    AppointmentController_25.get.checkAppointmentSetup,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "checkAppointmentSetup",
      Nil,
      "GET",
      """""",
      this.prefix + """checkAppointmentSetup"""
    )
  )

  // @LINE:299
  private[this] lazy val controllers_v3_AppointmentController_updateProviders240_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("providerFilter")))
  )
  private[this] lazy val controllers_v3_AppointmentController_updateProviders240_invoker = createInvoker(
    AppointmentController_25.get.updateProviders,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "updateProviders",
      Nil,
      "PUT",
      """""",
      this.prefix + """providerFilter"""
    )
  )

  // @LINE:300
  private[this] lazy val controllers_v3_AppointmentController_create241_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment")))
  )
  private[this] lazy val controllers_v3_AppointmentController_create241_invoker = createInvoker(
    AppointmentController_25.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """appointment"""
    )
  )

  // @LINE:301
  private[this] lazy val controllers_v3_AppointmentController_update242_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_AppointmentController_update242_invoker = createInvoker(
    AppointmentController_25.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """appointment/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:302
  private[this] lazy val controllers_v3_AppointmentController_delete243_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("appointment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_AppointmentController_delete243_invoker = createInvoker(
    AppointmentController_25.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """appointment/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:303
  private[this] lazy val controllers_v3_AppointmentController_listOptionsReadAll244_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("listOptionsReadAll/"), DynamicPart("listType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_AppointmentController_listOptionsReadAll244_invoker = createInvoker(
    AppointmentController_25.get.listOptionsReadAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.AppointmentController",
      "listOptionsReadAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """listOptionsReadAll/""" + "$" + """listType<[^/]+>"""
    )
  )

  // @LINE:305
  private[this] lazy val controllers_v3_TreatmentPlanController_readAll245_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlan/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanController_readAll245_invoker = createInvoker(
    TreatmentPlanController_3.get.readAll(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanController",
      "readAll",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatmentPlan/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:306
  private[this] lazy val controllers_v3_TreatmentPlanController_create246_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlan")))
  )
  private[this] lazy val controllers_v3_TreatmentPlanController_create246_invoker = createInvoker(
    TreatmentPlanController_3.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatmentPlan"""
    )
  )

  // @LINE:307
  private[this] lazy val controllers_v3_TreatmentPlanController_update247_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlan/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanController_update247_invoker = createInvoker(
    TreatmentPlanController_3.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatmentPlan/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:308
  private[this] lazy val controllers_v3_TreatmentPlanController_delete248_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlan/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanController_delete248_invoker = createInvoker(
    TreatmentPlanController_3.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatmentPlan/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:310
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_readAll249_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlanDetails/"), DynamicPart("planId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_readAll249_invoker = createInvoker(
    TreatmentPlanDetailsController_26.get.readAll(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanDetailsController",
      "readAll",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatmentPlanDetails/""" + "$" + """planId<[^/]+>"""
    )
  )

  // @LINE:311
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_create250_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlanDetails")))
  )
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_create250_invoker = createInvoker(
    TreatmentPlanDetailsController_26.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanDetailsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatmentPlanDetails"""
    )
  )

  // @LINE:312
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_update251_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlanDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_update251_invoker = createInvoker(
    TreatmentPlanDetailsController_26.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanDetailsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatmentPlanDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:313
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_delete252_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlanDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_delete252_invoker = createInvoker(
    TreatmentPlanDetailsController_26.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanDetailsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatmentPlanDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:314
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_generatePdf253_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatmentPlanDetails/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v3_TreatmentPlanDetailsController_generatePdf253_invoker = createInvoker(
    TreatmentPlanDetailsController_26.get.generatePdf(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.TreatmentPlanDetailsController",
      "generatePdf",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatmentPlanDetails/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:316
  private[this] lazy val controllers_v3_StaffMembersController_readAll254_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("staffMembers")))
  )
  private[this] lazy val controllers_v3_StaffMembersController_readAll254_invoker = createInvoker(
    StaffMembersController_31.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.StaffMembersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """staffMembers"""
    )
  )

  // @LINE:317
  private[this] lazy val controllers_v3_StaffMembersController_create255_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("staffMembers")))
  )
  private[this] lazy val controllers_v3_StaffMembersController_create255_invoker = createInvoker(
    StaffMembersController_31.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.StaffMembersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """staffMembers"""
    )
  )

  // @LINE:318
  private[this] lazy val controllers_v3_StaffMembersController_update256_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("staffMembers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_StaffMembersController_update256_invoker = createInvoker(
    StaffMembersController_31.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.StaffMembersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """staffMembers/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:319
  private[this] lazy val controllers_v3_StaffMembersController_delete257_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("staffMembers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_StaffMembersController_delete257_invoker = createInvoker(
    StaffMembersController_31.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.StaffMembersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """staffMembers/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:321
  private[this] lazy val controllers_v3_SharedPatientController_sharedList258_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sharedPatient/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_SharedPatientController_sharedList258_invoker = createInvoker(
    SharedPatientController_27.get.sharedList(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "sharedList",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """sharedPatient/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:322
  private[this] lazy val controllers_v3_SharedPatientController_shareToAdminList259_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shareToAdmin/"), DynamicPart("patientId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_SharedPatientController_shareToAdminList259_invoker = createInvoker(
    SharedPatientController_27.get.shareToAdminList(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "shareToAdminList",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """shareToAdmin/""" + "$" + """patientId<[^/]+>"""
    )
  )

  // @LINE:323
  private[this] lazy val controllers_v3_SharedPatientController_create260_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sharedPatient")))
  )
  private[this] lazy val controllers_v3_SharedPatientController_create260_invoker = createInvoker(
    SharedPatientController_27.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """sharedPatient"""
    )
  )

  // @LINE:324
  private[this] lazy val controllers_v3_SharedPatientController_update261_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sharedPatient/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_SharedPatientController_update261_invoker = createInvoker(
    SharedPatientController_27.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """sharedPatient/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:325
  private[this] lazy val controllers_v3_SharedPatientController_delete262_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sharedPatient/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_SharedPatientController_delete262_invoker = createInvoker(
    SharedPatientController_27.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """sharedPatient/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:326
  private[this] lazy val controllers_v3_SharedPatientController_getDoctors263_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getDoctorsList")))
  )
  private[this] lazy val controllers_v3_SharedPatientController_getDoctors263_invoker = createInvoker(
    SharedPatientController_27.get.getDoctors,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "getDoctors",
      Nil,
      "GET",
      """""",
      this.prefix + """getDoctorsList"""
    )
  )

  // @LINE:327
  private[this] lazy val controllers_v3_SharedPatientController_mainSquareList264_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("mainSquareList")))
  )
  private[this] lazy val controllers_v3_SharedPatientController_mainSquareList264_invoker = createInvoker(
    SharedPatientController_27.get.mainSquareList,
    HandlerDef(this.getClass.getClassLoader,
      "v4",
      "controllers.v3.SharedPatientController",
      "mainSquareList",
      Nil,
      "GET",
      """""",
      this.prefix + """mainSquareList"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_v1_HealthCheckController_check0_route(params) =>
      call { 
        controllers_v1_HealthCheckController_check0_invoker.call(HealthCheckController_35.get.check)
      }
  
    // @LINE:9
    case controllers_v1_SessionController_check1_route(params) =>
      call { 
        controllers_v1_SessionController_check1_invoker.call(SessionController_12.get.check)
      }
  
    // @LINE:10
    case controllers_v1_SessionController_logIn2_route(params) =>
      call { 
        controllers_v1_SessionController_logIn2_invoker.call(SessionController_12.get.logIn)
      }
  
    // @LINE:11
    case controllers_v1_SessionController_logOut3_route(params) =>
      call { 
        controllers_v1_SessionController_logOut3_invoker.call(SessionController_12.get.logOut)
      }
  
    // @LINE:12
    case controllers_v1_SessionController_signUpRequest4_route(params) =>
      call { 
        controllers_v1_SessionController_signUpRequest4_invoker.call(SessionController_12.get.signUpRequest)
      }
  
    // @LINE:14
    case controllers_v1_SessionController_getAuthorizeNetDetails5_route(params) =>
      call { 
        controllers_v1_SessionController_getAuthorizeNetDetails5_invoker.call(SessionController_12.get.getAuthorizeNetDetails)
      }
  
    // @LINE:16
    case controllers_v1_PasswordController_forgot6_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_PasswordController_forgot6_invoker.call(PasswordController_33.get.forgot(email))
      }
  
    // @LINE:17
    case controllers_v1_PasswordController_check7_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_check7_invoker.call(PasswordController_33.get.check(secret))
      }
  
    // @LINE:18
    case controllers_v1_PasswordController_reset8_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_reset8_invoker.call(PasswordController_33.get.reset(secret))
      }
  
    // @LINE:19
    case controllers_v1_PasswordController_change9_route(params) =>
      call { 
        controllers_v1_PasswordController_change9_invoker.call(PasswordController_33.get.change)
      }
  
    // @LINE:21
    case controllers_v3_AnalyticsController_read10_route(params) =>
      call { 
        controllers_v3_AnalyticsController_read10_invoker.call(AnalyticsController_11.get.read)
      }
  
    // @LINE:23
    case controllers_v2_CategoriesController_readAll11_route(params) =>
      call { 
        controllers_v2_CategoriesController_readAll11_invoker.call(CategoriesController_7.get.readAll)
      }
  
    // @LINE:24
    case controllers_v2_CategoriesController_bulkUpdate12_route(params) =>
      call { 
        controllers_v2_CategoriesController_bulkUpdate12_invoker.call(CategoriesController_7.get.bulkUpdate)
      }
  
    // @LINE:25
    case controllers_v2_CategoriesController_update13_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_update13_invoker.call(CategoriesController_7.get.update(id))
      }
  
    // @LINE:26
    case controllers_v2_CategoriesController_delete14_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_delete14_invoker.call(CategoriesController_7.get.delete(id))
      }
  
    // @LINE:28
    case controllers_v3_ProductsController_generateCSV15_route(params) =>
      call { 
        controllers_v3_ProductsController_generateCSV15_invoker.call(ProductsController_14.get.generateCSV)
      }
  
    // @LINE:29
    case controllers_v3_ProductsController_readAll16_route(params) =>
      call { 
        controllers_v3_ProductsController_readAll16_invoker.call(ProductsController_14.get.readAll)
      }
  
    // @LINE:30
    case controllers_v3_ProductsController_create17_route(params) =>
      call { 
        controllers_v3_ProductsController_create17_invoker.call(ProductsController_14.get.create)
      }
  
    // @LINE:31
    case controllers_v3_ProductsController_read18_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_read18_invoker.call(ProductsController_14.get.read(id))
      }
  
    // @LINE:32
    case controllers_v3_ProductsController_update19_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_update19_invoker.call(ProductsController_14.get.update(id))
      }
  
    // @LINE:33
    case controllers_v3_ProductsController_bulkUpdate20_route(params) =>
      call { 
        controllers_v3_ProductsController_bulkUpdate20_invoker.call(ProductsController_14.get.bulkUpdate)
      }
  
    // @LINE:34
    case controllers_v3_ProductsController_delete21_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_delete21_invoker.call(ProductsController_14.get.delete(id))
      }
  
    // @LINE:35
    case controllers_v3_ProductsController_generatePdf22_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_generatePdf22_invoker.call(ProductsController_14.get.generatePdf(id))
      }
  
    // @LINE:36
    case controllers_v3_ProductsController_findProducts23_route(params) =>
      call { 
        controllers_v3_ProductsController_findProducts23_invoker.call(ProductsController_14.get.findProducts)
      }
  
    // @LINE:37
    case controllers_v3_ProductsController_getCategories24_route(params) =>
      call { 
        controllers_v3_ProductsController_getCategories24_invoker.call(ProductsController_14.get.getCategories)
      }
  
    // @LINE:39
    case controllers_v2_FavouritesController_add25_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_add25_invoker.call(FavouritesController_13.get.add(id))
      }
  
    // @LINE:40
    case controllers_v2_FavouritesController_remove26_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_remove26_invoker.call(FavouritesController_13.get.remove(id))
      }
  
    // @LINE:42
    case controllers_v3_ProductsController_webService27_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_webService27_invoker.call(ProductsController_14.get.webService(id))
      }
  
    // @LINE:45
    case controllers_v2_VendorController_readAll28_route(params) =>
      call { 
        controllers_v2_VendorController_readAll28_invoker.call(VendorController_10.get.readAll)
      }
  
    // @LINE:46
    case controllers_v2_VendorController_create29_route(params) =>
      call { 
        controllers_v2_VendorController_create29_invoker.call(VendorController_10.get.create)
      }
  
    // @LINE:47
    case controllers_v2_VendorController_read30_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_read30_invoker.call(VendorController_10.get.read(id))
      }
  
    // @LINE:48
    case controllers_v2_VendorController_update31_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_update31_invoker.call(VendorController_10.get.update(id))
      }
  
    // @LINE:50
    case controllers_v2_VendorController_stateTaxReadAll32_route(params) =>
      call { 
        controllers_v2_VendorController_stateTaxReadAll32_invoker.call(VendorController_10.get.stateTaxReadAll)
      }
  
    // @LINE:51
    case controllers_v2_VendorController_stateTaxUpdate33_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_stateTaxUpdate33_invoker.call(VendorController_10.get.stateTaxUpdate(id))
      }
  
    // @LINE:52
    case controllers_v2_VendorController_findSaleTax34_route(params) =>
      call(params.fromPath[Long]("shippingId", None)) { (shippingId) =>
        controllers_v2_VendorController_findSaleTax34_invoker.call(VendorController_10.get.findSaleTax(shippingId))
      }
  
    // @LINE:54
    case controllers_v2_VendorController_saleTaxReadAll35_route(params) =>
      call(params.fromPath[Long]("vendorId", None)) { (vendorId) =>
        controllers_v2_VendorController_saleTaxReadAll35_invoker.call(VendorController_10.get.saleTaxReadAll(vendorId))
      }
  
    // @LINE:55
    case controllers_v2_VendorController_saleTaxCreate36_route(params) =>
      call { 
        controllers_v2_VendorController_saleTaxCreate36_invoker.call(VendorController_10.get.saleTaxCreate)
      }
  
    // @LINE:56
    case controllers_v2_VendorController_saleTaxUpdate37_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_saleTaxUpdate37_invoker.call(VendorController_10.get.saleTaxUpdate(id))
      }
  
    // @LINE:58
    case controllers_v2_VendorController_stateReadAll38_route(params) =>
      call { 
        controllers_v2_VendorController_stateReadAll38_invoker.call(VendorController_10.get.stateReadAll)
      }
  
    // @LINE:60
    case controllers_v2_VendorController_archive39_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_archive39_invoker.call(VendorController_10.get.archive(id))
      }
  
    // @LINE:61
    case controllers_v2_VendorController_unarchive40_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_unarchive40_invoker.call(VendorController_10.get.unarchive(id))
      }
  
    // @LINE:62
    case controllers_v2_VendorController_increasePrice41_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_increasePrice41_invoker.call(VendorController_10.get.increasePrice(id))
      }
  
    // @LINE:63
    case controllers_v2_VendorController_getPriceChangeHisotry42_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_getPriceChangeHisotry42_invoker.call(VendorController_10.get.getPriceChangeHisotry(id))
      }
  
    // @LINE:64
    case controllers_v2_VendorController_deleteAllProducts43_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_deleteAllProducts43_invoker.call(VendorController_10.get.deleteAllProducts(id))
      }
  
    // @LINE:66
    case controllers_v1_OrdersController_generateCSV44_route(params) =>
      call(params.fromQuery[Option[String]]("status", None)) { (status) =>
        controllers_v1_OrdersController_generateCSV44_invoker.call(OrdersController_8.get.generateCSV(status))
      }
  
    // @LINE:67
    case controllers_v1_OrdersController_readAll45_route(params) =>
      call { 
        controllers_v1_OrdersController_readAll45_invoker.call(OrdersController_8.get.readAll)
      }
  
    // @LINE:68
    case controllers_v1_OrdersController_catalogOrders46_route(params) =>
      call { 
        controllers_v1_OrdersController_catalogOrders46_invoker.call(OrdersController_8.get.catalogOrders)
      }
  
    // @LINE:69
    case controllers_v1_OrdersController_create47_route(params) =>
      call { 
        controllers_v1_OrdersController_create47_invoker.call(OrdersController_8.get.create)
      }
  
    // @LINE:70
    case controllers_v1_OrdersController_read48_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_read48_invoker.call(OrdersController_8.get.read(id))
      }
  
    // @LINE:71
    case controllers_v1_OrdersController_update49_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_update49_invoker.call(OrdersController_8.get.update(id))
      }
  
    // @LINE:72
    case controllers_v1_OrdersController_delete50_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_delete50_invoker.call(OrdersController_8.get.delete(id))
      }
  
    // @LINE:73
    case controllers_v1_OrdersController_updateGrandTotal51_route(params) =>
      call { 
        controllers_v1_OrdersController_updateGrandTotal51_invoker.call(OrdersController_8.get.updateGrandTotal)
      }
  
    // @LINE:74
    case controllers_v1_OrdersController_updateVendorPayment52_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_updateVendorPayment52_invoker.call(OrdersController_8.get.updateVendorPayment(id))
      }
  
    // @LINE:76
    case controllers_v1_TreatmentsController_generateReferralSourcePdf53_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_generateReferralSourcePdf53_invoker.call(TreatmentsController_6.get.generateReferralSourcePdf(fromDate, toDate))
      }
  
    // @LINE:77
    case controllers_v1_TreatmentsController_generateReferralSourceCsv54_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_generateReferralSourceCsv54_invoker.call(TreatmentsController_6.get.generateReferralSourceCsv(fromDate, toDate))
      }
  
    // @LINE:78
    case controllers_v1_TreatmentsController_referralSourceList55_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_referralSourceList55_invoker.call(TreatmentsController_6.get.referralSourceList(fromDate, toDate))
      }
  
    // @LINE:79
    case controllers_v1_TreatmentsController_readAll56_route(params) =>
      call { 
        controllers_v1_TreatmentsController_readAll56_invoker.call(TreatmentsController_6.get.readAll)
      }
  
    // @LINE:80
    case controllers_v1_TreatmentsController_create57_route(params) =>
      call { 
        controllers_v1_TreatmentsController_create57_invoker.call(TreatmentsController_6.get.create)
      }
  
    // @LINE:81
    case controllers_v1_TreatmentsController_patientList58_route(params) =>
      call { 
        controllers_v1_TreatmentsController_patientList58_invoker.call(TreatmentsController_6.get.patientList)
      }
  
    // @LINE:82
    case controllers_v1_TreatmentsController_read59_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_read59_invoker.call(TreatmentsController_6.get.read(id))
      }
  
    // @LINE:83
    case controllers_v1_TreatmentsController_mergeAll1B1C60_route(params) =>
      call { 
        controllers_v1_TreatmentsController_mergeAll1B1C60_invoker.call(TreatmentsController_6.get.mergeAll1B1C)
      }
  
    // @LINE:84
    case controllers_v1_TreatmentsController_mergeAll3B3C61_route(params) =>
      call { 
        controllers_v1_TreatmentsController_mergeAll3B3C61_invoker.call(TreatmentsController_6.get.mergeAll3B3C)
      }
  
    // @LINE:85
    case controllers_v1_TreatmentsController_mergeForms62_route(params) =>
      call(params.fromQuery[String]("sourceForm", None), params.fromQuery[String]("targetForm", None)) { (sourceForm, targetForm) =>
        controllers_v1_TreatmentsController_mergeForms62_invoker.call(TreatmentsController_6.get.mergeForms(sourceForm, targetForm))
      }
  
    // @LINE:86
    case controllers_v1_TreatmentsController_migrateTreatment63_route(params) =>
      call { 
        controllers_v1_TreatmentsController_migrateTreatment63_invoker.call(TreatmentsController_6.get.migrateTreatment)
      }
  
    // @LINE:87
    case controllers_v1_TreatmentsController_migrateMedicalCondtionSelected64_route(params) =>
      call { 
        controllers_v1_TreatmentsController_migrateMedicalCondtionSelected64_invoker.call(TreatmentsController_6.get.migrateMedicalCondtionSelected)
      }
  
    // @LINE:89
    case controllers_v1_TreatmentsController_getTreatmentAdditionalData65_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_getTreatmentAdditionalData65_invoker.call(TreatmentsController_6.get.getTreatmentAdditionalData(id))
      }
  
    // @LINE:90
    case controllers_v1_TreatmentsController_newPatientsList66_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_newPatientsList66_invoker.call(TreatmentsController_6.get.newPatientsList(fromDate, toDate))
      }
  
    // @LINE:92
    case controllers_v1_TreatmentsController_update67_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_update67_invoker.call(TreatmentsController_6.get.update(id))
      }
  
    // @LINE:93
    case controllers_v1_TreatmentsController_delete68_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_delete68_invoker.call(TreatmentsController_6.get.delete(id))
      }
  
    // @LINE:94
    case controllers_v1_TreatmentsController_pdf69_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Option[String]]("step", None)) { (id, step) =>
        controllers_v1_TreatmentsController_pdf69_invoker.call(TreatmentsController_6.get.pdf(id, step))
      }
  
    // @LINE:95
    case controllers_v1_TreatmentsController_testpdf70_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Option[String]]("step", None)) { (id, step) =>
        controllers_v1_TreatmentsController_testpdf70_invoker.call(TreatmentsController_6.get.testpdf(id, step))
      }
  
    // @LINE:96
    case controllers_v1_TreatmentsController_updateCtScan71_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateCtScan71_invoker.call(TreatmentsController_6.get.updateCtScan(id))
      }
  
    // @LINE:97
    case controllers_v1_TreatmentsController_updateContracts72_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateContracts72_invoker.call(TreatmentsController_6.get.updateContracts(id))
      }
  
    // @LINE:98
    case controllers_v1_TreatmentsController_recallPeriod73_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_recallPeriod73_invoker.call(TreatmentsController_6.get.recallPeriod(id))
      }
  
    // @LINE:99
    case controllers_v1_TreatmentsController_updateScratchPad74_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateScratchPad74_invoker.call(TreatmentsController_6.get.updateScratchPad(id))
      }
  
    // @LINE:100
    case controllers_v1_TreatmentsController_getMeasurementHisotry75_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_getMeasurementHisotry75_invoker.call(TreatmentsController_6.get.getMeasurementHisotry(id))
      }
  
    // @LINE:101
    case controllers_v1_TreatmentsController_updateToothChartData76_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateToothChartData76_invoker.call(TreatmentsController_6.get.updateToothChartData(id))
      }
  
    // @LINE:102
    case controllers_v1_TreatmentsController_getToothChartData77_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_getToothChartData77_invoker.call(TreatmentsController_6.get.getToothChartData(id))
      }
  
    // @LINE:103
    case controllers_v1_TreatmentsController_getPerioChartData78_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_getPerioChartData78_invoker.call(TreatmentsController_6.get.getPerioChartData(id))
      }
  
    // @LINE:104
    case controllers_v1_TreatmentsController_updatePerioChartData79_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updatePerioChartData79_invoker.call(TreatmentsController_6.get.updatePerioChartData(id))
      }
  
    // @LINE:105
    case controllers_v1_TreatmentsController_updateXray80_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateXray80_invoker.call(TreatmentsController_6.get.updateXray(id))
      }
  
    // @LINE:106
    case controllers_v1_TreatmentsController_updateToothChartDate81_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateToothChartDate81_invoker.call(TreatmentsController_6.get.updateToothChartDate(id))
      }
  
    // @LINE:107
    case controllers_v1_TreatmentsController_migrateAllMedicalCondtionLog82_route(params) =>
      call { 
        controllers_v1_TreatmentsController_migrateAllMedicalCondtionLog82_invoker.call(TreatmentsController_6.get.migrateAllMedicalCondtionLog)
      }
  
    // @LINE:108
    case controllers_v1_TreatmentsController_updatePerioChartDataFormat83_route(params) =>
      call { 
        controllers_v1_TreatmentsController_updatePerioChartDataFormat83_invoker.call(TreatmentsController_6.get.updatePerioChartDataFormat)
      }
  
    // @LINE:109
    case controllers_v1_TreatmentsController_generateNewPatientPDF84_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_generateNewPatientPDF84_invoker.call(TreatmentsController_6.get.generateNewPatientPDF(fromDate, toDate))
      }
  
    // @LINE:110
    case controllers_v1_TreatmentsController_generateNewPatientCSV85_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_generateNewPatientCSV85_invoker.call(TreatmentsController_6.get.generateNewPatientCSV(fromDate, toDate))
      }
  
    // @LINE:111
    case controllers_v1_TreatmentsController_patientTypePdf86_route(params) =>
      call(params.fromQuery[Option[String]]("patientType", None)) { (patientType) =>
        controllers_v1_TreatmentsController_patientTypePdf86_invoker.call(TreatmentsController_6.get.patientTypePdf(patientType))
      }
  
    // @LINE:112
    case controllers_v1_TreatmentsController_patientTypeCSV87_route(params) =>
      call(params.fromQuery[Option[String]]("patientType", None)) { (patientType) =>
        controllers_v1_TreatmentsController_patientTypeCSV87_invoker.call(TreatmentsController_6.get.patientTypeCSV(patientType))
      }
  
    // @LINE:113
    case controllers_v1_TreatmentsController_generatePatientsOutstandingBalancePdf88_route(params) =>
      call { 
        controllers_v1_TreatmentsController_generatePatientsOutstandingBalancePdf88_invoker.call(TreatmentsController_6.get.generatePatientsOutstandingBalancePdf)
      }
  
    // @LINE:114
    case controllers_v1_TreatmentsController_generatePatientsOutstandingBalanceCsv89_route(params) =>
      call { 
        controllers_v1_TreatmentsController_generatePatientsOutstandingBalanceCsv89_invoker.call(TreatmentsController_6.get.generatePatientsOutstandingBalanceCsv)
      }
  
    // @LINE:115
    case controllers_v1_TreatmentsController_patientsOutstandingBalanceList90_route(params) =>
      call { 
        controllers_v1_TreatmentsController_patientsOutstandingBalanceList90_invoker.call(TreatmentsController_6.get.patientsOutstandingBalanceList)
      }
  
    // @LINE:116
    case controllers_v1_TreatmentsController_outstandingInsurancePDF91_route(params) =>
      call { 
        controllers_v1_TreatmentsController_outstandingInsurancePDF91_invoker.call(TreatmentsController_6.get.outstandingInsurancePDF)
      }
  
    // @LINE:117
    case controllers_v1_TreatmentsController_outstandingInsuranceCSV92_route(params) =>
      call { 
        controllers_v1_TreatmentsController_outstandingInsuranceCSV92_invoker.call(TreatmentsController_6.get.outstandingInsuranceCSV)
      }
  
    // @LINE:118
    case controllers_v1_TreatmentsController_outstandingInsurance93_route(params) =>
      call { 
        controllers_v1_TreatmentsController_outstandingInsurance93_invoker.call(TreatmentsController_6.get.outstandingInsurance)
      }
  
    // @LINE:119
    case controllers_v1_TreatmentsController_totalCollectedAmountPDF94_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_totalCollectedAmountPDF94_invoker.call(TreatmentsController_6.get.totalCollectedAmountPDF(fromDate, toDate))
      }
  
    // @LINE:120
    case controllers_v1_TreatmentsController_totalCollectedAmountCSV95_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_totalCollectedAmountCSV95_invoker.call(TreatmentsController_6.get.totalCollectedAmountCSV(fromDate, toDate))
      }
  
    // @LINE:121
    case controllers_v1_TreatmentsController_totalCollectedAmount96_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v1_TreatmentsController_totalCollectedAmount96_invoker.call(TreatmentsController_6.get.totalCollectedAmount(fromDate, toDate))
      }
  
    // @LINE:122
    case controllers_v1_TreatmentsController_generatePatientsStatementPdf97_route(params) =>
      call(params.fromQuery[Option[Long]]("treatmentPlanId", None)) { (treatmentPlanId) =>
        controllers_v1_TreatmentsController_generatePatientsStatementPdf97_invoker.call(TreatmentsController_6.get.generatePatientsStatementPdf(treatmentPlanId))
      }
  
    // @LINE:123
    case controllers_v1_TreatmentsController_patientEstimatePDF98_route(params) =>
      call(params.fromQuery[Option[Long]]("treatmentPlanId", None)) { (treatmentPlanId) =>
        controllers_v1_TreatmentsController_patientEstimatePDF98_invoker.call(TreatmentsController_6.get.patientEstimatePDF(treatmentPlanId))
      }
  
    // @LINE:124
    case controllers_v1_TreatmentsController_patientTypeList99_route(params) =>
      call(params.fromQuery[Option[String]]("patientType", None)) { (patientType) =>
        controllers_v1_TreatmentsController_patientTypeList99_invoker.call(TreatmentsController_6.get.patientTypeList(patientType))
      }
  
    // @LINE:125
    case controllers_v1_TreatmentsController_shareToAdmin100_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v1_TreatmentsController_shareToAdmin100_invoker.call(TreatmentsController_6.get.shareToAdmin(patientId))
      }
  
    // @LINE:127
    case controllers_v1_PatientsController_readAll101_route(params) =>
      call { 
        controllers_v1_PatientsController_readAll101_invoker.call(PatientsController_4.get.readAll)
      }
  
    // @LINE:128
    case controllers_v1_PatientsController_read102_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_read102_invoker.call(PatientsController_4.get.read(id))
      }
  
    // @LINE:129
    case controllers_v1_PatientsController_generateHealthHistoryForm103_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_generateHealthHistoryForm103_invoker.call(PatientsController_4.get.generateHealthHistoryForm(id))
      }
  
    // @LINE:130
    case controllers_v1_PatientsController_generateConsentForm104_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_generateConsentForm104_invoker.call(PatientsController_4.get.generateConsentForm(id))
      }
  
    // @LINE:133
    case controllers_v1_ConsultationsController_readAll105_route(params) =>
      call { 
        controllers_v1_ConsultationsController_readAll105_invoker.call(ConsultationsController_15.get.readAll)
      }
  
    // @LINE:134
    case controllers_v1_ConsultationsController_create106_route(params) =>
      call { 
        controllers_v1_ConsultationsController_create106_invoker.call(ConsultationsController_15.get.create)
      }
  
    // @LINE:135
    case controllers_v1_ConsultationsController_read107_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_read107_invoker.call(ConsultationsController_15.get.read(id))
      }
  
    // @LINE:136
    case controllers_v1_ConsultationsController_update108_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_update108_invoker.call(ConsultationsController_15.get.update(id))
      }
  
    // @LINE:137
    case controllers_v1_ConsultationsController_delete109_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_delete109_invoker.call(ConsultationsController_15.get.delete(id))
      }
  
    // @LINE:139
    case controllers_v1_UsersController_readAll110_route(params) =>
      call { 
        controllers_v1_UsersController_readAll110_invoker.call(UsersController_1.get.readAll)
      }
  
    // @LINE:140
    case controllers_v1_UsersController_create111_route(params) =>
      call { 
        controllers_v1_UsersController_create111_invoker.call(UsersController_1.get.create)
      }
  
    // @LINE:141
    case controllers_v1_UsersController_read112_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_read112_invoker.call(UsersController_1.get.read(id))
      }
  
    // @LINE:142
    case controllers_v1_UsersController_update113_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_update113_invoker.call(UsersController_1.get.update(id))
      }
  
    // @LINE:143
    case controllers_v1_UsersController_delete114_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_delete114_invoker.call(UsersController_1.get.delete(id))
      }
  
    // @LINE:145
    case controllers_v1_StaffController_readAll115_route(params) =>
      call(params.fromQuery[Option[Long]]("practice", None)) { (practice) =>
        controllers_v1_StaffController_readAll115_invoker.call(StaffController_29.get.readAll(practice))
      }
  
    // @LINE:146
    case controllers_v1_StaffController_create116_route(params) =>
      call { 
        controllers_v1_StaffController_create116_invoker.call(StaffController_29.get.create)
      }
  
    // @LINE:147
    case controllers_v1_StaffController_read117_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_read117_invoker.call(StaffController_29.get.read(id))
      }
  
    // @LINE:148
    case controllers_v1_StaffController_update118_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_update118_invoker.call(StaffController_29.get.update(id))
      }
  
    // @LINE:149
    case controllers_v1_StaffController_updateAuthorizeCustId119_route(params) =>
      call { 
        controllers_v1_StaffController_updateAuthorizeCustId119_invoker.call(StaffController_29.get.updateAuthorizeCustId)
      }
  
    // @LINE:150
    case controllers_v1_StaffController_setUDID120_route(params) =>
      call { 
        controllers_v1_StaffController_setUDID120_invoker.call(StaffController_29.get.setUDID)
      }
  
    // @LINE:151
    case controllers_v1_StaffController_setUDID1121_route(params) =>
      call { 
        controllers_v1_StaffController_setUDID1121_invoker.call(StaffController_29.get.setUDID1)
      }
  
    // @LINE:152
    case controllers_v1_StaffController_delete122_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_delete122_invoker.call(StaffController_29.get.delete(id))
      }
  
    // @LINE:153
    case controllers_v1_StaffController_extendExpirationDate123_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_extendExpirationDate123_invoker.call(StaffController_29.get.extendExpirationDate(id))
      }
  
    // @LINE:154
    case controllers_v1_StaffController_extendExpirationDate1124_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_extendExpirationDate1124_invoker.call(StaffController_29.get.extendExpirationDate1(id))
      }
  
    // @LINE:155
    case controllers_v1_StaffController_clearUDID125_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_StaffController_clearUDID125_invoker.call(StaffController_29.get.clearUDID(email))
      }
  
    // @LINE:156
    case controllers_v1_StaffController_roleToSquareAccessMapping126_route(params) =>
      call { 
        controllers_v1_StaffController_roleToSquareAccessMapping126_invoker.call(StaffController_29.get.roleToSquareAccessMapping)
      }
  
    // @LINE:158
    case controllers_v1_StaffController_readAllRenewalDetails127_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_readAllRenewalDetails127_invoker.call(StaffController_29.get.readAllRenewalDetails(id))
      }
  
    // @LINE:159
    case controllers_v1_StaffController_updateRenewalDetails128_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_updateRenewalDetails128_invoker.call(StaffController_29.get.updateRenewalDetails(id))
      }
  
    // @LINE:161
    case controllers_v1_StaffController_bulkUpdate129_route(params) =>
      call { 
        controllers_v1_StaffController_bulkUpdate129_invoker.call(StaffController_29.get.bulkUpdate)
      }
  
    // @LINE:164
    case controllers_v2_ProfileController_read130_route(params) =>
      call { 
        controllers_v2_ProfileController_read130_invoker.call(ProfileController_21.get.read)
      }
  
    // @LINE:165
    case controllers_v2_ProfileController_update131_route(params) =>
      call { 
        controllers_v2_ProfileController_update131_invoker.call(ProfileController_21.get.update)
      }
  
    // @LINE:167
    case controllers_v1_PracticeController_readAll132_route(params) =>
      call { 
        controllers_v1_PracticeController_readAll132_invoker.call(PracticeController_36.get.readAll)
      }
  
    // @LINE:168
    case controllers_v1_PracticeController_create133_route(params) =>
      call { 
        controllers_v1_PracticeController_create133_invoker.call(PracticeController_36.get.create)
      }
  
    // @LINE:169
    case controllers_v1_PracticeController_read134_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_read134_invoker.call(PracticeController_36.get.read(id))
      }
  
    // @LINE:170
    case controllers_v1_PracticeController_update135_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_update135_invoker.call(PracticeController_36.get.update(id))
      }
  
    // @LINE:171
    case controllers_v1_PracticeController_delete136_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_delete136_invoker.call(PracticeController_36.get.delete(id))
      }
  
    // @LINE:173
    case controllers_v3_CoursesController_readAll137_route(params) =>
      call(params.fromPath[String]("resource", None)) { (resource) =>
        controllers_v3_CoursesController_readAll137_invoker.call(CoursesController_2.get.readAll(resource))
      }
  
    // @LINE:174
    case controllers_v3_CoursesController_read138_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_read138_invoker.call(CoursesController_2.get.read(resource, id))
      }
  
    // @LINE:175
    case controllers_v3_CoursesController_create139_route(params) =>
      call(params.fromPath[String]("resource", None)) { (resource) =>
        controllers_v3_CoursesController_create139_invoker.call(CoursesController_2.get.create(resource))
      }
  
    // @LINE:176
    case controllers_v3_CoursesController_update140_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_update140_invoker.call(CoursesController_2.get.update(resource, id))
      }
  
    // @LINE:177
    case controllers_v3_CoursesController_delete141_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_delete141_invoker.call(CoursesController_2.get.delete(resource, id))
      }
  
    // @LINE:179
    case controllers_v1_UploadsController_signature142_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_v1_UploadsController_signature142_invoker.call(UploadsController_17.get.signature(name))
      }
  
    // @LINE:181
    case controllers_v2_EmailMappingController_readAll143_route(params) =>
      call { 
        controllers_v2_EmailMappingController_readAll143_invoker.call(EmailMappingController_37.get.readAll)
      }
  
    // @LINE:182
    case controllers_v2_EmailMappingController_create144_route(params) =>
      call { 
        controllers_v2_EmailMappingController_create144_invoker.call(EmailMappingController_37.get.create)
      }
  
    // @LINE:183
    case controllers_v2_EmailMappingController_read145_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_read145_invoker.call(EmailMappingController_37.get.read(id))
      }
  
    // @LINE:184
    case controllers_v2_EmailMappingController_update146_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_update146_invoker.call(EmailMappingController_37.get.update(id))
      }
  
    // @LINE:185
    case controllers_v2_EmailMappingController_delete147_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_delete147_invoker.call(EmailMappingController_37.get.delete(id))
      }
  
    // @LINE:186
    case controllers_v2_EmailMappingController_addUser148_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_addUser148_invoker.call(EmailMappingController_37.get.addUser(id, userId))
      }
  
    // @LINE:187
    case controllers_v2_EmailMappingController_removeUser149_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_removeUser149_invoker.call(EmailMappingController_37.get.removeUser(id, userId))
      }
  
    // @LINE:189
    case controllers_v2_ShippingController_readAll150_route(params) =>
      call { 
        controllers_v2_ShippingController_readAll150_invoker.call(ShippingController_19.get.readAll)
      }
  
    // @LINE:190
    case controllers_v2_ShippingController_create151_route(params) =>
      call { 
        controllers_v2_ShippingController_create151_invoker.call(ShippingController_19.get.create)
      }
  
    // @LINE:191
    case controllers_v2_ShippingController_read152_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_read152_invoker.call(ShippingController_19.get.read(id))
      }
  
    // @LINE:192
    case controllers_v2_ShippingController_update153_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_update153_invoker.call(ShippingController_19.get.update(id))
      }
  
    // @LINE:193
    case controllers_v2_ShippingController_delete154_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_delete154_invoker.call(ShippingController_19.get.delete(id))
      }
  
    // @LINE:195
    case controllers_v2_FormsController_readForms155_route(params) =>
      call { 
        controllers_v2_FormsController_readForms155_invoker.call(FormsController_5.get.readForms)
      }
  
    // @LINE:196
    case controllers_v2_FormsController_readAllConsents156_route(params) =>
      call { 
        controllers_v2_FormsController_readAllConsents156_invoker.call(FormsController_5.get.readAllConsents)
      }
  
    // @LINE:197
    case controllers_v2_FormsController_updateConsent157_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_v2_FormsController_updateConsent157_invoker.call(FormsController_5.get.updateConsent(id))
      }
  
    // @LINE:198
    case controllers_v2_FormsController_addConsent158_route(params) =>
      call { 
        controllers_v2_FormsController_addConsent158_invoker.call(FormsController_5.get.addConsent)
      }
  
    // @LINE:200
    case controllers_v2_PrescriptionController_readAll159_route(params) =>
      call { 
        controllers_v2_PrescriptionController_readAll159_invoker.call(PrescriptionController_23.get.readAll)
      }
  
    // @LINE:201
    case controllers_v2_PrescriptionController_create160_route(params) =>
      call { 
        controllers_v2_PrescriptionController_create160_invoker.call(PrescriptionController_23.get.create)
      }
  
    // @LINE:202
    case controllers_v2_PrescriptionController_update161_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_PrescriptionController_update161_invoker.call(PrescriptionController_23.get.update(id))
      }
  
    // @LINE:203
    case controllers_v2_PrescriptionController_delete162_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_PrescriptionController_delete162_invoker.call(PrescriptionController_23.get.delete(id))
      }
  
    // @LINE:206
    case controllers_v2_PrescriptionHistoryController_generatePdf163_route(params) =>
      call(params.fromQuery[Option[String]]("patientId", None)) { (patientId) =>
        controllers_v2_PrescriptionHistoryController_generatePdf163_invoker.call(PrescriptionHistoryController_0.get.generatePdf(patientId))
      }
  
    // @LINE:207
    case controllers_v2_PrescriptionController_generatePdf164_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[String]("treatment", None), params.fromQuery[String]("signature", None), params.fromQuery[Option[String]]("timezone", None)) { (id, treatment, signature, timezone) =>
        controllers_v2_PrescriptionController_generatePdf164_invoker.call(PrescriptionController_23.get.generatePdf(id, treatment, signature, timezone))
      }
  
    // @LINE:208
    case controllers_v2_PrescriptionHistoryController_samplePDF165_route(params) =>
      call(params.fromQuery[Option[String]]("patientId", None)) { (patientId) =>
        controllers_v2_PrescriptionHistoryController_samplePDF165_invoker.call(PrescriptionHistoryController_0.get.samplePDF(patientId))
      }
  
    // @LINE:210
    case controllers_v2_PrescriptionHistoryController_readAll166_route(params) =>
      call { 
        controllers_v2_PrescriptionHistoryController_readAll166_invoker.call(PrescriptionHistoryController_0.get.readAll)
      }
  
    // @LINE:211
    case controllers_v2_PrescriptionHistoryController_getPatient167_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v2_PrescriptionHistoryController_getPatient167_invoker.call(PrescriptionHistoryController_0.get.getPatient(patientId))
      }
  
    // @LINE:212
    case controllers_v2_PrescriptionHistoryController_create168_route(params) =>
      call { 
        controllers_v2_PrescriptionHistoryController_create168_invoker.call(PrescriptionHistoryController_0.get.create)
      }
  
    // @LINE:213
    case controllers_v2_PrescriptionHistoryController_delete169_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_PrescriptionHistoryController_delete169_invoker.call(PrescriptionHistoryController_0.get.delete(id))
      }
  
    // @LINE:216
    case controllers_v3_ResourceCategoriesController_readAll170_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v3_ResourceCategoriesController_readAll170_invoker.call(ResourceCategoriesController_9.get.readAll(resourceType))
      }
  
    // @LINE:217
    case controllers_v3_ResourceCategoriesController_read171_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceCategoriesController_read171_invoker.call(ResourceCategoriesController_9.get.read(resourceType, id))
      }
  
    // @LINE:218
    case controllers_v3_ResourceCategoriesController_readSubCategories172_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("categoryId", None)) { (resourceType, categoryId) =>
        controllers_v3_ResourceCategoriesController_readSubCategories172_invoker.call(ResourceCategoriesController_9.get.readSubCategories(resourceType, categoryId))
      }
  
    // @LINE:219
    case controllers_v3_ResourceCategoriesController_create173_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v3_ResourceCategoriesController_create173_invoker.call(ResourceCategoriesController_9.get.create(resourceType))
      }
  
    // @LINE:220
    case controllers_v3_ResourceCategoriesController_bulkUpdate174_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v3_ResourceCategoriesController_bulkUpdate174_invoker.call(ResourceCategoriesController_9.get.bulkUpdate(resourceType))
      }
  
    // @LINE:221
    case controllers_v3_ResourceCategoriesController_update175_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceCategoriesController_update175_invoker.call(ResourceCategoriesController_9.get.update(resourceType, id))
      }
  
    // @LINE:222
    case controllers_v3_ResourceCategoriesController_delete176_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceCategoriesController_delete176_invoker.call(ResourceCategoriesController_9.get.delete(resourceType, id))
      }
  
    // @LINE:224
    case controllers_v2_EducationRequestController_readAll177_route(params) =>
      call { 
        controllers_v2_EducationRequestController_readAll177_invoker.call(EducationRequestController_20.get.readAll)
      }
  
    // @LINE:225
    case controllers_v2_EducationRequestController_read178_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_read178_invoker.call(EducationRequestController_20.get.read(id))
      }
  
    // @LINE:226
    case controllers_v2_EducationRequestController_update179_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_update179_invoker.call(EducationRequestController_20.get.update(id))
      }
  
    // @LINE:227
    case controllers_v2_EducationRequestController_delete180_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_delete180_invoker.call(EducationRequestController_20.get.delete(id))
      }
  
    // @LINE:228
    case controllers_v2_EducationRequestController_create181_route(params) =>
      call { 
        controllers_v2_EducationRequestController_create181_invoker.call(EducationRequestController_20.get.create)
      }
  
    // @LINE:230
    case controllers_v3_CartController_readAll182_route(params) =>
      call { 
        controllers_v3_CartController_readAll182_invoker.call(CartController_16.get.readAll)
      }
  
    // @LINE:231
    case controllers_v3_CartController_getCartWithProducts183_route(params) =>
      call { 
        controllers_v3_CartController_getCartWithProducts183_invoker.call(CartController_16.get.getCartWithProducts)
      }
  
    // @LINE:233
    case controllers_v3_CartController_update184_route(params) =>
      call { 
        controllers_v3_CartController_update184_invoker.call(CartController_16.get.update)
      }
  
    // @LINE:234
    case controllers_v3_CartController_delete185_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_CartController_delete185_invoker.call(CartController_16.get.delete(id))
      }
  
    // @LINE:236
    case controllers_v3_ResourceSubCategoriesController_readAll186_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("categoryId", None)) { (resourceType, categoryId) =>
        controllers_v3_ResourceSubCategoriesController_readAll186_invoker.call(ResourceSubCategoriesController_32.get.readAll(resourceType, categoryId))
      }
  
    // @LINE:237
    case controllers_v3_ResourceSubCategoriesController_read187_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_read187_invoker.call(ResourceSubCategoriesController_32.get.read(resourceType, id))
      }
  
    // @LINE:238
    case controllers_v3_ResourceSubCategoriesController_create188_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v3_ResourceSubCategoriesController_create188_invoker.call(ResourceSubCategoriesController_32.get.create(resourceType))
      }
  
    // @LINE:239
    case controllers_v3_ResourceSubCategoriesController_update189_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_update189_invoker.call(ResourceSubCategoriesController_32.get.update(resourceType, id))
      }
  
    // @LINE:240
    case controllers_v3_ResourceSubCategoriesController_delete190_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_delete190_invoker.call(ResourceSubCategoriesController_32.get.delete(resourceType, id))
      }
  
    // @LINE:242
    case controllers_v3_ClinicalNotesController_readAll191_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ClinicalNotesController_readAll191_invoker.call(ClinicalNotesController_30.get.readAll(id))
      }
  
    // @LINE:243
    case controllers_v3_ClinicalNotesController_create192_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ClinicalNotesController_create192_invoker.call(ClinicalNotesController_30.get.create(id))
      }
  
    // @LINE:244
    case controllers_v3_ClinicalNotesController_update193_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ClinicalNotesController_update193_invoker.call(ClinicalNotesController_30.get.update(id))
      }
  
    // @LINE:245
    case controllers_v3_ClinicalNotesController_delete194_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ClinicalNotesController_delete194_invoker.call(ClinicalNotesController_30.get.delete(id))
      }
  
    // @LINE:246
    case controllers_v3_ClinicalNotesController_updateLock195_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ClinicalNotesController_updateLock195_invoker.call(ClinicalNotesController_30.get.updateLock(id))
      }
  
    // @LINE:247
    case controllers_v3_ClinicalNotesController_clinicalNotesMigration196_route(params) =>
      call { 
        controllers_v3_ClinicalNotesController_clinicalNotesMigration196_invoker.call(ClinicalNotesController_30.get.clinicalNotesMigration)
      }
  
    // @LINE:250
    case controllers_v3_OperatoriesController_readAll197_route(params) =>
      call { 
        controllers_v3_OperatoriesController_readAll197_invoker.call(OperatoriesController_18.get.readAll)
      }
  
    // @LINE:251
    case controllers_v3_OperatoriesController_create198_route(params) =>
      call { 
        controllers_v3_OperatoriesController_create198_invoker.call(OperatoriesController_18.get.create)
      }
  
    // @LINE:252
    case controllers_v3_OperatoriesController_update199_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_OperatoriesController_update199_invoker.call(OperatoriesController_18.get.update(id))
      }
  
    // @LINE:253
    case controllers_v3_OperatoriesController_delete200_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_OperatoriesController_delete200_invoker.call(OperatoriesController_18.get.delete(id))
      }
  
    // @LINE:255
    case controllers_v3_ProvidersController_readAll201_route(params) =>
      call(params.fromQuery[Option[Long]]("practiceId", None)) { (practiceId) =>
        controllers_v3_ProvidersController_readAll201_invoker.call(ProvidersController_24.get.readAll(practiceId))
      }
  
    // @LINE:256
    case controllers_v3_ProvidersController_create202_route(params) =>
      call { 
        controllers_v3_ProvidersController_create202_invoker.call(ProvidersController_24.get.create)
      }
  
    // @LINE:257
    case controllers_v3_ProvidersController_update203_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProvidersController_update203_invoker.call(ProvidersController_24.get.update(id))
      }
  
    // @LINE:258
    case controllers_v3_ProvidersController_delete204_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProvidersController_delete204_invoker.call(ProvidersController_24.get.delete(id))
      }
  
    // @LINE:260
    case controllers_v3_ProcedureController_companyReadAll205_route(params) =>
      call { 
        controllers_v3_ProcedureController_companyReadAll205_invoker.call(ProcedureController_22.get.companyReadAll)
      }
  
    // @LINE:261
    case controllers_v3_ProcedureController_feeReadAll206_route(params) =>
      call { 
        controllers_v3_ProcedureController_feeReadAll206_invoker.call(ProcedureController_22.get.feeReadAll)
      }
  
    // @LINE:262
    case controllers_v3_ProcedureController_updateInsuranceFee207_route(params) =>
      call { 
        controllers_v3_ProcedureController_updateInsuranceFee207_invoker.call(ProcedureController_22.get.updateInsuranceFee)
      }
  
    // @LINE:264
    case controllers_v3_ProcedureController_readAll208_route(params) =>
      call { 
        controllers_v3_ProcedureController_readAll208_invoker.call(ProcedureController_22.get.readAll)
      }
  
    // @LINE:265
    case controllers_v3_ProcedureController_read209_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProcedureController_read209_invoker.call(ProcedureController_22.get.read(id))
      }
  
    // @LINE:266
    case controllers_v3_ProcedureController_create210_route(params) =>
      call { 
        controllers_v3_ProcedureController_create210_invoker.call(ProcedureController_22.get.create)
      }
  
    // @LINE:267
    case controllers_v3_ProcedureController_update211_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProcedureController_update211_invoker.call(ProcedureController_22.get.update(id))
      }
  
    // @LINE:268
    case controllers_v3_ProcedureController_delete212_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProcedureController_delete212_invoker.call(ProcedureController_22.get.delete(id))
      }
  
    // @LINE:270
    case controllers_v3_NonWorkingDaysController_readAll213_route(params) =>
      call { 
        controllers_v3_NonWorkingDaysController_readAll213_invoker.call(NonWorkingDaysController_34.get.readAll)
      }
  
    // @LINE:271
    case controllers_v3_NonWorkingDaysController_create214_route(params) =>
      call { 
        controllers_v3_NonWorkingDaysController_create214_invoker.call(NonWorkingDaysController_34.get.create)
      }
  
    // @LINE:272
    case controllers_v3_NonWorkingDaysController_update215_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_NonWorkingDaysController_update215_invoker.call(NonWorkingDaysController_34.get.update(id))
      }
  
    // @LINE:273
    case controllers_v3_NonWorkingDaysController_delete216_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_NonWorkingDaysController_delete216_invoker.call(NonWorkingDaysController_34.get.delete(id))
      }
  
    // @LINE:275
    case controllers_v3_WorkingHoursController_readAll217_route(params) =>
      call { 
        controllers_v3_WorkingHoursController_readAll217_invoker.call(WorkingHoursController_28.get.readAll)
      }
  
    // @LINE:276
    case controllers_v3_WorkingHoursController_create218_route(params) =>
      call { 
        controllers_v3_WorkingHoursController_create218_invoker.call(WorkingHoursController_28.get.create)
      }
  
    // @LINE:277
    case controllers_v3_WorkingHoursController_update219_route(params) =>
      call(params.fromPath[Long]("practiceId", None)) { (practiceId) =>
        controllers_v3_WorkingHoursController_update219_invoker.call(WorkingHoursController_28.get.update(practiceId))
      }
  
    // @LINE:279
    case controllers_v3_AppointmentController_appointmentReasonReadAll220_route(params) =>
      call { 
        controllers_v3_AppointmentController_appointmentReasonReadAll220_invoker.call(AppointmentController_25.get.appointmentReasonReadAll)
      }
  
    // @LINE:280
    case controllers_v3_AppointmentController_generatePdf221_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None), params.fromQuery[Option[String]]("providerId", None)) { (fromDate, toDate, providerId) =>
        controllers_v3_AppointmentController_generatePdf221_invoker.call(AppointmentController_25.get.generatePdf(fromDate, toDate, providerId))
      }
  
    // @LINE:281
    case controllers_v3_AppointmentController_appointmentCSV222_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None), params.fromQuery[Option[String]]("providerId", None)) { (fromDate, toDate, providerId) =>
        controllers_v3_AppointmentController_appointmentCSV222_invoker.call(AppointmentController_25.get.appointmentCSV(fromDate, toDate, providerId))
      }
  
    // @LINE:282
    case controllers_v3_AppointmentController_generateCancelReportPdf223_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_generateCancelReportPdf223_invoker.call(AppointmentController_25.get.generateCancelReportPdf(fromDate, toDate))
      }
  
    // @LINE:283
    case controllers_v3_AppointmentController_providerAppointmentList224_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None), params.fromQuery[Option[String]]("providerId", None)) { (fromDate, toDate, providerId) =>
        controllers_v3_AppointmentController_providerAppointmentList224_invoker.call(AppointmentController_25.get.providerAppointmentList(fromDate, toDate, providerId))
      }
  
    // @LINE:284
    case controllers_v3_AppointmentController_cancelReportList225_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_cancelReportList225_invoker.call(AppointmentController_25.get.cancelReportList(fromDate, toDate))
      }
  
    // @LINE:285
    case controllers_v3_AppointmentController_recallReportList226_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_recallReportList226_invoker.call(AppointmentController_25.get.recallReportList(fromDate, toDate))
      }
  
    // @LINE:286
    case controllers_v3_AppointmentController_generateCancelReportCsv227_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_generateCancelReportCsv227_invoker.call(AppointmentController_25.get.generateCancelReportCsv(fromDate, toDate))
      }
  
    // @LINE:287
    case controllers_v3_AppointmentController_availableOperatories228_route(params) =>
      call(params.fromQuery[Option[String]]("date", None), params.fromQuery[Option[String]]("startTime", None), params.fromQuery[Option[String]]("endTime", None)) { (date, startTime, endTime) =>
        controllers_v3_AppointmentController_availableOperatories228_invoker.call(AppointmentController_25.get.availableOperatories(date, startTime, endTime))
      }
  
    // @LINE:288
    case controllers_v3_AppointmentController_recallReportCSV229_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_recallReportCSV229_invoker.call(AppointmentController_25.get.recallReportCSV(fromDate, toDate))
      }
  
    // @LINE:289
    case controllers_v3_AppointmentController_recallReportPDF230_route(params) =>
      call(params.fromQuery[Option[String]]("fromDate", None), params.fromQuery[Option[String]]("toDate", None)) { (fromDate, toDate) =>
        controllers_v3_AppointmentController_recallReportPDF230_invoker.call(AppointmentController_25.get.recallReportPDF(fromDate, toDate))
      }
  
    // @LINE:290
    case controllers_v3_AppointmentController_getProviders231_route(params) =>
      call { 
        controllers_v3_AppointmentController_getProviders231_invoker.call(AppointmentController_25.get.getProviders)
      }
  
    // @LINE:291
    case controllers_v3_AppointmentController_readAppointment232_route(params) =>
      call(params.fromQuery[Option[String]]("date", None)) { (date) =>
        controllers_v3_AppointmentController_readAppointment232_invoker.call(AppointmentController_25.get.readAppointment(date))
      }
  
    // @LINE:292
    case controllers_v3_AppointmentController_dailyBrief233_route(params) =>
      call(params.fromQuery[Option[String]]("date", None)) { (date) =>
        controllers_v3_AppointmentController_dailyBrief233_invoker.call(AppointmentController_25.get.dailyBrief(date))
      }
  
    // @LINE:293
    case controllers_v3_AppointmentController_dailyBriefPdf234_route(params) =>
      call(params.fromQuery[Option[String]]("date", None)) { (date) =>
        controllers_v3_AppointmentController_dailyBriefPdf234_invoker.call(AppointmentController_25.get.dailyBriefPdf(date))
      }
  
    // @LINE:294
    case controllers_v3_AppointmentController_patientAppointments235_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v3_AppointmentController_patientAppointments235_invoker.call(AppointmentController_25.get.patientAppointments(patientId))
      }
  
    // @LINE:295
    case controllers_v3_AppointmentController_readAll236_route(params) =>
      call { 
        controllers_v3_AppointmentController_readAll236_invoker.call(AppointmentController_25.get.readAll)
      }
  
    // @LINE:296
    case controllers_v3_AppointmentController_roleReadAll237_route(params) =>
      call { 
        controllers_v3_AppointmentController_roleReadAll237_invoker.call(AppointmentController_25.get.roleReadAll)
      }
  
    // @LINE:297
    case controllers_v3_AppointmentController_migratePatientName238_route(params) =>
      call { 
        controllers_v3_AppointmentController_migratePatientName238_invoker.call(AppointmentController_25.get.migratePatientName)
      }
  
    // @LINE:298
    case controllers_v3_AppointmentController_checkAppointmentSetup239_route(params) =>
      call { 
        controllers_v3_AppointmentController_checkAppointmentSetup239_invoker.call(AppointmentController_25.get.checkAppointmentSetup)
      }
  
    // @LINE:299
    case controllers_v3_AppointmentController_updateProviders240_route(params) =>
      call { 
        controllers_v3_AppointmentController_updateProviders240_invoker.call(AppointmentController_25.get.updateProviders)
      }
  
    // @LINE:300
    case controllers_v3_AppointmentController_create241_route(params) =>
      call { 
        controllers_v3_AppointmentController_create241_invoker.call(AppointmentController_25.get.create)
      }
  
    // @LINE:301
    case controllers_v3_AppointmentController_update242_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_AppointmentController_update242_invoker.call(AppointmentController_25.get.update(id))
      }
  
    // @LINE:302
    case controllers_v3_AppointmentController_delete243_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_AppointmentController_delete243_invoker.call(AppointmentController_25.get.delete(id))
      }
  
    // @LINE:303
    case controllers_v3_AppointmentController_listOptionsReadAll244_route(params) =>
      call(params.fromPath[String]("listType", None)) { (listType) =>
        controllers_v3_AppointmentController_listOptionsReadAll244_invoker.call(AppointmentController_25.get.listOptionsReadAll(listType))
      }
  
    // @LINE:305
    case controllers_v3_TreatmentPlanController_readAll245_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v3_TreatmentPlanController_readAll245_invoker.call(TreatmentPlanController_3.get.readAll(patientId))
      }
  
    // @LINE:306
    case controllers_v3_TreatmentPlanController_create246_route(params) =>
      call { 
        controllers_v3_TreatmentPlanController_create246_invoker.call(TreatmentPlanController_3.get.create)
      }
  
    // @LINE:307
    case controllers_v3_TreatmentPlanController_update247_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_TreatmentPlanController_update247_invoker.call(TreatmentPlanController_3.get.update(id))
      }
  
    // @LINE:308
    case controllers_v3_TreatmentPlanController_delete248_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_TreatmentPlanController_delete248_invoker.call(TreatmentPlanController_3.get.delete(id))
      }
  
    // @LINE:310
    case controllers_v3_TreatmentPlanDetailsController_readAll249_route(params) =>
      call(params.fromPath[Long]("planId", None)) { (planId) =>
        controllers_v3_TreatmentPlanDetailsController_readAll249_invoker.call(TreatmentPlanDetailsController_26.get.readAll(planId))
      }
  
    // @LINE:311
    case controllers_v3_TreatmentPlanDetailsController_create250_route(params) =>
      call { 
        controllers_v3_TreatmentPlanDetailsController_create250_invoker.call(TreatmentPlanDetailsController_26.get.create)
      }
  
    // @LINE:312
    case controllers_v3_TreatmentPlanDetailsController_update251_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_TreatmentPlanDetailsController_update251_invoker.call(TreatmentPlanDetailsController_26.get.update(id))
      }
  
    // @LINE:313
    case controllers_v3_TreatmentPlanDetailsController_delete252_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_TreatmentPlanDetailsController_delete252_invoker.call(TreatmentPlanDetailsController_26.get.delete(id))
      }
  
    // @LINE:314
    case controllers_v3_TreatmentPlanDetailsController_generatePdf253_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_TreatmentPlanDetailsController_generatePdf253_invoker.call(TreatmentPlanDetailsController_26.get.generatePdf(id))
      }
  
    // @LINE:316
    case controllers_v3_StaffMembersController_readAll254_route(params) =>
      call { 
        controllers_v3_StaffMembersController_readAll254_invoker.call(StaffMembersController_31.get.readAll)
      }
  
    // @LINE:317
    case controllers_v3_StaffMembersController_create255_route(params) =>
      call { 
        controllers_v3_StaffMembersController_create255_invoker.call(StaffMembersController_31.get.create)
      }
  
    // @LINE:318
    case controllers_v3_StaffMembersController_update256_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_StaffMembersController_update256_invoker.call(StaffMembersController_31.get.update(id))
      }
  
    // @LINE:319
    case controllers_v3_StaffMembersController_delete257_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_StaffMembersController_delete257_invoker.call(StaffMembersController_31.get.delete(id))
      }
  
    // @LINE:321
    case controllers_v3_SharedPatientController_sharedList258_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v3_SharedPatientController_sharedList258_invoker.call(SharedPatientController_27.get.sharedList(patientId))
      }
  
    // @LINE:322
    case controllers_v3_SharedPatientController_shareToAdminList259_route(params) =>
      call(params.fromPath[Long]("patientId", None)) { (patientId) =>
        controllers_v3_SharedPatientController_shareToAdminList259_invoker.call(SharedPatientController_27.get.shareToAdminList(patientId))
      }
  
    // @LINE:323
    case controllers_v3_SharedPatientController_create260_route(params) =>
      call { 
        controllers_v3_SharedPatientController_create260_invoker.call(SharedPatientController_27.get.create)
      }
  
    // @LINE:324
    case controllers_v3_SharedPatientController_update261_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_SharedPatientController_update261_invoker.call(SharedPatientController_27.get.update(id))
      }
  
    // @LINE:325
    case controllers_v3_SharedPatientController_delete262_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_SharedPatientController_delete262_invoker.call(SharedPatientController_27.get.delete(id))
      }
  
    // @LINE:326
    case controllers_v3_SharedPatientController_getDoctors263_route(params) =>
      call { 
        controllers_v3_SharedPatientController_getDoctors263_invoker.call(SharedPatientController_27.get.getDoctors)
      }
  
    // @LINE:327
    case controllers_v3_SharedPatientController_mainSquareList264_route(params) =>
      call { 
        controllers_v3_SharedPatientController_mainSquareList264_invoker.call(SharedPatientController_27.get.mainSquareList)
      }
  }
}
