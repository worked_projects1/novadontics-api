
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/v3.routes
// @DATE:Fri Jun 19 20:14:07 IST 2020

package v3

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HealthCheckController_23: javax.inject.Provider[controllers.v1.HealthCheckController],
  // @LINE:9
  SessionController_9: javax.inject.Provider[controllers.v1.SessionController],
  // @LINE:15
  PasswordController_22: javax.inject.Provider[controllers.v1.PasswordController],
  // @LINE:20
  AnalyticsController_8: javax.inject.Provider[controllers.v3.AnalyticsController],
  // @LINE:22
  CategoriesController_5: javax.inject.Provider[controllers.v2.CategoriesController],
  // @LINE:27
  ProductsController_11: javax.inject.Provider[controllers.v3.ProductsController],
  // @LINE:35
  FavouritesController_10: javax.inject.Provider[controllers.v2.FavouritesController],
  // @LINE:38
  VendorController_7: javax.inject.Provider[controllers.v2.VendorController],
  // @LINE:45
  OrdersController_6: javax.inject.Provider[controllers.v1.OrdersController],
  // @LINE:52
  TreatmentsController_4: javax.inject.Provider[controllers.v1.TreatmentsController],
  // @LINE:60
  PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
  // @LINE:63
  ConsultationsController_12: javax.inject.Provider[controllers.v1.ConsultationsController],
  // @LINE:69
  UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
  // @LINE:75
  StaffController_20: javax.inject.Provider[controllers.v1.StaffController],
  // @LINE:88
  ProfileController_17: javax.inject.Provider[controllers.v2.ProfileController],
  // @LINE:91
  PracticeController_24: javax.inject.Provider[controllers.v1.PracticeController],
  // @LINE:97
  CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
  // @LINE:103
  UploadsController_14: javax.inject.Provider[controllers.v1.UploadsController],
  // @LINE:105
  EmailMappingController_25: javax.inject.Provider[controllers.v2.EmailMappingController],
  // @LINE:113
  ShippingController_15: javax.inject.Provider[controllers.v2.ShippingController],
  // @LINE:119
  FormsController_3: javax.inject.Provider[controllers.v2.FormsController],
  // @LINE:124
  PrescriptionController_18: javax.inject.Provider[controllers.v2.PrescriptionController],
  // @LINE:130
  ResourceCategoriesController_19: javax.inject.Provider[controllers.v2.ResourceCategoriesController],
  // @LINE:139
  EducationRequestController_16: javax.inject.Provider[controllers.v2.EducationRequestController],
  // @LINE:145
  CartController_13: javax.inject.Provider[controllers.v3.CartController],
  // @LINE:149
  ResourceSubCategoriesController_21: javax.inject.Provider[controllers.v3.ResourceSubCategoriesController],
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HealthCheckController_23: javax.inject.Provider[controllers.v1.HealthCheckController],
    // @LINE:9
    SessionController_9: javax.inject.Provider[controllers.v1.SessionController],
    // @LINE:15
    PasswordController_22: javax.inject.Provider[controllers.v1.PasswordController],
    // @LINE:20
    AnalyticsController_8: javax.inject.Provider[controllers.v3.AnalyticsController],
    // @LINE:22
    CategoriesController_5: javax.inject.Provider[controllers.v2.CategoriesController],
    // @LINE:27
    ProductsController_11: javax.inject.Provider[controllers.v3.ProductsController],
    // @LINE:35
    FavouritesController_10: javax.inject.Provider[controllers.v2.FavouritesController],
    // @LINE:38
    VendorController_7: javax.inject.Provider[controllers.v2.VendorController],
    // @LINE:45
    OrdersController_6: javax.inject.Provider[controllers.v1.OrdersController],
    // @LINE:52
    TreatmentsController_4: javax.inject.Provider[controllers.v1.TreatmentsController],
    // @LINE:60
    PatientsController_2: javax.inject.Provider[controllers.v1.PatientsController],
    // @LINE:63
    ConsultationsController_12: javax.inject.Provider[controllers.v1.ConsultationsController],
    // @LINE:69
    UsersController_0: javax.inject.Provider[controllers.v1.UsersController],
    // @LINE:75
    StaffController_20: javax.inject.Provider[controllers.v1.StaffController],
    // @LINE:88
    ProfileController_17: javax.inject.Provider[controllers.v2.ProfileController],
    // @LINE:91
    PracticeController_24: javax.inject.Provider[controllers.v1.PracticeController],
    // @LINE:97
    CoursesController_1: javax.inject.Provider[controllers.v3.CoursesController],
    // @LINE:103
    UploadsController_14: javax.inject.Provider[controllers.v1.UploadsController],
    // @LINE:105
    EmailMappingController_25: javax.inject.Provider[controllers.v2.EmailMappingController],
    // @LINE:113
    ShippingController_15: javax.inject.Provider[controllers.v2.ShippingController],
    // @LINE:119
    FormsController_3: javax.inject.Provider[controllers.v2.FormsController],
    // @LINE:124
    PrescriptionController_18: javax.inject.Provider[controllers.v2.PrescriptionController],
    // @LINE:130
    ResourceCategoriesController_19: javax.inject.Provider[controllers.v2.ResourceCategoriesController],
    // @LINE:139
    EducationRequestController_16: javax.inject.Provider[controllers.v2.EducationRequestController],
    // @LINE:145
    CartController_13: javax.inject.Provider[controllers.v3.CartController],
    // @LINE:149
    ResourceSubCategoriesController_21: javax.inject.Provider[controllers.v3.ResourceSubCategoriesController]
  ) = this(errorHandler, HealthCheckController_23, SessionController_9, PasswordController_22, AnalyticsController_8, CategoriesController_5, ProductsController_11, FavouritesController_10, VendorController_7, OrdersController_6, TreatmentsController_4, PatientsController_2, ConsultationsController_12, UsersController_0, StaffController_20, ProfileController_17, PracticeController_24, CoursesController_1, UploadsController_14, EmailMappingController_25, ShippingController_15, FormsController_3, PrescriptionController_18, ResourceCategoriesController_19, EducationRequestController_16, CartController_13, ResourceSubCategoriesController_21, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    v3.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HealthCheckController_23, SessionController_9, PasswordController_22, AnalyticsController_8, CategoriesController_5, ProductsController_11, FavouritesController_10, VendorController_7, OrdersController_6, TreatmentsController_4, PatientsController_2, ConsultationsController_12, UsersController_0, StaffController_20, ProfileController_17, PracticeController_24, CoursesController_1, UploadsController_14, EmailMappingController_25, ShippingController_15, FormsController_3, PrescriptionController_18, ResourceCategoriesController_19, EducationRequestController_16, CartController_13, ResourceSubCategoriesController_21, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """healthcheck""", """@controllers.v1.HealthCheckController@.check"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.check"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logIn"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session""", """@controllers.v1.SessionController@.logOut"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """session/signUpRequest""", """@controllers.v1.SessionController@.signUpRequest"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forgot/""" + "$" + """email<[^/]+>""", """@controllers.v1.PasswordController@.forgot(email:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.check(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reset/""" + "$" + """secret<[^/]+>""", """@controllers.v1.PasswordController@.reset(secret:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """password""", """@controllers.v1.PasswordController@.change"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """analytics""", """@controllers.v3.AnalyticsController@.read"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.readAll"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories""", """@controllers.v2.CategoriesController@.bulkUpdate"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/""" + "$" + """id<[^/]+>""", """@controllers.v2.CategoriesController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.update(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """@controllers.v3.ProductsController@.bulkUpdate"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """@controllers.v3.ProductsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/generatePdf/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v3.ProductsController@.generatePdf(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.add(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>/favourite""", """@controllers.v2.FavouritesController@.remove(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors""", """@controllers.v2.VendorController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>""", """@controllers.v2.VendorController@.archive(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """vendors/""" + "$" + """id<[^/]+>/unarchive""", """@controllers.v2.VendorController@.unarchive(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders""", """@controllers.v1.OrdersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/updateVendorPayment/""" + "$" + """id<[^/]+>""", """@controllers.v1.OrdersController@.updateVendorPayment(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments""", """@controllers.v1.TreatmentsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v1.TreatmentsController@.pdf(id:Long, step:Option[String])"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """treatments/updateCtScan/""" + "$" + """id<[^/]+>""", """@controllers.v1.TreatmentsController@.updateCtScan(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients""", """@controllers.v1.PatientsController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """patients/""" + "$" + """id<[^/]+>""", """@controllers.v1.PatientsController@.read(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations""", """@controllers.v1.ConsultationsController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consultations/""" + "$" + """id<[^/]+>""", """@controllers.v1.ConsultationsController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """@controllers.v1.UsersController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>""", """@controllers.v1.UsersController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.readAll(practice:Option[Long])"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts""", """@controllers.v1.StaffController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.update(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/udid""", """@controllers.v1.StaffController@.setUDID"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>/extend""", """@controllers.v1.StaffController@.extendExpirationDate(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/""" + "$" + """id<[^/]+>/extend1""", """@controllers.v1.StaffController@.extendExpirationDate1(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/clearUDID/""" + "$" + """email<[^/]+>""", """@controllers.v1.StaffController@.clearUDID(email:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """renewalDetails/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.readAllRenewalDetails(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """renewalDetails/""" + "$" + """id<[^/]+>""", """@controllers.v1.StaffController@.updateRenewalDetails(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.read"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """@controllers.v2.ProfileController@.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices""", """@controllers.v1.PracticeController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """practices/""" + "$" + """id<[^/]+>""", """@controllers.v1.PracticeController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>""", """@controllers.v3.CoursesController@.readAll(resource:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.read(resource:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>""", """@controllers.v3.CoursesController@.create(resource:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.update(resource:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.CoursesController@.delete(resource:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signature/""" + "$" + """name<[^/]+>""", """@controllers.v1.UploadsController@.signature(name:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings""", """@controllers.v2.EmailMappingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>""", """@controllers.v2.EmailMappingController@.delete(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/add""", """@controllers.v2.EmailMappingController@.addUser(id:Long, userId:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """emailMappings/""" + "$" + """id<[^/]+>/rem""", """@controllers.v2.EmailMappingController@.removeUser(id:Long, userId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping""", """@controllers.v2.ShippingController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """shipping/""" + "$" + """id<[^/]+>""", """@controllers.v2.ShippingController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """forms""", """@controllers.v2.FormsController@.readForms"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.readAllConsents"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents/""" + "$" + """id<[^/]+>""", """@controllers.v2.FormsController@.updateConsent(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """consents""", """@controllers.v2.FormsController@.addConsent"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions""", """@controllers.v2.PrescriptionController@.create"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescriptions/""" + "$" + """id<[^/]+>""", """@controllers.v2.PrescriptionController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prescription/""" + "$" + """id<[^/]+>/pdf""", """@controllers.v2.PrescriptionController@.generatePdf(id:Long, treatment:String, signature:String, timezone:Option[String])"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.readAll(resourceType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.read(resourceType:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.create(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.bulkUpdate(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.update(resourceType:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v2.ResourceCategoriesController@.delete(resourceType:String, id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests""", """@controllers.v2.EducationRequestController@.readAll"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.read(id:Long)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.update(id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests/""" + "$" + """id<[^/]+>""", """@controllers.v2.EducationRequestController@.delete(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """educationRequests""", """@controllers.v2.EducationRequestController@.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart""", """@controllers.v3.CartController@.readAll"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart""", """@controllers.v3.CartController@.update"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart/""" + "$" + """id<[^/]+>""", """@controllers.v3.CartController@.delete(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """categoryId<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.readAll(resourceType:String, categoryId:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/subCategory/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.read(resourceType:String, id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.create(resourceType:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.update(resourceType:String, id:Long)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>""", """@controllers.v3.ResourceSubCategoriesController@.delete(resourceType:String, id:Long)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_v1_HealthCheckController_check0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("healthcheck")))
  )
  private[this] lazy val controllers_v1_HealthCheckController_check0_invoker = createInvoker(
    HealthCheckController_23.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.HealthCheckController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """healthcheck"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_v1_SessionController_check1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_check1_invoker = createInvoker(
    SessionController_9.get.check,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.SessionController",
      "check",
      Nil,
      "GET",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_v1_SessionController_logIn2_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logIn2_invoker = createInvoker(
    SessionController_9.get.logIn,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.SessionController",
      "logIn",
      Nil,
      "PUT",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_v1_SessionController_logOut3_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session")))
  )
  private[this] lazy val controllers_v1_SessionController_logOut3_invoker = createInvoker(
    SessionController_9.get.logOut,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.SessionController",
      "logOut",
      Nil,
      "DELETE",
      """""",
      this.prefix + """session"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_v1_SessionController_signUpRequest4_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("session/signUpRequest")))
  )
  private[this] lazy val controllers_v1_SessionController_signUpRequest4_invoker = createInvoker(
    SessionController_9.get.signUpRequest,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.SessionController",
      "signUpRequest",
      Nil,
      "PUT",
      """""",
      this.prefix + """session/signUpRequest"""
    )
  )

  // @LINE:15
  private[this] lazy val controllers_v1_PasswordController_forgot5_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forgot/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_forgot5_invoker = createInvoker(
    PasswordController_22.get.forgot(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PasswordController",
      "forgot",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """forgot/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_v1_PasswordController_check6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_check6_invoker = createInvoker(
    PasswordController_22.get.check(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PasswordController",
      "check",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_v1_PasswordController_reset7_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reset/"), DynamicPart("secret", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PasswordController_reset7_invoker = createInvoker(
    PasswordController_22.get.reset(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PasswordController",
      "reset",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """reset/""" + "$" + """secret<[^/]+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_v1_PasswordController_change8_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("password")))
  )
  private[this] lazy val controllers_v1_PasswordController_change8_invoker = createInvoker(
    PasswordController_22.get.change,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PasswordController",
      "change",
      Nil,
      "PUT",
      """""",
      this.prefix + """password"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_v3_AnalyticsController_read9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("analytics")))
  )
  private[this] lazy val controllers_v3_AnalyticsController_read9_invoker = createInvoker(
    AnalyticsController_8.get.read,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.AnalyticsController",
      "read",
      Nil,
      "GET",
      """""",
      this.prefix + """analytics"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_v2_CategoriesController_readAll10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_readAll10_invoker = createInvoker(
    CategoriesController_5.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.CategoriesController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate11_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories")))
  )
  private[this] lazy val controllers_v2_CategoriesController_bulkUpdate11_invoker = createInvoker(
    CategoriesController_5.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.CategoriesController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """categories"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_v2_CategoriesController_update12_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_update12_invoker = createInvoker(
    CategoriesController_5.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.CategoriesController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_v2_CategoriesController_delete13_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_CategoriesController_delete13_invoker = createInvoker(
    CategoriesController_5.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.CategoriesController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """categories/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_v3_ProductsController_readAll14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_readAll14_invoker = createInvoker(
    ProductsController_11.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_v3_ProductsController_create15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_create15_invoker = createInvoker(
    ProductsController_11.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_v3_ProductsController_read16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_read16_invoker = createInvoker(
    ProductsController_11.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_v3_ProductsController_update17_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_update17_invoker = createInvoker(
    ProductsController_11.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_v3_ProductsController_bulkUpdate18_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_v3_ProductsController_bulkUpdate18_invoker = createInvoker(
    ProductsController_11.get.bulkUpdate,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "bulkUpdate",
      Nil,
      "PUT",
      """""",
      this.prefix + """products"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_v3_ProductsController_delete19_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ProductsController_delete19_invoker = createInvoker(
    ProductsController_11.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_v3_ProductsController_generatePdf20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/generatePdf/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v3_ProductsController_generatePdf20_invoker = createInvoker(
    ProductsController_11.get.generatePdf(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ProductsController",
      "generatePdf",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """products/generatePdf/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_v2_FavouritesController_add21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_add21_invoker = createInvoker(
    FavouritesController_10.get.add(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FavouritesController",
      "add",
      Seq(classOf[Long]),
      "POST",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_v2_FavouritesController_remove22_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/favourite")))
  )
  private[this] lazy val controllers_v2_FavouritesController_remove22_invoker = createInvoker(
    FavouritesController_10.get.remove(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FavouritesController",
      "remove",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """products/""" + "$" + """id<[^/]+>/favourite"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_v2_VendorController_readAll23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_readAll23_invoker = createInvoker(
    VendorController_7.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_v2_VendorController_create24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors")))
  )
  private[this] lazy val controllers_v2_VendorController_create24_invoker = createInvoker(
    VendorController_7.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """vendors"""
    )
  )

  // @LINE:40
  private[this] lazy val controllers_v2_VendorController_read25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_read25_invoker = createInvoker(
    VendorController_7.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_v2_VendorController_update26_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_update26_invoker = createInvoker(
    VendorController_7.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:42
  private[this] lazy val controllers_v2_VendorController_archive27_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_VendorController_archive27_invoker = createInvoker(
    VendorController_7.get.archive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "archive",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:43
  private[this] lazy val controllers_v2_VendorController_unarchive28_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("vendors/"), DynamicPart("id", """[^/]+""",true), StaticPart("/unarchive")))
  )
  private[this] lazy val controllers_v2_VendorController_unarchive28_invoker = createInvoker(
    VendorController_7.get.unarchive(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.VendorController",
      "unarchive",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """vendors/""" + "$" + """id<[^/]+>/unarchive"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_v1_OrdersController_readAll29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_readAll29_invoker = createInvoker(
    OrdersController_6.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_v1_OrdersController_create30_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders")))
  )
  private[this] lazy val controllers_v1_OrdersController_create30_invoker = createInvoker(
    OrdersController_6.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """orders"""
    )
  )

  // @LINE:47
  private[this] lazy val controllers_v1_OrdersController_read31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_read31_invoker = createInvoker(
    OrdersController_6.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:48
  private[this] lazy val controllers_v1_OrdersController_update32_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_update32_invoker = createInvoker(
    OrdersController_6.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:49
  private[this] lazy val controllers_v1_OrdersController_delete33_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_delete33_invoker = createInvoker(
    OrdersController_6.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """orders/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:50
  private[this] lazy val controllers_v1_OrdersController_updateVendorPayment34_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/updateVendorPayment/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_OrdersController_updateVendorPayment34_invoker = createInvoker(
    OrdersController_6.get.updateVendorPayment(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.OrdersController",
      "updateVendorPayment",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """orders/updateVendorPayment/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_v1_TreatmentsController_readAll35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_readAll35_invoker = createInvoker(
    TreatmentsController_4.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:53
  private[this] lazy val controllers_v1_TreatmentsController_create36_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_create36_invoker = createInvoker(
    TreatmentsController_4.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """treatments"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_v1_TreatmentsController_read37_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_read37_invoker = createInvoker(
    TreatmentsController_4.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:55
  private[this] lazy val controllers_v1_TreatmentsController_update38_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_update38_invoker = createInvoker(
    TreatmentsController_4.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:56
  private[this] lazy val controllers_v1_TreatmentsController_delete39_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_delete39_invoker = createInvoker(
    TreatmentsController_4.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_v1_TreatmentsController_pdf40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v1_TreatmentsController_pdf40_invoker = createInvoker(
    TreatmentsController_4.get.pdf(fakeValue[Long], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "pdf",
      Seq(classOf[Long], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """treatments/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_v1_TreatmentsController_updateCtScan41_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("treatments/updateCtScan/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_TreatmentsController_updateCtScan41_invoker = createInvoker(
    TreatmentsController_4.get.updateCtScan(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.TreatmentsController",
      "updateCtScan",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """treatments/updateCtScan/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:60
  private[this] lazy val controllers_v1_PatientsController_readAll42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients")))
  )
  private[this] lazy val controllers_v1_PatientsController_readAll42_invoker = createInvoker(
    PatientsController_2.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PatientsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """patients"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_v1_PatientsController_read43_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("patients/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PatientsController_read43_invoker = createInvoker(
    PatientsController_2.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PatientsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """patients/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:63
  private[this] lazy val controllers_v1_ConsultationsController_readAll44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_readAll44_invoker = createInvoker(
    ConsultationsController_12.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.ConsultationsController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:64
  private[this] lazy val controllers_v1_ConsultationsController_create45_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations")))
  )
  private[this] lazy val controllers_v1_ConsultationsController_create45_invoker = createInvoker(
    ConsultationsController_12.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.ConsultationsController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """consultations"""
    )
  )

  // @LINE:65
  private[this] lazy val controllers_v1_ConsultationsController_read46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_read46_invoker = createInvoker(
    ConsultationsController_12.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.ConsultationsController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:66
  private[this] lazy val controllers_v1_ConsultationsController_update47_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_update47_invoker = createInvoker(
    ConsultationsController_12.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.ConsultationsController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:67
  private[this] lazy val controllers_v1_ConsultationsController_delete48_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consultations/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_ConsultationsController_delete48_invoker = createInvoker(
    ConsultationsController_12.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.ConsultationsController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """consultations/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:69
  private[this] lazy val controllers_v1_UsersController_readAll49_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_readAll49_invoker = createInvoker(
    UsersController_0.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UsersController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:70
  private[this] lazy val controllers_v1_UsersController_create50_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_v1_UsersController_create50_invoker = createInvoker(
    UsersController_0.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UsersController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """users"""
    )
  )

  // @LINE:71
  private[this] lazy val controllers_v1_UsersController_read51_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_read51_invoker = createInvoker(
    UsersController_0.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UsersController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:72
  private[this] lazy val controllers_v1_UsersController_update52_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_update52_invoker = createInvoker(
    UsersController_0.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UsersController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:73
  private[this] lazy val controllers_v1_UsersController_delete53_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UsersController_delete53_invoker = createInvoker(
    UsersController_0.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UsersController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """users/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:75
  private[this] lazy val controllers_v1_StaffController_readAll54_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_readAll54_invoker = createInvoker(
    StaffController_20.get.readAll(fakeValue[Option[Long]]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "readAll",
      Seq(classOf[Option[Long]]),
      "GET",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:76
  private[this] lazy val controllers_v1_StaffController_create55_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts")))
  )
  private[this] lazy val controllers_v1_StaffController_create55_invoker = createInvoker(
    StaffController_20.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts"""
    )
  )

  // @LINE:77
  private[this] lazy val controllers_v1_StaffController_read56_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_read56_invoker = createInvoker(
    StaffController_20.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:78
  private[this] lazy val controllers_v1_StaffController_update57_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_update57_invoker = createInvoker(
    StaffController_20.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_v1_StaffController_setUDID58_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/udid")))
  )
  private[this] lazy val controllers_v1_StaffController_setUDID58_invoker = createInvoker(
    StaffController_20.get.setUDID,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "setUDID",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/udid"""
    )
  )

  // @LINE:80
  private[this] lazy val controllers_v1_StaffController_delete59_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_delete59_invoker = createInvoker(
    StaffController_20.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:81
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate60_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true), StaticPart("/extend")))
  )
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate60_invoker = createInvoker(
    StaffController_20.get.extendExpirationDate(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "extendExpirationDate",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>/extend"""
    )
  )

  // @LINE:82
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate161_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/"), DynamicPart("id", """[^/]+""",true), StaticPart("/extend1")))
  )
  private[this] lazy val controllers_v1_StaffController_extendExpirationDate161_invoker = createInvoker(
    StaffController_20.get.extendExpirationDate1(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "extendExpirationDate1",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """accounts/""" + "$" + """id<[^/]+>/extend1"""
    )
  )

  // @LINE:83
  private[this] lazy val controllers_v1_StaffController_clearUDID62_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/clearUDID/"), DynamicPart("email", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_clearUDID62_invoker = createInvoker(
    StaffController_20.get.clearUDID(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "clearUDID",
      Seq(classOf[String]),
      "DELETE",
      """""",
      this.prefix + """accounts/clearUDID/""" + "$" + """email<[^/]+>"""
    )
  )

  // @LINE:85
  private[this] lazy val controllers_v1_StaffController_readAllRenewalDetails63_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("renewalDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_readAllRenewalDetails63_invoker = createInvoker(
    StaffController_20.get.readAllRenewalDetails(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "readAllRenewalDetails",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """renewalDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:86
  private[this] lazy val controllers_v1_StaffController_updateRenewalDetails64_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("renewalDetails/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_StaffController_updateRenewalDetails64_invoker = createInvoker(
    StaffController_20.get.updateRenewalDetails(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.StaffController",
      "updateRenewalDetails",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """renewalDetails/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:88
  private[this] lazy val controllers_v2_ProfileController_read65_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_read65_invoker = createInvoker(
    ProfileController_17.get.read,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ProfileController",
      "read",
      Nil,
      "GET",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:89
  private[this] lazy val controllers_v2_ProfileController_update66_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_v2_ProfileController_update66_invoker = createInvoker(
    ProfileController_17.get.update,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ProfileController",
      "update",
      Nil,
      "PUT",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:91
  private[this] lazy val controllers_v1_PracticeController_readAll67_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_readAll67_invoker = createInvoker(
    PracticeController_24.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PracticeController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:92
  private[this] lazy val controllers_v1_PracticeController_create68_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices")))
  )
  private[this] lazy val controllers_v1_PracticeController_create68_invoker = createInvoker(
    PracticeController_24.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PracticeController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """practices"""
    )
  )

  // @LINE:93
  private[this] lazy val controllers_v1_PracticeController_read69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_read69_invoker = createInvoker(
    PracticeController_24.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PracticeController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:94
  private[this] lazy val controllers_v1_PracticeController_update70_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_update70_invoker = createInvoker(
    PracticeController_24.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PracticeController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:95
  private[this] lazy val controllers_v1_PracticeController_delete71_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("practices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_PracticeController_delete71_invoker = createInvoker(
    PracticeController_24.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.PracticeController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """practices/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:97
  private[this] lazy val controllers_v3_CoursesController_readAll72_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_readAll72_invoker = createInvoker(
    CoursesController_1.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CoursesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>"""
    )
  )

  // @LINE:98
  private[this] lazy val controllers_v3_CoursesController_read73_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_read73_invoker = createInvoker(
    CoursesController_1.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CoursesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:99
  private[this] lazy val controllers_v3_CoursesController_create74_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_create74_invoker = createInvoker(
    CoursesController_1.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CoursesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>"""
    )
  )

  // @LINE:100
  private[this] lazy val controllers_v3_CoursesController_update75_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_update75_invoker = createInvoker(
    CoursesController_1.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CoursesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:101
  private[this] lazy val controllers_v3_CoursesController_delete76_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("courses/"), DynamicPart("resource", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CoursesController_delete76_invoker = createInvoker(
    CoursesController_1.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CoursesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """courses/""" + "$" + """resource<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:103
  private[this] lazy val controllers_v1_UploadsController_signature77_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signature/"), DynamicPart("name", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v1_UploadsController_signature77_invoker = createInvoker(
    UploadsController_14.get.signature(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v1.UploadsController",
      "signature",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """signature/""" + "$" + """name<[^/]+>"""
    )
  )

  // @LINE:105
  private[this] lazy val controllers_v2_EmailMappingController_readAll78_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_readAll78_invoker = createInvoker(
    EmailMappingController_25.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:106
  private[this] lazy val controllers_v2_EmailMappingController_create79_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_create79_invoker = createInvoker(
    EmailMappingController_25.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """emailMappings"""
    )
  )

  // @LINE:107
  private[this] lazy val controllers_v2_EmailMappingController_read80_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_read80_invoker = createInvoker(
    EmailMappingController_25.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:108
  private[this] lazy val controllers_v2_EmailMappingController_update81_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_update81_invoker = createInvoker(
    EmailMappingController_25.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:109
  private[this] lazy val controllers_v2_EmailMappingController_delete82_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EmailMappingController_delete82_invoker = createInvoker(
    EmailMappingController_25.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:110
  private[this] lazy val controllers_v2_EmailMappingController_addUser83_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/add")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_addUser83_invoker = createInvoker(
    EmailMappingController_25.get.addUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "addUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/add"""
    )
  )

  // @LINE:111
  private[this] lazy val controllers_v2_EmailMappingController_removeUser84_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("emailMappings/"), DynamicPart("id", """[^/]+""",true), StaticPart("/rem")))
  )
  private[this] lazy val controllers_v2_EmailMappingController_removeUser84_invoker = createInvoker(
    EmailMappingController_25.get.removeUser(fakeValue[Long], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EmailMappingController",
      "removeUser",
      Seq(classOf[Long], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """emailMappings/""" + "$" + """id<[^/]+>/rem"""
    )
  )

  // @LINE:113
  private[this] lazy val controllers_v2_ShippingController_readAll85_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_readAll85_invoker = createInvoker(
    ShippingController_15.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ShippingController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:114
  private[this] lazy val controllers_v2_ShippingController_create86_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping")))
  )
  private[this] lazy val controllers_v2_ShippingController_create86_invoker = createInvoker(
    ShippingController_15.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ShippingController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """shipping"""
    )
  )

  // @LINE:115
  private[this] lazy val controllers_v2_ShippingController_read87_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_read87_invoker = createInvoker(
    ShippingController_15.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ShippingController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:116
  private[this] lazy val controllers_v2_ShippingController_update88_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_update88_invoker = createInvoker(
    ShippingController_15.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ShippingController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:117
  private[this] lazy val controllers_v2_ShippingController_delete89_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("shipping/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ShippingController_delete89_invoker = createInvoker(
    ShippingController_15.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ShippingController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """shipping/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:119
  private[this] lazy val controllers_v2_FormsController_readForms90_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("forms")))
  )
  private[this] lazy val controllers_v2_FormsController_readForms90_invoker = createInvoker(
    FormsController_3.get.readForms,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FormsController",
      "readForms",
      Nil,
      "GET",
      """""",
      this.prefix + """forms"""
    )
  )

  // @LINE:120
  private[this] lazy val controllers_v2_FormsController_readAllConsents91_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_readAllConsents91_invoker = createInvoker(
    FormsController_3.get.readAllConsents,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FormsController",
      "readAllConsents",
      Nil,
      "GET",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:121
  private[this] lazy val controllers_v2_FormsController_updateConsent92_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_FormsController_updateConsent92_invoker = createInvoker(
    FormsController_3.get.updateConsent(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FormsController",
      "updateConsent",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """consents/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:122
  private[this] lazy val controllers_v2_FormsController_addConsent93_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("consents")))
  )
  private[this] lazy val controllers_v2_FormsController_addConsent93_invoker = createInvoker(
    FormsController_3.get.addConsent,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.FormsController",
      "addConsent",
      Nil,
      "POST",
      """""",
      this.prefix + """consents"""
    )
  )

  // @LINE:124
  private[this] lazy val controllers_v2_PrescriptionController_readAll94_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_readAll94_invoker = createInvoker(
    PrescriptionController_18.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.PrescriptionController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:125
  private[this] lazy val controllers_v2_PrescriptionController_create95_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_create95_invoker = createInvoker(
    PrescriptionController_18.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.PrescriptionController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """prescriptions"""
    )
  )

  // @LINE:126
  private[this] lazy val controllers_v2_PrescriptionController_delete96_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescriptions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_PrescriptionController_delete96_invoker = createInvoker(
    PrescriptionController_18.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.PrescriptionController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """prescriptions/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:127
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf97_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prescription/"), DynamicPart("id", """[^/]+""",true), StaticPart("/pdf")))
  )
  private[this] lazy val controllers_v2_PrescriptionController_generatePdf97_invoker = createInvoker(
    PrescriptionController_18.get.generatePdf(fakeValue[Long], fakeValue[String], fakeValue[String], fakeValue[Option[String]]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.PrescriptionController",
      "generatePdf",
      Seq(classOf[Long], classOf[String], classOf[String], classOf[Option[String]]),
      "GET",
      """""",
      this.prefix + """prescription/""" + "$" + """id<[^/]+>/pdf"""
    )
  )

  // @LINE:130
  private[this] lazy val controllers_v2_ResourceCategoriesController_readAll98_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_readAll98_invoker = createInvoker(
    ResourceCategoriesController_19.get.readAll(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "readAll",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:131
  private[this] lazy val controllers_v2_ResourceCategoriesController_read99_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_read99_invoker = createInvoker(
    ResourceCategoriesController_19.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:132
  private[this] lazy val controllers_v2_ResourceCategoriesController_create100_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_create100_invoker = createInvoker(
    ResourceCategoriesController_19.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:133
  private[this] lazy val controllers_v2_ResourceCategoriesController_bulkUpdate101_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_bulkUpdate101_invoker = createInvoker(
    ResourceCategoriesController_19.get.bulkUpdate(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "bulkUpdate",
      Seq(classOf[String]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:134
  private[this] lazy val controllers_v2_ResourceCategoriesController_update102_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_update102_invoker = createInvoker(
    ResourceCategoriesController_19.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:135
  private[this] lazy val controllers_v2_ResourceCategoriesController_delete103_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_ResourceCategoriesController_delete103_invoker = createInvoker(
    ResourceCategoriesController_19.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.ResourceCategoriesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """resource-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:139
  private[this] lazy val controllers_v2_EducationRequestController_readAll104_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests")))
  )
  private[this] lazy val controllers_v2_EducationRequestController_readAll104_invoker = createInvoker(
    EducationRequestController_16.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EducationRequestController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """educationRequests"""
    )
  )

  // @LINE:140
  private[this] lazy val controllers_v2_EducationRequestController_read105_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_read105_invoker = createInvoker(
    EducationRequestController_16.get.read(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EducationRequestController",
      "read",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:141
  private[this] lazy val controllers_v2_EducationRequestController_update106_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_update106_invoker = createInvoker(
    EducationRequestController_16.get.update(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EducationRequestController",
      "update",
      Seq(classOf[Long]),
      "PUT",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:142
  private[this] lazy val controllers_v2_EducationRequestController_delete107_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v2_EducationRequestController_delete107_invoker = createInvoker(
    EducationRequestController_16.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EducationRequestController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """educationRequests/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:143
  private[this] lazy val controllers_v2_EducationRequestController_create108_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("educationRequests")))
  )
  private[this] lazy val controllers_v2_EducationRequestController_create108_invoker = createInvoker(
    EducationRequestController_16.get.create,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v2.EducationRequestController",
      "create",
      Nil,
      "POST",
      """""",
      this.prefix + """educationRequests"""
    )
  )

  // @LINE:145
  private[this] lazy val controllers_v3_CartController_readAll109_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart")))
  )
  private[this] lazy val controllers_v3_CartController_readAll109_invoker = createInvoker(
    CartController_13.get.readAll,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CartController",
      "readAll",
      Nil,
      "GET",
      """""",
      this.prefix + """cart"""
    )
  )

  // @LINE:146
  private[this] lazy val controllers_v3_CartController_update110_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart")))
  )
  private[this] lazy val controllers_v3_CartController_update110_invoker = createInvoker(
    CartController_13.get.update,
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CartController",
      "update",
      Nil,
      "POST",
      """""",
      this.prefix + """cart"""
    )
  )

  // @LINE:147
  private[this] lazy val controllers_v3_CartController_delete111_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_CartController_delete111_invoker = createInvoker(
    CartController_13.get.delete(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.CartController",
      "delete",
      Seq(classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """cart/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:149
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_readAll112_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("categoryId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_readAll112_invoker = createInvoker(
    ResourceSubCategoriesController_21.get.readAll(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ResourceSubCategoriesController",
      "readAll",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """categoryId<[^/]+>"""
    )
  )

  // @LINE:150
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_read113_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/subCategory/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_read113_invoker = createInvoker(
    ResourceSubCategoriesController_21.get.read(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ResourceSubCategoriesController",
      "read",
      Seq(classOf[String], classOf[Long]),
      "GET",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/subCategory/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:151
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_create114_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_create114_invoker = createInvoker(
    ResourceSubCategoriesController_21.get.create(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ResourceSubCategoriesController",
      "create",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>"""
    )
  )

  // @LINE:152
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_update115_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_update115_invoker = createInvoker(
    ResourceSubCategoriesController_21.get.update(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ResourceSubCategoriesController",
      "update",
      Seq(classOf[String], classOf[Long]),
      "PUT",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:153
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_delete116_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("resource-sub-categories/"), DynamicPart("resourceType", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_v3_ResourceSubCategoriesController_delete116_invoker = createInvoker(
    ResourceSubCategoriesController_21.get.delete(fakeValue[String], fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "v3",
      "controllers.v3.ResourceSubCategoriesController",
      "delete",
      Seq(classOf[String], classOf[Long]),
      "DELETE",
      """""",
      this.prefix + """resource-sub-categories/""" + "$" + """resourceType<[^/]+>/""" + "$" + """id<[^/]+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_v1_HealthCheckController_check0_route(params) =>
      call { 
        controllers_v1_HealthCheckController_check0_invoker.call(HealthCheckController_23.get.check)
      }
  
    // @LINE:9
    case controllers_v1_SessionController_check1_route(params) =>
      call { 
        controllers_v1_SessionController_check1_invoker.call(SessionController_9.get.check)
      }
  
    // @LINE:10
    case controllers_v1_SessionController_logIn2_route(params) =>
      call { 
        controllers_v1_SessionController_logIn2_invoker.call(SessionController_9.get.logIn)
      }
  
    // @LINE:11
    case controllers_v1_SessionController_logOut3_route(params) =>
      call { 
        controllers_v1_SessionController_logOut3_invoker.call(SessionController_9.get.logOut)
      }
  
    // @LINE:12
    case controllers_v1_SessionController_signUpRequest4_route(params) =>
      call { 
        controllers_v1_SessionController_signUpRequest4_invoker.call(SessionController_9.get.signUpRequest)
      }
  
    // @LINE:15
    case controllers_v1_PasswordController_forgot5_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_PasswordController_forgot5_invoker.call(PasswordController_22.get.forgot(email))
      }
  
    // @LINE:16
    case controllers_v1_PasswordController_check6_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_check6_invoker.call(PasswordController_22.get.check(secret))
      }
  
    // @LINE:17
    case controllers_v1_PasswordController_reset7_route(params) =>
      call(params.fromPath[String]("secret", None)) { (secret) =>
        controllers_v1_PasswordController_reset7_invoker.call(PasswordController_22.get.reset(secret))
      }
  
    // @LINE:18
    case controllers_v1_PasswordController_change8_route(params) =>
      call { 
        controllers_v1_PasswordController_change8_invoker.call(PasswordController_22.get.change)
      }
  
    // @LINE:20
    case controllers_v3_AnalyticsController_read9_route(params) =>
      call { 
        controllers_v3_AnalyticsController_read9_invoker.call(AnalyticsController_8.get.read)
      }
  
    // @LINE:22
    case controllers_v2_CategoriesController_readAll10_route(params) =>
      call { 
        controllers_v2_CategoriesController_readAll10_invoker.call(CategoriesController_5.get.readAll)
      }
  
    // @LINE:23
    case controllers_v2_CategoriesController_bulkUpdate11_route(params) =>
      call { 
        controllers_v2_CategoriesController_bulkUpdate11_invoker.call(CategoriesController_5.get.bulkUpdate)
      }
  
    // @LINE:24
    case controllers_v2_CategoriesController_update12_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_update12_invoker.call(CategoriesController_5.get.update(id))
      }
  
    // @LINE:25
    case controllers_v2_CategoriesController_delete13_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_CategoriesController_delete13_invoker.call(CategoriesController_5.get.delete(id))
      }
  
    // @LINE:27
    case controllers_v3_ProductsController_readAll14_route(params) =>
      call { 
        controllers_v3_ProductsController_readAll14_invoker.call(ProductsController_11.get.readAll)
      }
  
    // @LINE:28
    case controllers_v3_ProductsController_create15_route(params) =>
      call { 
        controllers_v3_ProductsController_create15_invoker.call(ProductsController_11.get.create)
      }
  
    // @LINE:29
    case controllers_v3_ProductsController_read16_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_read16_invoker.call(ProductsController_11.get.read(id))
      }
  
    // @LINE:30
    case controllers_v3_ProductsController_update17_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_update17_invoker.call(ProductsController_11.get.update(id))
      }
  
    // @LINE:31
    case controllers_v3_ProductsController_bulkUpdate18_route(params) =>
      call { 
        controllers_v3_ProductsController_bulkUpdate18_invoker.call(ProductsController_11.get.bulkUpdate)
      }
  
    // @LINE:32
    case controllers_v3_ProductsController_delete19_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_delete19_invoker.call(ProductsController_11.get.delete(id))
      }
  
    // @LINE:33
    case controllers_v3_ProductsController_generatePdf20_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_ProductsController_generatePdf20_invoker.call(ProductsController_11.get.generatePdf(id))
      }
  
    // @LINE:35
    case controllers_v2_FavouritesController_add21_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_add21_invoker.call(FavouritesController_10.get.add(id))
      }
  
    // @LINE:36
    case controllers_v2_FavouritesController_remove22_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_FavouritesController_remove22_invoker.call(FavouritesController_10.get.remove(id))
      }
  
    // @LINE:38
    case controllers_v2_VendorController_readAll23_route(params) =>
      call { 
        controllers_v2_VendorController_readAll23_invoker.call(VendorController_7.get.readAll)
      }
  
    // @LINE:39
    case controllers_v2_VendorController_create24_route(params) =>
      call { 
        controllers_v2_VendorController_create24_invoker.call(VendorController_7.get.create)
      }
  
    // @LINE:40
    case controllers_v2_VendorController_read25_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_read25_invoker.call(VendorController_7.get.read(id))
      }
  
    // @LINE:41
    case controllers_v2_VendorController_update26_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_update26_invoker.call(VendorController_7.get.update(id))
      }
  
    // @LINE:42
    case controllers_v2_VendorController_archive27_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_archive27_invoker.call(VendorController_7.get.archive(id))
      }
  
    // @LINE:43
    case controllers_v2_VendorController_unarchive28_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_VendorController_unarchive28_invoker.call(VendorController_7.get.unarchive(id))
      }
  
    // @LINE:45
    case controllers_v1_OrdersController_readAll29_route(params) =>
      call { 
        controllers_v1_OrdersController_readAll29_invoker.call(OrdersController_6.get.readAll)
      }
  
    // @LINE:46
    case controllers_v1_OrdersController_create30_route(params) =>
      call { 
        controllers_v1_OrdersController_create30_invoker.call(OrdersController_6.get.create)
      }
  
    // @LINE:47
    case controllers_v1_OrdersController_read31_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_read31_invoker.call(OrdersController_6.get.read(id))
      }
  
    // @LINE:48
    case controllers_v1_OrdersController_update32_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_update32_invoker.call(OrdersController_6.get.update(id))
      }
  
    // @LINE:49
    case controllers_v1_OrdersController_delete33_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_delete33_invoker.call(OrdersController_6.get.delete(id))
      }
  
    // @LINE:50
    case controllers_v1_OrdersController_updateVendorPayment34_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_OrdersController_updateVendorPayment34_invoker.call(OrdersController_6.get.updateVendorPayment(id))
      }
  
    // @LINE:52
    case controllers_v1_TreatmentsController_readAll35_route(params) =>
      call { 
        controllers_v1_TreatmentsController_readAll35_invoker.call(TreatmentsController_4.get.readAll)
      }
  
    // @LINE:53
    case controllers_v1_TreatmentsController_create36_route(params) =>
      call { 
        controllers_v1_TreatmentsController_create36_invoker.call(TreatmentsController_4.get.create)
      }
  
    // @LINE:54
    case controllers_v1_TreatmentsController_read37_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_read37_invoker.call(TreatmentsController_4.get.read(id))
      }
  
    // @LINE:55
    case controllers_v1_TreatmentsController_update38_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_update38_invoker.call(TreatmentsController_4.get.update(id))
      }
  
    // @LINE:56
    case controllers_v1_TreatmentsController_delete39_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_delete39_invoker.call(TreatmentsController_4.get.delete(id))
      }
  
    // @LINE:57
    case controllers_v1_TreatmentsController_pdf40_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Option[String]]("step", None)) { (id, step) =>
        controllers_v1_TreatmentsController_pdf40_invoker.call(TreatmentsController_4.get.pdf(id, step))
      }
  
    // @LINE:58
    case controllers_v1_TreatmentsController_updateCtScan41_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_TreatmentsController_updateCtScan41_invoker.call(TreatmentsController_4.get.updateCtScan(id))
      }
  
    // @LINE:60
    case controllers_v1_PatientsController_readAll42_route(params) =>
      call { 
        controllers_v1_PatientsController_readAll42_invoker.call(PatientsController_2.get.readAll)
      }
  
    // @LINE:61
    case controllers_v1_PatientsController_read43_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PatientsController_read43_invoker.call(PatientsController_2.get.read(id))
      }
  
    // @LINE:63
    case controllers_v1_ConsultationsController_readAll44_route(params) =>
      call { 
        controllers_v1_ConsultationsController_readAll44_invoker.call(ConsultationsController_12.get.readAll)
      }
  
    // @LINE:64
    case controllers_v1_ConsultationsController_create45_route(params) =>
      call { 
        controllers_v1_ConsultationsController_create45_invoker.call(ConsultationsController_12.get.create)
      }
  
    // @LINE:65
    case controllers_v1_ConsultationsController_read46_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_read46_invoker.call(ConsultationsController_12.get.read(id))
      }
  
    // @LINE:66
    case controllers_v1_ConsultationsController_update47_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_update47_invoker.call(ConsultationsController_12.get.update(id))
      }
  
    // @LINE:67
    case controllers_v1_ConsultationsController_delete48_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_ConsultationsController_delete48_invoker.call(ConsultationsController_12.get.delete(id))
      }
  
    // @LINE:69
    case controllers_v1_UsersController_readAll49_route(params) =>
      call { 
        controllers_v1_UsersController_readAll49_invoker.call(UsersController_0.get.readAll)
      }
  
    // @LINE:70
    case controllers_v1_UsersController_create50_route(params) =>
      call { 
        controllers_v1_UsersController_create50_invoker.call(UsersController_0.get.create)
      }
  
    // @LINE:71
    case controllers_v1_UsersController_read51_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_read51_invoker.call(UsersController_0.get.read(id))
      }
  
    // @LINE:72
    case controllers_v1_UsersController_update52_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_update52_invoker.call(UsersController_0.get.update(id))
      }
  
    // @LINE:73
    case controllers_v1_UsersController_delete53_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_UsersController_delete53_invoker.call(UsersController_0.get.delete(id))
      }
  
    // @LINE:75
    case controllers_v1_StaffController_readAll54_route(params) =>
      call(params.fromQuery[Option[Long]]("practice", None)) { (practice) =>
        controllers_v1_StaffController_readAll54_invoker.call(StaffController_20.get.readAll(practice))
      }
  
    // @LINE:76
    case controllers_v1_StaffController_create55_route(params) =>
      call { 
        controllers_v1_StaffController_create55_invoker.call(StaffController_20.get.create)
      }
  
    // @LINE:77
    case controllers_v1_StaffController_read56_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_read56_invoker.call(StaffController_20.get.read(id))
      }
  
    // @LINE:78
    case controllers_v1_StaffController_update57_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_update57_invoker.call(StaffController_20.get.update(id))
      }
  
    // @LINE:79
    case controllers_v1_StaffController_setUDID58_route(params) =>
      call { 
        controllers_v1_StaffController_setUDID58_invoker.call(StaffController_20.get.setUDID)
      }
  
    // @LINE:80
    case controllers_v1_StaffController_delete59_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_delete59_invoker.call(StaffController_20.get.delete(id))
      }
  
    // @LINE:81
    case controllers_v1_StaffController_extendExpirationDate60_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_extendExpirationDate60_invoker.call(StaffController_20.get.extendExpirationDate(id))
      }
  
    // @LINE:82
    case controllers_v1_StaffController_extendExpirationDate161_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_extendExpirationDate161_invoker.call(StaffController_20.get.extendExpirationDate1(id))
      }
  
    // @LINE:83
    case controllers_v1_StaffController_clearUDID62_route(params) =>
      call(params.fromPath[String]("email", None)) { (email) =>
        controllers_v1_StaffController_clearUDID62_invoker.call(StaffController_20.get.clearUDID(email))
      }
  
    // @LINE:85
    case controllers_v1_StaffController_readAllRenewalDetails63_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_readAllRenewalDetails63_invoker.call(StaffController_20.get.readAllRenewalDetails(id))
      }
  
    // @LINE:86
    case controllers_v1_StaffController_updateRenewalDetails64_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_StaffController_updateRenewalDetails64_invoker.call(StaffController_20.get.updateRenewalDetails(id))
      }
  
    // @LINE:88
    case controllers_v2_ProfileController_read65_route(params) =>
      call { 
        controllers_v2_ProfileController_read65_invoker.call(ProfileController_17.get.read)
      }
  
    // @LINE:89
    case controllers_v2_ProfileController_update66_route(params) =>
      call { 
        controllers_v2_ProfileController_update66_invoker.call(ProfileController_17.get.update)
      }
  
    // @LINE:91
    case controllers_v1_PracticeController_readAll67_route(params) =>
      call { 
        controllers_v1_PracticeController_readAll67_invoker.call(PracticeController_24.get.readAll)
      }
  
    // @LINE:92
    case controllers_v1_PracticeController_create68_route(params) =>
      call { 
        controllers_v1_PracticeController_create68_invoker.call(PracticeController_24.get.create)
      }
  
    // @LINE:93
    case controllers_v1_PracticeController_read69_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_read69_invoker.call(PracticeController_24.get.read(id))
      }
  
    // @LINE:94
    case controllers_v1_PracticeController_update70_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_update70_invoker.call(PracticeController_24.get.update(id))
      }
  
    // @LINE:95
    case controllers_v1_PracticeController_delete71_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v1_PracticeController_delete71_invoker.call(PracticeController_24.get.delete(id))
      }
  
    // @LINE:97
    case controllers_v3_CoursesController_readAll72_route(params) =>
      call(params.fromPath[String]("resource", None)) { (resource) =>
        controllers_v3_CoursesController_readAll72_invoker.call(CoursesController_1.get.readAll(resource))
      }
  
    // @LINE:98
    case controllers_v3_CoursesController_read73_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_read73_invoker.call(CoursesController_1.get.read(resource, id))
      }
  
    // @LINE:99
    case controllers_v3_CoursesController_create74_route(params) =>
      call(params.fromPath[String]("resource", None)) { (resource) =>
        controllers_v3_CoursesController_create74_invoker.call(CoursesController_1.get.create(resource))
      }
  
    // @LINE:100
    case controllers_v3_CoursesController_update75_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_update75_invoker.call(CoursesController_1.get.update(resource, id))
      }
  
    // @LINE:101
    case controllers_v3_CoursesController_delete76_route(params) =>
      call(params.fromPath[String]("resource", None), params.fromPath[Long]("id", None)) { (resource, id) =>
        controllers_v3_CoursesController_delete76_invoker.call(CoursesController_1.get.delete(resource, id))
      }
  
    // @LINE:103
    case controllers_v1_UploadsController_signature77_route(params) =>
      call(params.fromPath[String]("name", None)) { (name) =>
        controllers_v1_UploadsController_signature77_invoker.call(UploadsController_14.get.signature(name))
      }
  
    // @LINE:105
    case controllers_v2_EmailMappingController_readAll78_route(params) =>
      call { 
        controllers_v2_EmailMappingController_readAll78_invoker.call(EmailMappingController_25.get.readAll)
      }
  
    // @LINE:106
    case controllers_v2_EmailMappingController_create79_route(params) =>
      call { 
        controllers_v2_EmailMappingController_create79_invoker.call(EmailMappingController_25.get.create)
      }
  
    // @LINE:107
    case controllers_v2_EmailMappingController_read80_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_read80_invoker.call(EmailMappingController_25.get.read(id))
      }
  
    // @LINE:108
    case controllers_v2_EmailMappingController_update81_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_update81_invoker.call(EmailMappingController_25.get.update(id))
      }
  
    // @LINE:109
    case controllers_v2_EmailMappingController_delete82_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EmailMappingController_delete82_invoker.call(EmailMappingController_25.get.delete(id))
      }
  
    // @LINE:110
    case controllers_v2_EmailMappingController_addUser83_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_addUser83_invoker.call(EmailMappingController_25.get.addUser(id, userId))
      }
  
    // @LINE:111
    case controllers_v2_EmailMappingController_removeUser84_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[Long]("userId", None)) { (id, userId) =>
        controllers_v2_EmailMappingController_removeUser84_invoker.call(EmailMappingController_25.get.removeUser(id, userId))
      }
  
    // @LINE:113
    case controllers_v2_ShippingController_readAll85_route(params) =>
      call { 
        controllers_v2_ShippingController_readAll85_invoker.call(ShippingController_15.get.readAll)
      }
  
    // @LINE:114
    case controllers_v2_ShippingController_create86_route(params) =>
      call { 
        controllers_v2_ShippingController_create86_invoker.call(ShippingController_15.get.create)
      }
  
    // @LINE:115
    case controllers_v2_ShippingController_read87_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_read87_invoker.call(ShippingController_15.get.read(id))
      }
  
    // @LINE:116
    case controllers_v2_ShippingController_update88_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_update88_invoker.call(ShippingController_15.get.update(id))
      }
  
    // @LINE:117
    case controllers_v2_ShippingController_delete89_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_ShippingController_delete89_invoker.call(ShippingController_15.get.delete(id))
      }
  
    // @LINE:119
    case controllers_v2_FormsController_readForms90_route(params) =>
      call { 
        controllers_v2_FormsController_readForms90_invoker.call(FormsController_3.get.readForms)
      }
  
    // @LINE:120
    case controllers_v2_FormsController_readAllConsents91_route(params) =>
      call { 
        controllers_v2_FormsController_readAllConsents91_invoker.call(FormsController_3.get.readAllConsents)
      }
  
    // @LINE:121
    case controllers_v2_FormsController_updateConsent92_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_v2_FormsController_updateConsent92_invoker.call(FormsController_3.get.updateConsent(id))
      }
  
    // @LINE:122
    case controllers_v2_FormsController_addConsent93_route(params) =>
      call { 
        controllers_v2_FormsController_addConsent93_invoker.call(FormsController_3.get.addConsent)
      }
  
    // @LINE:124
    case controllers_v2_PrescriptionController_readAll94_route(params) =>
      call { 
        controllers_v2_PrescriptionController_readAll94_invoker.call(PrescriptionController_18.get.readAll)
      }
  
    // @LINE:125
    case controllers_v2_PrescriptionController_create95_route(params) =>
      call { 
        controllers_v2_PrescriptionController_create95_invoker.call(PrescriptionController_18.get.create)
      }
  
    // @LINE:126
    case controllers_v2_PrescriptionController_delete96_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_PrescriptionController_delete96_invoker.call(PrescriptionController_18.get.delete(id))
      }
  
    // @LINE:127
    case controllers_v2_PrescriptionController_generatePdf97_route(params) =>
      call(params.fromPath[Long]("id", None), params.fromQuery[String]("treatment", None), params.fromQuery[String]("signature", None), params.fromQuery[Option[String]]("timezone", None)) { (id, treatment, signature, timezone) =>
        controllers_v2_PrescriptionController_generatePdf97_invoker.call(PrescriptionController_18.get.generatePdf(id, treatment, signature, timezone))
      }
  
    // @LINE:130
    case controllers_v2_ResourceCategoriesController_readAll98_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_readAll98_invoker.call(ResourceCategoriesController_19.get.readAll(resourceType))
      }
  
    // @LINE:131
    case controllers_v2_ResourceCategoriesController_read99_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_read99_invoker.call(ResourceCategoriesController_19.get.read(resourceType, id))
      }
  
    // @LINE:132
    case controllers_v2_ResourceCategoriesController_create100_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_create100_invoker.call(ResourceCategoriesController_19.get.create(resourceType))
      }
  
    // @LINE:133
    case controllers_v2_ResourceCategoriesController_bulkUpdate101_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v2_ResourceCategoriesController_bulkUpdate101_invoker.call(ResourceCategoriesController_19.get.bulkUpdate(resourceType))
      }
  
    // @LINE:134
    case controllers_v2_ResourceCategoriesController_update102_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_update102_invoker.call(ResourceCategoriesController_19.get.update(resourceType, id))
      }
  
    // @LINE:135
    case controllers_v2_ResourceCategoriesController_delete103_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v2_ResourceCategoriesController_delete103_invoker.call(ResourceCategoriesController_19.get.delete(resourceType, id))
      }
  
    // @LINE:139
    case controllers_v2_EducationRequestController_readAll104_route(params) =>
      call { 
        controllers_v2_EducationRequestController_readAll104_invoker.call(EducationRequestController_16.get.readAll)
      }
  
    // @LINE:140
    case controllers_v2_EducationRequestController_read105_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_read105_invoker.call(EducationRequestController_16.get.read(id))
      }
  
    // @LINE:141
    case controllers_v2_EducationRequestController_update106_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_update106_invoker.call(EducationRequestController_16.get.update(id))
      }
  
    // @LINE:142
    case controllers_v2_EducationRequestController_delete107_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v2_EducationRequestController_delete107_invoker.call(EducationRequestController_16.get.delete(id))
      }
  
    // @LINE:143
    case controllers_v2_EducationRequestController_create108_route(params) =>
      call { 
        controllers_v2_EducationRequestController_create108_invoker.call(EducationRequestController_16.get.create)
      }
  
    // @LINE:145
    case controllers_v3_CartController_readAll109_route(params) =>
      call { 
        controllers_v3_CartController_readAll109_invoker.call(CartController_13.get.readAll)
      }
  
    // @LINE:146
    case controllers_v3_CartController_update110_route(params) =>
      call { 
        controllers_v3_CartController_update110_invoker.call(CartController_13.get.update)
      }
  
    // @LINE:147
    case controllers_v3_CartController_delete111_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_v3_CartController_delete111_invoker.call(CartController_13.get.delete(id))
      }
  
    // @LINE:149
    case controllers_v3_ResourceSubCategoriesController_readAll112_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("categoryId", None)) { (resourceType, categoryId) =>
        controllers_v3_ResourceSubCategoriesController_readAll112_invoker.call(ResourceSubCategoriesController_21.get.readAll(resourceType, categoryId))
      }
  
    // @LINE:150
    case controllers_v3_ResourceSubCategoriesController_read113_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_read113_invoker.call(ResourceSubCategoriesController_21.get.read(resourceType, id))
      }
  
    // @LINE:151
    case controllers_v3_ResourceSubCategoriesController_create114_route(params) =>
      call(params.fromPath[String]("resourceType", None)) { (resourceType) =>
        controllers_v3_ResourceSubCategoriesController_create114_invoker.call(ResourceSubCategoriesController_21.get.create(resourceType))
      }
  
    // @LINE:152
    case controllers_v3_ResourceSubCategoriesController_update115_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_update115_invoker.call(ResourceSubCategoriesController_21.get.update(resourceType, id))
      }
  
    // @LINE:153
    case controllers_v3_ResourceSubCategoriesController_delete116_route(params) =>
      call(params.fromPath[String]("resourceType", None), params.fromPath[Long]("id", None)) { (resourceType, id) =>
        controllers_v3_ResourceSubCategoriesController_delete116_invoker.call(ResourceSubCategoriesController_21.get.delete(resourceType, id))
      }
  }
}
