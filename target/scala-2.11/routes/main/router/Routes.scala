
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/rifluxyss/ScalaProjects/git/13.0/conf/routes
// @DATE:Fri Jun 19 20:14:06 IST 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:5
  v1_Routes_1: v1.Routes,
  // @LINE:6
  v2_Routes_0: v2.Routes,
  // @LINE:7
  v3_Routes_3: v3.Routes,
  // @LINE:8
  v4_Routes_2: v4.Routes,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:5
    v1_Routes_1: v1.Routes,
    // @LINE:6
    v2_Routes_0: v2.Routes,
    // @LINE:7
    v3_Routes_3: v3.Routes,
    // @LINE:8
    v4_Routes_2: v4.Routes
  ) = this(errorHandler, v1_Routes_1, v2_Routes_0, v3_Routes_3, v4_Routes_2, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, v1_Routes_1, v2_Routes_0, v3_Routes_3, v4_Routes_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    prefixed_v1_Routes_1_0.router.documentation,
    prefixed_v2_Routes_0_1.router.documentation,
    prefixed_v3_Routes_3_2.router.documentation,
    prefixed_v4_Routes_2_3.router.documentation,
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:5
  private[this] val prefixed_v1_Routes_1_0 = Include(v1_Routes_1.withPrefix(this.prefix + (if (this.prefix.endsWith("/")) "" else "/") + "v1"))

  // @LINE:6
  private[this] val prefixed_v2_Routes_0_1 = Include(v2_Routes_0.withPrefix(this.prefix + (if (this.prefix.endsWith("/")) "" else "/") + "v2"))

  // @LINE:7
  private[this] val prefixed_v3_Routes_3_2 = Include(v3_Routes_3.withPrefix(this.prefix + (if (this.prefix.endsWith("/")) "" else "/") + "v3"))

  // @LINE:8
  private[this] val prefixed_v4_Routes_2_3 = Include(v4_Routes_2.withPrefix(this.prefix + (if (this.prefix.endsWith("/")) "" else "/") + "v4"))


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:5
    case prefixed_v1_Routes_1_0(handler) => handler
  
    // @LINE:6
    case prefixed_v2_Routes_0_1(handler) => handler
  
    // @LINE:7
    case prefixed_v3_Routes_3_2(handler) => handler
  
    // @LINE:8
    case prefixed_v4_Routes_2_3(handler) => handler
  }
}
