
package views.txt

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object forgotPassword_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.txt._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class forgotPassword extends BaseScalaTemplate[play.twirl.api.TxtFormat.Appendable,Format[play.twirl.api.TxtFormat.Appendable]](play.twirl.api.TxtFormat) with play.twirl.api.Template3[String,String,String,play.twirl.api.TxtFormat.Appendable] {

  /**/
  def apply/*1.2*/(fullName: String, baseUrl: String, token: String):play.twirl.api.TxtFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.52*/("""

"""),format.raw/*3.1*/("""Hi """),_display_(/*3.5*/fullName),format.raw/*3.13*/(""",

Please use the link below to reset your password.

"""),_display_(/*7.2*/baseUrl),format.raw/*7.9*/("""/reset/"""),_display_(/*7.17*/token),format.raw/*7.22*/("""

"""),format.raw/*9.1*/("""If you did not request to reset your password, ignore this email.
"""))
      }
    }
  }

  def render(fullName:String,baseUrl:String,token:String): play.twirl.api.TxtFormat.Appendable = apply(fullName,baseUrl,token)

  def f:((String,String,String) => play.twirl.api.TxtFormat.Appendable) = (fullName,baseUrl,token) => apply(fullName,baseUrl,token)

  def ref: this.type = this

}


}

/**/
object forgotPassword extends forgotPassword_Scope0.forgotPassword
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/forgotPassword.scala.txt
                  HASH: 6815f6fb637b25d4395b97220018bfefd3f7b390
                  MATRIX: 553->1|697->51|725->53|754->57|782->65|862->120|888->127|922->135|947->140|975->142
                  LINES: 20->1|25->1|27->3|27->3|27->3|31->7|31->7|31->7|31->7|33->9
                  -- GENERATED --
              */
          