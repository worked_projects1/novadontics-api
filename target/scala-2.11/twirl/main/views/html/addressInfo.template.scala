
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addressInfo_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object addressInfo_Scope1 {
import models.daos.tables.OrderRow.Address

class addressInfo extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Option[Address],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(address: Option[Address]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.28*/("""

"""),_display_(/*4.2*/if(address.isDefined)/*4.23*/{_display_(Seq[Any](format.raw/*4.24*/("""
    """),format.raw/*5.5*/("""<div style="display: block">
        <p style="margin-bottom: 20px"><b>Shipping Address</b></p>
        <table width="40%" cellpadding="0" cellspacing="0" align="left">
            <tr>
                <td style="font-weight: 700">Name</td>
                <td>"""),_display_(/*10.22*/address/*10.29*/.get.name.getOrElse("")),format.raw/*10.52*/("""</td>
            </tr>
            <tr>
                <td style="font-weight: 700">Phone</td>
                <td>"""),_display_(/*14.22*/address/*14.29*/.get.phone),format.raw/*14.39*/("""</td>
            </tr>
            <tr>
                <td style="font-weight: 700">Street</td>
                <td>"""),_display_(/*18.22*/address/*18.29*/.get.address),format.raw/*18.41*/("""</td>
            </tr>
            <tr>
                <td style="font-weight: 700">City/State</td>
                <td>"""),_display_(/*22.22*/address/*22.29*/.get.city),format.raw/*22.38*/(""", """),_display_(/*22.41*/address/*22.48*/.get.state),format.raw/*22.58*/(""", """),_display_(/*22.61*/address/*22.68*/.get.zip),format.raw/*22.76*/("""</td>
            </tr>
            <tr>
                <td style="font-weight: 700">Country</td>
                <td>"""),_display_(/*26.22*/address/*26.29*/.get.country),format.raw/*26.41*/("""</td>
            </tr>
        </table>
    </div>
""")))}))
      }
    }
  }

  def render(address:Option[Address]): play.twirl.api.HtmlFormat.Appendable = apply(address)

  def f:((Option[Address]) => play.twirl.api.HtmlFormat.Appendable) = (address) => apply(address)

  def ref: this.type = this

}


}
}

/**/
object addressInfo extends addressInfo_Scope0.addressInfo_Scope1.addressInfo
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/addressInfo.scala.html
                  HASH: 2b02a77ff191c528ae89fcd9a772526c38f7b234
                  MATRIX: 625->45|746->71|774->74|803->95|841->96|872->101|1161->363|1177->370|1221->393|1366->511|1382->518|1413->528|1559->647|1575->654|1608->666|1758->789|1774->796|1804->805|1834->808|1850->815|1881->825|1911->828|1927->835|1956->843|2103->963|2119->970|2152->982
                  LINES: 23->2|28->2|30->4|30->4|30->4|31->5|36->10|36->10|36->10|40->14|40->14|40->14|44->18|44->18|44->18|48->22|48->22|48->22|48->22|48->22|48->22|48->22|48->22|48->22|52->26|52->26|52->26
                  -- GENERATED --
              */
          