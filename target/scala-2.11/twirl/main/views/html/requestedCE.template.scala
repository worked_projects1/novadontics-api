
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object requestedCE_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class requestedCE extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[String,String,String,String,String,Long,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dentistName: String, dentistEmail: String, date: String, courseTitle: String, courseId: String, ceRequestId: Long, note: String, courseCategory: String, speaker: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.172*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <style>
            th, td """),format.raw/*7.20*/("""{"""),format.raw/*7.21*/("""
                """),format.raw/*8.17*/("""text-align: left;
                padding: 5px;
            """),format.raw/*10.13*/("""}"""),format.raw/*10.14*/("""
            """),format.raw/*11.13*/("""table, th, td """),format.raw/*11.27*/("""{"""),format.raw/*11.28*/("""
                """),format.raw/*12.17*/("""border: 0px solid black;
                border-collapse: collapse;
            """),format.raw/*14.13*/("""}"""),format.raw/*14.14*/("""
            """),format.raw/*15.13*/("""</style>
    </head>

    <body>
        <h3> New Continuing Education request:</h3>
        <table align="left">
            <tr>
                <td> CE Request id: </td>
                <td> """),_display_(/*23.23*/ceRequestId),format.raw/*23.34*/(""" """),format.raw/*23.35*/("""</td>
            </tr>
            <tr>
                <td> Course title: </td>
                <td> """),_display_(/*27.23*/courseTitle),format.raw/*27.34*/(""" """),format.raw/*27.35*/("""</td>
            </tr>
            <tr>
                <td> Course Category: </td>
                <td> """),_display_(/*31.23*/courseCategory),format.raw/*31.37*/(""" """),format.raw/*31.38*/("""</td>
            </tr>
            <tr>
                <td> Speaker: </td>
                <td> """),_display_(/*35.23*/speaker),format.raw/*35.30*/(""" """),format.raw/*35.31*/("""</td>
            </tr>
            <tr>
                <td> Course id: </td>
                <td> """),_display_(/*39.23*/courseId),format.raw/*39.31*/("""</td>
            </tr>
            <tr>
                <td> Dentist name: </td>
                <td> """),_display_(/*43.23*/dentistName),format.raw/*43.34*/(""" """),format.raw/*43.35*/("""</td>
            </tr>
            <tr>
                <td> Dentist email: </td>
                <td> """),_display_(/*47.23*/dentistEmail),format.raw/*47.35*/(""" """),format.raw/*47.36*/("""</td>
            </tr>
            <tr>
                <td> Additional note: </td>
                <td> """),_display_(/*51.23*/note),format.raw/*51.27*/(""" """),format.raw/*51.28*/("""</td>
            </tr>
            <tr>
                <td> Date requested: </td>
                <td> """),_display_(/*55.23*/date),format.raw/*55.27*/(""" """),format.raw/*55.28*/("""</td>
            </tr>
        </table>
    </body>

</html>
"""))
      }
    }
  }

  def render(dentistName:String,dentistEmail:String,date:String,courseTitle:String,courseId:String,ceRequestId:Long,note:String,courseCategory:String,speaker:String): play.twirl.api.HtmlFormat.Appendable = apply(dentistName,dentistEmail,date,courseTitle,courseId,ceRequestId,note,courseCategory,speaker)

  def f:((String,String,String,String,String,Long,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (dentistName,dentistEmail,date,courseTitle,courseId,ceRequestId,note,courseCategory,speaker) => apply(dentistName,dentistEmail,date,courseTitle,courseId,ceRequestId,note,courseCategory,speaker)

  def ref: this.type = this

}


}

/**/
object requestedCE extends requestedCE_Scope0.requestedCE
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/requestedCE.scala.html
                  HASH: 516a1a22e6c78bca9e9e9d647285b314b7a8fed5
                  MATRIX: 593->1|859->171|886->172|1027->286|1055->287|1099->304|1187->364|1216->365|1257->378|1299->392|1328->393|1373->410|1481->490|1510->491|1551->504|1773->699|1805->710|1834->711|1965->815|1997->826|2026->827|2160->934|2195->948|2224->949|2350->1048|2378->1055|2407->1056|2535->1157|2564->1165|2695->1269|2727->1280|2756->1281|2888->1386|2921->1398|2950->1399|3084->1506|3109->1510|3138->1511|3271->1617|3296->1621|3325->1622
                  LINES: 20->1|25->1|26->2|31->7|31->7|32->8|34->10|34->10|35->11|35->11|35->11|36->12|38->14|38->14|39->15|47->23|47->23|47->23|51->27|51->27|51->27|55->31|55->31|55->31|59->35|59->35|59->35|63->39|63->39|67->43|67->43|67->43|71->47|71->47|71->47|75->51|75->51|75->51|79->55|79->55|79->55
                  -- GENERATED --
              */
          