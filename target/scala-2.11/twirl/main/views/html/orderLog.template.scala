
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object orderLog_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object orderLog_Scope1 {
import controllers.v1.OrdersController.Output.Order
import models.daos.tables.OrderRow.Address

class orderLog extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(orderId: Long,userAgent:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.34*/("""

"""),format.raw/*5.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
       
    </head>

    <body>
       <strong>Order id:</strong>"""),_display_(/*13.35*/orderId),format.raw/*13.42*/("""<br>
       <strong>User Agent:</strong>"""),_display_(/*14.37*/userAgent),format.raw/*14.46*/("""
    """),format.raw/*15.5*/("""</body>
</html>
"""))
      }
    }
  }

  def render(orderId:Long,userAgent:String): play.twirl.api.HtmlFormat.Appendable = apply(orderId,userAgent)

  def f:((Long,String) => play.twirl.api.HtmlFormat.Appendable) = (orderId,userAgent) => apply(orderId,userAgent)

  def ref: this.type = this

}


}
}

/**/
object orderLog extends orderLog_Scope0.orderLog_Scope1.orderLog
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/orderLog.scala.html
                  HASH: 98585833fcc6c27a9747786c24cdcce908ee55bf
                  MATRIX: 664->98|791->130|819->132|987->273|1015->280|1083->321|1113->330|1145->335
                  LINES: 24->3|29->3|31->5|39->13|39->13|40->14|40->14|41->15
                  -- GENERATED --
              */
          