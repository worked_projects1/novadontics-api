
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object patientFeeNotification_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class patientFeeNotification extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[String,String,String,Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(adminName: String, dentistName: String, dentistEmail: String, treatmentId: Long, patient: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.100*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style>
                th, td """),format.raw/*7.24*/("""{"""),format.raw/*7.25*/("""
                    """),format.raw/*8.21*/("""text-align: left;
                    padding: 5px;
                """),format.raw/*10.17*/("""}"""),format.raw/*10.18*/("""
                """),format.raw/*11.17*/("""table, th, td """),format.raw/*11.31*/("""{"""),format.raw/*11.32*/("""
                    """),format.raw/*12.21*/("""border: 0px solid black;
                    border-collapse: collapse;
                """),format.raw/*14.17*/("""}"""),format.raw/*14.18*/("""
        """),format.raw/*15.9*/("""</style>
    </head>

    <body>
        <h2> Hello, """),_display_(/*19.22*/adminName),format.raw/*19.31*/(""". </h2>
        <h3> """),_display_(/*20.15*/patient),format.raw/*20.22*/(""" """),format.raw/*20.23*/("""started Visit One </h3>
        <table align="left">
            <tr>
                <td> Treatment id: </td>
                <td> """),_display_(/*24.23*/treatmentId),format.raw/*24.34*/(""" """),format.raw/*24.35*/("""</td>
            </tr>
            <tr>
                <td> Dentist name: </td>
                <td> """),_display_(/*28.23*/dentistName),format.raw/*28.34*/(""" """),format.raw/*28.35*/("""</td>
            </tr>
            <tr>
                <td> Dentist email: </td>
                <td> """),_display_(/*32.23*/dentistEmail),format.raw/*32.35*/(""" """),format.raw/*32.36*/("""</td>
            </tr>
            <tr>
                <td> Patient name: </td>
                <td> """),_display_(/*36.23*/patient),format.raw/*36.30*/(""" """),format.raw/*36.31*/("""</td>
            </tr>
        </table>
    </body>
</html>
"""))
      }
    }
  }

  def render(adminName:String,dentistName:String,dentistEmail:String,treatmentId:Long,patient:String): play.twirl.api.HtmlFormat.Appendable = apply(adminName,dentistName,dentistEmail,treatmentId,patient)

  def f:((String,String,String,Long,String) => play.twirl.api.HtmlFormat.Appendable) = (adminName,dentistName,dentistEmail,treatmentId,patient) => apply(adminName,dentistName,dentistEmail,treatmentId,patient)

  def ref: this.type = this

}


}

/**/
object patientFeeNotification extends patientFeeNotification_Scope0.patientFeeNotification
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/patientFeeNotification.scala.html
                  HASH: 6ce780f7733d39922beaba189b266254cc5771f7
                  MATRIX: 587->1|781->99|808->100|949->214|977->215|1025->236|1121->304|1150->305|1195->322|1237->336|1266->337|1315->358|1431->446|1460->447|1496->456|1577->510|1607->519|1656->541|1684->548|1713->549|1873->682|1905->693|1934->694|2065->798|2097->809|2126->810|2258->915|2291->927|2320->928|2451->1032|2479->1039|2508->1040
                  LINES: 20->1|25->1|26->2|31->7|31->7|32->8|34->10|34->10|35->11|35->11|35->11|36->12|38->14|38->14|39->15|43->19|43->19|44->20|44->20|44->20|48->24|48->24|48->24|52->28|52->28|52->28|56->32|56->32|56->32|60->36|60->36|60->36
                  -- GENERATED --
              */
          