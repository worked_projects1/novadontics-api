
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object vendorOrder_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object vendorOrder_Scope1 {
import controllers.v1.OrdersController.Output.Order
import models.daos.tables.OrderRow.Address

class vendorOrder extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Double,Order.Dentist,scala.Tuple3[List[Order.Item], Double, String],Option[Address],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(total: Double, dentist: Order.Dentist, order: (List[Order.Item], Double, String), address: Option[Address]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.110*/("""

"""),format.raw/*5.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style>
            th, td """),format.raw/*10.20*/("""{"""),format.raw/*10.21*/("""
                """),format.raw/*11.17*/("""text-align: center;
                padding: 5px;
            """),format.raw/*13.13*/("""}"""),format.raw/*13.14*/("""
            
            """),format.raw/*15.13*/("""table, th, td """),format.raw/*15.27*/("""{"""),format.raw/*15.28*/("""
                """),format.raw/*16.17*/("""border: 1px solid black;
                border-collapse: collapse;
            """),format.raw/*18.13*/("""}"""),format.raw/*18.14*/("""

            """),format.raw/*20.13*/(""".itemImage """),format.raw/*20.24*/("""{"""),format.raw/*20.25*/("""
                """),format.raw/*21.17*/("""width: 65px;
                height: 65px;
                object-fit: contain;
            """),format.raw/*24.13*/("""}"""),format.raw/*24.14*/("""
            """),format.raw/*25.13*/(""".overnight """),format.raw/*25.24*/("""{"""),format.raw/*25.25*/("""
                """),format.raw/*26.17*/("""font-size: 15px;
                color: #B33A3A;
            """),format.raw/*28.13*/("""}"""),format.raw/*28.14*/("""
         """),format.raw/*29.10*/("""</style>
    </head>

    <body>
        <p>
            <label> New order from <b>"""),_display_(/*34.40*/dentist/*34.47*/.practice),format.raw/*34.56*/("""</b> dental practice </label>
            <br/>
            <label> via Novadontics, LLC.</label>
        </p>
    <div>
        """),_display_(/*39.10*/if(order._1.head.overnight.getOrElse(false))/*39.54*/{_display_(Seq[Any](format.raw/*39.55*/("""
            """),format.raw/*40.13*/("""<p class="overnight"><b>Order needs to be shipped overnight.</b></p>
        """)))}),format.raw/*41.10*/("""
        """),format.raw/*42.9*/("""<table width="100%" cellpadding="0" cellspacing="0" align="left">
            <tr>
                <th></th>
                <th> Reference Number </th>
                <th> Vendor </th>
                <th> Product Name </th>
                <th> Quantity </th>
                <th> Price </th>
                <th> Total </th>
            </tr>
            """),_display_(/*52.14*/for(item <- order._1) yield /*52.35*/{_display_(Seq[Any](format.raw/*52.36*/("""
                """),format.raw/*53.17*/("""<tr>
                    <td><img class="itemImage" src=""""),_display_(/*54.54*/item/*54.58*/.imageUrl.getOrElse("https://s3.amazonaws.com/ds-static.spfr.co/img/placeholder.jpg")),format.raw/*54.143*/(""""/></td>
                    <td style="text-align: right"> """),_display_(/*55.53*/item/*55.57*/.serialNumber),format.raw/*55.70*/(""" """),format.raw/*55.71*/("""</td>
                    <td> """),_display_(/*56.27*/item/*56.31*/.manufacturer),format.raw/*56.44*/(""" """),format.raw/*56.45*/("""</td>
                    <td> """),_display_(/*57.27*/item/*57.31*/.name),format.raw/*57.36*/(""" """),format.raw/*57.37*/("""</td>
                    <td style="text-align: right"> """),_display_(/*58.53*/(item.amount)),format.raw/*58.66*/(""" """),format.raw/*58.67*/("""</td>
                    <td style="text-align: right"> $"""),_display_(/*59.54*/("%.2f".format(item.price))),format.raw/*59.81*/(""" """),format.raw/*59.82*/("""</td>
                    <td style="text-align: right"> $"""),_display_(/*60.54*/("%.2f".format(item.price * item.amount))),format.raw/*60.95*/(""" """),format.raw/*60.96*/("""</td>
                </tr>
            """)))}),format.raw/*62.14*/("""
            """),format.raw/*63.13*/("""<tr>
                <td colspan="6" align="right"></td>
                <td style="text-align: right; background: lavender"> $"""),_display_(/*65.72*/("%.2f".format(order._2))),format.raw/*65.97*/(""" """),format.raw/*65.98*/("""</td>
            </tr>
        </table>
    </div>
    <p><br>&emsp;</p>
    """),_display_(/*70.6*/addressInfo(address)),format.raw/*70.26*/("""
    """),format.raw/*71.5*/("""</body>
</html>
"""))
      }
    }
  }

  def render(total:Double,dentist:Order.Dentist,order:scala.Tuple3[List[Order.Item], Double, String],address:Option[Address]): play.twirl.api.HtmlFormat.Appendable = apply(total,dentist,order,address)

  def f:((Double,Order.Dentist,scala.Tuple3[List[Order.Item], Double, String],Option[Address]) => play.twirl.api.HtmlFormat.Appendable) = (total,dentist,order,address) => apply(total,dentist,order,address)

  def ref: this.type = this

}


}
}

/**/
object vendorOrder extends vendorOrder_Scope0.vendorOrder_Scope1.vendorOrder
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/vendorOrder.scala.html
                  HASH: 8be9a4820bc99fb204ace70ba7c192d51c14c196
                  MATRIX: 745->98|949->206|977->208|1115->318|1144->319|1189->336|1279->398|1308->399|1362->425|1404->439|1433->440|1478->457|1586->537|1615->538|1657->552|1696->563|1725->564|1770->581|1890->673|1919->674|1960->687|1999->698|2028->699|2073->716|2162->777|2191->778|2229->788|2340->872|2356->879|2386->888|2543->1018|2596->1062|2635->1063|2676->1076|2785->1154|2821->1163|3208->1523|3245->1544|3284->1545|3329->1562|3414->1620|3427->1624|3534->1709|3622->1770|3635->1774|3669->1787|3698->1788|3757->1820|3770->1824|3804->1837|3833->1838|3892->1870|3905->1874|3931->1879|3960->1880|4045->1938|4079->1951|4108->1952|4194->2011|4242->2038|4271->2039|4357->2098|4419->2139|4448->2140|4520->2181|4561->2194|4716->2322|4762->2347|4791->2348|4896->2427|4937->2447|4969->2452
                  LINES: 24->3|29->3|31->5|36->10|36->10|37->11|39->13|39->13|41->15|41->15|41->15|42->16|44->18|44->18|46->20|46->20|46->20|47->21|50->24|50->24|51->25|51->25|51->25|52->26|54->28|54->28|55->29|60->34|60->34|60->34|65->39|65->39|65->39|66->40|67->41|68->42|78->52|78->52|78->52|79->53|80->54|80->54|80->54|81->55|81->55|81->55|81->55|82->56|82->56|82->56|82->56|83->57|83->57|83->57|83->57|84->58|84->58|84->58|85->59|85->59|85->59|86->60|86->60|86->60|88->62|89->63|91->65|91->65|91->65|96->70|96->70|97->71
                  -- GENERATED --
              */
          