
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object requestedConsult_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class requestedConsult extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[String,String,String,String,Option[Long],Long,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(adminName: String, dentistName: String, dentistEmail: String, date: String, treatmentId: Option[Long], consultationId: Long, consultType: String, consultant: String, patient: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.185*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <style>
            th, td """),format.raw/*7.20*/("""{"""),format.raw/*7.21*/("""
                """),format.raw/*8.17*/("""text-align: left;
                padding: 5px;
            """),format.raw/*10.13*/("""}"""),format.raw/*10.14*/("""
            """),format.raw/*11.13*/("""table, th, td """),format.raw/*11.27*/("""{"""),format.raw/*11.28*/("""
                """),format.raw/*12.17*/("""border: 0px solid black;
                border-collapse: collapse;
            """),format.raw/*14.13*/("""}"""),format.raw/*14.14*/("""
            """),format.raw/*15.13*/("""</style>
    </head>

    <body>
        <h2> Hello, """),_display_(/*19.22*/adminName),format.raw/*19.31*/(""" """),format.raw/*19.32*/("""</h2>
        <h3> """),_display_(/*20.15*/consultType),format.raw/*20.26*/(""" """),format.raw/*20.27*/("""for """),_display_(/*20.32*/consultant),format.raw/*20.42*/("""</h3>
        <h3> from  """),_display_(/*21.21*/dentistName),format.raw/*21.32*/("""</h3>
        <h3> Consultation info: </h3>
        <table align="left">
            <tr>
                <td> Consultation id: </td>
                <td> """),_display_(/*26.23*/consultationId),format.raw/*26.37*/(""" """),format.raw/*26.38*/("""</td>
            </tr>
            """),_display_(/*28.14*/if(treatmentId != null)/*28.37*/ {_display_(Seq[Any](format.raw/*28.39*/("""
                """),format.raw/*29.17*/("""<tr>
                    <td> Treatment id: </td>
                    <td> """),_display_(/*31.27*/treatmentId),format.raw/*31.38*/(""" """),format.raw/*31.39*/("""</td>
                </tr>
            """)))}),format.raw/*33.14*/("""
            """),format.raw/*34.13*/("""<tr>
                <td> Dentist name: </td>
                <td> """),_display_(/*36.23*/dentistName),format.raw/*36.34*/(""" """),format.raw/*36.35*/("""</td>
            </tr>
            <tr>
                <td> Dentist email: </td>
                <td> """),_display_(/*40.23*/dentistEmail),format.raw/*40.35*/(""" """),format.raw/*40.36*/("""</td>
            </tr>
            <tr>
                <td> Patient name: </td>
                <td> """),_display_(/*44.23*/patient),format.raw/*44.30*/(""" """),format.raw/*44.31*/("""</td>
            </tr>
            <tr>
                <td> Date requested: </td>
                <td> """),_display_(/*48.23*/date),format.raw/*48.27*/(""" """),format.raw/*48.28*/("""</td>
            </tr>
        </table>
    </body>

</html>
"""))
      }
    }
  }

  def render(adminName:String,dentistName:String,dentistEmail:String,date:String,treatmentId:Option[Long],consultationId:Long,consultType:String,consultant:String,patient:String): play.twirl.api.HtmlFormat.Appendable = apply(adminName,dentistName,dentistEmail,date,treatmentId,consultationId,consultType,consultant,patient)

  def f:((String,String,String,String,Option[Long],Long,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (adminName,dentistName,dentistEmail,date,treatmentId,consultationId,consultType,consultant,patient) => apply(adminName,dentistName,dentistEmail,date,treatmentId,consultationId,consultType,consultant,patient)

  def ref: this.type = this

}


}

/**/
object requestedConsult extends requestedConsult_Scope0.requestedConsult
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/requestedConsult.scala.html
                  HASH: 6088da70baab989611f5f821cc0c5e5e65eaac86
                  MATRIX: 609->1|888->184|915->185|1056->299|1084->300|1128->317|1216->377|1245->378|1286->391|1328->405|1357->406|1402->423|1510->503|1539->504|1580->517|1661->571|1691->580|1720->581|1767->601|1799->612|1828->613|1860->618|1891->628|1944->654|1976->665|2159->821|2194->835|2223->836|2287->873|2319->896|2359->898|2404->915|2507->991|2539->1002|2568->1003|2640->1044|2681->1057|2776->1125|2808->1136|2837->1137|2969->1242|3002->1254|3031->1255|3162->1359|3190->1366|3219->1367|3352->1473|3377->1477|3406->1478
                  LINES: 20->1|25->1|26->2|31->7|31->7|32->8|34->10|34->10|35->11|35->11|35->11|36->12|38->14|38->14|39->15|43->19|43->19|43->19|44->20|44->20|44->20|44->20|44->20|45->21|45->21|50->26|50->26|50->26|52->28|52->28|52->28|53->29|55->31|55->31|55->31|57->33|58->34|60->36|60->36|60->36|64->40|64->40|64->40|68->44|68->44|68->44|72->48|72->48|72->48
                  -- GENERATED --
              */
          