
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object cartLog_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class cartLog extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(userId:Long,userAgent:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
       
    </head>

    <body>
       <strong>User id:</strong>"""),_display_(/*11.34*/userId),format.raw/*11.40*/("""<br>
       <strong>User Agent:</strong>"""),_display_(/*12.37*/userAgent),format.raw/*12.46*/("""
    """),format.raw/*13.5*/("""</body>
</html>
"""))
      }
    }
  }

  def render(userId:Long,userAgent:String): play.twirl.api.HtmlFormat.Appendable = apply(userId,userAgent)

  def f:((Long,String) => play.twirl.api.HtmlFormat.Appendable) = (userId,userAgent) => apply(userId,userAgent)

  def ref: this.type = this

}


}

/**/
object cartLog extends cartLog_Scope0.cartLog
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/cartLog.scala.html
                  HASH: ba7e77fbcfc46aa376419ccc19d085140a88277d
                  MATRIX: 536->1|661->31|689->33|856->173|883->179|951->220|981->229|1013->234
                  LINES: 20->1|25->1|27->3|35->11|35->11|36->12|36->12|37->13
                  -- GENERATED --
              */
          