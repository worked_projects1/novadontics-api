
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object signUpRequest_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object signUpRequest_Scope1 {
import controllers.v1.Output.Account

class signUpRequest extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Account,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(account: Account, baseUrl: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.37*/("""

"""),format.raw/*4.1*/("""<html lang="en">
<head>
   <style>
            th, td """),format.raw/*7.20*/("""{"""),format.raw/*7.21*/("""
                """),format.raw/*8.17*/("""text-align: left;
                padding: 5px;
            """),format.raw/*10.13*/("""}"""),format.raw/*10.14*/("""
           """),format.raw/*11.12*/("""td"""),format.raw/*11.14*/("""{"""),format.raw/*11.15*/("""
             """),format.raw/*12.14*/("""padding-left: 10px;
            """),format.raw/*13.13*/("""}"""),format.raw/*13.14*/("""
           
    """),format.raw/*15.5*/("""</style>
</head>
<body>

<h3> Sign Up Request</h3>

We have received sign up request from below user. <br>

<br/>
<table>
    <tr>
        <td style="width:150px"><b>Title</b></td>
        <td>"""),_display_(/*27.14*/account/*27.21*/.title),format.raw/*27.27*/("""</td>
    </tr>
    <tr>
        <td><b>First Name</b></td>
        <td>"""),_display_(/*31.14*/account/*31.21*/.firstName),format.raw/*31.31*/("""</td>
    </tr>
    <tr>
        <td><b>Last Name</b></td>
        <td>"""),_display_(/*35.14*/account/*35.21*/.lastName),format.raw/*35.30*/("""</td>
    </tr>
    <tr>
        <td><b>Email</b></td>
        <td>"""),_display_(/*39.14*/account/*39.21*/.email),format.raw/*39.27*/("""</td>
    </tr>
    <tr>
        <td><b>Address</b></td>
        <td>"""),_display_(/*43.14*/account/*43.21*/.address),format.raw/*43.29*/("""</td>
    </tr>
    <tr>
        <td><b>City</b></td>
        <td>"""),_display_(/*47.14*/account/*47.21*/.city),format.raw/*47.26*/("""</td>
    </tr>
    <tr>
        <td><b>State</b></td>
        <td>"""),_display_(/*51.14*/account/*51.21*/.state),format.raw/*51.27*/("""</td>
    </tr>
    <tr>
        <td><b>Zip</b></td>
        <td>"""),_display_(/*55.14*/account/*55.21*/.zip),format.raw/*55.25*/("""</td>
    </tr>
    <tr>
        <td><b>County</b></td>
        <td>"""),_display_(/*59.14*/account/*59.21*/.county),format.raw/*59.28*/("""</td>
    </tr>
    <tr>
        <td><b>Business Phone</b></td>
        <td>"""),_display_(/*63.14*/account/*63.21*/.businessPhone),format.raw/*63.35*/("""</td>
    </tr>
    <tr>
        <td><b>Cell Phone</b></td>
        <td>"""),_display_(/*67.14*/account/*67.21*/.cellPhone),format.raw/*67.31*/("""</td>
    </tr>
</table>
</body>
</html>
"""))
      }
    }
  }

  def render(account:Account,baseUrl:String): play.twirl.api.HtmlFormat.Appendable = apply(account,baseUrl)

  def f:((Account,String) => play.twirl.api.HtmlFormat.Appendable) = (account,baseUrl) => apply(account,baseUrl)

  def ref: this.type = this

}


}
}

/**/
object signUpRequest extends signUpRequest_Scope0.signUpRequest_Scope1.signUpRequest
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/signUpRequest.scala.html
                  HASH: 06b4dd51a44ecda11d6c9cb796b8908f9096dcd9
                  MATRIX: 624->39|754->74|782->76|863->130|891->131|935->148|1023->208|1052->209|1092->221|1122->223|1151->224|1193->238|1253->270|1282->271|1326->288|1547->482|1563->489|1590->495|1690->568|1706->575|1737->585|1836->657|1852->664|1882->673|1977->741|1993->748|2020->754|2117->824|2133->831|2162->839|2256->906|2272->913|2298->918|2393->986|2409->993|2436->999|2529->1065|2545->1072|2570->1076|2666->1145|2682->1152|2710->1159|2814->1236|2830->1243|2865->1257|2965->1330|2981->1337|3012->1347
                  LINES: 23->2|28->2|30->4|33->7|33->7|34->8|36->10|36->10|37->11|37->11|37->11|38->12|39->13|39->13|41->15|53->27|53->27|53->27|57->31|57->31|57->31|61->35|61->35|61->35|65->39|65->39|65->39|69->43|69->43|69->43|73->47|73->47|73->47|77->51|77->51|77->51|81->55|81->55|81->55|85->59|85->59|85->59|89->63|89->63|89->63|93->67|93->67|93->67
                  -- GENERATED --
              */
          