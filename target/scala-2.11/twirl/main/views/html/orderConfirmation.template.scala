
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object orderConfirmation_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object orderConfirmation_Scope1 {
import controllers.v1.OrdersController.Output.Order
import models.daos.tables.OrderRow.Address

class orderConfirmation extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template10[Long,Double,Order.Dentist,List[scala.Tuple3[List[Order.Item], Double, String]],Option[String],Option[Address],Float,Float,Float,Float,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(orderId: Long, total: Double, dentist: Order.Dentist, orders: List[(List[Order.Item], Double, String)], adminName: Option[String], address: Option[Address], shippingCharge:Float, tax:Float, creditCardFee:Float, totalAmountSaved:Float):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.237*/("""

"""),format.raw/*5.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style>
            th, td """),format.raw/*10.20*/("""{"""),format.raw/*10.21*/("""
                """),format.raw/*11.17*/("""text-align: center;
                padding: 5px;
            """),format.raw/*13.13*/("""}"""),format.raw/*13.14*/("""
            
            """),format.raw/*15.13*/("""table, th, td """),format.raw/*15.27*/("""{"""),format.raw/*15.28*/("""
                """),format.raw/*16.17*/("""border: 1px solid black;
                border-collapse: collapse;
            """),format.raw/*18.13*/("""}"""),format.raw/*18.14*/("""
            """),format.raw/*19.13*/(""".itemImage """),format.raw/*19.24*/("""{"""),format.raw/*19.25*/("""
                """),format.raw/*20.17*/("""width: 65px;
                height: 65px;
                object-fit: contain;
            """),format.raw/*23.13*/("""}"""),format.raw/*23.14*/("""
         """),format.raw/*24.10*/("""</style>
    </head>

    <body>
        """),_display_(/*28.10*/if(adminName.isEmpty)/*28.31*/{_display_(Seq[Any](format.raw/*28.32*/("""
            """),format.raw/*29.13*/("""<h3> Hi, """),_display_(/*29.23*/dentist/*29.30*/.name),format.raw/*29.35*/(""" """),format.raw/*29.36*/("""</h3>
            <h4> Here is the invoice for your order id: <strong>"""),_display_(/*30.66*/orderId),format.raw/*30.73*/("""</strong></h4>
        """)))}/*31.11*/else/*31.16*/{_display_(Seq[Any](format.raw/*31.17*/("""
            """),format.raw/*32.13*/("""<h3> Hi, """),_display_(/*32.23*/adminName),format.raw/*32.32*/(""" """),format.raw/*32.33*/("""</h3>
            <h4> """),_display_(/*33.19*/dentist/*33.26*/.name),format.raw/*33.31*/(""" """),format.raw/*33.32*/("""placed a new order, order id: <strong>"""),_display_(/*33.71*/orderId),format.raw/*33.78*/("""</strong></h4>
        """)))}),format.raw/*34.10*/("""
    """),format.raw/*35.5*/("""<div>
        <table width="100%" cellpadding="0" cellspacing="0" align="left">
            <tr>
                <th></th>
                <th> Reference Number </th>
                <th> Vendor </th>
                <th> Product Name </th>
                <th> Quantity </th>
                <th> Price </th>
                <th> Total </th>
            </tr>
            """),_display_(/*46.14*/for(order <- orders) yield /*46.34*/{_display_(Seq[Any](format.raw/*46.35*/("""
                """),format.raw/*47.17*/("""<tr>
                    <td colspan="7" align="right">
                    <b>"""),_display_(/*49.25*/(order._1.head.manufacturer)),format.raw/*49.53*/("""</b>
                    </td>
                </tr>
                """),_display_(/*52.18*/for(item <- order._1) yield /*52.39*/{_display_(Seq[Any](format.raw/*52.40*/("""
                    """),format.raw/*53.21*/("""<tr>
                        <td><img class="itemImage" src=""""),_display_(/*54.58*/item/*54.62*/.imageUrl.getOrElse("https://s3.amazonaws.com/ds-static.spfr.co/img/placeholder.jpg")),format.raw/*54.147*/(""""/></td>
                        <td style="text-align: right"> """),_display_(/*55.57*/item/*55.61*/.serialNumber),format.raw/*55.74*/(""" """),format.raw/*55.75*/("""</td>
                        <td> """),_display_(/*56.31*/item/*56.35*/.manufacturer),format.raw/*56.48*/(""" """),format.raw/*56.49*/("""</td>
                        <td> """),_display_(/*57.31*/item/*57.35*/.name),format.raw/*57.40*/(""" """),format.raw/*57.41*/("""</td>
                        <td style="text-align: right"> """),_display_(/*58.57*/(item.amount)),format.raw/*58.70*/(""" """),format.raw/*58.71*/("""</td>
                        <td style="text-align: right"> $ """),_display_(/*59.59*/("%.2f".format(item.price))),format.raw/*59.86*/(""" """),format.raw/*59.87*/("""</td>
                        <td style="text-align: right"> $ """),_display_(/*60.59*/("%.2f".format(item.price * item.amount))),format.raw/*60.100*/(""" """),format.raw/*60.101*/("""</td>
                    </tr>
                """)))}),format.raw/*62.18*/("""
                """),format.raw/*63.17*/("""<tr>
                    <td colspan="4"></td>
                    <td colspan="2" align="right">Sub Total</td>
                    <td style="text-align: right; background: lavender"> $"""),_display_(/*66.76*/("%.2f".format(order._2))),format.raw/*66.101*/(""" """),format.raw/*66.102*/("""</td>
                </tr>
                """),_display_(/*68.18*/if(order._1.exists(_.overnight.getOrElse(false)))/*68.67*/ {_display_(Seq[Any](format.raw/*68.69*/("""
                    """),format.raw/*69.21*/("""<tr>
                        <td colspan="4"></td>
                        <td colspan="2" align="right">
                         <b>Overnight shipping cost</b>
                        </td>
                        <td style="text-align: right; background: lavender"> $ 52.00 </td>
                    </tr>
                """)))}),format.raw/*76.18*/("""
            """)))}),format.raw/*77.14*/("""
            """),_display_(/*78.14*/defining(orders.map(order => order._1.exists(_.overnight.getOrElse(false))).count(_ == true))/*78.107*/ { overnightCount: Int =>_display_(Seq[Any](format.raw/*78.132*/("""
              """),format.raw/*79.15*/("""<tr><td colspan="7"></td></tr>
              <tr>
                  <td colspan="4" style="background-color: #F5F5F5"></td>
                  <td colspan="2" align="right">Item Cost</td>
                  <td style="text-align: right">$ """),_display_(/*83.52*/("%.2f".format(total))),format.raw/*83.74*/("""</td>
              </tr>
              <tr>
                  <td colspan="4" style="background-color: #F5F5F5"></td>
                  <td colspan="2" align="right">Overnight Shipping Cost</td>
                  <td style="text-align: right">$ """),_display_(/*88.52*/("%.2f".format(overnightCount.toFloat * 52))),format.raw/*88.96*/("""</td>
              </tr>
            <tr>
                <td colspan="4" style="background-color: #F5F5F5"></td>
                <td colspan="2" align="right">Ground Shipping Cost If Applicable</td>
                <td style="text-align: right">$ """),_display_(/*93.50*/("%.2f".format(shippingCharge))),format.raw/*93.81*/("""</td>
            </tr>
            <tr>
                <td colspan="4" style="background-color: #F5F5F5"></td>
                <td colspan="2" align="right">Sales Tax If Applicable</td>
                <td style="text-align: right">$ """),_display_(/*98.50*/("%.2f".format(tax))),format.raw/*98.70*/("""</td>
            </tr>
            <tr>
                <td colspan="4" style="background-color: #F5F5F5"></td>
                <td colspan="2" align="right">Processing Fee</td>
                <td style="text-align: right">$ """),_display_(/*103.50*/("%.2f".format(creditCardFee))),format.raw/*103.80*/("""</td>
            </tr>
            <tr>
                <td colspan="4" style="background-color: #F5F5F5"></td>
                <td colspan="2" align="right">Total Amount Saved</td>
                <td style="text-align: right">$ """),_display_(/*108.50*/("%.2f".format(totalAmountSaved))),format.raw/*108.83*/("""</td>
            </tr>
              <tr>
                  <td colspan="4" style="background-color: #F5F5F5"></td>
                  <td colspan="2" align="right"><b>Grand Total</b></td>
                  <td style="text-align: right"> <b>$ """),_display_(/*113.56*/("%.2f".format(total + shippingCharge + tax + creditCardFee + overnightCount * 52))),format.raw/*113.139*/("""</b> </td>
              </tr>
            """)))}),format.raw/*115.14*/("""
        """),format.raw/*116.9*/("""</table>
    </div>
    <p><br>&emsp;</p>
    """),_display_(/*119.6*/addressInfo(address)),format.raw/*119.26*/("""
    """),format.raw/*120.5*/("""<p><br>&emsp;</p>
    <div style="display: inline-block">
        <p>
            This order confirmation email serves as an invoice for your order. For any questions, please send an email to <a style="color: darkorange" href="team@novadontics.com">team@novadontics.com</a> citing your oder id number referenced above.
        </p>
    </div>
    </body>
</html>
"""))
      }
    }
  }

  def render(orderId:Long,total:Double,dentist:Order.Dentist,orders:List[scala.Tuple3[List[Order.Item], Double, String]],adminName:Option[String],address:Option[Address],shippingCharge:Float,tax:Float,creditCardFee:Float,totalAmountSaved:Float): play.twirl.api.HtmlFormat.Appendable = apply(orderId,total,dentist,orders,adminName,address,shippingCharge,tax,creditCardFee,totalAmountSaved)

  def f:((Long,Double,Order.Dentist,List[scala.Tuple3[List[Order.Item], Double, String]],Option[String],Option[Address],Float,Float,Float,Float) => play.twirl.api.HtmlFormat.Appendable) = (orderId,total,dentist,orders,adminName,address,shippingCharge,tax,creditCardFee,totalAmountSaved) => apply(orderId,total,dentist,orders,adminName,address,shippingCharge,tax,creditCardFee,totalAmountSaved)

  def ref: this.type = this

}


}
}

/**/
object orderConfirmation extends orderConfirmation_Scope0.orderConfirmation_Scope1.orderConfirmation
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/orderConfirmation.scala.html
                  HASH: fbaa2eaf0b6560c424ec8141eca3fe4876ffbce8
                  MATRIX: 814->98|1145->333|1173->335|1311->445|1340->446|1385->463|1475->525|1504->526|1558->552|1600->566|1629->567|1674->584|1782->664|1811->665|1852->678|1891->689|1920->690|1965->707|2085->799|2114->800|2152->810|2221->852|2251->873|2290->874|2331->887|2368->897|2384->904|2410->909|2439->910|2537->981|2565->988|2608->1013|2621->1018|2660->1019|2701->1032|2738->1042|2768->1051|2797->1052|2848->1076|2864->1083|2890->1088|2919->1089|2985->1128|3013->1135|3068->1159|3100->1164|3501->1538|3537->1558|3576->1559|3621->1576|3728->1656|3777->1684|3874->1754|3911->1775|3950->1776|3999->1797|4088->1859|4101->1863|4208->1948|4300->2013|4313->2017|4347->2030|4376->2031|4439->2067|4452->2071|4486->2084|4515->2085|4578->2121|4591->2125|4617->2130|4646->2131|4735->2193|4769->2206|4798->2207|4889->2271|4937->2298|4966->2299|5057->2363|5120->2404|5150->2405|5230->2454|5275->2471|5489->2658|5536->2683|5566->2684|5638->2729|5696->2778|5736->2780|5785->2801|6142->3127|6187->3141|6228->3155|6331->3248|6395->3273|6438->3288|6703->3526|6746->3548|7020->3795|7085->3839|7362->4089|7414->4120|7678->4357|7719->4377|7975->4605|8027->4635|8287->4867|8342->4900|8614->5144|8720->5227|8796->5271|8833->5280|8907->5327|8949->5347|8982->5352
                  LINES: 24->3|29->3|31->5|36->10|36->10|37->11|39->13|39->13|41->15|41->15|41->15|42->16|44->18|44->18|45->19|45->19|45->19|46->20|49->23|49->23|50->24|54->28|54->28|54->28|55->29|55->29|55->29|55->29|55->29|56->30|56->30|57->31|57->31|57->31|58->32|58->32|58->32|58->32|59->33|59->33|59->33|59->33|59->33|59->33|60->34|61->35|72->46|72->46|72->46|73->47|75->49|75->49|78->52|78->52|78->52|79->53|80->54|80->54|80->54|81->55|81->55|81->55|81->55|82->56|82->56|82->56|82->56|83->57|83->57|83->57|83->57|84->58|84->58|84->58|85->59|85->59|85->59|86->60|86->60|86->60|88->62|89->63|92->66|92->66|92->66|94->68|94->68|94->68|95->69|102->76|103->77|104->78|104->78|104->78|105->79|109->83|109->83|114->88|114->88|119->93|119->93|124->98|124->98|129->103|129->103|134->108|134->108|139->113|139->113|141->115|142->116|145->119|145->119|146->120
                  -- GENERATED --
              */
          