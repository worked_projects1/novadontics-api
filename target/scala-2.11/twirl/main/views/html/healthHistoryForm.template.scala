
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object healthHistoryForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object healthHistoryForm_Scope1 {
import controllers.v1.PatientsController.Output.HealthHisotryObj
import com.itextpdf.text.Image

class healthHistoryForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[HealthHisotryObj,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(healthHistory: HealthHisotryObj):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.35*/("""

"""),format.raw/*5.1*/("""<html lang="en">
  <head>
    <title>Health History Form</title>
    <style>
      .subTitle"""),format.raw/*9.16*/("""{"""),format.raw/*9.17*/("""
          """),format.raw/*10.11*/("""font-size:18px;
          font-weight:bold;
       """),format.raw/*12.8*/("""}"""),format.raw/*12.9*/("""
      """),format.raw/*13.7*/(""".fieldName"""),format.raw/*13.17*/("""{"""),format.raw/*13.18*/("""
          """),format.raw/*14.11*/("""font-size:18px;
          font-weight:normal
       """),format.raw/*16.8*/("""}"""),format.raw/*16.9*/("""
       """),format.raw/*17.8*/(""".fieldValue"""),format.raw/*17.19*/("""{"""),format.raw/*17.20*/("""
          """),format.raw/*18.11*/("""font-size:18px;
          borderBottom: 2px solid black;
       """),format.raw/*20.8*/("""}"""),format.raw/*20.9*/("""
       """),format.raw/*21.8*/(""".boldText"""),format.raw/*21.17*/("""{"""),format.raw/*21.18*/("""
         """),format.raw/*22.10*/("""font-weight:bold;
       """),format.raw/*23.8*/("""}"""),format.raw/*23.9*/("""  
    """),format.raw/*24.5*/("""</style>
  </head>  
    <body>
      <table width="90%" align="center" cellpadding="5" cellspacing="10"> 
          <tr>
            <td width="22%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="5%">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr><td colspan="5"  align="center"><img src="app/assets/logo.png" width="350px" height="75px"/></td></tr>   
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="3">&nbsp;</td>
              <td class="subTitle" colspan="2" align="right">Today's Date """),_display_(/*40.76*/healthHistory/*40.89*/.currentDate),format.raw/*40.101*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">PATIENT INFORMATION</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Patient Name</td>
              <td class="fieldValue">"""),_display_(/*49.39*/healthHistory/*49.52*/.full_name),format.raw/*49.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Preferred Name</td>
              <td class="fieldValue">"""),_display_(/*52.39*/healthHistory/*52.52*/.preferredName),format.raw/*52.66*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Address</td>
              <td class="fieldValue">"""),_display_(/*56.39*/healthHistory/*56.52*/.address),format.raw/*56.60*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">City</td>
              <td class="fieldValue">"""),_display_(/*59.39*/healthHistory/*59.52*/.city),format.raw/*59.57*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Home Phone</td>
              <td class="fieldValue">"""),_display_(/*63.39*/healthHistory/*63.52*/.homePhone),format.raw/*63.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Cell#</td>
              <td class="fieldValue">"""),_display_(/*66.39*/healthHistory/*66.52*/.phone),format.raw/*66.58*/("""</td>
          </tr>
         <tr>
              <td class="fieldName">Email</td>
              <td class="fieldValue">"""),_display_(/*70.39*/healthHistory/*70.52*/.email),format.raw/*70.58*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Gender</td>
              <td class="fieldValue">"""),_display_(/*73.39*/healthHistory/*73.52*/.gender),format.raw/*73.59*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Birth Date</td>
              <td class="fieldValue">"""),_display_(/*77.39*/healthHistory/*77.52*/.birth_date),format.raw/*77.63*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Marital	Status</td>
              <td class="fieldValue">"""),_display_(/*80.39*/healthHistory/*80.52*/.marital_status),format.raw/*80.67*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Social Security #</td>
              <td class="fieldValue">"""),_display_(/*84.39*/healthHistory/*84.52*/.socialSecurity),format.raw/*84.67*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Driver’s License #</td>
              <td class="fieldValue">"""),_display_(/*87.39*/healthHistory/*87.52*/.driverLicense),format.raw/*87.66*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Employer</td>
              <td class="fieldValue">"""),_display_(/*91.39*/healthHistory/*91.52*/.employer),format.raw/*91.61*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Occupation</td>
              <td class="fieldValue">"""),_display_(/*94.39*/healthHistory/*94.52*/.occupation),format.raw/*94.63*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Business Address</td>
              <td class="fieldValue">"""),_display_(/*98.39*/healthHistory/*98.52*/.businessAddress),format.raw/*98.68*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Business Phone</td>
              <td class="fieldValue">"""),_display_(/*101.39*/healthHistory/*101.52*/.businessPhone),format.raw/*101.66*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Name of Spouse</td>
              <td class="fieldValue">"""),_display_(/*105.39*/healthHistory/*105.52*/.nameOfSpouse),format.raw/*105.65*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Spouse’s Occupation</td>
              <td class="fieldValue">"""),_display_(/*108.39*/healthHistory/*108.52*/.spouseOccupaton),format.raw/*108.68*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Spouse’s Phone</td>
              <td class="fieldValue">"""),_display_(/*112.39*/healthHistory/*112.52*/.spousePhone),format.raw/*112.64*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Person To Contact in Emergency</td>
              <td class="fieldValue">"""),_display_(/*115.39*/healthHistory/*115.52*/.personToContact),format.raw/*115.68*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Emergency Contact Phone</td>
              <td class="fieldValue">"""),_display_(/*119.39*/healthHistory/*119.52*/.emergencyContact),format.raw/*119.69*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Whom May We Thank For Referring You</td>
              <td class="fieldValue">"""),_display_(/*122.39*/healthHistory/*122.52*/.referredBy),format.raw/*122.63*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">RESPONSIBLE PARTY</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Name of Responsible Party</td>
              <td class="fieldValue">"""),_display_(/*131.39*/healthHistory/*131.52*/.responsibleParty),format.raw/*131.69*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Relationship to Patient</td>
              <td class="fieldValue">"""),_display_(/*134.39*/healthHistory/*134.52*/.patientRelationship),format.raw/*134.72*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Address (if different than above)</td>
              <td class="fieldValue">"""),_display_(/*138.39*/healthHistory/*138.52*/.differentAddress),format.raw/*138.69*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Phone Number</td>
              <td class="fieldValue">"""),_display_(/*141.39*/healthHistory/*141.52*/.phoneNumber),format.raw/*141.64*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">MEDICAL INFORMATION</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Do You Have Medical Insurance Coverage</td>
              <td class="fieldValue">"""),_display_(/*150.39*/healthHistory/*150.52*/.medicalInsurance),format.raw/*150.69*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Physician Name</td>
              <td class="fieldValue">"""),_display_(/*153.39*/healthHistory/*153.52*/.physicianName),format.raw/*153.66*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Phone</td>
              <td class="fieldValue">"""),_display_(/*157.39*/healthHistory/*157.52*/.medicalInsurancePhone),format.raw/*157.74*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Last Exam</td>
              <td class="fieldValue">"""),_display_(/*160.39*/healthHistory/*160.52*/.lastExam),format.raw/*160.61*/("""</td>
          </tr>
          <tr>
             <td class="fieldName" colspan="5">Are you under medical treatment now? (Please list)</td>
          </tr>   
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*166.50*/healthHistory/*166.63*/.medicalTreatment),format.raw/*166.80*/("""</td>
          </tr>
          <tr>
             <td class="fieldName" colspan="5">Have you been hospitalized within the last five years? (Please list)</td>
          </tr>   
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*172.50*/healthHistory/*172.63*/.hospitalizedLastFiveYrs),format.raw/*172.87*/("""</td>
          </tr>
          <tr>
             <td class="fieldName" colspan="5">Are you taking any medication at this time? (Please list)</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*178.50*/healthHistory/*178.63*/.medicationList),format.raw/*178.78*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Have you ever taken Phen-Fen/Redux?</td>
              <td class="fieldValue">"""),_display_(/*182.39*/healthHistory/*182.52*/.takenRedux),format.raw/*182.63*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Do you use controlled substances?</td>
              <td class="fieldValue">"""),_display_(/*185.39*/healthHistory/*185.52*/.controlSubstances),format.raw/*185.70*/("""</td>
          </tr>
          <tr>
             <td class="fieldName" colspan="5">Do you use tobacco or tobacco products in any form (Please list)</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*191.50*/healthHistory/*191.63*/.tobaccoProducts),format.raw/*191.79*/("""</td>
          </tr>
          
          
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName"><span class="boldText">Women:</span> Are you pregnant or think you may be pregnant?</td>
              <td class="fieldValue">"""),_display_(/*198.39*/healthHistory/*198.52*/.pregnant),format.raw/*198.61*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">If yes, how many weeks?</td>
              <td class="fieldValue">"""),_display_(/*201.39*/healthHistory/*201.52*/.numberOfWeeks),format.raw/*201.66*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Are you nursing?</td>
              <td class="fieldValue">"""),_display_(/*205.39*/healthHistory/*205.52*/.nursing),format.raw/*205.60*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Do you take birth control pills?</td>
              <td class="fieldValue">"""),_display_(/*208.39*/healthHistory/*208.52*/.birthControlPills),format.raw/*208.70*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
             <td class="fieldName"  colspan="5"><span class="boldText">Allergic to (if any)</span></td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*215.50*/healthHistory/*215.63*/.allergicTo),format.raw/*215.74*/("""</td>
          </tr>   
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
             <td class="fieldName"  colspan="5"><span class="boldText">Present or past medical conditions (if any)</span></td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*222.50*/healthHistory/*222.63*/.medicalConditionList),format.raw/*222.84*/("""</td>
          </tr>   
          <div style="page-break-before:always">&nbsp;</div>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">PATIENT DENTAL HISTORY</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Name of Previous Dentist</td>
              <td class="fieldValue">"""),_display_(/*232.39*/healthHistory/*232.52*/.previousDentist),format.raw/*232.68*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Date of Last Exam and X-rays</td>
              <td class="fieldValue">"""),_display_(/*235.39*/healthHistory/*235.52*/.lastExamXray),format.raw/*235.65*/("""</td>
          </tr>
          <tr>
             <td class="fieldName"  colspan="5">Why are you changing dentists?</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*241.50*/healthHistory/*241.63*/.changingDentists),format.raw/*241.80*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Do your gums bleed while brushing or flossing?</td>
              <td class="fieldValue">"""),_display_(/*245.39*/healthHistory/*245.52*/.gumsBleed),format.raw/*245.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Sensitive to sweet or sour liquids/foods?</td>
              <td class="fieldValue">"""),_display_(/*248.39*/healthHistory/*248.52*/.sensitiveTeethSweetSour),format.raw/*248.76*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Are your teeth sensitive to hot or cold liquids/foods?</td>
              <td class="fieldValue">"""),_display_(/*252.39*/healthHistory/*252.52*/.sensitiveTeethHotCold),format.raw/*252.74*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Have you had any head neck or jaw injuries?</td>
              <td class="fieldValue">"""),_display_(/*255.39*/healthHistory/*255.52*/.head_neck_jaw_injuries),format.raw/*255.75*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Do you feel anything unusual in your mouth?</td>
              <td class="fieldValue">"""),_display_(/*259.39*/healthHistory/*259.52*/.feelAnythingUnusual),format.raw/*259.72*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Have you had orthodontic treatment?</td>
              <td class="fieldValue">"""),_display_(/*262.39*/healthHistory/*262.52*/.orthodonticTreatment),format.raw/*262.73*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Do you wear dentures or partials?</td>
              <td class="fieldValue">"""),_display_(/*266.39*/healthHistory/*266.52*/.wearDentures),format.raw/*266.65*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">lf yes, placement date? </td>
              <td class="fieldValue">"""),_display_(/*269.39*/healthHistory/*269.52*/.placementDate),format.raw/*269.66*/("""</td>
          </tr>
          <tr>
             <td class="fieldName"  colspan="5">Have you received instructions for the care of your teeth and gums?</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*275.50*/healthHistory/*275.63*/.careTeeth),format.raw/*275.73*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Do you like you, smile?</td>
              <td class="fieldValue">"""),_display_(/*279.39*/healthHistory/*279.52*/.likeSmile),format.raw/*279.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Do you clench or grind your teeth?</td>
              <td class="fieldValue">"""),_display_(/*282.39*/healthHistory/*282.52*/.clenchTeeth),format.raw/*282.64*/("""</td>
          </tr>
           <tr>
             <td class="fieldName"  colspan="5"><span class="boldText">Please select any problems you have experienced in your jaw joint (TMJ):</span></td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*288.50*/healthHistory/*288.63*/.problemsInJaw),format.raw/*288.77*/("""</td>
          </tr>
          <tr>
             <td class="fieldName"  colspan="5">Is there any other information we should know regarding your medical or dental health?</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*294.50*/healthHistory/*294.63*/.medicalHealth),format.raw/*294.77*/("""</td>
          </tr>   
          <tr>
             <td class="fieldName"  colspan="5">Do you wish to speak to the doctor privately about anything?</td>
          </tr>
          <tr>
             <td class="fieldValue" colspan="5">"""),_display_(/*300.50*/healthHistory/*300.63*/.speakDoctorPrivate),format.raw/*300.82*/("""</td>
          </tr>
          <tr>
            <td colspan="5">
              <p><br /><br /><br /> <br/>
                  I consent to the diagnostic imaging procedures deemed necessary by this office. I understand that the cone beam CT scanner produces images that are intended only for evaluation of the mandibular & maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. 3D CT scans are complimentary for diagnostic & treatment purposes for our in-house patients. There is a fee for replication of your scan onto a disc and/or print out, should you request for your personal use. I authorize my dentist and his/her designated staff to perform an oral examination for the purpose of diagnosis and treatment planning. Furthermore, I authorize the taking of all x-rays required as a necessary part of this examination. In addition, by signing this document I understand and agree to the above policy.
              </p>
            </td>
          </tr>
          <tr>
              <td class="fieldName" colspan="4">Patient’s Signature</td>
              <td class="fieldName" colspan="2">Date: """),_display_(/*311.56*/healthHistory/*311.69*/.date1),format.raw/*311.75*/("""</td>
          </tr>
          <tr>
              <td class="fieldName" colspan="3">
                """),_display_(/*315.18*/if(healthHistory.signature1 != "")/*315.52*/{_display_(Seq[Any](format.raw/*315.53*/("""
                    """),format.raw/*316.21*/("""<img src=""""),_display_(/*316.32*/healthHistory/*316.45*/.signature1),format.raw/*316.56*/("""" />
                """)))}),format.raw/*317.18*/("""
                """),format.raw/*318.17*/("""&nbsp;
              </td>
              <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="5" class="subTitle"> <br/> <br/>AUTHORIZATION AND RELEASE</td>
          </tr>
          <tr>
              <td colspan="5">
                  <p>l authorize the release of any information to third party payers and/or health practitioners. I agree to be responsible for payment of full services rendered on my behalf, or my dependents behalf. I understand that the consultation visit is provided at no charge. I certify I have read and understand the above information and that the information I have provided is accurate.</p>
              </td>
          </tr>
          <tr>
              <td class="fieldName" colspan="4">Patient Signature</td>
              <td class="fieldName" colspan="2">Date: """),_display_(/*332.56*/healthHistory/*332.69*/.patientDate),format.raw/*332.81*/("""</td>
          </tr>
          <tr>
              <td class="fieldName" colspan="3">
                  """),_display_(/*336.20*/if(healthHistory.patientSignature != "")/*336.60*/{_display_(Seq[Any](format.raw/*336.61*/("""
                  """),format.raw/*337.19*/("""<img src=""""),_display_(/*337.30*/healthHistory/*337.43*/.patientSignature),format.raw/*337.60*/("""" />
                  """)))}),format.raw/*338.20*/("""
                  """),format.raw/*339.19*/("""&nbsp;
              </td>
              <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="5" class="subTitle"> <br/> <br/>FINANCIAL POLICY</td>
          </tr>
          <tr>
              <td colspan="5">
                  <p>
                      We are committed to providing you with the best possible dental care. If you have dental insurance, we will be happy to help you receive your maximum allowable benefits. In order to achieve these goals, we need your assistance and understanding of our financial policy. We will gladly discuss any insurance questions you may have. However, you must realize that your insurance plan is a contract between you and/or your employer and the insurance company. While the filing of claims is a courtesy that we extend to our patients, all charges are your responsibility on the date the services are rendered regardless of insurance benefits.
                  </p>
              <br/><br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">

                  <p>
                      <u>Payments for services are due in full at the time of services rendered.</u>
                  </p>
              <br/><br/>

                  <p>
                      A 50% deposit is required for those patients who are starting any major dental treatment regardless of what your insurance will pay. We gladly accept Visa, Master Card, personal in-state checks, or cash.
                  </p>
               <br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">

                  <p>
                      If there is still a balance owing after your insurance company pays, that balance will be due and payable within 30 days. If there is a credit on the account, we will issue a refund check.
                  </p>
              <br/>

                  <p>Return checks and balances older than 60 days are subject to additional collection fees and interest charges of 0.83% per month, or 10% per year.
                  </p>
               <br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">

                  <p>Charges may also be made for broken appointments and appointments cancelled without a 48 hour notice. A charge of $125.00 will be charged for any hygiene broken appointment and $200.00-$500.00 for a broken appointment with the Doctor. If an account is turned into collections, you will be responsible for all legal fees.
                  </p>
               <br/>

                  <p><b>I have read the above and fully understand and accept the terms and conditions set forth. I authorize the release of any information relating to my dental claim. I understand I am responsible for all costs of dental treatment regardless of insurance.</b>
                  </p>
              </td>
          </tr>
          <tr>
              <td class="fieldName" colspan="4">Patient Signature</td>
              <td class="fieldName" colspan="2">Date: """),_display_(/*394.56*/healthHistory/*394.69*/.patientDate1),format.raw/*394.82*/("""</td>
          </tr>
          <tr>
              <td class="fieldName" colspan="3">
                  """),_display_(/*398.20*/if(healthHistory.patientSignature1 != "")/*398.61*/{_display_(Seq[Any](format.raw/*398.62*/("""
                  """),format.raw/*399.19*/("""<img src=""""),_display_(/*399.30*/healthHistory/*399.43*/.patientSignature1),format.raw/*399.61*/("""" />
                  """)))}),format.raw/*400.20*/("""
                  """),format.raw/*401.19*/("""&nbsp;
              </td>
              <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
              <td colspan="5" class="subTitle">NOTICE OF PRIVACY PRACTICES</td>
          </tr>
          <tr>
            <td colspan="5">
                  <p>
                      This notice describes how your health information may be used and disclosed by our office and how you can access this information. Please review this document and sign the acknowledgement attached to this form. At our office, we have always kept your health information secure and confidential. A new law requires us to continue maintaining your privacy in addition to providing you with this information and adhering to the new law set in place. This law allows us to:
                  </p><br/>
            </td>
          </tr>
          <tr>
              <td colspan="5">
                  <p>
                    <ul>
                        <li>
                            Use or disclose your health information to those involved in your health treatment, i.e. a review of your file by a specialist whom we may involve in your care.
                        </li>
                        <li>
                            We may use or disclose your health information for payment services,i.e.we may send a report of progress with your claim to your insurance company.
                        </li>
                        <li>
                            We may disclose your health information with our business associates, such as a dental laboratory. Business associates have a written contract with the Doctor, which requires them to protect your privacy.
                        </li>
                        <li>
                            We may use your information to contact you, i.e. newsletters, educational materials or other information. We may also contact you to confirm your appointments in lieu of your absence when the confirmation call is made our office may leave a message on your answering machine or with persons who answer the contact number provided.
                        </li>
                        <li>
                            In an emergency, we may also disclose your health information to a family member or another person responsible for your care.
                        </li>
                        <li>
                            We may also release your health information if required by law. Exceptions are as follows: we will not disclose your health information without prior written knowledge.
                        </li>
                    </ul>
                    </p>    <br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">
                    <p>
                        You may request in writing that we do not disclose your health information as described above, our office will inform you if that request cannot be met. You have the right to know any of the uses or disclosures of your health information beyond the normal uses. As we will need to contact you periodically we will use whatever address or telephone number you prefer.
                    </p><br/>
                    <p>
                        You have the right to transfer copies of your information to another practice. You have the right to view and receive a copy of your health information. A written request is needed to prepare the documents requested. All copies of x-rays and/or records will ensue a reasonable fee for copies of documents.
                    </p>  <br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">
                    <p>
                        You have the right to request an amendment or change to your health information. All requests should be submitted in writing to our office. If you wish to include a statement in your file, please submit this in writing. The changes requested will be made at the discretion of our office, no documents shall be removed or altered the file can only be changed with new information.
                    </p>    <br/>
                    <p>
                        All patients have a right to receive a copy of this notice. If any details in this notice have been changed or updated, we will notify of the changes in writing. Complaints can be filed with:
                    </p> <br/>
              </td>
          </tr>
          <tr>
              <td colspan="5">
                    <p>
                        Department of Health and Human Services 200
                    </p>
                    <p>
                        IndependentAvenue,S.W.,Room5009F Washington,
                    </p>
                    <p>
                        DC 20201
                    </p>    <br/>
                    <p>
                        By signing below, I acknowledge that I have received and read a copy of the Notice of Privacy Practices. I am in full understanding that the notice given to me may be updated in the future, and I will be notified of any amendments to this notice by telephone or mail.
                    </p>
            </td>
          </tr>
          <tr>
              <td class="fieldName" colspan="4">Name: """),_display_(/*478.56*/healthHistory/*478.69*/.name2),format.raw/*478.75*/("""</td>
              <td class="fieldName">Relationship to minor patient (if applicable): <br />"""),_display_(/*479.91*/healthHistory/*479.104*/.minor),format.raw/*479.110*/("""</td>
          </tr>
          <tr>
              <td class="fieldName" colspan="4">Patient Signature</td>
              <td class="fieldName">Date: """),_display_(/*483.44*/healthHistory/*483.57*/.patientDate2),format.raw/*483.70*/("""</td>
          </tr>
          <tr>
              <td class="fieldName" colspan="3">
                  """),_display_(/*487.20*/if(healthHistory.patientSignature2 != "")/*487.61*/{_display_(Seq[Any](format.raw/*487.62*/("""
                  """),format.raw/*488.19*/("""<img src=""""),_display_(/*488.30*/healthHistory/*488.43*/.patientSignature2),format.raw/*488.61*/("""" />
                  """)))}),format.raw/*489.20*/("""
                  """),format.raw/*490.19*/("""&nbsp;
              </td>
              <td colspan="2">&nbsp;</td>
          </tr>
      </table>  
    </body>
</html>
"""))
      }
    }
  }

  def render(healthHistory:HealthHisotryObj): play.twirl.api.HtmlFormat.Appendable = apply(healthHistory)

  def f:((HealthHisotryObj) => play.twirl.api.HtmlFormat.Appendable) = (healthHistory) => apply(healthHistory)

  def ref: this.type = this

}


}
}

/**/
object healthHistoryForm extends healthHistoryForm_Scope0.healthHistoryForm_Scope1.healthHistoryForm
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/healthHistoryForm.scala.html
                  HASH: f141f2f12299d653f62d986e25983deadf036d4d
                  MATRIX: 697->99|825->132|853->134|972->226|1000->227|1039->238|1117->289|1145->290|1179->297|1217->307|1246->308|1285->319|1364->371|1392->372|1427->380|1466->391|1495->392|1534->403|1625->467|1653->468|1688->476|1725->485|1754->486|1792->496|1844->521|1872->522|1906->529|2616->1212|2638->1225|2672->1237|3025->1563|3047->1576|3078->1586|3235->1716|3257->1729|3292->1743|3443->1867|3465->1880|3494->1888|3641->2008|3663->2021|3689->2026|3843->2153|3865->2166|3896->2176|4044->2297|4066->2310|4093->2316|4241->2437|4263->2450|4290->2456|4439->2578|4461->2591|4489->2598|4643->2725|4665->2738|4697->2749|4854->2879|4876->2892|4912->2907|5073->3041|5095->3054|5131->3069|5292->3203|5314->3216|5349->3230|5501->3355|5523->3368|5553->3377|5706->3503|5728->3516|5760->3527|5920->3660|5942->3673|5979->3689|6137->3819|6160->3832|6196->3846|6355->3977|6378->3990|6413->4003|6576->4138|6599->4151|6637->4167|6796->4298|6819->4311|6853->4323|7027->4469|7050->4482|7088->4498|7256->4638|7279->4651|7318->4668|7497->4819|7520->4832|7553->4843|7918->5180|7941->5193|7980->5210|8147->5349|8170->5362|8212->5382|8390->5532|8413->5545|8452->5562|8608->5690|8631->5703|8665->5715|9045->6067|9068->6080|9107->6097|9265->6227|9288->6240|9324->6254|9474->6376|9497->6389|9541->6411|9694->6536|9717->6549|9748->6558|9999->6781|10022->6794|10061->6811|10330->7052|10353->7065|10399->7089|10654->7316|10677->7329|10714->7344|10894->7496|10917->7509|10950->7520|11127->7669|11150->7682|11190->7700|11452->7934|11475->7947|11513->7963|11810->8232|11833->8245|11864->8254|12031->8393|12054->8406|12090->8420|12251->8553|12274->8566|12304->8574|12480->8722|12503->8735|12543->8753|12839->9021|12862->9034|12895->9045|13217->9339|13240->9352|13283->9373|13716->9778|13739->9791|13777->9807|13949->9951|13972->9964|14007->9977|14236->10178|14259->10191|14298->10208|14489->10371|14512->10384|14544->10394|14729->10551|14752->10564|14798->10588|14997->10759|15020->10772|15064->10794|15251->10953|15274->10966|15319->10989|15507->11149|15530->11162|15572->11182|15751->11333|15774->11346|15817->11367|15995->11517|16018->11530|16053->11543|16221->11683|16244->11696|16280->11710|16546->11948|16569->11961|16601->11971|16769->12111|16792->12124|16824->12134|17002->12284|17025->12297|17059->12309|17361->12583|17384->12596|17420->12610|17705->12867|17728->12880|17764->12894|18026->13128|18049->13141|18090->13160|19269->14311|19292->14324|19320->14330|19451->14433|19495->14467|19535->14468|19585->14489|19624->14500|19647->14513|19680->14524|19734->14546|19780->14563|20639->15394|20662->15407|20696->15419|20829->15524|20879->15564|20919->15565|20967->15584|21006->15595|21029->15608|21068->15625|21124->15649|21172->15668|24239->18707|24262->18720|24297->18733|24430->18838|24481->18879|24521->18880|24569->18899|24608->18910|24631->18923|24671->18941|24727->18965|24775->18984|30015->24196|30038->24209|30066->24215|30190->24311|30214->24324|30243->24330|30422->24481|30445->24494|30480->24507|30613->24612|30664->24653|30704->24654|30752->24673|30791->24684|30814->24697|30854->24715|30910->24739|30958->24758
                  LINES: 24->3|29->3|31->5|35->9|35->9|36->10|38->12|38->12|39->13|39->13|39->13|40->14|42->16|42->16|43->17|43->17|43->17|44->18|46->20|46->20|47->21|47->21|47->21|48->22|49->23|49->23|50->24|66->40|66->40|66->40|75->49|75->49|75->49|78->52|78->52|78->52|82->56|82->56|82->56|85->59|85->59|85->59|89->63|89->63|89->63|92->66|92->66|92->66|96->70|96->70|96->70|99->73|99->73|99->73|103->77|103->77|103->77|106->80|106->80|106->80|110->84|110->84|110->84|113->87|113->87|113->87|117->91|117->91|117->91|120->94|120->94|120->94|124->98|124->98|124->98|127->101|127->101|127->101|131->105|131->105|131->105|134->108|134->108|134->108|138->112|138->112|138->112|141->115|141->115|141->115|145->119|145->119|145->119|148->122|148->122|148->122|157->131|157->131|157->131|160->134|160->134|160->134|164->138|164->138|164->138|167->141|167->141|167->141|176->150|176->150|176->150|179->153|179->153|179->153|183->157|183->157|183->157|186->160|186->160|186->160|192->166|192->166|192->166|198->172|198->172|198->172|204->178|204->178|204->178|208->182|208->182|208->182|211->185|211->185|211->185|217->191|217->191|217->191|224->198|224->198|224->198|227->201|227->201|227->201|231->205|231->205|231->205|234->208|234->208|234->208|241->215|241->215|241->215|248->222|248->222|248->222|258->232|258->232|258->232|261->235|261->235|261->235|267->241|267->241|267->241|271->245|271->245|271->245|274->248|274->248|274->248|278->252|278->252|278->252|281->255|281->255|281->255|285->259|285->259|285->259|288->262|288->262|288->262|292->266|292->266|292->266|295->269|295->269|295->269|301->275|301->275|301->275|305->279|305->279|305->279|308->282|308->282|308->282|314->288|314->288|314->288|320->294|320->294|320->294|326->300|326->300|326->300|337->311|337->311|337->311|341->315|341->315|341->315|342->316|342->316|342->316|342->316|343->317|344->318|358->332|358->332|358->332|362->336|362->336|362->336|363->337|363->337|363->337|363->337|364->338|365->339|420->394|420->394|420->394|424->398|424->398|424->398|425->399|425->399|425->399|425->399|426->400|427->401|504->478|504->478|504->478|505->479|505->479|505->479|509->483|509->483|509->483|513->487|513->487|513->487|514->488|514->488|514->488|514->488|515->489|516->490
                  -- GENERATED --
              */
          