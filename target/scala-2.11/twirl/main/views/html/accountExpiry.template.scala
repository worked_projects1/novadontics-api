
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object accountExpiry_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object accountExpiry_Scope1 {
import co.spicefactory.tasks.Output.Account

class accountExpiry extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[Account],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(accounts: List[Account], baseUrl: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.44*/("""

"""),format.raw/*4.1*/("""<html lang="en">
    <head>
        <style>
            th, td """),format.raw/*7.20*/("""{"""),format.raw/*7.21*/("""
                """),format.raw/*8.17*/("""text-align: center;
                padding: 5px;
            """),format.raw/*10.13*/("""}"""),format.raw/*10.14*/("""

            """),format.raw/*12.13*/("""table, th, td """),format.raw/*12.27*/("""{"""),format.raw/*12.28*/("""
                """),format.raw/*13.17*/("""border: 1px solid black;
                border-collapse: collapse;
            """),format.raw/*15.13*/("""}"""),format.raw/*15.14*/("""
        """),format.raw/*16.9*/("""</style>
    </head>
    <body>
        <p> List of users at the end of account validity</p>
        <br/>
        <table align="left">
            <tr>
                <th>Name</th>
                <th>Practice</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Expires At</th>
                <th>Link</th>
            </tr>
            """),_display_(/*30.14*/for(account <- accounts) yield /*30.38*/{_display_(Seq[Any](format.raw/*30.39*/("""
            """),format.raw/*31.13*/("""<tr>
                <th>"""),_display_(/*32.22*/account/*32.29*/.name),format.raw/*32.34*/("""</th>
                <th>"""),_display_(/*33.22*/account/*33.29*/.practice),format.raw/*33.38*/("""</th>
                <th>"""),_display_(/*34.22*/account/*34.29*/.phone),format.raw/*34.35*/("""</th>
                <th>"""),_display_(/*35.22*/account/*35.29*/.email),format.raw/*35.35*/("""</th>
                <th>"""),_display_(/*36.22*/account/*36.29*/.expirationDate),format.raw/*36.44*/("""</th>
                <th>"""),_display_(/*37.22*/baseUrl),format.raw/*37.29*/("""/accounts/"""),_display_(/*37.40*/account/*37.47*/.id),format.raw/*37.50*/("""</th>
            </tr>
            """)))}),format.raw/*39.14*/("""
        """),format.raw/*40.9*/("""</table>
    </body>
</html>
"""))
      }
    }
  }

  def render(accounts:List[Account],baseUrl:String): play.twirl.api.HtmlFormat.Appendable = apply(accounts,baseUrl)

  def f:((List[Account],String) => play.twirl.api.HtmlFormat.Appendable) = (accounts,baseUrl) => apply(accounts,baseUrl)

  def ref: this.type = this

}


}
}

/**/
object accountExpiry extends accountExpiry_Scope0.accountExpiry_Scope1.accountExpiry
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/accountExpiry.scala.html
                  HASH: c398beac3c843439f902d45c7ebc26502b6638d9
                  MATRIX: 637->46|774->88|802->90|892->153|920->154|964->171|1054->233|1083->234|1125->248|1167->262|1196->263|1241->280|1349->360|1378->361|1414->370|1817->746|1857->770|1896->771|1937->784|1990->810|2006->817|2032->822|2086->849|2102->856|2132->865|2186->892|2202->899|2229->905|2283->932|2299->939|2326->945|2380->972|2396->979|2432->994|2486->1021|2514->1028|2552->1039|2568->1046|2592->1049|2660->1086|2696->1095
                  LINES: 23->2|28->2|30->4|33->7|33->7|34->8|36->10|36->10|38->12|38->12|38->12|39->13|41->15|41->15|42->16|56->30|56->30|56->30|57->31|58->32|58->32|58->32|59->33|59->33|59->33|60->34|60->34|60->34|61->35|61->35|61->35|62->36|62->36|62->36|63->37|63->37|63->37|63->37|63->37|65->39|66->40
                  -- GENERATED --
              */
          