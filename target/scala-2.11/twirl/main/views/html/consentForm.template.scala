
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object consentForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

     object consentForm_Scope1 {
import controllers.v1.PatientsController.Output.ConsentFormObj
import com.itextpdf.text.Image

class consentForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[ConsentFormObj,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(consentForm: ConsentFormObj):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.31*/("""

"""),format.raw/*5.1*/("""<html lang="en">
  <head>
    <title>Consent Form</title>
    <style>
      .subTitle"""),format.raw/*9.16*/("""{"""),format.raw/*9.17*/("""
          """),format.raw/*10.11*/("""font-size:18px;
          font-weight:bold;
          font-style: italic;
       """),format.raw/*13.8*/("""}"""),format.raw/*13.9*/("""
      """),format.raw/*14.7*/(""".fieldName"""),format.raw/*14.17*/("""{"""),format.raw/*14.18*/("""
          """),format.raw/*15.11*/("""font-size:18px;
          font-weight:normal
       """),format.raw/*17.8*/("""}"""),format.raw/*17.9*/("""
       """),format.raw/*18.8*/(""".fieldValue"""),format.raw/*18.19*/("""{"""),format.raw/*18.20*/("""
          """),format.raw/*19.11*/("""font-size:18px;
          borderBottom: 2px solid black;
       """),format.raw/*21.8*/("""}"""),format.raw/*21.9*/("""
       """),format.raw/*22.8*/(""".boldText"""),format.raw/*22.17*/("""{"""),format.raw/*22.18*/("""
         """),format.raw/*23.10*/("""font-weight:bold;
       """),format.raw/*24.8*/("""}"""),format.raw/*24.9*/("""  
    """),format.raw/*25.5*/("""</style>
  </head>  
    <body>
      <table width="90%" align="center" cellpadding="5" cellspacing="10"> 
          <tr>
            <td width="22%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="5%">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr><td colspan="5"  align="left"><img src="app/assets/logo2.png" width="350px" height="75px"/></td></tr>   
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
               <td colspan="5"  align="center">CT SCAN PATIENT REGISTRATION AND CONSENT FORM</td> 
          </tr>
          <tr>
                <td colspan="5"  align="center"><span style="font-weight: bold;">Novadontics, LLC</span> strives to ensure that the <span style="font-weight: bold;">Novadontics Smile™</span> experience exceeds your expectations.</td> 
           </tr>
           <tr>
                <td colspan="5"  align="center">Please fill in the following information and provide your initials and signature at the bottom to confirm consent.</td> 
           </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">Patient Information:</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Patient Name:</td>
              <td class="fieldValue">"""),_display_(/*55.39*/consentForm/*55.50*/.patientName),format.raw/*55.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Gender (M/F):</td>
              <td class="fieldValue">"""),_display_(/*58.39*/consentForm/*58.50*/.gender),format.raw/*58.57*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Date of Birth</td>
              <td class="fieldValue">"""),_display_(/*62.39*/consentForm/*62.50*/.dateOfBirth),format.raw/*62.62*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Name of Doctor requesting CT-Scan:</td>
              <td class="fieldValue">"""),_display_(/*65.39*/consentForm/*65.50*/.requestingCTScan),format.raw/*65.67*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">Contact Information:</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Address:</td>
              <td class="fieldValue">"""),_display_(/*74.39*/consentForm/*74.50*/.address),format.raw/*74.58*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Cell:</td>
              <td class="fieldValue">"""),_display_(/*77.39*/consentForm/*77.50*/.cell),format.raw/*77.55*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Phone (Home):</td>
              <td class="fieldValue">"""),_display_(/*81.39*/consentForm/*81.50*/.phone),format.raw/*81.56*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">eMail Address:</td>
              <td class="fieldValue">"""),_display_(/*84.39*/consentForm/*84.50*/.emailAddress),format.raw/*84.63*/("""</td>
          </tr>
          <tr>
                <td class="fieldName">Work:</td>
                <td class="fieldValue">"""),_display_(/*88.41*/consentForm/*88.52*/.work),format.raw/*88.57*/("""</td>
            </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">Emergency Contact:</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Name:</td>
              <td class="fieldValue">"""),_display_(/*97.39*/consentForm/*97.50*/.Name),format.raw/*97.55*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Relationship:</td>
              <td class="fieldValue">"""),_display_(/*100.39*/consentForm/*100.50*/.relationship),format.raw/*100.63*/("""</td>
          </tr>
          <tr>
              <td class="fieldName">Phone:</td>
              <td class="fieldValue">"""),_display_(/*104.39*/consentForm/*104.50*/.emergencyPhone),format.raw/*104.65*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">Medical History:</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName" colspan="2">Have you been a patient here before?</td>
              <td class="fieldValue" colspan="3">"""),_display_(/*113.51*/consentForm/*113.62*/.patientBefore),format.raw/*113.76*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
            <td class="fieldName"  colspan="2">Have you had a diagnostic scan of the area done previously?</td>
            <td class="fieldValue"  colspan="3">"""),_display_(/*118.50*/consentForm/*118.61*/.diagnosticScan),format.raw/*118.76*/("""</td>
          </tr>  
            
          <tr>
              <td class="fieldName">If Yes (list here):</td>
              <td class="fieldValue">"""),_display_(/*123.39*/consentForm/*123.50*/.diagnosticList),format.raw/*123.65*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Where?</td>
              <td class="fieldValue">"""),_display_(/*128.39*/consentForm/*128.50*/.where),format.raw/*128.56*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">When?</td>
              <td class="fieldValue">"""),_display_(/*131.39*/consentForm/*131.50*/.when),format.raw/*131.55*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td class="fieldName">Pacemaker?</td>
              <td class="fieldValue">"""),_display_(/*136.39*/consentForm/*136.50*/.pacemaker),format.raw/*136.60*/("""</td>
              <td>&nbsp;</td>
              <td class="fieldName">Latex Allergies? </td>
              <td class="fieldValue">"""),_display_(/*139.39*/consentForm/*139.50*/.latexAllergies),format.raw/*139.65*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="2" class="fieldName">(For Females) Is there a possibility you may be pregnant?</td>
              <td>&nbsp;</td>
              <td class="fieldValue">"""),_display_(/*145.39*/consentForm/*145.50*/.pregnant),format.raw/*145.59*/("""</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
              <td colspan="5" class="subTitle">CONSENT FOR TREATMENT: (Please Initial)</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
            <td class="fieldValue" colspan="5"><strong>"""),_display_(/*153.57*/consentForm/*153.68*/.treatmentName),format.raw/*153.82*/("""</strong> &nbsp; I consent to the diagnostic imaging procedures deemed necessary by Dr.
            <strong>"""),_display_(/*154.22*/consentForm/*154.33*/.doctorName),format.raw/*154.44*/("""</strong></td>
          </tr>
          <tr>
            <td  colspan="5" class="fieldValue"><strong>"""),_display_(/*157.58*/consentForm/*157.69*/.treatmentName2),format.raw/*157.84*/("""</strong>  I understand that I have to pay the amount of <strong>$"""),_display_(/*157.151*/consentForm/*157.162*/.treatmentAmount),format.raw/*157.178*/("""</strong> for the CT scan today.</td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
            <td colspan="5">The I CAT cone beam CT scanner produces images that are intended only for evaluation of the mandibular and maxillary jawbones. This study is insufficient to detect intracranial and soft tissue disease processes. </td>
          </tr>
          <tr><td colspan="5">&nbsp;</td></tr>
          <tr>
                <td class="fieldName" colspan="3">Patient Signature</td>
                <td class="fieldName">Date</td>
                <td class="fieldValue">"""),_display_(/*167.41*/consentForm/*167.52*/.patientDate),format.raw/*167.64*/("""</td>
            </tr>
            <tr>
                <td class="fieldName" colspan="3">
                  """),_display_(/*171.20*/if(consentForm.patientSignature != "")/*171.58*/{_display_(Seq[Any](format.raw/*171.59*/("""
                      """),format.raw/*172.23*/("""<img src=""""),_display_(/*172.34*/consentForm/*172.45*/.patientSignature),format.raw/*172.62*/("""" />
                  """)))}),format.raw/*173.20*/("""
                  """),format.raw/*174.19*/("""&nbsp;
                </td>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>  
    </body>
</html>
"""))
      }
    }
  }

  def render(consentForm:ConsentFormObj): play.twirl.api.HtmlFormat.Appendable = apply(consentForm)

  def f:((ConsentFormObj) => play.twirl.api.HtmlFormat.Appendable) = (consentForm) => apply(consentForm)

  def ref: this.type = this

}


}
}

/**/
object consentForm extends consentForm_Scope0.consentForm_Scope1.consentForm
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/consentForm.scala.html
                  HASH: cc780fcf3c8a7ee6963973b75ed2edb0d8c1cae1
                  MATRIX: 675->97|799->126|827->128|939->213|967->214|1006->225|1114->306|1142->307|1176->314|1214->324|1243->325|1282->336|1361->388|1389->389|1424->397|1463->408|1492->409|1531->420|1622->484|1650->485|1685->493|1722->502|1751->503|1789->513|1841->538|1869->539|1903->546|3385->2001|3405->2012|3438->2024|3594->2153|3614->2164|3642->2171|3799->2301|3819->2312|3852->2324|4029->2474|4049->2485|4087->2502|4437->2825|4457->2836|4486->2844|4634->2965|4654->2976|4680->2981|4837->3111|4857->3122|4884->3128|5041->3258|5061->3269|5095->3282|5248->3408|5268->3419|5294->3424|5641->3744|5661->3755|5687->3760|5844->3889|5865->3900|5900->3913|6051->4036|6072->4047|6109->4062|6508->4433|6529->4444|6565->4458|6838->4703|6859->4714|6896->4729|7075->4880|7096->4891|7133->4906|7331->5076|7352->5087|7380->5093|7529->5214|7550->5225|7577->5230|7779->5404|7800->5415|7832->5425|7993->5558|8014->5569|8051->5584|8342->5847|8363->5858|8394->5867|8732->6177|8753->6188|8789->6202|8926->6311|8947->6322|8980->6333|9111->6436|9132->6447|9169->6462|9265->6529|9287->6540|9326->6556|9955->7157|9976->7168|10010->7180|10149->7291|10197->7329|10237->7330|10289->7353|10328->7364|10349->7375|10388->7392|10444->7416|10492->7435
                  LINES: 24->3|29->3|31->5|35->9|35->9|36->10|39->13|39->13|40->14|40->14|40->14|41->15|43->17|43->17|44->18|44->18|44->18|45->19|47->21|47->21|48->22|48->22|48->22|49->23|50->24|50->24|51->25|81->55|81->55|81->55|84->58|84->58|84->58|88->62|88->62|88->62|91->65|91->65|91->65|100->74|100->74|100->74|103->77|103->77|103->77|107->81|107->81|107->81|110->84|110->84|110->84|114->88|114->88|114->88|123->97|123->97|123->97|126->100|126->100|126->100|130->104|130->104|130->104|139->113|139->113|139->113|144->118|144->118|144->118|149->123|149->123|149->123|154->128|154->128|154->128|157->131|157->131|157->131|162->136|162->136|162->136|165->139|165->139|165->139|171->145|171->145|171->145|179->153|179->153|179->153|180->154|180->154|180->154|183->157|183->157|183->157|183->157|183->157|183->157|193->167|193->167|193->167|197->171|197->171|197->171|198->172|198->172|198->172|198->172|199->173|200->174
                  -- GENERATED --
              */
          