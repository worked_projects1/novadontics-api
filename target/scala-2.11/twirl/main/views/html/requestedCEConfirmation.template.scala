
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object requestedCEConfirmation_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class requestedCEConfirmation extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,Long,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(dentistName: String, courseTitle: String, ceRequest: Long, speaker: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.78*/("""
"""),format.raw/*2.1*/("""<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>

    <body>
        <p> Hello, dr. """),_display_(/*9.25*/dentistName),format.raw/*9.36*/("""</p>
        <p> We processed your request for continuing education course <b><em>"""),_display_(/*10.79*/courseTitle),format.raw/*10.90*/("""</em></b> held by <b><em>"""),_display_(/*10.116*/speaker),format.raw/*10.123*/("""</em></b></p>

        <p> Request Id: <b>"""),_display_(/*12.29*/ceRequest),format.raw/*12.38*/("""</b></p>
        <bt/>
        <p> Thank you for your interest, our team will contact you shortly with course specific information. </p>
        <br/>
        <p>
            Regards,<br/>
            Novadontics Team
        </p>
    </body>

</html>
"""))
      }
    }
  }

  def render(dentistName:String,courseTitle:String,ceRequest:Long,speaker:String): play.twirl.api.HtmlFormat.Appendable = apply(dentistName,courseTitle,ceRequest,speaker)

  def f:((String,String,Long,String) => play.twirl.api.HtmlFormat.Appendable) = (dentistName,courseTitle,ceRequest,speaker) => apply(dentistName,courseTitle,ceRequest,speaker)

  def ref: this.type = this

}


}

/**/
object requestedCEConfirmation extends requestedCEConfirmation_Scope0.requestedCEConfirmation
              /*
                  -- GENERATED --
                  DATE: Fri Jun 19 20:14:07 IST 2020
                  SOURCE: /home/rifluxyss/ScalaProjects/git/13.0/app/views/requestedCEConfirmation.scala.html
                  HASH: 0332c29d8735e0d063591397dc322d67ded2780e
                  MATRIX: 582->1|753->77|780->78|929->201|960->212|1070->295|1102->306|1156->332|1185->339|1255->382|1285->391
                  LINES: 20->1|25->1|26->2|33->9|33->9|34->10|34->10|34->10|34->10|36->12|36->12
                  -- GENERATED --
              */
          