# --- !Ups
ALTER TABLE "forms"
  ADD COLUMN "print" BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE "forms"
  DROP COLUMN "print";
