# --- !Ups
UPDATE "forms" SET "schema" = '[{"rows":[{"columns":[{"columnItems":[{"group":{"name":"treatment","radio":true,"title":"Select Treatment","fields":[{"name":"c6a1","type":"Checkbox","title":"Full Arch C&B","options":[{"title":"Test","value":"test"}],"attributes":{"note":false,"images":false,"color":"EA6225"}},{"name":"c6a2","type":"Checkbox","title":"Full Arch Hybrid Acrylic","attributes":{"note":false,"images":false,"color":"228B22"}},{"name":"c6a3","type":"Checkbox","title":"Full Arch Hybrid Zirconia","attributes":{"note":false,"images":false,"color":"fff436"}},{"name":"c6a4","type":"Checkbox","title":"Full Arch Fixed-Detachable with Flanges","attributes":{"note":false,"images":false,"color":"A0522D"}},{"name":"c6a5","type":"Checkbox","title":"Full Arch Locator Overdenture","attributes":{"note":false,"images":false,"color":"8B0000"}},{"name":"c6a6","type":"Checkbox","title":"Full Arch Bar Overdenture","attributes":{"note":false,"images":false,"color":"00008B"}},{"name":"c6a7","type":"Checkbox","title":"Single C&B","attributes":{"note":false,"images":false,"color":"800080"}},{"name":"c6a8","type":"Checkbox","title":"Multi-unit C&B","attributes":{"note":false,"images":false,"color":"008080"}}]}}]}]}],"title":"C6 Chose Treatment"}]' WHERE "id" = '1F1';
UPDATE "forms" SET "title" = '3G Postop Apt' WHERE "id" = '3G';

UPDATE "forms" SET "schema" = '[{"rows":[{"columns":[{"columnItems":[{"field":{"name":"full_name","type":"Input","title":"Full Name","required":true}},{"field":{"name":"phone","type":"Input","title":"Phone","required":true}}]},{"columnItems":[{"field":{"name":"email","type":"Input","title":"Email","required":true}},{"field":{"name":"complaint","type":"Input","title":"Patient''s chief complaint and goal","required":true}},{"field":{"url":"https://s3.amazonaws.com/static.novadonticsllc.com/PDF/C1.pdf","type":"PrintButton","title":"Print Patient Information Form"}},{"field":{"url":"https://s3.amazonaws.com/static.novadonticsllc.com/PDF/C2.pdf","type":"PrintButton","title":"CT Scan Consent Form"}},{"field":{"url":"https://s3.amazonaws.com/static.novadonticsllc.com/PDF/1F.pdf","type":"PrintButton","title":"Quality Survey"}}]}]},{"columns":[{"columnItems":[{"field":{"name":"personal_info","type":"Images","title":"Personal info form photos"}},{"field":{"name":"C2-1","type":"Checkbox","title":"Consent Form (obtained after signature)","attributes":{"note":false,"images":true}}},{"field":{"name":"1f1","type":"Checkbox","title":"The “Novadontics Quality Survey” has been administered.","attributes":{"note":true,"images":false}}}]}]}],"title":"C1 Personal Info"}]' WHERE "id" = '1A';


WITH first_update AS (
         SELECT distinct on (steps1."treatmentId") steps1.value || steps2.value as value, steps2."treatmentId", steps2.state
         FROM steps AS steps2
           LEFT JOIN steps AS steps1 ON steps2."treatmentId" = steps1."treatmentId"
         WHERE steps2."formId" = '1A' AND steps1."formId" = '1B'
         ORDER BY steps1."treatmentId", steps1."dateCreated"),
     second_update AS (
         SELECT distinct on (steps1."treatmentId") steps1.value || steps2.value as value, steps2."treatmentId"
         FROM steps AS steps2
           LEFT JOIN steps AS steps1 ON steps2."treatmentId" = steps1."treatmentId"
         WHERE steps2."formId" = '1A' AND steps1."formId" = '1L'
         ORDER BY steps1."treatmentId", steps1."dateCreated")
     INSERT INTO steps("value", "state", "treatmentId", "formId", "formVersion", "dateCreated")
     SELECT coalesce(first_update.value, '{}') || coalesce(second_update.value, '{}'), first_update.state, first_update."treatmentId", '1A', 1, now()
           from first_update LEFT OUTER JOIN second_update ON first_update."treatmentId" = second_update."treatmentId";

DELETE FROM "steps" WHERE "formId" = '1B';
DELETE FROM "steps" WHERE "formId" = '1L';

DELETE FROM "forms" WHERE "id" = '1B';
DELETE FROM "forms" WHERE "id" = '1L';

UPDATE "forms" SET "title" = REPLACE("title", 'C3', 'C2'), "schema" = REPLACE("schema"::text, 'C3', 'C2')::jsonb where "title" like '%C3%';
UPDATE "forms" SET "title" = REPLACE("title", 'C4', 'C3'), "schema" = REPLACE("schema"::text, 'C4', 'C3')::jsonb where "title" like '%C4%';
UPDATE "forms" SET "title" = REPLACE("title", 'C5', 'C4'), "schema" = REPLACE("schema"::text, 'C5', 'C4')::jsonb where "title" like '%C5%';
UPDATE "forms" SET "title" = REPLACE("title", 'C6', 'C5'), "schema" = REPLACE("schema"::text, 'C6', 'C5')::jsonb where "title" like '%C6%';

# --- !Downs
UPDATE "forms" SET "schema" = '[{"rows": [{"columns": [{"columnItems": [{"field": {"name": "full_name", "type": "Input", "title": "Full Name", "required": true}}, {"field": {"name": "phone", "type": "Input", "title": "Phone", "required": true}}]}, {"columnItems": [{"field": {"name": "email", "type": "Input", "title": "Email", "required": true}}, {"field": {"name": "complaint", "type": "Input", "title": "Patient''s chief complaint and goal", "required": true}}, {"field": {"url": "https://s3.amazonaws.com/static.novadonticsllc.com/PDF/C1.pdf", "type": "PrintButton", "title": "Print Patient Information Form"}}]}]}, {"columns": [{"columnItems": [{"field": {"name": "personal_info", "type": "Images", "title": "Personal info form photos"}}]}]}], "title": "C1 Personal Info"}]' WHERE "id" = '1A';
UPDATE "forms" SET "title" = REPLACE("title", 'C5', 'C6'), "schema" = REPLACE("schema"::text, 'C5', 'C6')::jsonb where "title" like '%C5%';
UPDATE "forms" SET "title" = REPLACE("title", 'C4', 'C5'), "schema" = REPLACE("schema"::text, 'C4', 'C5')::jsonb where "title" like '%C4%';
UPDATE "forms" SET "title" = REPLACE("title", 'C3', 'C4'), "schema" = REPLACE("schema"::text, 'C3', 'C4')::jsonb where "title" like '%C3%';
UPDATE "forms" SET "title" = REPLACE("title", 'C2', 'C3'), "schema" = REPLACE("schema"::text, 'C2', 'C3')::jsonb where "title" like '%C2%';
INSERT INTO "forms"("id", "version", "section", "title", "icon", "schema") VALUES ('1B', 1, 'Consultation Visit', 'C2 CT Consent', 'Consent', '[{"rows": [{"columns": [{"columnItems": [{"field": {"name": "C2-1", "type": "Checkbox", "title": "Consent Form (obtained after signature)", "attributes": {"note": false, "images": true, "printTemplate": "https://s3.amazonaws.com/static.novadonticsllc.com/PDF/C2.pdf"}}}]}]}], "title": "C2 CT Consent"}]');
INSERT INTO "forms"("id", "version", "section", "title", "icon", "schema") VALUES ('1L', 1, 'Visit One - Data Collection', '1F Quality Survey', 'Signature', '[{"rows": [{"columns": [{"columnItems": [{"field": {"name": "1f1", "type": "Checkbox", "title": "The “Novadontics Quality Survey” has been administered.", "attributes": {"note": true, "images": false, "printTemplate": "https://s3.amazonaws.com/static.novadonticsllc.com/PDF/1F.pdf"}}}]}]}], "title": "1F Quality Survey"}]');
