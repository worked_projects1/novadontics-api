# --- !Ups
ALTER TABLE "products"
  ADD COLUMN "quantity" BIGINT;

ALTER TABLE "orderItems"
  ADD COLUMN "packageQuantity" BIGINT;

# --- !Downs
ALTER TABLE "products"
  DROP COLUMN "quantity";

ALTER TABLE "orderItems"
  DROP COLUMN "packageQuantity";