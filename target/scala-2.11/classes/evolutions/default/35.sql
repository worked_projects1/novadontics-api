# --- !Ups
CREATE TABLE "expiryNotifications" (
  "id" BIGSERIAL NOT NULL,
  "staffId" BIGINT NOT NULL,
  "expiryDate" TIMESTAMP NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("staffId") REFERENCES "staff" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('expiryNotifications', 'Expiry Notifications', 'owner@ds.spfr.co', FALSE);

# --- !Downs
DROP TABLE "expiryNotifications"