# --- !Ups
CREATE TABLE "customerFavourites" (
  "id" BIGSERIAL NOT NULL,
  "product" BIGSERIAL NOT NULL,
  "customer" BIGSERIAL NOT NULL,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("product") REFERENCES "products" ("id"),
  FOREIGN KEY ("customer") REFERENCES "staff" ("id")
);


# --- !Downs
DROP TABLE "customerFavourites"