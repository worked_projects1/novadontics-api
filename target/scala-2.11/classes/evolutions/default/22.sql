# --- !Ups
CREATE TABLE IF NOT EXISTS "resourceCategories" (
	"id" BIGSERIAL NOT NULL,
	"name" VARCHAR UNIQUE NOT NULL,
	"resourceType" VARCHAR NOT NULL,
	"order" INT NOT NULL,

	PRIMARY KEY ("id")
);

INSERT INTO "resourceCategories"("name", "resourceType", "order")
	SELECT DISTINCT "category", 'videos', ROW_NUMBER() OVER (ORDER BY "category") AS rownum FROM "videos"
	GROUP BY "category";

INSERT INTO "resourceCategories"("name", "resourceType", "order")
	SELECT DISTINCT "category", 'documents', ROW_NUMBER() OVER (ORDER BY "category") AS rownum FROM "documents"
  GROUP BY "category";


ALTER TABLE "videos" ADD FOREIGN KEY ("category") REFERENCES "resourceCategories" ("name") ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "documents" ADD FOREIGN KEY ("category") REFERENCES "resourceCategories" ("name") ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE "videos" ALTER COLUMN "category" DROP NOT NULL;
ALTER TABLE "documents" ALTER COLUMN "category" DROP NOT NULL;
# --- !Downs

ALTER TABLE "videos" DROP CONSTRAINT "category";
ALTER TABLE "documents" DROP CONSTRAINT "category";
DROP TABLE "resourceCategories";