# --- !Ups
ALTER TABLE staff 
	ADD COLUMN "cellPhone" VARCHAR DEFAULT '';

ALTER TABLE consultations 
	ADD COLUMN "closedDate" TIMESTAMP;

# --- !Downs
ALTER TABLE staff 
	DROP COLUMN "cellPhone";

ALTER TABLE consultations 
	DROP COLUMN "closedDate";
