# --- !Ups
ALTER TABLE "educationRequests" ADD COLUMN "deleted" BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE "educationRequests" DROP COLUMN "deleted";
