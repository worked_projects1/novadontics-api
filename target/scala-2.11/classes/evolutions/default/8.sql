# --- !Ups

CREATE TABLE "emailMapping"(
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "title" VARCHAR NOT NULL,
  "defaultEmail" VARCHAR NULL,
  "isConsultation" BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY ("id")
);

CREATE TABLE "userEmailMapping"(
  "id" BIGSERIAL NOT NULL,
  "userId" BIGSERIAL NOT NULL,
  "emailMappingId" BIGSERIAL NOT NULL,

  FOREIGN KEY ("userId") REFERENCES "users" ("id"),
  FOREIGN KEY ("emailMappingId") REFERENCES "emailMapping" ("id"),
  PRIMARY KEY ("id")
);

INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('order', 'New order', 'owner@ds.spfr.co', FALSE);
INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('overThePhone', 'Request for phone consultation', 'owner@ds.spfr.co', TRUE);
INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('needsLabTechnician', 'Request for site visit by a lab technician', 'owner@ds.spfr.co', TRUE);
INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('needsProstodonthist', 'Request for site visit by a prosthodontist', 'owner@ds.spfr.co', TRUE);
INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('needsSurgicalDentist', 'Request for site visit by a surgical dentist', 'owner@ds.spfr.co', TRUE);
INSERT INTO "emailMapping"("name", "title", "defaultEmail", "isConsultation") VALUES ('needsAnesthesiologist', 'Request for site visit by a anesthesiologist', 'owner@ds.spfr.co', TRUE);


# --- !Downs
DROP TABLE "userEmailMapping";
DROP TABLE "emailMapping";