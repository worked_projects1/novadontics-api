# --- !Ups

ALTER TABLE "appointment" ADD COLUMN "patientName" VARCHAR DEFAULT '';
ALTER TABLE "appointment" ADD COLUMN "referredBy" VARCHAR DEFAULT '';
ALTER TABLE "appointment" ADD COLUMN "alerts" VARCHAR DEFAULT '';
ALTER TABLE "appointment" ADD COLUMN "insurance" VARCHAR DEFAULT '';
ALTER TABLE "appointment" ADD COLUMN "phoneNumber" VARCHAR DEFAULT '';

ALTER TABLE "appointmentLog" ADD COLUMN "patientName" VARCHAR DEFAULT '';
ALTER TABLE "appointmentLog" ADD COLUMN "referredBy" VARCHAR DEFAULT '';
ALTER TABLE "appointmentLog" ADD COLUMN "alerts" VARCHAR DEFAULT '';
ALTER TABLE "appointmentLog" ADD COLUMN "insurance" VARCHAR DEFAULT '';
ALTER TABLE "appointmentLog" ADD COLUMN "phoneNumber" VARCHAR DEFAULT '';

UPDATE "appointmentLog" a
SET reason = b.reason, "patientName" = b."patientName"
FROM appointment b WHERE a."appointmentId" = b.id

CREATE TABLE "appointmentReason"(
  "id" BIGSERIAL NOT NULL,
  "reason" VARCHAR,
  PRIMARY KEY ("id")
);

INSERT INTO "appointmentReason" VALUES (1,'New EX'),(2,'Emergency Ex'),(3,'Limited EX'),(4,'Complete EX'),(5,'Perio Ex'),(6,'FMX'),(7,'1 PA'),(8,'+ PA'),(9,'1 BWX'),(10,'+ BWX'),(11,'PanoX'),(12,'CT Scan'),(13,'Prophy'),(14,'Prophy check'),(15,'3 month Hygiene Recall'),(16,'4 month Hygiene Recall'),(17,'6 month Hygiene Recall'),(18,'Polish'),(19,'1 Quad SRP'),(20,'2 Quad SRP'),(21,'FM SRP'),(22,'1 Filling'),(23,'+ Fillings'),(24,'1 Inlay'),(25,'+ Inlay'),(26,'1 Onlay'),(27,'+ Onlay'),(28,'1 Crown'),(29,'+ Crown'),(30,'1 Bridge'),(31,'+ Bridge'),(32,'1 Veneer'),(33,'+ Veneer'),(34,'Crown or Denture Adjustment'),(35,'Tray Impression'),(36,'Digital Impression'),(37,'PIC Scan'),(38,'Bite Registration'),(39,'Full Denture Try in'),(40,'Full Denture Delivery'),(41,'Partial Denture Try-in'),(42,'Partial Denture Delivery'),(43,'Denture Soft Reline'),(44,'Denture Hard Reline'),(45,'Whitening In-Home'),(46,'Whitening In-Office'),(47,'Invisalign® Impressions'),(48,'Invisalign® Delivery'),(49,'Invisalign® Check'),(50,'Open & Med'),(51,'1 RCT'),(52,'+ RCT'),(53,'1 EXT Simple'),(54,'1 EXT Surgical'),(55,'1 EXT STI'),(56,'1 EXT PBI'),(57,'1 EXT FBI'),(58,'+ EXT'),(59,'FM Ext'),(60,'1 Implant'),(61,'+ Implants'),(62,'Trefoil®'),(63,'All on 4®'),(64,'All on 5'),(65,'All on 6'),(66,'All on 7'),(67,'Second Stage (HA) 1'),(68,'Second Stage (HA) +'),(69,'IV Sedation'),(70,'Oral Sedation'),(71,'Alveoloplasty'),(72,'1 GBR'),(73,'+ GBR'),(74,'Sine Lift Single'),(75,'Bilateral Sinus Lift'),(76,'Ridge Split (MAx)'),(77,'PSP (Man)'),(78,'Block Graft 1'),(79,'Block Graft +'),(80,'Suture Removal')

# --- !Downs

ALTER TABLE "appointment" DROP COLUMN "patientName";

ALTER TABLE "appointment" DROP COLUMN "referredBy";

ALTER TABLE "appointment" DROP COLUMN "alerts";

ALTER TABLE "appointment" DROP COLUMN "insurance";

ALTER TABLE "appointment" DROP COLUMN "phoneNumber";

ALTER TABLE "appointmentLog" DROP COLUMN "patientName";

ALTER TABLE "appointmentLog" DROP COLUMN "referredBy";

ALTER TABLE "appointmentLog" DROP COLUMN "alerts";

ALTER TABLE "appointmentLog" DROP COLUMN "insurance";

ALTER TABLE "appointmentLog" DROP COLUMN "phoneNumber";

DROP TABLE "appointmentReason";