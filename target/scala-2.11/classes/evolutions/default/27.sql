# --- !Ups
ALTER TABLE "videos" ADD COLUMN "courseId" VARCHAR;

# --- !Downs
ALTER TABLE "videos" DROP COLUMN "courseId";