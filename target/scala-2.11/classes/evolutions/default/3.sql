# --- !Ups
CREATE TABLE "videos" (
  "id" BIGSERIAL NOT NULL,
  "title" VARCHAR NOT NULL,
  "url" VARCHAR NOT NULL,
  "thumbnailUrl" VARCHAR NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  "category" VARCHAR NOT NULL,

  PRIMARY KEY ("id")
);

INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video1', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/1f303352a9158225d5d5a40b00afc61f.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/1f303352a9158225d5d5a40b00afc61f.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video2', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/3b3898bddcbdd204af9cefec180a58e4.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/3b3898bddcbdd204af9cefec180a58e4.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video3', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/5f335eb3e56a892626d1d71727d1794d.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/5f335eb3e56a892626d1d71727d1794d.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/93ca266ab6501145aa7059236ffa3e0f.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/93ca266ab6501145aa7059236ffa3e0f.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video5', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/bb30d9cf65a899747b590f7c4c4a95eb.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/bb30d9cf65a899747b590f7c4c4a95eb.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Video6', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/fdf95a163926716475b184d50ed9773d.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/fdf95a163926716475b184d50ed9773d.png', CURRENT_TIMESTAMP, 'Patient Educational Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('video7', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/4da3bb7a25312343b5d7f3963ca8ccb5.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/4da3bb7a25312343b5d7f3963ca8ccb5.png', CURRENT_TIMESTAMP, 'Product Demonstration Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('video8', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/dd01b6e7df7ad319f98b13d40240b441.mp4', 'https://s3.amazonaws.com/ds-static.spfr.co/videos/dd01b6e7df7ad319f98b13d40240b441.png', CURRENT_TIMESTAMP, 'Product Demonstration Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('Converting A Removable Denture - The Direct Method', 'https://s3.amazonaws.com/static.novadonticsllc.com/videos/converting_a_removable_denture_the_direct_method.mp4', 'https://s3.amazonaws.com/static.novadonticsllc.com/videos/converting_a_removable_denture_the_direct_method.png', CURRENT_TIMESTAMP, 'Live-patient Demonstration Videos');
INSERT INTO "videos"("title", "url", "thumbnailUrl", "dateCreated", "category") VALUES ('The Surgical Protocols of the All-on-Four Technique', 'https://s3.amazonaws.com/static.novadonticsllc.com/videos/The+Surgical+Protocols+of+the+All-on-Four+Technique.mp4', 'https://s3.amazonaws.com/static.novadonticsllc.com/videos/The+Surgical+Protocols+of+the+All-on-Four+Technique.png', CURRENT_TIMESTAMP, 'Live-patient Demonstration Videos');

# --- !Downs
DROP TABLE "videos";