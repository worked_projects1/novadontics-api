# --- !Ups
ALTER TABLE "orderItems" ADD COLUMN "overnightShipping" BOOLEAN;
ALTER TABLE "vendors" ADD COLUMN "email" VARCHAR;

# --- !Downs
ALTER TABLE "orderItems" DROP COLUMN "overnightShipping";
ALTER TABLE "vendors" DROP COLUMN "email";