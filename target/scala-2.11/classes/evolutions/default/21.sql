# --- !Ups
ALTER TABLE "staff" ADD COLUMN "deaNumber" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "npiNumber" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "phone" VARCHAR;

# --- !Downs
ALTER TABLE "staff" DROP COLUMN "deaNumber";
ALTER TABLE "staff" DROP COLUMN "npiNumber";
ALTER TABLE "staff" DROP COLUMN "phone";