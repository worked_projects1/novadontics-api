# --- !Ups
CREATE TABLE "practices"(
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "country" VARCHAR NOT NULL,
  "city" VARCHAR NOT NULL,
  "state" VARCHAR NOT NULL,
  "zip" VARCHAR NOT NULL,
  "address" VARCHAR NOT NULL,
  "phone" VARCHAR NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  "archived" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id")
);

ALTER TABLE "dentists" RENAME TO "staff";

ALTER TABLE "treatments" RENAME "dentistId" TO "staffId";
ALTER TABLE "orders" RENAME "dentistId" TO "staffId";
ALTER TABLE "consultations" RENAME "dentistId" TO "staffId";
ALTER TABLE "sessions" RENAME "dentistId" TO "staffId";
ALTER TABLE "passwordChangeTokens" RENAME "dentistId" TO "staffId";

INSERT INTO "practices" ("name", "country", "city", "state", "zip", "address", "phone", "dateCreated")
  SELECT DISTINCT "practice"
    "practice", "country", "city", "state", "zip", "address", "phone", "dateCreated" FROM "staff";

ALTER TABLE "staff"
  ADD COLUMN "role" VARCHAR NOT NULL DEFAULT 'Dentist',
  ADD COLUMN "practiceId" BIGINT;

UPDATE "staff"
  SET "practiceId" = "practices"."id"
  FROM "practices"
  WHERE "practices"."name" = "staff"."practice";

ALTER TABLE "staff"
  ALTER COLUMN "practiceId" SET NOT NULL,
  ADD FOREIGN KEY ("practiceId") REFERENCES "practices" ("id");

ALTER TABLE "staff"
  DROP COLUMN "practice",
  DROP COLUMN "country",
  DROP COLUMN "city",
  DROP COLUMN "state",
  DROP COLUMN "zip",
  DROP COLUMN "phone",
  DROP COLUMN "address";

# --- !Downs
ALTER TABLE "staff"
  ADD COLUMN "practice" VARCHAR,
  ADD COLUMN "country" VARCHAR,
  ADD COLUMN "city" VARCHAR,
  ADD COLUMN "state" VARCHAR,
  ADD COLUMN "zip" VARCHAR,
  ADD COLUMN "phone" VARCHAR,
  ADD COLUMN "address" VARCHAR;

UPDATE "staff"
  SET ("practice", "country", "city", "state", "zip", "address", "phone", "dateCreated") = ("practices"."name", "practices"."country", "practices"."city", "practices"."state", "practices"."zip", "practices"."address", "practices"."phone", "practices"."dateCreated")
  FROM "practices"
  WHERE "practices"."id" = "staff"."practiceId";

ALTER TABLE "staff"
  ALTER COLUMN "practice" SET NOT NULL,
  ALTER COLUMN "country" SET NOT NULL,
  ALTER COLUMN "city" SET NOT NULL,
  ALTER COLUMN "state" SET NOT NULL,
  ALTER COLUMN "zip" SET NOT NULL,
  ALTER COLUMN "phone" SET NOT NULL,
  ALTER COLUMN "address" SET NOT NULL;

ALTER TABLE "staff"
  DROP COLUMN "practiceId",
  DROP COLUMN "role";

ALTER TABLE "staff" RENAME TO "dentists";

ALTER TABLE "treatments" RENAME "staffId" TO "dentistId";
ALTER TABLE "orders" RENAME "staffId" TO "dentistId";
ALTER TABLE "consultations" RENAME "staffId" TO "dentistId";
ALTER TABLE "sessions" RENAME "staffId" TO "dentistId";
ALTER TABLE "passwordChangeTokens" RENAME "staffId" TO "dentistId";

DROP TABLE "practices";