# --- !Ups
CREATE TABLE "cart" (
  "id" BIGSERIAL NOT NULL,
  "userId" BIGINT,
  "productId" BIGINT,
  "quantity" INT,
  PRIMARY KEY ("id")
);

ALTER TABLE "staff" 
	ADD COLUMN "ciiPrograms" VARCHAR DEFAULT '',
  ADD COLUMN "custToLeadFlag" BOOLEAN DEFAULT FALSE,
  ADD COLUMN "photo" VARCHAR DEFAULT '',
  ADD COLUMN "amountCollected" BIGINT,
  ADD COLUMN "collectedDate" TIMESTAMP;

update "staff" set "amountCollected"="plan" where 1=1;

ALTER TABLE "renewalDetails"
	ADD COLUMN "amountCollected" BIGINT,
	ADD COLUMN "collectedDate" TIMESTAMP;

update "renewalDetails" set "amountCollected"="plan" where 1=1;  
  
ALTER TABLE "vendors" 
	ADD COLUMN "shippingChargeLimit" FLOAT,
  ADD COLUMN "shippingCharge" FLOAT,
  ADD COLUMN "tax" FLOAT;
  
ALTER TABLE "orders" 
  ADD COLUMN "shippingCharge" FLOAT,
  ADD COLUMN "tax" FLOAT,  
  ADD COLUMN "creditCardFee" FLOAT;  
  
ALTER TABLE "products" 
	ADD COLUMN "listPrice" FLOAT;
  
ALTER TABLE "courses" 
	ADD COLUMN "subCategory" VARCHAR DEFAULT '';

ALTER TABLE "resourceCategories"
    ADD COLUMN "parent" BIGINT NULL;
    
ALTER TABLE "orderItems" 
	ADD COLUMN "invoice" VARCHAR DEFAULT '';

ALTER TABLE "treatments"
    ADD COLUMN "ctScanFile" VARCHAR DEFAULT '';
  
INSERT INTO  "emailMapping" (name,title,"defaultEmail","isConsultation") VALUES ('signupRequest','SignUp Request','team@novadontics.com',false); 

INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('ANNUAL FELLOWSHIP','ciicourse',1);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('ADVANCED IMPLANT PROSTHODONTICS','ciicourse',2);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('BASIC TO ADVANCED IMPLANT SURGICAL EXTERNSHIP','ciicourse',3);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('ADVANCED BONE GRAFTING SURGICAL EXTERNSHIP','ciicourse',4);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('ALL ON 4 SURGICAL EXTERNSHIP','ciicourse',5);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('TREFOIL SURGICAL  EXTERNSHIP','ciicourse',6);




# --- !Downs
DROP TABLE "cart";


ALTER TABLE "staff" 
	DROP COLUMN "ciiPrograms",
  DROP COLUMN "custToLeadFlag",
  DROP COLUMN "photo",
  DROP COLUMN "amountCollected",
  DROP COLUMN "collectedDate";

ALTER TABLE "renewalDetails"
	DROP COLUMN "amountCollected",
	DROP COLUMN "collectedDate";

ALTER TABLE "vendors" 
	DROP COLUMN "shippingChargeLimit",
  DROP COLUMN "shippingCharge",
  DROP COLUMN "tax";

ALTER TABLE "orders" 
  DROP COLUMN "shippingCharge",
  DROP COLUMN "tax",  
  DROP COLUMN "creditCardFee";  

ALTER TABLE "products" 
  DROP COLUMN "listPrice"; 
  
ALTER TABLE "courses" 
	DROP COLUMN "subCategory";  
    
ALTER TABLE "resourceCategories"
  DROP COLUMN "parent";
    
ALTER TABLE "orderItems" 
	DROP COLUMN "invoice";   

ALTER TABLE "treatments"
    DROP COLUMN "ctScanFile";

DELETE FROM "emailMapping" WHERE name='signupRequest';   

DELETE FROM "resourceCategories" WHERE name='ANNUAL FELLOWSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ADVANCED IMPLANT PROSTHODONTICS' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='BASIC TO ADVANCED IMPLANT SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ADVANCED BONE GRAFTING SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ALL ON 4 SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='TREFOIL SURGICAL  EXTERNSHIP' AND  "resourceType"='ciicourse';


