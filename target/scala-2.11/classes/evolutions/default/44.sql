# --- !Ups

ALTER TABLE "vendors"
	ADD COLUMN "creditCardFee" FLOAT8;

DELETE FROM "resourceCategories" WHERE name='ANNUAL FELLOWSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ADVANCED IMPLANT PROSTHODONTICS' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='BASIC TO ADVANCED IMPLANT SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ADVANCED BONE GRAFTING SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='ALL ON 4 SURGICAL EXTERNSHIP' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='TREFOIL SURGICAL  EXTERNSHIP' AND  "resourceType"='ciicourse';


INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('Annual Fellowship','ciicourse',1);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('Advanced Implant Prosthodontics','ciicourse',2);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('Basic to Intermediate Implant Placement Surgical Externship','ciicourse',3);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('Advanced Bone Grafting Surgical Externship','ciicourse',4);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('All on 4 Surgical Externship','ciicourse',5);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('Trefoil Surgical Externship','ciicourse',6);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('CII Meetings & Events','ciicourse',7);
INSERT INTO "resourceCategories" ("name","resourceType","order") VALUES ('CII Study Club','ciicourse',8);

# --- !Downs

ALTER TABLE "vendors"
	DROP COLUMN "creditCardFee";

DELETE FROM "resourceCategories" WHERE name='Annual Fellowship' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='Advanced Implant Prosthodontics' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='Basic to Intermediate Implant Placement Surgical Externship' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='Advanced Bone Grafting Surgical Externship' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='All on 4 Surgical Externship' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='Trefoil Surgical Externship' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='CII Meetings & Events' AND  "resourceType"='ciicourse';
DELETE FROM "resourceCategories" WHERE name='CII Study Club' AND  "resourceType"='ciicourse';