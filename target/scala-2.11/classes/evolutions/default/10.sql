# --- !Ups
ALTER TABLE "orders"
  ADD COLUMN "dateFulfilled" TIMESTAMP,
  ADD COLUMN "approvedBy" VARCHAR;


# --- !Downs
ALTER TABLE "orders"
  DROP COLUMN "dateFulfilled",
  DROP COLUMN "approvedBy";
