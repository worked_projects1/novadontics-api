# --- !Ups

CREATE TABLE "toothChart" (
  "id" BIGSERIAL NOT NULL,
  "patientId" BIGINT NOT NULL,
  "toothId" INT NOT Null,
  "toothConditions" VARCHAR DEFAULT '',
  "staffId" BIGINT DEFAULT 0,
  "userId" BIGINT DEFAULT 0,
  "dateUpdated" TIMESTAMP NOT NULL,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("patientId") REFERENCES "treatments" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE "perioChart" (
  "id" BIGSERIAL NOT NULL,
  "patientId" BIGINT NOT NULL,
  "rowPosition" VARCHAR NOT NULL,
  "perioType" VARCHAR NOT NULL,
  "dateSelected" TIMESTAMP NOT NULL,
  "perioNumbers" JSONB,
  "staffId" BIGINT DEFAULT 0,
  "userId" BIGINT DEFAULT 0,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("patientId") REFERENCES "treatments" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);



# --- !Downs

DROP TABLE "toothChart";

DROP TABLE "perioChart";





