# --- !Ups
ALTER TABLE "staff"
  ADD COLUMN "udid" VARCHAR;


# --- !Downs
ALTER TABLE "orders"
  DROP COLUMN "udid";
