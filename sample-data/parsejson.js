let fs = require('fs');

fs.readFile('fullJson.json', 'utf8', function(err, data) {
  if (err) throw err; // we'll not consider error handling for now

  let obj = JSON.parse(data);

  let scalaImportString = ""
  let sqlString = 'INSERT INTO forms ("id","version","section","title","icon","schema") VALUES \n'
  obj.forEach(function(category) {
    // Form

    category.forms.forEach(function(step) {
      let stepTitle = step.title
      let formId = step.title.split(" ").shift()
      let stepSchema = JSON.stringify(step.schema)
        .replace(/\n|\r/g, "")
        .replace(/\'/g,"\\'").replace(/\t/g, "  ")
        .replace(/\\n/g, "\\\\n")
        .replace(/\\"/g, '\\\\"')
      ;

      sqlString = sqlString.concat(`('${formId}', 1, '${category.title}','${stepTitle}', '${step.icon}', E'${stepSchema}'), \n`)
      scalaImportString = scalaImportString.concat(`FormRow("${formId}", 1, "${category.title}","${stepTitle}", "${step.icon}", Json.parse("${stepSchema.replace(/\"/g, "\\\"")}")), \n`)

      //FormRow("1A", 1, "Visit One - Data Collection","1A Personal Info", "PersonalInfo", Json.parse(""))
    })

  })

  sqlString = sqlString.slice(0, -3) + ";"



  fs.writeFile('steps.sql', sqlString, function(err){
    if(err) throw err;
  })

  fs.writeFile('scalaImport.scala', scalaImportString, function(err) {
    if(err) throw err;
  })
});
