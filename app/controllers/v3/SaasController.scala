package controllers.v3
import controllers.NovadonticsController
import controllers.v1.StaffController

import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import play.api.libs.json.Json
import javax.inject._
import models.daos.tables.{StaffRow,SaasRow,SaasPaymentRow,EligibilityRow}
import play.api.mvc.Result
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
// import java.io.File
import java.text.SimpleDateFormat
// import java.util.Calendar;
import java.util.Date;
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};
import java.nio.file.{Files, Paths, StandardCopyOption}
import play.api.Logger
import play.api.libs.Files.TemporaryFile
// import play.api.mvc.MultipartFormData
import play.api.mvc.Request
// import scala.io.Source
import java.nio.charset.StandardCharsets
// import org.apache.http.entity.mime._
import java.io.{File, FileOutputStream, FileInputStream}

// import org.apache.http.entity.mime.content._
import java.io.ByteArrayOutputStream
// import play.api.libs.ws.WS
import scalaj.http.{Http, HttpOptions}
import scalaj.http.MultiPart

// import akka.stream.scaladsl.FileIO
// import akka.stream.scaladsl.Source
// import play.api.libs.ws._
// import play.api.libs.ws.WSClient
// import play.api.mvc.MultipartFormData
// import play.api.mvc.MultipartFormData.FilePart
// import play.api.mvc.MultipartFormData.DataPart
// import com.ning.http.client.multipart
// import akka.http.scaladsl.model.Multipart.BodyPart
// import akka.http.scaladsl.model.{HttpResponse, Multipart, StatusCodes}
// import akka.http.scaladsl.server.Directives._
// import akka.http.scaladsl.server.Route

// import org.apache.http.HttpEntity;
// import org.apache.http.HttpResponse;
// import org.apache.http.client.methods.HttpUriRequest;
// import org.apache.http.client.methods.RequestBuilder;
// import org.apache.http.entity.ContentType;
// import org.apache.http.entity.mime.HttpMultipartMode;
// import org.apache.http.entity.mime.MultipartEntityBuilder;
// import org.apache.http.entity.mime.content.FileBody;
// import org.apache.http.impl.client.CloseableHttpClient;
// import org.apache.http.impl.client.HttpClients;
// import org.apache.http.util.EntityUtils;
// import java.io.File;
// import java.io.IOException;
// import java.net.URISyntaxException;

// import com.ning.http.client.AsyncHttpClient
// import com.ning.http.client.multipart.FilePart
// import com.ning.http.client.multipart.StringPart
// import org.springframework.web.multipart.MockMultipartFile
class SaasController @Inject()( silhouette: Silhouette[BearerTokenEnv], val application: Application ,sasController: StaffController)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  // implicit val SaasFormat = Json.format[SaasRow] Json.writes[EligibilityRow]
implicit val EligibilityRowWrites =  Writes { request: (EligibilityRow) =>
    val (eligibilityRow) = request

  Json.obj(
      "id" -> eligibilityRow.id,
      "patientId" -> eligibilityRow.patientId,
      "requestDate" -> eligibilityRow.requestDate,
      "receivedDate" -> eligibilityRow.receivedDate,
      "subscriberType" -> eligibilityRow.subscriberType,
      "subscriberChartNumber" -> eligibilityRow.subscriberChartNumber,
      "eligibilityResponse" -> eligibilityRow.eligibilityResponse,
      "activeCoverage" -> eligibilityRow.activeCoverage,
      "resultCode" -> eligibilityRow.resultCode,
      "resultDesc" -> eligibilityRow.resultDesc
      )
  }
  // patientId: Option[Long],
  // requestDate: Option[LocalDate],
  // receivedDate: Option[LocalDate],
  // subscriberType: Option[String],
  // subscriberChartNumber: Option[String],
  // eligibilityResponse: Option[String],
  // activeCoverage: Option[String],
  // resultCode: Option[String],
  // resultDesc: Option[String]

  implicit val saasRowWrites = Writes { request: (SaasRow) =>
    val (saasRow) = request

  Json.obj(
      "id" -> saasRow.id,
      "practiceId" -> saasRow.practiceId,
      "saasType" -> saasRow.saasType,
      "logo" -> saasRow.logo,
      "name" -> saasRow.name,
      "subStartDate" -> saasRow.subStartDate,
      "subExpDate" -> saasRow.subExpDate,
      "subscriptionType" -> saasRow.subscriptionType,
      "subscriptionFee" -> Math.round(convertOptionFloat(saasRow.subscriptionFee) * 100.0) / 100.0,
      "notes" -> saasRow.notes,
      "autoRenew" -> saasRow.autoRenew,
      "documents" -> saasRow.documents
      )
  }

  def readAll = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
      db.run {
            SaasTable.filterNot(_.deleted).filter(_.practiceId === practiceId).result } map { saasList =>
        Ok(Json.toJson(saasList))
      }
  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val saas = for {
      saasType <- (request.body \ "saasType").validate[String]
      logo <- (request.body \ "logo").validate[String]
      name <- (request.body \ "name").validate[String]
      subStartDate <- (request.body \ "subStartDate").validate[LocalDate]
      subExpDate <- (request.body \ "subExpDate").validateOpt[LocalDate]
      subscriptionType <- (request.body \ "subscriptionType").validateOpt[String]
      subscriptionFee <- (request.body \ "subscriptionFee").validateOpt[Float]
      notes <- (request.body \ "notes").validateOpt[String]
      autoRenew <- (request.body \ "autoRenew").validateOpt[String]
      documents <- (request.body \ "documents").validateOpt[String]
    } yield {

      SaasRow(None,Some(practiceId),saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents,false)
    }
    
    saas match {
      case JsSuccess(saasRow, _) => db.run {
        SaasTable.returning(SaasTable.map(_.id)) += saasRow
       
      } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(saasRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  
  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val saas = for {
      saasType <- (request.body \ "saasType").validateOpt[String]
      logo <- (request.body \ "logo").validateOpt[String]
      name <- (request.body \ "name").validateOpt[String]
      subStartDate <- (request.body \ "subStartDate").validateOpt[LocalDate]
      subExpDate <- (request.body \ "subExpDate").validateOpt[LocalDate]
      subscriptionType <- (request.body \ "subscriptionType").validateOpt[String]
      subscriptionFee <- (request.body \ "subscriptionFee").validateOpt[Float]
      notes <- (request.body \ "notes").validateOpt[String]
      autoRenew <- (request.body \ "autoRenew").validateOpt[String]
      documents <- (request.body \ "documents").validateOpt[String]
    } yield {
      (saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents)
    } 
    
    saas match {
      case JsSuccess((saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents), _) =>
       db.run {
          val query = SaasTable.filter(_.id === id)

          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](
                saasType.map(value => query.map(_.saasType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                logo.map(value => query.map(_.logo).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                subStartDate.map(value => query.map(_.subStartDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subExpDate).map(value => query.map(_.subExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subscriptionType).map(value => query.map(_.subscriptionType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subscriptionFee).map(value => query.map(_.subscriptionFee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(notes).map(value => query.map(_.notes).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(autoRenew).map(value => query.map(_.autoRenew).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(documents).map(value => query.map(_.documents).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ => Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  

def delete(id: Long) = silhouette.SecuredAction.async {
    db.run {
      SaasTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }


def updateSaasPayment(saasId: Long, year: Long) = silhouette.SecuredAction.async(parse.json) { request =>
var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  val saasPayment = for {
      saas <- (request.body \ "saas").validate[JsValue]
    } yield {
      (saas)
    }

  saasPayment match {
      case JsSuccess((saas), _) =>
      val jsonList: List[JsValue] = Json.parse(saas.toString).as[List[JsValue]]
      (jsonList).foreach( result => {
        val month = convertOptionString((result \ ("month")).asOpt[String])
        val amount = convertOptionFloat((result \ ("amount")).asOpt[Float])
        // println(month + " " + amount)
        db.run {
          val query = SaasPaymentTable.filter(_.practiceId === practiceId).filter(_.saasId === saasId).filter(_.year === year).filter(_.month === month)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true => DBIO.seq[Effect.Write](
              Some(amount).map(value => query.map(_.amount).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map {_ => Ok}
            case false =>
            var block = db.run{
                SaasPaymentTable.returning(SaasPaymentTable.map(_.id)) += SaasPaymentRow(None,saasId,practiceId,month,year,amount)
                } map {
                case 0L => PreconditionFailed
                case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
            DBIO.successful(Ok)
          }
        }
      })
      Future(Ok)
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def getSaasPayment(saasId: Long,year: Long) = silhouette.SecuredAction.async { request =>
  var saasPayment:scala.collection.mutable.Map[String,Float]=scala.collection.mutable.Map()
      db.run {
        SaasPaymentTable.filter(_.saasId === saasId).filter(_.year === year).result
          } map { saasList =>
          (saasList).foreach(result => {
              saasPayment.update(result.month,result.amount)
          })
          var map = scala.util.parsing.json.JSONObject(saasPayment.toMap)
          val jsonObject: JsValue = Json.parse(String.valueOf(map))
        Ok(Json.toJson(Array(jsonObject)))
      }
  }

def saasPaymentSummary = silhouette.SecuredAction.async { request =>
  var jsonObject: JsValue = null
  var dm  = List[Long]()
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  db.run{
    SaasPaymentTable.filter(_.practiceId === practiceId).sortBy(_.year).result
  } map { paymentList =>
      paymentList.foreach(s => {
        if(!(dm contains s.year)){
        dm = s.year :: dm
        }
      })
      var strJson = ""
      for(a <- dm){
        var total: Float = 0
        var amount: Float = 0
        var saasPayment:scala.collection.mutable.Map[String,Float]=scala.collection.mutable.Map()
        var block = db.run{
          SaasPaymentTable.filter(_.practiceId === practiceId).filter(_.year === a).result
        } map { saasList =>
            (saasList).foreach(result => {
              total = total + result.amount
              amount = convertOptionFloat(saasPayment.get(result.month)) +  result.amount
              saasPayment.update(result.month,amount)
            })
            var temp = saasPayment += ("year" -> a,"total" -> total)
            var map = scala.util.parsing.json.JSONObject(temp.toMap)
            strJson = strJson + String.valueOf(map) + ","
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        jsonObject = Json.parse(s"[${strJson.dropRight(1)}]")
      }
       Ok(Json.toJson(jsonObject))
    }
  }

def getValue(spanId: String, htmlContent: String) = {
  val value = htmlContent.substring(htmlContent.indexOf(spanId)).slice(
htmlContent.substring(htmlContent.indexOf(spanId)).indexOf(">")+1,htmlContent.substring(htmlContent.indexOf(spanId)).indexOf("<")
  )
  println("value1 = "+value)
  value
}
  def testUpload = silhouette.SecuredAction.async(parse.json) { request =>
//  println("myFile ==> "+ myFile)
var controlno = ""
var patientId = ""
var requestDate = LocalDate.now();
var receivedDate = LocalDate.now();
println("requestDate = "+requestDate);
// println("resultdesc = "+resultdesc)
var subscriberType = ""
var subscriberChartNumber = ""
var eligibilityResponse = ""
var activeCoverage = "no"
var resultCode = ""
var resultDesc = ""
 val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
for{
    trojen <- (request.body \ "url").validate[String]
  }yield{
if(trojen.contains("resultcode")){
  resultCode = getValue("resultcode",trojen)
  println("resultCode = "+resultCode)
}
if(trojen.contains("resultdesc")){
resultDesc = getValue("resultdesc",trojen)
  println("resultdesc = "+resultDesc)
}
if(trojen.contains("controlno")){
 controlno = getValue("controlno",trojen)
  println("controlno = "+controlno)
}
if(trojen.contains("patientid")){
 patientId = getValue("patientid",trojen)
  println("patientid = "+patientId)
}
if(trojen.contains("requestDate")){
val requestDate1 = getValue("requestDate",trojen)
  println("requestDate = "+requestDate1)
}
if(resultCode == "G"){
  activeCoverage = "yes"
}
eligibilityResponse = trojen

  }

  var block =  db.run {
          val query = EligibilityTable.filter(_.patientId === patientId.toLong)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true => DBIO.seq[Effect.Write](
              query.map(_.id).update(controlno.toLong),
              query.map(_.resultCode).update(resultCode),
              query.map(_.resultDesc).update(resultDesc),
              query.map(_.activeCoverage).update(activeCoverage),
              query.map(_.eligibilityResponse).update(eligibilityResponse)

            ) map {_ => Ok}
            case false => DBIO.successful(NotFound)
          }
        }
        var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
//  println("request ==> "+ (request.body \ "url").validateOpt[String])
println("create = "+patientId)
Future { Ok { Json.obj("status"->"success","message"->"Clinical Notes Migrated Successfully")}
}
//  db.run {
//         SaasTable.result
//     } map { operatorsList =>
//       Ok(Json.toJson(operatorsList))
//     } 
  } 

def createEligiblity = silhouette.SecuredAction.async(parse.json) { request =>
println("create = "+(request.body \ "subscriberChartNumber").validateOpt[String])
println("create11 = "+(request.body \ "patientId").validate[Long])
println("create 22= "+(request.body \ "requestDate").validateOpt[LocalDate])
println("create33 = "+(request.body \ "subscriberType").validateOpt[String])
    val eligibility = for {
      patientId <- (request.body \ "patientId").validate[Long]
      requestDate <- (request.body \ "requestDate").validateOpt[LocalDate]
      receivedDate <- (request.body \ "receivedDate").validateOpt[LocalDate]
      subscriberType <- (request.body \ "subscriberType").validateOpt[String]
      subscriberChartNumber <- (request.body \ "subscriberChartNumber").validateOpt[String]
      eligibilityResponse <- (request.body \ "eligibilityResponse").validateOpt[String]
      activeCoverage <- (request.body \ "activeCoverage").validateOpt[String]
      resultCode <- (request.body \ "resultCode").validateOpt[String]
      resultDesc <- (request.body \ "resultDesc").validateOpt[String]
    } yield {
println("create1 = "+patientId+" ,requestdate = "+requestDate)
      EligibilityRow(None,patientId,requestDate,receivedDate,convertOptionString(subscriberType),convertOptionString(subscriberChartNumber),convertOptionString(eligibilityResponse),convertOptionString(activeCoverage),convertOptionString(resultCode),convertOptionString(resultDesc))
    }
    eligibility match {
      case JsSuccess(eligibilityRow, _) => 
      println("eligibilityRow = ",eligibilityRow)
      db.run {
        EligibilityTable.returning(EligibilityTable.map(_.id)) += eligibilityRow
       
      } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(eligibilityRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

//   def testFile = silhouette.UnsecuredAction(parse.multipartFormData) { request =>
//   var controlno = ""
//   var patientId = ""
//   // var requestDate = LocalDate.now();
//   // var receivedDate = LocalDate.now();
//   // var subscriberType = ""
//   // var subscriberChartNumber = ""
//   println("request = "+request.body.file("file.html"))
//   var eligibilityResponse = ""
//   var activeCoverage = "no"
//   var resultCode = ""
//   var resultDesc = ""
//       request.body.file("customFile") match {
//         case Some(file) => {
//           import java.io.File
//           val filename = file.filename
//           val trojen = Source.fromFile(file.ref.file).mkString
//           println("trojen = "+trojen)
//           val contetType = file.contentType

//           if(trojen.contains("resultcode")){
//             resultCode = getValue("resultcode",trojen)
//             println("resultCode = "+resultCode)
//           }
//           if(trojen.contains("resultdesc")){
//           resultDesc = getValue("resultdesc",trojen)
//             println("resultdesc = "+resultDesc)
//           }
//           if(trojen.contains("controlno")){
//           controlno = getValue("controlno",trojen)
//             println("controlno = "+controlno)
//           }
//           if(trojen.contains("patientid")){
//           patientId = getValue("patientid",trojen)
//             println("patientid = "+patientId)
//           }
//           // if(trojen.contains("requestDate")){
//           // val requestDate1 = getValue("requestDate",trojen)
//           //   println("requestDate = "+requestDate1)
//           // }
//           if(resultCode == "G"){
//             activeCoverage = "yes"
//           }
//           eligibilityResponse = trojen

//         //   var block =  db.run {
//         //             val query = EligibilityTable.filter(_.id === controlno.toLong)
//         //             query.exists.result.flatMap[Result, NoStream, Effect.Write] {
//         //               case true => DBIO.seq[Effect.Write](
//         //                 query.map(_.patientId).update(patientId.toLong),
//         //                 query.map(_.resultCode).update(resultCode),
//         //                 query.map(_.resultDesc).update(resultDesc),
//         //                 query.map(_.activeCoverage).update(activeCoverage),
//         //                 query.map(_.eligibilityResponse).update(eligibilityResponse)

//         //               ) map {_ => Ok}
//         //               case false => DBIO.successful(NotFound)
//         //             }
//         //           }
//         // var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
//           Ok("congratz you did it")
//   }

  
        
//         case _ => Ok
//       }
//   }

// // object MultiPart {
// //    def apply(name: String, filename: String, mime: String, data: String): MultiPart = {
// //      apply(name, filename, mime, data.getBytes(HttpConstants.utf8))
// //    }

// //    def apply(name: String, filename: String, mime: String, data: Array[Byte]): MultiPart = {
// //       MultiPart(name, filename, mime, new ByteArrayInputStream(data), data.length, n => ())
// //    }
// // }
//   // def uploadFile = silhouette.UnsecuredAction(parse.multipartFormData) { request =>
  def uploadFile = silhouette.UnsecuredAction.async(parse.json) { request =>

  val stringFile = "<span id='resultcode' style='visibility: hidden;'>H</span><span id='resultdesc' style='visibility: hidden;'>Coverage</span><span id='controlno' style='visibility: hidden;'>2</span><span id='patientid' style='visibility: hidden;'>577</span>"

//   val file1 = Files.write(Paths.get("file.html"), stringFile.getBytes(StandardCharsets.UTF_8))
//     println("file = "+file1)
// val bytes: Array[Byte] = Files.readAllBytes(Paths.get("file.html"))
// println("bytes = "+bytes)
// val fileOriginal = new File("file.html")
// // fileOriginal.text = file1
// Files.write(Paths.get("file.html"), stringFile.getBytes(StandardCharsets.UTF_8))

// val tempFile = TemporaryFile("TEST_REMOVE_")
//     Files.copy(fileOriginal.toPath, tempFile.file.toPath, StandardCopyOption.REPLACE_EXISTING)
// var queryStringMap:scala.collection.immutable.Map[String,Seq[String]]=scala.collection.immutable.Map()
//     // val params = parameters.map { case (k, v) => k -> Seq(v)}
//     // val part = FilePart[TemporaryFile](key = tempFile.file.getName, filename = tempFile.
//     // file.getName, contentType = Some("text/plain"), ref = tempFile)
//     val part = FilePart("hello", "file.html", Option("text/plain"), FileIO.fromFile(fileOriginal))
//     val formData = MultipartFormData(dataParts = queryStringMap , files = Seq(part), badParts = Nil)
// println("formData = "+formData)
// val inputstream = new FileInputStream("file.html");

    // val request = FakeRequest("POST", url, FakeHeaders(), formData)


// println("fileOriginal = "+Source.fromFile("file.html").mkString)
// val multi = new MultipartFormData(fileOriginal)
// println("multi === "+multi )
// val fileItem = new DiskFileItem("mainFile", Files.probeContentType(fileOriginal.toPath()), false, fileOriginal.getName(), fileOriginal.length(), fileOriginal.getParentFile());

// println("fileItem = "+fileItem)
//     val contents ="contents string"
// val file = File.createTempFile("sample", ".txt")

// val bw = new java.io.BufferedWriter(new java.io.FileWriter(file))
// bw.write(stringFile);
// bw.close();

// builder.addPart("file", new FileBody(file, org.apache.http.entity.ContentType.create("text/plain"), "sample"))
// builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
// val entity = builder.build

//         val outputstream = new ByteArrayOutputStream
//         entity.writeTo(outputstream)
//         val header = (entity.getContentType.getName -> entity.getContentType.getValue)
//         val response = WS.url("/testUpload").withHeaders(header).post(outputstream.toByteArray())

// println("test = "+sasController.signatureFile)
val res = Http("http://192.168.0.83:9000/v4/signatureFile")
            .postMulti(MultiPart(name="customFile", filename="file.html", mime="text/plain", data=stringFile))
            .header("Content-type", "multipart/form-data")
            .method("POST")
            .option(HttpOptions.readTimeout(100000)).asString

// val asyncHttpClient:AsyncHttpClient = WS.client.underlying
//     val postBuilder = asyncHttpClient.preparePost("http://symlink.dk/code/php/submit/")
//     val builder = postBuilder.addBodyPart(new StringPart("myField", "abc", "UTF-8"))

// postMulti(Source(FilePart("hello", "file.html", Option("text/plain"), FileIO.fromFile(fileOriginal)) :: DataPart("key", "value") :: List()))

// val res = ws.url("http://192.168.0.83:9000/v4/testUpload")
//       // .withHttpHeaders(("content-type" -> "multipart/form-data"))
//       // .setContentType("multipart/form-data")
//       .post(Source(  "http://192.168.0.83:9000/v4/signatureFile"
//         FilePart("File", file1, Option("application/pdf"), FileIO.fromPath(Paths.get(file1)))  :: List()
//       ))

            
db.run {
        SaasTable.result
    } map { operatorsList =>
      Ok(Json.toJson(operatorsList))
    }

// Ok("congratz you did it")
}
}