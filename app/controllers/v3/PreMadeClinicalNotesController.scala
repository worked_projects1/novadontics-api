
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{PreMadeClinicalNotesRow,StaffRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._


class PreMadeClinicalNotesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val PreMadeClinicalNotesFormat = Json.format[PreMadeClinicalNotesRow]
 
   def readAll = silhouette.SecuredAction.async { request =>
    var strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId

     db.run {
          PreMadeClinicalNotesTable.filterNot(_.deleted).filter(_.practiceId === strPracticeId).result
      } map { notesList =>
        Ok(Json.toJson(notesList))
      }
  }

 def create = silhouette.SecuredAction.async(parse.json) { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 
    val preClinicalNotes = for {
      name <- (request.body \ "name").validate[String]
      note <- (request.body \ "note").validate[String]
    } yield {
      PreMadeClinicalNotesRow(None,practiceId,name,note,false)
    }

    preClinicalNotes match {
      case JsSuccess(preMadeClinicalNotes, _) =>
      db.run{
       PreMadeClinicalNotesTable.returning(PreMadeClinicalNotesTable.map(_.id)) += preMadeClinicalNotes
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(preMadeClinicalNotes.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }
  }

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val preClinicalNotes = for {
      name <- (request.body \ "name").validateOpt[String]
      note <- (request.body \ "note").validateOpt[String]
      deleted <- (request.body \ "deleted").validateOpt[Boolean]
    } yield {
      (name,note,deleted)
    }
    preClinicalNotes match {
      case JsSuccess((name,note,deleted), _) => db.run {

        val query = PreMadeClinicalNotesTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            note.map(value => query.map(_.note).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            deleted.map(value => query.map(_.deleted).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }


def delete(id: Long) = silhouette.SecuredAction.async { request =>
      db.run {
          PreMadeClinicalNotesTable.filter(_.id === id).map(_.deleted).update(true)
      } map {
      case 0 => NotFound
      case _ => Ok
    }
}
}


