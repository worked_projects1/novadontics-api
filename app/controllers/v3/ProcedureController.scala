
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{ProcedureCodeRow, StaffRow, InsuranceFeeRow, InsuranceCompanyRow, UserRow}
import play.api.mvc.Result
import scala.concurrent.Await
import scala.concurrent.duration._
   

class ProcedureController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


  implicit val ProcedureFormat = Json.format[ProcedureCodeRow]

 implicit val procedureRowWrites = Writes { request: (ProcedureCodeRow) =>
    val (procedureRow) = request

  Json.obj(
      "id" -> procedureRow.id,
      "procedureCode" -> procedureRow.procedureCode,
      "procedureType" -> procedureRow.procedureType,
      "categoryId" -> procedureRow.categoryId
      )
  }

 case class ProcedureList(
   id: Long,
   procedureCode: String,
   procedureType : String,
   categoryId: Long,
   category: String,
   privateFee: Option[Float],
   deleted : Boolean
 )

  implicit val ProcedureListWrites = Writes { ProcedureListRecord: (ProcedureList) =>
    val (procedureListRow) = ProcedureListRecord

    Json.obj(
      "id" -> procedureListRow.id,
      "procedureCode" -> procedureListRow.procedureCode,
      "procedureType" -> procedureListRow.procedureType,
      "categoryId" -> procedureListRow.categoryId,
      "category" -> procedureListRow.category,
      "privateFee" ->  Math.round(convertOptionFloat(procedureListRow.privateFee) * 100.0) / 100.0,
      "deleted" -> procedureListRow.deleted
    )
  }


  // implicit val InsuranceFeeFormat = Json.format[InsuranceFeeRow]

 implicit val insuranceFeeRowWrites = Writes { request: (InsuranceFeeRow) =>
    val (insuranceFeeRow) = request

    var vInsuranceCompanyName = ""
    var block = db.run{
      DentalCarrierTable.filterNot(_.archived).filter(_.id === insuranceFeeRow.insuranceId).result
        } map { insCompanyMap =>
          vInsuranceCompanyName = insCompanyMap.head.name
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  Json.obj(
      "id" -> insuranceFeeRow.id,
      "practiceId" -> insuranceFeeRow.practiceId,
      "insuranceId" -> insuranceFeeRow.insuranceId,
      "vInsuranceCompanyName" -> vInsuranceCompanyName,
      "procedureCode" -> insuranceFeeRow.procedureCode,
      "fee" -> Math.round(insuranceFeeRow.fee * 100.0) / 100.0
      )
  }


  implicit val InsuranceCompanyFormat = Json.format[InsuranceCompanyRow]

 implicit val insuranceCompanyRowWrites = Writes { request: (InsuranceCompanyRow) =>
    val (insuranceCompanyRow) = request

  Json.obj(
      "id" -> insuranceCompanyRow.id,
      "name" -> insuranceCompanyRow.name
      )
  }


  def readAll = silhouette.SecuredAction.async { request =>
  var insuranceId: Long = 1
  request.identity match {
  case row: StaffRow =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
        ProcedureTable.filterNot(_.deleted).joinLeft(InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === insuranceId)).on(_.procedureCode === _.procedureCode).join(ResourceCategoryTable).on(_._1.categoryId === _.id).result
    } map { procedureList =>
      val data = procedureList.groupBy(_._1._1.id).map {
        case (procedureId, entries) =>
          var fee = Option.empty[Float]
          val procedure = entries.head._1._1
          val insurance = entries.head._1._2.map{ins => fee = Some(ins.fee) }
          val procedureCodeCategory = entries.head._2


          ProcedureList(
            procedure.id.get,
            procedure.procedureCode,
            procedure.procedureType,
            convertOptionLong(procedure.categoryId),
            procedureCodeCategory.name,
            fee,
            procedure.deleted
          )
      }.toList.sortBy(_.id)
      Ok(Json.toJson(data))
    }

    case row: UserRow =>
    db.run {
        ProcedureTable.filterNot(_.deleted).joinLeft(InsuranceFeeTable.filter(_.insuranceId === insuranceId)).on(_.procedureCode === _.procedureCode).join(ResourceCategoryTable).on(_._1.categoryId === _.id).result
    } map { procedureList =>
      val data = procedureList.groupBy(_._1._1.id).map {
        case (procedureId, entries) =>
          var fee = Option.empty[Float]
          val procedure = entries.head._1._1
          // val insurance = entries.head._1._2.map{ins => fee = Some(ins.fee) }
          // val courses = rows.map(row => Output.output(row._1, row._2.isDefined))
          val procedureCodeCategory = entries.head._2

          ProcedureList(
            procedure.id.get,
            procedure.procedureCode,
            procedure.procedureType,
            convertOptionLong(procedure.categoryId),
            procedureCodeCategory.name,
            None,
            procedure.deleted
          )
      }.toList.sortBy(_.id)
      Ok(Json.toJson(data))
    }
            }


  }

  def read(id: Long) = silhouette.SecuredAction.async {
    db.run {
      ProcedureTable.filterNot(_.deleted).filter(_.id === id).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case Some(procedure) => DBIO.successful(Ok(Json.toJson(procedure)))
        case None => DBIO.successful(NotFound)
      }
    } 
}


 def create = silhouette.SecuredAction.async(parse.json) { request =>
    val providers = for {
      procedureCode <- (request.body \ "procedureCode").validate[String]
      procedureType <- (request.body \ "procedureType").validate[String]
      categoryId <- (request.body \ "categoryId").validateOpt[Long]
    } yield ProcedureCodeRow(None, procedureCode, procedureType, categoryId, false)

    providers match {
      case JsSuccess(procedureRow, _) => db.run{
       ProcedureTable.returning(ProcedureTable.map(_.id)) += procedureRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(procedureRow.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }

  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val providers = for {
      procedureCode <- (request.body \ "procedureCode").validateOpt[String]
      procedureType <- (request.body \ "procedureType").validateOpt[String]
      categoryId <- (request.body \ "categoryId").validateOpt[Long]
    } yield {
      (procedureCode, procedureType, categoryId)
    }

    providers match {
      case JsSuccess((procedureCode, procedureType, categoryId), _) => db.run {
        val query = ProcedureTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            procedureCode.map(value => query.map(_.procedureCode).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            procedureType.map(value => query.map(_.procedureType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(categoryId).map(value => query.map(_.categoryId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

                ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }
  
  def delete(id: Long) = silhouette.SecuredAction.async {
      db.run {
          ProcedureTable.filter(_.id === id).map(_.deleted).update(true)
      } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

  def feeReadAll(procedureCode: Option[String]) = silhouette.SecuredAction.async { request =>
    var strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
    var vProcedureCode = convertOptionString(procedureCode)
    
    var query = InsuranceFeeTable.sortBy(_.id)
    if(vProcedureCode != ""){
      query = query.filter(_.procedureCode === vProcedureCode)
    }

    db.run {
      query.filter(_.practiceId === strPracticeId).result
    } map { insuranceFeeList =>
      Ok(Json.toJson(insuranceFeeList))
    }
  }

  def companyReadAll = silhouette.SecuredAction.async { request =>

    db.run {
      InsuranceCompanyTable.sortBy(_.companyOrder).result
    } map { insuranceCompanyList =>
      Ok(Json.toJson(insuranceCompanyList))
    }
  }

  def updateInsuranceFee = silhouette.SecuredAction.async(parse.json) { request =>

    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId

    val insuranceFee = for {
      insuranceId <- (request.body \ "insuranceId").validate[Long]
      procedureCode <- (request.body \ "procedureCode").validate[String]
      fee <- (request.body \ "fee").validate[Float]
    } yield {
      (insuranceId, procedureCode, fee)
    }

    insuranceFee match {
      case JsSuccess((insuranceId, procedureCode, fee), _) => 
      db.run {
        val query = InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === insuranceId).filter(_.procedureCode === procedureCode)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(fee).map(value => query.map(_.fee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map { _ => Ok }
          case false =>
            (InsuranceFeeTable.returning(InsuranceFeeTable.map(_.id)) += InsuranceFeeRow(None, practiceId, insuranceId, procedureCode, fee))map {
            case 0L => PreconditionFailed
            case id => Ok
          }
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


def updateInsuranceFeeByPCode (procedureCode: String) = silhouette.SecuredAction.async(parse.json) { request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val insFee = for {
      insuranceFee <- (request.body \ "insuranceFee").validate[JsValue]
    } yield {
      (insuranceFee)
    }

    insFee match {
      case JsSuccess((insuranceFee), _) => 
      if(insuranceFee != None && insuranceFee != ""){
          var jsonList = insuranceFee.as[List[JsValue]]
            jsonList.foreach(result => { 
              val insuranceId = (result \ "insuranceId").as[Long]
              val fee = (result \ "fee").asOpt[Float]

    if(fee != null && fee != None){
     var block = db.run {
        val query = InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === insuranceId).filter(_.procedureCode === procedureCode)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            fee.map(value => query.map(_.fee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map { _ => Ok }
          case false =>
            (InsuranceFeeTable.returning(InsuranceFeeTable.map(_.id)) += InsuranceFeeRow(None, practiceId, insuranceId, procedureCode, convertOptionFloat(fee)))map {
            case 0L => PreconditionFailed
            case id => Ok
            }
          }
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    } else {
      db.run{
          InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === insuranceId).filter(_.procedureCode === procedureCode).delete map {
          case 0 => NotFound
          case _ => Ok
          }
    }
    }
    })
   }
      Future(Ok)
      case JsError(_) => Future.successful(BadRequest)
    }
  }
}


