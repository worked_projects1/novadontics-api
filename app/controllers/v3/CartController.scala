package controllers.v3

import co.spicefactory.util.BearerTokenEnv
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables._
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CartController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._

  case class Product(
                      id: Option[Long],
                      name: String,
                      description: Option[String],
                      serialNumber: String,
                      imageUrl: Option[String],
                      vendorId: Long,
                      categoryId: Option[Long],
                      price: Double,
                      manufacturer: String,
                      quantity: Option[Long],
                      favourite: Option[Boolean],
                      listPrice: Option[Double],
                      amount: Option[Long],
                      unit: Option[String],
                      financeEligible: Option[Boolean]
                    )
  object Product {
    implicit val writes = Json.writes[Product]
  }


  implicit val writes = Writes { result: (CartRow, ProductRow, VendorRow) =>
    val (cartRow, productRow, vendorRow) = result

   /* Json.obj(
      "vendorId" -> vendorRow.id,
      "vendorName" -> vendorRow.name,
      "orderDetails" -> Json.arr(
        Json.obj(
          "id" -> productRow.id,
          "qty" -> cartRow.quantity,
          "overnight" -> false,
          "productInfo" ->
            Json.obj(
              "id" -> productRow.id,
              "name" -> productRow.name,
              "description" -> productRow.description,
              "imageUrl" -> productRow.imageUrl,
              "manufacturer" -> vendorRow.name,
              "vendorId" -> vendorRow.id,
              "categoryId" -> productRow.categoryId,
              "price" -> productRow.price,
              "serialNumber" -> productRow.serialNumber,
              "quantity" -> productRow.quantity
            )
        )
      )
    )*/
    Json.obj(
      "id" -> productRow.id,
      "quantity" -> cartRow.quantity
    )
  }

  def readAll = silhouette.SecuredAction.async { request =>

    db.run {
      CartTable.filter(_.userId === request.identity.id).join(ProductTable).on(_.productId === _.id).join(VendorTable).on(_._2.vendorId === _.id).result
    } map { cart =>

      if (cart == None) {
        Ok
      } else {
        val result = cart.groupBy(_._1._1.id).map { row =>
          val (_, group) = row
          val ((cart, product), vendor) = group.head

          (cart, product, vendor)
        }.toList.sortBy(_._1.id).reverse
        //Console.println("result size == " + result.size)
        Ok(
          Json.toJson(result)
          /*Json.obj(
            "total"->result.size,
            "cart"->Json.toJson(result)
          )*/

        )
      }
    }

    /*  for{
        allOrdersByVendor <-  CartTable.filter(_.userId === request.identity.id).join(OrderItemTable).on(_.productId === _.productId).join(OrderItemTable).on(_._1.vendorId === _.productVendorId).result

      }yield{
        allOrdersByVendor
        allOrdersByVendor.map {
          case (id, productId) =>
            Json.obj(
              "id" -> id,
              "productId" -> productId
            )
        }
      }*/

  }
  
  (((ProductTable,VendorTable),CustomerFavouritesTable),CartTable)
  
  def getCartWithProducts = silhouette.SecuredAction.async { request =>
    db.run {
      ProductTable.filterNot(_.deleted)
        .join(VendorTable).on(_.vendorId === _.id)
        .joinLeft(CustomerFavouritesTable.filter(_.customer === request.identity.id.get)).on(_._1.id === _.product)
        .join(CartTable.filter(_.userId === request.identity.id)).on(_._1._1.id === _.productId)
        .map({
          case (((products, vendor), favourites),cart) => (products, vendor.name, favourites.isDefined,cart.quantity)
        })
        .result
    } map { products =>
      val data = products.groupBy(_._1.id).map {
        case (id, items) => {
          val item = items.head._1
          val vendorName = items.head._2
          val favourite = items.head._3
          val cartQuantity = items.head._4

          var productPrice = Math.round(item.price * 100.0) / 100.0
          var listPrice = Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0

          Product(
            item.id,
            item.name,
            item.description,
            item.serialNumber,
            item.imageUrl,
            item.vendorId,
            item.categoryId,
            productPrice,
            vendorName,
            item.quantity,
            Some(favourite),
            Some(listPrice),
            cartQuantity,
            item.unit,
            item.financeEligible
          )
        }
      }.toList.sortBy(_.serialNumber)
      Ok(Json.toJson(data))
    }
  }

  def update = silhouette.SecuredAction(DentistOnly).async(parse.json) { request =>
    val cart = for {
      productId <- (request.body \ "productId").validateOpt[Long]
      quantity <- (request.body \ "quantity").validateOpt[Long]
    } yield {
      CartRow(None, request.identity.id, productId, quantity)
    }

    cart match {
      case JsSuccess(cartRow, _) =>
        db.run {
          CartTable.filter(_.userId === cartRow.userId).filter(_.productId === cartRow.productId).exists.result.flatMap { exists =>
            if (!exists) {
              CartTable.returning(CartTable.map(_.id)) += cartRow
            } else {
              for {
                prevQuantity <- CartTable.filter(_.userId === cartRow.userId).filter(_.productId === cartRow.productId).map(_.quantity).result.head
              } yield {
                prevQuantity
                var total: Long = convertOptionLong(prevQuantity) + convertOptionLong(cartRow.quantity)
                if(total <= 0){
                  total = 1
                }
                db.run {
                  CartTable.filter(_.userId === cartRow.userId).filter(_.productId === cartRow.productId).map(_.quantity).update(Some(total))
                }
              }
            }
          }
        } map (_ => Ok)
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(DentistOnly).async { request =>
    /*val cart = for {
      productId <- (request.body \ "productId").validateOpt[Long]
      vendorId <- (request.body \ "vendorId").validateOpt[Long]
    } yield {
      (productId,vendorId)
    }

    cart match {
      case JsSuccess((productId,vendorId), _) =>
        db.run {
          CartTable.filter(_.productId === productId).filter(_.vendorId === vendorId).delete map {
            case 0 => NotFound
            case _ => Ok
          }
        }
      case JsError(_) => Future.successful(BadRequest)
    }*/
    db.run {
      CartTable.filter(_.userId === request.identity.id).filter(_.productId === id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

  def deleteAll = silhouette.SecuredAction(DentistOnly).async { request =>
    db.run {
      CartTable.filter(_.userId === request.identity.id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

}
