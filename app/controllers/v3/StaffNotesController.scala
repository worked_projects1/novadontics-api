
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffNotesRow}
import play.api.mvc.Result
import java.text.SimpleDateFormat
import java.time.ZonedDateTime;

class StaffNotesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

    val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
    val outputTimeFormat = new SimpleDateFormat("hh:mm a")
implicit val StaffNotesWrites = Writes { staffNotes: StaffNotesRow =>
    var outputTime = ""
    if(staffNotes.time != None){
    val inputTime = inputTimeFormat.parse(convertOptionalLocalTime(staffNotes.time).toString)
    outputTime = outputTimeFormat.format(inputTime)
    }
    Json.obj(
      "id" -> staffNotes.id,
      "staffId" -> staffNotes.staffId,
      "patientId" -> staffNotes.patientId,
      "date" -> staffNotes.date,
      "notes" -> staffNotes.notes,
      "time" -> outputTime,
      "notesFor" -> staffNotes.notesFor
    )
  }
  def read(patientId: Long, staffId: Option[String]) = silhouette.SecuredAction.async { request =>
  var query = StaffNotesTable.filter(_.patientId === patientId)
  var currentDateTime = ZonedDateTime.now()

    if(staffId != None && staffId != Some("")){
      query = query.filter(_.staffId === staffId)
    }
    db.run {
        query.result
      } map { staffNotesMap =>
      Ok(Json.toJson(staffNotesMap.sortBy(t=> (t.date,t.time)).reverse))
    }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
    val formatter = DateTimeFormat.forPattern("hh:mm a");
    val staffNotes = for {
      patientId <- (request.body \ "patientId").validate[Long]
      date <- (request.body \ "date").validate[LocalDate]
      staffId <- (request.body \ "staffId").validate[String]
      notes <- (request.body \ "notes").validate[String]
      time <- (request.body \ "time").validate[String]
      notesFor <- (request.body \ "notesFor").validate[String]
    } yield {
        var timeValue: Option[LocalTime] = None
        if(time != None && time != Some("")){
          timeValue = Some(LocalTime.parse(time, formatter))
        }
      StaffNotesRow(None,staffId,patientId,date,notes,timeValue,Some(notesFor))
    }

    staffNotes match {
      case JsSuccess(staffNoteRow, _) => 
          db.run {
            StaffNotesTable.returning(StaffNotesTable.map(_.id)) += staffNoteRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(staffNoteRow.copy(id = Some(id))))
                }
      
      case JsError(_) => Future.successful(BadRequest)
    }
  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
val formatter = DateTimeFormat.forPattern("hh:mm a");
    val staffNotes = for {
      patientId <- (request.body \ "patientId").validateOpt[Long]
      date <- (request.body \ "date").validateOpt[LocalDate]
      staffId <- (request.body \ "staffId").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      time <- (request.body \ "time").validateOpt[String]
      notesFor <- (request.body \ "notesFor").validateOpt[String]
    } yield {
      var timeValue: Option[LocalTime] = None
      if(time != None && time != Some("")){
        timeValue = Some(LocalTime.parse(convertOptionString(time), formatter))
      }
      (patientId,date,staffId,notes,timeValue,notesFor)
    }
    staffNotes match {
      case JsSuccess((patientId,date,staffId,notes,timeValue,notesFor), _) => db.run {
        val query = StaffNotesTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            patientId.map(value => query.map(_.patientId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            date.map(value => query.map(_.date).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            staffId.map(value => query.map(_.staffId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            notes.map(value => query.map(_.notes).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            timeValue.map(value => query.map(_.time).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            notesFor.map(value => query.map(_.notesFor).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }
  

def delete(id: Long) = silhouette.SecuredAction.async { request =>   
       db.run {
         StaffNotesTable.filter(_.id === id).delete.map {
        case 0 => NotFound
        case _ => Ok
       }
    }
      
  }
}


