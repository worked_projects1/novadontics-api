
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import scala.util.control._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{ AppointmentRow, AppointmentLogRow,ReferralSourceRow,StaffRow,OperatorTableRow,RoleRow,AppointmentReasonRow,ListOptionsRow,AppointmentTreatmentPlanRow}
import play.api.mvc.Result
import org.joda.time.DateTime
import org.joda.time.{ LocalDate, LocalTime}

import java.time.format.DateTimeFormatter
import java.time.LocalTime
import java.text.SimpleDateFormat
import java.io.File
import java.net.{URL,HttpURLConnection,URLConnection}
import com.github.tototoshi.csv._
import play.api.mvc.Action
import java.util.Date;
import scala.collection.mutable.ListBuffer

//Pdf
import akka.stream.scaladsl.StreamConverters
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import play.api.http.HttpEntity
import play.api.{Application, Logger}
import java.awt.geom.AffineTransform
import play.api.mvc.{ResponseHeader, Result}

class AppointmentController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

private val logger = Logger(this.getClass)

  implicit val roleFormat = Json.format[RoleRow]

  implicit val AppointmentReasonFormat = Json.format[AppointmentReasonRow]

  implicit val ListOptionsFormat = Json.format[ListOptionsRow]

  implicit val ReferralSourceFormat = Json.format[ReferralSourceRow]

 implicit val appointmentRowWrites = Writes { appointmentRecord: (AppointmentRow) =>
    val (appointmentsRow) = appointmentRecord

val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
val outputTimeFormat = new SimpleDateFormat("hh:mm a")
val inputStartTime = inputTimeFormat.parse(appointmentsRow.startTime.toString)
val inputEndTime = inputTimeFormat.parse(appointmentsRow.endTime.toString)
val outputStartTime = outputTimeFormat.format(inputStartTime)
val outputEndTime = outputTimeFormat.format(inputEndTime)

  Json.obj(
      "id" -> appointmentsRow.id,
      "practiceId" -> appointmentsRow.practiceId,
      "date" -> appointmentsRow.date,
      "startTime" -> outputStartTime,
      "endTime" -> outputEndTime,
      "patientId" -> appointmentsRow.patientId,
      "operatoryId" -> appointmentsRow.operatoryId,
      "providerId" -> appointmentsRow.providerId,
      "procedureType" -> appointmentsRow.procedureType,
      "procedureCode" -> appointmentsRow.procedureCode,
      "anaesthesia" -> appointmentsRow.anaesthesia,
      "notes" -> appointmentsRow.notes,
      "status" -> appointmentsRow.status,
      "amountToBeCollected" -> appointmentsRow.amountToBeCollected,
      "reason" -> appointmentsRow.reason,
      "patientName" -> appointmentsRow.patientName,
      "referredBy" -> appointmentsRow.referredBy,
      "alerts" -> appointmentsRow.alerts,
      "insurance" -> appointmentsRow.insurance,
      "phoneNumber" -> appointmentsRow.phoneNumber,
      "assistantId" -> appointmentsRow.assistantId,
      "email" -> appointmentsRow.email,
      "procedure" -> appointmentsRow.procedure,
      "creator" -> appointmentsRow.creator,
      "reminderSentDate" -> appointmentsRow.reminderSentDate
      )
  }


 implicit val appointmentLogRowWrites = Writes { request: (AppointmentLogRow) =>
    val (appointmentLogRow) = request

    val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
    val outputTimeFormat = new SimpleDateFormat("hh:mm a")
    val inputStartTime = inputTimeFormat.parse(appointmentLogRow.startTime.toString)
    val inputEndTime = inputTimeFormat.parse(appointmentLogRow.endTime.toString)
    val outputStartTime = outputTimeFormat.format(inputStartTime)
    val outputEndTime = outputTimeFormat.format(inputEndTime)

      Json.obj(
          "id" -> appointmentLogRow.id,
          "appointmentId" -> appointmentLogRow.appointmentId,
          "practiceId" -> appointmentLogRow.practiceId,
          "date" -> appointmentLogRow.date,
          "startTime" -> outputStartTime,
          "endTime" -> outputEndTime,
          "patientId" -> appointmentLogRow.patientId,
          "operatoryId" -> appointmentLogRow.operatoryId,
          "providerId" -> appointmentLogRow.providerId,
          "procedureType" -> appointmentLogRow.procedureType,
          "procedureCode" -> appointmentLogRow.procedureCode,
          "anaesthesia" -> appointmentLogRow.anaesthesia,
          "notes" -> appointmentLogRow.notes,
          "status" -> appointmentLogRow.status,
          "operation" -> appointmentLogRow.operation,
          "updatedBy" -> appointmentLogRow.updatedBy,
          "updatedAt" -> appointmentLogRow.updatedAt
          )
  }

  case class AppointmentList(
  id: Option[Long],
  practiceId: Long,
  date: LocalDate,
  dob: String,
  startTime: LocalTime,
  endTime: LocalTime,
  medicalCondition: Boolean,
  patientId: Long,
  patientName: String,
  operatoryId: Long,
  operatoryName: String,
  providerId: Long,
  title: String,
  providerName: String,
  color: String,
  procedureType: String,
  procedureCode: String,
  anaesthesia: String,
  notes: String,
  status: String,
  amountToBeCollected: Float,
  reason : String,
  referredBy : String,
  alerts : String,
  insurance : String,
  phoneNumber : String,
  assistantId: Option[String],
  // assistantName: String,
  email: Option[String],
  procedure: Option[String],
  creator: Option[String],
  reminderSentDate : Option[LocalDate]
)

implicit val appointmentListRowWrites = Writes { appointmentListRecord: (AppointmentList) =>
    val (appointmentListsRow) = appointmentListRecord

val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
val outputTimeFormat = new SimpleDateFormat("hh:mm a")
val inputStartTime = inputTimeFormat.parse(appointmentListsRow.startTime.toString)
val inputEndTime = inputTimeFormat.parse(appointmentListsRow.endTime.toString)
val outputStartTime = outputTimeFormat.format(inputStartTime)
val outputEndTime = outputTimeFormat.format(inputEndTime)

  Json.obj(
      "id" -> appointmentListsRow.id,
      "practiceId" -> appointmentListsRow.practiceId,
      "date" -> appointmentListsRow.date,
      "dob" -> appointmentListsRow.dob,
      "startTime" -> outputStartTime,
      "endTime" -> outputEndTime,
      "medicalCondition" -> appointmentListsRow.medicalCondition,
      "patientId" -> appointmentListsRow.patientId,
      "patientName" -> appointmentListsRow.patientName,
      "operatoryId" -> appointmentListsRow.operatoryId,
      "operatoryName" -> appointmentListsRow.operatoryName,
      "providerId" -> appointmentListsRow.providerId,
      "title" -> appointmentListsRow.title,
      "providerName" -> appointmentListsRow.providerName,
      "color" -> appointmentListsRow.color,
      "procedureType" -> appointmentListsRow.procedureType,
      "procedureCode" -> appointmentListsRow.procedureCode,
      "anaesthesia" -> appointmentListsRow.anaesthesia,
      "notes" -> appointmentListsRow.notes,
      "status" -> appointmentListsRow.status,
      "amountToBeCollected" ->  Math.round(appointmentListsRow.amountToBeCollected * 100.0) / 100.0,
      "reason" -> appointmentListsRow.reason,
      "referredBy" -> appointmentListsRow.referredBy,
      "alerts" -> appointmentListsRow.alerts,
      "insurance" -> appointmentListsRow.insurance,
      "phoneNumber" -> appointmentListsRow.phoneNumber,
      "assistantId" -> appointmentListsRow.assistantId,
      // "assistantName" -> appointmentListsRow.assistantName,
      "email" -> appointmentListsRow.email,
      "procedure" -> appointmentListsRow.procedure,
      "creator" -> appointmentListsRow.creator,
      "reminderSentDate" -> appointmentListsRow.reminderSentDate
      )
  }

case class AppointmentPDF(
  date: String,
  startTime: String,
  endTime: String,
  patientName: String,
  providerName: String,
  operatoryName: String,
  procedureType: String,
  procedureCode: String,
  reason: String,
  email: String,
  phoneNumber: String
  )


implicit val AppointmentPDFWrites = Writes { appointmentPdfRecord: (AppointmentPDF) =>
    val (appointmentPDFRow) = appointmentPdfRecord

  Json.obj(
      "date" -> appointmentPDFRow.date,
      "startTime" -> appointmentPDFRow.startTime,
      "endTime" -> appointmentPDFRow.endTime,
      "patientName" -> appointmentPDFRow.patientName,
      "providerName" -> appointmentPDFRow.providerName,
      "operatoryName" -> appointmentPDFRow.operatoryName,
      "procedureType" -> appointmentPDFRow.procedureType,
      "procedureCode" -> appointmentPDFRow.procedureCode,
      "reason" -> appointmentPDFRow.reason,
      "email" -> appointmentPDFRow.email,
      "phoneNumber" -> appointmentPDFRow.phoneNumber
      )
  }

case class CancelReport(
  date: String,
  patientName: String,
  phoneNumber : String,
  providerName: String,
  referredBy: String,
  reason : String,
  status : String,
  email: String
  )

implicit val CancelReportWrites = Writes { CancelReportRecord: (CancelReport) =>
    val (cancelReportRow) = CancelReportRecord

  Json.obj(
      "date" -> cancelReportRow.date,
      "patientName" -> cancelReportRow.patientName,
      "phoneNumber" -> cancelReportRow.phoneNumber,
      "email" -> cancelReportRow.email,
      "providerName" -> cancelReportRow.providerName,
      "referredBy" -> cancelReportRow.referredBy,
      "reason" -> cancelReportRow.reason,
      "status" -> cancelReportRow.status
      )
  }

case class AppointmentDailyBriefPdf(
   patientName: String,
   phoneNumber : String,
   startTime : LocalTime,
   endTime : LocalTime,
   providerName: String,
   reason : String,
   referredBy: String,
   alerts : String,
   patientPhoto: String
)

case class AppointmentDailyBrief(
  id : Long,
  date : LocalDate,
  startTime : LocalTime,
  endTime : LocalTime,
  providerName : String,
  patientName : String,
  patientPhoto : String,
  phoneNumber : String,
  reason : String,
  referredBy: String,
  alerts : String
  )

  implicit val dailyBriefRowWrites = Writes { dailyBriefRecord: (AppointmentDailyBrief) =>
      val (dailyBriefRow) = dailyBriefRecord

val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
val outputTimeFormat = new SimpleDateFormat("hh:mm a")
val inputStartTime = inputTimeFormat.parse(dailyBriefRow.startTime.toString)
val inputEndTime = inputTimeFormat.parse(dailyBriefRow.endTime.toString)
val outputStartTime = outputTimeFormat.format(inputStartTime)
val outputEndTime = outputTimeFormat.format(inputEndTime)

    Json.obj(
        "id" -> dailyBriefRow.id,
        "date" -> dailyBriefRow.date,
        "startTime" -> outputStartTime,
        "endTime" -> outputEndTime,
        "providerName" -> dailyBriefRow.providerName,
        "patientName" -> dailyBriefRow.patientName,
        "patientPhoto" -> dailyBriefRow.patientPhoto,
        "phoneNumber" -> dailyBriefRow.phoneNumber,
        "reason" -> dailyBriefRow.reason,
        "referredBy" -> dailyBriefRow.referredBy,
        "alerts" -> dailyBriefRow.alerts
    )
  }

case class PatientAppointmentList(
  id: Option[Long],
  practiceId: Long,
  date : LocalDate,
  dayOfWeek: String,
  startTime : LocalTime,
  endTime : LocalTime,
  patientId : Long,
  patientName: String,
  operatoryId : Long,
  operatoryName: String,
  providerId : Long,
  providerName: String,
  procedureType : String,
  procedureCode : String,
  anaesthesia : String,
  notes : String,
  status : String,
  state: String,
  amountToBeCollected: Float,
  reason : String,
  referredBy : String,
  alerts : String,
  insurance : String,
  phoneNumber : String,
  assistantId: Option[String],
  email: Option[String],
  procedure: Option[String],
  creator: Option[String],
  reminderSentDate : Option[LocalDate]
)

implicit val patientListRowWrites = Writes { patientListRecord: (PatientAppointmentList) =>
    val (patientListsRow) = patientListRecord

val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
val outputTimeFormat = new SimpleDateFormat("hh:mm a")
val inputStartTime = inputTimeFormat.parse(patientListsRow.startTime.toString)
val inputEndTime = inputTimeFormat.parse(patientListsRow.endTime.toString)
val outputStartTime = outputTimeFormat.format(inputStartTime)
val outputEndTime = outputTimeFormat.format(inputEndTime)

  Json.obj(
      "id" -> patientListsRow.id,
      "practiceId" -> patientListsRow.practiceId,
      "date" -> patientListsRow.date,
      "dayOfWeek" -> patientListsRow.dayOfWeek,
      "startTime" -> outputStartTime,
      "endTime" -> outputEndTime,
      "patientId" -> patientListsRow.patientId,
      "patientName" -> patientListsRow.patientName,
      "operatoryId" -> patientListsRow.operatoryId,
      "operatoryName" -> patientListsRow.operatoryName,
      "providerName" -> patientListsRow.providerName,
      "providerId" -> patientListsRow.providerId,
      "procedureType" -> patientListsRow.procedureType,
      "procedureCode" -> patientListsRow.procedureCode,
      "anaesthesia" -> patientListsRow.anaesthesia,
      "notes" -> patientListsRow.notes,
      "status" -> patientListsRow.status,
      "state" -> patientListsRow.state,
      "amountToBeCollected" ->  Math.round(patientListsRow.amountToBeCollected * 100.0) / 100.0,
      "reason" -> patientListsRow.reason,
      "referredBy" -> patientListsRow.referredBy,
      "alerts" -> patientListsRow.alerts,
      "insurance" -> patientListsRow.insurance,
      "phoneNumber" -> patientListsRow.phoneNumber,
      "assistantId" -> patientListsRow.assistantId,
      "email" -> patientListsRow.email,
      "procedure" -> patientListsRow.procedure,
      "creator" -> patientListsRow.creator,
      "reminderSentDate" -> patientListsRow.reminderSentDate
      )
  }

    case class RecallReportRow(
    patientName : String,
    phoneNumber : String,
    email : String,
    address : String,
    providerName : String,
    referredBy : String,
    recallType: String,
    recallCode: String,
    interval: String,
    recallDueDate: LocalDate,
    recallNote: String
  )

implicit val RecallReportWrites = Writes { RecallReportRecord: (RecallReportRow) =>
    val (recallReportRow) = RecallReportRecord

  Json.obj(
      "patientName" -> recallReportRow.patientName,
      "phoneNumber" -> recallReportRow.phoneNumber,
      "email" -> recallReportRow.email,
      "address" -> recallReportRow.address,
      "providerName" -> recallReportRow.providerName,
      "referredBy" -> recallReportRow.referredBy,
      "nextVisitdate" -> recallReportRow.recallDueDate,
      "recallType" -> recallReportRow.recallType,
      "recallCode" -> recallReportRow.recallCode,
      "interval" -> recallReportRow.interval,
      "recallNote" -> recallReportRow.recallNote
      )
  }

  def appointmentReasonReadAll = silhouette.SecuredAction.async { request =>
    db.run {
      AppointmentReasonTable.sortBy(_.id).result
    } map { appointmentReason =>
      Ok(Json.toJson(appointmentReason))
    }
  }


  def readAll = silhouette.SecuredAction.async { request =>
    var medicalCondition: Boolean = false
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

      db.run {
            AppointmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
            .filterNot(_.status.toLowerCase === "no show").filterNot(_.status.toLowerCase === "canceled")
            .join(ProviderTable).on(_.providerId === _.id)
            .join(OperatorTable).on(_._1.operatoryId === _.id)
            .result
          } map { appointment =>

            val data = appointment.groupBy(_._1._1.id).map {
              case (appointmentId, entries) =>
                val entry = entries.head._1._1
                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                val providerColor = entries.head._1._2.color
                val providerName = providerFirstName + " " + providerLastName

          var medicalCondition :Boolean  = false
          var block1 = db.run{
                MedicalConditionLogTable.filter(_.patientId === entry.patientId).length.result
            } map { result =>
              if(result > 0) {
                  medicalCondition = true
              }
            }
          var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var patientDob = ""
          var block3 = db.run{
               StepTable.filter(_.treatmentId === entry.id).result.map { stepRows =>
            val steps = stepRows.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)
            patientDob = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]))
            }
          }
          var AwaitResult3 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))


                AppointmentList(
                  Some(entry.id.get),
                  entry.practiceId,
                  entry.date,
                  patientDob,
                  entry.startTime,
                  entry.endTime,
                  medicalCondition,
                  entry.patientId,
                  entry.patientName,
                  entry.operatoryId,
                  operatoryName,
                  entry.providerId,
                  providerTitle,
                  providerName,
                  providerColor,
                  entry.procedureType,
                  entry.procedureCode,
                  entry.anaesthesia,
                  entry.notes,
                  entry.status,
                  entry.amountToBeCollected,
                  entry.reason,
                  entry.referredBy,
                  entry.alerts,
                  entry.insurance,
                  entry.phoneNumber,
                  entry.assistantId,
                  // assistantName,
                  entry.email,
                  entry.procedure,
                  entry.creator,
                  entry.reminderSentDate
                )
            }.toList.sortBy(_.id)
            Ok(Json.toJson(data))
          }
  }

    def patientAppointments(patientId: Long) = silhouette.SecuredAction.async {
    var state: String = ""
    db.run {
      AppointmentTable.filterNot(_.deleted).filter(_.patientId === patientId)
      .filterNot(_.status.toLowerCase === "no show").filterNot(_.status.toLowerCase === "canceled")
      .join(ProviderTable).on(_.providerId === _.id)
      .join(OperatorTable).on(_._1.operatoryId === _.id)

      .result
     } map { appointment =>
             val data = appointment.groupBy(_._1._1.id).map {
              case (id,entries) =>
                val entry = entries.head._1._1

                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                val providerName = providerTitle + " " + providerFirstName + " " + providerLastName

                val currentDate: LocalDate = new LocalDate()
                val currentTime: LocalTime = new LocalTime()

                if(currentDate > entry.date) {
                  state = "Past"
                } else if(currentDate == entry.date){
                  if(currentTime > entry.startTime){
                     state = "Past"
                  } else{
                     state = "Future"
                  }
                } else if(currentDate < entry.date){
                   state = "Future"
                }

                val dayOfWeek = DateTimeFormat.forPattern("EEEE").print(entry.date)

                PatientAppointmentList(
                  Some(entry.id.get),
                  entry.practiceId,
                  entry.date,
                  dayOfWeek,
                  entry.startTime,
                  entry.endTime,
                  entry.patientId,
                  entry.patientName,
                  entry.operatoryId,
                  operatoryName,
                  entry.providerId,
                  providerName,
                  entry.procedureType,
                  entry.procedureCode,
                  entry.anaesthesia,
                  entry.notes,
                  entry.status,
                  state,
                  entry.amountToBeCollected,
                  entry.reason,
                  entry.referredBy,
                  entry.alerts,
                  entry.insurance,
                  entry.phoneNumber,
                  entry.assistantId,
                  entry.email,
                  entry.procedure,
                  entry.creator,
                  entry.reminderSentDate
                )  
                }.toList.sortBy(_.date).reverse
            Ok(Json.toJson(data))
          }   
  } 


   def readAppointment(date: Option[String]) = silhouette.SecuredAction.async { request =>
     var operatories: Long = 0
     var providers: Long = 0
     var workingHours: Long = 0
     var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
     var medicalCondition: Boolean = false


   val count = db.run{
          for {
            operatoriesLength <- OperatorTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result
            providersLength <- ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result  
            workingHoursLength <- WorkingHoursTable.filter(_.practiceId === practiceId).length.result
          } yield { (operatories = operatoriesLength, providers = providersLength,  workingHours = workingHoursLength) }
   }
  
   var AwaitResult = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

    if(operatories == 0 || providers == 0 || workingHours == 0){
      db.run {
        AppointmentTable.sortBy(_.id).result
      } map { appointmentTableList =>
        val obj = Json.obj()
        val newObj = obj + ("status code" -> JsString("1001")) + ("Result" -> JsString("Failed")) + ("message" -> JsString("Appointment setup is Pending"))
        PreconditionFailed{Json.toJson(Array(newObj))}
      }
    }

    else {
         db.run {
            AppointmentTable.filterNot(_.deleted).filterNot(_.status.toLowerCase === "canceled").filterNot(_.status.toLowerCase === "no show")
            .filter(_.practiceId === practiceId)
            .filter(_.date === LocalDate.parse(convertOptionString(date)))
            .join(ProviderTable).on(_.providerId === _.id)
            .join(OperatorTable).on(_._1.operatoryId === _.id)
            .result
          } map { appointment =>
            val data = appointment.groupBy(_._1._1.id).map {
              case (appointmentId, entries) =>
                val entry = entries.head._1._1
                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                val providerColor = entries.head._1._2.color
                val providerName = providerFirstName + " " + providerLastName
                var changedProcedure = ""
                var dataList : List[JsValue] = List()
                val obj = Json.obj()

              var medicalCondition :Boolean  = false
              var block1 = db.run{
                    MedicalConditionLogTable.filter(_.patientId === entry.patientId).length.result
                } map { result =>
                  if(result > 0) {
                      medicalCondition = true
                  }
                }
            var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var patientDob = ""
          val block3 = db.run{
                  for {
                  step <-StepTable.filter(_.treatmentId === entry.patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
                  }yield{
                  patientDob = (step.value \ "birth_date").as[String]
                  }
                }
          var AwaitResult3 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var tempProcedure = convertOptionString(entry.procedure)
          if(entry.status.toLowerCase != "completed"){
            if(tempProcedure != null && tempProcedure != ""){
              var temp = tempProcedure.replace('★', '"')
              val jsonObject: JsValue = Json.parse(temp)
              val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

              if(procedureJson != None){
                procedureJson.get.foreach(s => {
                dataList = dataList :+ s.as[JsObject] + ("checked" -> JsBoolean(true))
              })
            }
          }
          }
          if(entry.status.toLowerCase != "completed" && entry.status.toLowerCase != "canceled" && entry.status.toLowerCase != "no show"){
          var feeBlock = db.run{
            TreatmentPlanDetailsTable.filter(_.patientId === entry.patientId).filter(s=> s.status === "Accepted" || s.status === "Proposed").sortBy(_.id).joinLeft(AppointmentTreatmentPlanTable).on(_.id === _.treatmentPlanId).join(ProcedureTable).on(_._1.cdcCode === _.procedureCode).result
              } map { planDetailsList =>
            
              planDetailsList.foreach(treatmentPlan=>{
                if(!(treatmentPlan._1._2.isDefined)){
                var planDetail = treatmentPlan._1._1
                var procedure = treatmentPlan._2
                var vFeeSchedule: Double = 0
                var tooth: Long = 0

                if(planDetail.fee != ""){
                vFeeSchedule = f"${planDetail.fee}%.2f".toDouble
                }
                if(planDetail.toothNumber!=""){
                tooth = planDetail.toothNumber.toLong
                }
                
                val newObj = obj + ("id" -> JsNumber(convertOptionLong(planDetail.id))) + ("procedureCode" -> JsString(planDetail.cdcCode)) + ("procedureDescription" -> JsString(procedure.procedureType)) + ("status" -> JsString(planDetail.status)) + ("tooth" -> JsNumber(tooth)) + ("fee" -> JsNumber(vFeeSchedule))
                dataList = dataList :+ newObj
                }
                })
            }
            Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          } 
          val newObj = obj + ("procedure" -> Json.toJson(dataList)) 
          var procedureString = newObj.toString()
          changedProcedure = procedureString.replace( '"','★')

                AppointmentList(
                  Some(entry.id.get),
                  entry.practiceId,
                  entry.date,
                  patientDob,
                  entry.startTime,
                  entry.endTime,
                  medicalCondition,
                  entry.patientId,
                  entry.patientName,
                  entry.operatoryId,
                  operatoryName,
                  entry.providerId,
                  providerTitle,
                  providerName,
                  providerColor,
                  entry.procedureType,
                  entry.procedureCode,
                  entry.anaesthesia,
                  entry.notes,
                  entry.status,
                  entry.amountToBeCollected,
                  entry.reason,
                  entry.referredBy,
                  entry.alerts,
                  entry.insurance,
                  entry.phoneNumber,
                  entry.assistantId,
                  // assistantName,
                  entry.email,
                  // entry.procedure,
                  Some(changedProcedure),
                  entry.creator,
                  entry.reminderSentDate
                )
            }.toList
            Ok(Json.toJson(data))
          }
      }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
 val formatter = DateTimeFormat.forPattern("hh:mm a");
 var operatoryBooked: Boolean = false
 var providerBooked: Boolean = false
 var nonWorkingDate: Boolean = false
 var workingHours: Boolean = false
 var procedureCodeList = new ListBuffer[Long]()
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

    var appointment = for {

      date <- (request.body \ "date").validate[LocalDate]
      startTime <- (request.body \ "startTime").validate[String]
      endTime <- (request.body \ "endTime").validate[String]
      patientId <- (request.body \ "patientId").validate[String]
      operatoryId <- (request.body \ "operatoryId").validate[String]
      providerId <- (request.body \ "providerId").validate[String]
      procedureType <- (request.body \ "procedureType").validateOpt[String]
      procedureCode <- (request.body \ "procedureCode").validateOpt[String]
      anaesthesia <- (request.body \ "anaesthesia").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      status <- (request.body \ "status").validate[String]
      amountToBeCollected <- (request.body \ "amountToBeCollected").validateOpt[String]
      reason <- (request.body \ "reason").validate[String]
      referredBy <- (request.body \ "referredBy").validateOpt[String]
      alerts <- (request.body \ "alerts").validateOpt[String]
      insurance <- (request.body \ "insurance").validateOpt[String]
      phoneNumber <- (request.body \ "phoneNumber").validate[String]
      assistantId <- (request.body \ "assistantId").validateOpt[String]
      email <- (request.body \ "email").validate[String]
      procedure <- (request.body \ "procedure").validateOpt[String]
      creator <- (request.body \ "creator").validate[String]
    }
    yield {

       val patientStatus = scala.util.Try(patientId.toLong).isSuccess
       var patient_ID = 0
       var patient_NAME = ""
       var strAmountToBeCollected: Float = 0

      var strStatus  = ""
      if(status == Some("") || status == None){
          strStatus = "Scheduled"
      } else {
          strStatus = status
      }

      if(amountToBeCollected != Some("") && amountToBeCollected != None){
        strAmountToBeCollected = convertStringToFloat(amountToBeCollected)
      }

      var tempProcedure = convertOptionString(procedure)
      if(patientStatus) {
        patient_ID = patientId.toInt
        if(tempProcedure != null && tempProcedure != ""){
           var temp = tempProcedure.replace('★', '"')
        val jsonObject: JsValue = Json.parse(temp)
        val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

        if(procedureJson != None){
        procedureJson.get.foreach(s => {
        var a = (s \ "id").as[Long]
        procedureCodeList += a
        })
      }
      }

        var block1 = db.run{
              TreatmentTable.filter(_.id === patientId.toLong).result.head
        }map { stepMap =>
              val firstName =  convertOptionString(stepMap.firstName)
              val lastName =  convertOptionString(stepMap.lastName)
              patient_NAME = firstName+" "+lastName
        }
        val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      } else {
        patient_NAME = patientId
      }

      AppointmentRow(None, practiceId, date,LocalTime.parse(startTime, formatter),LocalTime.parse(endTime, formatter),patient_ID,convertStringToLong(Some(operatoryId)),convertStringToLong(Some(providerId)),convertOptionString(procedureType),convertOptionString(procedureCode),convertOptionString(anaesthesia),convertOptionString(notes),strStatus ,strAmountToBeCollected,false,reason,patient_NAME,convertOptionString(referredBy),convertOptionString(alerts),convertOptionString(insurance),phoneNumber,assistantId,Some(email),procedure,Some(creator),None)
    }

    appointment match {
      case JsSuccess(appointmentsRow, _) =>

        var actualApptEndTime = appointmentsRow.endTime
        if (appointmentsRow.endTime.toString == "00:00:00.000"){
          actualApptEndTime = actualApptEndTime.minusMinutes(1)
        }

        val dayDate = DateTimeFormat.forPattern("EEEE").print(appointmentsRow.date)
    val count = db.run{
          for {
            dateLength <- NonWorkingDaysTable.filter(_.practiceId === practiceId)
            .filter(_.date === appointmentsRow.date).length.result

            workingHoursData <- WorkingHoursTable.filter(_.practiceId === practiceId).filter(_.day === dayDate).result

            operatoriesLength <- AppointmentTable.filterNot(_.deleted).filterNot(_.status.toLowerCase === "canceled")
             .filter(_.practiceId === appointmentsRow.practiceId)
             .filter(_.date === appointmentsRow.date)
             .filter(_.operatoryId === appointmentsRow.operatoryId)
             .filter(a => (a.startTime >= appointmentsRow.startTime && a.startTime < actualApptEndTime) ||
             (a.endTime > appointmentsRow.startTime && a.endTime <= actualApptEndTime) ||
             (a.startTime < appointmentsRow.startTime  && a.endTime > actualApptEndTime)).length.result

          } yield {

            if (dateLength>0){
                nonWorkingDate = true
            }
            if (workingHoursData.length>0){
              var practiceStartTime = workingHoursData(0).startTime
              var practiceEndTime = workingHoursData(0).endTime

              if (practiceStartTime!=practiceEndTime){
                 if (practiceEndTime.toString == "00:00:00.000"){
                   practiceEndTime = practiceEndTime.minusMinutes(1)
                 }

                if (!((appointmentsRow.startTime >= practiceStartTime && appointmentsRow.startTime < practiceEndTime) && (appointmentsRow.endTime> practiceStartTime && appointmentsRow.endTime<=practiceEndTime))){
                  workingHours = true
                }
              }
            }

            if (operatoriesLength>0){
                operatoryBooked = true
            }
  }
}
var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

     if(actualApptEndTime <= appointmentsRow.startTime){
         val obj = Json.obj()
         val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("End time should be greater than start time!"))
         Future(PreconditionFailed{Json.toJson(Array(newObj))})
    } else if(workingHours){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Practice is closed on selected time. Please choose another time slot!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else if(actualApptEndTime <= appointmentsRow.startTime){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("End time should be greater than start time!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }else if( operatoryBooked ) {
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Operatory not available for the selected date and time!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }else{

        db.run{
          AppointmentTable.returning(AppointmentTable.map(_.id)) += appointmentsRow
          } map {
              case 0L => PreconditionFailed
              case id =>  db.run{
                 AppointmentTable.sortBy(_.id.desc).result.head
              } map { appointment =>
                    val appointmentId = convertOptionLong(appointment.id)
                    val practiceId = appointment.practiceId
                    val date = appointment.date
                    val startTime = appointment.startTime
                    val endTime = appointment.endTime
                    val patientId = appointment.patientId
                    val operatoryId = appointment.operatoryId
                    val providerId = appointment.providerId
                    val procedureType = appointment.procedureType
                    val procedureCode = appointment.procedureCode
                    val anaesthesia = appointment.anaesthesia
                    val notes = appointment.notes
                    val status = appointment.status
                    val operation = "insert"
                    val updatedBy = convertOptionLong(request.identity.id)
                    val updatedAt = new DateTime()
                    val amountToBeCollected = appointment.amountToBeCollected
                    val reason = appointment.reason
                    val patientName = appointment.patientName
                    val referredBy = appointment.referredBy
                    val alerts = appointment.alerts
                    val insurance = appointment.insurance
                    val phoneNumber = appointment.phoneNumber
                    val assistantId = appointment.assistantId
                    val email = appointment.email
                    val procedure = appointment.procedure
                    val creator = appointment.creator

                  var appointmentLogRow = AppointmentLogRow(None,appointmentId, practiceId,date,startTime,
                  endTime,patientId,operatoryId,providerId,procedureType,procedureCode,anaesthesia,
                  notes,status,operation,updatedBy,updatedAt,amountToBeCollected,reason,patientName,referredBy,alerts,insurance,phoneNumber,assistantId,email,procedure,creator)

                 db.run{
                  AppointmentLogTable.returning(AppointmentLogTable.map(_.id)) += appointmentLogRow
                  } map {
                        case 0L => PreconditionFailed
                        case id => Ok
                  }
                  if(procedureCodeList.size>0){
                  for(i<-0 to procedureCodeList.size-1){
                    var appointmentTreatmentPlanRow = AppointmentTreatmentPlanRow(None,appointmentId, procedureCodeList(i))
                    db.run{
                  AppointmentTreatmentPlanTable.returning(AppointmentTreatmentPlanTable.map(_.id)) += appointmentTreatmentPlanRow
                  } map {
                        case 0L => PreconditionFailed
                        case id => Ok
                  }
                  }
                }
                }
                Ok{ Json.toJson(appointmentsRow.copy(id = Some(id))) }
          }
    }
      case JsError(_) => Future.successful(BadRequest)
  }
}

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
 val formatter = DateTimeFormat.forPattern("hh:mm a");
 var operatoryBooked: Boolean = false
 var providerBooked: Boolean = false
 var nonWorkingDate: Boolean = false
 var workingHours: Boolean = false
 var statusCompleted: Boolean = false
 var appointmentsFound: Boolean = false
 var skipEndtimeValidation : Boolean = false
 val currentDate: LocalDate = new LocalDate()
 val currentTime: LocalTime = new LocalTime()
 var practiceId: Option[Long]= Some(request.identity.asInstanceOf[StaffRow].practiceId)
 var strPatientId: Long = 0
 var patientName = ""
 var procedureCodeList = new ListBuffer[Long]()

    val appointment = for {

      date <- (request.body \ "date").validateOpt[LocalDate]
      startTime <- (request.body \ "startTime").validateOpt[String]
      endTime <- (request.body \ "endTime").validateOpt[String]
      patientId <- (request.body \ "patientId").validateOpt[String]
      operatoryId <- (request.body \ "operatoryId").validateOpt[String]
      providerId <- (request.body \ "providerId").validateOpt[String]
      procedureType <- (request.body \ "procedureType").validateOpt[String]
      procedureCode <- (request.body \ "procedureCode").validateOpt[String]
      anaesthesia <- (request.body \ "anaesthesia").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      status <- (request.body \ "status").validateOpt[String]
      amountToBeCollected <- (request.body \ "amountToBeCollected").validateOpt[String]
      reason <- (request.body \ "reason").validateOpt[String]
      referredBy <- (request.body \ "referredBy").validateOpt[String]
      alerts <- (request.body \ "alerts").validateOpt[String]
      insurance <- (request.body \ "insurance").validateOpt[String]
      phoneNumber <- (request.body \ "phoneNumber").validateOpt[String]
      assistantId <- (request.body \ "assistantId").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      procedure <- (request.body \ "procedure").validateOpt[String]
      creator <- (request.body \ "creator").validateOpt[String]
    }
    yield {
      val patientStatus = scala.util.Try(convertOptionString(patientId).toLong).isSuccess
      var tempProcedure = convertOptionString(procedure)
      if(patientStatus) {
        strPatientId = convertOptionString(patientId).toInt
        if(convertOptionString(status).toLowerCase != "canceled"){
        if(tempProcedure != null && tempProcedure != ""){
           var temp = tempProcedure.replace('★', '"')
        val jsonObject: JsValue = Json.parse(temp)
        val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

        if(procedureJson != None){
        procedureJson.get.foreach(s => {
        var a = (s \ "id").as[Long]
        procedureCodeList += a
        })
      }
      }
      }

        var block1 = db.run{
              TreatmentTable.filter(_.id === convertOptionString(patientId).toLong).result.head
        }map { stepMap =>
              val firstName =  convertOptionString(stepMap.firstName)
              val lastName =  convertOptionString(stepMap.lastName)
              patientName = firstName+" "+lastName
        }
        val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      } else {
        patientName = convertOptionString(patientId)
      }

      (practiceId, date,LocalTime.parse(convertOptionString(startTime), formatter),LocalTime.parse(convertOptionString(endTime), formatter),patientId,convertStringToLong(operatoryId),convertStringToLong(providerId),procedureType,procedureCode,anaesthesia,notes,status,convertStringToFloat(amountToBeCollected),reason,"",referredBy,alerts,insurance,phoneNumber,assistantId,email, AppointmentRow(None, convertOptionLong(practiceId), convertOptionalLocalDate(date),LocalTime.parse(convertOptionString(startTime), formatter),LocalTime.parse(convertOptionString(endTime), formatter),strPatientId,convertStringToLong(operatoryId),convertStringToLong(providerId),convertOptionString(procedureType),convertOptionString(procedureCode),convertOptionString(anaesthesia),convertOptionString(notes),convertOptionString(status) ,convertStringToFloat(amountToBeCollected),false,convertOptionString(reason),patientName,convertOptionString(referredBy),convertOptionString(alerts),convertOptionString(insurance),convertOptionString(phoneNumber),assistantId,email,procedure,creator,None))
    } 
    appointment match {
      case JsSuccess((practiceId, date,startTime,endTime,patientId,operatoryId,providerId,procedureType,procedureCode,anaesthesia,notes,status,amountToBeCollected,reason,"",referredBy,alerts,insurance,phoneNumber,assistantId,email,appointmentRows), _) =>

      var actualApptEndTime = endTime
      if (endTime.toString == "00:00:00.000"){
        actualApptEndTime = actualApptEndTime.minusMinutes(1)
      }


      val dayDate = DateTimeFormat.forPattern("EEEE").print(convertOptionalLocalDate(date))
      val count = db.run{
            for {

             appointmentLength <-  AppointmentTable.filterNot(_.deleted).filter(_.id === id).length.result

             dateLength <- NonWorkingDaysTable.filter(_.practiceId === practiceId)
             .filter(_.date === date).length.result

             workingHoursData <- WorkingHoursTable.filter(_.practiceId === practiceId).filter(_.day === dayDate).result

          	 operatoriesLength <- AppointmentTable.filterNot(_.deleted).filterNot(_.status.toLowerCase === "canceled")
             .filter(_.practiceId === practiceId)
             .filter(_.date === date)
             .filterNot(_.id === id).filter(_.operatoryId === operatoryId)
             .filter(a => (a.startTime >= startTime && a.startTime < actualApptEndTime) ||
             (a.endTime > startTime && a.endTime <= actualApptEndTime) ||
             (a.startTime < startTime  && a.endTime > actualApptEndTime)).length.result
          } yield {

            if(appointmentLength <= 0){
                appointmentsFound = true
            }

            if (dateLength>0){
                nonWorkingDate = true
            }

            if (workingHoursData.length>0){
              var practiceStartTime = workingHoursData(0).startTime
              var practiceEndTime = workingHoursData(0).endTime

              if (practiceStartTime!=practiceEndTime){
                 if (practiceEndTime.toString == "00:00:00.000"){
                   practiceEndTime = practiceEndTime.minusMinutes(1)
                 }

                if (!((startTime >= practiceStartTime && startTime < practiceEndTime) && (actualApptEndTime> practiceStartTime && actualApptEndTime<=practiceEndTime))){
                  workingHours = true
                }
              }
            }

            if (operatoriesLength>0){
              operatoryBooked = true
            }

      if(endTime.toString == "00:00:00.000"){
        skipEndtimeValidation = true
      }
   }
}
  var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(appointmentsFound){
      val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Appointment not found!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else if(endTime <= startTime && !skipEndtimeValidation){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("End time should be greater than start time!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    } else if(workingHours){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Practice is closed on selected time. Please choose another time!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }else if(operatoryBooked) {
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Operatory not available for the selected date and time!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }else{
      db.run {
        val query = AppointmentTable.filterNot(_.deleted).filter(_.id === id)

        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            practiceId.map(value => query.map(_.practiceId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            date.map(value => query.map(_.date).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            query.map(_.startTime).update(startTime),
            query.map(_.endTime).update(endTime),
            Some(operatoryId).map(value => query.map(_.operatoryId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerId).map(value => query.map(_.providerId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            procedureType.map(value => query.map(_.procedureType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            procedureCode.map(value => query.map(_.procedureCode).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            anaesthesia.map(value => query.map(_.anaesthesia).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            notes.map(value => query.map(_.notes).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(amountToBeCollected).map(value => query.map(_.amountToBeCollected).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            reason.map(value => query.map(_.reason).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            referredBy.map(value => query.map(_.referredBy).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            alerts.map(value => query.map(_.alerts).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            insurance.map(value => query.map(_.insurance).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            phoneNumber.map(value => query.map(_.phoneNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(assistantId).map(value => query.map(_.assistantId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(email).map(value => query.map(_.email).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(appointmentRows.procedure).map(value => query.map(_.procedure).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(appointmentRows.creator).map(value => query.map(_.creator).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(appointmentRows.patientId).map(value => query.map(_.patientId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(appointmentRows.patientName).map(value => query.map(_.patientName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => 

              db.run{
                 AppointmentTable.filterNot(_.deleted).filter(_.id === id).result
              } map { appointmentData => 
                    appointmentData.map{
                      appointment => 
                      val appointmentId = convertOptionLong(appointment.id)
                      val practiceId = appointment.practiceId
                      val date = appointment.date
                      val startTime = appointment.startTime
                      val endTime = appointment.endTime
                      val patientId = appointment.patientId
                      val operatoryId = appointment.operatoryId
                      val providerId = appointment.providerId
                      val procedureType = appointment.procedureType
                      val procedureCode = appointment.procedureCode
                      val anaesthesia = appointment.anaesthesia
                      val notes = appointment.notes
                      val status = appointment.status
                      val operation = "update"
                      val updatedBy = convertOptionLong(request.identity.id)
                      val updatedAt = new DateTime()
                      val amountToBeCollected = appointment.amountToBeCollected
                      val reason= appointment.reason
                      val patientName = appointment.patientName
                      val referredBy = appointment.referredBy
                      val alerts = appointment.alerts
                      val insurance = appointment.insurance
                      val phoneNumber = appointment.phoneNumber
                      val assistantId = appointment.assistantId
                      val email = appointment.email
                      val procedure = appointment.procedure
                      val creator = appointment.creator

                        var appointmentLogRow = AppointmentLogRow(None,appointmentId, practiceId,date,startTime,
                        endTime,patientId,operatoryId,providerId,procedureType,procedureCode,anaesthesia,
                        notes,status,operation,updatedBy,updatedAt,amountToBeCollected,reason,patientName,referredBy,alerts,insurance,phoneNumber,assistantId,email,procedure,creator)

                        db.run{
                          AppointmentLogTable.filter(_.id === appointmentLogRow.id).exists.result.flatMap[Result, NoStream, Effect.Write] {
                            case true => DBIO.successful(PreconditionFailed)
                            case false => (AppointmentLogTable.returning(AppointmentLogTable.map(_.id)) += appointmentLogRow) map {
                                case 0L => PreconditionFailed
                                case id => Ok
                            }
                          }
                        }
                        db.run{
                    for{
                      treatmentPlan <- AppointmentTreatmentPlanTable.filter(_.appointmentId === appointmentId).result
                    }yield{
                       if(procedureCodeList.size >= treatmentPlan.length){
                          for(i<-0 to procedureCodeList.size-1){
                              if(i < treatmentPlan.length){
                                var block =  db.run {
                                var query = AppointmentTreatmentPlanTable.filter(_.id === treatmentPlan(i).id)
                                query.map(_.treatmentPlanId).update(procedureCodeList(i))
                                }map{
                                  case 0 => NotFound
                                  case _ => Ok
                                }
                                var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
                              }
                              else{
                                var appointmentTreatmentPlanRow = AppointmentTreatmentPlanRow(None,appointmentId, procedureCodeList(i))
                                db.run{
                                AppointmentTreatmentPlanTable.returning(AppointmentTreatmentPlanTable.map(_.id)) += appointmentTreatmentPlanRow
                                } map {
                                      case 0L => PreconditionFailed
                                      case id => Ok
                                }
                              }
                          }
                  } else if(procedureCodeList.size < treatmentPlan.length){
                    for(i<-0 to treatmentPlan.length-1){
                      
                      if(i < procedureCodeList.size){
                        var block1 =  db.run {
                                var query = AppointmentTreatmentPlanTable.filter(_.id === treatmentPlan(i).id)
                                query.map(_.treatmentPlanId).update(procedureCodeList(i))
                                }map{
                                  case 0 => NotFound
                                  case _ => Ok
                                }
                                var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
                      }
                      else{
                          db.run{
                          AppointmentTreatmentPlanTable.filter(_.id === treatmentPlan(i).id).delete map {
                            case 0 => NotFound
                            case _ => Ok
                            }
                          }
                      }
                    }
                  }
                  }
                  }
                    }  
                }
           
              val obj = Json.obj()
              val newObj = obj + ("Result" -> JsString("Passed")) + ("message" -> JsString("Appointment updated successfully"))
              Ok{ Json.toJson(Array(newObj)) }}
          case false => DBIO.successful(NotFound)
        }
    }
    }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }


 def getProviders = silhouette.SecuredAction.async { request =>
  var selectedProviders = ""
  var providersResult :List[Long] = List.empty[Long]
  var providersList: List[Long] = List.empty[Long]

    var count = db.run {
      StaffTable.filter(_.id === request.identity.id.get).map(_.providersFilter).result
    } map { data => data.map{ finalData =>
    providersList = convertOptionString(finalData).split(",").map(_.trim.toLong).toList }
    }
    var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

       db.run{
        ProviderTable.filter(_.id inSet providersList).sortBy(_.id).result
        } map { result =>  result.map{provider =>
        providersResult =  providersResult :+ convertOptionLong(provider.id)
        selectedProviders = providersResult.mkString(",")
    }
        val obj = Json.obj()
        val newObj = obj + ("providersFiltered" -> JsString(selectedProviders))
        Ok{Json.toJson(Array(newObj))}
      }
}

 def updateProviders = silhouette.SecuredAction.async(parse.json) { request =>
    val providersList = for {
      providers <- (request.body \ "providers").validate[String]
    }
    yield {
        (providers)
    }
    providersList match {
      case JsSuccess((providers), _) => db.run {
        val query = StaffTable.filter(_.id === request.identity.id.get)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            query.map(_.providersFilter).update(Some(providers))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

implicit val OperatoriesRowWrites = Json.writes[OperatorTableRow]

def availableOperatories(date: Option[String],startTime: Option[String],endTime: Option[String]) = silhouette.SecuredAction.async { request =>
  val formatter = DateTimeFormat.forPattern("hh:mm a");
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var operatoriesList = List.empty[Long]

 var operatoryIdList = db.run{
              for {

                  operatoryData <- AppointmentTable.filterNot(_.deleted).filterNot(_.status.toLowerCase === "canceled")
                  .filter(_.practiceId === practiceId)
                  .filter(_.date === LocalDate.parse(convertOptionString(date)))
                  .filter(a => (a.startTime >= LocalTime.parse(convertOptionString(startTime), formatter)
                  && a.startTime < LocalTime.parse(convertOptionString(endTime), formatter)) ||(a.endTime > LocalTime.parse(convertOptionString(startTime), formatter) && a.endTime <= LocalTime.parse(convertOptionString(endTime), formatter))).result
              } yield{
                  operatoryData.map{operator=> operatoriesList =  operatoriesList :+ operator.operatoryId}
                  }
            }
                 val AwaitResult = Await.ready(operatoryIdList, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          db.run {
                OperatorTable.filterNot(_.deleted).filter(_.practiceId === request.identity.asInstanceOf[StaffRow].practiceId)
                      .filterNot(_.id inSet operatoriesList).sortBy(_.id).result
                } map { Operatories =>
                  Ok(Json.toJson(Operatories))
                }
}


  def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
        AppointmentTable.filterNot(_.deleted).filter(_.id === id).result
    } map { appointmentData => 
          appointmentData.map {
            appointment => 
                val appointmentId = convertOptionLong(appointment.id)
                val practiceId = appointment.practiceId
                val date = appointment.date
                val startTime = appointment.startTime
                val endTime = appointment.endTime
                val patientId = appointment.patientId
                val operatoryId = appointment.operatoryId
                val providerId = appointment.providerId
                val procedureType = appointment.procedureType
                val procedureCode = appointment.procedureCode
                val anaesthesia = appointment.anaesthesia
                val notes = appointment.notes
                val status = appointment.status
                val operation = "delete"
                val updatedBy = convertOptionLong(request.identity.id)
                val updatedAt = new DateTime()
                val amountToBeCollected = appointment.amountToBeCollected
                val reason = appointment.reason
                val patientName = appointment.patientName
                val referredBy = appointment.referredBy
                val alerts = appointment.alerts
                val insurance = appointment.insurance
                val phoneNumber = appointment.phoneNumber
                val assistantId = appointment.assistantId
                val email = appointment.email
                val procedure = appointment.procedure
                val creator = appointment.creator

              var appointmentLogRow = AppointmentLogRow(None,appointmentId, practiceId,date,startTime,
              endTime,patientId,operatoryId,providerId,procedureType,procedureCode,anaesthesia,
              notes,status,operation,updatedBy,updatedAt,amountToBeCollected,reason,patientName,referredBy,alerts,insurance,phoneNumber,assistantId,email,procedure,creator)

              db.run{
                AppointmentLogTable.filter(_.id === appointmentLogRow.id).exists.result.flatMap[Result, NoStream, Effect.Write] {
                  case true => DBIO.successful(PreconditionFailed)
                  case false => (AppointmentLogTable.returning(AppointmentLogTable.map(_.id)) += appointmentLogRow) map {
                      case 0L => PreconditionFailed
                      case id => Ok
                  }
                }
              }
          }  
      }

      db.run {
          AppointmentTable.filter(_.id === id).map(_.deleted).update(true)
      } map {
          case 0 => NotFound
          case _ => Ok
        }
  }

  def generatePdf(fromDate: Option[String],toDate: Option[String],provider: Option[String]) =silhouette.SecuredAction.async { request =>
  var appointmentResult = ""
  var providerResult:Long = 0
  var providerName = ""
  var providerId = convertOptionString(provider)
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerQuery = ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

  var providerIdMap:scala.collection.mutable.Map[Long,Long]=scala.collection.mutable.Map()
  var providerNameMap:scala.collection.mutable.Map[Long,String]=scala.collection.mutable.Map()

      if(providerId.toLowerCase != "" && providerId.toLowerCase != "all") {
         providerQuery = providerQuery.filter(_.id === convertStringToLong(Some(providerId)))
      }

      db.run{
      providerQuery.result
      } map { provider =>

      (provider).foreach(row=> {
      providerIdMap.update(convertOptionLong(row.id),convertOptionLong(row.id))
      providerNameMap.update(convertOptionLong(row.id) , row.title +" "+ row.firstName +" "+ row.lastName) })
      Ok

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {

          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL


          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);

          var cell = new PdfPCell(new Phrase("Appointments", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));


          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

        var cell1 = new PdfPCell

        var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
        var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

        if(fromDate != toDate){
          cell1 = new PdfPCell(new Phrase("From Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);

          var cell2 = new PdfPCell(new Phrase("To Date     : "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_TITLE1));
          cell2.setBorder(Rectangle.NO_BORDER);
          cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell2);
        } else {
          cell1 = new PdfPCell(new Phrase("Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);
        }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var noRecordsFound: Boolean = true
          providerIdMap.foreach {
            case (key,providerValue) =>

    val providerAwait =  db.run {
         AppointmentTable.filter(_.practiceId === practiceId).filterNot(_.deleted).filter(_.providerId === providerValue)
        .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
        .filterNot(_.status.toLowerCase === "no show")
        .filterNot(_.status.toLowerCase === "canceled")
        .join(ProviderTable).on(_.providerId === _.id)
        .join(OperatorTable).on(_._1.operatoryId === _.id)
        .result
      } map { appointment => appointment.map{ result=>  appointmentResult = result.toString }

         if(appointmentResult != null && appointmentResult != ""){
           noRecordsFound = false
             val data = appointment.groupBy(_._1._1.id).map {
              case (id,entries) =>
                val entry = entries.head._1._1

                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                providerName = providerTitle + " " + providerFirstName + " " + providerLastName

                val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
                val outputTimeFormat = new SimpleDateFormat("hh:mm a")
                val inputStartTime = inputTimeFormat.parse(entry.startTime.toString)
                val inputEndTime = inputTimeFormat.parse(entry.endTime.toString)
                val outputStartTime = outputTimeFormat.format(inputStartTime)
                val outputEndTime = outputTimeFormat.format(inputEndTime)

                var procedureCode = ""
                var procedureType = ""

              var tempProcedure = convertOptionString(entry.procedure)
              if(tempProcedure != null && tempProcedure != ""){
                var temp = tempProcedure.replace('★', '"')
                val jsonObject: JsValue = Json.parse(temp)
                val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

                if(procedureJson != None){
                procedureJson.get.foreach(s => {
                var a = (s \ "procedureCode").asOpt[String]
                var c = (s \ "toothNumber").asOpt[String]
                var toothNumber = ""
                if(convertOptionString(c) != ""){
                  toothNumber = s" (T-${c.get})"
                }
                if(convertOptionString(a) != ""){
                  procedureCode = procedureCode + a.get + toothNumber + ", "
                }
                var b = (s \ "procedureDescription").asOpt[String]
                procedureType = procedureType + b.get + ", "})
                }
              }

                AppointmentPDF(
                  dateFormatter.print(entry.date),
                  outputStartTime,
                  outputEndTime,
                  entry.patientName,
                  providerName,
                  operatoryName,
                  procedureType.dropRight(2),
                  procedureCode.dropRight(2),
                  entry.reason,
                  convertOptionString(entry.email),
                  entry.phoneNumber
                 )
              }.toList.sortBy(r => (r.date,r.startTime))

          var rowsPerPage = 8;
          var recNum =  0;

          appointmentResult = ""
          var headerTextTable2: PdfPTable = new PdfPTable(1)
          headerTextTable2.setWidthPercentage(100);
          var cell1 = new PdfPCell(new Phrase("Provider Name : " + providerName, FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell1.setPaddingBottom(15)
          headerTextTable2.addCell(cell1)
          document.add(headerTextTable2);


         (data).foreach(row => {

            var sampleData = List(s"${row.date}",s"${row.startTime}", s"${row.endTime}", s"${row.patientName}", s"${row.phoneNumber}", s"${row.email}", s"${row.operatoryName}",s"${row.procedureCode}",s"${row.reason}")

            var columnWidths:Array[Float] = Array(11,11,11,11,12,11,11,11,11);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Start Time", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("End Time", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Operatory Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Procedure Code", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Reason", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                //cell.setPaddingLeft(5)
                cell.setPadding(13);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_LEFT);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
            })

            document.add(table);

            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 10
            }
          })
          document.add(new Paragraph("\n"));
        }
      }
        var AwaitResult = Await.ready(providerAwait, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      }

      if(noRecordsFound){
            var columnWidths:Array[Float] = Array(11,11,11,11,12,11,11,11,11);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Start Time", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("End Time", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Operatory Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Procedure Code", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Reason", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

            document.add(table);

          document.add(new Paragraph("\n"));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(97);
          var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);
      }

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"appointments"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )

      }
  }


def generateCancelReportPdf(fromDate: Option[String],toDate: Option[String])=silhouette.SecuredAction.async { request =>

  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerName = ""
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

    db.run {

      AppointmentTable.filter(_.practiceId === practiceId)
        .filter(s=> s.status.toLowerCase === "no show" || s.status.toLowerCase === "canceled")
        .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
        .join(ProviderTable).on(_.providerId === _.id)
        .result
    } map { appointment =>

      val data = appointment.groupBy(_._1.id).map {
        case (id, items) => {
          val entry = items.head._1
          val providerTitle = items.head._2.title
          val providerFirstName = items.head._2.firstName
          val providerLastName = items.head._2.lastName
          providerName = providerTitle + " " + providerFirstName + " " + providerLastName

            CancelReport(
              dateFormatter.print(entry.date),
              entry.patientName,
              entry.phoneNumber,
              providerName,
              entry.referredBy,
              entry.reason,
              entry.status,
              convertOptionString(entry.email)
            )
          }
        }.toList.sortBy(_.date)

      val sample = data

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


          var cell = new PdfPCell(new Phrase("Canceled / No Show appointments", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var cell1 = new PdfPCell

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
          var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

          if(fromDate != toDate){
            cell1 = new PdfPCell(new Phrase("From Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);

            var cell2 = new PdfPCell(new Phrase("To Date     : "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_TITLE1));
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell2);
          } else {
            cell1 = new PdfPCell(new Phrase("Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;

          if(sample.length != 0){
            (sample).foreach(row => {
              var sampleData = List(s"${row.date}", s"${row.patientName}", s"${row.phoneNumber}", s"${row.email}", s"${row.providerName}", s"${row.referredBy}", s"${row.reason}", s"${row.status}")
              var i=1

              var columnWidths:Array[Float] = Array(12,13,13,13,13,12,12,12);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Reason", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {
                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                cell.setPaddingLeft(5);
                cell.setPaddingTop(16);
                cell.setPaddingBottom(16);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
              })

              document.add(table);
              recNum = recNum+1;
              if(recNum == 0 ) {
                document.newPage();
              }

            })
          } else {
          var columnWidths:Array[Float] = Array(12,13,13,13,13,12,12,12);
          var table: PdfPTable = new PdfPTable(columnWidths);
          table.setWidthPercentage(100);
          if(recNum == 0 ) {

            document.add(new Paragraph("\n"));

            cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Provider Name", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Reason", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            document.add(table);

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);

          }
        }
          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"cancelReport"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }


def generateCancelReportCsv(fromDate: Option[String],toDate: Option[String])=silhouette.SecuredAction { request =>


  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerName: String = ""
  var date: String = ""
  var patientName: String = ""
  var phoneNumber: String = ""
  var referredBy: String = ""
  var reason: String = ""
  var status: String = ""
  var csvList = List.empty[CancelReport]

  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

  var cancelReportList = db.run {

    AppointmentTable.filter(_.practiceId === practiceId)
      .filter(s=> s.status.toLowerCase === "no show" || s.status.toLowerCase === "canceled")
      .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
      .join(ProviderTable).on(_.providerId === _.id)
      .result
  } map { appointment =>
      val data = appointment.groupBy(_._1.id).map {
        case (id, items) => {
          val entry = items.head._1
          val providerTitle = items.head._2.title
          val providerFirstName = items.head._2.firstName
          val providerLastName = items.head._2.lastName
          providerName = providerTitle + " " + providerFirstName + " " + providerLastName

          var date = dateFormatter.print(entry.date)
          var patientName = entry.patientName
          var phoneNumber = entry.phoneNumber
          var referredBy = entry.referredBy
          var reason = entry.reason
          var status = entry.status
          var email = entry.email

          var dataList = CancelReport(date, patientName, phoneNumber, providerName, referredBy, reason, status, convertOptionString(email))
          csvList = csvList :+ dataList

        }
      }
  }

  val AwaitResult = Await.ready(cancelReportList, atMost = scala.concurrent.duration.Duration(120, SECONDS))

  val file = new java.io.File("/tmp/cancelReport.csv")
  val writer = CSVWriter.open(file)
  writer.writeRow(List("Date", "Patient Name", "Phone Number", "Email", "Provider", "Referred By", "Reason", "Status"))
  csvList.sortBy(_.date).map {
    cancelList => writer.writeRow(List(cancelList.date, cancelList.patientName, cancelList.phoneNumber, cancelList.email, cancelList.providerName, cancelList.referredBy, cancelList.reason, cancelList.status))
  }
  writer.close()
  Ok.sendFile(file, inline = false, _ => file.getName)

}



def checkAppointmentSetup = silhouette.SecuredAction.async { request =>
  var appointmentSetUp : Boolean = false
  var operatories: Long = 0
  var providers: Long = 0
  var workingHours: Long = 0
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
        val count = db.run{
              for {
                operatoriesLength <- OperatorTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result
                providersLength <- ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result
                workingHoursLength <- WorkingHoursTable.filter(_.practiceId === practiceId).length.result
              } yield { (operatories = operatoriesLength, providers = providersLength,  workingHours = workingHoursLength) }
            }

              var AwaitResult = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

              if(operatories > 0 && providers > 0 && workingHours > 0){
                  appointmentSetUp = true
        }

        val obj = Json.obj()
        val newObj = obj + ("appointmentSetUp" -> JsBoolean(appointmentSetUp))
        Future(Ok{Json.toJson(newObj)})

}

  def roleReadAll = silhouette.SecuredAction.async { request =>
      db.run {
          RoleTable.result
      } map { roleList =>
      Ok(Json.toJson(roleList))
      }
  }

   def dailyBrief(date: Option[String]) = silhouette.SecuredAction.async { request =>
         var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

          db.run{
            AppointmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId).filter(_.date === LocalDate.parse(convertOptionString(date)))
            .filterNot(_.status.toLowerCase === "no show").filterNot(_.status.toLowerCase === "canceled")
            .join(ProviderTable).on(_.providerId === _.id)
            .join(OperatorTable).on(_._1.operatoryId === _.id)
            .result
          }map { resultMap =>

                val data = resultMap.groupBy(_._1._1.id).map {
                case (appointmentId, entries) =>
                val appointment = entries.head._1._1
                val provider = entries.head._1._2
                var patientPhoto = ""
                var strPatientPhoto = ""
                var insurance = ""

                if(appointment.patientId != 0){
                  var block1 = db.run{
                    StepTable.filter(_.treatmentId === appointment.patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
                  } map { stepMap =>
                    patientPhoto =  (stepMap.value \ "patient_photo").asOpt[String].getOrElse("")

                    if(patientPhoto != "") {
                      var temp = patientPhoto.replace('★', '"')
                      val jsonObject: JsValue = Json.parse(temp)
                      val photo  =  (jsonObject \ "images").as[JsValue]
                      if(photo.as[JsArray].value.size > 0){
                        strPatientPhoto = photo(0).as[String]
                        if(strPatientPhoto.endsWith(".pdf") == true){
                          strPatientPhoto = ""
                        }
                      }
                    }
                  }
                  val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(45, SECONDS))

                  if(patientPhoto == "" || strPatientPhoto == "") {
                    var block2 = db.run {
                      StepTable.filter(_.treatmentId === appointment.patientId).filter(_.formId === "1H").sortBy(_.dateCreated.desc).result.head
                    } map { stepMap =>
                      patientPhoto = (stepMap.value \ "1b1").asOpt[String].getOrElse("")

                      if (patientPhoto != "") {
                        var temp = patientPhoto.replace('★', '"')
                        val jsonObject: JsValue = Json.parse(temp)
                        val photo = (jsonObject \ "images").as[JsValue]
                        if (photo.as[JsArray].value.size > 0) {
                          strPatientPhoto = photo(0).as[String]
                          if(strPatientPhoto.endsWith(".pdf") == true){
                            strPatientPhoto = ""
                          }
                        }
                      }
                    }
                    val AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(45, SECONDS))
                  }

                // if(appointment.insurance != "-1" && appointment.insurance != "0" && appointment.insurance != ""){
                //   var insuranceId = (appointment.insurance).toLong
                //   var block3 = db.run{
                //     DentalCarrierTable.filterNot(_.archived).filter(_.practiceId === practiceId).filter(_.id === insuranceId).result
                //   } map { dentalCarrier =>
                //       dentalCarrier.map{ s => insurance = s.name}
                //   }
                //   Await.ready(block3, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                // } else {
                //     if(appointment.insurance == "-1"){
                //       insurance = "UCR"
                //     } else {
                //       insurance = "Private"
                //     }
                // }
              }

             AppointmentDailyBrief(
                 convertOptionLong(appointment.id),
                 appointment.date,
                 appointment.startTime,
                 appointment.endTime,
                 provider.title + " " + provider.firstName +" "+provider.lastName,
                 appointment.patientName,
                 strPatientPhoto,
                 appointment.phoneNumber,
                 appointment.reason,
                 appointment.referredBy,
                 appointment.alerts
                )
          }.toList
        Ok(Json.toJson(data))
          }
        }


def dailyBriefPdf(date: Option[String])=silhouette.SecuredAction.async { request =>

    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val imgLogo =  Image.getInstance("https://s3.amazonaws.com/ds-static.spfr.co/img/placeholder.jpg");
    val datePattern = "\\d{4}-\\d{2}-\\d{2}"
    val convertedDate = convertOptionString(date)
    val isDate1 = convertedDate.matches(datePattern)

    if(isDate1 == true){

      db.run{
        AppointmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId).filter(_.date === LocalDate.parse(convertOptionString(date)))
          .filterNot(_.status.toLowerCase === "no show").filterNot(_.status.toLowerCase === "canceled")
          .join(ProviderTable).on(_.providerId === _.id)
          .join(OperatorTable).on(_._1.operatoryId === _.id)
          .result
      } map { resultMap =>

        val data = resultMap.groupBy(_._1._1.id).map {
          case (appointmentId, entries) =>
            val appointment = entries.head._1._1
            val provider = entries.head._1._2
            var patientPhoto = ""
            var StrPatientPhoto = ""
            var insurance = ""
            if (appointment.patientId != 0) {
              var block1 = db.run{
                StepTable.filter(_.treatmentId === appointment.patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepMap =>
                patientPhoto =  (stepMap.value \ "patient_photo").asOpt[String].getOrElse("")

                if(patientPhoto != "") {
                  var temp = patientPhoto.replace('★', '"')
                  val jsonObject: JsValue = Json.parse(temp)
                  val photo  =  (jsonObject \ "images").as[JsValue]
                  if(photo.as[JsArray].value.size > 0){
                    StrPatientPhoto = photo(0).as[String]
                    if(StrPatientPhoto.endsWith(".pdf") == true){
                      StrPatientPhoto = ""
                    }
                  }
                }
              }
              val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(45, SECONDS))

              if(patientPhoto == "" || StrPatientPhoto == "") {
                var block2 = db.run {
                  StepTable.filter(_.treatmentId === appointment.patientId).filter(_.formId === "1H").sortBy(_.dateCreated.desc).result.head
                } map { stepMap =>
                  patientPhoto = (stepMap.value \ "1b1").asOpt[String].getOrElse("")

                  if (patientPhoto != "") {
                    var temp = patientPhoto.replace('★', '"')
                    val jsonObject: JsValue = Json.parse(temp)
                    val photo = (jsonObject \ "images").as[JsValue]
                    if (photo.as[JsArray].value.size > 0) {
                      StrPatientPhoto = photo(0).as[String]
                      if(StrPatientPhoto.endsWith(".pdf") == true){
                        StrPatientPhoto = ""
                      }
                    }
                  }
                }
                val AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(45, SECONDS))
              }

              // if(appointment.insurance != "-1" && appointment.insurance != "0" && appointment.insurance != ""){
              //     var insuranceId = (appointment.insurance).toLong
              //     var block3 = db.run{
              //       DentalCarrierTable.filterNot(_.archived).filter(_.practiceId === practiceId).filter(_.id === insuranceId).result
              //     } map { dentalCarrier =>
              //         dentalCarrier.map{ s => insurance = s.name}
              //     }
              //     Await.ready(block3, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              //   } else {
              //       if(appointment.insurance == "-1"){
              //         insurance = "UCR"
              //       } else {
              //         insurance = "Private"
              //       }
              //   }
            }

            var patientName: String = appointment.patientName
            var phoneNumber = appointment.phoneNumber
            var startTime = appointment.startTime
            var endTime = appointment.endTime
            var providerName = provider.title + " " + provider.firstName + " " + provider.lastName
            var reason = appointment.reason
            var referredBy = appointment.referredBy
            // var insurance = appointment.insurance
            var alerts = appointment.alerts
            patientPhoto = StrPatientPhoto

            AppointmentDailyBriefPdf(
              patientName,
              phoneNumber,
              startTime,
              endTime,
              providerName,
              reason,
              referredBy,
              alerts,
              patientPhoto
            )
        }.toList

        val sample = data

        val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {
            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }

          }


          class BorderEvent extends PdfPTableEvent {
            override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
              val width = widths(0)
              val x1 = width(0)
              val x2 = width(width.length - 1)
              val y1 = heights(0)
              val y2 = heights(heights.length - 1)
              val cb = canvases(PdfPTable.LINECANVAS)
              cb.rectangle(x1, y1, x2 - x1, y2 - y1)
              cb.setLineWidth(0.4)
              cb.stroke()
              cb.resetRGBColorStroke()
            }
          }


          val document = new Document(PageSize.A4,0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(30, 30, 15 , 15)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 9)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10)
            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL

            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

            val inputFormat = new SimpleDateFormat("yyyy-MM-dd")
            val outputFormat = new SimpleDateFormat("dd MMM yyyy")
            val todayDate = outputFormat.format(inputFormat.parse(convertOptionString(date)))

            var cell = new PdfPCell(new Phrase("Daily Brief Report for "+todayDate, FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

            document.add(new Paragraph("\n"));
            if(sample.length != 0) {
              (sample).foreach(row => {
                val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
                val outputTimeFormat = new SimpleDateFormat("hh:mm a")
                val inputStartTime = inputTimeFormat.parse(row.startTime.toString)
                val inputEndTime = inputTimeFormat.parse(row.endTime.toString)
                val outputStartTime = outputTimeFormat.format(inputStartTime)
                val outputEndTime = outputTimeFormat.format(inputEndTime)

                document.add(new Paragraph("\n"));


                val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 11)
                FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
                var headerTextTable1: PdfPTable = new PdfPTable(2)
                headerTextTable1.setWidthPercentage(100);
                var cell1 = new PdfPCell(new Phrase(s"Patient Name    :  ${row.patientName}\n\nPhone Number  :  ${row.phoneNumber}\n\nAppt. Time         :  ${outputStartTime} - ${outputEndTime}\n\nProvider Name  :  ${row.providerName}\n\nReason              :  ${row.reason}\n\nReferred By       :  ${row.referredBy}\n\nAlerts                 :  ${row.alerts} ", FONT_TITLE1));
                cell1.setBorder(Rectangle.NO_BORDER);
                // cell1.setPaddingTop(0);
                cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                headerTextTable1.addCell(cell1)

                headerTextTable1.setWidthPercentage(100);

                if (row.patientPhoto == "") {
                  cell1 = createImageCell2(imgLogo, 100f, 100f, true, document);
                  cell1.setBackgroundColor(BaseColor.WHITE);
                  cell1.setBorder(Rectangle.TOP);
                  cell1.setUseVariableBorders(true)
                  cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                  cell1.setPaddingLeft(15);
                  // cell1.setPaddingTop(0);
                  //cell.setBorderColorTop(BaseColor.GREEN);
                  cell1.setBorderColorTop(new BaseColor(255, 255, 255));
                  headerTextTable1.addCell(cell1);
                } else {
                  cell1 = createImageCellTest(row.patientPhoto, 100f, 100f, true, document);
                  cell1.setBackgroundColor(BaseColor.WHITE);
                  cell1.setBorder(Rectangle.TOP);
                  cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                  cell1.setPaddingLeft(15);
                  // cell1.setPaddingTop(0);
                  cell1.setBorderColorTop(new BaseColor(255, 255, 255));
                  cell1.setUseVariableBorders(true);
                  headerTextTable1.addCell(cell1);
                }
                document.add(headerTextTable1);
                document.add(new Paragraph("\n"));

              })
            } else {
              val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
              FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
              var headerTextTable1: PdfPTable = new PdfPTable(1)
              headerTextTable1.setWidthPercentage(97);
              var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1)
              document.add(headerTextTable1);
            }
            document.close();

          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"dailyBriefReport"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )

      }
    } else {
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Invalid Date Format!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }
}

  def createImageCell2(img:Image,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
    val indentation = 0
    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 17
    img.scalePercent(scaler)
    val cell = new PdfPCell(img);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  def createImageCellTest(path:String,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
    var image:Image = null
    try {
      val bi = ImageIO.read(new URL(path))
      val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
      val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
      val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
      val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
      val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
      val g = img.createGraphics()
      g.drawRenderedImage(bi, at)
      val imgBytes = new ByteArrayOutputStream()
      ImageIO.write(img, "PNG", imgBytes)
      image = Image.getInstance(imgBytes.toByteArray)
      image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
      image.setCompressionLevel(PdfStream.NO_COMPRESSION)
      image.setAlignment(Image.MIDDLE)
    } catch {
      case error: Throwable => logger.debug(error.toString)
    }

    val indentation = 0
    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / image.getHeight) * 17
    image.scalePercent(scaler)
    val cell = new PdfPCell(image);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

def appointmentCSV(fromDate: Option[String],toDate: Option[String],provider: Option[String]) = silhouette.SecuredAction.async { request =>
  var appointmentResult = ""
  var providerResult:Long = 0
  var providerId = convertOptionString(provider)
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerQuery = ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

  var providerIdMap:scala.collection.mutable.Map[Long,Long]=scala.collection.mutable.Map()
  var providerNameMap:scala.collection.mutable.Map[Long,String]=scala.collection.mutable.Map()
  var csvList = List.empty[AppointmentPDF]

      if(providerId.toLowerCase != "" && providerId.toLowerCase != "all") {
         providerQuery = providerQuery.filter(_.id === convertStringToLong(Some(providerId)))
      }

      db.run{
      providerQuery.result
      } map { providerMap =>
      (providerMap).foreach(providerResult => {

      val block1 =  db.run {
         AppointmentTable.filter(_.practiceId === practiceId).filterNot(_.deleted).filter(_.providerId === convertOptionLong(providerResult.id))
        .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
        .filterNot(_.status.toLowerCase === "no show")
        .filterNot(_.status.toLowerCase === "canceled")
        .join(ProviderTable).on(_.providerId === _.id)
        .join(OperatorTable).on(_._1.operatoryId === _.id)
        .result
      } map { appointment =>
             val data = appointment.groupBy(_._1._1.id).map {
              case (id,entries) =>
                val entry = entries.head._1._1

                val date = dateFormatter.print(entry.date)
                val startTime = entry.startTime
                val endTime = entry.endTime
                val patientName = entry.patientName
                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                val providerName = providerTitle + " " + providerFirstName + " " + providerLastName
                // val procedureCode = entry.procedureCode
                val reason = entry.reason
                val email = convertOptionString(entry.email)
                val phoneNumber = entry.phoneNumber

                val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
                val outputTimeFormat = new SimpleDateFormat("hh:mm a")
                val inputStartTime = inputTimeFormat.parse(startTime.toString)
                val inputEndTime = inputTimeFormat.parse(endTime.toString)
                val outputStartTime = outputTimeFormat.format(inputStartTime)
                val outputEndTime = outputTimeFormat.format(inputEndTime)

                var procedureCode = ""
                var procedureType = ""

              var tempProcedure = convertOptionString(entry.procedure)
              if(tempProcedure != null && tempProcedure != ""){
                var temp = tempProcedure.replace('★', '"')
                val jsonObject: JsValue = Json.parse(temp)
                val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

                if(procedureJson != None){
                procedureJson.get.foreach(s => {
                var a = (s \ "procedureCode").asOpt[String]
                var c = (s \ "toothNumber").asOpt[String]
                var toothNumber = ""
                if(convertOptionString(c) != ""){
                  toothNumber = s" (T-${c.get})"
                }
                if(convertOptionString(a) != ""){
                  procedureCode = procedureCode + a.get + toothNumber + ", "
                }
                // procedureCode = procedureCode + a.get + ", "
                var b = (s \ "procedureDescription").asOpt[String]
                procedureType = procedureType + b.get + ", "})
                }
              }

              var dataList = AppointmentPDF(date,outputStartTime,outputEndTime,patientName,providerName,operatoryName,procedureType.dropRight(2),procedureCode.dropRight(2),reason,email,phoneNumber)
              csvList = csvList :+ dataList
              }
          }
        val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))
      })
    val file = new File("/tmp/appointment.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Date", "Start Time", "End Time", "Patient Name", "Phone Number", "Email", "Operatory Name", "Provider Name", "Procedure Code", "Reason"))
    csvList.sortBy(s => (s.date, s.startTime)).map {
      appointmentList => writer.writeRow(List(appointmentList.date, appointmentList.startTime, appointmentList.endTime, appointmentList.patientName, appointmentList.phoneNumber, appointmentList.email, appointmentList.operatoryName, appointmentList.providerName, appointmentList.procedureCode, appointmentList.reason))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    }
  }

  def migratePatientName = silhouette.SecuredAction.async { request =>
  db.run{
    for{
        appointment <- AppointmentTable.filterNot(_.deleted).map(_.patientId).distinct.result
        } yield{
            (appointment).foreach(patientId => {
         db.run{
            StepTable.filter(_.treatmentId === patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
          }map { stepMap =>
              val firstName =  (stepMap.value \ "full_name").as[String]
              val lastName =  (stepMap.value \ "last_name").as[String]
              val patientName = firstName+" "+lastName

             var block1 = db.run{
                AppointmentTable.filter(_.patientId === patientId).map(_.patientName).update(patientName)
              } map {
                case 0 => NotFound
                case _ => Ok
              }
            val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
            }
          })
        Ok{Json.obj("status"->"Success","message"->"Data Migrated Successfully")}
      }
    }
  }

  def recallReportPDF(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction.async{ request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val StrFromDate  = LocalDate.parse(convertOptionString(fromDate))
    val StrToDate = LocalDate.parse(convertOptionString(toDate))
    var data1 = List.empty[RecallReportRow]
    val currentDate = DateTime.now()
    var treatmentCount = 0
      db.run {
        TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
          .join(ProviderTable).on(_.providerId === _.id)
          .join(StepTable).on(_._1.id === _.treatmentId).result
        } map { recallMap =>


               var recordsFound :Boolean = false
               val data = recallMap.groupBy(_._1._1.id).map {
                case (treatmentId, entries) =>
                val treatments = entries.head._1._1
                val provider = entries.head._1._2
                val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      if(treatments.recallDueDate != None && treatments.recallDueDate != ""){
                var phoneNumber = ""
                var fullAddress = ""
                var email = ""
                var patientName = ""
                var revisitDate: DateTime = null
                var referredBy = ""
                var recallType = ""
                var recallCode = ""
                var interval = ""
                var recallDueDate = convertOptionalLocalDate(treatments.recallDueDate)
                var recallNote = ""
                var providerName = ""


              val firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
              val lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))

                if(StrFromDate <= recallDueDate && StrToDate >= recallDueDate) {
                    recordsFound = true
                    patientName = firstName +" "+ lastName
                    phoneNumber = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
                    email = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
                    fullAddress = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))
                    referredBy = convertOptionString(treatments.referralSource)
                    recallType = convertOptionString(treatments.recallType)
                    recallCode = convertOptionString(treatments.recallCode)
                    interval = convertOptionLong(treatments.intervalNumber) + " " + convertOptionString(treatments.intervalUnit)
                    recallNote = convertOptionString(treatments.recallNote)
                    providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

                val dataList = RecallReportRow(
                    patientName,
                    phoneNumber,
                    email,
                    fullAddress,
                    providerName,
                    referredBy,
                    recallType,
                    recallCode,
                    interval,
                    recallDueDate,
                    recallNote
                  )
                  data1 = data1 :+ dataList
                }
 }
            }.toList


        val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

           class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }

          val document = new Document(PageSize.A3.rotate(),0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(30, 30, 15 , 15)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL

            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

            var cell = new PdfPCell(new Phrase("Recall Report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

            document.add(new Paragraph("\n"));

            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(100);
            var cell1 = new PdfPCell

            var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
            var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
            var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

            if(fromDate != toDate){
              cell1 = new PdfPCell(new Phrase("From Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_CONTENT));
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1);

              var cell2 = new PdfPCell(new Phrase("To Date     : "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_CONTENT));
              cell2.setBorder(Rectangle.NO_BORDER);
              cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell2);
            } else {
              cell1 = new PdfPCell(new Phrase("Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_CONTENT));
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1);
            }
            document.add(headerTextTable1);

            document.add(new Paragraph("\n"));

            var rowsPerPage = 8;
            var recNum =  0;
            if(recordsFound){

              (data1.sortBy(_.recallDueDate)).foreach(row => {
              var sampleData = List(s"${row.patientName}", s"${row.phoneNumber}", s"${row.email}", s"${row.address}",s"${row.referredBy}" ,s"${row.recallType}",s"${row.recallCode}",s"${row.interval}",s"${row.recallDueDate}",s"${row.providerName}",s"${row.recallNote}")
              var columnWidths:Array[Float] = Array(9,10,10,9,9,9,8,8,10,9,9);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);

              if(recNum == 0 || recNum % rowsPerPage == 0) {

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Type", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Intervel", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Due Date", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Note", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {

                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  cell.setPadding(13);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_LEFT);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
              })

              document.add(table);

              recNum = recNum+1;
              if(recNum == 0 || recNum % rowsPerPage == 0) {
                document.newPage();
              }
              if(recNum==9 && rowsPerPage!=10){
                recNum = 1
                rowsPerPage = 10
              }
            })
            } else {

              var columnWidths:Array[Float] = Array(9,10,10,9,9,9,8,8,10,9,9);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Type", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Intervel", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Due Date", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Recall Note", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                document.add(table);

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);
            }

            document.close();

          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"Recall report"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
    }

  def recallReportCSV(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction{ request =>

     val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
     val StrFromDate  = LocalDate.parse(convertOptionString(fromDate))
     val StrToDate = LocalDate.parse(convertOptionString(toDate))
     var csvList = List.empty[RecallReportRow]


      var block1 = db.run {
        TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
        .join(ProviderTable).on(_.providerId === _.id)
        .join(StepTable).on(_._1.id === _.treatmentId).result

        } map { recallMap =>
               val data = recallMap.groupBy(_._1._1.id).map {
                case (treatmentId, entries) =>
                val treatments = entries.head._1._1
                val provider = entries.head._1._2
                val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

if(treatments.recallDueDate != None && treatments.recallDueDate != ""){
                var phoneNumber = ""
                var fullAddress = ""
                var email = ""
                var patientName = ""
                // var nextVisitdate = ""
                var referredBy = ""
                var revisitDate: DateTime = null
                // val recallPeriod = convertOptionString(treatments.recallPeriod).filter(_.isDigit).toInt
                var recallType = ""
                var recallCode = ""
                var interval = ""
                var recallDueDate = convertOptionalLocalDate(treatments.recallDueDate)
                var recallNote = ""
                var providerName = ""


                val firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
                val lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))

                if(StrFromDate <= recallDueDate && StrToDate >= recallDueDate) {
                    patientName = firstName +" "+ lastName
                    phoneNumber = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
                    email = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
                    fullAddress = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))
                    referredBy = convertOptionString(treatments.referralSource)
                    recallType = convertOptionString(treatments.recallType)
                    recallCode = convertOptionString(treatments.recallCode)
                    interval = convertOptionLong(treatments.intervalNumber) + " " + convertOptionString(treatments.intervalUnit)
                    recallNote = convertOptionString(treatments.recallNote)
                    providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

                var dataList = RecallReportRow(patientName,phoneNumber,email,fullAddress,providerName,referredBy,recallType,recallCode,interval,recallDueDate,recallNote)
                csvList = csvList :+ dataList
                }
          }
        }
      }
      val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))
      val file = new File("/tmp/recall report.csv")
      val writer = CSVWriter.open(file)
      writer.writeRow(List("Patient Name", "Phone Number", "Email", "Address","Referred By", "Recall Type","Recall Code","Intervel","Recall Due Date","Provider","Recall Note"))
      csvList.sortBy(_.recallDueDate).map {
        recallReport => writer.writeRow(List(recallReport.patientName,recallReport.phoneNumber,recallReport.email,recallReport.address,recallReport.referredBy,recallReport.recallType,recallReport.recallCode,recallReport.interval,recallReport.recallDueDate,recallReport.providerName,recallReport.recallNote))
      }
      writer.close()
      Ok.sendFile(file, inline = false, _ => file.getName)
      }


  def listOptionsReadAll(listType : String ) = silhouette.SecuredAction.async { request =>
      db.run {
          ListOptionTable.filter(_.listType.toLowerCase === listType.toLowerCase).sortBy(_.listValue).result
      } map { listMap =>
        Ok(Json.toJson(listMap))
      }
  }

  def providerAppointmentList(fromDate: Option[String],toDate: Option[String],provider: Option[String]) =silhouette.SecuredAction.async { request =>
  var appointmentResult = ""
  var providerResult:Long = 0
  var providerName = ""
  var providerId = convertOptionString(provider)

  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerQuery = ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

      if(providerId.toLowerCase != "" && providerId.toLowerCase != "all") {
         providerQuery = providerQuery.filter(_.id === convertStringToLong(Some(providerId)))
      }

      db.run {
         AppointmentTable.filter(_.practiceId === practiceId).filterNot(_.deleted)
        .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
        .filterNot(_.status.toLowerCase === "no show")
        .filterNot(_.status.toLowerCase === "canceled")
        .join(providerQuery).on(_.providerId === _.id)
        .join(OperatorTable).on(_._1.operatoryId === _.id)
        .result
      } map { appointment =>

              val data = appointment.groupBy(_._1._1.id).map {
              case (id,entries) =>
                val entry = entries.head._1._1
                val operatoryName = entries.head._2.name
                val providerTitle = entries.head._1._2.title
                val providerFirstName = entries.head._1._2.firstName
                val providerLastName = entries.head._1._2.lastName
                providerName = providerTitle + " " + providerFirstName + " " + providerLastName

                val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
                val outputTimeFormat = new SimpleDateFormat("hh:mm a")
                val inputStartTime = inputTimeFormat.parse(entry.startTime.toString)
                val inputEndTime = inputTimeFormat.parse(entry.endTime.toString)
                val outputStartTime = outputTimeFormat.format(inputStartTime)
                val outputEndTime = outputTimeFormat.format(inputEndTime)

                var procedureCode = ""
                var procedureType = ""

              var tempProcedure = convertOptionString(entry.procedure)
              if(tempProcedure != null && tempProcedure != ""){
                var temp = tempProcedure.replace('★', '"')
                val jsonObject: JsValue = Json.parse(temp)
                val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]

                if(procedureJson != None){
                procedureJson.get.foreach(s => {
                var a = (s \ "procedureCode").asOpt[String]
                var c = (s \ "toothNumber").asOpt[String]
                var toothNumber = ""
                if(convertOptionString(c) != ""){
                  toothNumber = s" (T-${c.get})"
                }
                if(convertOptionString(a) != ""){
                  procedureCode = procedureCode + a.get + toothNumber + ", "
                }
                var b = (s \ "procedureDescription").asOpt[String]
                procedureType = procedureType + b.get + ", "})
                }
              }

                AppointmentPDF(
                  dateFormatter.print(entry.date),
                  outputStartTime,
                  outputEndTime,
                  entry.patientName,
                  providerName,
                  operatoryName,
                  procedureType,
                  procedureCode,
                  entry.reason,
                  convertOptionString(entry.email),
                  entry.phoneNumber
                 )
              }.toList.sortBy(r => (r.date,r.startTime))
      Ok(Json.toJson(data))
   }
}

def cancelReportList(fromDate: Option[String],toDate: Option[String])=silhouette.SecuredAction.async { request =>

  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var providerName = ""
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

    db.run {

      AppointmentTable.filter(_.practiceId === practiceId)
        .filter(s=> s.status.toLowerCase === "no show" || s.status.toLowerCase === "canceled")
        .filter(s=> s.date >= LocalDate.parse(convertOptionString(fromDate)) && s.date <= LocalDate.parse(convertOptionString(toDate)))
        .join(ProviderTable).on(_.providerId === _.id)
        .result
    } map { appointment =>

      val data = appointment.groupBy(_._1.id).map {
        case (id, items) => {
          val entry = items.head._1
          val providerTitle = items.head._2.title
          val providerFirstName = items.head._2.firstName
          val providerLastName = items.head._2.lastName
          providerName = providerTitle + " " + providerFirstName + " " + providerLastName

            CancelReport(
              dateFormatter.print(entry.date),
              entry.patientName,
              entry.phoneNumber,
              providerName,
              entry.referredBy,
              entry.reason,
              entry.status,
              convertOptionString(entry.email)
            )
          }
        }.toList.sortBy(_.date)
      Ok(Json.toJson(data))
    }
}


def recallReportList(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction.async{ request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val StrFromDate  = LocalDate.parse(convertOptionString(fromDate))
    val StrToDate = LocalDate.parse(convertOptionString(toDate))
    val currentDate = DateTime.now()
    var treatmentCount = 0
    var data1 = List.empty[RecallReportRow]

      db.run {
        TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)
          .join(ProviderTable).on(_.providerId === _.id)
          .join(StepTable).on(_._1.id === _.treatmentId).result
        } map { recallMap =>

               var recordsFound :Boolean = false
               val data = recallMap.groupBy(_._1._1.id).map {
                case (treatmentId, entries) =>
                val treatments = entries.head._1._1
                val provider = entries.head._1._2
                val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      if(treatments.recallDueDate != None && treatments.recallDueDate != ""){

                var phoneNumber = ""
                var fullAddress = ""
                var email = ""
                var patientName = ""
                var nextVisitdate = ""
                var revisitDate: DateTime = null
                var referredBy = ""
                // val recallPeriod = convertOptionString(treatments.recallPeriod).filter(_.isDigit).toInt
                var recallType = ""
                var recallCode = ""
                var interval = ""
                // var recallDueDate: Option[LocalDate] = None
                var recallDueDate = convertOptionalLocalDate(treatments.recallDueDate)
                var recallNote = ""
                var providerName = ""


              val firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
              val lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))

                if(StrFromDate <= recallDueDate && StrToDate >= recallDueDate) {

                    patientName = firstName +" "+ lastName
                    phoneNumber = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
                    email = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
                    fullAddress = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))
                    referredBy = convertOptionString(treatments.referralSource)
                    recallType = convertOptionString(treatments.recallType)
                    recallCode = convertOptionString(treatments.recallCode)
                    interval = convertOptionLong(treatments.intervalNumber) + " " + convertOptionString(treatments.intervalUnit)
                    recallNote = convertOptionString(treatments.recallNote)
                    providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"

          // if(nextVisitdate != ""){

                val dataList = RecallReportRow(patientName,phoneNumber,email,fullAddress,providerName,referredBy,recallType,recallCode,interval,recallDueDate,recallNote)
                data1 = data1 :+ dataList
          }
                }
            }.toList
            Ok(Json.toJson(data1.sortBy(_.recallDueDate)))
        }
}

  def migrateAppointmentProcedure = silhouette.SecuredAction.async { request =>
  var script = "{★procedure★:[{★procedureCode★:★rCode★,★procedureDescription★:★rDescription★}]}"

  db.run{
    for{
        appointment <- AppointmentTable.filterNot(_.deleted).result
        } yield{
            (appointment).foreach(row => {
              var procedureCode = row.procedureCode
              var procedureDescription = row.procedureType
              var value = script.replace("rCode",procedureCode).replace("rDescription",procedureDescription)
                var block = db.run{
                  AppointmentTable.filter(_.id === row.id).map(_.procedure).update(Some(value))
                }
                val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          })
        Ok{Json.obj("status"->"Success","message"->"Procedure Migrated Successfully")}
      }
    }
  }

case class ApptProviderFeeRow(
  providerName: String,
  fee: Float
)
implicit val ApptProviderFeeFormat = Json.format[ApptProviderFeeRow]

def apptProviderFee(date: String) = silhouette.SecuredAction.async { request =>
var apptDate = LocalDate.parse(date)
var dataList = List.empty[ApptProviderFeeRow]
  db.run{
      AppointmentTable.filterNot(_.deleted).filter(_.date === apptDate).join(ProviderTable).on(_.providerId === _.id).result
    } map { apptMap =>
      val data = apptMap.groupBy(_._1.providerId).map {
        case (providerId, entries) =>
          val appointment = entries.map(_._1)
          val provider = entries.head._2
          var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"
            var fee = 0f
            (appointment).foreach(apptResult => {
                fee = fee + apptResult.amountToBeCollected
            })
            var data = ApptProviderFeeRow(providerName,fee)
            dataList = dataList :+ data
    }
      Ok{Json.toJson(dataList)}
    }
  }
}



