
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffMembersRow,StaffRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import scala.util.parsing.json._

class StaffMembersController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

 implicit val staffMembersWrites = Writes { request: (StaffMembersRow) =>
    val (staffMembers) = request
    val name = staffMembers.firstName+" "+ staffMembers.lastName
  Json.obj(
      "id" -> staffMembers.id,
      "practiceId" -> staffMembers.practiceId,
      "firstName" -> staffMembers.firstName,
      "lastName" -> staffMembers.lastName,
      "title" -> staffMembers.title,
      // "name" -> name.split(' ').map(_.capitalize).mkString(" "),
      "name" -> name,
      "empStartDate" -> staffMembers.empStartDate,
      "empTerminationDate" -> staffMembers.empTerminationDate,
      "color" -> staffMembers.color,
      "rda" -> staffMembers.rda,
      "expDateOfRda" -> staffMembers.expDateOfRda,
      "role" -> staffMembers.role,
      "dob" -> staffMembers.dob
      )
  }

   def readAll = silhouette.SecuredAction.async { request =>
    val strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
     db.run {
          StaffMembersTable.filterNot(_.deleted).filter(_.practiceId === strPracticeId).sortBy(r => (r.firstName, r.lastName)).result
      } map { staffMembersList =>
        Ok(Json.toJson(staffMembersList))
      }
  }
 
 def create = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val staffMembers = for {
      firstName <- (request.body \ "firstName").validate[String]
      lastName <- (request.body \ "lastName").validate[String]
      title <- (request.body \ "title").validate[String]
      empStartDate <- (request.body \ "empStartDate").validate[LocalDate]
      empTerminationDate <- (request.body \ "empTerminationDate").validateOpt[LocalDate]
      color <- (request.body \ "color").validateOpt[String]
      rda <- (request.body \ "rda").validateOpt[String]
      expDateOfRda <- (request.body \ "expDateOfRda").validateOpt[LocalDate]
      role <- (request.body \ "role").validateOpt[String]
      dob <- (request.body \ "dob").validate[LocalDate]

    } yield {
      var vColor = "#ffdfd9"
      if(color != None && color != Some("")){
        vColor = convertOptionString(color)
      }
      StaffMembersRow(None,practiceId,firstName,lastName,title,empStartDate,empTerminationDate,vColor,rda,expDateOfRda,false,role,Some(dob))
    }

    staffMembers match {
      case JsSuccess(staffMembersRow, _) =>
          db.run {   
            StaffMembersTable.returning(StaffMembersTable.map(_.id)) += staffMembersRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(staffMembersRow.copy(id = Some(id))))
                }
      
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val staffMembers = for {
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
      title <- (request.body \ "title").validateOpt[String]
      empStartDate <- (request.body \ "empStartDate").validateOpt[LocalDate]
      empTerminationDate <- (request.body \ "empTerminationDate").validateOpt[LocalDate]
      color <- (request.body \ "color").validateOpt[String]
      rda <- (request.body \ "rda").validateOpt[String]
      expDateOfRda <- (request.body \ "expDateOfRda").validateOpt[LocalDate]
      role <- (request.body \ "role").validateOpt[String]
      dob <- (request.body \ "dob").validateOpt[LocalDate]
      } yield {
      (firstName,lastName,title,empStartDate,empTerminationDate,color,rda,expDateOfRda,role,dob)
    }
    staffMembers match {
      case JsSuccess((firstName,lastName,title,empStartDate,empTerminationDate,color,rda,expDateOfRda,role,dob), _) => db.run {
        val query = StaffMembersTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            firstName.map(value => query.map(_.firstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            lastName.map(value => query.map(_.lastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            empStartDate.map(value => query.map(_.empStartDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(empTerminationDate).map(value => query.map(_.empTerminationDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            color.map(value => query.map(_.color).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(rda).map(value => query.map(_.rda).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(expDateOfRda).map(value => query.map(_.expDateOfRda).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(role).map(value => query.map(_.role).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(dob).map(value => query.map(_.dob).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
 
   def delete(id: Long) = silhouette.SecuredAction.async { request =>
      db.run {
          StaffMembersTable.filter(_.id === id).map(_.deleted).update(true)
      } map {
      case 0 => NotFound
      case _ => Ok
    }
  }
}


