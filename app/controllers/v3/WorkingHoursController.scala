
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import play.api.libs.json.Json
import javax.inject._
import models.daos.tables.{ StaffRow, WorkingHoursRow}
import play.api.mvc.Result
import org.joda.time.{ LocalDate }
import java.time.format.DateTimeFormatter
import java.time.LocalTime
import java.text.SimpleDateFormat
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.control.Breaks.{break, breakable}



class WorkingHoursController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._


  //implicit val workingHoursFormat = Json.format[WorkingHoursRow]

  implicit val workingHoursRowWrites = Writes { workingHourRocord: (WorkingHoursRow) =>
    val (workingRow) = workingHourRocord

    val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
    val outputTimeFormat = new SimpleDateFormat("hh:mm a")
    val inputStartTime = inputTimeFormat.parse(workingRow.startTime.toString)
    val inputEndTime = inputTimeFormat.parse(workingRow.endTime.toString)

    val inputLunchStartTime = inputTimeFormat.parse(convertOptionalLocalTime(workingRow.lunchStartTime).toString)
    val inputLunchEndTime = inputTimeFormat.parse(convertOptionalLocalTime(workingRow.lunchEndTime).toString)

    val outputStartTime = outputTimeFormat.format(inputStartTime)
    val outputEndTime = outputTimeFormat.format(inputEndTime)


    Json.obj(
      "id" -> workingRow.id,
      "practiceId" -> workingRow.practiceId,
      "day" -> workingRow.day,
      "startTime" -> outputStartTime,
      "endTime" -> outputEndTime,
      "lunchStartTime" -> outputTimeFormat.format(inputLunchStartTime),
      "lunchEndTime" -> outputTimeFormat.format(inputLunchEndTime)

    )
  }

  def readAll = silhouette.SecuredAction.async { request =>
    var practiceId:Long = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
      WorkingHoursTable.filter(_.practiceId === practiceId).sortBy(_.id).result
    } map { workingHoursList =>
      var responseObj = Json.obj()
      responseObj = responseObj + ("id" -> JsNumber(practiceId)) + ("workingHours" -> Json.toJson(workingHoursList))
      Ok(Json.toJson(Array(responseObj)))
    }
  }



  def create = silhouette.SecuredAction.async(parse.json) { request =>
    val formatter = DateTimeFormat.forPattern("hh:mm a");
    var workingHours = for {

      practiceId <- (request.body \ "practiceId").validate[Long]
      day <- (request.body \ "day").validate[String]
      startTime <- (request.body \ "startTime").validate[String]
      endTime <- (request.body \ "endTime").validate[String]
      lunchStartTime <- (request.body \ "lunchStartTime").validate[String]
      lunchEndTime <- (request.body \ "lunchEndTime").validate[String]
    }
      yield

        WorkingHoursRow(None, practiceId, day,LocalTime.parse(startTime, formatter),LocalTime.parse(endTime, formatter),
        Some(LocalTime.parse(lunchStartTime, formatter)),Some(LocalTime.parse(lunchEndTime, formatter)))

    workingHours match {
      case JsSuccess(workingRow, _) =>

        if(workingRow.endTime < workingRow.startTime){
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("End time should be greater than start time!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
        } else {
          db.run{
            WorkingHoursTable.returning(WorkingHoursTable.map(_.id)) += workingRow
          } map {
            case 0L => PreconditionFailed
            case id => Created(Json.toJson(workingRow.copy(id = Some(id))))
          }
        }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def update(strPracticeId: Long)= silhouette.SecuredAction.async(parse.json) { request =>

    var timeFormat : Boolean = false
    var alreadyAppBooked_Time : Boolean = false
    var alreadyAppBooked_Date : Boolean = false
    var skipEndtimeValidation : Boolean = false
    var lunchHoursValidation : Boolean = false
    var lunchTimeValidation : Boolean = false
    val formatter = DateTimeFormat.forPattern("hh:mm a");
    val currentDate: LocalDate = LocalDate.now()

    class Time(
                var startTime:LocalTime,
                var endTime:LocalTime,
                var lunchStartTime:LocalTime,
                var lunchEndTime:LocalTime
              )

    val jsonList = (request.body \ "workingHours").as[List[JsValue]]
    val dayTimeMap:scala.collection.mutable.Map[String,Time]=scala.collection.mutable.Map()

    (jsonList).foreach { list=>
      val result = scala.util.parsing.json.JSON.parseFull(list.toString)
      result match {
        case Some(e:Map[String,String]) => {
          val startTime =  LocalTime.parse(convertOptionString(e.get("startTime")), formatter)
          var endTime = LocalTime.parse(convertOptionString(e.get("endTime")), formatter)
          var lunchStartTime = LocalTime.parse(convertOptionString(e.get("lunchStartTime")), formatter)
          var lunchEndTime = LocalTime.parse(convertOptionString(e.get("lunchEndTime")), formatter)

          if(endTime.toString == "00:00:00.000"){
            endTime = endTime.minusMinutes(1)
          }
          val day = convertOptionString(e.get("day"))

          if(endTime.toString == "00:00:00.000"){
            skipEndtimeValidation = true
          }

          if(endTime < startTime && !skipEndtimeValidation){
            timeFormat = true
          }

          if(startTime == lunchStartTime && endTime == lunchEndTime){
            lunchHoursValidation = true
          }

          if(lunchEndTime < lunchStartTime){
            lunchTimeValidation = true
          }

          var t1 = new Time(startTime,endTime,lunchStartTime,lunchEndTime)
          dayTimeMap.update(day,t1)
        }
      }
    }

    var missedDay = ""
    var block1 = db.run{
      AppointmentTable.filterNot(_.deleted).filterNot(_.status.toLowerCase === "canceled").filterNot(_.status.toLowerCase === "completed")
        .filter(_.practiceId === strPracticeId).filter(_.date >= currentDate).result
    } map { appointmentMap =>
      (appointmentMap).foreach(results => {
        val dayDate = DateTimeFormat.forPattern("EEEE").print(results.date)

        breakable {
          if(dayTimeMap contains dayDate){

            val inputStartTime = dayTimeMap(dayDate).startTime
            var inputEndTime =dayTimeMap(dayDate).endTime
            var inputLunchStartTime =dayTimeMap(dayDate).lunchStartTime
            var inputLunchEndTime =dayTimeMap(dayDate).lunchEndTime

            if(inputEndTime.toString == "00:00:00.000"){
              inputEndTime = inputEndTime.minusMinutes(1)
            }

            if((inputStartTime == inputEndTime) || (results.startTime >= inputStartTime && inputStartTime < results.endTime &&
                            inputEndTime > results.startTime && results.endTime <= inputEndTime) ) {
            } else {
              alreadyAppBooked_Time = true
              missedDay = dayDate
              break
            }
          } else {
            alreadyAppBooked_Date = true
            missedDay = dayDate
            break
          }
        }
      })
      Ok
    }
    val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))


    if(timeFormat){
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("End time should be greater than start time!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})

    // } else if(alreadyAppBooked_Date){
    //   val obj = Json.obj()
    //   val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Some appointments already scheduled on "+ missedDay +".Please reschedule those appointments and try again!"))
    //   Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else if(lunchTimeValidation){
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Lunch end time should be greater than lunch start time!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else if(lunchHoursValidation){
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Lunch time not same as working hours!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else if(alreadyAppBooked_Time) {
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Some appointments already scheduled for "+ missedDay +" beyond the selected working hours.Please reschedule those appointments and try again!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else {
      var workingHouesDelete = db.run{
        WorkingHoursTable.filter(_.practiceId === strPracticeId).delete map {
          case 0 => NotFound
          case _ => Ok
        }
      }
      var AwaitResult1 = Await.ready(workingHouesDelete, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      (jsonList).foreach{list=>
        val result = scala.util.parsing.json.JSON.parseFull(list.toString)
        result match {
          case Some(e:Map[String,String]) => {

            val startTime = convertOptionString(e.get("startTime"))
            val endTime = convertOptionString(e.get("endTime"))
            val day = convertOptionString(e.get("day"))
            val lunchStartTime = convertOptionString(e.get("lunchStartTime"))
            val lunchEndTime = convertOptionString(e.get("lunchEndTime"))

            InsertNewRecord(strPracticeId,day,startTime,endTime,lunchStartTime,lunchEndTime)
          }
          case None =>
        }
      }
      db.run {
        WorkingHoursTable.sortBy(_.id).countDistinct.result
      } map { workingHoursList =>
        Ok
      }
    }
  }

  def InsertNewRecord(strPracticeId: Long,strDay: String, strStartTime: String, strEndTime: String, strLunchStartTime: String, strLunchEndTime: String) {
    val formatter = DateTimeFormat.forPattern("hh:mm a");
    var workingRow = WorkingHoursRow(None, strPracticeId, strDay,LocalTime.parse(strStartTime, formatter),LocalTime.parse(strEndTime, formatter),Some(LocalTime.parse(strLunchStartTime, formatter)), Some(LocalTime.parse(strLunchEndTime, formatter)))

    db.run{
      WorkingHoursTable.returning(WorkingHoursTable.map(_.id)) += workingRow
    } map {
      case 0L => PreconditionFailed
      case id => Created(Json.toJson(workingRow.copy(id = Some(id))))
    }
  }
}


