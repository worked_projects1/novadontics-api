package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{VendorInvoiceRow, StaffRow, OrderRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._


class VendorInvoiceController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

// implicit val vendorInvoiceFormat = Json.format[VendorInvoiceRow]

  implicit val VendorInvoiceRowWrites = Writes { request: (VendorInvoiceRow) =>
    val (vendorInvoiceRow) = request
    Json.obj(
      "id" -> vendorInvoiceRow.id,
      "vendorId" -> vendorInvoiceRow.vendorId,
      "orderId" -> vendorInvoiceRow.orderId,
      "vendorName" -> vendorInvoiceRow.vendorName,
      "invoiceNum" -> vendorInvoiceRow.invoiceNum,
      "paymentType" -> vendorInvoiceRow.paymentType,
      "amountPaid" -> Math.round(vendorInvoiceRow.amountPaid * 100.0) / 100.0,
      "paymentDate" -> vendorInvoiceRow.paymentDate,
      "checkNumber" -> vendorInvoiceRow.checkNumber,
      "notes" -> vendorInvoiceRow.notes,
      "invoice" -> vendorInvoiceRow.invoice
    )
  }

 def read(orderId: Option[Long], vendorId: Option[Long]) = silhouette.SecuredAction.async {

   var query = VendorInvoiceTable.sortBy(_.id)
   
   if(orderId != None && orderId != Some("")){
     query = query.filter(_.orderId === orderId)
   }  

   if(vendorId != None && vendorId != Some("")){
     query = query.filter(_.vendorId === vendorId)
   }

    db.run {
      query.result
    } map { vendorList =>
      Ok(Json.toJson(vendorList))
      
    }
  }

 def getVendorInvoices(orderId: Long) = silhouette.SecuredAction.async {
   db.run{
     VendorInvoiceTable.filter(_.orderId === orderId).result
   } map {vendorMap =>
    Ok(Json.toJson(vendorMap))
   }
}

 def create = silhouette.SecuredAction.async(parse.json) { request =>
 
    val vendorInvoie = for {    
      vendorId <- (request.body \ "vendorId").validate[Long]
      orderId <- (request.body \ "orderId").validate[Long]
      vendorName <- (request.body \ "vendorName").validate[String]
      invoiceNum <- (request.body \ "invoiceNum").validate[String]
      paymentType <- (request.body \ "paymentType").validate[String]
      amountPaid <- (request.body \ "amountPaid").validate[Float]
      paymentDate <- (request.body \ "paymentDate").validate[String]
      checkNumber <- (request.body \ "checkNumber").validate[String]
      notes <- (request.body \ "notes").validateOpt[String]
      invoice <- (request.body \ "invoice").validate[String]
      
    } yield {
      var vPaymentDate:Option[LocalDate] = None
      if(paymentDate != None && paymentDate != Some("")){
        vPaymentDate = Some(LocalDate.parse(paymentDate))
      }
      
    VendorInvoiceRow(None,vendorId,orderId,vendorName,invoiceNum,paymentType,amountPaid,convertOptionalLocalDate(vPaymentDate),checkNumber,notes,invoice)
    }

    vendorInvoie match {
      case JsSuccess(vendorInvoiceRow, _) =>
      db.run{
       VendorInvoiceTable.returning(VendorInvoiceTable.map(_.id)) += vendorInvoiceRow
        } map {
              case 0L => PreconditionFailed
              case id => db.run {
                  OrderItemTable.filter(_.orderId === vendorInvoiceRow.orderId).filter(_.productVendorId === vendorInvoiceRow.vendorId).map(_.itemPaid).update(Some(true))
                  } map {
                   case 0 => NotFound
                   case _ =>
                   var allItemPaid = true
                   db.run {
                     OrderItemTable.filter(_.orderId === vendorInvoiceRow.orderId).result
                     } map { itemMap =>
                       itemMap.foreach(itemList => {
                         if(convertOptionalBoolean(itemList.itemPaid) == false){
                           allItemPaid = false
                         }
                       })

                      if(allItemPaid == true){
                         val query = OrderTable.filter(_.id === vendorInvoiceRow.orderId)
                         val allVendorsPaid = true
                         var block = db.run{
                           query.map(_.status).result
                         } map { status =>
                          status.map{ statusValue =>
                            var orderStatus: OrderRow.Status = statusValue
                            if(statusValue == OrderRow.Status.Fulfilled){
                              orderStatus = OrderRow.Status.Void
                            }

                          db.run {
                            query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                            case true => DBIO.seq[Effect.Write](
                              Some(orderStatus).map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                              Some(allVendorsPaid).map(value => query.map(_.allVendorsPaid).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                            ) map {_ => Ok}
                            case false => DBIO.successful(NotFound)
                            }
                          }
                          }
                        }
                         var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

                     }
                     }

                  }
              Created(Json.toJson(vendorInvoiceRow.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }
  }

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>

    val invoiceOrder = for {
      vendorId <- (request.body \ "vendorId").validateOpt[Long]
      orderId <- (request.body \ "orderId").validateOpt[Long]
      vendorName <- (request.body \ "vendorName").validateOpt[String]
      invoiceNum <- (request.body \ "invoiceNum").validateOpt[String]
      paymentType <- (request.body \ "paymentType").validateOpt[String]
      amountPaid <- (request.body \ "amountPaid").validateOpt[Float]
      paymentDate <- (request.body \ "paymentDate").validateOpt[String]
      checkNumber <- (request.body \ "checkNumber").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      invoice <- (request.body \ "invoice").validateOpt[String]

    } yield {

      var vPaymentDate:Option[LocalDate] = None
      if(paymentDate != None && paymentDate != Some("")){
        vPaymentDate = Some(LocalDate.parse(convertOptionString(paymentDate)))
      }
     (vendorId,orderId,vendorName,invoiceNum,paymentType,amountPaid,convertOptionalLocalDate(vPaymentDate),checkNumber,notes,invoice)
    }

    invoiceOrder match {
      case JsSuccess((vendorId,orderId,vendorName,invoiceNum,paymentType,amountPaid,vPaymentDate,checkNumber,notes,invoice), _) => db.run {
        val query = VendorInvoiceTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            vendorId.map(value => query.map(_.vendorId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            orderId.map(value => query.map(_.orderId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            vendorName.map(value => query.map(_.vendorName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            invoiceNum.map(value => query.map(_.invoiceNum).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            paymentType.map(value => query.map(_.paymentType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            amountPaid.map(value => query.map(_.amountPaid).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(vPaymentDate).map(value => query.map(_.paymentDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            checkNumber.map(value => query.map(_.checkNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            notes.map(value => query.map(_.notes).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            invoice.map(value => query.map(_.invoice).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
            
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def delete(id: Long) = silhouette.SecuredAction.async {
      db.run {
          VendorInvoiceTable.filter(_.id === id).delete
      } map {
      case 0 => NotFound
      case _ => Ok
    }
}

def vendorInvoiceMigration = silhouette.SecuredAction.async { request =>
  db.run{
      OrderItemTable.result
  } map { orderMap =>

    orderMap.map {orderResult =>
    if(orderResult.paymentType != None && convertOptionString(orderResult.paymentType) != ""){
     var block =  db.run{
        VendorInvoiceTable.filter(_.orderId === orderResult.orderId).filter(_.vendorId === orderResult.product.productVendorId).result
      } map { vendorMap =>
      vendorMap.foreach( vendorMap1 =>
          if(vendorMap.length == 0){


      var vendorInvoiceRow = VendorInvoiceRow(None,convertOptionLong(orderResult.product.productVendorId),orderResult.orderId,orderResult.product.productVendorName,convertOptionString(orderResult.vendorInvoiceNum),convertOptionString(orderResult.paymentType),convertOptionDouble(orderResult.amountPaid).toFloat,convertOptionalLocalDate(orderResult.paymentDate),convertOptionString(orderResult.checkNumber),vendorMap1.notes,convertOptionString(orderResult.invoice))

     var block = db.run{
       VendorInvoiceTable.returning(VendorInvoiceTable.map(_.id)) += vendorInvoiceRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(vendorInvoiceRow.copy(id = Some(id))))
            }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
          })
      }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    }
    }
  }
    Future(Ok {Json.obj("status"->"success","message"->"Vendor invoice migrated successfully")})
  }


}


