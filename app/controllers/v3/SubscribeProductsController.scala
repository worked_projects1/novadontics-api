
package controllers.v3
import controllers.NovadonticsController
import controllers.v1.OrdersController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffRow,SubscribeProductsRow,OrderRow,OrderItemRow,SubscribeOrderHistoryRow}
import play.api.mvc.Result
import scalaj.http.{Http, HttpOptions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import co.spicefactory.services.EmailService


class SubscribeProductsController @Inject()(ses: AmazonSimpleEmailService, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController  {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  private val replyTo = application.configuration.getString("aws.ses.orderInvoice.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val from = application.configuration.getString("aws.ses.orderInvoice.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val devEmail = application.configuration.getString("devEmail").getOrElse(throw new RuntimeException("Configuration is missing `devEmail` property."))

  private val emailService = new EmailService(ses, application)

case class subscribeRow(
  id: Option[Long],
  staffId: Long,
  productId: Long,
  frequency: String,
  quantity: Long,
  shippingAddressId: Long,
  startDate: LocalDate,
  unsubscribed: Boolean,
  endDate: Option[LocalDate],
  customerPaymentProfileId: Long,
  lastOrderedDate: LocalDate,
  disabled: Boolean,
  shippingCharge: Double,
  salesTax: Double,
  grandTotal: Double,
  productPrice: Double,
  productName: String,
  vendorName: String,
  totalAmountSaved: Double
)

implicit val OperatorFormat = Json.format[subscribeRow]

def getMySubscribedProducts = silhouette.SecuredAction.async { request =>
    var staffId = request.identity.id.get
    db.run {
      SubscribeProductsTable.filter(_.staffId === staffId).filterNot(_.unsubscribed).join(ProductTable).on(_.productId === _.id).join(VendorTable).on(_._2.vendorId === _.id).result
    } map { subscribeMap =>
      val data = subscribeMap.groupBy(_._1._1.id).map {
        case (subscribeId, entries) =>
          val subscribe = entries.head._1._1
          val product = entries.head._1._2
          val vendor = entries.head._2

      var s = Math.round(subscribe.grandTotal * 100.0) / 100.0
          subscribeRow(
            subscribeId,
            subscribe.staffId,
            subscribe.productId,
            subscribe.frequency,
            subscribe.quantity,
            subscribe.shippingAddressId,
            subscribe.startDate,
            subscribe.unsubscribed,
            subscribe.endDate,
            subscribe.customerPaymentProfileId,
            subscribe.lastOrderedDate,
            subscribe.disabled,
            BigDecimal(subscribe.shippingCharge).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble,
            BigDecimal(subscribe.salesTax).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble,
            BigDecimal(subscribe.grandTotal).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble,
            BigDecimal(subscribe.productPrice).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble,
            product.name,
            vendor.name,
            BigDecimal(subscribe.totalAmountSaved).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
          )
      }.toList.sortBy(_.id)
      Ok(Json.toJson(data))
    }
  }

 def create = silhouette.SecuredAction.async(parse.json) { request =>
    var staffId = request.identity.id.get
    var userAgent = request.headers.get("User-Agent")
    val obj = Json.obj()

    val subscribeProducts = for {
      productId <- (request.body \ "productId").validate[Long]
      frequency <- (request.body \ "frequency").validate[String]
      quantity <- (request.body \ "quantity").validate[Long]
      shippingAddressId <- (request.body \ "shippingAddressId").validate[Long]
      customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validate[Long]
      shippingCharge <- (request.body \ "shippingCharge").validate[Float]
      salesTax <- (request.body \ "salesTax").validate[Float]
      grandTotal <- (request.body \ "grandTotal").validate[Float]
      productPrice <- (request.body \ "productPrice").validate[Float]
      totalAmountSaved <- (request.body \ "totalAmountSaved").validate[Float]
      device <- (request.body \ "device").validateOpt[String]


    } yield (SubscribeProductsRow(None, staffId, productId, frequency, quantity, shippingAddressId, LocalDate.now(), false, None, customerPaymentProfileId, LocalDate.now(), false, shippingCharge, salesTax, grandTotal, productPrice,totalAmountSaved),device)
    
    subscribeProducts match {
      case JsSuccess((subscribeRow,device), _) =>

    var subscribeLength = 0
    var lengthBlock = db.run{
      SubscribeProductsTable.filterNot(_.unsubscribed).filter(_.staffId === staffId).filter(_.productId === subscribeRow.productId).result
    } map { subscribeMap =>
      subscribeLength = subscribeMap.length
    }
    var AwaitResult = Await.ready(lengthBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    if(subscribeLength == 0){

    val apiLoginId = application.configuration.getString("aws.authorizeNet.apiLoginId").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiLoginId` property."))
    val transactionKey = application.configuration.getString("aws.authorizeNet.transactionKey").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.transactionKey` property."))
    val apiURL = application.configuration.getString("aws.authorizeNet.apiURL").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiURL` property."))


    var practiceName = ""
    var firstName = ""
    var lastName = ""
    var name: Option[String] = Some("")
    var company = ""
    var address = ""
    var city = ""
    var state = ""
    var zip = ""
    var country = ""
    var phone = ""
    var suite: Option[String] = Some("")
    var customerProfileId: Long = 0
    var productName = ""

   var block =  db.run{
          StaffTable.filter(_.id === staffId).result
        } map { staffMap =>
          staffMap.map { staffResult =>
            customerProfileId = convertOptionLong(staffResult.authorizeCustId)
            firstName = (staffResult.name).substring(0, Math.min((staffResult.name).length(), 50))
            lastName = (convertOptionString(staffResult.lastName)).substring(0, Math.min((convertOptionString(staffResult.lastName)).length(), 50))

    var practiceBlock =  db.run{
          PracticeTable.filter(_.id === staffResult.practiceId).result
        } map { practiceMap =>
          practiceMap.map { practiceResult =>
            practiceName = (practiceResult.name).substring(0, Math.min((practiceResult.name).length(), 50))
          }
        }
      var AwaitResult = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    if(subscribeRow.shippingAddressId == 0){
      var practiceBlock =  db.run{
          PracticeTable.filter(_.id === staffResult.practiceId).result
        } map { practiceMap =>
          practiceMap.map { practiceResult =>
            name = Some(practiceResult.name)
            city = (practiceResult.city).substring(0, Math.min((practiceResult.city).length(), 40))
            state = (practiceResult.state).substring(0, Math.min((practiceResult.state).length(), 40))
            zip = (practiceResult.zip).substring(0, Math.min((practiceResult.zip).length(), 20))
            country = (practiceResult.country).substring(0, Math.min((practiceResult.country).length(), 60))
            address = (practiceResult.address).substring(0, Math.min((practiceResult.address).length(), 60))
            phone = practiceResult.phone
            suite = practiceResult.suite
          }
        }
      var AwaitResult = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  } else {
      var shippingBlock = db.run{
      ShippingTable.filter(_.id === subscribeRow.shippingAddressId).result
    } map { shippingMap=>
      shippingMap.foreach(shippingResult => {
        name = shippingResult.name
        city = shippingResult.city.substring(0, Math.min(firstName.length(), 40))
        state = shippingResult.state.substring(0, Math.min(firstName.length(), 40))
        zip = (shippingResult.zip).substring(0, Math.min(firstName.length(), 20))
        country = (shippingResult.country).substring(0, Math.min(firstName.length(), 60))
        address = (shippingResult.address).substring(0, Math.min(firstName.length(), 60))
        suite = shippingResult.suite
      })
    }
    var AwaitResult = Await.ready(shippingBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    }
    }
  }
   var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))


    var chargeProfileJson = """{ "createTransactionRequest": { "merchantAuthentication": { "name": "strApiLoginId", "transactionKey": "strTransactionkey" }, "transactionRequest": { "transactionType": "authCaptureTransaction", "amount": strGrandTotal, "profile": { "customerProfileId": strCustomerProfileId, "paymentProfile": { "paymentProfileId": strPaymentProfileId } },"order": {"description": "SubscribeOrder"},"shipTo": { "firstName": "strFirstName", "lastName": "strLastName", "company": "strCompany", "address": "strAddress", "city": "strCity", "state": "strState", "zip": "strZip", "country": "strCountry" } } } }"""

    var loginIdReplace = chargeProfileJson.replace("strApiLoginId", apiLoginId)
    var transactionKeyReplace = loginIdReplace.replace("strTransactionkey", transactionKey)
    var customerProfileIdReplace = transactionKeyReplace.replace("strCustomerProfileId", customerProfileId.toString)
    var cityReplace = customerProfileIdReplace.replace("strCity", city)
    var stateReplace = cityReplace.replace("strState", state)
    var zipReplace = stateReplace.replace("strZip", zip)
    var countryReplace = zipReplace.replace("strCountry", country)
    var companyReplace = countryReplace.replace("strCompany", practiceName)
    var addressReplace = companyReplace.replace("strAddress", address)
    var grandTotalReplace = addressReplace.replace("strGrandTotal", subscribeRow.grandTotal.toString)
    var paymentProfileIdReplace = grandTotalReplace.replace("strPaymentProfileId", subscribeRow.customerPaymentProfileId.toString)
    var firstNameReplace = paymentProfileIdReplace.replace("strFirstName", firstName)
    var lastNameReplace = firstNameReplace.replace("strLastName", lastName)

    var finalJson = lastNameReplace

    val response = Http(apiURL).postData(finalJson)
    .header("Content-Type", "application/json")
    .header("Charset", "UTF-8")
    .option(HttpOptions.readTimeout(100000)).asString

    var s = response.body.trim().replaceFirst("\ufeff", "");
    val jsonObject: JsValue = Json.parse(s)

    val transactionResponse = (jsonObject \ ("transactionResponse")).as[JsValue]
    val responseCode = (transactionResponse \ ("responseCode")).as[String]
    val transId = (transactionResponse \ ("transId")).as[String]

    var description = ""
    if(responseCode == "1" || responseCode == "4"){
      val messages = (transactionResponse \ ("messages")).as[List[JsValue]]

      messages.foreach(result => {
        description = (result \ ("description")).as[String]
      })
          // Subscribe Product Create
      var subscribeBlock = db.run {
            SubscribeProductsTable.returning(SubscribeProductsTable.map(_.id)) += subscribeRow
             } map {
                  case 0L => PreconditionFailed
                  case subscriptionId =>

            var vDevice: Option[String] = Some("desktop")
            if(device != None && device != Some("")){
                vDevice = device
            }
            //Order Create
                  var orderAddress = OrderRow.Address(name, country, city, state, zip, address, phone, vDevice, suite)

                    var mckessonOrder = false
                    var block = db.run{
                        VendorTable.filter(_.name === "McKesson").result
                    }map{  vendorList =>
                      vendorList.foreach(vendorResult => {
                        if(vendorResult.id == subscribeRow.productId){
                          mckessonOrder = true
                        }
                      })
                    }
                    val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                  var status: OrderRow.Status = OrderRow.Status.Fulfilled
                  var paymentCompleted = true
                    if(responseCode == "4"){
                      status = OrderRow.Status.Received
                      paymentCompleted = false
                    }
                  var payment = OrderRow.Payment(Some("Credit Card"),Some(false),Some(customerProfileId),Some(subscribeRow.customerPaymentProfileId),Some(transId.toLong),Some(responseCode),Some(paymentCompleted))

                  var orderRow = OrderRow(None, status, DateTime.now, staffId, null, None, Some(orderAddress), Some(""),Some(subscribeRow.shippingCharge),Some(subscribeRow.salesTax),Option.empty[Float],Some(subscribeRow.grandTotal),Some(subscribeRow.totalAmountSaved),userAgent,Some(false),Some(""),Some(""),Some(payment))

            var orderBlock = db.run {
                OrderTable.returning(OrderTable.map(_.id)) += orderRow
                } map {
                  case 0L => PreconditionFailed
                  case id =>


            // Subscribe Order History Create
            var subscribeOrderHistoryRow = SubscribeOrderHistoryRow(None,subscriptionId,LocalDate.now(),id,customerProfileId,subscribeRow.customerPaymentProfileId,transId.toLong,responseCode, subscribeRow.grandTotal)

            var historyblock = db.run {
            SubscribeOrderHistoryTable.returning(SubscribeOrderHistoryTable.map(_.id)) += subscribeOrderHistoryRow
             } map {
                  case 0L => PreconditionFailed
                  case id =>
                  }
            val AwaitResult = Await.ready(historyblock, atMost = scala.concurrent.duration.Duration(120, SECONDS))

            // OrderItem Create
             var block = db.run{
                ProductTable.filter(_.id === subscribeRow.productId).join(CategoryTable).on(_.categoryId === _.id).join(VendorTable).on(_._1.vendorId  === _.id).result
              } map { productsMap =>
                val data = productsMap.groupBy(_._1._1.id).map {
                case (productId, entries) =>
                  val product = entries.head._1._1
                  val category = entries.head._1._2
                  val vendor = entries.head._2

                  var imageUrl = convertOptionString(product.imageUrl).split(',')(0)
                  productName = product.name

                  var orderItemRow =  OrderItemRow((subscribeRow.quantity).toInt,subscribeRow.productId,id,Some(false),Some(false),Some(""),Some(""),None,Some(""),None,None,
                      OrderItemRow.Product(
                        product.name,
                        product.description.getOrElse(""),
                        product.serialNumber,
                        Some(imageUrl),
                        Some(product.vendorId),
                        vendor.name,
                        Some(OrderItemRow.Product.Category(
                        category.id.get,
                        category.name)),
                        subscribeRow.productPrice,
                        product.quantity,
                        product.unit
                      )
                    )

                var block = db.run {
                OrderItemTable.returning(OrderItemTable.map(_.amount)) += orderItemRow
                } map {
                  case 0L => PreconditionFailed
                  case id =>
                }
                val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              // Email Portion
                var emailBlock = db.run{
                    OrderTable.filter(_.id === id)
                    .join(OrderItemTable).on(_.id === _.orderId)
                    .join(StaffTable).on(_._1.staffId === _.id)
                    .join(PracticeTable).on(_._2.practiceId === _.id)
                    .result
                } map { pairs => {
                  (id,convertOptionLong(request.identity.id))

                  val order = OrdersController.Output.Orders.from(pairs).head
                  // val total = order.items.map { i => i.price * i.amount }.sum
                  // val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                  //   item => (item._2, item._2.map(i => i.price * i.amount).sum, item._1)
                  // }.toList.partition(_._3 == "NobelBiocare")
                  val total = order.items.map { i => f"${i.price}%.2f".toDouble * i.amount }.sum
                  val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                    item => (item._2,item._2.map(i => f"${i.price}%.2f".toDouble * i.amount).sum , item._1)
                  }.toList.partition(_._3 == "NobelBiocare")
                  val items = nobel ::: other.sortWith(_._3.capitalize < _._3.capitalize)

                  var dentistName = order.dentist.name
                  var shippingCharge = convertOptionFloat(order.shippingCharge)
                  var tax = convertOptionFloat(order.tax)
                  var creditCardFee = convertOptionFloat(order.creditCardFee)
                  var totalAmountSaved = convertOptionFloat(order.totalAmountSaved)
                  var vendorNoteMap:scala.collection.mutable.Map[Option[Long],String] = scala.collection.mutable.Map()

                  // Send email to the customer
                  emailService.sendEmail(List(s"${dentistName} <${order.dentist.email}>"), from, replyTo, s"Subscription ${subject}", views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, None, order.address, shippingCharge, tax, creditCardFee,totalAmountSaved," subscription","Subscription",vendorNoteMap))

                  // send to admins and assigned email addresses
                  db.run {
                    EmailMappingTable.filter(_.name === "order")
                      .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                      .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                      .result
                  }.map { data =>
                    val defaultAddresses = data.head._1._1.defaultEmail.getOrElse("").split(",").toList
                    val addresses = data.filter(_._2.isDefined).map(_._2.get).map(user => s"${user.name} <${user.email}>").toList ::: defaultAddresses

                    emailService.sendEmail(addresses, from, replyTo, s"New Subscription Order(id: ${order.id}) - ${dentistName}", views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, Some(s""), order.address, shippingCharge, tax, creditCardFee,totalAmountSaved," subscription","Subscription",vendorNoteMap))
                  }
                  // Send email to the vendor
                  db.run {
                    VendorTable.result
                  } map { vendors =>
                      items.foreach(productGroup => {
                          val vendor = vendors.find(_.id == productGroup._1.head.vendorId)
                          if (vendor.isDefined && vendor.get.email.isDefined) {
                            emailService.sendEmail(List(vendor.get.email.get), from, replyTo, s"New Order(id: ${order.id}) - Novadontics", views.html.vendorOrder(total, order.dentist, productGroup, order.address, vendorNoteMap))
                          }
                        })
                    }
                    var sstaffId:Option[Long]=Some(staffId)
                    var block = db.run{
                      CartTable.filter(_.userId === sstaffId).filter(_.productId === product.id).delete.map {
                      case 0L => NotFound
                      case _ => Ok
                        }
                      }
                val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }
                }
                val AwaitResult1 = Await.ready(emailBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              }
              }
              val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
             }
             val AwaitResult1 = Await.ready(orderBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          }
          val AwaitResult1 = Await.ready(subscribeBlock, atMost = scala.concurrent.duration.Duration(120, SECONDS))

          var message = ""
          if(responseCode == "1"){
            message = s"Subscription for ${productName.capitalize} for automatic delivery every ${subscribeRow.frequency} has been successfully created. You can update or unsubscribe using the ${"\"" + "Subscribed Products" + "\""} icon located in the catalog home page. You will receive an email confirmation shortly."
          } else{
            message = s"Subscription for ${productName.capitalize} for automatic delivery every ${subscribeRow.frequency} has been successfully created. You can update or unsubscribe using the ${"\"" + "Subscribed Products" + "\""} icon located in the catalog home page. You will receive an email confirmation shortly. Your payment is pending from Authorize.net. Items will be shipped once the payment is cleared."
          }

        val newObj = obj + ("Result" -> JsString("Success")) + ("message" -> JsString(message))
        Future(Ok{Json.toJson(Array(newObj))})

    } else if(responseCode == "3"){

      val errors = (transactionResponse \ ("errors")).as[List[JsValue]]
      errors.foreach(result => {
        description = (result \ ("errorText")).as[String]
      })

      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString(description))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})

    } else {
      val error = (transactionResponse \ ("errors")).asOpt[List[JsValue]].getOrElse("")
      if(error != ""){
        val errors = (transactionResponse \ ("errors")).as[List[JsValue]]
        errors.foreach(result => {
        description = (result \ ("errorText")).as[String]
        })
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString(description))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else {
      val messages = (transactionResponse \ ("messages")).as[List[JsValue]]
        messages.foreach(result => {
        description = (result \ ("description")).as[String]
        })
        val newObj = obj + ("Result" -> JsString("Success")) + ("message" -> JsString(description))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
      }
    }
    } else {
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("You have already subscribed for this product!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }
      case JsError(_) => Future.successful(BadRequest)
    }

  }

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val subscribeProducts = for {
      // productId <- (request.body \ "productId").validateOpt[Long]
      frequency <- (request.body \ "frequency").validateOpt[String]
      quantity <- (request.body \ "quantity").validateOpt[Long]
      shippingAddressId <- (request.body \ "shippingAddressId").validateOpt[Long]
      customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validateOpt[Long]
      disabled <- (request.body \ "disabled").validateOpt[Boolean]
      shippingCharge <- (request.body \ "shippingCharge").validateOpt[Float]
      salesTax <- (request.body \ "salesTax").validateOpt[Float]
      grandTotal <- (request.body \ "grandTotal").validateOpt[Float]
      productPrice <- (request.body \ "productPrice").validateOpt[Float]
      totalAmountSaved <- (request.body \ "totalAmountSaved").validateOpt[Float]

    } yield {
      (frequency, quantity, shippingAddressId, customerPaymentProfileId, disabled, shippingCharge, salesTax, grandTotal, productPrice, totalAmountSaved)
    }

    subscribeProducts match {
      case JsSuccess((frequency, quantity, shippingAddressId, customerPaymentProfileId, disabled, shippingCharge, salesTax, grandTotal, productPrice, totalAmountSaved), _) => db.run {
        val query = SubscribeProductsTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            frequency.map(value => query.map(_.frequency).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            quantity.map(value => query.map(_.quantity).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            shippingAddressId.map(value => query.map(_.shippingAddressId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            customerPaymentProfileId.map(value => query.map(_.customerPaymentProfileId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            disabled.map(value => query.map(_.disabled).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            shippingCharge.map(value => query.map(_.shippingCharge).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            salesTax.map(value => query.map(_.salesTax).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            grandTotal.map(value => query.map(_.grandTotal).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            productPrice.map(value => query.map(_.productPrice).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            totalAmountSaved.map(value => query.map(_.totalAmountSaved).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def unsubscribeProduct(id: Long) = silhouette.SecuredAction.async { request =>
       db.run {
         SubscribeProductsTable.filter(_.id === id).map(_.unsubscribed).update(true)
        } map {
        case 0 => NotFound
        case _ => Ok
        }
  }
}


