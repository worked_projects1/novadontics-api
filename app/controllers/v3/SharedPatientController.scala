
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{SharedPatientRow, StaffRow,FormRow,TreatmentRow,FriendReferralRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import co.spicefactory.services.EmailService

class SharedPatientController @Inject()(ses: AmazonSimpleEmailService,silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


implicit val SharedPatientFormat = Json.format[SharedPatientRow]
implicit val FriendReferralFormat = Json.format[FriendReferralRow]

implicit val shareToAdminRowWrites = Writes { request: (TreatmentRow) =>
  val (shareToAdminList) = request

    Json.obj(
        "shareToAdmin" -> shareToAdminList.shareToAdmin,
        "mainSquares" -> shareToAdminList.mainSquares,
        "icons" -> shareToAdminList.icons,
        "sharedDate" -> shareToAdminList.sharedDate
      )
    }

case class DoctorsListRow(
  staffId: Long,
  name : String
  )

  implicit val doctorListRowWrites = Writes { doctorsListRecord: (DoctorsListRow) =>
    val (doctorsList) = doctorsListRecord

    Json.obj(
        "staffId" -> doctorsList.staffId,
        "name" -> doctorsList.name
      )
    }

case class MergeListRow(
  title: String,
  value: String
)

implicit val MergeTypeWrites = Writes { MergeListRecord: (MergeListRow) =>
    val (mergeListRecord) = MergeListRecord

  Json.obj(
      "title" -> mergeListRecord.title,
      "value" -> mergeListRecord.value
      )
  }

case class ShareListRow(
  id: Long,
  patientId : Long,
  practiceId: Long,
  practiceName: String,
  sharedTo: Long,
  staffName: String,
  sharedBy: Long,
  sharedDate: LocalDate,
  mainSquares: String,
  icons: String,
  accessTo: String,
  accessLevel: String
  )

  implicit val shareListRowWrites = Writes { shareListRecord: (ShareListRow) =>
    val (shareList) = shareListRecord

    Json.obj(
        "id" -> shareList.id,
        "patientId" -> shareList.patientId,
        "practiceId" -> shareList.practiceId,
        "practiceName" -> shareList.practiceName,
        "shareTo" -> shareList.sharedTo,
        "staffName" -> shareList.staffName,
        "sharedBy" -> shareList.sharedBy,
        "sharedDate" -> shareList.sharedDate,
        "mainSquares" -> shareList.mainSquares,
        "icons" -> shareList.icons,
        "accessTo" -> shareList.accessTo,
        "accessLevel" -> shareList.accessLevel
      )
    }

 implicit val mainSquareRowWrites = Writes { request: (FormRow) =>
    val (mainSquareRow) = request
  
  Json.obj(
      "id" -> mainSquareRow.id,
      "title" -> mainSquareRow.title
      )
  }

  private val replyTo = application.configuration.getString("aws.ses.signUpRequest.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.signUpRequest.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.subject` property."))
  private val from = application.configuration.getString("aws.ses.signUpRequest.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.from` property."))
  private val devEmail = application.configuration.getString("devEmail").getOrElse(throw new RuntimeException("Configuration is missing `devEmail` property."))

private val emailService = new EmailService(ses, application)


   def sharedList(patientId: Long) = silhouette.SecuredAction.async { request =>

     db.run {
          SharedPatientTable.filter(_.patientId === patientId).join(StaffTable).on(_.shareTo === _.id).join(PracticeTable).on(_._1.practiceId === _.id).result
      } map { sharedPatient =>
      val data = sharedPatient.groupBy(_._1._1.id).map {
        case (shareId, entries) =>
        val sharedPatient = entries.head._1._1
        val staff = entries.head._1._2
        val practice = entries.head._2

        val staffName = staff.name +" "+convertOptionString(staff.lastName)

        ShareListRow( 
          convertOptionLong(shareId),
          sharedPatient.patientId,
          sharedPatient.practiceId,
          practice.name,
          sharedPatient.shareTo,
          staffName,
          sharedPatient.sharedBy,
          sharedPatient.sharedDate,
          sharedPatient.mainSquares,
          sharedPatient.icons,
          sharedPatient.accessTo,
          sharedPatient.accessLevel
        )
      }.toList        
        Ok(Json.toJson(data))
      }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
 var staffId = request.identity.id.get
 var patientAlreadyShared = false
 var practiceId: Long = 0
 var dentistName = ""
 var email = ""
 var shareToDentistName = ""
 var patientName = ""
    val sharedPatient = for {
      patientId <- (request.body \ "patientId").validate[Long]
      shareTo <- (request.body \ "shareTo").validate[Long]
      mainSquares <- (request.body \ "mainSquares").validateOpt[String]
      icons <- (request.body \ "icons").validateOpt[String]
      accessTo <- (request.body \ "accessTo").validate[String]
      accessLevel <- (request.body \ "accessLevel").validate[String]
    } yield {
      var block = db.run{
            StaffTable.filter(_.id === shareTo).map(_.practiceId).result
      }map{staffResult =>  staffResult.map(s => practiceId = s )
        Ok
      }

      var block2 = db.run{
        SharedPatientTable.filter(_.shareTo === shareTo).filter(_.patientId === patientId).length.result
      } map {shareResult =>
          if(shareResult > 0){
              patientAlreadyShared = true
          }
        Ok
      }
    var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(10, SECONDS))
    var AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(10, SECONDS))
     var block3 = db.run{
            StaffTable.filter(_.id === staffId).result
      }map{staffResult =>  
      staffResult.map{result =>
      dentistName = result.title +" "+ result.name +" "+ convertOptionString(result.lastName)
      
        }
      }
      var AwaitResult3 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(10, SECONDS))
      var block4 = db.run{
            TreatmentTable.filter(_.id === patientId).result
      }map{patientResult =>  
      patientResult.map{result =>
      patientName = convertOptionString(result.firstName) + " " + convertOptionString(result.lastName)
        }
      }
      var AwaitResult4 = Await.ready(block4, atMost = scala.concurrent.duration.Duration(10, SECONDS))
      var block5 = db.run{
            StaffTable.filter(_.id === shareTo).result
      }map{patientResult =>  
      patientResult.map{result =>
      email = result.email
      shareToDentistName = result.title +" "+ result.name +" "+ convertOptionString(result.lastName)
      }}
      var AwaitResult5 = Await.ready(block5, atMost = scala.concurrent.duration.Duration(10, SECONDS))
    SharedPatientRow(None, patientId,practiceId,shareTo,staffId,LocalDate.now(),convertOptionString(mainSquares),convertOptionString(icons),accessTo,accessLevel)
    }
    sharedPatient match {
      case JsSuccess(sharedPatientRow, _) =>
      if(patientAlreadyShared){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("This patient already shared for selected dentist. Please choose another dentist"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
      }else{
        // Send email to the user2(shareTo)
        emailService.sendEmail(List(s"<${email}>"), from, replyTo, "Novadontics Shared Care", views.html.sharedCare(shareToDentistName,dentistName,patientName))
      db.run{
       SharedPatientTable.returning(SharedPatientTable.map(_.id)) += sharedPatientRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(sharedPatientRow.copy(id = Some(id))))
          }
          
      }
      case JsError(_) => Future.successful(BadRequest)
    }

  }

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val providers = for {
      mainSquares <- (request.body \ "mainSquares").validateOpt[String]
      icons <- (request.body \ "icons").validateOpt[String]
      accessTo <- (request.body \ "accessTo").validateOpt[String]
      accessLevel <- (request.body \ "accessLevel").validateOpt[String]

    } yield {

      (mainSquares,icons,accessTo,accessLevel)
    }

    providers match {
      case JsSuccess((mainSquares,icons,accessTo,accessLevel), _) => db.run {
        val query = SharedPatientTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            mainSquares.map(value => query.map(_.mainSquares).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            icons.map(value => query.map(_.icons).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            accessTo.map(value => query.map(_.accessTo).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            accessLevel.map(value => query.map(_.accessLevel).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

  def getDoctors = silhouette.SecuredAction.async { request =>
  var staffId = request.identity.id.get
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
          StaffTable.filterNot(_.practiceId === practiceId).filter(s=> s.title === "Dr." || s.title === "CDT").join(PracticeTable).on(_.practiceId === _.id).result
      } map { staffResult =>

      val data = staffResult.groupBy(_._1.id).map {
        case (staffId, entries) =>
        val staff = entries.head._1
        val practice = entries.head._2
        val practiceName = practice.name
        val name = staff.name +" "+convertOptionString(staff.lastName) +s"($practiceName)"
        DoctorsListRow( 
          convertOptionLong(staff.id),
          name
        )
      
      }.toList.sortBy(_.staffId)       
      Ok(Json.toJson(data))
      
      }
  }  

  def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      SharedPatientTable.filter(_.id === id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

  def mainSquareList = silhouette.SecuredAction.async { request =>

     db.run {
	FormTable.filter(_.types === "mainSquare").sortBy(_.listOrder).result
      } map { forms =>
        Ok(Json.toJson(forms))
      }
  }  

def shareToAdminList(patientId: Long) = silhouette.SecuredAction.async { request =>
     db.run {
          TreatmentTable.filter(_.id === patientId).result
      } map { sharedPatient =>
        Ok(Json.toJson(sharedPatient))
      }
  }



  def mainSquareIconList = silhouette.SecuredAction.async { request =>
  var mergeList = List.empty[MergeListRow]

    db.run {
          ListOptionTable.filter(_.listType === "icons").result
      } map { listMap =>
        listMap.foreach(result => {
         var dataList =  MergeListRow(result.listOptions,result.listValue)
         mergeList = mergeList :+ dataList
        })

     var block = db.run {
	      FormTable.filter(_.types === "mainSquare").sortBy(_.listOrder).result
      } map { formsMap =>
        formsMap.foreach(result => {
          var dataList =  MergeListRow(result.title,result.id)
          mergeList = mergeList :+ dataList
        })
      }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      Ok(Json.toJson(mergeList))
      }
  }

def createFriendReferral = silhouette.SecuredAction.async(parse.json) { request =>
var staffId = request.identity.id.get
var dentistName = ""

      val friendReferral = for {
        title <- (request.body \ "title").validate[String]
        firstName <- (request.body \ "firstName").validate[String]
        lastName <- (request.body \ "lastName").validate[String]
        practiceName <- (request.body \ "practiceName").validateOpt[String]
        phone <- (request.body \ "phone").validate[String]
        email <- (request.body \ "email").validateOpt[String]
        note <- (request.body \ "note").validateOpt[String]
      } yield {
      FriendReferralRow(None,title,firstName,lastName,convertOptionString(practiceName),phone,convertOptionString(email),note,Some(staffId))
    }
    friendReferral match {
      case JsSuccess((friendReferralRow), _) =>
      var block = db.run {
        StaffTable.filter(_.id === staffId).result
      } map { staffResult =>
        staffResult.map{ staff =>
          dentistName = staff.name + " " + convertOptionString(staff.lastName)
        }
      }
      var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(10, SECONDS))
      // send to admins and assigned email addresses
                 var block2 =  db.run {
                    EmailMappingTable.filter(_.name === "signupRequest")
                      .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                      .result
                  }.map { data =>
                    val defaultAddresses = data.head._1.defaultEmail.getOrElse("").split(",").toList
                    var friendName = friendReferralRow.title + " " + friendReferralRow.firstName + " " +friendReferralRow.lastName
                    emailService.sendEmail(defaultAddresses, from, replyTo, s"${dentistName} referred a freind", views.html.friendReferral(dentistName,friendName,friendReferralRow.practiceName,friendReferralRow.phone,friendReferralRow.email,convertOptionString(friendReferralRow.note)))
                  }
          var AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(10, SECONDS))
          db.run {
            FriendReferralTable.returning(FriendReferralTable.map(_.id)) += friendReferralRow
          } map {
            case 0L => PreconditionFailed
            case id => 
        Created(Json.toJson(friendReferralRow.copy(id = Some(id))))
         }
    case JsError(_) => Future.successful(BadRequest)
    }
}

 def migrateAccessTo = silhouette.SecuredAction.async(parse.json) { request =>

    db.run {
      for{
          sharedPatient <- SharedPatientTable.result
      } yield {
            (sharedPatient).foreach( result => {
              var sAccessTo = ""
                if(result.accessTo.contains("allergies")){
                  if(result.accessTo.contains("medicalConditions")){
                    sAccessTo = result.accessTo.replaceAll("allergies,","").replaceAll(",allergies","").replaceAll("allergies", "")
                  } else {
                    sAccessTo = result.accessTo.replaceAll("allergies","medicalConditions")
                  }
                  db.run{
                    SharedPatientTable.filter(_.id === result.id).map(_.accessTo).update(sAccessTo)
                  }
                }
            })
        Ok {Json.obj("status"->"success","message"->"Allergies Migrated Successfully")}
        }
     }
  }

  def readAll = silhouette.SecuredAction.async { request =>
      db.run {
            FriendReferralTable.result } map { friendReferral =>
        Ok(Json.toJson(friendReferral))
      }
  }
}


