
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{NonWorkingDaysRow,StaffRow}
import play.api.mvc.Result
import org.joda.time.{ LocalTime, LocalDate }
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


class NonWorkingDaysController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


  implicit val NonWorkingDaysFormat = Json.format[NonWorkingDaysRow]

 implicit val nonWorkingDaysRowWrites = Writes { request: (NonWorkingDaysRow) =>
    val (nonWorkingRow) = request

  Json.obj(
      "id" -> nonWorkingRow.id,
      "practiceId" -> nonWorkingRow.practiceId,
      "date" -> nonWorkingRow.date
      )
  }

 
 def readAll = silhouette.SecuredAction.async { request =>
    db.run {
        NonWorkingDaysTable.filter(_.practiceId === request.identity.asInstanceOf[StaffRow].practiceId).sortBy(_.date).result
    } map { nonWorkingList =>
      Ok(Json.toJson(nonWorkingList))
    }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var currentDate: LocalDate = LocalDate.now()
 var nonWorkingDate: Boolean = false
 var alreadyAppbooked: Boolean = false

    val providers = for {

      date <- (request.body \ "date").validate[LocalDate]
      
    } yield NonWorkingDaysRow(None, practiceId, date)

    providers match {
      case JsSuccess(nonWorkingRow, _) =>

      val count = db.run{
          for {
               dateLength <- NonWorkingDaysTable.filter(_.practiceId === nonWorkingRow.practiceId).filter(_.date === nonWorkingRow.date).length.result

              appointmentLength <- AppointmentTable.filterNot(_.deleted).filterNot(s => s.status.toLowerCase === "canceled" || s.status.toLowerCase === "completed" || s.status.toLowerCase === "no show")
               .filter(_.practiceId === practiceId).filter(_.date >= currentDate).filter(_.date === nonWorkingRow.date).length.result
            }
            yield {

            if (dateLength>0){
                nonWorkingDate = true
            }

            if(appointmentLength > 0){
              alreadyAppbooked = true
            }
         }
      }
      var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(nonWorkingDate){

        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("This date is already marked as non working day!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else if(alreadyAppbooked){
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Some appointments already scheduled on this date. Please reschedule those appointments and try again!"))
        Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else {
        db.run{
          NonWorkingDaysTable.returning(NonWorkingDaysTable.map(_.id)) += nonWorkingRow
           } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(nonWorkingRow.copy(id = Some(id))))
            }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val currentDate: LocalDate = LocalDate.now()
    var alreadyAppbooked: Boolean = false
    val providers = for {
       practiceId <- (request.body \ "practiceId").validateOpt[Long]
       date <- (request.body \ "date").validateOpt[LocalDate]
    } yield {
      (practiceId, date)
    }

    providers match {
      case JsSuccess((practiceId, date), _) =>

       var block = db.run{
         AppointmentTable.filterNot(_.deleted).filterNot(s => s.status.toLowerCase === "canceled" || s.status.toLowerCase === "completed" || s.status.toLowerCase === "no show")
         .filter(_.practiceId === practiceId).filter(_.date >= currentDate).filter(_.date === date).length.result
        }map{ result =>
        if(result > 0){
          alreadyAppbooked = true
        }
      }
      var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(alreadyAppbooked) {
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Appointment already booked for this date!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})

      } else {
        db.run {
        val query = NonWorkingDaysTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            practiceId.map(value => query.map(_.practiceId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            date.map(value => query.map(_.date).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

   def delete(id: Long) = silhouette.SecuredAction.async { request =>
      db.run{
        NonWorkingDaysTable.filter(_.id === id).delete map {
          case 0 => NotFound
          case _ => Ok
        }
      }
    }
  
}


