package controllers.v3

import com.typesafe.config.{Config, ConfigFactory}
import scala.io.StdIn.readLine
import com.twilio.Twilio
import com.twilio.`type`.PhoneNumber
import com.twilio.rest.api.v2010.account.Message
import com.mohiva.play.silhouette.api.Silhouette
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import co.spicefactory.util.BearerTokenEnv
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import play.api.{Application, Logger}
import com.google.inject.name.Named
import com.google.inject._
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.json.Json.{obj, toJson}
import play.api.libs.json._
import controllers.NovadonticsController
import scala.util.{Failure, Success, Try}
import models.daos.tables.{ StaffRow,AppointmentRow, AppointmentReminderTrackRow, StepRow }
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import co.spicefactory.services.EmailService
import java.text.SimpleDateFormat
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.format.{DateTimeFormatter, DateTimeFormatterBuilder}
import org.joda.time.ReadablePartial;

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._
import org.json.JSONObject
import org.json.JSONArray
import play.api.libs.json._
import scala.util.parsing.json._


import java.security.MessageDigest
import java.util
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.codec.binary.Base64
import com.twilio.Twilio
import com.twilio.rest.verify.v2.Service;
import com.twilio.rest.verify.v2.service.Verification
import com.twilio.rest.verify.v2.service.VerificationCheck
import scala.util.control.Breaks._
import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer

@Singleton
class PatientModuleController @Inject()(ses: AmazonSimpleEmailService , silhouette : Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext , val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._
  import java.net.{URLDecoder, URLEncoder}
  implicit val productFormat = Json.format[AppointmentReminderTrackRow]
  private val emailService = new EmailService(ses, application)
  private val config = ConfigFactory.load()
  private val patientSiteBaseUrl = application.configuration.getString("aws.ses.appointmentReminder.patientSiteBaseUrl").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.patientSiteBaseUrl` property."))

 def  getPEK = silhouette.UnsecuredAction.async {request => 

  val keyValue = application.configuration.getString("encodeKey").getOrElse(throw new RuntimeException("Configuration is missing `encodeKey` property."))

  def encodeKey(str: String): String = {
        var encoded: Array[Byte] = Base64.encodeBase64(str.getBytes());
        return new String(encoded);
  }

  var encodedBase64Key: String  = encodeKey(keyValue);

  Future(Ok{Json.toJson(encodedBase64Key)})
}

def reminderResponse(id: Long) = silhouette.UnsecuredAction.async(parse.json) { request =>

  val obj = Json.obj()
  val emailService = new EmailService(ses, application)
  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))

    val reminderResponse = for {
      status <- (request.body \ "status").validate[String]
      note <- (request.body \ "other").validateOpt[String]
      reason <- (request.body \ "reason").validateOpt[String]
    } yield {
      (status, note, reason)
    }

  reminderResponse match {
      case JsSuccess((status,note,reason), _) => 
        val b = db.run{
          var query = AppointmentTable.filter(_.id === id)
                 query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](
                Some(status).map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>     

              var appointmentReminderTrackRow = AppointmentReminderTrackRow(None,id,DateTime.now,"response received",status,convertOptionString(note),convertOptionString(reason))  
              db.run {
                AppointmentReminderTrackTable.returning(AppointmentReminderTrackTable.map(_.id)) += appointmentReminderTrackRow              
              } map {
                case 0L => PreconditionFailed
                case id => Created(Json.toJson(appointmentReminderTrackRow.copy(id = Some(id))))
              }
             Ok
            }
            case false =>
              DBIO.successful(NotFound)
          }                   
        } 
       Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))

       db.run{
            AppointmentTable.filter(_.id === id).join(PracticeTable).on(_.practiceId === _.id).join(ProviderTable).on(_._1.providerId === _.id).result
          }map{ resultData =>
          
          resultData.map{row =>

          var toAddress = convertOptionString(row._1._2.email)
          val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
          val outputTimeFormat = new SimpleDateFormat("hh:mm a")
          val inputStartTime = inputTimeFormat.parse(row._1._1.startTime.toString)
          val outputStartTime = outputTimeFormat.format(inputStartTime)
          val providerName = row._2.title + row._2.firstName + row._2.lastName

          var content = ""
          var subject = ""

            if(status.toLowerCase == "confirmed"){
               content = "The patient has confirmed the above appointment."
               subject = "Appointment is Confirmed"
            }else if(status == "Need to reschedule"){
               content = "The patient has requested to reschedule the appointment date for the below reason."
               subject = "Request for Reschedule"
            }else if(status.toLowerCase == "canceled"){
               content = "The patient has canceled the appointment for the below reason."
               subject = "Appointment is Cancelled"
            }

            emailService.sendEmail(List(toAddress), from, replyTo, subject, views.html.reminderResponse(providerName,row._1._1.patientName,row._1._1.date.toString,outputStartTime.toString,content,convertOptionString(reason),convertOptionString(note)))

          }

        val newObj = obj + ("actionStatus" -> JsString("success")) + ("appointmentStatus" -> JsString(status))
        Ok{Json.toJson(newObj)}
          }

      case JsError(_) => Future.successful(BadRequest)
    }    
  }


def updatePdfUrl() = silhouette.UnsecuredAction.async(parse.json) { request =>
  var patientId = 0L
  var consentName = ""
  var formName = ""
  var pdfUrl = ""
  val formId = "1AL"
  var resultString = ""
  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))

  val inputJson = for {
      pId <- (request.body \ "patientId").validate[Long]
      cName <- (request.body \ "consentName").validate[String]
      url <- (request.body \ "pdfUrl").validate[String]
      formTitle <- (request.body \ "formTitle").validate[String]
    } yield {
      patientId = pId
      consentName = cName
      pdfUrl = url
      formName = formTitle
    }

    db.run {
            StepTable.filter(_.treatmentId === patientId)
            .filter(_.formId === "1AL").sortBy(_.dateCreated.desc).result
          } map { stepRows =>
               var obj = new JSONObject()
               if(stepRows.size != 0) {
               var stepRow = stepRows.head
                var jsonString: String = stepRow.value.toString
                obj = new JSONObject(jsonString)
                var consent = obj.has(consentName)

                if(consent) {
                    var consentObject = new JSONObject(obj.getString(consentName).replaceAll("★", "\""))
                    
                     if (consentObject.has("images")){
                      var imagesArray = consentObject.getJSONArray("images")
                      imagesArray.put(pdfUrl)
                      consentObject.put("images",imagesArray)
                    } else {
                      var imgArray = new JSONArray()
                      imgArray.put(pdfUrl)
                      consentObject.put("images",imgArray)
                    }

                    if (consentObject.has("checked")){
                      consentObject.put("checked",true)
                    } else {
                      consentObject.put("checked",true)
                    }

                    var consobjStr = consentObject.toString.replaceAll("\"", "★")
                    obj.put(consentName, consobjStr)
                    resultString = "Consent Updated"
                } else {
                  var consentObject = new JSONObject()

                  var imagesArray = new JSONArray()
                  imagesArray.put(pdfUrl)
                  consentObject.put("images", imagesArray)
                  consentObject.put("checked", true)

                  obj.put(consentName, consentObject.toString.replaceAll("\"", "★"))
                  resultString = "Consent Created"
                }
            } else {
              var consentObject = new JSONObject()
              var imagesArray = new JSONArray()
              imagesArray.put(pdfUrl)
              consentObject.put("images", imagesArray)
              consentObject.put("checked", true)
              obj.put(consentName, consentObject.toString.replaceAll("\"", "★"))
              resultString = "Consent Created"
            }
            var jsValueObj: JsValue = Json.parse(obj.toString)
            var tb = db.run {
                 StepTable += StepRow(jsValueObj,StepRow.State.Completed, patientId, "1AL", 1, DateTime.now)
               }
                val AwaitResult = Await.ready(tb, atMost = scala.concurrent.duration.Duration(20, SECONDS))
       
                var tBlock = db.run {
                      TreatmentTable.filterNot(_.deleted).filter(_.id === patientId).result.head
                    } map { result => 
                       
                          var vPracticeId = convertOptionLong(result.practiceId)
                          var patientName = convertOptionString(result.firstName) +" "+ convertOptionString(result.lastName)

                          var practiceBlock = db.run {
                            PracticeTable.filter(_.id === vPracticeId).result.head
                          } map { result => 
                            var emailContent = "Hi,\n" + patientName + " signed and submitted the below " + formName + "\n" + "A  copy of the completed form is already posted in the patient file." +pdfUrl
                            var subject = "Confirmation Email for the " + formName
                            emailService.sendReminder(List(convertOptionString(result.email)), from, replyTo, subject, emailContent)
                          }
                          val Await1 = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(20, SECONDS))


                    }
                    val Await1 = Await.ready(tBlock, atMost = scala.concurrent.duration.Duration(20, SECONDS))

          Ok(Json.toJson(resultString))
        }
    }

def sendConsentFormLink(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  
  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw  new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))
  var response = ""
  var status = ""
  var apiResponse = ""

  val inputJson = for {
      actionType <- (request.body \ "actionType").validate[String]
      formTitle <- (request.body \ "formTitle").validate[String]
      consentName <- (request.body \ "consentName").validate[String]
    } yield {
      (actionType,formTitle,consentName)
    }
  inputJson match {
      case JsSuccess((actionType,formTitle,consentName), _) =>
            val b = db.run {
              TreatmentTable.filterNot(_.deleted).filter(_.id === patientId)
              .join(PracticeTable).on(_.practiceId === _.id)
              .joinLeft(ProviderTable).on(_._1.providerId === _.id)
              .result
            } map { resultData =>
              
            resultData.foreach(data => {
              val treatmentsData = data._1._1
              val practiceData = data._1._2

              if(convertOptionalBoolean(practiceData.onlineRegistrationModule)){
              val providerData = data._2

              if(providerData != None){
              var patientName = convertOptionString(treatmentsData.firstName) +" "+ convertOptionString(treatmentsData.lastName)
              var providerName = "&providerFirstName=" + providerData.get.title + providerData.get.firstName + "&providerLastName=" + providerData.get.lastName
              var enStr = "patientId="+ patientId + providerName
              var encryptedString = encryptMethod(enStr)
              val temp = consentName.startsWith("consent")
              var consentNameTemp = consentName
              if(!temp){
                  consentNameTemp = "consent" + consentName
              }
              var webFormLink = patientSiteBaseUrl + "/" + consentNameTemp + "/" + encryptedString
              var messageContent = convertOptionString(practiceData.consentFormMessage)
              messageContent = messageContent.replace("<<form name>>", formTitle).replace("<<link>>", webFormLink).replace("<<patient name>>", patientName)

            if(actionType == "SMS") {
                response = sendSMS(treatmentsData.phoneNumber, messageContent, convertOptionString(treatmentsData.country))
                if(response == "Message Sent"){
                  status = "Success"
                  apiResponse = "Consent form link sent to your patient through SMS"
                 }else{
                  status = "Failure"
                  apiResponse = response
                 }
              } else if (actionType == "Email"){
                emailService.sendReminder(List(treatmentsData.emailAddress), from, replyTo, formTitle, messageContent)
                status = "Success"
                apiResponse = "Consent form link sent to your patient through Email"
              } else {
                status = "Failure"
                apiResponse = "Action type is incorrect"
              }
              }else{
                status = "Failure"
                apiResponse = "Please select primary provider for this patient in Personal Information / Insurance / Health History form"
              }
            }else{
              status = "Failure"
              apiResponse = "Online registration module is not enabled. To enable please send mail to team@novadontics.com"
            }
            }) 
            }
            Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    val obj = Json.obj()
    if(status == "Success"){
    val newObj = obj + ("status" -> JsString(status)) + ("response" -> JsString(apiResponse))
    Future(Ok{Json.toJson(Array(newObj))})
    }else{
    val newObj = obj + ("status" -> JsString(status)) + ("response" -> JsString(apiResponse))
    Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }
       
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def resendLink(patientId: Long) = silhouette.SecuredAction.async { request =>

    var smsResponse = ""
    var a = db.run {
      TreatmentTable.filter(_.id === patientId).filterNot(_.deleted)
      .join(PracticeTable).on(_.practiceId === _.id).result
    } map { resultData =>
      resultData.foreach { data =>
        val treatmentsTableData = data._1
        val practiceTableData = data._2
        val emailService = new EmailService(ses, application)
        var patientName = convertOptionString(treatmentsTableData.firstName) +" "+ convertOptionString(treatmentsTableData.lastName)
        val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
        val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))

        var stringToEncrypt = "patientId=" + convertOptionLong(treatmentsTableData.id)
        var registrationFormLink = patientSiteBaseUrl + "/onlineRegistration/" + encryptMethod(stringToEncrypt)
        
        var subject = "Online Registration form"
        var patientPhoneNumber = treatmentsTableData.phoneNumber
        var patientEmailAddr = treatmentsTableData.emailAddress
        var msgContent = convertOptionString(practiceTableData.onlineInviteMessage)
        var patientCountry = convertOptionString(treatmentsTableData.country)
        msgContent = msgContent.replaceAll("<<link>>", registrationFormLink).replaceAll("<<patient name>>", patientName)
        if(msgContent != "" && patientPhoneNumber != "" && patientEmailAddr != "") {
          smsResponse = sendSMS(patientPhoneNumber,msgContent,patientCountry)
          emailService.sendReminder(List(patientEmailAddr), from, replyTo,subject, msgContent)
          var tb = db.run{
                TreatmentTable.filter(_.id === patientId).map(_.lastInvitationSent).update(Some(DateTime.now)) 
             }
          val AwaitResult = Await.ready(tb, atMost = scala.concurrent.duration.Duration(20, SECONDS)) 
        }
    }
    }
    val AwaitResult1 = Await.ready(a, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    val obj = Json.obj()

    if(smsResponse == "Message Sent"){
    val newObj = obj + ("status" -> JsString("Success")) + ("response" -> JsString(smsResponse))
    Future(Ok{Json.toJson(Array(newObj))})
    }else{
    val newObj = obj + ("status" -> JsString("Failure")) + ("response" -> JsString(smsResponse))
    Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }

    }

    def migrateTreatmentCountry() = silhouette.SecuredAction.async { request =>
      var block = db.run {
        TreatmentTable.filterNot(_.deleted).result
        } map { treatmentResult =>
          treatmentResult.foreach(treatment => {
          var block = db.run{
                StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
                  } map { stepResult =>
                  var country =  (stepResult.value \ "country").as[String]
                   var block1 = db.run {
                    TreatmentTable.filter(_.id === treatment.id).map(_.country).update(Some(country))
                    } map {
                        case 0L => PreconditionFailed
                        case id => Ok
                      }
                   val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                  }
        })
        }
        val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        Future (Ok{Json.obj( "status"->"success","message"->"Country Migrated successfully")})
      }


      def sendOtp(patientId: Long) = silhouette.UnsecuredAction.async { request =>
        val ACCOUNT_SID = config.getString("twilio.account_sid")
        val AUTH_TOKEN = config.getString("twilio.auth_token")
        var patientMobileNumber = ""
        var country = ""
        var phoneNumberEndsWith = ""
          
          var block = db.run {
            TreatmentTable.filterNot(_.deleted).filter(_.id === patientId).result.head
          } map { result => 
            patientMobileNumber = result.phoneNumber
            country = convertOptionString(result.country)
            patientMobileNumber = getCountryIdByCountryName(country) + patientMobileNumber
            phoneNumberEndsWith = patientMobileNumber.slice(patientMobileNumber.length - 3, patientMobileNumber.length)
          }
        val Await1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(20, SECONDS))

        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        var service: Service = Service.creator("Novadontics").create();
        var verification: Verification = Verification.creator(
                    service.getSid,
                    patientMobileNumber,
                    "sms")
                    .create();
      
        Future(Ok {Json.obj(
          "serviceId" -> service.getSid,
          "patientMobileNumber" -> phoneNumberEndsWith
          )})
  }

  def verifyOtp(patientId: Long, serviceId: String, otp: String) = silhouette.UnsecuredAction.async { request =>
    var patientMobileNumber = ""
    var country = ""

    var block = db.run {
          TreatmentTable.filterNot(_.deleted).filter(_.id === patientId).result.head
        } map { result => 
          patientMobileNumber = result.phoneNumber
          country = convertOptionString(result.country)
          patientMobileNumber = getCountryIdByCountryName(country) + patientMobileNumber
        }
    Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    var verificationCheck: VerificationCheck = VerificationCheck.creator(
            serviceId, otp)
          .setTo(patientMobileNumber).create();
    
    Future(Ok {Json.obj(
              "status" -> verificationCheck.getStatus
            )})
  }


  def migrateStepTableCountry() = silhouette.SecuredAction.async { request =>
     

    def getOriginalCountryName(countryName: String): String = {
      var countryMap: Map[String,String] = Map()
      countryMap.update("us", "USA")
      countryMap.update("usa", "USA")
      countryMap.update("united states", "USA")
      countryMap.update("united states of america", "USA")
      countryMap.update("india", "India")
      countryMap.update("canada", "Canada")
      countryMap.update("australia", "Australia")
      countryMap.update("england", "England")
      var originalCountry = countryName
      if(countryMap.keySet.contains(countryName)) {
        originalCountry = countryMap(countryName)
      } else {
        originalCountry = "USA"
      }

      return originalCountry
    }

    var exceptMap: ListBuffer[String] = ListBuffer()
    exceptMap += "Bangladesh"
    exceptMap += "CHINA/bangladesh"
    exceptMap += "Germany"

    var block = db.run {
        TreatmentTable.filterNot(_.deleted).result
        } map { treatmentResult =>
          treatmentResult.foreach(treatment => {
            breakable {
            var block = db.run{
              StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
           } map { stepResult =>
              var country =  (stepResult.value \ "country").as[String]
              
              if(country == null || country == None || country == "" || exceptMap.contains(country)) {
                break
              } else {
                var updatedCountry = getOriginalCountryName(country.toLowerCase)
                var jsonValue = new JSONObject(stepResult.value.toString)
                jsonValue.put("country", updatedCountry)
                var jsValue = Json.parse(jsonValue.toString)
                var tb = db.run {
                   StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A")
                   .filter(_.dateCreated === stepResult.dateCreated).map(_.value)
                  .update(jsValue)
                }
                val AwaitResult = Await.ready(tb, atMost = scala.concurrent.duration.Duration(60, SECONDS))
              }
            }
          }
        })
      }
      val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
   Future (Ok{Json.obj( "status"->"success","message"->"Country Migrated successfully")})
   }

  def sendInstructions(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  
  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw  new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  var response = ""
  var status = ""
  var apiResponse = ""

  val inputJson = for {
      actionType <- (request.body \ "actionType").validate[String]
      formTitle <- (request.body \ "formTitle").validate[String]
      docLink <- (request.body \ "docLink").validate[String]
    } yield {
      (actionType,formTitle,docLink)
    }
  inputJson match {
      case JsSuccess((actionType,formTitle,docLink), _) =>
            val b = db.run {
              TreatmentTable.filterNot(_.deleted).filter(_.id === patientId)
              .join(PracticeTable).on(_.practiceId === _.id)
              .result
            } map { resultData =>
              
            resultData.foreach(data => {
              val treatmentsData = data._1
              val practiceData = data._2
              
              if(convertOptionalBoolean(practiceData.onlineRegistrationModule)){
              var patientName = convertOptionString(treatmentsData.firstName) +" "+ convertOptionString(treatmentsData.lastName)

              // var messageContent = "Hello <<patient name>>, \nPlease find the below <<form name>>.\n<<link>>"
              var messageContent = StringContext treatEscapes convertOptionString(practiceData.postopInstructionsMessage)
              messageContent = messageContent.replace("<<form name>>", formTitle).replace("<<link>>", docLink).replace("<<patient name>>", patientName).replace("\\n","\n")

            if(actionType == "SMS") {
                response = sendSMS(treatmentsData.phoneNumber, messageContent, convertOptionString(treatmentsData.country))
                if(response == "Message Sent"){
                  status = "Success"
                  apiResponse = formTitle + " " +"sent to Patient through SMS"
                 }else{
                  status = "Failure"
                  apiResponse = response
                 }
              } else if (actionType == "Email"){
                emailService.sendReminder(List(treatmentsData.emailAddress), from, replyTo, formTitle, messageContent)
                status = "Success"
                apiResponse = formTitle + " " + "sent to Patient through Email"
              } else {
                status = "Failure"
                apiResponse = "Action type is incorrect"
              }
            }else{
              status = "Failure"
              apiResponse = "Online registration module is not enabled. To enable please send mail to team@novadontics.com"
            }
            }) 
            }
            Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    val obj = Json.obj()
    if(status == "Success"){
    val newObj = obj + ("status" -> JsString(status)) + ("response" -> JsString(apiResponse))
    Future(Ok{Json.toJson(Array(newObj))})
    }else{
    val newObj = obj + ("status" -> JsString(status)) + ("response" -> JsString(apiResponse))
    Future(PreconditionFailed{Json.toJson(Array(newObj))})
    }
       
      case JsError(_) => Future.successful(BadRequest)
    }
  }


}
