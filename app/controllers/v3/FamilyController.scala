
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{FamilyRow,FamilyMembersRow,TreatmentRow,SubscriberRow}
import play.api.mvc.Result


class FamilyController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


 implicit val FamilyFormat = Json.format[FamilyRow]

 implicit val FamilyMemberFormat = Json.format[FamilyMembersRow]
 implicit val SubscriberFormat = Json.format[SubscriberRow]

case class GetFamilyMembersRow(
  patientId: Long,
  name: String,
  email: String,
  phone: String
)

implicit val familyMembersListRowWrites = Writes { familyMembers: GetFamilyMembersRow  =>
    val (familyMembersRow) = familyMembers

  Json.obj(
    "patientId" -> familyMembersRow.patientId,
    "name" -> familyMembersRow.name,
    "email" -> familyMembersRow.email,
    "phone" -> familyMembersRow.phone
  )
  }

 def addMembersToFamily = silhouette.SecuredAction.async(parse.json) { request =>
    var isFamilyIdSame = false
    var selectedPatientId: Long = 0

    val family = for {
      patientId <- (request.body \ "patientId").validate[Long]
      familyId <- (request.body \ "familyId").validate[Long]

    } yield {
        db.run{
          FamilyMembersTable.filter(_.familyId === familyId).result
        } map { familyMap =>
            if(familyMap.length == 1){
              selectedPatientId = familyMap.head.patientId
            }
        }

        var block = db.run{
            FamilyMembersTable.filter(_.patientId === patientId).result
        }map{ familyMembersMap => 

        if(familyMembersMap.length > 0){
          familyMembersMap.map{ result =>
             if(result.familyId == familyId){
                isFamilyIdSame = true
             }
          }
        }
      }
      var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      FamilyMembersRow(None, familyId, patientId)
    } 
    
    family match {
      case JsSuccess(familyMembersRow, _) => 
        
        if(isFamilyIdSame){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("Member already added to this family"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})
        } else{

            db.run {
              FamilyMembersTable.returning(FamilyMembersTable.map(_.id)) += familyMembersRow
             } map {
                  case 0L => PreconditionFailed
                  case id =>
                  var block = db.run{
                    FamilyMembersTable.filter(_.patientId === familyMembersRow.patientId).sortBy(_.familyId).result.head
                  } map { familyMap =>
                      db.run{
                        FamilyMembersTable.filter(_.familyId === familyMap.familyId).result
                      } map { familyLength =>
                      familyLength.foreach(r => {
                          if(familyLength.length > 1){
                          db.run{
                            TreatmentTable.filter(s=> s.id === r.patientId || s.id === selectedPatientId).map(_.isFamily).update(Some(true))
                          }
                        }
                      })
                      }
                    }
                  var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                  Created(Json.toJson(familyMembersRow.copy(id = Some(id))))
                }
        }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

def getFamilyMembers(patientId: Long) = silhouette.SecuredAction.async { request =>
    var familyID: Long = 0
    var guarantorId: Long = 0
    var guarantorFirstName = ""
    var guarantorLastName = ""
    var guarantorGender = ""
    var guarantorEmail = ""
    var guarantorPhone = ""
    var guarantorDOB  = ""
    var guarantorSS = ""
    var patientType = ""

  var block1 = db.run{
    FamilyMembersTable.filter(_.patientId === patientId).result
  } map { familyMembersMap =>
  
  if(familyMembersMap.length <= 0){
    var familyRow = FamilyRow(None,null,Some(""),Some(""),Some(""),Some(""),Some(""),null,Some(""),Some(""))
      var block = db.run {   
          FamilyTable.returning(FamilyTable.map(_.id)) += familyRow
          } map {
            case 0L => PreconditionFailed
            case id => //Created(Json.toJson(familyRow.copy(id = Some(id))))
            var familyMembersRow = FamilyMembersRow(None,id,patientId)
          var block1 = db.run{
                FamilyMembersTable.returning(FamilyMembersTable.map(_.id))  += familyMembersRow
            } map {
              case 0L => PreconditionFailed
              case id => Ok
            }
            var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
          }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }
  }

  var AwaitResult2 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  var familyId: Long = 0
    val block3 = db.run{
          for {
                familyMembers <-FamilyMembersTable.filter(_.patientId === patientId).sortBy(_.familyId).result.head
          }yield{
               familyId = familyMembers.familyId
          }
    }

      var AwaitResult = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    var block4 = db.run{
        FamilyTable.filter(_.id === familyId).result
      } map { familyMap =>
      familyMap.map{ familyDetails => 
        familyID = convertOptionLong(familyDetails.id)
        guarantorId = convertOptionLong(familyDetails.guarantorId)
        guarantorFirstName = convertOptionString(familyDetails.guarantorFirstName)
        guarantorLastName = convertOptionString(familyDetails.guarantorLastName)
        guarantorGender = convertOptionString(familyDetails.guarantorGender)
        guarantorEmail = convertOptionString(familyDetails.guarantorEmail)
        guarantorPhone = convertOptionString(familyDetails.guarantorPhone)
        if(familyDetails.guarantorDOB != None) {
          guarantorDOB = convertOptionalLocalDate(familyDetails.guarantorDOB).toString
        }        
        guarantorSS = convertOptionString(familyDetails.guarantorSS)
        patientType = convertOptionString(familyDetails.patientType)
      }
      }
      var AwaitResult1 = Await.ready(block4, atMost = scala.concurrent.duration.Duration(60, SECONDS))

   db.run{
        FamilyMembersTable.filter(_.familyId === familyId).join(TreatmentTable).on(_.patientId === _.id).filterNot(_._2.deleted).result
      } map { result =>

        val data = result.groupBy(_._1.id).map {
          case (familyMemberId, entries) =>
          val family = entries.head._1
          val treatment = entries.head._2
          val patientName = convertOptionString(treatment.firstName) +" "+ convertOptionString(treatment.lastName)

       GetFamilyMembersRow(family.patientId,patientName,treatment.emailAddress,treatment.phoneNumber)
      }

         Ok {Json.obj("family" -> Json.obj(
        "familyId" -> familyId,
        "guarantorId" -> guarantorId,
        "guarantorFirstName" -> guarantorFirstName,
        "guarantorLastName" ->guarantorLastName,
        "guarantorGender" -> guarantorGender,
        "guarantorEmail" -> guarantorEmail,
        "guarantorPhone" -> guarantorPhone,
        "guarantorDOB" -> guarantorDOB,
        "guarantorSS" -> guarantorSS,
        "patientType" -> patientType
      ),"familyMembers" -> data)}
      }
}

 
  def updateGuarantor = silhouette.SecuredAction.async(parse.json) { request =>
  var isPatientError = false
  var isNonPatientError = false

    val updateGuarantor = for {
      familyId <- (request.body \ "familyId").validate[Long]
      patientId <- (request.body \ "patientId").validateOpt[Long]
      patientType <- (request.body \ "patientType").validate[String]
      guarantorFirstName <- (request.body \ "guarantorFirstName").validateOpt[String]
      guarantorLastName <- (request.body \ "guarantorLastName").validateOpt[String]
      guarantorGender <- (request.body \ "guarantorGender").validateOpt[String]
      guarantorEmail <- (request.body \ "guarantorEmail").validateOpt[String]
      guarantorPhone <- (request.body \ "guarantorPhone").validateOpt[String]
      guarantorDOB <- (request.body \ "guarantorDOB").validateOpt[String]
      guarantorSS <- (request.body \ "guarantorSS").validateOpt[String]

    } yield {
      if(patientType == "patient"){
        if(patientId == None || patientId == null){
          isPatientError = true
        }
      } else {
        if(guarantorFirstName == None || guarantorLastName == None || guarantorGender == None || guarantorEmail == None || guarantorPhone == None || guarantorDOB == None || guarantorSS == None){
          isNonPatientError = true
        }

      }
      var vguarantorDOB: Option[LocalDate] = None
      if(guarantorDOB != None && guarantorDOB != Some("")){
        vguarantorDOB = Some(LocalDate.parse(convertOptionString(guarantorDOB)))
      }
          (familyId,patientId,Some(patientType),guarantorFirstName,guarantorLastName,guarantorGender,guarantorEmail,guarantorPhone,vguarantorDOB,guarantorSS)
    }

    updateGuarantor match {
      case JsSuccess((familyId,patientId,patientType,guarantorFirstName,guarantorLastName,guarantorGender,guarantorEmail,guarantorPhone,vguarantorDOB,guarantorSS), _) =>
      if(isPatientError){
         val obj = Json.obj()
         val newObj = obj + ("result" -> JsString("Failed")) + ("message" -> JsString("Please provide required details!"))
         Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else if(isNonPatientError){
         val obj = Json.obj()
         val newObj = obj + ("result" -> JsString("Failed")) + ("message" -> JsString("Please provide required details!"))
         Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else{
      db.run {
        val query = FamilyTable.filter(_.id === familyId)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

          Some(patientId).map(value => query.map(_.guarantorId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorFirstName).map(value => query.map(_.guarantorFirstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorLastName).map(value => query.map(_.guarantorLastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorGender).map(value => query.map(_.guarantorGender).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorEmail).map(value => query.map(_.guarantorEmail).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorPhone).map(value => query.map(_.guarantorPhone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(vguarantorDOB).map(value => query.map(_.guarantorDOB).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(guarantorSS).map(value => query.map(_.guarantorSS).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
          Some(patientType).map(value => query.map(_.patientType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
    }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


def deleteFamilyMember(familyId: Long, patientId: Long) = silhouette.SecuredAction.async { request =>
        db.run {
            FamilyMembersTable.filter(_.familyId === familyId).filter(_.patientId === patientId).delete map {
            case 0 => NotFound
            case _ =>

              var block1 = db.run{
                FamilyMembersTable.filter(_.familyId === familyId).result
              } map { memberMap =>
                if(memberMap.length == 1) {
                  db.run{
                    TreatmentTable.filter(_.id === memberMap.head.patientId).map(_.isFamily).update(Some(false))
                  }
                }
              }
              val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              var block2 = db.run{
                    FamilyMembersTable.filter(_.patientId === patientId).sortBy(_.familyId).result
                  } map { familyMap =>
                    if( familyMap.length == 0){
                      db.run{
                        TreatmentTable.filter(_.id === patientId).map(_.isFamily).update(Some(false))
                        }
                      } else {
                        db.run{
                          FamilyMembersTable.filter(_.familyId === familyMap.head.familyId).length.result
                        } map { familyLength =>
                            if(familyLength <= 1){
                              db.run{
                                TreatmentTable.filter(_.id === patientId).map(_.isFamily).update(Some(false))
                                }
                            }
                        }
                      }
                    }
                val AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              Ok
          }
        }
  }


def subscriber(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  val iSubscriber = for {
      types <- (request.body \ "type").validate[String]
      firstName <- (request.body \ "firstName").validate[String]
      lastName <- (request.body \ "lastName").validate[String]
      dob <- (request.body \ "dob").validate[LocalDate]
      address <- (request.body \ "address").validate[String]
      city <- (request.body \ "city").validate[String]
      state <- (request.body \ "state").validate[String]
      zip <- (request.body \ "zip").validate[String]
      phone <- (request.body \ "phone").validate[String]
      ssn <- (request.body \ "ssn").validateOpt[String]
      relationship <- (request.body \ "relationship").validate[String]
      carrierId <- (request.body \ "carrierId").validate[Long]
      memberId <- (request.body \ "memberId").validate[String]
      groupNumber <- (request.body \ "groupNumber").validateOpt[String]
      planName <- (request.body \ "planName").validate[String]
      employerName <- (request.body \ "employerName").validateOpt[String]
      employerPhone <- (request.body \ "employerPhone").validateOpt[String]

    } yield {
       SubscriberRow(None,patientId,types.toLowerCase,firstName,lastName,dob,address,city,state,zip,phone,convertOptionString(ssn),relationship,carrierId,memberId,convertOptionString(groupNumber),planName,convertOptionString(employerName),convertOptionString(employerPhone))
    }
    iSubscriber match {
      case JsSuccess((subscriberRow), _) =>
      val query = SubscriberTable.filter(_.patientId === patientId).filter(_.`type` === subscriberRow.`type`)
      db.run{
        query.result } map { subscriberVector =>
        if(subscriberVector.length == 0){
          var b = db.run{
            SubscriberTable.returning(SubscriberTable.map(_.id)) += subscriberRow
          }
          Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        } else {
          db.run {
            query.exists.result.flatMap[Result, NoStream, Effect.Write] {
              case true => DBIO.seq[Effect.Write](
              Some(subscriberRow.firstName).map(value => query.map(_.firstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.lastName).map(value => query.map(_.lastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.dob).map(value => query.map(_.dob).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.address).map(value => query.map(_.address).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.city).map(value => query.map(_.city).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.state).map(value => query.map(_.state).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.zip).map(value => query.map(_.zip).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.phone).map(value => query.map(_.phone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.ssn).map(value => query.map(_.ssn).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.relationship).map(value => query.map(_.relationship).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.carrierId).map(value => query.map(_.carrierId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.memberId).map(value => query.map(_.memberId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.groupNumber).map(value => query.map(_.groupNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.planName).map(value => query.map(_.planName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.employerName).map(value => query.map(_.employerName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(subscriberRow.employerPhone).map(value => query.map(_.employerPhone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map {_ => Ok}
              case false => DBIO.successful(NotFound)
            }
          }
        }
        Ok
      }
    case JsError(_) => Future.successful(BadRequest)
    }
  }

case class Temp(
  primary: JsValue,
  secondary: JsValue
)

implicit val TempFormat = Json.format[Temp]

def readSubscriber(patientId: Long) = silhouette.SecuredAction.async { request =>
var primary: JsValue = Json.parse("{}")
var secondary: JsValue = Json.parse("{}")
  db.run {
      SubscriberTable.filter(_.patientId === patientId).result
    } map { subscriberVector =>
    (subscriberVector).foreach(result => {
      if(result.`type` == "primary"){
          primary = Json.toJson(result)
      } else {
          secondary = Json.toJson(result)
      }
    })
    var data = Temp(primary,secondary)
    Ok(Json.toJson(data))
    }
  }
}


