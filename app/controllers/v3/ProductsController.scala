package controllers.v3

import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import java.net.{URL,HttpURLConnection,URLConnection}
import java.io.File
import com.github.tototoshi.csv._
import play.api.mvc.Action 

import co.spicefactory.services.EmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import akka.stream.scaladsl.StreamConverters
import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.google.inject.name.Named
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.{ProductRow, VendorRow, PriceChangeHistoryRow, CategoryRow, StaffRow, UserRow}
import play.api.{Application, Logger}
import play.api.http.HttpEntity
import play.api.data.validation.ValidationError
import play.api.libs.json.Json.obj
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.json._
import play.api.libs.ws._
import play.api.Play.current
import play.api.mvc.{ResponseHeader, Result}
import java.text.NumberFormat.getCurrencyInstance
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO

import play.api.libs.ws._
import scala.util.{Failure, Success}
import scalaj.http.{Http, HttpOptions}


/*class ProductsController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {*/
@Singleton
class ProductsController @Inject() (ws: WSClient) (silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._

  private val from = application.configuration.getString("aws.ses.consultation.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val replyTo = application.configuration.getString("aws.ses.consultation.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val baseUrl = application.configuration.getString("baseUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))

  implicit val productFormat = Json.format[ProductRow]
  
  implicit val categoryFormat = Json.format[CategoryRow]

  private val emailService = new EmailService(ses, application)


  def parseLong(s: String) = try { Some(s.toLong) } catch { case _ => None }

  private def toNumber(implicit reads: Reads[String]) = {
    Reads[JsNumber](js =>
      reads.reads(js).flatMap { value =>
        parseDouble(value) match {
          case Some(number) => JsSuccess(JsNumber(number))
          case _ => JsError(ValidationError("error.number", value))
        }
      }
    )
  }
  
  
  
  
  /*public static boolean exists(String URLName){
    try {
      HttpURLConnection.setFollowRedirects(false);
      // note : you may also need
      //        HttpURLConnection.setInstanceFollowRedirects(false)
      HttpURLConnection con =
         (HttpURLConnection) new URL(URLName).openConnection();
      con.setRequestMethod("HEAD");
      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
    }
    catch (Exception e) {
       e.printStackTrace();
       return false;
    }
  }  */
  
  
  /*private def validateURL(url:String):Boolean={
    val request: WSRequest = WS.url(url)
    //val response = WS.url(url)
    val futureResponse: Future[WSResponse] = request.get()
    //Console.println("response Status"+futureResponse.getStatus())
    return false
    if (valid(response))
      return true
    else 
      return false
 }*/
 
 
  
  private val logger = Logger(this.getClass)

  object Input {
    trait Product
    case class NewProduct(serialNumber:String, name: String, description: Option[String], imageUrl:Option[String], manufacturer:String, categoryName:Option[String], price:Double, quantity:Option[Long], listPrice:Option[Double], document : Option[String], video : Option[JsValue], stockAvailable: Option[String], unit: Option[String]) extends Product
    object Product {
      lazy val reads = Reads
      implicit object Reads extends Reads[Product] {
        override def reads(data: JsValue) =
          for {
            name <- (data \ "name").validate[String]
            description <- (data \ "description").validateOpt[String]
            serialNumber  <- (data \ "serialNumber").validate[String]
            imageUrl <- (data \ "imageUrl").validateOpt[String]
            manufacturer <- (data \ "manufacturer").validate[String]
            categoryName <- (data \ "categoryName").validateOpt[String]
            price <- (data \ "price").transform(toNumber)
            quantity <- (data \ "quantity").validateOpt[String]
            listPrice <- (data \ "listPrice").transform(toNumber)
            document <- (data \ "document").validateOpt[String]
            video <- (data \ "video").validateOpt[JsValue]
            stockAvailable <- (data \ "stockAvailable").validateOpt[String]
            unit <- (data \ "unit").validateOpt[String]
          } yield NewProduct(serialNumber, name, description, imageUrl, manufacturer, categoryName, price.as[Double], parseLong(quantity.getOrElse("0")), listPrice.asOpt[Double],document,video,stockAvailable,unit)
      }
    }
  }
  
  object Output {
    case class Category(id: Long, name: String, order: Int, children: Option[List[Category]], prodCount: Int)

    object Category {
      implicit val writes: Writes[Category] = Writes { category =>
        Json.obj(
          "id" -> category.id,
          "name" -> category.name,
          "order" -> category.order,
          "productCount" -> category.prodCount
        ) ++ (category.children match {
          case Some(children) =>
            Json.obj(
              "children" -> JsArray(children.map(c => writes.writes(c)))
            )
          case None =>
            Json.obj()
        })
      }
    }
  }



  case class Product(
                      id: Option[Long],
                      name: String,
                      description: Option[String],
                      serialNumber: String,
                      imageUrl: Option[String],
                      vendorId: Long,
                      categoryId: Option[Long],
                      price: Double,
                      manufacturer: String,
                      quantity: Option[Long],
                      favourite: Option[Boolean],
                      listPrice: Option[Double],
                      document: Option[String],
                      video: Option[JsValue],
                      stockAvailable: Option[Boolean],
                      unit: Option[String],
                      financeEligible: Option[Boolean]
                    )
  object Product {
    implicit val writes = Json.writes[Product]
  }


  case class  CategoryList(
                        id: Option[Long],
                        name:String,
                        count:Int
                      )
                      
  object CategoryList {
    implicit val writes = Json.writes[CategoryList]
  }                    

  case class ProductCSVList(
                        vendorName: String,
                        categoryName: String, 
                        referenceNumber: String, 
                        name: String, 
                        retailPrice: String, 
                        novaPrice: String, 
                        countNumber: String, 
                        imageData: String
                      )                            

  case class CatalogProduct(
                             id: Option[Long],
                             name: String,
                             description: Option[String],
                             serialNumber: String,
                             imageUrl: String,
                             vendorId: Long,
                             categoryId: Option[Long],
                             price: String,
                             listPrice: String,
                             manufacturer: String,
                             categoryName: String,
                             quantity: Long

                           )
                           
   

 def readAll = silhouette.SecuredAction.async { request =>

    var pageNumber:Int = 0
    var recPerPage:Int = 0
    var vendorList = List.empty[Long]
    var categoryList = List.empty[Long]

    val searchTextVal: Option[String] = request.headers.get("searchText")
    val searchText:String = convertOptionString(searchTextVal)

    var pageNumberVal: Option[String] = request.headers.get("pageNumber")
    if ( pageNumberVal != None){
      pageNumber = convertOptionString(pageNumberVal).toInt
    }

    if (pageNumber == 0 ){
      pageNumber = 1
    }
    
    var recPerPageVal: Option[String] = request.headers.get("recPerPage")
    if( recPerPageVal != None){
      recPerPage = convertOptionString(recPerPageVal).toInt
    }    

    var query = ProductTable.filterNot(_.deleted)
    request.identity match {
            case row: StaffRow => query = query.filter(_.stockAvailable === true)
            case row: UserRow =>
            }


    if (searchText != ""){

        var vendorIdList = db.run{
              for {
                  vendorData <- VendorTable.filter(_.name.toLowerCase like "%"+searchText.toLowerCase+"%").result
              } yield{
                  vendorData.map{vendor=> vendorList =  vendorList :+ convertOptionLong(vendor.id)}
                }
        } 

        val AwaitResult = Await.ready(vendorIdList, atMost = scala.concurrent.duration.Duration(60, SECONDS))
         

        var categoryIdList = db.run{
                  for {
                      categoryData <- CategoryTable.filter(_.name.toLowerCase like "%"+searchText.toLowerCase+"%").result
                  } yield{
                      categoryData.map{category=> categoryList =  categoryList :+ convertOptionLong(category.id)}
                    }
            } 

         val AwaitResult2 = Await.ready(categoryIdList, atMost = scala.concurrent.duration.Duration(60, SECONDS))
          

          if(categoryList.length!=0) {
            query = query.filter(f=>((f.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.serialNumber.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.categoryId inSet categoryList)))
          }
          else if (vendorList.length!=0)  {  
             query = query.filter(f=>((f.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.serialNumber.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.vendorId inSet vendorList)))
          }else {
              query = query.filter(f=>((f.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.serialNumber.toLowerCase like "%"+searchText.toLowerCase+"%")))
          }

       db.run {
          query.join(VendorTable).on(_.vendorId === _.id)
            .joinLeft(CustomerFavouritesTable.filter(_.customer === request.identity.id.get))
            .on(_._1.id === _.product)
            .map({
              case ((products, vendor), favourites) => (products, vendor.name, favourites.isDefined)
            })
            .result

        } map { products =>
        
          val data = products.groupBy(_._1.id).map {
            case (id, items) => {
              val item = items.head._1
              val vendorName = items.head._2
              val favourite = items.head._3

              var productName = item.name
              if(productName.length > 30){
                productName = productName.substring(0,30)
              }

            var productPrice = Math.round(item.price * 100.0) / 100.0
            var listPrice = Some(Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0)

              Product(
                item.id,
                item.name,
                item.description,
                item.serialNumber,
                item.imageUrl,
                item.vendorId,
                item.categoryId,
                productPrice,
                vendorName,
                item.quantity,
                Some(favourite),
                listPrice,
                item.document,
                item.video,
                item.stockAvailable,
                item.unit,
                item.financeEligible
              )
            }
          }.toList.sortBy(_.serialNumber)
          if (recPerPage == 0){
            val pageData = data
            Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> data.length.toString()))
          }
          else{
              val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
              Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> data.length.toString()))
          }
          
        }
      }
      else{ 
        db.run {
            query.join(VendorTable).on(_.vendorId === _.id)
              .joinLeft(CustomerFavouritesTable.filter(_.customer === request.identity.id.get))
              .on(_._1.id === _.product)
              .map({
                case ((products, vendor), favourites) => (products, vendor.name, favourites.isDefined)
              })
              .result

            } map { products =>
            
              val data = products.groupBy(_._1.id).map {
                case (id, items) => {
                  val item = items.head._1
                  val vendorName = items.head._2
                  val favourite = items.head._3

                  var productName = item.name
                  if(productName.length > 30){
                    productName = productName.substring(0,30)
                  }

            var productPrice = Math.round(item.price * 100.0) / 100.0
            var listPrice = Some(Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0)

                  Product(
                    item.id,
                    item.name,
                    item.description,
                    item.serialNumber,
                    item.imageUrl,
                    item.vendorId,
                    item.categoryId,
                    productPrice,
                    vendorName,
                    item.quantity,
                    Some(favourite),
                    listPrice,
                    item.document,
                    item.video,
                    item.stockAvailable,
                    item.unit,
                    item.financeEligible
                  )
                }
              }.toList.sortBy(_.serialNumber)
              if (recPerPage == 0){
                val pageData = data
                Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> data.length.toString()))
              }
          else{
              val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
              Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> data.length.toString()))
          }
        }
      }
    }
  
  
  def getCategories = silhouette.SecuredAction.async(parse.json) { request =>
    val inputData = for {
      searchTextVal <- (request.body \ "searchText").validateOpt[String]
      favouritesVal <- (request.body \ "favourites").validateOpt[String]
      saveLaterVal <- (request.body \ "saveLater").validateOpt[String]
      priceVal <- (request.body \ "price").validateOpt[Double]
      vendorsVal <- (request.body \ "vendors").validateOpt[List[Long]]
    } yield {
      (searchTextVal,favouritesVal,saveLaterVal,priceVal,vendorsVal)
    }
    
    inputData match {
      case JsSuccess((searchTextVal,favouritesVal,saveLaterVal,priceVal,vendorsVal), _) =>
            val searchText:String = convertOptionString(searchTextVal)
            val favouritesStr:String = convertOptionString(favouritesVal)
            val saveLaterStr:String = convertOptionString(saveLaterVal)
            val price:Double = convertOptionDouble(priceVal)
            
            val vendors:List[Long] = convertOptionListLong(vendorsVal)
            
            var productCount = 0
            
            var query = ProductTable.filterNot(_.deleted)
            request.identity match {
            case row: StaffRow => query = query.filter(_.stockAvailable === true)
            case row: UserRow =>
            }

            if (searchText!=""){
              query = query.filter(_.name.toLowerCase like "%"+searchText.toLowerCase+"%")
            }
            
            if (price!=0){
              query = query.filter(f=>(f.listPrice<=priceVal || f.price<=priceVal))
            }
            

            if (vendors.length!=0)  {
              query = query.filter(_.vendorId inSet vendors)
            } 
            

            if (favouritesStr == "true"){
              var favouriteProductList = List.empty[Long]
              var fProducts = db.run{
                for{
                  fp <- CustomerFavouritesTable.filter(_.customer === request.identity.id.get).result
                }yield{
                  var favouriteProducts = fp
                  favouriteProducts.map{ 
                      favProducts => favouriteProductList =  favouriteProductList :+ favProducts.product
                  }
                }
              }
              
              val AwaitResult = Await.ready(fProducts, atMost = scala.concurrent.duration.Duration(3, SECONDS))
              query = query.filter(_.id inSet favouriteProductList)
            }  else if(saveLaterStr == "true"){
              var saveLaterProductList = List.empty[Long]
              var sProducts = db.run{
                for{
                  fp <- SaveLaterTable.filter(_.staffId === request.identity.id.get).result
                }yield{
                  var saveLaterProducts = fp
                  saveLaterProducts.map{
                      saveProducts => saveLaterProductList =  saveLaterProductList :+ saveProducts.productId
                  }
                }
              }

              val AwaitResult = Await.ready(sProducts, atMost = scala.concurrent.duration.Duration(3, SECONDS))
              query = query.filter(_.id inSet saveLaterProductList)
            }  else if(saveLaterStr == "true"){
            }

            var f = true
            //if (favouritesStr == "true"){
              val action = (for {
                (category, p) <- CategoryTable joinLeft query on(_.id === _.categoryId)  
              } yield (category, p.map(_.id))).groupBy({
                case (category, _) => category
              }).map({
                case ((category), products) => {
                  (category, products.map(_._2).countDistinct)
                }
              }).sortBy(c => c._1.order.desc.nullsFirst)

              db.run{ action.result } map {categories =>
                //Console.println(categories)
                var categoryProductCountArr = new Array[Int](categories.length)
                
                var categoryProductCountMap:scala.collection.mutable.Map[Long,Int]=scala.collection.mutable.Map()
                
                for(i<-0 to categories.length-1){
                    var catId = convertOptionLong(categories(i)._1.id)
                    categoryProductCountMap.update(catId,categories(i)._2)
                    if (categories(i)._1.parent == None){
                      categoryProductCountMap(catId) = categoryProductCountMap(catId) + categories.filter(_._1.parent == categories(i)._1.id).map(c=> c._2).sum
                    }  
                }
                
                
                val categoriesWithCountUpdated = categories.collect{case (catRow,count) => (catRow,categoryProductCountMap(convertOptionLong(catRow.id)))}
                
                var categoriesFiltered = categoriesWithCountUpdated.filter(f=>((f._1.parent == None)||(f._1.parent != None && f._2 != 0) ))
                
                var categoriesOnlyWithZeroCount = categoriesWithCountUpdated.filter(f=>(f._1.parent == None && f._2 == 0))
                
                
                
                
                categoriesOnlyWithZeroCount.map{(row: (CategoryRow, Int)) =>
                   val categoryId: Long = row._1.id.get
                   val subCategories = categoriesFiltered.filter(f=>f._1.parent == Some(categoryId))
                   if (subCategories.isEmpty){
                     categoriesFiltered = categoriesFiltered.filter(f=>f._1.id != Some(categoryId))
                   }
                }
                
                         
                
                
                
                val childrenOf = categoriesFiltered.foldLeft(Map.empty[Long, List[(CategoryRow, Int)]].withDefaultValue(Nil)) { (acc, category) =>
                  var id = category._1.parent.getOrElse(0L)
                  acc.updated(id, (category._1, category._2) :: acc(id))
                }

                def buildTrees(rows: List[(CategoryRow, Int)]): List[Output.Category] = {
                  rows.map { (row: (CategoryRow, Int)) =>
                    //Console.println(row._1)
                    val id: Long = row._1.id.get
                    val children: scala.List[ProductsController.this.Output.Category] = buildTrees(childrenOf(id))
                    Output.Category.apply(id, row._1.name, row._1.order, if (children.isEmpty) scala.None else Some.apply(children), row._2)
                  }
                }

                Ok(Json.toJson(buildTrees(childrenOf(0))))
                
                
              }
              
     }       
  }  
  
  def findProducts = silhouette.SecuredAction.async(parse.json) { request =>
    val inputData = for {
      searchTextVal <- (request.body \ "searchText").validateOpt[String]
      favouritesVal <- (request.body \ "favourites").validateOpt[String]
      saveLaterVal <- (request.body \ "saveLater").validateOpt[String]
      priceVal <- (request.body \ "price").validateOpt[Double]
      categoriesVal <- (request.body \ "categories").validateOpt[List[Long]]
      vendorsVal <- (request.body \ "vendors").validateOpt[List[Long]]
      pageNumberVal <- (request.body \ "pageNumber").validateOpt[Int]
      recPerPageVal <- (request.body \ "recPerPage").validateOpt[Int]
    } yield {
      (searchTextVal,favouritesVal,saveLaterVal,priceVal,categoriesVal,vendorsVal,pageNumberVal,recPerPageVal)
    }
    
    inputData match {
      case JsSuccess((searchTextVal,favouritesVal,saveLaterVal,priceVal,categoriesVal,vendorsVal,pageNumberVal,recPerPageVal), _) =>
            val searchText:String = convertOptionString(searchTextVal)
            val favouritesStr:String = convertOptionString(favouritesVal)
            val saveLaterStr:String = convertOptionString(saveLaterVal)
            val price:Double = convertOptionDouble(priceVal)
            val categories:List[Long] = convertOptionListLong(categoriesVal)
            val vendors:List[Long] = convertOptionListLong(vendorsVal)
            
            
            var pageNumber:Int = convertOptionInt(pageNumberVal)
            var recPerPage:Int = convertOptionInt(recPerPageVal)
            
            if (pageNumber ==0 ){
              pageNumber = 1
            }  
            if (recPerPage ==0 ){
              recPerPage = 50
            }  
            
            
            var productCount = 0
            
            val qMaxPrice = ProductTable.filterNot(_.deleted).groupBy { _ => true }.map {
              case (_, group) =>
                (group.map(_.price).max)
            }
            var maxPrice:Long = 10000
            var fQueryPrice = db.run {
              for {
                maxPriceRes <- qMaxPrice.result
              }yield { 
                 maxPrice = convertOptionDouble(maxPriceRes(0)).toLong
                 
              }  
            }
            
            
            val AwaitResult = Await.ready(fQueryPrice, atMost = scala.concurrent.duration.Duration(3, SECONDS))
            
            //Console.println("maxPrice="+maxPrice)
            
            
            
            var query = ProductTable.filterNot(_.deleted)
            request.identity match {
            case row: StaffRow => query = query.filter(_.stockAvailable === true)
            case row: UserRow =>
            }

            if (searchText!=""){
              //var searchTextStr = searchText.trim
              //Console.println("searchText="+searchText)
              query = query.filter(f=>((f.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.serialNumber.toLowerCase like "%"+searchText.toLowerCase+"%")))
            }
            
            if (price!=0){
              //Console.println("priceVal="+priceVal)
              //query = query.filter(_.listPrice<=priceVal)
              query = query.filter(f=>(f.listPrice<=priceVal || f.price<=priceVal))
              
              }
            
            //Console.println("vendors="+vendors)
            if (vendors.length!=0)  {
              query = query.filter(_.vendorId inSet vendors)
            }  
            
              
                
            
            //Console.println("categories="+categories)
            if (categories.length!=0)  {
             /* var subCategoriesList = List.empty[Long]
              
              var fSubCategory = db.run{
                  for {
                      rsSubCategories <- CategoryTable.filter(_.parent inSet categories).result
                  }yield{
                      rsSubCategories.map{subCategory=> subCategoriesList =  subCategoriesList :+ convertOptionLong(subCategory.id)}
                  }
              }
              
              val AwaitResult = Await.ready(fSubCategory, atMost = scala.concurrent.duration.Duration(3, SECONDS))     
              val totalCategories = categories ++ subCategoriesList
             
              query = query.filter(_.categoryId inSet totalCategories)*/
              
              query = query.filter(_.categoryId inSet categories)
              
            }  
              
            
            
            var f = true
            if (favouritesStr == "true"){
              db.run {
                     query.join(VendorTable).on(_.vendorId === _.id)
                    .join(CustomerFavouritesTable.filter(_.customer === request.identity.id.get))
                    .on(_._1.id === _.product)
                    .map({
                      case ((products, vendor), favourites) => (products, vendor.name, favourites.id)
                    })
                    .result
              } map { products =>
                  val data = products.groupBy(_._1.id).map {
                    case (id, items) => {
                      val item = items.head._1
                      val vendorName = items.head._2
                      val favourite = items.head._3

                    var productName = item.name
                    if(productName.length > 30){
                        productName = productName.substring(0,30)
                    }

                var productPrice = Math.round(item.price * 100.0) / 100.0
                var listPrice = Some(Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0)

                      Product(
                        item.id,
                        item.name,
                        item.description,
                        item.serialNumber,
                        item.imageUrl,
                        item.vendorId,
                        item.categoryId,
                        productPrice,
                        vendorName,
                        item.quantity,
                        Some(true),
                        listPrice,
                        item.document,
                        item.video,
                        item.stockAvailable,
                        item.unit,
                        item.financeEligible
                      )
                    }
                  }.toList.sortBy(_.serialNumber)
                  var productCount = data.length
                  val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
                  Ok(Json.toJson(pageData)).withHeaders(
                              ("productCount" -> productCount.toString()))
              }
            } else if(saveLaterStr == "true") {
              db.run {
                     query.join(VendorTable).on(_.vendorId === _.id)
                    .join(SaveLaterTable.filter(_.staffId === request.identity.id.get)).on(_._1.id === _.productId)
                    .joinLeft(CustomerFavouritesTable.filter(_.customer === request.identity.id.get))
                    .on(_._1._1.id === _.product)
                    .map({
                      case (((products, vendor), saveLaters),favourites) => (products, vendor.name, saveLaters, favourites.isDefined)
                    })
                    .result
              } map { products =>
                  val data = products.groupBy(_._1.id).map {
                    case (id, items) => {
                      val item = items.head._1
                      val vendorName = items.head._2
                      val saveLater = items.head._3
                      val favourite = items.head._4

                    var productName = item.name
                    if(productName.length > 30){
                        productName = productName.substring(0,30)
                    }
                var productPrice = Math.round(item.price * 100.0) / 100.0
                var listPrice = Some(Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0)

                      Product(
                        item.id,
                        item.name,
                        item.description,
                        item.serialNumber,
                        item.imageUrl,
                        item.vendorId,
                        item.categoryId,
                        productPrice,
                        vendorName,
                        item.quantity,
                        Some(favourite),
                        listPrice,
                        item.document,
                        item.video,
                        item.stockAvailable,
                        item.unit,
                        item.financeEligible
                      )
                    }
                  }.toList.sortBy(_.serialNumber)
                  var productCount = data.length
                  val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
                  //Ok(Json.obj("products"->Json.toJson(pageData),"productCount"->productCount))
                  Ok(Json.toJson(pageData)).withHeaders(
                              ("productCount" -> productCount.toString()),("maxPrice" -> maxPrice.toString()))
              }
            }else{
              db.run {
                     query.join(VendorTable).on(_.vendorId === _.id)
                    .joinLeft(CustomerFavouritesTable.filter(_.customer === request.identity.id.get))
                    .on(_._1.id === _.product)
                    .map({
                      case ((products, vendor), favourites) => (products, vendor.name, favourites.isDefined)
                    })
                    .result
              } map { products =>
                  val data = products.groupBy(_._1.id).map {
                    case (id, items) => {
                      val item = items.head._1
                      val vendorName = items.head._2
                      val favourite = items.head._3

                    var productName = item.name
                    if(productName.length > 30){
                        productName = productName.substring(0,30)
                    }
                var productPrice = Math.round(item.price * 100.0) / 100.0
                var listPrice = Some(Math.round(convertOptionDouble(item.listPrice) * 100.0) / 100.0)

                      Product(
                        item.id,
                        item.name,
                        item.description,
                        item.serialNumber,
                        item.imageUrl,
                        item.vendorId,
                        item.categoryId,
                        productPrice,
                        vendorName,
                        item.quantity,
                        Some(favourite),
                        listPrice,
                        item.document,
                        item.video,
                        item.stockAvailable,
                        item.unit,
                        item.financeEligible
                      )
                    }
                  }.toList.sortBy(_.serialNumber)
                  var productCount = data.length
                  val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
                  //Ok(Json.obj("products"->Json.toJson(pageData),"productCount"->productCount))
                  Ok(Json.toJson(pageData)).withHeaders(
                              ("productCount" -> productCount.toString()),("maxPrice" -> maxPrice.toString()))
              }
            }    
                
     }       
  }

  def productTotalCount = silhouette.SecuredAction.async { request =>
    var staffId = request.identity.id.get
    var subscribeOrderExist: String = "false"
    db.run {
      ProductTable.filterNot(_.deleted).filter(_.stockAvailable).result
    } map { products =>
      var block = db.run {
        SubscribeProductsTable.filter(_.staffId === staffId).filterNot(_.unsubscribed).result
      } map { orderMap =>
        if(orderMap.length > 0){
          subscribeOrderExist = "true"
        }
      }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(10, SECONDS))
      var productCount = products.length
      var responseObj = Json.obj()
      responseObj = responseObj + ("productCount" -> JsNumber(productCount)) + ("subscribeOrders" -> JsString(subscribeOrderExist))
      Ok(Json.toJson(responseObj))
    }
  }

  
  
  
  def findProductsOld = silhouette.SecuredAction.async(parse.json) { request =>
    val inputData = for {
      searchTextVal <- (request.body \ "searchText").validateOpt[String]
      favouritesVal <- (request.body \ "favourites").validateOpt[String]
      priceVal <- (request.body \ "price").validateOpt[Double]
      categoriesVal <- (request.body \ "categories").validateOpt[List[Long]]
      vendorsVal <- (request.body \ "vendors").validateOpt[List[Long]]
      pageNumberVal <- (request.body \ "pageNumber").validateOpt[Int]
      recPerPageVal <- (request.body \ "recPerPage").validateOpt[Int]
    } yield {
      (searchTextVal,favouritesVal,priceVal,categoriesVal,vendorsVal,pageNumberVal,recPerPageVal)
    }
    
    inputData match {
      case JsSuccess((searchTextVal,favouritesVal,priceVal,categoriesVal,vendorsVal,pageNumberVal,recPerPageVal), _) => 
            val searchText:String = convertOptionString(searchTextVal)
            val favourites:String = convertOptionString(favouritesVal)
            val price:Double = convertOptionDouble(priceVal)
            val categories:List[Long] = convertOptionListLong(categoriesVal)
            val vendors:List[Long] = convertOptionListLong(vendorsVal)
            
            
            var pageNumber:Int = convertOptionInt(pageNumberVal)
            var recPerPage:Int = convertOptionInt(recPerPageVal)
            
            if (pageNumber ==0 ){
              pageNumber = 1
            }  
            if (recPerPage ==0 ){
              recPerPage = 50
            }  
            
            
            var productCount = 0
            
            
            var query = ProductTable.filterNot(_.deleted)
            
            if (searchText!=""){
              //Console.println("searchText="+searchText)
              query = query.filter(_.name.toLowerCase like "%"+searchText.toLowerCase+"%")
            }
            
            if (price!=0){
              //Console.println("priceVal="+priceVal)
              query = query.filter(_.listPrice<=priceVal)
            }
            
           // Console.println("vendors="+vendors)
            if (vendors.length!=0)  {
              query = query.filter(_.vendorId inSet vendors)
            }  
            
            //Console.println("categories="+categories)
            if (categories.length!=0)  {
               
               query = query.filter(_.categoryId inSet categories)
            }
              
            
            var action= 
              for{
                  products <- query
                 }  
              yield(products) 
            
            
            if (favourites=="true"){
              
              action= 
                for{
                    (products,customerFavourites) <- query.join(CustomerFavouritesTable).on(_.id === _.product)
                    //customerFavourites <- CustomerFavouritesTable.filter(_.customer === request.identity.id.get) if customerFavourites.product===products.id
                   }  
                yield(products)
            }
                 
              
            db.run { 
                 action.result 
            }.map{
                 result =>
                 var data = result.drop((pageNumber-1)*recPerPage).take(recPerPage)
                // Console.println(result) 
                 //Console.println(result.length)  
                     
                 
                 Ok(Json.obj("products"->Json.toJson(data),"productCount"->result.length))
                 //Ok(Json.arr(Json.toJson(data),result.length))
            } 
           
      case JsError(_) => Future.successful(BadRequest)
    }
    
  }
  
  
  
  
  
  
  
  def validateURL(URLName: String):Boolean = {
     val obj:URL = new java.net.URL(URLName)
     val con:HttpURLConnection = obj.openConnection().asInstanceOf[HttpURLConnection]
         
     con.setRequestMethod("HEAD")
     //Console.println(URLName)
     //Console.println(con.getResponseCode())
     return (con.getResponseCode() == HttpURLConnection.HTTP_OK)
    
  }
  
  def bulkImageUpdate(vid:Long)= silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    //var imageBaseUrl="http://uploads.ds.spfr.co/"
   // var imageBaseUrl="http://s3.amazonaws.com/uploads.novadonticsllc.com/"
    val postParam = for {
      imageBaseUrl <- (request.body \ "imageBaseUrl").validate[String]
    }yield{
      (imageBaseUrl)
    }
    postParam match {
      case JsSuccess((imageBaseUrl), _) =>
         
         db.run {
          ProductTable.filter(_.vendorId===vid).result
         }map {
          rows =>
          (rows).foreach(row=>{
            val imageUrl=imageBaseUrl+row.serialNumber+".jpg"
            val query = ProductTable.filter(_.id === row.id)
            if (validateURL(imageUrl)){
              db.run {
                 query.map(_.imageUrl).update(Some(imageUrl))
              }
            }
          })  
          //Ok(Json.toJson(rows))
          Ok
        }  
      case JsError(_) =>
        Future.successful(BadRequest)  
    }
  }  

  def create = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>
    val product = for {
      name <- (request.body \ "name").validate[String]
      description <- (request.body \ "description").validateOpt[String]
      serialNumber  <- (request.body \ "serialNumber").validate[String]
      imageUrl <- (request.body \ "imageUrl").validateOpt[String]
      manufacturer <- (request.body \ "manufacturer").validate[String]
      categoryId <- (request.body \ "categoryId").validateOpt[Long]
      price <- (request.body \ "price").validate[Double]
      quantity <- (request.body \ "quantity").validateOpt[Long]
      listPrice <- (request.body \ "listPrice").validateOpt[Double]
      document <- (request.body \ "document").validateOpt[String]
      video <- (request.body \ "video").validateOpt[JsValue]
      stockAvailable <- (request.body \ "stockAvailable").validateOpt[Boolean]
      unit <- (request.body \ "unit").validateOpt[String]
      financeEligible <- (request.body \ "financeEligible").validateOpt[Boolean]
    } yield {
      var vStockAvailable: Option[Boolean] = Some(true)
      if(stockAvailable != None){
        vStockAvailable = stockAvailable
      }
      (name, description, serialNumber, imageUrl, manufacturer, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible)
    }

    product match {
      case JsSuccess((name, description, serialNumber, imageUrl, manufacturer, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible), _) =>
        db.run {
          VendorTable.filter(_.name === manufacturer).map(_.id).result.headOption flatMap {
            case Some(vendorId) =>
              (ProductTable.returning(ProductTable.map(_.id)) += ProductRow(None, name, description, serialNumber, imageUrl, vendorId, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible)) map { id =>
                ProductRow(Some(id), name, description, serialNumber, imageUrl, vendorId, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible)
              }
            case None =>
              (VendorTable.returning(VendorTable.map(_.id)) += VendorRow(None, manufacturer, None, None, deleted = false,None,None,None,None,None,None,None,None)) flatMap { vendorId =>
                (ProductTable.returning(ProductTable.map(_.id)) += ProductRow(None, name, description, serialNumber, imageUrl, vendorId, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible)) map { id =>
                  ProductRow(Some(id), name, description, serialNumber, imageUrl, vendorId, categoryId, price, quantity, listPrice, document, video, vStockAvailable, unit, financeEligible)
                }
              }
          }
        } map { productRow =>
          Created(Json.toJson(productRow))
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

 def placeMcKessonOrder(orderId: Long, payLoadId: String) = silhouette.SecuredAction.async { request => {

  var quantity = ""
  var price = ""
  var serialNumber = ""
  var productName = ""
  var addressId: Long = 0
  var unit = ""
  var finalItemOut = ""
  var finalXmlString = ""
  var description = ""
  var line = 0
  var totalAmount: Double  = 0

  var timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z").format(new Date());

  val vSource = application.configuration.getString("mckessonFeedSource").getOrElse(throw new RuntimeException("Configuration is missing `mckessonFeedSource` property."))
  val devEmail = application.configuration.getString("devEmail").getOrElse(throw new RuntimeException("Configuration is missing `devEmail` property."))
  val mckessonSharedSecret = application.configuration.getString("mckessonSharedSecret").getOrElse(throw new RuntimeException("Configuration is missing `mckessonSharedSecret` property."))
  val mckessonOrderUrl = application.configuration.getString("mckessonOrderUrl").getOrElse(throw new RuntimeException("Configuration is missing `mckessonOrderUrl` property."))

  var xmlString = """<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE cXML SYSTEM "http://xml.cXML.org/schemas/cXML/1.2.011/cXML.dtd">
<cXML payloadID="rPayloadId" xml:lang="en-US" timestamp="rTimeStamp" version="1.2.011"><Header><From><Credential domain="NetworkID">
<Identity></Identity></Credential></From><To><Credential domain="DUNS"><Identity>023904428</Identity></Credential></To><Sender>
<Credential domain="NetworkID"><Identity>NOVB2B</Identity><SharedSecret>rShareSecret</SharedSecret></Credential><UserAgent></UserAgent>
</Sender></Header><Request><OrderRequest><OrderRequestHeader orderDate="rTimeStamp" orderID="rOrderId" type="new" orderType="regular"><Total><Money currency="USD">rTotalAmt</Money></Total><ShipTo><Address addressID="rAddressId"><Name xml:lang="en-US">Ship To Address Name</Name><PostalAddress><Street>Novadontics</Street><Street>4710 RUFFNER ST STE B</Street><City>SAN DIEGO</City><State>CA</State><PostalCode>92111</PostalCode><Country isoCountryCode="">US</Country></PostalAddress><Email name="default"></Email></Address></ShipTo><BillTo><Address addressID=""><Name xml:lang="en-US">Bill To Address Name</Name></Address></BillTo></OrderRequestHeader>rItemOut</OrderRequest></Request></cXML>"""

var itemOutString = """<ItemOut quantity="rQuality" lineNumber="rLineNumber"><ItemID><SupplierPartID>rSerialNumer</SupplierPartID>
<SupplierPartAuxiliaryID>10-2800</SupplierPartAuxiliaryID></ItemID><ItemDetail><UnitPrice><Money currency="USD">rProductPrice</Money>
</UnitPrice><Description xml:lang="en-US">rDescription</Description>
<UnitOfMeasure>rUnit</UnitOfMeasure><Classification domain="UNSPSC">NA</Classification></ItemDetail></ItemOut>"""

      var block1 = db.run{
        VendorTable.filter(_.name === "McKesson").result
        }map { vendorList =>
         vendorList.map{ vendor =>

         var block = db.run{
              OrderItemTable.filter(_.orderId === orderId).filter(_.productVendorId === vendor.id).result
            } map { orderItemMap =>
              if(orderItemMap.length > 0){

              orderItemMap.map{ result =>
              var staffId: Long = 0
              var block2 = db.run{
                  OrderTable.filter(_.id === orderId).result
                  } map { orderMap =>
                  orderMap.map{orderResult =>
                    staffId = orderResult.staffId
                }
              }
              var AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(20, SECONDS))

                var block1 = db.run{
                  StaffTable.filter(_.id === staffId).result
                  } map { staffMap =>
                  staffMap.map{staffresult =>
                    addressId = convertOptionLong(staffresult.mckessonId)
                }
              }
              var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(20, SECONDS))
              quantity = result.amount.toString
              price =  BigDecimal(result.product.price).setScale(2, BigDecimal.RoundingMode.HALF_UP).toString
              serialNumber = result.product.serialNumber
              description = result.product.name
              unit = convertOptionString(result.product.unit)
              var s = unitsMap.filter(_._2 == unit).keys
              s.map{units =>
              if(unitsMap.contains(units)){
                 unit = units
                 }
              }

              var replaceOrderId = xmlString.replace("rOrderId", orderId.toString)
              var replacePayLoadId = replaceOrderId.replace("rPayloadId", payLoadId)
              var replaceTimeStamp = replacePayLoadId.replace("rTimeStamp", timeStamp)
              var replaceSharesecret = replaceTimeStamp.replace("rShareSecret", mckessonSharedSecret)
              finalXmlString = replaceSharesecret.replace("rAddressId", addressId.toString)

              totalAmount =   (totalAmount + (result.product.price * result.amount))

              line = line + 1
              var replaceQuantity = itemOutString.replace("rQuality",quantity)
              var replaceSerialNo = replaceQuantity.replace("rSerialNumer",serialNumber)
              var replaceUnit = replaceSerialNo.replace("rUnit",unit)
              var replaceDescription = replaceUnit.replace("rDescription",description)
              var replacePrice = replaceDescription.replace("rProductPrice", price)
              var replaceLine = replacePrice.replace("rLineNumber", line.toString)

              finalItemOut = finalItemOut + replaceLine
              }
              var replaceTotalAmt = finalXmlString.replace("rTotalAmt",totalAmount.toString)
              var finalXml = replaceTotalAmt.replace("rItemOut",finalItemOut)

              val response = Http(mckessonOrderUrl).postData(finalXml)
              .header("Content-Type", "text/xml")
              .header("Charset", "UTF-8")
              .option(HttpOptions.readTimeout(100000)).asString

              var replacedStr = response.body.replaceAll("<!DOCTYPE((.|\n|\r)*?)\">", "");

              val itemList = scala.xml.XML.loadString(replacedStr)
              val result = (itemList \ "Response")
              val status = (result \ "Status")
              var code = ""
              status.foreach{ unit =>
              code = (unit \ "@code").text

              if(code == "200"){
                emailService.sendEmail(List(devEmail), from, replyTo, s"McKesson order for ${orderId}  - Success (${code}) (from product controller)", views.html.orderWebservice(s"McKesson order for ${orderId} placed successfully"))
              } else {
                emailService.sendEmail(List(devEmail), from, replyTo, s"McKesson order for ${orderId}  - Failed (${code}) (from product controller)", views.html.orderWebservice(s"McKesson order for ${orderId} failed. Please check and try again."))
              }
                db.run {
                      OrderTable.filter(_.id === orderId).map(_.mckessonStatus).update(Some(code))
                    } map {
                      case 0 => NotFound
                      case _ => Ok
                    }
            }

          }
        }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(20, SECONDS))
         }
       }
       var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(20, SECONDS))
  }
  Future(Ok)
 }

 def mckessonProductImport(offset: Long, pageSize: Long) = silhouette.SecuredAction.async { request => {

   val vSource = application.configuration.getString("mckessonFeedSource").getOrElse(throw new RuntimeException("Configuration is missing `mckessonFeedSource` property."))

    val url = "https://mms.mckesson.com/services/xml/3B7BxHPxCcPkndhipBsynwN4Id7sBR/ItemFeed"

    val xmlString = "<ItemFeedRequest itemType='detail extra availability'><Credentials><Identity>NOVB2B</Identity><SharedSecret>Vgb21ed8f78</SharedSecret><Account>64848730</Account><ShipTo>64848731</ShipTo></Credentials><Feed><Source>"+vSource+"</Source><PageSize>"+pageSize+"</PageSize><Offset>"+offset+"</Offset></Feed></ItemFeedRequest>"

	  val response = Http(url).postData(xmlString.toString)
          .header("Content-Type", "text/xml")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(100000)).asString
          val itemList = scala.xml.XML.loadString(response.body)

          val itemFeedResult = (itemList \ "FeedResult")
          var feedSize = ""
          itemFeedResult.foreach{ unit =>
          feedSize = (unit \ "@feedSize").text
          }

          val items = (itemList \ "FeedResult" \ "ItemOut")
          items.foreach{
            itemRecord => 

               val itemId = (itemRecord \ "ItemId").text
               val itemDesc = (itemRecord \ "ItemDetail" \ "Description").text
               val itemExtra = (itemRecord \ "ItemExtra" \ "ItemImage").text
               var imageUrl = "https://mms.image.mckesson.com/CumulusWeb/Images/High_Res/"+itemExtra

               var value = ""
               var minorValue = ""
               var majorValue = ""
               var stockAvailable = true
               var categoryId: Long = 0
               var parentCategoryId: Long = 0
               val categoryList = itemRecord \ "ItemDetail" \ "Category"
               categoryList.foreach{
                unit =>

                if((unit \ "@type").text == "Minor"){
                  minorValue = unit.text
                }
                if((unit \ "@type").text == "Major"){
                  majorValue = unit.text
                }
               }

               if(minorValue != ""){
                 value = minorValue
               }else{
                 value = majorValue
               }

              // --- FINDING CATEGORY ID
              if(majorValue != ""){
                var block = db.run{
                  CategoryTable.filter(_.parent.isEmpty).filter(_.name === majorValue).result
                } map { majorCategoryList =>
                  if(majorCategoryList.length > 0){
                      parentCategoryId = convertOptionLong(majorCategoryList.head.id)
                  } else {
                      var block2 = db.run{
                          CategoryTable.returning(CategoryTable.map(_.id)) += CategoryRow(None,majorValue,Some(false),null,0)
                        } map {
                            case 0L => PreconditionFailed
                            case id => parentCategoryId = id
                        }
                      var AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              }

              if(minorValue != ""){
                var block = db.run{
                  CategoryTable.filter(_.parent === parentCategoryId).filter(_.name === minorValue).result
                } map { minorCategoryList =>
                  if(minorCategoryList.length > 0){
                      categoryId = convertOptionLong(minorCategoryList.head.id)
                  } else {
                      var block2 = db.run{
                          CategoryTable.returning(CategoryTable.map(_.id)) += CategoryRow(None,minorValue,Some(false),Some(parentCategoryId),0)
                        } map {
                            case 0L => PreconditionFailed
                            case id => categoryId = id
                        }
                      var AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              }

               //fetch units price
               val unitsList = itemRecord \ "ItemAvailability" \ "ItemUom"

               val itemStatus = itemRecord \ "ItemAvailability" \ "ItemStatus"
               val itemStock = itemRecord \ "ItemAvailability" \ "ItemStock"

              var statusCode = ""
              var stockCode = ""
              var available = ""
               itemStatus.foreach{
                 unit=>
                 statusCode = (unit \ "@code").text
                 available = (unit \ "@available").text
               }

               itemStock.foreach{
                 unit=>
                 stockCode = (unit \ "@code").text
               }

               if(statusCode == "ER" || statusCode == "O"){
                 var block = db.run {
                    ProductTable.filter(_.serialNumber === itemId).delete.map {
                      case 0L => NotFound
                      case _ => Ok
                    }
                  }
                  var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
               }

               if(available == "false" || statusCode == "BO" && available == "true" || statusCode == "PB" && available == "true" || statusCode == "SH" && stockCode == "U"){
                  stockAvailable = false
               }

               var vendorID: Long = 0
               var block3 = db.run{
                 VendorTable.filter(_.name === "McKesson").map(_.id).result
               }map{vendorList =>
                vendorList.map{vendor => vendorID = vendor}
               }
               var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

               unitsList.foreach{
                unit => 
                var mckessonPrice = (unit \ "Price").text.toFloat
                var price = mckessonPrice + (mckessonPrice / 100 * 35)
                var listPrice = mckessonPrice + (mckessonPrice / 100 * 8)
                var quantity = (unit \ "@per").text.toLong
                var units = (unit \ "@units").text

              var unitsValue = units
              if(unitsMap.contains(units)){
                unitsValue = unitsMap(units)
              }

            if(mckessonPrice != 0 ){                      //No need to create and update when price comes zero
            var block4 = db.run{
              ProductTable.filter(_.serialNumber === itemId).filter(s=> s.unit === unitsValue || s.unit === units).result
            } map { productResult =>
            var productId: Long = 0
            productResult.map{ result =>
              productId = convertOptionLong(result.id)
            }

            if(productResult.length > 0){
              var block = db.run {
              val query = ProductTable.filter(_.id === productId)
              query.exists.result.flatMap[Result, NoStream, Effect.Write] {
              case true => DBIO.seq[Effect.Write](

            Some(itemDesc).map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(categoryId).map(value => query.map(_.categoryId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(imageUrl).map(value => query.map(_.imageUrl).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(price).map(value => query.map(_.price).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(vendorID).map(value => query.map(_.vendorId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(quantity).map(value => query.map(_.quantity).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(listPrice).map(value => query.map(_.listPrice).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(stockAvailable).map(value => query.map(_.stockAvailable).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(unitsValue).map(value => query.map(_.unit).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map {_ => Ok}
              case false => DBIO.successful(NotFound)
              }
            }
            var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
            } else {

              if(statusCode != "ER" || statusCode != "O"){                    // No need to create for code ER and O
              var productRow = ProductRow(None,itemDesc,Some(""),itemId,Some(imageUrl),vendorID,Some(categoryId),price,Some(quantity),Some(listPrice),Some(""),Some(null),Some(stockAvailable),Some(unitsMap(units)),Some(false))

            var block5 = db.run{
                              ProductTable.returning(ProductTable.map(_.id)) += productRow
                            } map {
                            case 0L => PreconditionFailed
                            case id => Created(Json.toJson(productRow.copy(id = Some(id))))
                          }
              var AwaitResult = Await.ready(block5, atMost = scala.concurrent.duration.Duration(180, SECONDS))
            }
            }
          }
            var AwaitResult1 = Await.ready(block4, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        }
          }

          }

        Future(Ok {Json.obj("offset"-> offset,"pageSize"-> pageSize,"feedSize"-> feedSize)
          })

  }
  }

  def read(id: Long) = silhouette.SecuredAction.async { request => {

    val action = (for {
      (product, fav) <- ProductTable.filter(_.id === id).filterNot(_.deleted) joinLeft CustomerFavouritesTable.filter(_.customer === request.identity.id.get) on(_.id === _.product)
    } yield (product, fav.map(_.id))).groupBy(_._1).map({
      case (products, favourites) => (products, favourites.map(_._2).countDistinct)
    })

    db.run {
      action.result
    } map {

      case data if data.nonEmpty => {
        def product = data.head._1

        var productName = product.name
        if(productName.length > 30){
            productName = productName.substring(0,30)
        }

        var productPrice = Math.round(product.price * 100.0) / 100.0
        var listPrice = Some(Math.round(convertOptionDouble(product.listPrice) * 100.0) / 100.0)

        Ok(Json.toJson(Product(
          product.id,
          product.name,
          product.description,
          product.serialNumber,
          product.imageUrl,
          product.vendorId,
          product.categoryId,
          productPrice,
          "",
          product.quantity,
          Some(if (data.head._2 > 0) true else false),
          listPrice,
          product.document,
          product.video,
          product.stockAvailable,
          product.unit,
          product.financeEligible
        ))
        )
      }
      case _ => NotFound
    }
  }
  }

  def update(id: Long) = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>
    val product = for {
      name <- (request.body \ "name").validateOpt[String]
      description <- (request.body \ "description").validateOpt[String]
      serialNumber <- (request.body \ "serialNumber").validateOpt[String]
      imageUrl <- (request.body \ "imageUrl").validateOpt[String]
      manufacturer <- (request.body \ "manufacturer").validateOpt[String]
      categoryId <- (request.body \ "categoryId").validateOpt[Long]
      price <- (request.body \ "price").validateOpt[Double]
      quantity <- (request.body \ "quantity").validateOpt[Long]
      listPrice <- (request.body \ "listPrice").validateOpt[Double]
      document <- (request.body \ "document").validateOpt[String]
      video <- (request.body \ "video").validateOpt[JsValue]
      stockAvailable <- (request.body \ "stockAvailable").validateOpt[Boolean]
      unit <- (request.body \ "unit").validateOpt[String]
      financeEligible <- (request.body \ "financeEligible").validateOpt[Boolean]
    } yield {
    //   var vStockAvailable: Option[Boolean] = Some(true)
    //   if(stockAvailable != None){
    //     vStockAvailable = stockAvailable
      //}
      (name, description, serialNumber,imageUrl , manufacturer, categoryId, price, quantity, listPrice, document, video,stockAvailable, unit,financeEligible)
    }

    product match {
      case JsSuccess((name, description, serialNumber, imageUrl, manufacturer, categoryId, price, quantity, listPrice, document, video, stockAvailable, unit,financeEligible), _) =>
        db.run {
          val query = ProductTable.filterNot(_.deleted).filter(_.id === id)

          def vendorQuery(manufacturer: String) = {
            VendorTable.filter(_.name === manufacturer).map(_.id).result.headOption flatMap {
              case Some(vendorId: Long) =>
                query.map(_.vendorId).update(vendorId).map(_ => Unit)
              case None =>
                (VendorTable.returning(VendorTable.map(_.id)) += VendorRow(None, manufacturer, None, None, false, None, None, None, None, None, None, None, None)) flatMap { vendorId =>
                  query.map(_.vendorId).update(vendorId).map(_ => Unit)
                }
            }
          }

          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](
                name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                description.map(value => query.map(_.description).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                serialNumber.map(value => query.map(_.serialNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                imageUrl.map(value => query.map(_.imageUrl).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                manufacturer.map(value => vendorQuery(value)).getOrElse(DBIO.successful(Unit)),
                categoryId.map(value => query.map(_.categoryId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                price.map(value => query.map(_.price).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                quantity.map(value => query.map(_.quantity).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                query.map(_.listPrice).update(listPrice),
                document.map(value => query.map(_.document).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                video.map(value => query.map(_.video).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                stockAvailable.map(value => query.map(_.stockAvailable).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                unit.map(value => query.map(_.unit).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                financeEligible.map(value => query.map(_.financeEligible).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }


  def bulkUpdate = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>
    request.body.validate[List[Input.Product]] match {
      case JsSuccess(products, _) =>

        val actions =
          for (product <- products) yield {
            product match {
              case product: Input.NewProduct =>
              var vStockAvailable = true
              if(convertOptionString(product.stockAvailable).toLowerCase == "false"){
                  vStockAvailable = false
              }
                for {
                  vendorId <- VendorTable.filter(_.name === product.manufacturer).map(_.id).result.headOption
                  categoryId <- CategoryTable.filter(_.name === product.categoryName.getOrElse("Unknown")).map(_.id).result.headOption
                  rowsAffected <- ProductTable.filter(_.serialNumber === product.serialNumber).filter(_.unit === product.unit)
                    .map(row => (row.name, row.description, row.imageUrl, row.vendorId, row.categoryId, row.price, row.quantity, row.listPrice,row.document, row.video, row.stockAvailable, row.unit))
                    .update((product.name, product.description, product.imageUrl, vendorId.get, categoryId, product.price, product.quantity, product.listPrice,product.document, product.video, Some(vStockAvailable), product.unit))
                  result <- rowsAffected match {
                    case 0 => ProductTable += ProductRow(None, product.name, product.description, product.serialNumber, product.imageUrl, vendorId.get, categoryId, product.price, product.quantity, product.listPrice, product.document, product.video, Some(vStockAvailable), product.unit, Some(false))
                    case 1 => DBIO.successful()
                    case n => DBIO.successful(Unit)
                  }
                } yield result
            }
          }
        db.run(actions.reduceLeft(_ andThen _).asTry.transactionally) map {
          case Success(()) => Ok
          case Failure(e) => BadRequest(obj("message" -> "products.badCsv", "description" -> "Unknown vendor"))
          case _ => BadRequest(obj("message" -> "products.badCsv", "description" -> "Invaild csv"))
        }

      case JsError(e) =>
        val badRows = e.map(_._1.toString()).mkString(", ").replaceAll("""[()]""", "")
        Future.successful(BadRequest(obj("message" -> "products.badCsv", "description" -> ("Found errors in rows: " + badRows))))
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(UsersOnly).async {
    db.run {
      ProductTable.filterNot(_.deleted).filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }
  
  
  

  def createImageCellTest(path:String,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
    //    val img = Image.getInstance(path);
    //    img.scaleAbsolute(width,height);
    var image:Image = null
    try {
      val bi = ImageIO.read(new URL(path))
      val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
      val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
      val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
      val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
      val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
      val g = img.createGraphics()
      g.drawRenderedImage(bi, at)
      val imgBytes = new ByteArrayOutputStream()
      ImageIO.write(img, "PNG", imgBytes)
      image = Image.getInstance(imgBytes.toByteArray)
      image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
      image.setCompressionLevel(PdfStream.NO_COMPRESSION)
      image.setAlignment(Image.MIDDLE)
    } catch {
      case error: Throwable => logger.debug(error.toString)
    }

    val indentation = 0
    //whatever
//    val img =  Image.getInstance(path);

    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / image.getHeight) * 7

    image.scalePercent(scaler)

    //img.scaleAbsolute(height, width);
    //img.scalePercent(height);
    val cell = new PdfPCell(image);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  def createImageCell(path:String,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
//    val img = Image.getInstance(path);
//    img.scaleAbsolute(width,height);

    val indentation = 0
    //whatever
    val img =  Image.getInstance(path);

    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 7

    img.scalePercent(scaler)

    //img.scaleAbsolute(height, width);
    //img.scalePercent(height);
    val cell = new PdfPCell(img);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  def createImageCell2(img:Image,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
    //    val img = Image.getInstance(path);
    //    img.scaleAbsolute(width,height);

    val indentation = 0
    //whatever
//    val img =  Image.getInstance(path);

    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 7

    img.scalePercent(scaler)

    //img.scaleAbsolute(height, width);
    //img.scalePercent(height);
    val cell = new PdfPCell(img);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  def createImageCell1(path:String,height:Float,width:Float,border:Boolean):PdfPCell ={
    import com.itextpdf.text.Element
    val img = Image.getInstance(path);
    img.scaleAbsolute(width,height);
    //img.scaleAbsolute(height, width);
    //img.scalePercent(height);
    val cell = new PdfPCell(img);
    cell.setPadding(8);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  

  def generatePdf(id:Long)=silhouette.UserAwareAction.async { request =>
//    val formatter = java.text.NumberFormat.getCurrencyInstance

    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    val imgLogo =  Image.getInstance("https://s3.amazonaws.com/ds-static.spfr.co/img/placeholder.jpg");

    //    val currentDirectory = new java.io.File(".").getCanonicalPath

    //    Console.println("currentDirectory == "+currentDirectory)
    db.run {

      if (id != 0){
        ProductTable.filterNot(_.deleted).filter(_.vendorId === id)
          .join(VendorTable).on(_.vendorId === _.id)
          .join(CategoryTable).on(_._1.categoryId === _.id)
          .map({
            case ((products, vendor),category) => (products, vendor.name,category.name)
          })
          .result
      }else{
        ProductTable.filterNot(_.deleted)
          .join(VendorTable).on(_.vendorId === _.id)
          .join(CategoryTable).on(_._1.categoryId === _.id)
          .map({
            case ((products, vendor),category) => (products, vendor.name,category.name)
          })
          .result
      }


    } map { products =>
      val data = products.groupBy(_._1.id).map {
        case (id, items) => {
          val item = items.head._1
          val vendorName = items.head._2
          val categoryName = items.head._3
          val quantity: Long = item.quantity match {
            case None => 0//Or handle the lack of a value another way: throw an error, etc.
            case Some(l: Long) => l //return  to set your value
          }
          val imageUrl: String = item.imageUrl match {
            case None => ""//Or handle the lack of a value another way: throw an error, etc.
            case Some(s: String) => s //return  to set your value
          }
          val price = formatter.format(item.price)
          val listPrice:String = item.listPrice match {
            case None => "$0"
            case Some(l:Double) => formatter.format(l).toString()
          }
          val image_Url = imageUrl.split(',')(0);

          CatalogProduct(
            item.id,
            item.name,
            item.description,
            item.serialNumber,
            image_Url,
            item.vendorId,
            item.categoryId,
            price,
            listPrice,
            vendorName,
            categoryName,
            quantity
          )
        }
      }.toList.sortBy(_.serialNumber)



      val sample = data
      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          //          val ffont = FontFactory.getFont(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", 10)
          //          ffont.setColor(new BaseColor(64, 64, 65));
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }



        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory




          //                    FontFactory.register(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", "Lato Regular")
          //          FontFactory.register(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", "Lato Regular")

          //          val FONT_TITLE = FontFactory.getFont("Lato Regular", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17f, Font.NORMAL, BaseColor.BLACK)
          //          val FONT_TITLE = FontFactory.getFont("/fonts/Lato-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17f, Font.NORMAL, BaseColor.BLACK)
          //          val baseFont = font.getBaseFont

          // Metadata
          //          val FONT_TITLE = FontFactory.getFont(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", 17)
          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          //          val FONT_TITLE = FontFactory.getFont("Lato Regular", 17)

          //          val font = FontFactory.getFont("/fonts/Lato_Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 17f, Font.NORMAL,new BaseColor(64, 64, 65))
          //          val FONT_TITLE = font.getBaseFont

          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

          //          val FONT_COL_HEADER = FontFactory.getFont(currentDirectory+"/app/assets/fonts/Lato-Bold.ttf", 12)
          //          val FONT_CONTENT = FontFactory.getFont(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          /*   val font_path = Thread.currentThread.getContextClassLoader.getResource("fontname")
             FontFactory.register(font_path.toString, "test_font")*/





          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);
          //    /      headerImageTable.setHorizontalAlignment(Element.ALIGN_RIGHT);

          /* var cell = new PdfPCell(new Phrase("Catalog Products", FONT_TITLE));
           cell.setBorder(Rectangle.NO_BORDER);
           cell.setHorizontalAlignment(Element.ALIGN_LEFT);
           cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
           headerImageTable.addCell(cell);*/
          //          headerImageTable.addCell(createImageCell1(currentDirectory+"/app/assets/logo.png",33f,160f,false)).setHorizontalAlignment(Element.ALIGN_RIGHT);
          //          headerImageTable.addCell(createImageCell1("https://s3.amazonaws.com/ds-static.spfr.co/img/logo.png",33f,160f,false)).setHorizontalAlignment(Element.ALIGN_RIGHT);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          //          document.add(headerImageTable);



          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);
          /*  var cell = new PdfPCell(new Phrase("Catalog Products", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            headerTextTable.addCell(cell)
            headerTextTable.setHorizontalAlignment(Element.ALIGN_CENTER);*/
          //          document.add(headerTextTable);

          var cell = new PdfPCell(new Phrase("Catalog Products", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;
          (sample).foreach(row => {

            var sampleData = List(s"${row.manufacturer}", s"${row.categoryName}", s"${row.serialNumber}",s"${row.name}",s"${row.price}",s"${row.listPrice}",s"${row.quantity}",s"${row.imageUrl}")
            var i=1

            var columnWidths:Array[Float] = Array(19,15,16,13,9,9,9,10);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Vendor", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);

              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Category", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Reference Number", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Name", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Price", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("List Price", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Counts", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Image", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingLeft(15);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

              if (i==sampleData.size){
                if (item == ""){
                  cell = createImageCell2(imgLogo,38f,38f,true,document);
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setBorder(Rectangle.TOP);
                  cell.setUseVariableBorders(true)
                  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                  cell.setPaddingLeft(15);
                  //cell.setBorderColorTop(BaseColor.GREEN);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  table.addCell(cell);
                }else{
                  cell = createImageCellTest(item,38f,38f,true,document);
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setBorder(Rectangle.TOP);
                  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                  cell.setPaddingLeft(15);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
                }
              }else{
                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                cell.setPaddingLeft(5)
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setUseVariableBorders(true);
                if (i==7){
                  cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                }else if (i==6){
                  cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                }
                table.addCell(cell);
              }
              i=i+1
            })

            document.add(table);
            //Console.println("recNum == "+recNum)
            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              //Console.println("newPage == ")
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 10
            }
          })



          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"products"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

 def generateCSV(vendorId: Option[Long]) = silhouette.SecuredAction { request =>

    var vendorName: String = ""
    var categoryName: String = ""
    var referenceNumber: String = ""
    var name: String = ""
    var retailPrice: String = ""
    var novaPrice: String = ""
    var countNumber: String = ""
    var imageData: String = ""
    var csvList = List.empty[ProductCSVList]
    var query = ProductTable.filterNot(_.deleted)

    if(convertOptionLong(vendorId) != 0){
      query = query.filter(_.vendorId === vendorId)
    }

    var productDataList = db.run {
            query.join(VendorTable).on(_.vendorId === _.id)
              .join(CategoryTable).on(_._1.categoryId === _.id)
              .map({
                case ((products, vendor), category) => (products, vendor.name, category.name)
              })
              .result

    } map { products => 

        val data = products.groupBy(_._1.id).map {
            case (id, items) => {
                val item = items.head._1
                val vendorDetail = items.head._2
                val categoryDetail = items.head._3

                vendorName = vendorDetail
                categoryName = categoryDetail
                referenceNumber = item.serialNumber.toString
                name = item.name
                retailPrice = item.price.toString
                novaPrice = convertOptionDouble(item.listPrice).toString
                countNumber = convertOptionLong(item.quantity).toString
                imageData = convertOptionString(item.imageUrl).toString

                var dataList = ProductCSVList(vendorName, categoryName, referenceNumber, name, retailPrice, novaPrice, countNumber, imageData)
                csvList = csvList :+ dataList              
            }
        } 
      }


    val AwaitResult = Await.ready(productDataList, atMost = scala.concurrent.duration.Duration(120, SECONDS))
    //Console.println(csvList)
    val file = new java.io.File("/tmp/catalog.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Vendor", "Category", "Reference Number", "Name", "Retail Price", "Novadontics Price", "Counts", "Image"))
    csvList.map {
      productList => writer.writeRow(List(productList.vendorName, productList.categoryName, productList.referenceNumber, productList.name, productList.retailPrice, productList.novaPrice, productList.countNumber, productList.imageData))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
} 

def productVideoMigrate = silhouette.SecuredAction.async { request => 
  db.run {
      for{
          products <- ProductTable.result
      } yield {
            (products).foreach( product => {
                var videos = product.video
                var newVideo = new JsArray
                if(product.video.get != JsNull && product.video.get != None && product.video.get != "" ){
                var temp = videos.get.toString  
                if(temp.contains("url")){
                val jsonObject = Json.parse(temp)
                for(i<-0 to jsonObject.as[JsArray].value.size-1){
                val mStabilityValues  =  jsonObject(i)
                val res = jsonObject(i).as[JsObject] - "thumbnail"
                 newVideo = newVideo :+ res
                 }
                val videoUrl = newVideo.as[JsValue]
                var block =  db.run {
                val query = ProductTable.filter(_.id === product.id)
                query.map(_.video).update(Some(videoUrl))
                } map {
                case 0 => NotFound
                case _ => Ok
                }
                var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
            }
          }
        })
      
        Ok { Json.obj("status"->"success","message"->"Product video Migrated Successfully")}
        }
     }
    }

}
