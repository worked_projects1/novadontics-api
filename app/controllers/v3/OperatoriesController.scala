
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffRow,OperatorTableRow}
import play.api.mvc.Result


class OperatoriesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


  implicit val OperatorFormat = Json.format[OperatorTableRow]

 implicit val operatorRowWrites = Writes { request: (OperatorTableRow) =>
    val (operatorRow) = request

  Json.obj(
      "id" -> operatorRow.id,
      "practiceId" -> operatorRow.practiceId,
      "name" -> operatorRow.name
      )
  }

 
  def readAll = silhouette.SecuredAction.async { request =>
    db.run {
        OperatorTable.filterNot(_.deleted).filter(_.practiceId === request.identity.asInstanceOf[StaffRow].practiceId)
        .sortBy(_.order).result
    } map { operatorsList =>
      Ok(Json.toJson(operatorsList))
    }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var operatories = false
    val operators = for {

      name <- (request.body \ "name").validate[String]
    } yield {
      var sOrder: Long = 0
      var block = db.run{
        OperatorTable.filterNot(_.deleted).filter(_.practiceId === practiceId).sortBy(_.order.desc).result
      } map { s =>
      if(s.length >= 20){
          operatories = true
      }
        sOrder = s.head.order + 1
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      OperatorTableRow(None, practiceId, name,false,sOrder)
    }

    operators match {
      case JsSuccess(operatorRow, _) => 
      if(!operatories){
          db.run {   
            OperatorTable.returning(OperatorTable.map(_.id)) += operatorRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(operatorRow.copy(id = Some(id))))
                }
      } else {
              val obj = Json.obj()
              val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Reached Maximum Limit"))
              Future(PreconditionFailed{Json.toJson(Array(newObj))})
      }
      case JsError(_) => Future.successful(BadRequest)
    }

  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val operators = for {
      practiceId <- (request.body \ "practiceId").validateOpt[Long]
      name <- (request.body \ "name").validateOpt[String]
    } yield {
      (practiceId, name)
      
    }

    operators match {
      case JsSuccess((practiceId,name), _) => db.run {
        val query = OperatorTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            practiceId.map(value => query.map(_.practiceId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def delete(id: Long) = silhouette.SecuredAction.async { request =>
    val currentDate: LocalDate = LocalDate.now()
    var operatoriesValue: Boolean = false
    val count = db.run{
       for {
            operatoriesLength <- AppointmentTable.filterNot(_.deleted).filterNot(s => s.status.toLowerCase === "canceled" || s.status.toLowerCase === "no show")
            .filter(_.operatoryId === id).filter(_.date >= currentDate).length.result
       } yield {
            if(operatoriesLength > 0){
              operatoriesValue = true
            }
       }
    }
    var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(operatoriesValue){
              val obj = Json.obj()
              val newObj = obj + ("message" -> JsString("Appointment scheduled for this operatory!"))
              Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else{
       db.run {
         OperatorTable.filter(_.id === id).map(_.deleted).update(true)
        } map {
        case 0 => NotFound
        case _ => Ok
        }
      }
  }

def operatorReorder = silhouette.SecuredAction.async(parse.json) { request =>
var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val operators = for {
      operatorId <- (request.body \ "operatorId").validate[List[Int]]
    } yield {
      var order:Long = 1
      (operatorId).foreach(sId => {
        db.run{
          OperatorTable.filter(_.id === sId.toLong).map(_.order).update(order)
        }
        order = order + 1
      })
    }
  Future(Ok)
  }
}


