package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{ReferredToRow,StaffRow}
import play.api.mvc.Result


class ReferredToController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


implicit val ReferredToFormat = Json.format[ReferredToRow]

 def readAll = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
        ReferredToTable.filterNot(_.deleted).filter(_.practiceId === practiceId).result
    } map { referredToList =>
      Ok(Json.toJson(referredToList))
    }
  }

def create = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var isInputMissing = false
    var isAlreadyAvailable = false
    val referredTo = for {

      name <- (request.body \ "name").validateOpt[String]
      types <- (request.body \ "types").validate[String]
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
    } yield {

      var sName = convertOptionString(name)
      var sTitle = convertOptionString(title)
      var sFirstName = convertOptionString(firstName)
      var sLastName = convertOptionString(lastName)

      if(types == "ER" || types == "Lab" || types == "Hospital" || types == "MRI Lab"){
          if(sName != "" && sName != None){
              sTitle = ""
              sFirstName = ""
              sLastName = ""
          }
          else{
              isInputMissing = true
          }
        } else {
          if(sFirstName != "" && sLastName != "" && sTitle != ""){
            sName = sTitle + " " +  sFirstName + " " + sLastName
          } else {
            isInputMissing = true
          }
        }

          var block = db.run{
              ReferredToTable.filterNot(_.deleted).filter(_.practiceId === practiceId).filter(_.types === types).filter(_.name === sName).result
          } map { result =>
          if(result.length > 0){
              isAlreadyAvailable = true
          }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      ReferredToRow(None,practiceId,sName,types,sTitle,sFirstName,sLastName, false)
    }

    referredTo match {
      case JsSuccess(referredToRow, _) => 
        if(isInputMissing){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("Please provide required details!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})
        } else if(isAlreadyAvailable){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("This referred To value already exist!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})

        } else {

            db.run {
              ReferredToTable.returning(ReferredToTable.map(_.id)) += referredToRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(referredToRow.copy(id = Some(id))))
                }
        }
      case JsError(_) => Future.successful(BadRequest)
    }

  }

 def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  var isAlreadyAvailable = false
  var isInputMissing = false
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

    val referredTo = for {
      name <- (request.body \ "name").validateOpt[String]
      types <- (request.body \ "types").validateOpt[String]
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
    } yield {
            
      var sName = convertOptionString(name)
      var sTitle = convertOptionString(title)
      var sFirstName = convertOptionString(firstName)
      var sLastName = convertOptionString(lastName)

        if(types == Some("ER") || types == Some("Lab") || types == Some("Hospital") || types == Some("MRI Lab")){
          if(sName != "" && sName != None){
              sTitle = ""
              sFirstName = ""
              sLastName = ""
          }
          else{
              isInputMissing = true
          }
        } else {
          if(sFirstName != "" && sLastName != "" && sTitle != ""){
            sName = sTitle + " " +  sFirstName + " " + sLastName
          } else {
            isInputMissing = true
          }
        }


        var block = db.run{
              ReferredToTable.filter(_.practiceId === practiceId).filter(_.types === types).filter(_.name === sName).result
          } map { result =>
          if(result.length > 0){
              isAlreadyAvailable = true
          }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))


      (sName,types,title,firstName,lastName)
    }

    referredTo match {
      case JsSuccess((sName,types,title,firstName,lastName), _) =>
      if(isAlreadyAvailable){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("This referred To value already exist!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})

      } 
      else if(isInputMissing){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("Please provide required details!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})
        } else {
      db.run {
        val query = ReferredToTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            Some(sName).map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            types.map(value => query.map(_.types).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            firstName.map(value => query.map(_.firstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            lastName.map(value => query.map(_.lastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

    def delete(id: Long) = silhouette.SecuredAction.async { request =>
  db.run {
         ReferredToTable.filter(_.id === id).map(_.deleted).update(true)
        } map {
        case 0 => NotFound
        case _ => Ok
        }
 }
}