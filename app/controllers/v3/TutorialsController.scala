
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{TutorialsRow, StaffRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import scala.util.Success

class TutorialsController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val TutorialsFormat = Json.format[TutorialsRow]

    object Input {
    trait Tutorial
    case class PlainTutorial(id: Long, order: Option[Long]) extends Tutorial
    case class NewTutorial(name: String, link: String, children: Option[List[Tutorial]]) extends Tutorial
    case class UpdatedTutorial(id: Long, order: Option[Long], name: Option[String], children: Option[List[Tutorial]]) extends Tutorial

    object Tutorial {
      lazy val reads = Reads

      implicit object Reads extends Reads[Tutorial] {
        override def reads(json: JsValue) = try {
          for {
              idOpt <- (json \ "id").validateOpt[Long]
              orderOpt <- (json \ "order").validateOpt[Long]
              nameOpt <- (json \ "name").validateOpt[String]
              linkOpt<- (json \ "link").validateOpt[String]
              childrenOpt <- (json \ "children").validateOpt[List[Tutorial]]
            } yield {

              (idOpt, orderOpt, nameOpt, linkOpt, childrenOpt) match {
                case (Some(id), order, name,linkOpt, children) if name.isDefined =>
                  UpdatedTutorial(id, order, name, children)
                case (Some(id), order, _, link, _) =>
                  PlainTutorial(id, order)
                case (_, _, Some(name), link, children) =>
                  NewTutorial(name, convertOptionString(link), children)

              }
            }
          } catch {
            case _: MatchError => JsError()
          }
        }
    }
  }

case class ChildrensRow(
  id: Option[Long],
  name: String,
  link: String,
  parent: Option[Long],
  order: Long,
  sectionType: Option[String],
  children: List[JsValue]
)

implicit val childrenRowWrites = Writes { childrenRow: ChildrensRow =>
    Json.obj(
      "id" -> childrenRow.id,
      "name" -> childrenRow.name,
      "link" -> childrenRow.link,
      "parent" -> childrenRow.parent,
      "order" -> childrenRow.order,
      "sectionType" -> childrenRow.sectionType,
      "children" -> childrenRow.children
    )
  }

   def readAll = silhouette.SecuredAction.async { request =>
   var newObj : JsValue = null
    var tutorialList = List.empty[ChildrensRow]
     db.run {
          TutorialsTable.result } map { tutorialsMap =>
          (tutorialsMap).foreach(result => {
            if(result.parent == None){
            var childObj : List[JsValue] = List()
            var f = db.run{
                TutorialsTable.filter(_.parent === result.id).result
            } map { parentList =>
              (parentList).foreach(s => {
                newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id))) + ("name" -> JsString(s.name)) + ("link" -> JsString(s.link)) + ("parent" -> JsNumber(convertOptionLong(s.parent))) + ("order" -> JsNumber(s.order)) + ("sectionType" -> JsString(convertOptionString(s.sectionType)))
                childObj = childObj :+ newObj
              })
            }
            Await.ready(f, atMost = scala.concurrent.duration.Duration(60, SECONDS))

            var dataList = ChildrensRow(result.id, result.name, result.link, result.parent, result.order, result.sectionType, childObj)
              tutorialList  = tutorialList  :+ dataList
            }
          })
        Ok(Json.toJson(tutorialList.sortBy(_.id)))
      }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
 var order: Long = 0
    val tutorials = for {
      name <- (request.body \ "name").validate[String]
      link <- (request.body \ "link").validateOpt[String]
      parent <- (request.body \ "parent").validateOpt[Long]
      sectionType <- (request.body \ "sectionType").validateOpt[String]

    } yield {
      var block = db.run{
        TutorialsTable.filter(_.parent === parent).length.result
      } map { tutorialsCount =>
        order = tutorialsCount
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        TutorialsRow(None,name,convertOptionString(link),parent,order,sectionType)
    }

    tutorials match {
      case JsSuccess(tutorialsRow, _) =>
      db.run{
       TutorialsTable.returning(TutorialsTable.map(_.id)) += tutorialsRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(tutorialsRow.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }

  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val tutorials = for {
      name <- (request.body \ "name").validate[String]
      link <- (request.body \ "link").validateOpt[String]
      parent <- (request.body \ "parent").validateOpt[Long]
      order <- (request.body \ "order").validateOpt[Long]
      sectionType <- (request.body \ "sectionType").validateOpt[String]
      } yield {
      (name,link,parent,order,sectionType)
    }

    tutorials match {
      case JsSuccess((name,link,parent,order,sectionType), _) => db.run {

        val query = TutorialsTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(name).map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            link.map(value => query.map(_.link).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(parent).map(value => query.map(_.parent).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            order.map(value => query.map(_.order).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(sectionType).map(value => query.map(_.sectionType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      TutorialsTable.filter(_.id === id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
}

  def bulkUpdate = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>
    request.body.validate[List[Input.Tutorial]] match {
      case JsSuccess(tutorials, _) => {

        def parse(tutorials: List[Input.Tutorial], parent: Option[Long]): List[DBIOAction[Unit, NoStream, Effect.All]] = {
          for (cat <- tutorials) yield {
            cat match {
              case cat: Input.PlainTutorial => for {
                _ <- TutorialsTable.filter(_.id === cat.id).map(a => (a.order))
                  .update((cat.order.get))
              } yield ()

              case cat: Input.UpdatedTutorial => for {
                _ <- TutorialsTable.filter(_.id === cat.id).map(a => (a.order, a.parent, a.name))
                  .update((cat.order.getOrElse(tutorials.indexOf(cat)), parent, cat.name.get))
                _ <- cat.children.nonEmpty && cat.children.get.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Option(cat.id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()

              case cat: Input.NewTutorial => for {
                cat_id <- TutorialsTable returning TutorialsTable.map(_.id) += TutorialsRow(None, cat.name, cat.link, parent, 0,Some(""))
                _ <- cat.children.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Some(cat_id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()
            }
          }
        }

        val actions = parse(tutorials, None: Option[Long])
        if (actions.nonEmpty) {
          db.run(actions.reduceLeft(_ andThen _).asTry.transactionally) map (s => s match {
            case Success(()) => Ok
            case _ => BadRequest
          })
        }
        else {
          Future.successful(BadRequest)
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

}


