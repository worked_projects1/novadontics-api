package controllers.v3

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.ResourceCategoryRow
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

@Singleton
class ResourceCategoriesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val categoryFormat = Json.format[ResourceCategoryRow]

  object ResourceType {
    private val types = List("courses", "documents", "ciicourse","prescriptions","vendorGroups","procedureCodeCategory")
    private val defaultType = "unknown"

    def get(resourceType: String): String = (types.contains(resourceType)) match {
      case true => resourceType
      case false => defaultType
    }
  }

  object Input {

    trait ResourceCategory

    case class PlainResourceCategory(id: Long, order: Option[Int]) extends ResourceCategory

    case class NewResourceCategory(name: String, color: Option[String], children: Option[List[ResourceCategory]]) extends ResourceCategory

    case class UpdatedResourceCategory(id: Long, order: Option[Int], name: Option[String], color: Option[String], children: Option[List[ResourceCategory]]) extends ResourceCategory

    object ResourceCategory {
      lazy val reads = Reads

      implicit object Reads extends Reads[ResourceCategory] {
        override def reads(json: JsValue) = try {
          for {
            idOpt <- (json \ "id").validateOpt[Long]
            orderOpt <- (json \ "order").validateOpt[Int]
            nameOpt <- (json \ "name").validateOpt[String]
            colorOpt <- (json \ "color").validateOpt[String]
            childrenOpt <- (json \ "children").validateOpt[List[ResourceCategory]]
          } yield {
            (idOpt, orderOpt, nameOpt, colorOpt, childrenOpt) match {
              case (Some(id), order, name, color, children) if name.isDefined =>
                UpdatedResourceCategory(id, order, name, color, children)
              case (Some(id), order, _, _, _) =>
                PlainResourceCategory(id, order)
              case (_, _, Some(name), Some(color), children) =>
                NewResourceCategory(name, Some(color), children)
            }
          }
        } catch {
          case _: MatchError => JsError()
        }
      }

    }

  }

  object Output {

    case class Category(id: Long, name: String, order: Int, children: Option[List[Category]], resourceType: String, color: Option[String])

    object Category {
      implicit val writes: Writes[Category] = Writes { category =>
        Json.obj(
          "id" -> category.id,
          "name" -> category.name,
          "order" -> category.order,
          "resourceType" -> category.resourceType,
          "color" -> category.color
        ) ++ (category.children match {
          case Some(children) =>
            Json.obj(
              "children" -> JsArray(children.map(c => writes.writes(c)))
            )
          case None =>
            Json.obj()
        })
      }
    }

  }

  /*def readAll(resourceType: String) = silhouette.SecuredAction.async { request =>
    Console.println("resource categories readAll == ")
    db.run{
      ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).sortBy(_.order).result
    } map {resourceCategories => Ok(Json.toJson(resourceCategories))}
  }*/

  def readAll(resourceType: String) = silhouette.SecuredAction.async { request =>
    val action = ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).sortBy(_.order.desc.nullsFirst)


    db.run {
      action.result
    } map { categories =>
      val childrenOf = categories.foldLeft(Map.empty[Long, List[(ResourceCategoryRow)]].withDefaultValue(Nil)) { (acc, category) =>
        val id = category.parent.getOrElse(0L)
        acc.updated(id, category :: acc(id))
      }

      def buildTrees(rows: List[(ResourceCategoryRow)]): List[Output.Category] = {
        rows.map { (row: (ResourceCategoryRow)) =>
          val id: Long = row.id.get
          val children: scala.List[ResourceCategoriesController.this.Output.Category] = buildTrees(childrenOf(id))
          Output.Category.apply(id, row.name, row.order, if (children.isEmpty) scala.None else Some.apply(children), row.resourceType, row.color)
        }
      }

      Ok(Json.toJson(buildTrees(childrenOf(0))))
    }
  }

  def read(resourceType: String, id: Long) = silhouette.SecuredAction.async { request =>
    db.run {
      ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === id).result.headOption
    } map {
      case Some(resourceCategory) => Ok(Json.toJson(resourceCategory))
      case None => NotFound
    }
  }

  def readSubCategories(resourceType: String, categoryId: Long) = silhouette.SecuredAction.async { request =>
    db.run {
      ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.parent === categoryId).result
    } map { resourceCategory =>
      Ok(Json.toJson(resourceCategory))
    }
  }

  def create(resourceType: String) = silhouette.SecuredAction(AdminOnly).async(parse.json) {
    request =>
      val resourceCategory = for {
        name <- (request.body \ "name").validate[String]
        categoryId <- (request.body \ "categoryId").validateOpt[String]
        order <- (request.body \ "order").validateOpt[String]
        color <- (request.body \ "color").validateOpt[String]
      } yield {
        val tempOrder: Int = order match {
          case None => 0
          case Some(l: String) => l.toInt + 1
        }

        if (categoryId.equals("")) {
          ResourceCategoryRow(None, name, ResourceType.get(resourceType), 0, None, color)
        } else {
          ResourceCategoryRow(None, name, ResourceType.get(resourceType), tempOrder, Some(convertStringToLong(categoryId)), color)
        }
      }

      resourceCategory match {
        case JsSuccess(resourceCategoryRow, _) => db.run {
          ResourceCategoryTable.returning(ResourceCategoryTable.map(_.id)) += resourceCategoryRow
        } map {
          case 0L => PreconditionFailed
          case id => Created(Json.toJson(resourceCategoryRow.copy(id = Some(id))))
        }
        case JsError(_) => Future.successful(BadRequest)
      }
  }

  def update(resourceType: String, id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val resourceCategory = for {
      name <- (request.body \ "name").validateOpt[String]
      color <- (request.body \ "color").validateOpt[String]

    } yield {
      (name,color)
    }

    resourceCategory match {
      case JsSuccess((name,color), _) => db.run {
        val query = ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(color).map(value => query.map(_.color).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def bulkUpdate(resourceType: String) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    request.body.validate[List[Input.ResourceCategory]] match {
      case JsSuccess(resourceCategories, _) => {

        def parse(resourceCategories: List[Input.ResourceCategory], parent: Option[Long]): List[DBIOAction[Unit, NoStream, Effect.All]] = {
          // TODO: Burn and rewrite
          for (cat <- resourceCategories) yield {
            cat match {
              case cat: Input.PlainResourceCategory => for {
                _ <- ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === cat.id).map(_.order).update(cat.order.get)
              } yield ()

              case cat: Input.UpdatedResourceCategory => for {
                _ <- ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === cat.id).map(a => (a.order, a.name, a.parent))
                  .update((cat.order.getOrElse(resourceCategories.indexOf(cat)), cat.name.get, parent))
                _ <- cat.children.nonEmpty && cat.children.get.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Option(cat.id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()

              case cat: Input.NewResourceCategory => for {
                cat_id <- ResourceCategoryTable returning ResourceCategoryTable.map(_.id) += ResourceCategoryRow(None, cat.name, ResourceType.get(resourceType), 0, parent, cat.color)
                _ <- cat.children.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Some(cat_id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()
            }
          }
        }

        val actions = parse(resourceCategories, None: Option[Long])
        if (actions.nonEmpty) {
          db.run(actions.reduceLeft(_ andThen _).asTry.transactionally) map (s => s match {
            case Success(()) => Ok
            case _ => BadRequest
          })
        }
        else {
          Future.successful(BadRequest)
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }


  def delete(resourceType: String, id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      val query = ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === id)
      query.map(_.name).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
        case Some(category) =>
          CoursesTable.filter(_.category === category).exists.result.flatMap {
            case true => DBIO.successful(BadRequest("There are still courses under this category"))
            case false => ResourceCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id === id).delete.map {
              case 0L => NotFound
              case _ => Ok
            }
         }
      }
    }
  }

}
