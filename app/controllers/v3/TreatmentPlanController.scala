
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import play.api.libs.json.Json
import javax.inject._
import models.daos.tables.TreatmentPlanRow
import play.api.mvc.Result
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};
import java.time.LocalTime
import java.text.SimpleDateFormat


class TreatmentPlanController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


  //implicit val TreatmentPlanFormat = Json.format[TreatmentPlanRow]

  implicit val TreatmentPlanRowWrites = Writes { TreatmentPlanRecord: (TreatmentPlanRow) =>
    val (TreatmentPlan) = TreatmentPlanRecord
    var formatter: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    Json.obj(
      "id" -> TreatmentPlan.id,
      "patientId" -> TreatmentPlan.patientId,
      "dateCreated" -> formatter.print(TreatmentPlan.dateCreated),
      "dateUpdated" -> formatter.print(TreatmentPlan.dateUpdated),
      "planName" -> TreatmentPlan.planName,
      "status" -> TreatmentPlan.status,
      "createdBy" -> TreatmentPlan.createdBy
    )
  }


  def readAll(patientId: Long) = silhouette.SecuredAction.async { request =>

      db.run {
            TreatmentPlanTable.filterNot(_.deleted).filter(_.patientId === patientId).sortBy(r => (r.dateUpdated.desc, r.dateCreated.desc)).result
      } map { treatmentPlanList =>
        Ok(Json.toJson(treatmentPlanList))
      }
  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
    val treatmentPlan = for {
      patientId <- (request.body \ "patientId").validate[Long]
      planName <- (request.body \ "planName").validate[String]
      //status <- (request.body \ "status").validate[String]
      dateCreated <- (request.body \ "dateCreated").validate[LocalDate]
      createdBy <- (request.body \ "createdBy").validate[String]
    } yield {
     
      TreatmentPlanRow(None,patientId,dateCreated,LocalDate.now(),planName,status = "Proposed",false,createdBy)
    }
    
    treatmentPlan match {
      case JsSuccess(treatmentPlanRow, _) => db.run {
        TreatmentPlanTable.returning(TreatmentPlanTable.map(_.id)) += treatmentPlanRow
       
      } map {
        case 0L => PreconditionFailed
        case id => db.run {
                  TreatmentTable.filterNot(_.deleted).filter(_.id === id).map(_.treatmentStarted).update(true)
              }map{ plan =>
                Ok
              }

        Created(Json.toJson(treatmentPlanRow.copy(id = Some(id))))

      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  
  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val treatmentPlan = for {
      patientId <- (request.body \ "patientId").validateOpt[Long]
      planName <- (request.body \ "planName").validateOpt[String]
      status <- (request.body \ "status").validateOpt[String]
      dateCreated <- (request.body \ "dateCreated").validateOpt[LocalDate]
      createdBy <- (request.body \ "createdBy").validateOpt[String]
    } yield {
      
      (patientId,LocalDate.now(),planName,status,createdBy,dateCreated)
    } 
    
    treatmentPlan match {
      case JsSuccess((patientId,dateUpdated,planName,status,createdBy,dateCreated), _) =>
       db.run {
          val query = TreatmentPlanTable.filterNot(_.deleted).filter(_.id === id)

          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](
                patientId.map(value => query.map(_.patientId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                query.map(_.dateUpdated).update(dateUpdated),
                planName.map(value => query.map(_.planName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                dateCreated.map(value => query.map(_.dateCreated).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                createdBy.map(value => query.map(_.createdBy).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  

    def delete(id: Long) = silhouette.SecuredAction.async {
    db.run {
      TreatmentPlanTable.filterNot(_.deleted).filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => db.run {
                  TreatmentTable.filterNot(_.deleted).filter(_.id === id).map(_.treatmentStarted).update(false)
              }map{ plan =>
                 Ok
              }
              Ok
    }
  }
}


