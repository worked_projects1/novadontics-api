
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{ReferralSourceRow,StaffRow}
import play.api.mvc.Result


class ReferralSourceController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


implicit val ReferralSourceFormat = Json.format[ReferralSourceRow]

 def readAll = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
        ReferralSourceTable.filterNot(_.archived).filter(_.practiceId === practiceId).result
    } map { referralSourceList =>
      Ok(Json.toJson(referralSourceList))
    }
  }

def create = silhouette.SecuredAction.async(parse.json) { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var isInputMissing = false
    var isAlreadyAvailable = false
    val referralSource = for {

      value <- (request.body \ "value").validateOpt[String]
      types <- (request.body \ "types").validate[String]
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
    } yield {

      var sValue = convertOptionString(value)
      var sTitle = convertOptionString(title)
      var sFirstName = convertOptionString(firstName)
      var sLastName = convertOptionString(lastName)

      if(types == "patient_referral" || types == "doctor_referral"){
        sValue = ""
        if(sFirstName != "" && sLastName != ""){
            if(sTitle != ""){
            sTitle = sTitle + " "
          }
          sValue = sTitle + sFirstName + " " + sLastName
        } else {
          isInputMissing = true
        }

        } else if(types == "walk-in"){
          sValue = "Walk-In"
        } else {
          sTitle = ""
          sFirstName = ""
          sLastName = ""
          if(sValue == ""){
            isInputMissing = true
          }
        }
          var block = db.run{
              ReferralSourceTable.filterNot(_.archived).filter(_.practiceId === practiceId).filter(_.types === types).filter(_.value === sValue).result
          } map { result =>
          if(result.length > 0){
              isAlreadyAvailable = true
          }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      ReferralSourceRow(None,practiceId,sValue,types,sTitle,sFirstName,sLastName, false)
    }

    referralSource match {
      case JsSuccess(referralSourceRow, _) => 
        if(isInputMissing){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("Please provide required details!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})
        } else if(isAlreadyAvailable){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("This referral source value already exist!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})

        } else {

            db.run {
              ReferralSourceTable.returning(ReferralSourceTable.map(_.id)) += referralSourceRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(referralSourceRow.copy(id = Some(id))))
                }
        }
      case JsError(_) => Future.successful(BadRequest)
    }

  }

 
 def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  var isAlreadyAvailable = false
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

    val referralSource = for {
      value <- (request.body \ "value").validateOpt[String]
      types <- (request.body \ "types").validateOpt[String]
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
    } yield {
            
        var sValue = value
        var sTitle = ""

      if(types == Some("patient_referral") || types == Some("doctor_referral")){
        if(title != None && convertOptionString(title) != ""){
          sTitle = convertOptionString(title) + " "
        }
          sValue = Some( sTitle + convertOptionString(firstName) + " " + convertOptionString(lastName))
        } else if(types == Some("walk-in")){
            sValue = Some("Walk-In")
        }

        var block = db.run{
              ReferralSourceTable.filter(_.practiceId === practiceId).filter(_.types === types).filter(_.value === sValue).result
          } map { result =>
          if(result.length > 0){
              isAlreadyAvailable = true
          }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))


      (sValue,types,title,firstName,lastName)
    }

    referralSource match {
      case JsSuccess((sValue,types,title,firstName,lastName), _) =>
      if(isAlreadyAvailable){
            val obj = Json.obj()
            val newObj = obj + ("result" -> JsString("failed")) + ("message" -> JsString("This referral source value already exist!"))
            Future(PreconditionFailed{Json.toJson(Array(newObj))})

      } else {
      db.run {
        val query = ReferralSourceTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            sValue.map(value => query.map(_.value).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            types.map(value => query.map(_.types).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            firstName.map(value => query.map(_.firstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            lastName.map(value => query.map(_.lastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

 def unsecuredReferralSource(patientId: Long) = silhouette.UnsecuredAction.async { request =>
    db.run {
        ReferralSourceTable.filterNot(_.archived).join(TreatmentTable.filter(_.id === patientId)).on(_.practiceId === _.practiceId).result
    } map { referralSourceList =>
      val data = referralSourceList.groupBy(_._1.id).map {
      case (treatmentId, entries) =>
      var referralSource = entries.head._1
      ReferralSourceRow(
        referralSource.id,
        referralSource.practiceId,
        referralSource.value,
        referralSource.types,
        referralSource.title,
        referralSource.firstName,
        referralSource.lastName,
        referralSource.archived
      )
      }.toList
      Ok(Json.toJson(data))
    }
  }

def referralTitleMigration = silhouette.SecuredAction.async { request =>
   db.run { 
     TreatmentTable.filterNot(_.deleted).result 
      } map { treatmentList => 
        treatmentList.foreach(treatment =>{
              var referralSourceData = convertOptionString(treatment.referralSource)
              if(referralSource.contains (referralSourceData)){
                 db.run{
                    TreatmentTable.filter(_.id === treatment.id).map(_.referralSource).update(Some(referralSource(referralSourceData)))
                    }map{
                    case 0L => PreconditionFailed
                    case id => Ok
                  } 
              } 
          }
        )  
      Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
      }
    
  }


def referralSourceCreateMigration = silhouette.SecuredAction.async { request =>
    db.run {
        PracticeTable.result
    } map { practiceList => practiceList.map{ result=>
      val mediaList = List("Billboard","Facebook","Google Search","Instagram","LinkedIn","Magazine Ad","Newspaper Ad","Postcard","Printed Material","Radio","Television","Twitter","Yelp","YouTube")
      mediaList.foreach(listValue => {
            
            migrateReferralSource(ReferralSourceRow(None,convertOptionLong(result.id),listValue,"media","","","",false))
      })

      val walkInList = List("Walk-In")
      walkInList.foreach(listValue => {
            
            migrateReferralSource(ReferralSourceRow(None,convertOptionLong(result.id),listValue,"walk-in","","","",false))
      })
    }

      Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
    }
  }

def migrateReferralSource(referralSourceRow : ReferralSourceRow){
        var block = db.run {   
            ReferralSourceTable.returning(ReferralSourceTable.map(_.id)) += (referralSourceRow)
             } map {
                case 0L => PreconditionFailed
                case id => Ok(Json.obj( "status"->"success","message"->"Form Migrated successfully"))
            }
            val AwaitResult2 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }


def treatmentRefSourceIdMigration = silhouette.SecuredAction.async { request =>
   db.run { TreatmentTable.filterNot(_.deleted).result 
   } map { result => 
    result.map{ treatment => 

        var block1 = db.run{
          StaffTable.filter(_.id === treatment.staffId).result
        } map { staffDetails => 
          staffDetails.map{staffResult => 

         var block2 = db.run{
            ReferralSourceTable.filter(_.practiceId === staffResult.practiceId).filter(_.value === treatment.referralSource).result
              }map{ referralDetails =>
                referralDetails.map{referralResult => 
          
            db.run{
             TreatmentTable.filter(_.id === treatment.id).map(_.referralSourceId).update(referralResult.id)
                    }map{
                    case 0L => PreconditionFailed
                    case id => Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
                  } 
          }

        }
        val AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        
        }

        }
        val AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    }
      Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
    }
  }


def migrateStepReferralSource = silhouette.SecuredAction.async { request =>
   db.run { TreatmentTable.filterNot(_.deleted).result 
    } map { treatmentResult => 
      treatmentResult.map{ treatment => 

        var block1 = db.run{
          StaffTable.filter(_.id === treatment.staffId).result
        } map { staffDetails => 
          staffDetails.map{staffResult => 

          var block = db.run{
            StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepResult => 
              var referralSourceValue =  (stepResult.value \ "patient_referer").as[String]

              var referralSourcetitle = ""
              if(referralSource.contains (referralSourceValue)){
                referralSourcetitle = referralSource(referralSourceValue)
              }

           var block2 = db.run{
            ReferralSourceTable.filter(_.practiceId === staffResult.practiceId).filter(s => s.value === referralSourceValue || s.value === referralSourcetitle).result
              }map{ referralDetails => 
                referralDetails.map{referralResult => 
               
               var stepSchema: String = Json.stringify(stepResult.value)
               var s = stepSchema.replace(referralSourceValue,convertOptionLong(referralResult.id).toString)
               var schema = Json.parse(s);
               
               db.run{
                 StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").filter(_.dateCreated === stepResult.dateCreated).map(_.value).update(schema)
               } map { 
                  case 0L => PreconditionFailed
                  case id => Ok
               }
            }
          }
              val AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))
          }
           val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS)) 
      
        }
      }
        val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      
    }
    Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
  }
}


def delete(id: Long) = silhouette.SecuredAction.async { request =>
  db.run {
         ReferralSourceTable.filter(_.id === id).map(_.archived).update(true)
        } map {
        case 0 => NotFound
        case _ => Ok
        }
}


 
}


