package controllers.v3

import java.text.SimpleDateFormat

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import controllers.v2.FormsController
import models.daos.tables.{OrderRow, StaffRow, UserRow,ConsultationRow,EducationRequestRow}
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class AnalyticsController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application, val formsController: FormsController)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  

  private def orderValue[A](arg: (A, OrderItemTable)): Rep[Double] = {
    val (_, orderItemTable) = arg
    orderItemTable.productPrice * orderItemTable.amount.asColumnOf[Double]
  }

  private def createMonthList(lastYear: DateTime) = (0 until 24).map(lastYear.plusMonths).map(row => row.toString("MMMyy")).toList

  private def getMonthList(lastYear: DateTime) = (0 until 24).map(lastYear.plusMonths).map(row => row.toString("yyyy-MM-dd")).toList

  private def monthOffset(year: DateTime)(date: DateTime) = date.getMonthOfYear + (Math.abs(date.getYear - year.getYear) * 12)

  private def lastYearMonthOffset(lastYear: DateTime): DateTime => Int = monthOffset(lastYear)


  private def orderDateRangeFilter(orderTable: Query[OrderTable, OrderTable#TableElementType, Seq])(from: DateTime, to: DateTime) = {
    orderTable.filter(_.dateCreated >= from).filter(_.dateCreated < to)
  }

    private def orderCreatedDateRangeFilter(orderTable: Query[OrderTable, OrderTable#TableElementType, Seq])(from: DateTime, to: DateTime) = {
    orderTable.filter(_.dateCreated >= from).filter(_.dateCreated < to)
    }

  

  private def patientDateRangeFilter(from: DateTime, to: DateTime) = {
    TreatmentTable.filter(_.firstVisit >= from).filter(_.firstVisit < to)
  }

  private def consultDateRangeFilter(from: DateTime, to: DateTime) = {
    ConsultationTable.filter(_.dateRequested >= from).filter(_.dateRequested < to)
  }
  
  private def ceRequestDateRangeFilter(from: DateTime, to: DateTime) = {
    EducationRequestTable.filter(_.status===EducationRequestRow.Status.Closed.asInstanceOf[EducationRequestRow.Status]).filter(_.dateCreated >= from).filter(_.dateCreated < to)
    
  }

  private def cePurchaseDateRangeFilter(from: DateTime, to: DateTime) = {
    CeCoursePurchaseTable.filter(_.purchasedDate >= from).filter(_.purchasedDate < to)

  }

  private def consultRevenueDateRangeFilter(from: DateTime, to: DateTime) = {
    ConsultationTable.filter(_.status===ConsultationRow.Status.Closed.asInstanceOf[ConsultationRow.Status]).filter(_.closedDate >= from).filter(_.closedDate < to).filter(_.majorType === "consultation")
  }

  private def dentistDateRangeFilter(from: DateTime, to: DateTime) = {
    StaffTable.filter(_.dateCreated >= from).filter(_.dateCreated < to)
  }

  private def activeMembersRangeFilter(from: DateTime, to: DateTime) = {
    val todayStartTime = DateTime.now.withTimeAtStartOfDay()
    //    StaffTable.filter(_.dateCreated >= from).filter(_.dateCreated < to).filter(_.expirationDate > yesterday) //.filter(_.expirationDate > to)
    StaffTable.filter(_.expirationDate >= todayStartTime).filter(_.accountType==="Paid").filterNot(_.archived)

  }

  private def activeMembersLastYearRangeFilter(from: DateTime, to: DateTime) = {
    //val lastYearLastDay = to.minusDays(1)
    //Console.println(lastYearLastDay)
    //RenewalDetailsTable.filter(_.accountType==="Paid").filter(_.renewalDate <= to).filter(_.newExpDate >= to)
    RenewalDetailsTable.join(StaffTable).on(_.staffId === _.id).filter(_._1.accountType==="Paid").filterNot(_._2.archived).filter(_._1.renewalDate <= to).filter(_._1.newExpDate >= to)
  }


  private def activeMembersTwoYearRangeFilter(from: DateTime, to: DateTime) = {
    RenewalDetailsTable.filter(_.accountType==="Paid").filter(_.renewalDate >= from).filter(_.renewalDate < to)

  }

 /* private def membershipRevenueRangeFilter(from: DateTime, to: DateTime) = {
    StaffTable.filter(_.dateCreated >= from).filter(_.dateCreated < to) //.filter(_.expirationDate > to)
  }
*/
  private def membershipRenewalRevenueRangeFilter(from: DateTime, to: DateTime) = {
    //RenewalDetailsTable.filter(_.renewalDate >= from).filter(_.renewalDate < to) //.filter(_.expirationDate > to)

//    RenewalDetailsTable.filter(_.accountType==="Paid").filter(_.renewalDate >= from).filter(_.renewalDate < to)
    RenewalDetailsTable.filter(_.accountType==="Paid").filter(_.collectedDate >= from).filter(_.collectedDate < to)
  }

  private def itemsByDate(items: Seq[DateTime], offset: DateTime => Int) = items.foldLeft(Map.empty[Int, Int].withDefaultValue(0)) {
    (acc, date) =>
      acc + (offset(date) -> (acc(offset(date)) + 1))

  }

  private def getItems(items: Seq[DateTime], offset: DateTime => Int) {
    itemsByDate(items, offset)
  }

  def read = silhouette.SecuredAction.async { request =>
    val now = DateTime.now
    val todayStartTime = DateTime.now.withTimeAtStartOfDay()
    val thisMonth = now.withDayOfMonth(1).withTimeAtStartOfDay()
    val nextMonth = thisMonth.plusMonths(1)
    val thisYear = now.withDayOfYear(1).withTimeAtStartOfDay()
//    Console.println("yesterdayEndTime = "+todayStartTime)
    val nextYear = thisYear.plusYears(1)
    val lastYear = thisYear.minusYears(1)
    //val thisQuarter = now.minusMonths(3)

    // Get the month of the current date
    val i = now.getMonthOfYear();

    // 1st - 3rd months represent quarter 1
    // 4th - 6th months represent quarter 2
    // 7th - 9th months represent quarter 3
    // 10th - 12th months represent quarter 4



    var startI:Int = 0;
    var endI:Int = 0;

    if (i >= 1 && i <= 3) {
      startI = i - 1;
      endI = i - 3;
    } else if (i >= 4 && i <= 6) {
      startI = i - 4;
      endI = i - 6;
    } else if (i >= 7 && i <= 9) {
      startI = i - 7;
      endI = i - 9;
    } else if (i >= 10 && i <= 12) {
      startI = i - 10;
      endI = i - 12;
    }

    startI = Math.abs(startI);
    endI = Math.abs(endI);
    val thisQuarter = now.minusMonths(startI).dayOfMonth().withMinimumValue().withTimeAtStartOfDay();
    val endQuarter = now.plusMonths(endI).dayOfMonth().withMaximumValue().withTimeAtStartOfDay();

    val nextQuarter = endQuarter.plusDays(1)
    val lastPreviousYear = thisYear.minusYears(2)
    val lastThreeYear = thisYear.minusYears(3)
    val lastFourYear = thisYear.minusYears(4)

    //Console.println("thisQuarter="+thisQuarter);
    //Console.println("nextQuarter="+nextQuarter);

    //def fulfilledOrders = OrderTable.filter(o=>o.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status] || o.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status])

    val statusFilter= List(OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status],OrderRow.Status.Void.asInstanceOf[OrderRow.Status])

    def fulfilledOrders = OrderTable

    def fulfilledRevenueOrdersTotal = OrderTable.filter(_.dateCreated >= DateTime.parse("2022-01-01"))

    def catalogOrdersTotal = OrderTable.filter(_.dateCreated >= DateTime.parse("2022-01-01"))

    def createdOrders = OrderTable

    //def fulfilledOrders = OrderTable.filter(_.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status])

    def thisMonthOrdersQuery = orderDateRangeFilter(fulfilledOrders)(thisMonth, nextMonth)

    def thisMonthCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(thisMonth, nextMonth)

    def thisYearOrdersQuery = orderDateRangeFilter(fulfilledOrders)(thisYear, nextYear)

    def thisYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(thisYear, nextYear)

    def lastYearOrdersQuery = orderDateRangeFilter(fulfilledOrders)(lastYear, thisYear)

    def lastYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(lastYear, thisYear)

    def lastPreviousYearOrdersQuery = orderDateRangeFilter(fulfilledOrders)(lastPreviousYear, lastYear)

    def lastPreviousYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(lastPreviousYear, lastYear)

    //records from 2017
    def lastThreeYearOrdersQuery = orderDateRangeFilter(fulfilledOrders)(lastThreeYear, lastPreviousYear)

    def lastThreeYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(lastThreeYear, lastPreviousYear)

    def lastFourYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(lastFourYear, lastThreeYear)

    def thisQuarterOrdersQuery = orderDateRangeFilter(fulfilledOrders)(thisQuarter, nextQuarter)

    def thisQuarterCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(thisQuarter, nextQuarter)

    def twoYearOrdersQuery = orderDateRangeFilter(fulfilledOrders)(lastYear, nextYear)

    def twoYearCreatedOrdersQuery = orderDateRangeFilter(createdOrders)(lastYear, nextYear)


    def thisMonthCERequestQuery = ceRequestDateRangeFilter(thisMonth, nextMonth)
    
    def thisQuarterCERequestQuery = ceRequestDateRangeFilter(thisQuarter, nextQuarter)
    
    def thisYearCERequestQuery = ceRequestDateRangeFilter(thisYear, nextYear)
    
    def lastYearCERequestQuery = ceRequestDateRangeFilter(lastYear, thisYear)
    
    def lastPreviousYearCERequestQuery = ceRequestDateRangeFilter(lastPreviousYear, lastYear)

    def twoYearCERequestQuery = ceRequestDateRangeFilter(lastYear, nextYear)


// CE PURCHASE QUERY
    def thisMonthCEPurchaseQuery = cePurchaseDateRangeFilter(thisMonth, nextMonth)

    def thisYearCEPurchaseQuery = cePurchaseDateRangeFilter(thisYear, nextYear)

    def thisMonthPatientQuery = patientDateRangeFilter(thisMonth, nextMonth)

    def thisYearPatientQuery = patientDateRangeFilter(thisYear, nextYear)

    def lastYearPatientQuery = patientDateRangeFilter(lastYear, thisYear)

    def lastPreviousYearPatientQuery = patientDateRangeFilter(lastPreviousYear, lastYear)

    def lastThreeYearPatientQuery = patientDateRangeFilter(lastThreeYear, lastPreviousYear)

    def lastFourYearPatientQuery = patientDateRangeFilter(lastFourYear, lastThreeYear)

    def thisQuarterPatientQuery = patientDateRangeFilter(thisQuarter, nextQuarter)

    def thisMonthActiveMembersQuery = activeMembersRangeFilter(thisMonth, now)

    def thisYearActiveMembersQuery = activeMembersRangeFilter(thisYear, nextYear)

    def lastYearActiveMembersQuery = activeMembersLastYearRangeFilter(lastYear, thisYear)

    def lastPreviousYearActiveMembersQuery = activeMembersLastYearRangeFilter(lastPreviousYear, lastYear)

    def lastThreeYearActiveMembersQuery = activeMembersLastYearRangeFilter(lastThreeYear, lastPreviousYear)

    def lastFourYearActiveMembersQuery = activeMembersLastYearRangeFilter(lastFourYear, lastThreeYear)

    def thisQuarterActiveMembersQuery = activeMembersRangeFilter(thisQuarter, nextQuarter)

    //def twoYearActiveMembersQuery = activeMembersTwoYearRangeFilter(lastYear, nextYear)

//    def thisMonthMembershipQuery = membershipRevenueRangeFilter(thisMonth, nextMonth)

    def thisMonthRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(thisMonth, nextMonth)

//    def thisYearMembershipQuery = membershipRevenueRangeFilter(thisYear, nextYear)

    def thisYearRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(thisYear, nextYear)

//    def lastYearMembershipQuery = membershipRevenueRangeFilter(lastYear, thisYear)

    def lastYearRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(lastYear, thisYear)

//    def lastPreviousYearMembershipQuery = membershipRevenueRangeFilter(lastPreviousYear, lastYear)

    def lastPreviousYearRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(lastPreviousYear, lastYear)

    def lastThreeYearRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(lastThreeYear, lastPreviousYear)

//    def thisQuarterMembershipQuery = membershipRevenueRangeFilter(thisQuarter, nextQuarter)

    def thisQuarterRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(thisQuarter, nextQuarter)

//    def twoYearMembershipQuery = membershipRevenueRangeFilter(lastYear, nextYear)

    def twoYearRenewalMembershipQuery = membershipRenewalRevenueRangeFilter(lastYear, nextYear)

    def twoYearConsultQuery = consultDateRangeFilter(lastYear, nextYear)

    def twoYearConsultRevenueQuery = consultRevenueDateRangeFilter(lastYear, nextYear)

    def twoYearDentistQuery = dentistDateRangeFilter(lastYear, nextYear)

    def extractReferers(form: JsValue): Map[String, String] = {
      val referrers: List[Map[String, String]] =
        form.validateOpt[List[formsController.Slide]] match {
          case JsSuccess(schema, _) =>
            schema.getOrElse(List()).flatMap(
              slide => slide.rows.flatMap(
                row => row.columns.getOrElse(List()).flatMap(
                  column => column.columnItems.getOrElse(List()).flatMap(item => {
                    item.field match {
                      case Some(f) if f.name.getOrElse("") == "patient_referer" =>
                        f.attributes match {
                          case Some(attributes) =>
                            attributes.options match {
                              case Some(options) =>
                                Some(options.groupBy(_.value.getOrElse("")).map(v => (v._1, v._2.head.title.getOrElse(""))))
                              case _ => None
                            }
                          case _ => None
                        }
                      case _ => None
                    }
                  })
                )
              )
            )
          case JsError(_) =>
            List()
        }
      referrers.head
    }
    val monthListDate = getMonthList(lastYear)
    var activeMembersByMonth:Map[Int,Int] = Map()
    var tempCount:Int=0
    var preparedActiveMembersByMonth:List[JsArray] = null
    val monthList = createMonthList(lastYear)


    def getActiveRecordsByMonth(position: Int): Unit = {
      monthListDate.lift(position).map{
        case (month) =>
          val startOfNextMonth = DateTime.now().dayOfMonth.withMaximumValue().withTimeAtStartOfDay().plusDays(1);
          val currentMonth = DateTime.parse(month)
          val startOfMonth = currentMonth.dayOfMonth.withMinimumValue().withTimeAtStartOfDay()
          val firstOfNextMonth = currentMonth.dayOfMonth.withMaximumValue().withTimeAtStartOfDay().plusDays(1)
          val endOfMonth = currentMonth.dayOfMonth.withMaximumValue().withTimeAtStartOfDay()

          db.run {
            for {
              count <- RenewalDetailsTable.join(StaffTable).on(_.staffId === _.id).filterNot(_._2.archived).filter(_._1.accountType==="Paid").filter(_._1.renewalDate <= endOfMonth).filter(_._1.newExpDate >= endOfMonth).map(_._1.staffId).countDistinct.result

              //allOrdersByVendor <- OrderItemTable.join(OrderTable).on(_.orderId === _.id).filter(_._2.status.inSet(statusFilter))

            } yield {
              count
              tempCount = tempCount+1
              if(endOfMonth >= startOfNextMonth){
                activeMembersByMonth = activeMembersByMonth + (tempCount -> 0)
              }else {
                activeMembersByMonth = activeMembersByMonth + (tempCount -> count)
              }
              if(tempCount<24){
                getActiveRecordsByMonth(tempCount)
              }else{
                preparedActiveMembersByMonth = monthList.zip(Stream.from(1)).map {
                  case (month, i) =>
                    Json.arr(month, activeMembersByMonth(i))
                }
              }
            }
          }
      }
    }

    def adminAnalytics = {
      //val f=Future{getActiveRecordsByMonth(0)}
      getActiveRecordsByMonth(0)
      //val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      Thread.sleep(700)
      val valueTwelve:Long = 12

      db.run {
        for {
          // allOrders <- fulfilledOrders.map(_.id).countDistinct.result
          allOrders <- catalogOrdersTotal.map(_.id).countDistinct.result
          thisMonthsOrders <- thisMonthCreatedOrdersQuery.map(_.id).countDistinct.result
          thisYearOrders <- thisYearCreatedOrdersQuery.map(_.id).countDistinct.result
          thisQuarterOrders <- thisQuarterCreatedOrdersQuery.map(_.id).countDistinct.result
          twoYearsOrderDates <- twoYearCreatedOrdersQuery.map(_.dateCreated.asColumnOf[DateTime]).result

          // revenueTotal <- fulfilledOrders.map(_.grandTotal).sum.result
          revenueTotal <- fulfilledRevenueOrdersTotal.map(_.grandTotal).sum.result
          revenueThisMonth <- thisMonthOrdersQuery.map(_.grandTotal).sum.result
          revenueThisYear <- thisYearOrdersQuery.map(_.grandTotal).sum.result
          revenueThisQuarter <- thisQuarterOrdersQuery.map(_.grandTotal).sum.result

          //twoYearsRevenueDates <- twoYearOrdersQuery.join(OrderItemTable).on(_.id === _.orderId).map(row => (row._1.dateFulfilled.asColumnOf[DateTime], orderValue(row))).result

          twoYearsRevenueDates <- twoYearOrdersQuery.map(row => (row.dateCreated.asColumnOf[DateTime], row.grandTotal)).result

          //twoYearsMembershipRevenueDates <- twoYearMembershipQuery.map(row => (row.dateCreated, row.plan * row.subscriptionPeriod.asColumnOf[Long])).result

          twoYearsRenewalMembershipRevenueDates <- twoYearRenewalMembershipQuery.map(row => (row.collectedDate, row.amountCollected)).result

          twoYearsConsultRevenueDates <- twoYearConsultRevenueQuery.map(row => (row.closedDate, row
            .amount)).result

          allOrdersByVendor <- OrderItemTable.join(OrderTable).on(_.orderId === _.id)
            .groupBy(_._1.productVendorId).map {
            case (vendorId, items) => (vendorId, items.map(_._1.productVendorName).max, items.map(_._1.amount).sum, items.map(i => i._1.productPrice * i._1.amount.asColumnOf[Double]).sum)
          }.result

          membersByPlan <- StaffTable.filterNot(_.plan.isEmpty).filterNot(_.archived).filter(_.accountType==="Paid").filter(_.expirationDate >= todayStartTime)
            .groupBy(_.plan).map {
            case (plan, items) => (plan, items.size)
          }.result

          membersByMarketing <- StaffTable.filterNot(_.marketingSource === "").filterNot(_.archived).filter(_.accountType === "Paid").filter(_.expirationDate >= todayStartTime)
            .groupBy(_.marketingSource).map {
            case (marketingSource, items) => (marketingSource, items.size)
          }.result



          monthsOrdersByVendor <- OrderItemTable.join(OrderTable).on(_.orderId === _.id)
            .filter(_._2.dateCreated >= thisMonth).filter(_._2.dateCreated < nextMonth)
            .groupBy(_._1.productVendorId).map {
            case (vendorId, items) => (vendorId, items.map(_._1.productVendorName).max, items.map(_._1.amount).sum, items.map(i => i._1.productPrice * i._1.amount.asColumnOf[Double]).sum)
          }.result

          patientsThisMonth <- thisMonthPatientQuery.map(_.id).countDistinct.result
          patientsThisYear <- thisYearPatientQuery.map(_.id).countDistinct.result
          patientsLastYear <- lastYearPatientQuery.map(_.id).countDistinct.result
          patientsLastPreviousYear <- lastPreviousYearPatientQuery.map(_.id).countDistinct.result
          patientsLastThreeYear <- lastThreeYearPatientQuery.map(_.id).countDistinct.result
          patientsLastFourYear <- lastFourYearPatientQuery.map(_.id).countDistinct.result
          patientsThisQuarter <- thisQuarterPatientQuery.map(_.id).countDistinct.result
          patientsTotal <- TreatmentTable.map(_.id).countDistinct.result


          // activeMembersThisMonth <- thisMonthActiveMembersQuery.map(_.id).countDistinct.result
          // activeMembersThisYear <- thisYearActiveMembersQuery.map(_.id).countDistinct.result
          // activeMembersLastYear <- lastYearActiveMembersQuery.map(_._1.staffId).countDistinct.result

          activeMembersLastYearResult <- lastYearActiveMembersQuery.map(_._1.staffId).result

          // activeMembersLastPreviousYear <- lastPreviousYearActiveMembersQuery.map(_._1.staffId).countDistinct.result

          // activeMembersLastThreeYear <- lastThreeYearActiveMembersQuery.map(_._1.staffId).countDistinct.result

          // activeMembersLastFourYear <- lastFourYearActiveMembersQuery.map(_._1.staffId).countDistinct.result

          activeMembersThisQuarter <- thisQuarterActiveMembersQuery.map(_.id).countDistinct.result

          activeMembersTotal <- StaffTable.filter(_.expirationDate >= todayStartTime).filter(_.accountType==="Paid").filterNot(_.archived).map(_.id).countDistinct.result

          totalActiveMembers <- StaffTable.filter(s => s.accountType === "Paid" || s.accountType === "Trial").filterNot(_.archived).map(_.id).countDistinct.result

//          membershipRenewalRevenueThisMonth <- thisMonthRenewalMembershipQuery.map(res=>(res.amountCollected *  res.subscriptionPeriod.asColumnOf[Long])/valueTwelve).sum.result
          membershipRenewalRevenueThisMonth <- thisMonthRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          membershipRenewalRevenueThisYear <- thisYearRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          //membershipRevenueLastYear <- lastYearMembershipQuery.map(res=>res.plan * res.subscriptionPeriod.asColumnOf[Long]).sum.result

          // membershipRenewalRevenueLastYear <- lastYearRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          // membershipRenewalRevenueLastPreviousYear <- lastPreviousYearRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          // membershipRenewalRevenueLastThreeYear <- lastThreeYearRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          membershipRenewalRevenueThisQuarter <- thisQuarterRenewalMembershipQuery.map(res=>(res.amountCollected)).sum.result

          //membershipRenewalRevenueTotal <- RenewalDetailsTable.map(res=>res.plan * res.subscriptionPeriod.asColumnOf[Long]).sum.result
          membershipRenewalRevenueTotal <-RenewalDetailsTable.filter(_.accountType==="Paid").filter(_.collectedDate >= DateTime.parse("2022-01-01")).map(res=> (res.amountCollected)).sum.result


          latestOrders <- OrderTable.sortBy(_.id.desc)
            .join(StaffTable).on(_.staffId === _.id)
            .join(PracticeTable).on(_._2.practiceId === _.id).take(3)
            .map {
            case ((order,staff),practice) =>
              (order.id, staff.name, staff.lastName, practice.name,order.grandTotal)
          }.result

          latestConsultations <- ConsultationTable.sortBy(_.dateRequested.desc)
            .join(StaffTable).on(_.staffId === _.id)
            .join(PracticeTable).on(_._2.practiceId === _.id)
            .join(TreatmentTable).on(_._1._1.treatmentId === _.id).take(3)
            .map {
              case (((consultation, dentist), practice), treatment) =>
                (consultation.id, dentist.name, dentist.lastName, practice.name, consultation.dateRequested, treatment)
            }.result
          twoYearsConsultationDates <- twoYearConsultQuery.map(_.dateRequested).result
          twoYearsDentistSignupDates <- twoYearDentistQuery.filter(_.role === StaffRow.Role.Dentist.asInstanceOf[StaffRow.Role]).map(_.dateCreated).result


          cePurchaseThisMonth <- thisMonthCEPurchaseQuery.map(_.cost).sum.result
          cePurchaseThisYear <- thisYearCEPurchaseQuery.map(_.cost).sum.result
          cePurchaseTotal <- CeCoursePurchaseTable.map(_.cost).sum.result

        } yield {

          val twoYearsMembershipRevenueDatesFinal = twoYearsRenewalMembershipRevenueDates


          // val totalMembershipRevenueThisMonth = convertOptionLong(membershipRenewalRevenueThisMonth)

          // var totalMembershipRevenueThisYear : Double =  convertOptionLong(membershipRenewalRevenueThisYear).toDouble

          // var totalMembershipRevenueLastYear : Double =  convertOptionLong(membershipRenewalRevenueLastYear).toDouble

          // var totalMembershipRevenueLastPreviousYear : Double =  convertOptionLong(membershipRenewalRevenueLastPreviousYear).toDouble

          // var totalMembershipRevenueLastThreeYear : Double =  convertOptionLong(membershipRenewalRevenueLastPreviousYear).toDouble

          val totalMembershipRevenueThisQuarter =  convertOptionLong(membershipRenewalRevenueThisQuarter)

          // var membershipRevenueTotalFinal : Double =  convertOptionLong(membershipRenewalRevenueTotal).toDouble

          val offset = lastYearMonthOffset(lastYear)

          val ordersByMonth = itemsByDate(twoYearsOrderDates, offset)

          val preparedOrdersByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, ordersByMonth(i))
          }

          val dentistsByMonth = itemsByDate(twoYearsDentistSignupDates, offset)

          val preparedDentistsByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, dentistsByMonth(i))
          }

          val revenueByMonth = twoYearsRevenueDates.foldLeft(Map.empty[Int, Double].withDefaultValue(0.0)) { (acc, row) =>
            acc + (offset(row._1) -> (acc(offset(row._1)) + convertOptionFloat(row._2) ))
          }

          val membershipRevenueByMonth = twoYearsMembershipRevenueDatesFinal.foldLeft(Map.empty[Int, Double].withDefaultValue(0.0)) { (acc, row) =>
            val long_value: Long = row._2 match {
              case None => 0 //Or handle the lack of a value another way: throw an error, etc.
              case Some(l: Long) => l //return  to set your value
            }

            val dateVal: DateTime = row._1 match {
              case None => DateTime.now //Or handle the lack of a value another way: throw an error, etc.
              case Some(l: DateTime) => l //return  to set your value
            }

            acc + (offset(dateVal) -> (acc(offset(dateVal)) + long_value))
          }

          val consultRevenueByMonth = twoYearsConsultRevenueDates.foldLeft(Map.empty[Int, Double].withDefaultValue(0.0)) { (acc, row) =>
            val float_value: Float = row._2 match {
              case None => 0 //Or handle the lack of a value another way: throw an error, etc.
              case Some(f: Float) => f //return  to set your value
            }
            val row1_value: DateTime = row._1 match {
              case Some(f: DateTime) => f //return  to set your value
            }
            acc + (offset(row1_value) -> (acc(offset(row1_value)) + float_value))
          }


          val preparedRevenueByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, revenueByMonth(i).toInt)
          }

          val preparedMembershipRevenueByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, membershipRevenueByMonth(i).toInt)
          }

          val preparedConsultRevenueByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, consultRevenueByMonth(i).toInt)
          }


          val consultationsByMonth = itemsByDate(twoYearsConsultationDates, offset)


          val preparedConsultationsByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, consultationsByMonth(i))
          }

          var cePurchaseLastYear = 29.00
          var cePurchaseLastPreviousYear = 0
          var cePurchaseLastThreeYear = 0
          var cePurchaseLastFourYear = 0
          var cePurchaseLastFiveYear = 0

          // setting this default value for the old records

          var lastYearOrders = 2543
          var lastPreviousYearOrders = 2060
          var lastThreeYearOrders = 1636
          var lastFourYearOrders = 1103
          var lastFiveYearOrders = 583

          var totalOrders = allOrders + lastFiveYearOrders + lastFourYearOrders + lastThreeYearOrders + lastPreviousYearOrders + lastYearOrders

          var activeMembersThisMonth = 15
          var activeMembersThisYear = 0
          var activeMembersLastYear = 288
          var activeMembersLastPreviousYear = 653
          var activeMembersLastThreeYear = 270
          var activeMembersLastFourYear = 36
          var activeMembersLastFiveYear = 22
          // var totalActiveMembers = 817

          // var totalActiveMembers = activeMembersTotal + activeMembersLastFourYear + activeMembersLastThreeYear + activeMembersLastPreviousYear + activeMembersLastYear

          var totalMembershipRevenueLastYear = 347543.94
          var totalMembershipRevenueLastPreviousYear = 228663.99
          var totalMembershipRevenueLastThreeYear = 302729.00
          var totalMembershipRevenueLastFourYear = 303817.50
          var totalMembershipRevenueLastFiveYear = 753500.00
          var totalMembershipRevenueThisMonth = 48426.50
          var totalMembershipRevenueThisYear = 0
          var membershipRevenueTotalFinal = 1934264.43
          // membershipRevenueTotalFinal = membershipRevenueTotalFinal+ totalMembershipRevenueLastFourYear + totalMembershipRevenueLastThreeYear + totalMembershipRevenueLastPreviousYear + totalMembershipRevenueLastYear;

          var revenueLastYear = 3151306.60
          var revenueLastPreviousYear = 2108235.80
          var revenueLastThreeYear = 1816393.65
          var revenueLastFourYear = 1080009.50
          var revenueLastFiveYear = 753493.67

          var revenueFinalTotal = convertOptionFloat(revenueTotal).toDouble + revenueLastFiveYear + revenueLastFourYear + revenueLastThreeYear + revenueLastPreviousYear + revenueLastYear;

          Ok {
            Json.obj(
              "orders" -> Json.obj(
                // "all" -> allOrders,
                "all" -> totalOrders,
                "thisMonth" -> thisMonthsOrders,
                "thisYear" -> thisYearOrders,
                "lastYear" -> lastYearOrders,
                "lastPreviousYear" -> lastPreviousYearOrders,
                "lastThreeYear" -> lastThreeYearOrders,
                "lastFourYear" -> lastFourYearOrders,
                "lastFiveYear" -> lastFiveYearOrders,
                "thisQuarter" -> thisQuarterOrders,
                "byMonth" -> preparedOrdersByMonth
              ),
              "orderByVendor" -> Json.obj(
                "all" ->
                  allOrdersByVendor.map {
                    case (id, vendor, amount, total) =>
                      Json.obj(
                        "id" -> id,
                        "vendor" -> vendor,
                        "amount" -> amount,
                        "total" -> total
                      )
                  },
                "thisMonth" ->
                  monthsOrdersByVendor.map {
                    case (id, vendor, amount, total) =>
                      Json.obj(
                        "id" -> id,
                        "vendor" -> vendor,
                        "amount" -> amount,
                        "total" -> total
                      )
                  }
              ),
              "members" -> Json.obj(
                "plan" ->
                  membersByPlan.map {
                    case (plan, member_count) =>
                      Json.obj(
                        "plan" -> plan,
                        "member_count" -> member_count
                      )
                  },
                "marketingSource" ->
                  membersByMarketing.map {
                    case (marketingSource, member_count) =>
                      Json.obj(
                        "marketingSource" -> marketingSource,
                        "member_count" -> member_count
                      )
                  },
                "byMonth" -> preparedActiveMembersByMonth
              ),
              "revenue" -> Json.obj(
                "total" -> revenueFinalTotal,
                "thisMonth" -> revenueThisMonth,
                "thisYear" -> revenueThisYear,
                "lastYear" -> revenueLastYear,
                "lastPreviousYear" -> revenueLastPreviousYear,
                "lastThreeYear" -> revenueLastThreeYear,
                "lastFourYear" -> revenueLastFourYear,
                "lastFiveYear" -> revenueLastFiveYear,
                "thisQuarter" -> revenueThisQuarter,
                "byMonth" -> preparedRevenueByMonth
              ),
              "patients" -> Json.obj(
                "thisMonth" -> patientsThisMonth,
                "total" -> patientsTotal,
                "thisYear" -> patientsThisYear,
                "lastYear" -> patientsLastYear,
                "lastPreviousYear" -> patientsLastPreviousYear,
                "lastThreeYear" -> patientsLastThreeYear,
                "lastFourYear" -> patientsLastFourYear,
                "thisQuarter" -> patientsThisQuarter
              ),
              "activeMembers" -> Json.obj(
                "thisMonth" -> activeMembersThisMonth,
                // "total" -> activeMembersTotal,
                "total" -> totalActiveMembers,
                "thisYear" -> activeMembersThisYear,
                "lastYear" -> activeMembersLastYear,
                "lastPreviousYear" -> activeMembersLastPreviousYear,
                "lastThreeYear" -> activeMembersLastThreeYear,
                "lastFourYear" -> activeMembersLastFourYear,
                "lastFiveYear" -> activeMembersLastFiveYear,
                "thisQuarter" -> activeMembersThisQuarter
              ),
              "membershipRevenue" -> Json.obj(
                "thisMonth" -> totalMembershipRevenueThisMonth,
                "total" -> membershipRevenueTotalFinal,
                "thisYear" -> totalMembershipRevenueThisYear,
                "lastYear" -> totalMembershipRevenueLastYear,
                "lastPreviousYear" -> totalMembershipRevenueLastPreviousYear,
                "lastThreeYear" -> totalMembershipRevenueLastThreeYear,
                "lastFourYear" -> totalMembershipRevenueLastFourYear,
                "lastFiveYear" -> totalMembershipRevenueLastFiveYear,
                "thisQuarter" -> totalMembershipRevenueThisQuarter,
                "byMonth" -> preparedMembershipRevenueByMonth
              ),
              "latestOrders" -> latestOrders.map {
                case (id, dentistFirstName, dentistLastName, practice, total) =>
                var dentist = dentistFirstName +" "+ convertOptionString(dentistLastName)
                  Json.obj(
                    "id" -> id,
                    "dentist" -> dentist,
                    "practice" -> practice,
                    "total" -> total
                  )
              },
              "latestConsultations" -> latestConsultations.map {
                case (id, dentistFirstName, dentistLastName, practice, date, treatment) =>
                var patientName =  convertOptionString(treatment.firstName) + " " + convertOptionString(treatment.lastName)
                var dentist = dentistFirstName +" "+ convertOptionString(dentistLastName)
                  Json.obj(
                    "id" -> id,
                    "dentist" -> dentist,
                    "practice" -> practice,
                    "patientName" -> patientName,
                    "date" -> date
                  )
              },
              "consultations" -> Json.obj(
                "byMonth" -> preparedConsultationsByMonth
              ),
              "consultationsRevenue" -> Json.obj(
                "byMonth" -> preparedConsultRevenueByMonth
              ),
              "dentists" -> Json.obj(
                "byMonth" -> preparedDentistsByMonth
              ),
              "cePurchase" -> Json.obj(
                "thisMonth" -> cePurchaseThisMonth,
                "thisYear" -> cePurchaseThisYear,
                "lastYear" -> cePurchaseLastYear,
                "lastPreviousYear" -> cePurchaseLastPreviousYear,
                "lastThreeYear" -> cePurchaseLastThreeYear,
                "lastFourYear" -> cePurchaseLastFourYear,
                "lastFiveYear" -> cePurchaseLastFiveYear,
                "total" -> cePurchaseTotal
              )
            )
          }
        }
      }
    }

    def dentistAnalytics(dentistId: Option[Long], practiceId: Long) = {
      
      def filterOrdersByPractice(query: Query[OrderTable, OrderTable#TableElementType, Seq]) =
        query.join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === practiceId).map {
          _._1
        }

      def filterPatientsByPractice(query: Query[TreatmentTable, TreatmentTable#TableElementType, Seq]) =
        query.join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === practiceId).map {
          _._1
        }
        
      
      def practiceFulfilledOreders = filterOrdersByPractice(fulfilledOrders)
          
      db.run {
        for {
          allOrders <- practiceFulfilledOreders.map(_.id).countDistinct.result

          thisMonthsOrders <- filterOrdersByPractice(thisMonthOrdersQuery).map(_.id).countDistinct.result
          thisYearOrders <- filterOrdersByPractice(thisYearOrdersQuery).map(_.id).countDistinct.result
          twoYearsOrderDates <- filterOrdersByPractice(twoYearOrdersQuery).map(_.dateCreated).result

          lastYearOrders <- filterOrdersByPractice(lastYearOrdersQuery).map(_.id).countDistinct.result
          lastPreviousYearOrders <- filterOrdersByPractice(lastPreviousYearOrdersQuery).map(_.id).countDistinct.result
          thisQuarterOrders <- filterOrdersByPractice(thisQuarterOrdersQuery).map(_.id).countDistinct.result


          spendingTotal <- practiceFulfilledOreders.map(_.grandTotal).sum.result
          spendingThisMonth <- filterOrdersByPractice(thisMonthOrdersQuery).map(_.grandTotal).sum.result
          spendingThisYear <- filterOrdersByPractice(thisYearOrdersQuery).map(_.grandTotal).sum.result
          spendingLastYear <- filterOrdersByPractice(lastYearOrdersQuery).map(_.grandTotal).sum.result
          spendingThisQuarter <- filterOrdersByPractice(thisQuarterOrdersQuery).map(_.grandTotal).sum.result
          spendingLastPreviousYears <- filterOrdersByPractice(lastPreviousYearOrdersQuery).map(_.grandTotal).sum.result

          allOrdersByVendor <- practiceFulfilledOreders.join(OrderItemTable).on(_.id === _.orderId)
            .groupBy(_._2.productVendorId).map {
            case (vendorId, items) => (vendorId, items.map(_._2.productVendorName).max, items.map(_._2.amount).sum, items.map(i => i._2.productPrice * i._2.amount.asColumnOf[Double]).sum)
          }.result

         /* patientsThisMonth <- thisMonthPatientQuery.map(_.id).countDistinct.result
          patientsThisYear <- thisYearPatientQuery.map(_.id).countDistinct.result
          patientsThisQuarter <- thisQuarterPatientQuery.map(_.id).countDistinct.result
          patientsLastYear <- lastYearPatientQuery.map(_.id).countDistinct.result
          patientsLastPreviousYears <- lastPreviousYearPatientQuery.map(_.id).countDistinct.result
          patientsTotal <- TreatmentTable.map(_.id).countDistinct.result*/

          patientsThisMonth <- filterPatientsByPractice(thisMonthPatientQuery).map(_.id).countDistinct.result
          patientsThisYear <- filterPatientsByPractice(thisYearPatientQuery).map(_.id).countDistinct.result
          patientsThisQuarter <- filterPatientsByPractice(thisQuarterPatientQuery).map(_.id).countDistinct.result
          patientsLastYear <- filterPatientsByPractice(lastYearPatientQuery).map(_.id).countDistinct.result
          patientsLastPreviousYears <- filterPatientsByPractice(lastPreviousYearPatientQuery).map(_.id).countDistinct.result
          patientsTotal <- filterPatientsByPractice(TreatmentTable).map(_.id).countDistinct.result
          
          ceRequestThisMonth <- thisMonthCERequestQuery.filter(_.staffId===dentistId).map(_.id).countDistinct.result
          ceRequestThisQuarter <- thisQuarterCERequestQuery.filter(_.staffId===dentistId).map(_.id).countDistinct.result
          ceRequestThisYear <- thisYearCERequestQuery.filter(_.staffId===dentistId).map(_.id).countDistinct.result
          ceRequestLastYear <- lastYearCERequestQuery.filter(_.staffId===dentistId).map(_.id).countDistinct.result
          ceRequestLastPreviousYear <- lastPreviousYearCERequestQuery.filter(_.staffId===dentistId).map(_.id).countDistinct.result
          ceRequestLastTotal <- EducationRequestTable.filter(_.staffId===dentistId).filter(_.status===EducationRequestRow.Status.Closed.asInstanceOf[EducationRequestRow.Status]).map(_.id).countDistinct.result
          
         
     /*     patientsReferer <- TreatmentTable
            .join(StaffTable)
            .on(_.staffId === _.id)
            .join(StepTable.filter(_.formId === "1A").sortBy(_.dateCreated.desc))
            .on(_._1.id === _.treatmentId)
            .joinLeft(StepTable.filter(_.formId === "2D1").sortBy(_.dateCreated.desc))
            .on(_._1._1.id === _.treatmentId)
            .filter(_._1._1._2.practiceId === practiceId)
            .result
            */

            patients <- TreatmentTable.filterNot(_.deleted)
            .join(StaffTable)
            .on(_.staffId === _.id)
            .filter(_._2.practiceId === practiceId)
            .result

            //  patientsActiveTreatment <- TreatmentTable.filterNot(_.deleted).filter(_.treatmentStarted === true)
            // .join(StaffTable)
            // .on(_.staffId === _.id)
            // .filter(_._2.practiceId === practiceId)
            // .result

            entryForm <- FormTable.filter(_.id === "1A").map(_.schema).result.head

            latestOrders <- filterOrdersByPractice(OrderTable.sortBy(_.id.desc))
            .join(StaffTable).on(_.staffId === _.id)
            .join(PracticeTable).on(_._2.practiceId === _.id).take(3)
            .map {
             case ((order,staff),practice) =>
              (order.id, staff.id, staff.name, staff.lastName, practice.name,order.grandTotal)
            }.result


          latestConsultations <- ConsultationTable.filter(_.majorType === "consultation")
            .join(StaffTable).on(_.staffId === _.id)
            .join(PracticeTable).on(_._2.practiceId === _.id).filter(_._1._2.practiceId === practiceId)
            .join(TreatmentTable).on(_._1._1.treatmentId === _.id).sortBy(_._1._1._1.id.desc).take(3)
            .map {
              case (((consultation, dentist), practice), treatment) =>
                (consultation.id, dentist.name, dentist.lastName, practice.name, consultation.dateRequested, treatment)
            }.result

          // twoYearCERequestsDates <- EducationRequestTable.filter(_.staffId === dentistId).map(_.dateCreated).result
          twoYearCERequestsDates <- twoYearCERequestQuery.filter(_.staffId === dentistId).map(_.dateCreated).result
        } yield {
          

          
          val monthList = createMonthList(lastYear)
          val offset = lastYearMonthOffset(lastYear)

          val ordersByMonth = itemsByDate(twoYearsOrderDates, offset)

          val preparedOrdersByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, ordersByMonth(i))
          }

          val ceRequestsByMonth = itemsByDate(twoYearCERequestsDates, offset)

          val preparedCERequestsByMonth = monthList.zip(Stream.from(1)).map {
            case (month, i) => Json.arr(month, ceRequestsByMonth(i))
          }

          //val refererLabels = extractReferers(entryForm)

          val patientsByReferer = patients.map{result=>
                      val referer  = convertOptionString(result._1.referralSource)
                      (referer)
                      }

          val patientsByStatus = patients.map{result=>
                      val referer  = convertOptionString(result._1.patientStatus)
                      (referer)
                      }

          // val patientsByTreatment = patientsActiveTreatment.map{result=>
          //             val refererTreatmentStarted  = convertOptionString(result._1.referralSource)
          //             (refererTreatmentStarted)
          //             }

          val allPatientsByReferer = patientsByReferer.groupBy(identity).mapValues(_.size)
          // val activePatientsByReferer = patientsByTreatment.groupBy(identity).mapValues(_.size)
          val allPatientsByStatus = patientsByStatus.groupBy(identity).mapValues(_.size)

          var patientStatus : Map[String,Int] = Map()
          var patientReferer: Map[String,Int] = Map()
          patientStatus = allPatientsByStatus.map { case (key, value) => (updatedKey(key), value)}
          patientReferer = allPatientsByReferer.map { case (key, value) => (updatedKey(key), value)}

          def updatedKey(key: String): String = {
              if (key=="")
                return "None"
              else
                return key
          }



          Ok {
            Json.obj(
              "orders" -> Json.obj(
                "all" -> allOrders,
                "thisMonth" -> thisMonthsOrders,
                "thisYear" -> thisYearOrders,
                "lastYear" -> lastYearOrders,
                "lastPreviousYear" -> lastPreviousYearOrders,
                "thisQuarter" -> thisQuarterOrders,
                "byMonth" -> preparedOrdersByMonth,
                "latest" -> latestOrders.map {
                  case (id, staffId, dentistFirstName, dentistLastName, practice, total) =>
                  var dentist = dentistFirstName +" "+ convertOptionString(dentistLastName)
                    Json.obj(
                      "id" -> id,
                      "dentist" -> dentist,
                      "practice" -> practice,
                      "total" -> total
                    )
                }
              ),
              "orderByVendor" -> Json.obj(
                "all" ->
                  allOrdersByVendor.map {
                    case (id, vendor, amount, total) =>
                      Json.obj(
                        "id" -> id,
                        "vendor" -> vendor,
                        "amount" -> amount,
                        "total" -> total
                      )
                  }
              ),
              "spending" -> Json.obj(
                "total" -> spendingTotal,
                "thisMonth" -> spendingThisMonth,
                "thisYear" -> spendingThisYear,
                "lastYear" -> spendingLastYear,
                "lastPreviousYear" -> spendingLastPreviousYears,
                "thisQuarter" -> spendingThisQuarter
              ),
              "patients" -> Json.obj(
                "thisMonth" -> patientsThisMonth,
                "total" -> patientsTotal,
                "thisYear" -> patientsThisYear,
                "lastYear" -> patientsLastYear,
                "lastPreviousYear" -> patientsLastPreviousYears,
                "thisQuarter" -> patientsThisQuarter
              ),
              "latestConsultations" -> latestConsultations.map {
                case (id, dentistFirstName, dentistLastName, practice, date, treatment) =>
                var patientName =  convertOptionString(treatment.firstName) + " " + convertOptionString(treatment.lastName)
                var dentist = dentistFirstName +" "+ convertOptionString(dentistLastName)
                  Json.obj(
                    "id" -> id,
                    "dentist" -> dentist,
                    "practice" -> practice,
                    "patientName" -> patientName,
                    "date" -> date
                  )
              },
              "ceRequests" -> Json.obj(
                "thisMonth" -> ceRequestThisMonth,
                "thisQuarter" -> ceRequestThisQuarter,
                "thisYear" -> ceRequestThisYear,
                "lastYear" -> ceRequestLastYear,
                "lastPreviousYear" -> ceRequestLastPreviousYear,
                "total" -> ceRequestLastTotal,
                "byMonth" -> preparedCERequestsByMonth
              ),
              "patientsByReferer" -> Json.obj(
                "all" -> patientReferer
                // "treatmentStarted" -> activePatientsByReferer
              ),
              "patientsByStatus" -> patientStatus
            )
          }
        }
      }
    }

    request.identity match {
      case StaffRow(id, _, _, _, _, practiceId, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) => dentistAnalytics(id, practiceId)
      case UserRow(_, _, _, _, _, _, _) => adminAnalytics
    }
  }
}


