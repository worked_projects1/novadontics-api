package controllers.v3
import controllers.NovadonticsController
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import java.net.{URL,HttpURLConnection,URLConnection}
import java.io.File
import com.github.tototoshi.csv._
import play.api.mvc.Action
import java.text.DecimalFormat

import org.joda.time.DateTime
import org.joda.time.{ LocalDate, LocalTime }
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};
import akka.stream.scaladsl.StreamConverters
import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.google.inject.name.Named
import com.mohiva.play.silhouette.api.Silhouette
import play.api.{Application, Logger}
import play.api.http.HttpEntity
import play.api.data.validation.ValidationError
import play.api.libs.json.Json.obj
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.json._
import play.api.libs.ws._
import play.api.Play.current
import play.api.mvc.{ResponseHeader, Result}
import java.text.NumberFormat.getCurrencyInstance
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.libs.json.Json
import models.daos.tables.{TreatmentPlanDetailsRow,StepRow,StaffRow,DefaultFeeRow}
import play.api.mvc.Result
import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO
import scalaj.http.{Http, HttpOptions}
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.io.Source
import scala.util.{Failure, Success}

import scala.collection.mutable.ListBuffer
import java.math.RoundingMode;
import java.math.BigDecimal;
import scala.util.control.Breaks._
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._

class TreatmentPlanDetailsController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val DefaultFeeDetails = Json.format[DefaultFeeRow]

  implicit val LedgerDetails = Json.format[LedgerRow]

  implicit val TreatmentPlanDetailsWrites = Writes { treatmentPlanDetailsRow: (TreatmentPlanDetailsRow) =>

  Json.obj(
      "id" ->  treatmentPlanDetailsRow.id,
      "treatmentPlanId" ->  treatmentPlanDetailsRow.treatmentPlanId,
      "cdcCode" ->  treatmentPlanDetailsRow.cdcCode,
      "procedureName" ->  treatmentPlanDetailsRow.procedureName,
      "toothNumber" ->  treatmentPlanDetailsRow.toothNumber,
      "status" ->  treatmentPlanDetailsRow.status,
      "datePlanned" ->  treatmentPlanDetailsRow.datePlanned,
      "datePerformed" ->  treatmentPlanDetailsRow.datePerformed,
      "surface" ->  treatmentPlanDetailsRow.surface,
      "fee" ->    treatmentPlanDetailsRow.fee,
      "estIns" ->   treatmentPlanDetailsRow.estIns,
      "providerId" ->  treatmentPlanDetailsRow.providerId,
      "insurancePaidAmt" ->  treatmentPlanDetailsRow.insurancePaidAmt,
      "insurancePaidDate" -> treatmentPlanDetailsRow.insurancePaidDate,
      "patientPaidAmt" ->  treatmentPlanDetailsRow.patientPaidAmt,
      "patientPaidDate" ->  treatmentPlanDetailsRow.patientPaidDate,
      "insurancePayMethod" ->  treatmentPlanDetailsRow.insurancePayMethod,
      "patientPayMethod" ->  treatmentPlanDetailsRow.patientPayMethod,
      "patientId" -> treatmentPlanDetailsRow.patientId,
      "insurance" -> treatmentPlanDetailsRow.insurance,
      "patientBank" -> treatmentPlanDetailsRow.patientBank,
      "refundAmount" -> Math.round(convertOptionFloat(treatmentPlanDetailsRow.refundAmount) * 100.0) / 100.0,
      "refundDate" -> treatmentPlanDetailsRow.refundDate,
      "refundPayMethod" -> treatmentPlanDetailsRow.refundPayMethod,
      "renewalDate" -> treatmentPlanDetailsRow.renewalDate,
      "familyAnnualMax" -> Math.round(convertOptionFloat(treatmentPlanDetailsRow.familyAnnualMax) * 100.0) / 100.0,
      "individualAnnualMax" -> Math.round(convertOptionFloat(treatmentPlanDetailsRow.individualAnnualMax) * 100.0) / 100.0,
      "medicalInsuranceName" -> treatmentPlanDetailsRow.medicalInsuranceName,
      "prognosis" -> treatmentPlanDetailsRow.prognosis,
      "priority" -> treatmentPlanDetailsRow.priority,
      "service" -> treatmentPlanDetailsRow.service,
      "phase" -> treatmentPlanDetailsRow.phase
      )
  }

case class LedgerRow(
    date: Option[LocalDate],
    patientName: String,
    toothNumber: String,
    surface: String,
    fee: Float,
    dentalInsuranceName: String,
    medicalInsuranceName: Option[String],
    insurancePaidAmt: Option[Float],
    insurancePayMethod: String,
    patientPaidAmt: Option[Float],
    patientPayMethod: String,
    cdcCode: String,
    description: String,
    provider: String,
    balance: Float
  )

  case class TreatmentDetailsRow(
    id: Long,
    treatmentPlanId: Long,
    patientId: Option[Long],
    cdcCode: String,
    procedureName: String,
    toothNumber: String,
    status: String,
    datePlanned: LocalDate,
    datePerformed: Option[LocalDate],
    surface: String,
    fee: Float,
    estIns: Float,
    disc: Float,
    providerId: Option[Long],
    providerName: Option[String],
    insurancePaidAmt: Option[Float],
    insurancePaidDate: Option[LocalDate],
    patientPaidAmt: Option[Float],
    patientPaidDate: Option[LocalDate],
    patientBalance: Float,
    insurancePayMethod: String,
    patientPayMethod: String,
    insurance: Option[Long],
    insuranceName: String,
    patientBank: Option[String],
    refundAmount: Option[Float],
    refundDate: Option[LocalDate],
    refundPayMethod: Option[String],
    renewalDate: Option[LocalDate],
    familyAnnualMax: Option[Float],
    individualAnnualMax: Option[Float],
    medicalInsuranceName: Option[String],
    scheduled: String,
    prognosis: Option[String],
    priority: Option[String],
    service: Option[String],
    phase: Option[String]
  )

  implicit val TreatmentPlanDetailsRowWrites = Writes { TreatmentPlanDetailsRecord: (TreatmentDetailsRow) =>
  val (treatmentPlanDetails) = TreatmentPlanDetailsRecord

  Json.obj(
      "id" -> treatmentPlanDetails.id,
      "treatmentPlanId" -> treatmentPlanDetails.treatmentPlanId,
      "patientId" -> treatmentPlanDetails.patientId,
      "cdcCode" -> treatmentPlanDetails.cdcCode,
      "procedureName" -> treatmentPlanDetails.procedureName,
      "toothNumber" -> treatmentPlanDetails.toothNumber,
      "status" -> treatmentPlanDetails.status,
      "datePlanned" -> treatmentPlanDetails.datePlanned,
      "datePerformed" -> treatmentPlanDetails.datePerformed,
      "surface" -> treatmentPlanDetails.surface,
      "fee" ->   treatmentPlanDetails.fee,
      "estIns" ->  treatmentPlanDetails.estIns,
      "disc" -> treatmentPlanDetails.disc,
      "providerId" -> treatmentPlanDetails.providerId,
      "providerName" -> treatmentPlanDetails.providerName,
      "insurancePaidAmt" -> treatmentPlanDetails.insurancePaidAmt,
      "insurancePaidDate" ->treatmentPlanDetails.insurancePaidDate,
      "patientPaidAmt" -> treatmentPlanDetails.patientPaidAmt,
      "patientPaidDate" -> treatmentPlanDetails.patientPaidDate,
      "patientBalance" -> treatmentPlanDetails.patientBalance,
      "insurancePayMethod" -> treatmentPlanDetails.insurancePayMethod,
      "patientPayMethod" -> treatmentPlanDetails.patientPayMethod,
      "insurance" -> treatmentPlanDetails.insurance,
      "insuranceName" -> treatmentPlanDetails.insuranceName,
      "patientBank" -> treatmentPlanDetails.patientBank,
      "refundAmount" -> Math.round(convertOptionFloat(treatmentPlanDetails.refundAmount) * 100.0) / 100.0,
      "refundDate" -> treatmentPlanDetails.refundDate,
      "refundPayMethod" -> treatmentPlanDetails.refundPayMethod,
      "renewalDate" -> treatmentPlanDetails.renewalDate,
      "familyAnnualMax" -> Math.round(convertOptionFloat(treatmentPlanDetails.familyAnnualMax) * 100.0) / 100.0,
      "individualAnnualMax" -> Math.round(convertOptionFloat(treatmentPlanDetails.individualAnnualMax) * 100.0) / 100.0,
      "medicalInsuranceName" -> treatmentPlanDetails.medicalInsuranceName,
      "scheduled" -> treatmentPlanDetails.scheduled,
      "prognosis" -> treatmentPlanDetails.prognosis,
      "priority" -> treatmentPlanDetails.priority,
      "service" -> treatmentPlanDetails.service,
      "phase" -> treatmentPlanDetails.phase
      )
  }

  private val logger = Logger(this.getClass)

  case class  PlanList(
                        planId: Long,
                        patientId: Long,
                        planName: String,
                        dateCreated: String,
                        planStatus: String
                      )
  case class  PlanDetailList(
                              cdcCode: String,
                              procedureName: String,
                              datePlanned: String,
                              datePerformed: String,
                              toothNumber: String,
                              status: String,
                              surface: String,
                              phase: String,
                              fee: Float,
                              estIns: Float,
                              disc: Float
                            )


  def readAll(patientId:Long) = silhouette.SecuredAction.async { request =>
      db.run {
      TreatmentPlanDetailsTable.filter(_.patientId === patientId).joinLeft(DentalCarrierTable).on(_.insurance === _.id).joinLeft(AppointmentTreatmentPlanTable).on(_._1.id === _.treatmentPlanId).joinLeft(ProviderTable).on(_._1._1.providerId === _.id).result
      } map { treatment =>
             val data = treatment.groupBy(_._1._1._1.id).map {
              case (id,entries) =>
                val entry = entries.head._1._1._1
                val dentalCarrier = entries.head._1._1._2
                val appointment = entries.head._1._2.isDefined
                var providerName = ""
                var scheduled = "No"
                var carrierName = ""
                if(dentalCarrier != None){
                  val dentalCarrier = entries.head._1._1._2.get
                  carrierName = dentalCarrier.name
                }
                if(appointment || entry.status == "Completed"){
                  scheduled = "Yes"
                }
                if(entries.head._2.isDefined){
                  var provider= entries.head._2.get
                  providerName = provider.title +" "+ provider.firstName + " "+ provider.lastName
                }

            val disc = entry.fee - entry.estIns
            val totalValue = Math.round((convertOptionFloat(entry.insurancePaidAmt) * 100.0) / 100.0) + Math.round((convertOptionFloat(entry.patientPaidAmt) * 100.0) / 100.0)
            val patientBalance = entry.fee - totalValue

                TreatmentDetailsRow(
                  entry.id.get,
                  entry.treatmentPlanId,
                  entry.patientId,
                  entry.cdcCode,
                  entry.procedureName,
                  entry.toothNumber,
                  entry.status,
                  entry.datePlanned,
                  entry.datePerformed,
                  entry.surface,
                  entry.fee,
                  entry.estIns,
                  disc,
                  entry.providerId,
                  Some(providerName),
                  entry.insurancePaidAmt,
                  entry.insurancePaidDate,
                  entry.patientPaidAmt,
                  entry.patientPaidDate,
                  patientBalance,
                  entry.insurancePayMethod,
                  entry.patientPayMethod,
                  entry.insurance,
                  carrierName,
                  entry.patientBank,
                  entry.refundAmount,
                  entry.refundDate,
                  entry.refundPayMethod,
                  entry.renewalDate,
                  entry.familyAnnualMax,
                  entry.individualAnnualMax,
                  entry.medicalInsuranceName,
                  scheduled,
                  entry.prognosis,
                  entry.priority,
                  entry.service,
                  entry.phase
                )
                }.toList.sortBy(_.id).reverse
            Ok(Json.toJson(data))
          }
  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
    val treatmentPlanDetails = for {
      treatmentPlanId <- (request.body \ "treatmentPlanId").validateOpt[Long]
      cdcCode <- (request.body \ "cdcCode").validate[String]
      procedureName <- (request.body \ "procedureName").validate[String]
      toothNumber <- (request.body \ "toothNumber").validateOpt[String]
      status <- (request.body \ "status").validate[String]
      datePlanned <- (request.body \ "datePlanned").validate[LocalDate]
      datePerformedValue <- (request.body \ "datePerformed").validateOpt[String]
      surface <- (request.body \ "surface").validateOpt[String]
      fee <- (request.body \ "fee").validate[Float]
      estIns <- (request.body \ "estIns").validate[Float]
      providerId <- (request.body \ "providerId").validateOpt[Long]
      insurancePaidAmt <- (request.body \ "insurancePaidAmt").validateOpt[Float]
      insurancePaidDate <- (request.body \ "insurancePaidDate").validateOpt[String]
      patientPaidAmt <- (request.body \ "patientPaidAmt").validateOpt[Float]
      patientPaidDate <- (request.body \ "patientPaidDate").validateOpt[String]
      insurancePayMethod <- (request.body \ "insurancePayMethod").validateOpt[String]
      patientPayMethod <- (request.body \ "patientPayMethod").validateOpt[String]
      patientId <- (request.body \ "patientId").validate[Long]
      insurance <- (request.body \ "insurance").validateOpt[Long]
      patientBank <- (request.body \ "patientBank").validateOpt[String]
      refundAmount <- (request.body \ "refundAmount").validateOpt[Float]
      refundDateValue <- (request.body \ "refundDate").validateOpt[String]
      refundPayMethod <- (request.body \ "refundPayMethod").validateOpt[String]
      renewalDateValue <- (request.body \ "renewalDate").validateOpt[String]
      familyAnnualMax <- (request.body \ "familyAnnualMax").validateOpt[Float]
      individualAnnualMax <- (request.body \ "individualAnnualMax").validateOpt[Float]
      medicalInsuranceName <- (request.body \ "medicalInsuranceName").validateOpt[String]
      prognosis <- (request.body \ "prognosis").validateOpt[String]
      priority <- (request.body \ "priority").validateOpt[String]
      service <- (request.body \ "service").validateOpt[String]
      phase <- (request.body \ "phase").validateOpt[String]

    } yield {
      var datePerformed: Option[LocalDate] = None
      if(datePerformedValue != None && datePerformedValue != Some("")){
        datePerformed = Some(LocalDate.parse(convertOptionString(datePerformedValue)))
      }

      var renewalDate: Option[LocalDate] = None
      if(renewalDateValue != None && renewalDateValue != Some("")){
        renewalDate = Some(LocalDate.parse(convertOptionString(renewalDateValue)))
      }

      var refundDate: Option[LocalDate] = None
      if(refundDateValue != None && refundDateValue != Some("")){
        refundDate = Some(LocalDate.parse(convertOptionString(refundDateValue)))
      }

      var insurancePaid_Date: Option[LocalDate] = None
      if(insurancePaidDate != None && insurancePaidDate != Some("")){
        insurancePaid_Date = Some(LocalDate.parse(convertOptionString(insurancePaidDate)))
      }

      var patientPaid_Date: Option[LocalDate] = None
      if(patientPaidDate != None && patientPaidDate != Some("")){
        patientPaid_Date = Some(LocalDate.parse(convertOptionString(patientPaidDate)))
      }

      TreatmentPlanDetailsRow(None,convertOptionLong(treatmentPlanId),cdcCode,procedureName,convertOptionString(toothNumber),status,datePlanned,datePerformed,convertOptionString(surface),fee,estIns,providerId,insurancePaidAmt,insurancePaid_Date,patientPaidAmt,patientPaid_Date,convertOptionString(insurancePayMethod),convertOptionString(patientPayMethod),Some(patientId),insurance,patientBank,refundAmount,refundDate,refundPayMethod,renewalDate,familyAnnualMax,individualAnnualMax,medicalInsuranceName,prognosis,priority,service,phase)
    }
    
    treatmentPlanDetails match {
      case JsSuccess(treatmentPlanDetailsRow, _) =>
      var statusValidation : Boolean = false
      if(treatmentPlanDetailsRow.status.toLowerCase != "completed"){
        if(treatmentPlanDetailsRow.datePerformed != None) {
              statusValidation = true
        }
      }

      var datePerformedValidation : Boolean = false
      if(treatmentPlanDetailsRow.status.toLowerCase == "completed"){
        if(treatmentPlanDetailsRow.datePerformed == None) {
              datePerformedValidation = true
        }
      }

      var codeToToothExists = false
      var repeatBlock = db.run{
      TreatmentPlanDetailsTable.filter(_.patientId === treatmentPlanDetailsRow.patientId).filter(s=> s.status === "Accepted" || s.status === "Proposed").filter(_.cdcCode === treatmentPlanDetailsRow.cdcCode).filter(_.toothNumber === treatmentPlanDetailsRow.toothNumber).result
      } map { planDetailsList =>
      planDetailsList.foreach(result => {
      codeToToothExists = true
      })
      
      }
      Await.ready(repeatBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

      if(codeToToothExists){
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Selected procedure already planned for the teeth !"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      }
      else {
      if(statusValidation){
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Date Performed should be blank for the status Proposed and Accepted!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else if(datePerformedValidation) {
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Date Performed should not be blank for the status completed!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else {
      db.run {
        TreatmentPlanDetailsTable.returning(TreatmentPlanDetailsTable.map(_.id)) += treatmentPlanDetailsRow
       
      } map {
        case 0L => PreconditionFailed
        case id =>   db.run{
          TreatmentPlanDetailsTable.filter(_.id === id).result
        } map{ planDetails => planDetails.map{ treatment =>
          db.run {
            TreatmentPlanTable.filterNot(_.deleted).filter(_.id === treatment.treatmentPlanId).map(_.dateUpdated).update(LocalDate.now())
          } map {
            case 0 => NotFound
            case _ => Ok
          }
        }
          Ok
        }
          Created(Json.toJson(treatmentPlanDetailsRow.copy(id = Some(id))))
       }
      }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val treatmentPlan = for {
      treatmentPlanId <- (request.body \ "treatmentPlanId").validateOpt[Long]
      cdcCode <- (request.body \ "cdcCode").validateOpt[String]
      procedureName <- (request.body \ "procedureName").validateOpt[String]
      toothNumber <- (request.body \ "toothNumber").validateOpt[String]
      status <- (request.body \ "status").validateOpt[String]
      datePlanned <- (request.body \ "datePlanned").validateOpt[LocalDate]
      datePerformedValue <- (request.body \ "datePerformed").validateOpt[String]
      surface <- (request.body \ "surface").validateOpt[String]
      fee <- (request.body \ "fee").validateOpt[Float]
      estIns <- (request.body \ "estIns").validateOpt[Float]
      providerId <- (request.body \ "providerId").validateOpt[Long]
      insurancePaidAmt <- (request.body \ "insurancePaidAmt").validateOpt[Float]
      insurancePaidDate <- (request.body \ "insurancePaidDate").validateOpt[String]
      patientPaidAmt <- (request.body \ "patientPaidAmt").validateOpt[Float]
      patientPaidDate <- (request.body \ "patientPaidDate").validateOpt[String]
      insurancePayMethod <- (request.body \ "insurancePayMethod").validateOpt[String]
      patientPayMethod <- (request.body \ "patientPayMethod").validateOpt[String]
      patientId <- (request.body \ "patientId").validateOpt[Long]
      insurance <- (request.body \ "insurance").validateOpt[Long]
      patientBank <- (request.body \ "patientBank").validateOpt[String]
      refundAmount <- (request.body \ "refundAmount").validateOpt[Float]
      refundDateValue <- (request.body \ "refundDate").validateOpt[String]
      refundPayMethod <- (request.body \ "refundPayMethod").validateOpt[String]
      renewalDateValue <- (request.body \ "renewalDate").validateOpt[String]
      familyAnnualMax <- (request.body \ "familyAnnualMax").validateOpt[Float]
      individualAnnualMax <- (request.body \ "individualAnnualMax").validateOpt[Float]
      medicalInsuranceName <- (request.body \ "medicalInsuranceName").validateOpt[String]
      prognosis <- (request.body \ "prognosis").validateOpt[String]
      priority <- (request.body \ "priority").validateOpt[String]
      service <- (request.body \ "service").validateOpt[String]
      phase <- (request.body \ "phase").validateOpt[String]


    } yield {
       var datePerformed: Option[LocalDate] = None
      if(datePerformedValue != None && datePerformedValue != Some("")){
        datePerformed = Some(LocalDate.parse(convertOptionString(datePerformedValue)))
      }

      var renewalDate: Option[LocalDate] = None
      if(renewalDateValue != None && renewalDateValue != Some("")){
        renewalDate = Some(LocalDate.parse(convertOptionString(renewalDateValue)))
      }

      var refundDate: Option[LocalDate] = None
      if(refundDateValue != None && refundDateValue != Some("")){
        refundDate = Some(LocalDate.parse(convertOptionString(refundDateValue)))
      }

      var insurancePaid_Date: Option[LocalDate] = None
      if(insurancePaidDate != None && insurancePaidDate != Some("")){
        insurancePaid_Date= Some(LocalDate.parse(convertOptionString(insurancePaidDate)))
      }

      var patientPaid_Date: Option[LocalDate] = None
      if(patientPaidDate != None && patientPaidDate != Some("")){
        patientPaid_Date = Some(LocalDate.parse(convertOptionString(patientPaidDate)))
      }

      (TreatmentPlanDetailsRow(None,convertOptionLong(treatmentPlanId),convertOptionString(cdcCode),convertOptionString(procedureName),convertOptionString(toothNumber),convertOptionString(status),convertOptionalLocalDate(datePlanned),datePerformed,convertOptionString(surface),convertOptionFloat(fee),convertOptionFloat(estIns),providerId,insurancePaidAmt,insurancePaid_Date,patientPaidAmt,patientPaid_Date,convertOptionString(insurancePayMethod),convertOptionString(patientPayMethod),patientId,insurance,patientBank,refundAmount,refundDate,refundPayMethod,renewalDate,familyAnnualMax,individualAnnualMax,medicalInsuranceName,prognosis,priority,service,phase),treatmentPlanId,cdcCode,procedureName,toothNumber,status,datePlanned,datePerformed,surface,fee,estIns,providerId,insurancePaidAmt,insurancePaid_Date,patientPaidAmt,patientPaid_Date,insurancePayMethod,patientPayMethod,patientId,insurance)
    } 
    
    treatmentPlan match {
      case JsSuccess((treatmentPlanDetail,treatmentPlanId,cdcCode,procedureName,toothNumber,status,datePlanned,datePerformed,surface,fee,estIns,providerId,insurancePaidAmt,insurancePaid_Date,patientPaidAmt,patientPaid_Date,insurancePayMethod,patientPayMethod,patientId,insurance), _) =>
      var statusValidation : Boolean = false
      if(convertOptionString(status).toLowerCase != "completed"){
        if(datePerformed != None) {
              statusValidation = true
        }
      }
      var datePerformedValidation : Boolean = false
      if(convertOptionString(status).toLowerCase == "completed"){
        if(datePerformed == None) {
              datePerformedValidation = true
        }
      }
      var codeToToothExists = false
      var repeatBlock = db.run{
      TreatmentPlanDetailsTable.filter(_.patientId === convertOptionLong(patientId)).filter(s=> s.status === "Accepted" || s.status === "Proposed").filter(_.cdcCode === cdcCode).filterNot(_.id === id).filter(_.toothNumber === toothNumber).result
      } map { planDetailsList =>
      planDetailsList.foreach(result => {
      codeToToothExists = true
      })
      
      }
      Await.ready(repeatBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

      if(codeToToothExists){
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Selected procedure already planned for the teeth !"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      }
      else {
      if(statusValidation){
      val obj = Json.obj()
      val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Date Performed should be blank for the status Proposed and Accepted!"))
      Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else if(datePerformedValidation){
          val obj = Json.obj()
          val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Date Performed should not be blank for the status completed!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else {
       db.run {
          val query = TreatmentPlanDetailsTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](

                treatmentPlanId.map(value => query.map(_.treatmentPlanId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                cdcCode.map(value => query.map(_.cdcCode).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                procedureName.map(value => query.map(_.procedureName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                toothNumber.map(value => query.map(_.toothNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                datePlanned.map(value => query.map(_.datePlanned).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                datePerformed.map(value => query.map(_.datePerformed).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                surface.map(value => query.map(_.surface).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                fee.map(value => query.map(_.fee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                estIns.map(value => query.map(_.estIns).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(providerId).map(value => query.map(_.providerId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(insurancePaidAmt).map(value => query.map(_.insurancePaidAmt).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(insurancePaid_Date).map(value => query.map(_.insurancePaidDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(patientPaidAmt).map(value => query.map(_.patientPaidAmt).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(patientPaid_Date).map(value => query.map(_.patientPaidDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                insurancePayMethod.map(value => query.map(_.insurancePayMethod).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                patientPayMethod.map(value => query.map(_.patientPayMethod).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(insurance).map(value => query.map(_.insurance).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.patientBank).map(value => query.map(_.patientBank).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.refundAmount).map(value => query.map(_.refundAmount).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.refundDate).map(value => query.map(_.refundDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.refundPayMethod).map(value => query.map(_.refundPayMethod).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.renewalDate).map(value => query.map(_.renewalDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.familyAnnualMax).map(value => query.map(_.familyAnnualMax).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.individualAnnualMax).map(value => query.map(_.individualAnnualMax).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.medicalInsuranceName).map(value => query.map(_.medicalInsuranceName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.prognosis).map(value => query.map(_.prognosis).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.priority).map(value => query.map(_.priority).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                // Some(treatmentPlanDetail.surface).map(value => query.map(_.surface).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(treatmentPlanDetail.phase).map(value => query.map(_.phase).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

              ) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
        db.run{
            TreatmentPlanDetailsTable.filter(_.id === id).result
        }map{ planDetails => planDetails.map{ treatment =>
            db.run {
            TreatmentPlanTable.filterNot(_.deleted).filter(_.id === treatment.treatmentPlanId).map(_.dateUpdated).update(LocalDate.now())
            } map {
                case 0 => NotFound
                case _ => Ok
            }
          }
          Ok
        }
      }
    }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  
    def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      TreatmentPlanDetailsTable.filter(_.id === id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }


  def generatePdf(patientId: Long, status: Option[String])=silhouette.SecuredAction.async { request =>
    var cdcCode: String = ""
    var procedureName: String = ""
    var toothNumber: String = ""
    var vStatus: String = ""
    var patientName = ""
    var chartNumber = ""
    var datePlanned: String = ""
    var datePerformed: String = ""
    var surface : String = ""
    var fee: Float = 0
    var estIns: Float = 0
    var disc: Float = 0
    var phase = ""
    var data = List.empty[PlanDetailList]
    val statusValue = convertOptionString(status)//.split(',')
    var dataList1 = List.empty[PlanDetailList]
    var dataList2 = List.empty[PlanDetailList]
    var dataList3 = List.empty[PlanDetailList]
    var dataList4 = List.empty[PlanDetailList]
    var dataList5 = List.empty[PlanDetailList]

    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    val imgLogo =  Image.getInstance("https://s3.amazonaws.com/ds-static.spfr.co/img/placeholder.jpg");

    db.run {
      TreatmentPlanDetailsTable.filter(_.patientId === patientId).filterNot(_.status === "").join(StepTable).on(_.patientId === _.treatmentId).result
      } map { treatmentPlanDetail =>
        val planData = treatmentPlanDetail.groupBy(_._1.id).map {
          case (id, entries) =>
          val planDetail = entries.head._1
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

              if((statusValue.toLowerCase) contains (planDetail.status.toLowerCase)){
              val firstName = convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
              val lastName =  convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
              patientName = firstName+" "+lastName

              chartNumber = convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "patient_id").asOpt[String]))
              cdcCode = planDetail.cdcCode
              procedureName = planDetail.procedureName
              toothNumber = planDetail.toothNumber
              vStatus = planDetail.status
              phase = convertOptionString(planDetail.phase)

              var formatter: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");
              datePlanned = formatter.print(planDetail.datePlanned)
              if(planDetail.datePerformed != None){
                  datePerformed = formatter.print(convertOptionalLocalDate(planDetail.datePerformed))
              }else{
                  datePerformed = ""
              }

              surface = planDetail.surface

              fee = (Math.round(planDetail.fee * 100.0) / 100.0).toFloat
              estIns = (Math.round(planDetail.estIns * 100.0) / 100.0).toFloat
              disc = (Math.round((planDetail.fee - planDetail.estIns) * 100.0) / 100.0).toFloat

              if(vStatus == "Proposed"){
                dataList1 = dataList1 :+ PlanDetailList(cdcCode,procedureName,datePlanned,datePerformed,toothNumber,vStatus,surface,phase,fee,estIns,disc)
              } else if(vStatus == "Accepted"){
                dataList2 = dataList2 :+ PlanDetailList(cdcCode,procedureName,datePlanned,datePerformed,toothNumber,vStatus,surface,phase,fee,estIns,disc)
              } else if(vStatus == "Completed"){
                dataList3 = dataList3 :+ PlanDetailList(cdcCode,procedureName,datePlanned,datePerformed,toothNumber,vStatus,surface,phase,fee,estIns,disc)
              } else if(vStatus == "Referred Out") {
                dataList4 = dataList4 :+ PlanDetailList(cdcCode,procedureName,datePlanned,datePerformed,toothNumber,vStatus,surface,phase,fee,estIns,disc)
              } else if(vStatus == "Existing"){
                dataList5 = dataList5 :+ PlanDetailList(cdcCode,procedureName,datePlanned,datePerformed,toothNumber,vStatus,surface,phase,fee,estIns,disc)
              }

              data = dataList1.sortBy(_.datePlanned) ::: dataList2.sortBy(_.datePlanned) ::: dataList3.sortBy(_.datePlanned) ::: dataList4.sortBy(_.datePlanned) ::: dataList5.sortBy(_.datePlanned)
          }
        }

            val planDetailSample = data

          val source = StreamConverters.fromInputStream { () =>

              import java.io.IOException
              import com.itextpdf.text.pdf._
              import com.itextpdf.text.{List => _, _}
              import com.itextpdf.text.Paragraph
              import com.itextpdf.text.Element
              import com.itextpdf.text.Rectangle
              import com.itextpdf.text.pdf.PdfPTable
              import com.itextpdf.text.Font;
              import com.itextpdf.text.Font.FontFamily;
              import com.itextpdf.text.BaseColor;
              import com.itextpdf.text.BaseColor
              import com.itextpdf.text.Font
              import com.itextpdf.text.FontFactory
              import com.itextpdf.text.pdf.BaseFont

              import com.itextpdf.text.pdf.PdfContentByte
              import com.itextpdf.text.pdf.PdfPTable
              import com.itextpdf.text.pdf.PdfPTableEvent

              import com.itextpdf.text.Element
              import com.itextpdf.text.Phrase
              import com.itextpdf.text.pdf.ColumnText
              import com.itextpdf.text.pdf.PdfContentByte
              import com.itextpdf.text.pdf.PdfPageEventHelper
              import com.itextpdf.text.pdf.PdfWriter

              class MyFooter extends PdfPageEventHelper {

                val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
                ffont.setColor(new BaseColor(64, 64, 65));

                override def onEndPage(writer: PdfWriter, document: Document): Unit = {
                  val cb = writer.getDirectContent
                  val footer = new Phrase("" + writer.getPageNumber(), ffont)
                  var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
                  val footerDate = new Phrase(submittedAt, ffont)

                  ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
                  ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
                }
              }

              class BorderEvent extends PdfPTableEvent {
                override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
                  val width = widths(0)
                  val x1 = width(0)
                  val x2 = width(width.length - 1)
                  val y1 = heights(0)
                  val y2 = heights(heights.length - 1)
                  val cb = canvases(PdfPTable.LINECANVAS)
                  cb.rectangle(x1, y1, x2 - x1, y2 - y1)
                  cb.setLineWidth(0.4)
                  cb.stroke()
                  cb.resetRGBColorStroke()
                }
              }

              val document = new Document(PageSize.A4.rotate(), 0, 0, 0, 0)
              val inputStream = new PipedInputStream()
              val outputStream = new PipedOutputStream(inputStream)
              val writer = PdfWriter.getInstance(document, outputStream)
              writer.setPageEvent(new MyFooter())
              document.setMargins(30, 30, 15, 15)
              Future({
                document.open()

                import com.itextpdf.text.BaseColor
                import com.itextpdf.text.Font
                import com.itextpdf.text.FontFactory
                import com.itextpdf.text.pdf.BaseFont

                import com.itextpdf.text.FontFactory


                val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
                FONT_TITLE.setColor(new BaseColor(234, 98, 37));
                val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
                // FONT_CONTENT.setColor(new BaseColor(64, 64, 65));
                val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
                // FONT_COL_HEADER.setColor(new BaseColor(64, 64, 65));
                FONT_COL_HEADER.setColor(BaseColor.WHITE);
                val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 13)

                import com.itextpdf.text.FontFactory
                import java.net.URL


                var headerImageTable: PdfPTable = new PdfPTable(1)
                headerImageTable.setWidthPercentage(100);

                try {
                  val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
                  val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
                  val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
                  val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
                  val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
                  val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
                  val g = img.createGraphics()
                  g.drawRenderedImage(bi, at)
                  val imgBytes = new ByteArrayOutputStream()
                  ImageIO.write(img, "PNG", imgBytes)
                  val image = Image.getInstance(imgBytes.toByteArray)
                  image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
                  image.setCompressionLevel(PdfStream.NO_COMPRESSION)
                  image.setAlignment(Image.RIGHT)
                  val cell = new PdfPCell(image);
                  cell.setPadding(8);
                  cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                  cell.setBorder(Rectangle.NO_BORDER);
                  headerImageTable.addCell(cell)
                  document.add(headerImageTable);
                } catch {
                  case error: Throwable => logger.debug(error.toString)
                }
                document.add(new Paragraph("\n"));

                var headerTextTable: PdfPTable = new PdfPTable(1)
                headerTextTable.setWidthPercentage(100);
                var cell = new PdfPCell(new Phrase("Treatment Plan", FONT_TITLE));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                headerTextTable.addCell(cell)
                document.add(headerTextTable);

                document.add(new Paragraph("\n"));
                val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 13)
                FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
                var headerTextTable1: PdfPTable = new PdfPTable(1)
                headerTextTable1.setWidthPercentage(100);
                var cell3= new PdfPCell(new Phrase("Name : "+patientName, FONT_TITLE1));
                cell3.setBorder(Rectangle.NO_BORDER);
                cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                //cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
                headerTextTable1.addCell(cell3)
                document.add(headerTextTable1);

                var headerTextTable2: PdfPTable = new PdfPTable(1)
                headerTextTable2.setWidthPercentage(100);
                var cell4 = new PdfPCell(new Phrase("Chart Number : "+ chartNumber, FONT_TITLE1));
                cell4.setBorder(Rectangle.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
                //cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
                headerTextTable2.addCell(cell4)
                document.add(headerTextTable2);

                    document.add(new Paragraph("\n"));

                        var rowsPerPage = 8;
                        var recNum = 0;
                        if(planDetailSample.length != 0){
                          (planDetailSample).foreach(planDetailRow => {
                            var sampleData = List(s"${planDetailRow.cdcCode}",s"${planDetailRow.datePlanned}", s"${planDetailRow.datePerformed}", s"${planDetailRow.toothNumber}", s"${planDetailRow.surface}",s"${planDetailRow.status}",s"${planDetailRow.phase}",s"${planDetailRow.fee}",s"${planDetailRow.estIns}",s"${planDetailRow.disc}")

                            if(planDetailRow.status == "Proposed"){
                              FONT_CONTENT.setColor(new BaseColor(234, 98, 37));
                            } else if(planDetailRow.status == "Accepted"){
                              FONT_CONTENT.setColor(new BaseColor(56, 160, 241));
                            } else if(planDetailRow.status == "Completed"){
                              FONT_CONTENT.setColor(new BaseColor(135, 180, 77));
                            } else if(planDetailRow.status == "Referred Out") {
                              FONT_CONTENT.setColor(new BaseColor(145, 40, 143));
                            } else if(planDetailRow.status == "Existing"){
                              FONT_CONTENT.setColor(new BaseColor(0, 0, 0));
                            }

                            var columnWidths:Array[Float] = Array(9,12,13,7,13,10,7,8,7,14);
                            var table: PdfPTable = new PdfPTable(columnWidths);
                            table.setWidthPercentage(100);
                            if(recNum == 0 ) {

                              document.add(new Paragraph("\n"));

                              cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                              cell.setPadding(7);
                              // cell.setPaddingBottom(16);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Date Planned", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Tooth", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Surface", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Phase", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(234, 98, 37));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(135, 180, 77));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setBackgroundColor(new BaseColor(135, 180, 77));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)

                              cell = new PdfPCell(new Phrase("Balance to patient", FONT_COL_HEADER));
                              cell.setPadding(7);
                              cell.setPaddingBottom(12);
                              cell.setBackgroundColor(new BaseColor(135, 180, 77));
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table.addCell(cell)
                            }

                            (sampleData).foreach(item => {
                              cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                              cell.setPaddingLeft(5);
                              cell.setPaddingTop(16);
                              cell.setPaddingBottom(16);
                              cell.setBorder(Rectangle.TOP);
                              cell.setBorderColorTop(new BaseColor(221, 221, 221));
                              cell.setBackgroundColor(BaseColor.WHITE);
                              cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                              cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
                              cell.setUseVariableBorders(true);
                              table.addCell(cell);
                            })

                            document.add(table);

                            recNum = recNum+1;
                            if(recNum == 0 ) {
                              document.newPage();
                            }

                          })
                        } else {
                          var columnWidths:Array[Float] = Array(9,12,13,7,13,10,7,8,7,14);
                          var table: PdfPTable = new PdfPTable(columnWidths);
                          table.setWidthPercentage(100);
                          if(recNum == 0 ) {

                            document.add(new Paragraph("\n"));

                            cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Date Planned", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Tooth", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Surface", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Phase", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(234, 98, 37));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(135, 180, 77));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)
                            cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setBackgroundColor(new BaseColor(135, 180, 77));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            cell = new PdfPCell(new Phrase("Balance to patient", FONT_COL_HEADER));
                            cell.setPadding(7);
                            cell.setPaddingBottom(12);
                            cell.setBackgroundColor(new BaseColor(135, 180, 77));
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            cell.setBorder(Rectangle.NO_BORDER);
                            table.addCell(cell)

                            document.add(table);

                            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
                            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
                            var headerTextTable1: PdfPTable = new PdfPTable(1)
                            headerTextTable1.setWidthPercentage(97);
                            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
                            cell1.setBorder(Rectangle.NO_BORDER);
                            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                            headerTextTable1.addCell(cell1)
                            document.add(headerTextTable1);

                          }
                        }

                        var rRecNum = 0;
                        (planDetailSample).groupBy(_.cdcCode).map{
                        case (id,entries) =>
                          var planDetailRow = entries.head

                            var sampleData = List(s"${planDetailRow.cdcCode}",s"${planDetailRow.procedureName}")
                            FONT_COL_HEADER.setColor(new BaseColor(64, 64, 65));
                            FONT_CONTENT.setColor(new BaseColor(64, 64, 65));

                            var columnWidths:Array[Float] = Array(30,70);
                            var table1: PdfPTable = new PdfPTable(columnWidths);
                            table1.setWidthPercentage(100);

                            if(rRecNum == 0 ) {

                              document.add(new Paragraph("\n\n"));

                              cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                              cell.setPadding(8);
                              cell.setBackgroundColor(BaseColor.WHITE);
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table1.addCell(cell)

                              cell = new PdfPCell(new Phrase("Description", FONT_COL_HEADER));
                              cell.setPadding(8);
                              cell.setBackgroundColor(BaseColor.WHITE);
                              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                              cell.setBorder(Rectangle.NO_BORDER);
                              table1.addCell(cell)
                            }

                            (sampleData).foreach(item => {
                              cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                              cell.setPaddingLeft(5);
                              cell.setPaddingTop(16);
                              cell.setPaddingBottom(16);
                              cell.setBorder(Rectangle.TOP);
                              cell.setBorderColorTop(new BaseColor(221, 221, 221));
                              cell.setBackgroundColor(BaseColor.WHITE);
                              cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                              cell.setUseVariableBorders(true);
                              table1.addCell(cell);
                            })

                            document.add(table1);

                            rRecNum = rRecNum+1;
                            if(rRecNum == 0 ) {
                              document.newPage();
                            }
                            // })
                          }

                document.close();

              })(ioBoundExecutor)

              inputStream
        }

      val filename = s"treatmentPlan"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )

      }
    }

def migrateTreatmentPlanPatientId = silhouette.SecuredAction.async(parse.json) { request =>
      db.run {
        TreatmentPlanDetailsTable
        .join(TreatmentPlanTable).on(_.treatmentPlanId === _.id).result
 } map { result =>
      val data = result.groupBy(_._1.id).map {
        case (planDetailId, entries) =>
         val treatmentPlanDetail = entries.head._1
         val treatmentPlan = entries.head._2

      db.run {
          TreatmentPlanDetailsTable.filter(_.treatmentPlanId === treatmentPlan.id).map(_.patientId).update(Some(treatmentPlan.patientId))
      } map {
      case 0 => NotFound
      case _ => Ok
    }
    }
      Ok {Json.obj(
                    "status"->"success",
                    "message"->"PatientId migrated successfully"
                  )
                }
    }
 }

case class TreatmentPlanAppList(
  patientName: String,
  paidAmount: Double,
  paidDate: LocalDate,
  bank: String

)
implicit val TreatmentAppRowWrites = Json.writes[TreatmentPlanAppList]
//
 def depositSlip(startDate:String, endDate:String, vPayMethod:Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var vStartDate = LocalDate.parse(startDate)
 var vEndDate = LocalDate.parse(endDate)
 val obj = Json.obj()
 var insurancePatientValue : List[JsValue] = List()
 var refundValue : List[JsValue] = List()
 var depositTotal: Float = 0
 var depositTotalEntries = 0
 var refundTotal: Float = 0
 var refundTotalEntries = 0
 var patientName = ""
 var payMethod = convertOptionString(vPayMethod)

     db.run{
       TreatmentPlanDetailsTable.filter(s => s.insurancePaidDate >= vStartDate && s.insurancePaidDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { insurancePaidList =>
     if(payMethod != None && payMethod != ""){
        var data = insurancePaidList.groupBy(_._1.insurancePayMethod).map{ row =>
        var mergeList = List.empty[TreatmentPlanAppList]
        var insurancePaidTotal: Float = 0
        var insuranceEntriesTotal = 0
        if((row._1 != "" && row._1 != None && row._1 != null) && (payMethod contains row._1) ){
        (row._2).foreach(r => {
            var insPayList = r._1
            var patientDetails = r._2
            val insurancePaidAmt = convertOptionFloat(insPayList.insurancePaidAmt)
            val str = f"${insurancePaidAmt}%.2f"
              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              insurancePaidTotal = insurancePaidTotal + convertOptionFloat(insPayList.insurancePaidAmt)
              insuranceEntriesTotal = insuranceEntriesTotal + 1
              var dataList = TreatmentPlanAppList(patientName,str.toDouble,convertOptionalLocalDate(insPayList.insurancePaidDate),convertOptionString(insPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
        })
            depositTotal = depositTotal + insurancePaidTotal
            depositTotalEntries = depositTotalEntries + insuranceEntriesTotal
            val newObj = obj + ("payMethod" -> JsString(row._1)) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(f"${insurancePaidTotal}%.2f".toDouble)) + ("totalEntries" -> JsNumber(insuranceEntriesTotal))
            insurancePatientValue = insurancePatientValue :+ newObj
        }
        }
     }

    // PATIENT PAID RECORDS
     var patientBlock = db.run{
       TreatmentPlanDetailsTable.filter(s => s.patientPaidDate >= vStartDate && s.patientPaidDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { patientPaidList =>
     if(payMethod != None && payMethod != ""){
     patientPaidList.groupBy(_._1.patientPayMethod).map{ row =>
     var mergeList = List.empty[TreatmentPlanAppList]
     var patientPaidTotal: Float = 0
     var patientEntriesTotal = 0
     if((row._1 != "" && row._1 != None && row._1 != null) && (payMethod contains row._1) ){
            (row._2).foreach(r => {
            var patientPayList = r._1
            var patientDetails = r._2
            val patientPaidAmt = convertOptionFloat(patientPayList.patientPaidAmt)
            val str = f"${patientPaidAmt}%.2f"
              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              patientEntriesTotal = patientEntriesTotal + 1
              patientPaidTotal = patientPaidTotal + convertOptionFloat(patientPayList.patientPaidAmt)
              var dataList = TreatmentPlanAppList(patientName,str.toFloat,convertOptionalLocalDate(patientPayList.patientPaidDate),convertOptionString(patientPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
            })
            depositTotal = depositTotal + patientPaidTotal
            depositTotalEntries = depositTotalEntries + patientEntriesTotal
            val newObj = obj + ("payMethod" -> JsString(row._1)) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(f"${patientPaidTotal}%.2f".toDouble))+ ("totalEntries" -> JsNumber(patientEntriesTotal))
            insurancePatientValue = insurancePatientValue :+ newObj
        }
      }
     }
     }
     Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    // REFUND PAID RECORDS
     var refundBlock = db.run{
       TreatmentPlanDetailsTable.filter(s => s.refundDate >= vStartDate && s.refundDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { refundList =>
     refundList.groupBy(_._1.refundPayMethod).map{ row =>
     if(payMethod != None && payMethod != ""){
     var mergeList = List.empty[TreatmentPlanAppList]
     var refundPaidTotal: Float = 0
     var refundEntries = 0

     if((convertOptionString(row._1) != "" && convertOptionString(row._1) != None && convertOptionString(row._1) != null) && (payMethod contains convertOptionString(row._1)) ){
            (row._2).foreach(r => {
            var refundPayList = r._1
            var patientDetails = r._2
            val refundAmount = convertOptionFloat(refundPayList.refundAmount)
            val str = f"${refundAmount}%.2f"
              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              refundEntries = refundEntries + 1
              refundPaidTotal = refundPaidTotal + convertOptionFloat(refundPayList.refundAmount)
              var dataList = TreatmentPlanAppList(patientName,str.toDouble,convertOptionalLocalDate(refundPayList.refundDate),convertOptionString(refundPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
            })
            refundTotal = refundTotal + refundPaidTotal
            refundTotalEntries = refundTotalEntries + refundEntries
            val newObj = obj + ("payMethod" -> JsString(convertOptionString(row._1))) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(f"${refundPaidTotal}%.2f".toDouble)) + ("totalEntries" -> JsNumber(refundEntries))
            refundValue = refundValue :+ newObj
          }
        }
      }
     }
     Await.ready(refundBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

     var grandTotal = depositTotal - refundTotal
     val resultObj = obj + ("deposits" -> JsArray(insurancePatientValue)) + ("refund" -> JsArray(refundValue)) + ("depositTotal" -> JsNumber(f"${depositTotal}%.2f".toDouble)) + ("depositTotalEntries" -> JsNumber(depositTotalEntries)) + ("refundTotal" -> JsNumber(f"${refundTotal}%.2f".toDouble)) + ("refundTotalEntries" -> JsNumber(refundTotalEntries)) + ("grandTotal" -> JsNumber(f"${grandTotal}%.2f".toDouble))
      Ok(Json.toJson(resultObj))
     }

 }

def importDefaultFee = silhouette.SecuredAction.async { request =>
val url = "https://s3.amazonaws.com/static.novadonticsllc.com/defaultFee/defaultFeeTestUpdate.csv"
var count: Int = 0
var line: String = null

	val response = Http(url)
          .header("Content-Type", "text/xml")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(10000)).asString

  val stream: java.io.InputStream = new java.io.ByteArrayInputStream(response.body.getBytes(java.nio.charset.StandardCharsets.UTF_8.name))
  val reader = new BufferedReader(new InputStreamReader(stream));

  line = reader.readLine()

    while (line  != null) {
    if(count != 0) {
      if(line != null && line != None && line != ""){
//      val cols = line.split(",").map(_.trim)
        val cols = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").map(_.trim)

      var b = db.run{
        DefaultFeeTable.filter(_.procedureCode === cols(0)).result
      } map { result =>

      if(result.length > 0){
            db.run {
            val query = DefaultFeeTable.filter(_.procedureCode === cols(0))
            var ucrFee = convertStringToLong(Some(cols(3)))
            var privateFee = convertStringToLong(Some(cols(2)))

            query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
              case true =>
                DBIO.seq[Effect.Read with Effect.Write](
                  Some(ucrFee).map(value => query.map(_.ucrFee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  Some(privateFee).map(value => query.map(_.privateFee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                ) map { _ => Ok
                }
              case false =>
                DBIO.successful(NotFound)
            }
          }
      } else {
        var defaultFeeRow = DefaultFeeRow(None, cols(0),convertStringToLong(Some(cols(2))),convertStringToLong(Some(cols(3))))
            var block =  db.run {
            DefaultFeeTable.returning(DefaultFeeTable.map(_.id)) += defaultFeeRow
            } map {
            case 0L => PreconditionFailed
            case id => Created(Json.toJson(defaultFeeRow.copy(id = Some(id))))
            }
        }
      }
      Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    }
  }
    count += 1
    line = reader.readLine()
    }
    reader.close();
    Future.successful(Ok{Json.obj("status"->"success","message"->"DefaultFee imported successfully")})
  }

def ledger(patientId :Option[Long]) = silhouette.SecuredAction.async { request =>
var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
var patientName = ""
var providerName = ""
var description = ""
var dentalInsuranceName = ""

  var query = TreatmentPlanDetailsTable.filter(_.status === "Completed")
  if(patientId != None){
    query = query.filter(_.patientId === patientId)
  } else {
    var emptyRec: Long = 0
    query = query.filter(_.patientId === emptyRec)
  }

  db.run{
    query
    .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id)
    .joinLeft(ProviderTable.filterNot(_.deleted)).on(_._1.providerId === _.id)
    .joinLeft(ProcedureTable.filterNot(_.deleted)).on(_._1._1.cdcCode === _.procedureCode)
    .joinLeft(DentalCarrierTable).on(_._1._1._1.insurance === _.id).result
  } map { treatmentPlanList =>
        val data = treatmentPlanList.groupBy(_._1._1._1._1.id).map {
        case (id, items) => {
            val treatmentPlan = items.head._1._1._1._1
            val treatment = items.head._1._1._1._2
            patientName = convertOptionString(treatment.firstName) + " " + convertOptionString(treatment.lastName)

            val provider = items.filter(_._1._1._2.isDefined).map(_._1._1._2.get).map(user => providerName = user.firstName + " " + user.lastName)

            val procedure = items.filter(_._1._2.isDefined).map(_._1._2.get).map(user => description = user.procedureType)

            val dental = items.filter(_._2.isDefined).map(_._2.get).map(user => dentalInsuranceName = user.name)

            val totalValue = Math.round((convertOptionFloat(treatmentPlan.insurancePaidAmt) * 100.0) / 100.0) + Math.round((convertOptionFloat(treatmentPlan.patientPaidAmt) * 100.0) / 100.0)
            val patientBalance = treatmentPlan.fee - totalValue

            LedgerRow(treatmentPlan.datePerformed,patientName,treatmentPlan.toothNumber,treatmentPlan.surface,treatmentPlan.fee,dentalInsuranceName,treatmentPlan.medicalInsuranceName,treatmentPlan.insurancePaidAmt,treatmentPlan.insurancePayMethod,treatmentPlan.patientPaidAmt,treatmentPlan.patientPayMethod,treatmentPlan.cdcCode,description,providerName,patientBalance)
        }
      }
      Ok(Json.toJson(data))
    }
}

def depositSlipPDF(startDate:String, endDate:String, vPayMethod:Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var vStartDate = LocalDate.parse(startDate)
 var vEndDate = LocalDate.parse(endDate)
 val obj = Json.obj()
 var insurancePatientValue : List[JsValue] = List()
 var refundValue : List[JsValue] = List()
 var depositTotal: Float = 0
 var depositTotalEntries = 0
 var refundTotal: Float = 0
 var refundTotalEntries = 0
 var patientName = ""
 var payMethod = convertOptionString(vPayMethod)
 import java.text.NumberFormat
 val locale = new Locale("en", "US")
 val formatter = NumberFormat.getCurrencyInstance(locale)
 val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

     db.run{
       TreatmentPlanDetailsTable.filter(s => s.insurancePaidDate >= vStartDate && s.insurancePaidDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { insurancePaidList =>
     if(payMethod != None && payMethod != ""){
        var data = insurancePaidList.groupBy(_._1.insurancePayMethod).map{ row =>
        var mergeList = List.empty[TreatmentPlanAppList]
        var insurancePaidTotal: Float = 0
        var insuranceEntriesTotal = 0
        if((row._1 != "" && row._1 != None && row._1 != null) && (payMethod contains row._1) ){
        (row._2).foreach(r => {
            var insPayList = r._1
            var patientDetails = r._2
              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              insurancePaidTotal = insurancePaidTotal + convertOptionFloat(insPayList.insurancePaidAmt)
              insuranceEntriesTotal = insuranceEntriesTotal + 1
              var dataList = TreatmentPlanAppList(patientName,convertOptionFloat(insPayList.insurancePaidAmt),convertOptionalLocalDate(insPayList.insurancePaidDate),convertOptionString(insPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
        })
            depositTotal = depositTotal + insurancePaidTotal
            depositTotalEntries = depositTotalEntries + insuranceEntriesTotal
            val newObj = obj + ("payMethod" -> JsString(row._1)) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(insurancePaidTotal)) + ("totalEntries" -> JsNumber(insuranceEntriesTotal))
            insurancePatientValue = insurancePatientValue :+ newObj
        }
        }
     }

    // PATIENT PAID RECORDS
     var patientBlock = db.run{
       TreatmentPlanDetailsTable.filter(s => s.patientPaidDate >= vStartDate && s.patientPaidDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { patientPaidList =>
     if(payMethod != None && payMethod != ""){
     patientPaidList.groupBy(_._1.patientPayMethod).map{ row =>
     var mergeList = List.empty[TreatmentPlanAppList]
     var patientPaidTotal: Float = 0
     var patientEntriesTotal = 0
     if((row._1 != "" && row._1 != None && row._1 != null) && (payMethod contains row._1) ){
            (row._2).foreach(r => {
            var patientPayList = r._1
            var patientDetails = r._2
              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              patientEntriesTotal = patientEntriesTotal + 1
              patientPaidTotal = patientPaidTotal + convertOptionFloat(patientPayList.patientPaidAmt)
              var dataList = TreatmentPlanAppList(patientName,convertOptionFloat(patientPayList.patientPaidAmt),convertOptionalLocalDate(patientPayList.patientPaidDate),convertOptionString(patientPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
            })
            depositTotal = depositTotal + patientPaidTotal
            depositTotalEntries = depositTotalEntries + patientEntriesTotal
            val newObj = obj + ("payMethod" -> JsString(row._1)) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(patientPaidTotal))+ ("totalEntries" -> JsNumber(patientEntriesTotal))
            insurancePatientValue = insurancePatientValue :+ newObj
        }
      }
     }
     }
     Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    // REFUND PAID RECORDS
     var refundBlock = db.run{
       TreatmentPlanDetailsTable.filter(s => s.refundDate >= vStartDate && s.refundDate <= vEndDate)
       .join(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id).result
     }map { refundList =>
     if(payMethod != None && payMethod != ""){
     refundList.groupBy(_._1.refundPayMethod).map{ row =>
     var mergeList = List.empty[TreatmentPlanAppList]
     var refundPaidTotal: Float = 0
     var refundEntries = 0

     if((convertOptionString(row._1) != "" && convertOptionString(row._1) != None && convertOptionString(row._1) != null) && (payMethod contains convertOptionString(row._1)) ){
            (row._2).foreach(r => {
            var refundPayList = r._1
            var patientDetails = r._2

              patientName = convertOptionString(patientDetails.firstName) +" "+ convertOptionString(patientDetails.lastName)
              refundEntries = refundEntries + 1
              refundPaidTotal = refundPaidTotal + convertOptionFloat(refundPayList.refundAmount)
              var dataList = TreatmentPlanAppList(patientName,convertOptionFloat(refundPayList.refundAmount),convertOptionalLocalDate(refundPayList.refundDate),convertOptionString(refundPayList.patientBank))
              mergeList = (mergeList :+ dataList).sortBy(_.paidDate)
            })
            refundTotal = refundTotal + refundPaidTotal
            refundTotalEntries = refundTotalEntries + refundEntries
            val newObj = obj + ("payMethod" -> JsString(convertOptionString(row._1))) + ("entries" -> Json.toJson(mergeList)) + ("total" -> JsNumber(refundPaidTotal)) + ("totalEntries" -> JsNumber(refundEntries))
            refundValue = refundValue :+ newObj
          }
        }
      }
     }
     Await.ready(refundBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)
              val footerFont = FontFactory.getFont(FontFactory.HELVETICA, 11)
              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 20, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 20, 0)
            }
          }

          val document = new Document(PageSize.A4.rotate(),0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(50, 45, 50, 60)
          document.setMarginMirroring(false);
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 19)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
            val FONT_BODY_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 13)
            val FONT_H2_TABLE = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 13)
            val FONT_H3_TABLE = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)
            
            FONT_BODY_TITLE.setColor(BaseColor.GRAY)
            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

            var cell = new PdfPCell(new Phrase("Deposit Slip Report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);
          var fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
          if(vStartDate != vEndDate){
            var cell1 = new PdfPCell(new Phrase(" Deposit Slip From "+ fmt.print(vStartDate) + " To "+fmt.print(vEndDate), FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          } else {
            var cell1 = new PdfPCell(new Phrase("Deposit Slip Date "+ fmt.print(vStartDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;
          if(insurancePatientValue.length > 0){
          document.add(new Paragraph("Deposits",FONT_H2_TABLE))
          insurancePatientValue.foreach(row=>{
          var payMethod = (row \ "payMethod").as[String]
          var payMethodParagraph = new Paragraph(payMethod.split(' ').map(_.capitalize).mkString(" "))
          payMethodParagraph.setSpacingBefore(10);
          document.add(payMethodParagraph)
          var columnWidths:Array[Float] = Array(25,25,25,25);
          var table1: PdfPTable = new PdfPTable(columnWidths)
          table1.setWidthPercentage(100);
          var entriesToDeposits = (row \ "entries").as[List[JsValue]]

          var cell2 = new PdfPCell(new Phrase( "Paid Date", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Patient Name", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Bank", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase("Paid Amount", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          entriesToDeposits.foreach(entry=>{
          var paidDate = (entry \ "paidDate").as[JsValue].toString
          paidDate = fmt.print(LocalDate.parse(paidDate.substring(1, paidDate.length()-1)))
          var patientName = (entry \ "patientName").as[JsValue].toString
          var bank = (entry \ "bank").as[JsValue].toString
          var paidAmount = (entry \ "paidAmount").as[JsValue].toString
          paidAmount = formatter.format(paidAmount.toFloat).toString
          var data = List(paidDate,patientName,bank,paidAmount)
          data.foreach(datas=>{
          if(datas!=paidAmount && datas!=paidDate){
          cell2 = new PdfPCell(new Phrase(datas.substring(1, datas.length()-1), FONT_TITLE1));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          } else {
          cell2 = new PdfPCell(new Phrase(datas, FONT_TITLE1));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          }
          })
          })
          document.add(table1);

          })
          }
          if(refundValue.length > 0){
            document.add(new Paragraph("\n"));
            document.add(new Paragraph("Refunds",FONT_H2_TABLE))
          refundValue.foreach(row=>{
          var payMethod = (row \ "payMethod").as[String]
          var payMethodParagraph = new Paragraph(payMethod.split(' ').map(_.capitalize).mkString(" "))
          payMethodParagraph.setSpacingBefore(10);
          document.add(payMethodParagraph)
          var columnWidths:Array[Float] = Array(25,25,25,25);
          var table2: PdfPTable = new PdfPTable(columnWidths)
          table2.setWidthPercentage(100);

          var entriesToDeposits = (row \ "entries").as[List[JsValue]]

          var cell2 = new PdfPCell(new Phrase( "Paid Date", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Patient Name", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Bank", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          cell2 = new PdfPCell(new Phrase("Paid Amount", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          entriesToDeposits.foreach(entry=>{
          var paidDate = (entry \ "paidDate").as[JsValue].toString
          paidDate = fmt.print(LocalDate.parse(paidDate.substring(1, paidDate.length()-1)))
          var patientName = (entry \ "patientName").as[JsValue].toString
          var bank = (entry \ "bank").as[JsValue].toString
          var paidAmount = (entry \ "paidAmount").as[JsValue].toString
          paidAmount = formatter.format(paidAmount.toFloat).toString
          var data = List(paidDate,patientName,bank,paidAmount)
          data.foreach(datas=>{
          if(datas!=paidAmount && datas!=paidDate){
          cell2 = new PdfPCell(new Phrase(datas.substring(1, datas.length()-1), FONT_TITLE1));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          } else {
          cell2 = new PdfPCell(new Phrase(datas, FONT_TITLE1));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table2.addCell(cell2);
          }
          })
          })
          document.add(table2);

          })
          }
          var columnWidths1:Array[Float] = Array(50,50);
          var table3: PdfPTable = new PdfPTable(columnWidths1)
          table3.setWidthPercentage(50);
          table3.setHorizontalAlignment(Element.ALIGN_RIGHT);
          table3.setKeepTogether(true);
          if(insurancePatientValue.length > 0){
            document.add(new Paragraph("\n"));
          
          var cell3 = new PdfPCell(new Phrase( "Deposits", FONT_H3_TABLE));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          cell3 = new PdfPCell(new Phrase( " ", FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          insurancePatientValue.foreach(row=>{
          var entryTotal = (row \ "total").as[JsValue].toString
          var method = (row \ "payMethod").as[JsValue].toString
          cell3 = new PdfPCell(new Phrase( method.substring(1, method.length()-1).split(' ').map(_.capitalize).mkString(" ") + " " + "Total", FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          cell3 = new PdfPCell(new Phrase( formatter.format(entryTotal.toFloat), FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          })
          cell3 = new PdfPCell(new Phrase( "Total", FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          cell3 = new PdfPCell(new Phrase(formatter.format(depositTotal).toString, FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setPadding(5)
          table3.addCell(cell3);
          
          }
           if(refundValue.length > 0){
             document.add(new Paragraph("\n"));
          
          var cell4 = new PdfPCell(new Phrase( "Refunds", FONT_H3_TABLE));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          cell4 = new PdfPCell(new Phrase( " ", FONT_TITLE1));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          refundValue.foreach(row=>{
          var entryTotal = (row \ "total").as[JsValue].toString
          var method = (row \ "payMethod").as[JsValue].toString
          cell4 = new PdfPCell(new Phrase( method.substring(1, method.length()-1).split(' ').map(_.capitalize).mkString(" ") + " " + "Total", FONT_TITLE1));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          cell4 = new PdfPCell(new Phrase( formatter.format(entryTotal.toFloat), FONT_TITLE1));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          })
          cell4 = new PdfPCell(new Phrase( "Total", FONT_TITLE1));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          cell4 = new PdfPCell(new Phrase(formatter.format(refundTotal), FONT_TITLE1));
          cell4.setBorder(Rectangle.NO_BORDER);
          cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell4.setPadding(5)
          table3.addCell(cell4);
          
          }
          document.add(new Paragraph("\n"));
          
          var cell5 = new PdfPCell(new Phrase("Grand Total", FONT_H3_TABLE));
          cell5.setBorder(Rectangle.NO_BORDER);
          cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell5.setPadding(5)
          table3.addCell(cell5);
          cell5 = new PdfPCell(new Phrase(formatter.format(depositTotal - refundTotal), FONT_TITLE1));
          cell5.setBorder(Rectangle.NO_BORDER);
          cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell5.setPadding(5)
          table3.addCell(cell5);
          document.add(table3);
          document.close();

          })(ioBoundExecutor)

          inputStream
        }
        val filename = s"Deposit Slip"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
     }

 }

case class TreatmentPlanList(
  date: String,
  toothNumber: String,
  fee: Double,
  procedureCode: String,
  procedureDescription: String
)
implicit val TreatmentPlanRowWrites = Json.writes[TreatmentPlanList]

case class InsuranceList(
  name: String,
  renewalDate: String,
  familyAnnualMax: Float
  )

  implicit val InsuranceRowWrites = Json.writes[InsuranceList]

 def unScheduledTreatmentPlan(startDate:String, endDate:String, providerId:Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var vStartDate = LocalDate.parse(startDate)
 var vEndDate = LocalDate.parse(endDate)
 val obj = Json.obj()
 var unScheduledTreatmentPlanValue : List[JsValue] = List()
 var formatter: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");

 var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(s=> s.status === "Accepted" || s.status === "Proposed").filter(s => s.datePlanned >= vStartDate && s.datePlanned <= vEndDate )
    var vProviderId = convertOptionString(providerId)
    if(vProviderId.toLowerCase != "all" && vProviderId.toLowerCase != None && vProviderId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(vProviderId)))
      }
        var patientBlock = db.run {
        treatmentPlanTableQuery
        .joinLeft(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id)
        .joinLeft(AppointmentTreatmentPlanTable).on(_._1.id === _.treatmentPlanId)
        .sortBy(_._1._1.datePlanned.desc)
        .result
        } map { treatmentMap =>
        
        var data = treatmentMap.groupBy(_._1._1.patientId).map{ row =>
        var treatmentPlanDetails = row._2
    
        var patientName = ""
        var mergeList = List.empty[TreatmentPlanList]
        var mergeList1 = List.empty[InsuranceList]
        var familyAnnualList = new ListBuffer[Float]()
        var insuranceIdList = new ListBuffer[Long]()
        var renewelDateList = new ListBuffer[String]()
        var patient_id = convertOptionLong(row._1)
        var treatmentPlanTotal:Float = 0
        (row._2).foreach(r => {

        var treatmentPlan = r._1._1
        var appointment = r._2

        var treatmentSome = r._1._2
        if(treatmentSome != None && treatmentSome != ""){
          var treatment = treatmentSome.get
        
        patientName = convertOptionString(treatment.firstName) + " " + convertOptionString(treatment.lastName)
        
        if(appointment == None){
        var treatmentInsurance:Long = 0
        var treatmentRenewel = ""
        var tooth = ""
        // var dotEnd = treatmentPlan.procedureName.indexOf('.')
        // var procedureName = treatmentPlan.procedureName.substring(0,20) + "..."
        // if(dotEnd != (-1)){
        //   procedureName = treatmentPlan.procedureName.substring(0,dotEnd)
        // }
        var procedureName = treatmentPlan.procedureName
        if(treatmentPlan.procedureName.length > 20){
            procedureName = treatmentPlan.procedureName.substring(0,20) + "..."
        }
        // if(treatmentPlan.toothNumber != "" && treatmentPlan.toothNumber != None && treatmentPlan.toothNumber.forall(_.isDigit)){
        if(treatmentPlan.toothNumber != "" && treatmentPlan.toothNumber != None){
        tooth = treatmentPlan.toothNumber
        }

        val fees = treatmentPlan.fee
        val str = f"${fees}%.2f"
        val feeValue = str.toDouble
        var treatmentList = TreatmentPlanList(formatter.print(treatmentPlan.datePlanned),tooth, feeValue,treatmentPlan.cdcCode,procedureName)

        mergeList = (mergeList :+ treatmentList)
        familyAnnualList += convertOptionFloat(treatmentPlan.familyAnnualMax)

        if(treatmentPlan.renewalDate != None){
        treatmentRenewel = formatter.print(convertOptionalLocalDate(treatmentPlan.renewalDate))
        }
        renewelDateList += treatmentRenewel

        if(treatmentPlan.insurance != "" && treatmentPlan.insurance != None && treatmentPlan.insurance != null){
        treatmentInsurance = convertOptionLong(treatmentPlan.insurance)
        }
        insuranceIdList += treatmentInsurance

        if(treatmentPlan.fee != "" && treatmentPlan.fee != None && treatmentPlan.fee != null){
        treatmentPlanTotal = treatmentPlanTotal + treatmentPlan.fee
        }

      }
      }
      })
      if(mergeList.length != 0){
      var familAnnual = familyAnnualList.distinct  
      var insuranceList = new ListBuffer[Long]()
      var insuranceRepeat = true
      for (i <- 0 to insuranceIdList.length-1){
        if(insuranceList.contains(insuranceIdList(i))){
        insuranceRepeat = false
        } else{
        insuranceRepeat = true
        }
        if(insuranceIdList(i) != 0 && insuranceIdList(i) != (-1) && insuranceRepeat ){
        var block =  db.run{
          DentalCarrierTable.filter(_.id === insuranceIdList(i)).result
        } map { insuranceMap=>
        insuranceMap.map{insuranceRow=>
          insuranceList += insuranceIdList(i)
          var insuranceList1 = InsuranceList(insuranceRow.name,renewelDateList(i),familyAnnualList(i))

        mergeList1 = (mergeList1 :+ insuranceList1)
        }
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
        }
      }
      if(!(mergeList.isEmpty)){
        
      val newObj = obj + ("patientName" -> JsString(patientName)) + ("treatmentPlan" -> Json.toJson(mergeList)) + ("treatmentPlanTotal" -> JsNumber(new BigDecimal(treatmentPlanTotal).setScale(2, RoundingMode.HALF_UP))) + ("insurance" -> Json.toJson(mergeList1))
      unScheduledTreatmentPlanValue = unScheduledTreatmentPlanValue :+ newObj
      }
    }
    Ok(Json.toJson(unScheduledTreatmentPlanValue))
    }
    Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
 }

case class UnScheduledTreatmentPlanRow(
  patientName: String,
  treatmentValue: List[TreatmentPlanList],
  treatmentTotal: Float,
  insuranceValue: List[InsuranceList]
)
def unScheduledTreatmentPlanPDF(startDate:String, endDate:String, providerId:Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var vStartDate = LocalDate.parse(startDate)
 var vEndDate = LocalDate.parse(endDate)
 val obj = Json.obj()
//  var unScheduledTreatmentPlanValue : List[JsValue] = List()
 var unScheduledList = List.empty[UnScheduledTreatmentPlanRow]
 var formatter: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");

 var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(s=> s.status === "Accepted" || s.status === "Proposed").filter(s => s.datePlanned >= vStartDate && s.datePlanned <= vEndDate )
    var vProviderId = convertOptionString(providerId)
    if(vProviderId.toLowerCase != "all" && vProviderId.toLowerCase != None && vProviderId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(vProviderId)))
      }
        db.run {
        treatmentPlanTableQuery
        .joinLeft(TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId)).on(_.patientId === _.id)
        .joinLeft(AppointmentTreatmentPlanTable).on(_._1.id === _.treatmentPlanId)
        .sortBy(_._1._1.datePlanned.desc)
        .result
        } map { treatmentMap => (treatmentMap)

        var data = treatmentMap.groupBy(_._1._1.patientId).map{ row =>
        var treatmentPlanDetails = row._2

        var patientName = ""
        var mergeList = List.empty[TreatmentPlanList]
        var mergeList1 = List.empty[InsuranceList]
        var familyAnnualList = new ListBuffer[Float]()
        var insuranceIdList = new ListBuffer[Long]()
        var renewelDateList = new ListBuffer[String]()
        var patient_id = convertOptionLong(row._1)
        var treatmentPlanTotal:Float = 0
        (row._2).foreach(r => {

        var treatmentPlan = r._1._1
        var appointment = r._2

        var treatmentSome = r._1._2
        if(treatmentSome != None && treatmentSome != ""){
          var treatment = treatmentSome.get

        patientName = convertOptionString(treatment.firstName) + " " + convertOptionString(treatment.lastName)

        if(appointment == None){
        var treatmentInsurance:Long = 0
        var treatmentRenewel = ""
        var tooth  = ""
        // var dotEnd = treatmentPlan.procedureName.indexOf('.')
        // var procedureName = treatmentPlan.procedureName.substring(0,20) + "..."
        // if(dotEnd != (-1)){
        //   procedureName = treatmentPlan.procedureName.substring(0,dotEnd)
        // }

        var procedureName = treatmentPlan.procedureName
        if(procedureName.length > 20){
            procedureName = procedureName.substring(0,20) + "..."
        }

        if(treatmentPlan.toothNumber != "" && treatmentPlan.toothNumber != None){
        tooth = treatmentPlan.toothNumber
        }
        val fees = treatmentPlan.fee
        val str = f"${fees}%.2f"
        val feeValue = str.toDouble
        var treatmentList = TreatmentPlanList(formatter.print(treatmentPlan.datePlanned),tooth, feeValue,treatmentPlan.cdcCode,procedureName)

        mergeList = (mergeList :+ treatmentList)
        familyAnnualList += convertOptionFloat(treatmentPlan.familyAnnualMax)

        if(treatmentPlan.renewalDate != None){
        treatmentRenewel = formatter.print(convertOptionalLocalDate(treatmentPlan.renewalDate))
        }
        renewelDateList += treatmentRenewel

        if(treatmentPlan.insurance != "" && treatmentPlan.insurance != None && treatmentPlan.insurance != null){
        treatmentInsurance = convertOptionLong(treatmentPlan.insurance)
        }
        insuranceIdList += treatmentInsurance

        if(treatmentPlan.fee != "" && treatmentPlan.fee != None && treatmentPlan.fee != null){
        treatmentPlanTotal = treatmentPlanTotal + treatmentPlan.fee
        }

      }
      }
      })
      if(mergeList.length != 0){
      var familAnnual = familyAnnualList.distinct
        var insuranceList = new ListBuffer[Long]()
        var insuranceId1 = true
      for (i <- 0 to insuranceIdList.length-1){
        breakable{
        for (j <- 0 to insuranceList.length-1){
          if(insuranceList(j) == insuranceIdList(i)){
            insuranceId1 = false
            break
          } else{
            insuranceId1 = true
          }
        }
        }
          if(insuranceIdList(i) != 0 && insuranceIdList(i) != (-1) && insuranceId1 ){
           var block =  db.run{
              DentalCarrierTable.filter(_.id === insuranceIdList(i)).result
            } map { insuranceMap=>
            insuranceMap.map{insuranceRow=>
              insuranceList += insuranceIdList(i)
              var insuranceList1 = InsuranceList(insuranceRow.name,renewelDateList(i),familyAnnualList(i))

        mergeList1 = (mergeList1 :+ insuranceList1)
            }
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          }
        }
      }
      if(!(mergeList.isEmpty)){
        // var f = new BigDecimal(treatmentPlanTotal).setScale(2, RoundingMode.HALF_UP)
        var dataList = UnScheduledTreatmentPlanRow(patientName,mergeList,treatmentPlanTotal,mergeList1)
      unScheduledList = unScheduledList :+ dataList
      }
    }

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


          var cell = new PdfPCell(new Phrase("UnScheduled Treatment Plan", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var cell1 = new PdfPCell

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var  fromDate: Date = simpleDateFormat.parse(startDate);
          var  toDate: Date = simpleDateFormat.parse(endDate);

          if(vStartDate != vEndDate){
            cell1 = new PdfPCell(new Phrase("From Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(fromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);

            var cell2 = new PdfPCell(new Phrase("To Date     : "+new SimpleDateFormat("mm/dd/yyyy").format(toDate), FONT_TITLE1));
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell2);
          } else {
            cell1 = new PdfPCell(new Phrase("Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(fromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }

            document.add(headerTextTable1);
            document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;

          if(unScheduledList.length != 0){
            (unScheduledList).foreach(row => {

          var headerTextTable3: PdfPTable = new PdfPTable(1)
          headerTextTable3.setWidthPercentage(100);

            var cell3 = new PdfPCell(new Phrase("Patient Name : "+ row.patientName));
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable3.addCell(cell3);

            document.add(headerTextTable3);
            // document.add(new Paragraph("\n"));
            recNum =  0;
            (row.treatmentValue).foreach(treatmentRow => {
              var sampleData = List(s"${treatmentRow.date}", s"${treatmentRow.toothNumber}", s"${treatmentRow.procedureCode}", s"${treatmentRow.procedureDescription}", s"${treatmentRow.fee}")

              var i=1
              var columnWidths:Array[Float] = Array(20,10,20,30,20);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Tooth", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Code", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Description", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Amount", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

              }

              (sampleData).foreach(item => {
                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                cell.setPaddingLeft(5);
                cell.setPaddingTop(16);
                cell.setPaddingBottom(16);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
              })


              document.add(table);
              recNum = recNum+1;
              if(recNum == 0 ) {
                document.newPage();
              }
              })
          var headerTextTable4: PdfPTable = new PdfPTable(1)
          headerTextTable4.setWidthPercentage(70);

            var cell4 = new PdfPCell(new Phrase("Treatment Plan Total : "+ row.treatmentTotal));
            cell4.setBorder(Rectangle.NO_BORDER);
            cell4.setHorizontalAlignment(Element.ALIGN_RIGHT);
            headerTextTable4.addCell(cell4);

            document.add(headerTextTable4);

            var insRecNum =  0;
            (row.insuranceValue).foreach(insuranceRow => {
              var sampleData = List(s"${insuranceRow.name}", s"${insuranceRow.renewalDate}", s"${insuranceRow.familyAnnualMax}")

              var i=1
              var columnWidths:Array[Float] = Array(40,30,30);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(insRecNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Insurance Company Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Renewal Date", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Family Annual Max", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {
                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                cell.setPaddingLeft(5);
                cell.setPaddingTop(16);
                cell.setPaddingBottom(16);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
              })

              document.add(table);
              insRecNum = insRecNum+1;
              // if(insRecNum == 0 ) {
              //   println("CALLED ==> ")
              //   document.newPage();
              // }
              })

            })
          }
          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"Unscheduled treatment plan report"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
}

def treatmentToothChart(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var vFeeSchedule = 0f
    val toothChart = for {
      toothId <- (request.body \ "toothId").validateOpt[String]
      datePlanned <- (request.body \ "datePlanned").validate[LocalDate]
      cdtCode <- (request.body \ "cdtCode").validate[String]
      surface <- (request.body \ "surface").validateOpt[String]
      providerId <- (request.body \ "providerId").validate[Long]
      prognosis <- (request.body \ "prognosis").validateOpt[String]
      priority <- (request.body \ "priority").validateOpt[String]
      service <- (request.body \ "service").validate[String]
      status <- (request.body \ "status").validate[String]
      procedureName <- (request.body \ "procedureName").validateOpt[String]
      phase <- (request.body \ "phase").validateOpt[String]
    } yield {
        (toothId,datePlanned,cdtCode,surface,providerId,prognosis,priority,service,status,procedureName,phase)
    }

    toothChart match {
        case JsSuccess((toothId,datePlanned,cdtCode,surface,providerId,prognosis,priority,service,status,procedureName,phase), _) =>
        var insuranceId = 0L
        var a = db.run{
            StepTable.filter(_.treatmentId === patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
          } map { stepList =>
            insuranceId =  convertStringToLong((stepList.value \ "carrierName").asOpt[String])
            var b = db.run{
                InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === insuranceId).filter(_.procedureCode === cdtCode).result.head
              } map { insuranceList =>
                vFeeSchedule = insuranceList.fee
              }
              Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          }
          Await.ready(a, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        if(toothId != None){
        (toothId.get.split(",")).foreach(toothNumber => {
          // (cdtCode).foreach(cdt => {
           var b = db.run{
              TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(_.toothNumber === toothNumber).filter(_.cdcCode === cdtCode).filter(t => t.status === "Proposed" || t.status === "Accepted").length.result
            } map { treatmentCount =>

              if(treatmentCount == 0){
                var toothRow = TreatmentPlanDetailsRow(None,0,cdtCode,convertOptionString(procedureName),toothNumber,status,datePlanned,None,convertOptionString(surface),vFeeSchedule,0.0f,Some(providerId),Some(0.0f),None,Some(0.0f),None,"","",Some(patientId),Some(insuranceId),Some(""),Some(0.0f),None,Some(""),None,Some(0.0f),Some(0.0f),None,prognosis,priority,Some(service),phase)
                  db.run {
                    TreatmentPlanDetailsTable.returning(TreatmentPlanDetailsTable.map(_.id)) += toothRow
                    } map {
                          case 0L => PreconditionFailed
                          case id => Created(Json.toJson(toothRow.copy(id = Some(id))))
                        }
                  }
              }
              Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          // })
        })
        } else {
            var c = db.run{
              TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(_.cdcCode === cdtCode).filter(_.toothNumber === "").filter(t => t.status === "Proposed" || t.status === "Accepted").length.result
            } map { treatmentCount =>
              if(treatmentCount == 0){
                var toothRow = TreatmentPlanDetailsRow(None,0,cdtCode,convertOptionString(procedureName),"",status,datePlanned,None,convertOptionString(surface),vFeeSchedule,0.0f,Some(providerId),Some(0.0f),None,Some(0.0f),None,"","",Some(patientId),Some(insuranceId),Some(""),Some(0.0f),None,Some(""),None,Some(0.0f),Some(0.0f),None,prognosis,priority,Some(service),phase)
                  db.run {
                    TreatmentPlanDetailsTable.returning(TreatmentPlanDetailsTable.map(_.id)) += toothRow
                    } map {
                          case 0L => PreconditionFailed
                          case id => Created(Json.toJson(toothRow.copy(id = Some(id))))
                        }
                  }
              }
            Await.ready(c, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
          Future(Ok)
      case JsError(_) => Future.successful(BadRequest)
    }
  }

}