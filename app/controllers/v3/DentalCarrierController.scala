package controllers.v3
import controllers.NovadonticsController
import controllers.v1.PracticeController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffRow,DentalCarrierRow,InsuranceFeeRow}
import play.api.mvc.Result


class DentalCarrierController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application, var practiceController: PracticeController)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

implicit val DentalFormat = Json.format[DentalCarrierRow]
 
def readAll = silhouette.SecuredAction.async { request =>
var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
        DentalCarrierTable.filterNot(_.archived).filter(_.practiceId === practiceId).result
    } map { dentalCarieerList =>
      Ok(Json.toJson(dentalCarieerList))
    }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val dentalCarieer = for {

      name <- (request.body \ "name").validate[String]
      address <- (request.body \ "address").validate[String]
      city <- (request.body \ "city").validate[String]
      state <- (request.body \ "state").validate[String]
      zip <- (request.body \ "zip").validate[String]  
      phone <- (request.body \ "phone").validateOpt[String]
      phone2 <- (request.body \ "phone2").validateOpt[String]
      payerId <- (request.body \ "payerId").validateOpt[String]
      claimType <- (request.body \ "claimType").validateOpt[String]
      website <- (request.body \ "website").validateOpt[String]
      contactPerson <- (request.body \ "contactPerson").validateOpt[String]
      insRefNumber <- (request.body \ "insRefNumber").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      feeSchedule <- (request.body \ "feeSchedule").validateOpt[String]

    } yield DentalCarrierRow(None, practiceId, name,address,city,state,zip,convertOptionString(phone),convertOptionString(phone2),convertOptionString(payerId),convertOptionString(claimType),convertOptionString(website),convertOptionString(contactPerson),convertOptionString(insRefNumber),convertOptionString(notes),convertOptionString(feeSchedule),false)

    dentalCarieer match {
      case JsSuccess(dentalCarrierRow, _) => 
          db.run {   
            DentalCarrierTable.returning(DentalCarrierTable.map(_.id)) += dentalCarrierRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(dentalCarrierRow.copy(id = Some(id))))
                }

      case JsError(_) => Future.successful(BadRequest)
    }

  }


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val dentalCarrier = for {
      name <- (request.body \ "name").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]  
      phone <- (request.body \ "phone").validateOpt[String]
      phone2 <- (request.body \ "phone2").validateOpt[String]
      payerId <- (request.body \ "payerId").validateOpt[String]
      claimType <- (request.body \ "claimType").validateOpt[String]
      website <- (request.body \ "website").validateOpt[String]
      contactPerson <- (request.body \ "contactPerson").validateOpt[String]
      insRefNumber <- (request.body \ "insRefNumber").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      feeSchedule <- (request.body \ "feeSchedule").validate[String]
    } yield {
      (name,address,city,state,zip,phone,phone2,payerId,claimType,website,contactPerson,insRefNumber,notes,feeSchedule)
    }

    dentalCarrier match {
      case JsSuccess((name,address,city,state,zip,phone,phone2,payerId,claimType,website,contactPerson,insRefNumber,notes,feeSchedule), _) => db.run {
        val query = DentalCarrierTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            address.map(value => query.map(_.address).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            city.map(value => query.map(_.city).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            state.map(value => query.map(_.state).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            zip.map(value => query.map(_.zip).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            phone.map(value => query.map(_.phone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            phone2.map(value => query.map(_.phone2).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            payerId.map(value => query.map(_.payerId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            claimType.map(value => query.map(_.claimType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            website.map(value => query.map(_.website).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            contactPerson.map(value => query.map(_.contactPerson).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            insRefNumber.map(value => query.map(_.insRefNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            notes.map(value => query.map(_.notes).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(feeSchedule).map(value => query.map(_.feeSchedule).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))


          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def delete(id: Long) = silhouette.SecuredAction.async { request =>
       db.run {
         DentalCarrierTable.filter(_.id === id).map(_.archived).update(true)
        } map {
        case 0 => NotFound
        case _ => Ok
        }
  }

def migrateFeeForPractice(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
      for{
          practice <- PracticeTable.filterNot(_.archived).filter(_.id === id).result
          defaultFee <- DefaultFeeTable.result
      } yield {
            (practice).foreach( practiceList => {
              (defaultFee).foreach( defaultFeeList => {
               var block = db.run{
                  InsuranceFeeTable.filter(_.practiceId === practiceList.id).filter(_.procedureCode === defaultFeeList.procedureCode).sortBy(_.id).result
                } map { insuranceFee =>
                    if(insuranceFee.length > 0){
                        (insuranceFee).foreach(insuranceFeeList => {
                          var insFee = insuranceFeeList.fee
                          if(insuranceFeeList.fee == 0.0 && insuranceFeeList.insuranceId == 0){
                            insFee = defaultFeeList.privateFee
                          } else if(insuranceFeeList.fee == 0.0 && insuranceFeeList.insuranceId == -1){
                            insFee = defaultFeeList.ucrFee
                          }
                         var b = db.run{
                            InsuranceFeeTable.filter(_.id === insuranceFeeList.id).map(_.fee).update(insFee)
                          }
                          Await.ready(b, atMost = scala.concurrent.duration.Duration(120, SECONDS))
                        })
                    } else {
                       var ucrId = -1
                       var privateId = 0
                       practiceController.defaultInsFeeCreation(InsuranceFeeRow(None, convertOptionLong(practiceList.id), privateId, defaultFeeList.procedureCode, defaultFeeList.privateFee))
                       practiceController.defaultInsFeeCreation(InsuranceFeeRow(None, convertOptionLong(practiceList.id), ucrId, defaultFeeList.procedureCode, defaultFeeList.ucrFee))
                     }
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(120, SECONDS))
            })
        })
        Ok { Json.obj("status"->"success","message"->"Fee Migrated successfully") }
      }
    }
  }

def setDefaultCarrier = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
      for{
          treatments <- TreatmentTable.filterNot(_.deleted).result
      } yield {
            (treatments).foreach( treatmentList => {
              db.run{
                StepTable.filter(_.treatmentId === treatmentList.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepList =>
                var carrierName = convertOptionString((stepList.value \ "carrierName").asOpt[String])
                if(carrierName == None || carrierName == ""){
                var value = stepList.value.as[JsObject] - "carrierName"
                var newObj =  value + ("carrierName" -> Json.toJson("-1"))
                var b = db.run{
                  StepTable.filter(_.treatmentId === stepList.treatmentId).filter(_.formId === stepList.formId).filter(_.dateCreated === stepList.dateCreated).map(_.value).update(newObj)
                }
                Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                }

              }
        })
        Ok { Json.obj("status"->"success","message"->"Carrier name migrated successfully") }
      }
    }
  }
}


