
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{StaffRow,SaveLaterRow,CartRow}
import play.api.mvc.Result


class SaveLaterController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._


  implicit val SaveLaterFormat = Json.format[SaveLaterRow]
 
  def readAll = silhouette.SecuredAction.async { request =>
    db.run {
        SaveLaterTable.filter(_.staffId === request.identity.id).result
    } map { saveLaterList =>
      Ok(Json.toJson(saveLaterList))
    }
  }


 def create = silhouette.SecuredAction.async(parse.json) { request =>
    var staffId = convertOptionLong(request.identity.id)
    var isProductExist = false

    val saveLater = for {
      productId <- (request.body \ "productId").validate[Long]
    } yield {

      var block = db.run{
        SaveLaterTable.filter(_.staffId === staffId).filter(_.productId === productId).result
      } map { saveList => 
        if(saveList.length > 0){
          isProductExist = true
        }
      }
      var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      
      SaveLaterRow(None, staffId, productId)
    }

    saveLater match {
      case JsSuccess(saveLaterRow, _) => 

     var block = db.run {
        CartTable.filter(_.productId === saveLaterRow.productId).delete map {
        case 0 => NotFound
        case _ => Ok
        }
     }
    var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(isProductExist) {
              val obj = Json.obj()
              val newObj = obj + ("Result" -> JsString("Success"))
              Future(Ok{Json.toJson(Array(newObj))})
      } else {
          db.run {   
            SaveLaterTable.returning(SaveLaterTable.map(_.id)) += saveLaterRow
             } map {
                  case 0L => PreconditionFailed
                  case id => //Created(Json.toJson(saveLaterRow.copy(id = Some(id))))
                  val obj = Json.obj()
                  val newObj = obj + ("Result" -> JsString("Success"))
                  Ok{Json.toJson(Array(newObj))}
                }
      }
         
      case JsError(_) => Future.successful(BadRequest)
    }

  }

 def moveToCart(productId: Long) = silhouette.SecuredAction.async { request =>
 var staffId = request.identity.id
 var plusQuantity: Long = 1
    db.run {
        CartTable.filter(_.userId === staffId).filter(_.productId === productId).result
    } map { cartMap =>

            var block = db.run {
              SaveLaterTable.filter(_.productId === productId).delete
              } map {
              case 0 => NotFound
              case _ => Ok
              }
              var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          if(cartMap.length > 0){
            cartMap.map{cartList =>
            var quantity = Some(convertOptionLong(cartList.quantity) + plusQuantity)
              db.run {
                     CartTable.filter(_.id === cartList.id).map(_.quantity).update(quantity)
                      } map {
                      case 0 => NotFound
                      case _ => Ok
                    }

            }
          } else {
            var cartRow = CartRow(None, request.identity.id, Some(productId), Some(plusQuantity))
            db.run{
              CartTable.returning(CartTable.map(_.id)) += cartRow
                } map {
                      case 0L => PreconditionFailed
                     case id => Ok
            }

          }
      Ok
    }
  }

def delete(productId: Long) = silhouette.SecuredAction.async { request =>
       db.run {
         SaveLaterTable.filter(_.productId === productId).delete
        } map {
        case 0 => NotFound
        case _ => Ok
        }
  }
}


