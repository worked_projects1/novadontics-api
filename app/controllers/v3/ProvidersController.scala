
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{ProviderTableRow, StaffRow,ProviderEducationRow,ProviderAwardsRow,ProviderExperienceRow}
import play.api.mvc.Result
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._


class ProvidersController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val educationFormat = Json.format[ProviderEducationRow]
  implicit val awardsFormat = Json.format[ProviderAwardsRow]
  implicit val experienceFormat = Json.format[ProviderExperienceRow]

 implicit val providerRowWrites = Writes { request: (ProviderTableRow) =>
  val (providerRow) = request
  val name = providerRow.title+" "+ providerRow.firstName+" "+ providerRow.lastName
  var providerEducation: JsValue = Json.parse("[]")
  var providerAwards: JsValue = Json.parse("[]")
  var providerExperience: JsValue = Json.parse("[]")

  var block1 = db.run{ ProviderEducationTable.filter(_.providerId === providerRow.id).result } map { providerVector =>
    providerEducation = Json.toJson(providerVector) }
    Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  var block2 = db.run{ ProviderAwardsTable.filter(_.providerId === providerRow.id).result } map { providerVector =>
    providerAwards = Json.toJson(providerVector) }
    Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  var block3 = db.run{ ProviderExperienceTable.filter(_.providerId === providerRow.id).result } map { providerVector =>
    providerExperience = Json.toJson(providerVector) }
    Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  Json.obj(
      "id" -> providerRow.id,
      "practiceId" -> providerRow.practiceId,
      "title" -> providerRow.title,
      "firstName" -> providerRow.firstName,
      "lastName" -> providerRow.lastName,
      "role" -> providerRow.role,
      // "name" -> name.split(' ').map(_.capitalize).mkString(" "),
      "name" -> name,
      "color" -> providerRow.color,
      "licenseStateAndNumber" -> providerRow.licenseStateAndNumber,
      "licenseExpDate" -> providerRow.licenseExpDate,
      "malPracticeIns" -> providerRow.malPracticeIns,
      "malPracticeInsExpDate" -> providerRow.malPracticeInsExpDate,
      "deaNumber" -> providerRow.deaNumber,
      "deaExpDate" -> providerRow.deaExpDate,
      "npiNumber" -> providerRow.npiNumber,
      "emailAddress" -> providerRow.emailAddress,
      "phoneNumber" -> providerRow.phoneNumber,
      "ocscNumber" -> providerRow.ocscNumber,
      "ocscExpDate" -> providerRow.ocscExpDate,
      "hygienistLicenseNumber" -> providerRow.hygienistLicenseNumber,
      "hygienistLicenseExpDate" -> providerRow.hygienistLicenseExpDate,
      "gender" -> providerRow.gender,
      "bio" -> providerRow.bio,
      "profileImage" -> providerRow.profileImage,
      "services" -> providerRow.services,
      "languages" -> providerRow.languages,
      "dob" -> providerRow.dob,
      "education" -> providerEducation,
      "awards" -> providerAwards,
      "experience" -> providerExperience
      )
  }

 
   def readAll(practiceId :Option[Long]) = silhouette.SecuredAction.async { request =>
    var strPracticeId:Long = 0
    if(practiceId != None){
      strPracticeId = convertOptionLong(practiceId)
    }else{
      strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
    }  
     db.run {
          ProviderTable.filterNot(_.deleted).filter(_.practiceId === strPracticeId).sortBy(r => (r.firstName, r.lastName)).result
      } map { providersList =>
        Ok(Json.toJson(providersList))
      }
  }

 def create = silhouette.SecuredAction.async(parse.json) { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val providers = for {

      title <- (request.body \ "title").validate[String]
      firstName <- (request.body \ "firstName").validate[String]
      lastName <- (request.body \ "lastName").validate[String]
      role <- (request.body \ "role").validate[String]
      color <- (request.body \ "color").validateOpt[String]
      licenseStateAndNumber <- (request.body \ "licenseStateAndNumber").validateOpt[String]
      licenseExpDate <- (request.body \ "licenseExpDate").validateOpt[String]
      malPracticeIns <- (request.body \ "malPracticeIns").validateOpt[String]
      malPracticeInsExpDate <- (request.body \ "malPracticeInsExpDate").validateOpt[String]
      deaNumber <- (request.body \ "deaNumber").validateOpt[String]
      deaExpDate <- (request.body \ "deaExpDate").validateOpt[String]
      npiNumber <- (request.body \ "npiNumber").validateOpt[String]
      emailAddress <- (request.body \ "emailAddress").validateOpt[String]
      phoneNumber <- (request.body \ "phoneNumber").validateOpt[String]
      ocscNumber <- (request.body \ "ocscNumber").validateOpt[String]
      ocscExpDate <- (request.body \ "ocscExpDate").validateOpt[String]
      hygienistLicenseNumber <- (request.body \ "hygienistLicenseNumber").validateOpt[String]
      hygienistLicenseExpDate <- (request.body \ "hygienistLicenseExpDate").validateOpt[String]
      gender <- (request.body \ "gender").validateOpt[String]
      bio <- (request.body \ "bio").validateOpt[String]
      profileImage <- (request.body \ "profileImage").validateOpt[String]
      services <- (request.body \ "services").validateOpt[String]
      languages <- (request.body \ "languages").validateOpt[String]
      dob <- (request.body \ "dob").validate[LocalDate]
      education <- (request.body \ "education").validateOpt[JsValue]
      experience <- (request.body \ "experience").validateOpt[JsValue]
      awards <- (request.body \ "awards").validateOpt[JsValue]

    } yield {
      var vLicenseExpDate: Option[LocalDate] = None
      if(licenseExpDate != None && licenseExpDate != Some("")){
        vLicenseExpDate = Some(LocalDate.parse(convertOptionString(licenseExpDate)))
      }
      var vMalPracticeInsExpDate: Option[LocalDate] = None
      if(malPracticeInsExpDate != None && malPracticeInsExpDate != Some("")){
        vMalPracticeInsExpDate = Some(LocalDate.parse(convertOptionString(malPracticeInsExpDate)))
      }
      var vDeaExpDate: Option[LocalDate] = None
      if(deaExpDate != None && deaExpDate != Some("")){
        vDeaExpDate = Some(LocalDate.parse(convertOptionString(deaExpDate)))
      }
      var vOcscExpDate: Option[LocalDate] = None
      if(ocscExpDate != None && ocscExpDate != Some("")){
        vOcscExpDate = Some(LocalDate.parse(convertOptionString(ocscExpDate)))
      }
      var vHygienistLicenseExpDate: Option[LocalDate] = None
      if(hygienistLicenseExpDate != None && hygienistLicenseExpDate != Some("")){
        vHygienistLicenseExpDate = Some(LocalDate.parse(convertOptionString(hygienistLicenseExpDate)))
      }
      var vColor = "#ffdfd9"
      if(color != None && color != Some("")){
        vColor = convertOptionString(color)
      }
    (ProviderTableRow(None, practiceId, title, firstName, lastName, role, false, vColor, convertOptionString(licenseStateAndNumber),vLicenseExpDate,convertOptionString(malPracticeIns),vMalPracticeInsExpDate,convertOptionString(deaNumber),vDeaExpDate,convertOptionString(npiNumber),convertOptionString(emailAddress),convertOptionString(phoneNumber),ocscNumber,vOcscExpDate,hygienistLicenseNumber,vHygienistLicenseExpDate,gender,bio,profileImage,services,languages,Some(dob)),education,experience,awards)
    }

    providers match {
      case JsSuccess((providerRow,education,experience,awards), _) =>
      db.run{
       ProviderTable.returning(ProviderTable.map(_.id)) += providerRow
        } map {
              case 0L => PreconditionFailed
              case id =>
                         providerEducationProcess(education,id,"create")
                         providerAwardsProcess(awards,id,"create")
                         providerExperienceProcess(experience,id,"create")
                         Created(Json.toJson(providerRow.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }

  }

def providerEducationProcess(providerEducation: Option[JsValue], providerId: Long, action: String){
  if(providerEducation != None && !providerEducation.isInstanceOf[JsUndefined]){
      val jsonList = providerEducation.get.as[List[JsValue]]
      if(action == "update"){
            db.run {
              ProviderEducationTable.filter(_.providerId === providerId).delete map{
              case 0 => NotFound
              case _ => Ok
            }
        }
      }
      jsonList.foreach(json => {
        // val educationProId = convertOptionLong((json \"providerId").asOpt[Long])
        val degree = convertOptionString((json \"degree").asOpt[String])
        val college = convertOptionString((json \"college").asOpt[String])
        val year = convertOptionLong((json \"educationYear").asOpt[Long])

        var b = db.run{
          ProviderEducationTable.returning(ProviderEducationTable.map(_.providerId)) += ProviderEducationRow(providerId,degree,college,year)
          }
          Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      })
  }
}

def providerAwardsProcess(providerAwards: Option[JsValue], providerId: Long, action: String){
  if(providerAwards != None && !providerAwards.isInstanceOf[JsUndefined]){
      val jsonList = providerAwards.get.as[List[JsValue]]
      if(action == "update"){
            db.run {
              ProviderAwardsTable.filter(_.providerId === providerId).delete map{
              case 0 => NotFound
              case _ => Ok
            }
        }
      }
      jsonList.foreach(json => {
        // val awardsProId = convertOptionLong((json \"providerId").asOpt[Long])
        val name = convertOptionString((json \"name").asOpt[String])
        val year = convertOptionLong((json \"awardsYear").asOpt[Long])

        var b = db.run{
          ProviderAwardsTable.returning(ProviderAwardsTable.map(_.providerId)) += ProviderAwardsRow(providerId,name,year)
          }
          Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      })
  }
}


def providerExperienceProcess(providerExperience: Option[JsValue], providerId: Long, action: String){
  if(providerExperience != None && !providerExperience.isInstanceOf[JsUndefined]){
      val jsonList = providerExperience.get.as[List[JsValue]]
      if(action == "update"){
            db.run {
              ProviderExperienceTable.filter(_.providerId === providerId).delete map{
              case 0 => NotFound
              case _ => Ok
            }
        }
      }
      jsonList.foreach(json => {
        // val experienceProId = convertOptionLong((json \"providerId").asOpt[Long])
        val place = convertOptionString((json \"place").asOpt[String])
        val from = convertOptionString((json \"from").asOpt[String])
        val to = convertOptionString((json \"to").asOpt[String])

        var b = db.run{
          ProviderExperienceTable.returning(ProviderExperienceTable.map(_.providerId)) += ProviderExperienceRow(providerId,place,from,to)
          }
          Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      })
  }
}


def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val providers = for {
      // practiceId <- (request.body \ "practiceId").validateOpt[Long]
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
      role <- (request.body \ "role").validateOpt[String]
      color <- (request.body \ "color").validateOpt[String]
      licenseStateAndNumber <- (request.body \ "licenseStateAndNumber").validateOpt[String]
      licenseExpDate <- (request.body \ "licenseExpDate").validateOpt[String]
      malPracticeIns <- (request.body \ "malPracticeIns").validateOpt[String]
      malPracticeInsExpDate <- (request.body \ "malPracticeInsExpDate").validateOpt[String]
      deaNumber <- (request.body \ "deaNumber").validateOpt[String]
      deaExpDate <- (request.body \ "deaExpDate").validateOpt[String]
      npiNumber <- (request.body \ "npiNumber").validateOpt[String]
      emailAddress <- (request.body \ "emailAddress").validateOpt[String]
      phoneNumber <- (request.body \ "phoneNumber").validateOpt[String]
      ocscNumber <- (request.body \ "ocscNumber").validateOpt[String]
      ocscExpDate <- (request.body \ "ocscExpDate").validateOpt[String]
      hygienistLicenseNumber <- (request.body \ "hygienistLicenseNumber").validateOpt[String]
      hygienistLicenseExpDate <- (request.body \ "hygienistLicenseExpDate").validateOpt[String]
      gender <- (request.body \ "gender").validateOpt[String]
      bio <- (request.body \ "bio").validateOpt[String]
      profileImage <- (request.body \ "profileImage").validateOpt[String]
      services <- (request.body \ "services").validateOpt[String]
      languages <- (request.body \ "languages").validateOpt[String]
      dob <- (request.body \ "dob").validateOpt[LocalDate]
      education <- (request.body \ "education").validateOpt[JsValue]
      experience <- (request.body \ "experience").validateOpt[JsValue]
      awards <- (request.body \ "awards").validateOpt[JsValue]

    } yield {

      var vLicenseExpDate: Option[LocalDate] = None
      if(licenseExpDate != None && licenseExpDate != Some("")){
        vLicenseExpDate = Some(LocalDate.parse(convertOptionString(licenseExpDate)))
      }
      var vMalPracticeInsExpDate: Option[LocalDate] = None
      if(malPracticeInsExpDate != None && malPracticeInsExpDate != Some("")){
        vMalPracticeInsExpDate = Some(LocalDate.parse(convertOptionString(malPracticeInsExpDate)))
      }
      var vDeaExpDate: Option[LocalDate] = None
      if(deaExpDate != None && deaExpDate != Some("")){
        vDeaExpDate = Some(LocalDate.parse(convertOptionString(deaExpDate)))
      }
      var vOcscExpDate: Option[LocalDate] = None
      if(ocscExpDate != None && ocscExpDate != Some("")){
        vOcscExpDate = Some(LocalDate.parse(convertOptionString(ocscExpDate)))
      }
      var vHygienistLicenseExpDate: Option[LocalDate] = None
      if(hygienistLicenseExpDate != None && hygienistLicenseExpDate != Some("")){
        vHygienistLicenseExpDate = Some(LocalDate.parse(convertOptionString(hygienistLicenseExpDate)))
      }

      (ProviderTableRow(None,0L,convertOptionString(title),convertOptionString(firstName),convertOptionString(lastName),convertOptionString(role),false,convertOptionString(color),convertOptionString(licenseStateAndNumber),vLicenseExpDate,convertOptionString(malPracticeIns),vMalPracticeInsExpDate,convertOptionString(deaNumber),vDeaExpDate,convertOptionString(npiNumber),convertOptionString(emailAddress),convertOptionString(phoneNumber),ocscNumber,vOcscExpDate,hygienistLicenseNumber,vHygienistLicenseExpDate,gender,bio,profileImage,services,languages,dob),education,experience,awards)
    }

    providers match {
      case JsSuccess((providerRow,education,experience,awards), _) => db.run {

        val query = ProviderTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            // providerRow.practiceId.map(value => query.map(_.practiceId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.title).map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.firstName).map(value => query.map(_.firstName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.lastName).map(value => query.map(_.lastName).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.role).map(value => query.map(_.role).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.color).map(value => query.map(_.color).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.licenseStateAndNumber).map(value => query.map(_.licenseStateAndNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.licenseExpDate).map(value => query.map(_.licenseExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.malPracticeIns).map(value => query.map(_.malPracticeIns).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.malPracticeInsExpDate).map(value => query.map(_.malPracticeInsExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.deaNumber).map(value => query.map(_.deaNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.deaExpDate).map(value => query.map(_.deaExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.npiNumber).map(value => query.map(_.npiNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.emailAddress).map(value => query.map(_.emailAddress).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.phoneNumber).map(value => query.map(_.phoneNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.ocscNumber).map(value => query.map(_.ocscNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.ocscExpDate).map(value => query.map(_.ocscExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.hygienistLicenseNumber).map(value => query.map(_.hygienistLicenseNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.hygienistLicenseExpDate).map(value => query.map(_.hygienistLicenseExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.gender).map(value => query.map(_.gender).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.bio).map(value => query.map(_.bio).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.profileImage).map(value => query.map(_.profileImage).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.services).map(value => query.map(_.services).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.languages).map(value => query.map(_.languages).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(providerRow.dob).map(value => query.map(_.dob).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ =>
                     providerEducationProcess(education,id,"update")
                     providerAwardsProcess(awards,id,"update")
                     providerExperienceProcess(experience,id,"update")
                     Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    } 
  }

def delete(id: Long) = silhouette.SecuredAction.async { request =>
    val currentDate: LocalDate = LocalDate.now()
    var providersValue: Boolean = false
    val count = db.run{
       for {
            providersLength <- AppointmentTable.filterNot(_.deleted).filterNot(s => s.status.toLowerCase === "canceled" || s.status.toLowerCase === "no show")
            .filter(_.providerId === id).filter(_.date >= currentDate).length.result
       } yield {
            if(providersLength > 0){
              providersValue = true
            }
       }
    }
    var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

      if(providersValue){
              val obj = Json.obj()
              val newObj = obj + ("message" -> JsString("Appointment scheduled for this provider!"))
              Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else{
      db.run {
          ProviderTable.filter(_.id === id).map(_.deleted).update(true)
      } map {
      case 0 => NotFound
      case _ => Ok
    }
  }
}
}


