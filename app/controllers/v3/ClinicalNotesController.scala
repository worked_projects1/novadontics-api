package controllers.v3

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.{ClinicalNotesRow,MedicationStabilityRow,MedicationEntryRow,StaffRow,ImplantDataRow,StepRow,BoneGraftingDataRow}
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result
import java.text.SimpleDateFormat
import scala.concurrent.{ExecutionContext,Await, Future}
import scala.concurrent.duration._
import scala.util.control._
import play.api.mvc.Action
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};
import java.util.Date;

//pdf
import akka.stream.scaladsl.StreamConverters
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import play.api.http.HttpEntity
import play.api.{Application, Logger}
import java.awt.geom.AffineTransform
import play.api.mvc.{ResponseHeader, Result}

import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.ExecutionContext.Implicits.global
import com.itextpdf.text.{List => _, _}
import com.itextpdf.text.Paragraph
import scala.collection.mutable.ListBuffer
import com.itextpdf.text.Chunk
import java.io.{ByteArrayInputStream, DataInputStream}
import com.amazonaws.services.s3.model.{ ObjectMetadata, S3ObjectInputStream, ListObjectsRequest, GetObjectRequest }
import scalaj.http.{Http, HttpOptions}
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.util.Random
import java.security.MessageDigest
import java.util.Base64
import java.net.URLEncoder

import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.name.Named
import com.itextpdf.text.pdf.{ColumnText, PdfPCell, PdfPageEventHelper, PdfWriter}
import com.itextpdf.text.pdf.PdfPTable
import java.net.URL

import scala.util.parsing.json.JSON._
import scala.util.parsing.json._
import com.itextpdf.text.{BaseColor, Document, Element, Font, FontFactory, Phrase, Rectangle}
/**
 * Created by stefan on 5/15/17.
 */

@Singleton
class ClinicalNotesController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext , val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._
  private val logger = Logger(this.getClass)

   implicit val BoneGraftingDataRowWrite = Writes { boneGraftingData: BoneGraftingDataRow =>
    Json.obj(
      "id" -> boneGraftingData.id,
      "practiceId" -> boneGraftingData.practiceId,
      "staffId" -> boneGraftingData.staffId,
      "providerName" -> boneGraftingData.providerName,
      "patientName" -> boneGraftingData.patientName,
      "date" -> boneGraftingData.date,
      "dob" -> boneGraftingData.dob,
      "allograftMaterialUsed" -> boneGraftingData.allograftMaterialUsed,
      "allograftSize" -> boneGraftingData.allograftSize,
      "allograftVolume" -> boneGraftingData.allograftVolume,
      "allograftBrand" -> boneGraftingData.allograftBrand,
      "xenograftMaterialUsed" -> boneGraftingData.xenograftMaterialUsed,
      "xenograftSize" -> boneGraftingData.xenograftSize,
      "xenograftVolume" -> boneGraftingData.xenograftVolume,
      "xenograftBrand" -> boneGraftingData.xenograftBrand,
      "alloplastMaterialUsed" -> boneGraftingData.alloplastMaterialUsed,
      "alloplastSize" -> boneGraftingData.alloplastSize,
      "alloplastVolume" -> boneGraftingData.alloplastVolume,
      "alloplastBrand" -> boneGraftingData.alloplastBrand,
      "autograftMaterialUsed" -> boneGraftingData.autograftMaterialUsed,
      "autograftSize" -> boneGraftingData.autograftSize,
      "autograftVolume" -> boneGraftingData.autograftVolume,
      "membraneUsed" -> boneGraftingData.membraneUsed,
      "membraneSize" -> boneGraftingData.membraneSize,
      "membraneBrand" -> boneGraftingData.membraneBrand,
      "additives" -> boneGraftingData.additives,
      "additivesVolume" -> boneGraftingData.additivesVolume,
      "additivesBrand" -> boneGraftingData.additivesBrand,
      "finalPA" -> boneGraftingData.finalPA,
      "rrpsd" -> boneGraftingData.rrpsd,
      "failureDate" -> boneGraftingData.failureDate
    )
    }

implicit val ImplantRowWrites = Writes { implantRecord: (ImplantDataRow) =>
    val (implantRow) = implantRecord
    Json.obj("id" -> implantRow.id,
    "implantSite" -> implantRow.implantSite,
    "implantBrand" -> implantRow.implantBrand,
    "implantSurface" -> implantRow.implantSurface,
    "implantReference" -> implantRow.implantReference,
    "implantLot" -> implantRow.implantLot,
    "diameter" -> implantRow.diameter,
    "length" -> implantRow.length,
    "surgProtocol" -> implantRow.surgProtocol,
    "boneDensity" -> implantRow.boneDensity,
    "osteotomySite" -> implantRow.osteotomySite,
    "ncm" -> implantRow.ncm,
    "isqBl" -> implantRow.isqBl,
    "isqMd" -> implantRow.isqMd,
    "implantDepth" -> implantRow.implantDepth,
    "loadingProtocol" -> implantRow.loadingProtocol,
    "dateOfRemoval" -> implantRow.dateOfRemoval,
    "reasonForRemoval" -> implantRow.reasonForRemoval,
    "finalPA" -> implantRow.finalPA,
    "recommendedHT" -> implantRow.recommendedHT,
    "implantRestored" -> implantRow.implantRestored,
    "patientName" -> implantRow.patientName,
    "provider" -> implantRow.provider,
    "date" -> implantRow.date,
    "dob" -> implantRow.dob)
}


implicit val clinicalNotesRowWrites = Writes { clinicalNotes: ClinicalNotesRow =>
  var implantList: JsValue = Json.parse("[]")
  var boneGraftList: JsValue = Json.parse("[]")
  var providerName = ""
  val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
  val outputTimeFormat = new SimpleDateFormat("hh:mm a")
  var time = ""
  var dischargeTime = ""

    if(clinicalNotes.time != None){
    val inputTime = inputTimeFormat.parse(convertOptionalLocalTime(clinicalNotes.time).toString)
    time = outputTimeFormat.format(inputTime)
    }

    if(clinicalNotes.dischargeTime != None){
    val inputTime = inputTimeFormat.parse(convertOptionalLocalTime(clinicalNotes.dischargeTime).toString)
    dischargeTime = outputTimeFormat.format(inputTime)
    }

    var block1 = db.run{
      ImplantDataTable.filter(_.noteId === clinicalNotes.id).result } map {
        implantVector => implantList = Json.toJson(implantVector)}
    Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    var block2 = db.run{
      BoneGraftingDataTable.filter(_.noteId === clinicalNotes.id).result } map {
        boneGraftVector => boneGraftList = Json.toJson(boneGraftVector)}
    Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    var block3 = db.run{
      ProviderTable.filter(_.id === clinicalNotes.provider).result.head } map {
        providerVector => providerName = providerVector.title + " " + providerVector.firstName +" "+providerVector.lastName }
    Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    Json.obj(
      "id" -> clinicalNotes.id,
      "staffId" -> clinicalNotes.staffId,
      "patientId" -> clinicalNotes.patientId,
      "note" -> clinicalNotes.note,
      "dateCreated" -> clinicalNotes.dateCreated,
      "date" -> clinicalNotes.date,
      "time" -> time,
      "provider" -> clinicalNotes.provider,
      "providerName" -> providerName,
      "lock" -> clinicalNotes.lock,
      "assistant1" -> clinicalNotes.assistant1,
      "assistant2" -> clinicalNotes.assistant2,
      "procedureCode" -> clinicalNotes.procedureCode,
      "procedureDescription" -> clinicalNotes.procedureDescription,
      "bp" -> clinicalNotes.bp,
      "pulse" -> clinicalNotes.pulse,
      "spco2" -> clinicalNotes.spco2,
      "co2" -> clinicalNotes.co2,
      "ekg" -> clinicalNotes.ekg,
      "resp" -> clinicalNotes.resp,
      "temp" -> clinicalNotes.temp,
      "allergies" -> clinicalNotes.allergies,
      "medicalConditions" -> clinicalNotes.medicalConditions,
      "postSurgicalHistory" -> clinicalNotes.postSurgicalHistory,
      "monitors" -> clinicalNotes.monitors,
      "asaStatus" -> clinicalNotes.asaStatus,
      "airwayClass" -> clinicalNotes.airwayClass,
      "anesthesia" -> clinicalNotes.anesthesia,
      "diagnosis" -> clinicalNotes.diagnosis,
      "dischargeDate" -> clinicalNotes.dischargeDate,
      "dischargeTime" -> dischargeTime,
      "brCompleted" -> clinicalNotes.brCompleted,
      "brMidline" -> clinicalNotes.brMidline,
      "brLipPostion" -> clinicalNotes.brLipPostion,
      "brUpper" -> clinicalNotes.brUpper,
      "brLower" -> clinicalNotes.brLower,
      "impression" -> clinicalNotes.impression,
      "escort" -> clinicalNotes.escort,
      "lapsPhoto" -> clinicalNotes.lapsPhoto,
      "surgeryPhoto" -> clinicalNotes.surgeryPhoto,
      "balancedOcclusion" -> clinicalNotes.balancedOcclusion,
      "anteriorGuidance" -> clinicalNotes.anteriorGuidance,
      "canineGuidance" -> clinicalNotes.canineGuidance,
      "fullDenture" -> clinicalNotes.fullDenture,
      "existingDenture" -> clinicalNotes.existingDenture,
      "newDenture" -> clinicalNotes.newDenture,
      "pmmaProsthesis" -> clinicalNotes.pmmaProsthesis,
      "compositeProsthesis" -> clinicalNotes.compositeProsthesis,
      "prosthesesPhoto" -> clinicalNotes.prosthesesPhoto,
      "medicationEntry" -> clinicalNotes.medicationEntry,
      "medicationStability" -> clinicalNotes.medicationStability,
      "vitalsPhoto" -> clinicalNotes.vitalsPhoto,
      "preMedicationTotal" -> clinicalNotes.preMedicationTotal,
      "patientTolerate" -> clinicalNotes.patientTolerate,
      "suturingMaterials" -> clinicalNotes.suturingMaterials,
      "suturingTechniques" -> clinicalNotes.suturingTechniques,
      "reasonForNextAppointment" -> clinicalNotes.reasonForNextAppointment,
      "procedure" -> clinicalNotes.procedure,
      "incision" -> clinicalNotes.incision,
      "boneGraftMaterials" -> clinicalNotes.boneGraftMaterials,
      "membranesUsed" -> clinicalNotes.membranesUsed,
      "fixationSystemUsed" -> clinicalNotes.fixationSystemUsed,
      "assistant3" -> clinicalNotes.assistant3,
      "verbalPostopIns" -> clinicalNotes.verbalPostopIns,
      "writtenPostopIns" -> clinicalNotes.writtenPostopIns,
      "rxVisit" -> clinicalNotes.rxVisit,
      "incisionMandible" -> clinicalNotes.incisionMandible,
      "voiceMemo" -> clinicalNotes.voiceMemo,
      "implantData" -> implantList,
      "boneGraftingData" -> boneGraftList
    )
  }

  // implicit val medicationEntryRowWrites = Writes { medicationEntryRow: MedicationEntryRow =>
  //   Json.obj(
  //     "medication" -> medicationEntryRow.medication,
  //     "dosage" -> medicationEntryRow.dosage
  //   )
  // }

  // implicit val medicationStabilityRowWrites = Writes { medicationStabilityRow: MedicationStabilityRow =>
  //   Json.obj(
  //     "implant" -> medicationStabilityRow.implant,
  //     "implantBrand" -> medicationStabilityRow.implantBrand,
  //     "diameter" -> medicationStabilityRow.diameter,
  //     "boneDensity" -> medicationStabilityRow.boneDensity,
  //     "length" -> medicationStabilityRow.length,
  //     "ncm" -> medicationStabilityRow.ncm,
  //     "isqBl" -> medicationStabilityRow.isqBl,
  //     "isqMd" -> medicationStabilityRow.isqMd
  //   )
  // }

def readAll(patientId: Long) = silhouette.SecuredAction.async { request =>
  var implantList: JsValue = Json.parse("[]")
  var boneGraftList: JsValue = Json.parse("[]")
    db.run{
      ClinicalNotesTable.filter(_.patientId=== patientId).result
    } map { rows =>
      Ok(Json.toJson(rows.sortBy(t=> (t.date, t.time)).reverse))
    }
  }

  def create(patientId: Long) = silhouette.SecuredAction.async(parse.json) {  request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var staffId = request.identity.id.get
    val formatter = DateTimeFormat.forPattern("hh:mm a");

      val clinicalNotes = for {
        note <- (request.body \ "note").validateOpt[String]
        time <- (request.body \ "time").validate[String]
        date <- (request.body \ "date").validate[String]
        provider <- (request.body \ "provider").validate[Long]
        assistant1 <- (request.body \ "assistant1").validateOpt[String]
        assistant2 <- (request.body \ "assistant2").validateOpt[String]
        procedureCode <- (request.body \ "procedureCode").validateOpt[String]
        procedureDescription <- (request.body \ "procedureDescription").validateOpt[String]
        bp <- (request.body \ "bp").validateOpt[String]
        pulse <- (request.body \ "pulse").validateOpt[String]
        spco2 <- (request.body \ "spco2").validateOpt[String]
        co2 <- (request.body \ "co2").validateOpt[String]
        ekg <- (request.body \ "ekg").validateOpt[String]
        resp <- (request.body \ "resp").validateOpt[String]
        temp <- (request.body \ "temp").validateOpt[String]
        allergies <- (request.body \ "allergies").validateOpt[String]
        medicalConditions <- (request.body \ "medicalConditions").validateOpt[String]
        postSurgicalHistory <- (request.body \ "postSurgicalHistory").validateOpt[String]
        monitors <- (request.body \ "monitors").validateOpt[String]
        asaStatus <- (request.body \ "asaStatus").validateOpt[String]
        airwayClass <- (request.body \ "airwayClass").validateOpt[String]
        anesthesia <- (request.body \ "anesthesia").validateOpt[String]
        diagnosis <- (request.body \ "diagnosis").validateOpt[String]
        dischargeDate <- (request.body \ "dischargeDate").validateOpt[LocalDate]
        dischargeTime <- (request.body \ "dischargeTime").validateOpt[String]
        brCompleted <- (request.body \ "brCompleted").validateOpt[String]
        brMidline <- (request.body \ "brMidline").validateOpt[String]
        brLipPostion <- (request.body \ "brLipPostion").validateOpt[String]
        brUpper <- (request.body \ "brUpper").validateOpt[String]
        brLower <- (request.body \ "brLower").validateOpt[String]
        impression <- (request.body \ "impression").validateOpt[String]
        escort <- (request.body \ "escort").validateOpt[String]
        lapsPhoto <- (request.body \ "lapsPhoto").validateOpt[String]
        surgeryPhoto <- (request.body \ "surgeryPhoto").validateOpt[String]
        balancedOcclusion <- (request.body \ "balancedOcclusion").validateOpt[String]
        anteriorGuidance <- (request.body \ "anteriorGuidance").validateOpt[String]
        canineGuidance <- (request.body \ "canineGuidance").validateOpt[String]
        fullDenture <- (request.body \ "fullDenture").validateOpt[String]
        existingDenture <- (request.body \ "existingDenture").validateOpt[String]
        newDenture <- (request.body \ "newDenture").validateOpt[String]
        pmmaProsthesis <- (request.body \ "pmmaProsthesis").validateOpt[String]
        compositeProsthesis <- (request.body \ "compositeProsthesis").validateOpt[String]
        prosthesesPhoto <- (request.body \ "prosthesesPhoto").validateOpt[String]
        medicationEntry <- (request.body \ "medicationEntry").validateOpt[String]
        medicationStability <- (request.body \ "medicationStability").validateOpt[String]
        vitalsPhoto <- (request.body \ "vitalsPhoto").validateOpt[String]
        preMedicationTotal <- (request.body \ "preMedicationTotal").validateOpt[String]
        patientTolerate <- (request.body \ "patientTolerate").validateOpt[String]
        suturingMaterials <- (request.body \ "suturingMaterials").validateOpt[String]
        suturingTechniques <- (request.body \ "suturingTechniques").validateOpt[String]
        reasonForNextAppointment <- (request.body \ "reasonForNextAppointment").validateOpt[String]
        procedure <- (request.body \ "procedure").validateOpt[JsValue]
        incision <- (request.body \ "incision").validateOpt[String]
        boneGraftMaterials <- (request.body \ "boneGraftMaterials").validateOpt[String]
        membranesUsed <- (request.body \ "membranesUsed").validateOpt[String]
        fixationSystemUsed <- (request.body \ "fixationSystemUsed").validateOpt[String]
        assistant3 <- (request.body \ "assistant3").validateOpt[String]
        verbalPostopIns <- (request.body \ "verbalPostopIns").validateOpt[String]
        writtenPostopIns <- (request.body \ "writtenPostopIns").validateOpt[String]
        rxVisit <- (request.body \ "rxVisit").validateOpt[String]
        incisionMandible <- (request.body \ "incisionMandible").validateOpt[String]
        voiceMemo <- (request.body \ "voiceMemo").validateOpt[String]
        implantData <- (request.body \ "implantData").validateOpt[JsValue]
        boneGraftingData <- (request.body \ "boneGraftingData").validateOpt[JsValue]

    } yield {

        var dateValue: Option[LocalDate] = None
        if(date != None && date != Some("")){
          dateValue = Some(LocalDate.parse(date))
        }

        var timeValue: Option[LocalTime] = None
        if(time != None && time != Some("")){
          timeValue = Some(LocalTime.parse(time, formatter))
        }

        var dischargeTimeValue: Option[LocalTime] = None
        if(dischargeTime != None && dischargeTime != Some("")){
          dischargeTimeValue = Some(LocalTime.parse(convertOptionString(dischargeTime), formatter))
        }

       (ClinicalNotesRow(None, request.identity.id, patientId, note,DateTime.now,dateValue,timeValue,Some(provider),Some(false), assistant1,assistant2,procedureCode,procedureDescription,bp,pulse,spco2,co2,ekg,resp,temp ,allergies,medicalConditions,postSurgicalHistory,monitors,asaStatus,airwayClass,anesthesia,diagnosis,dischargeDate,dischargeTimeValue,brCompleted,brMidline,brLipPostion,brUpper,brLower,impression,escort,lapsPhoto,surgeryPhoto,balancedOcclusion,anteriorGuidance,canineGuidance,fullDenture,existingDenture,newDenture,pmmaProsthesis,compositeProsthesis,prosthesesPhoto,medicationEntry,medicationStability, vitalsPhoto,preMedicationTotal,patientTolerate,suturingMaterials,suturingTechniques,reasonForNextAppointment,procedure,incision,
       boneGraftMaterials,membranesUsed,fixationSystemUsed,assistant3,verbalPostopIns,writtenPostopIns,rxVisit,incisionMandible,voiceMemo),implantData,boneGraftingData)
      }

      clinicalNotes match {
        case JsSuccess((clinicalNotesRow,implantData,boneGraftingData), _) =>
          db.run {
            ClinicalNotesTable.returning(ClinicalNotesTable.map(_.id)) += clinicalNotesRow
          } map {
            case 0L => PreconditionFailed
            case id => createUpdateDeleteImplantData(implantData,id,practiceId,staffId)
                       createUpdateDeleteBoneGraftData(boneGraftingData,id,practiceId,staffId)
        Created(Json.toJson(clinicalNotesRow.copy(id = Some(id))))
         }
    case JsError(_) => Future.successful(BadRequest)
     }
 }

def createUpdateDeleteBoneGraftData(boneGraftingData: Option[JsValue], noteId: Long, practiceId: Long, staffId: Long){
var skipDeleteList: List[Long] = List()
  if(boneGraftingData != None && !boneGraftingData.isInstanceOf[JsUndefined]){
      val jsonList: List[JsValue] = boneGraftingData.get.as[List[JsValue]]
      jsonList.foreach(json => {
        val id = convertOptionLong((json \"id").asOpt[Long])
        if(id != 0){
        skipDeleteList = skipDeleteList :+ id
        }
      })
      var block = db.run{
        BoneGraftingDataTable.filter(_.noteId === noteId).filterNot(_.id inSet skipDeleteList).delete map{
        case 0 => NotFound
        case _ => Ok
        }
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      jsonList.foreach(json => {
          val id = convertOptionLong((json \"id").asOpt[Long])
          val allograftMaterialUsed = convertOptionString((json \"allograftMaterialUsed").asOpt[String])
          val allograftSize = convertOptionString((json \"allograftSize").asOpt[String])
          val allograftVolume = convertOptionString((json \"allograftVolume").asOpt[String])
          val allograftBrand = convertOptionString((json \ "allograftBrand").asOpt[String])
          val xenograftMaterialUsed = convertOptionString((json \"xenograftMaterialUsed").asOpt[String])
          val xenograftSize = convertOptionString((json \"xenograftSize").asOpt[String])
          val xenograftVolume = convertOptionString((json \"xenograftVolume").asOpt[String])
          val xenograftBrand = convertOptionString((json \"xenograftBrand").asOpt[String])
          val alloplastMaterialUsed = convertOptionString((json \"alloplastMaterialUsed").asOpt[String])
          val alloplastSize = convertOptionString((json \"alloplastSize").asOpt[String])
          val alloplastVolume = convertOptionString((json \"alloplastVolume").asOpt[String])
          val alloplastBrand = convertOptionString((json \"alloplastBrand").asOpt[String])
          val autograftMaterialUsed = convertOptionString((json \"autograftMaterialUsed").asOpt[String])
          val autograftSize = convertOptionString((json \"autograftSize").asOpt[String])
          val autograftVolume = convertOptionString((json \"autograftVolume").asOpt[String])
          val membraneUsed = convertOptionString((json \"membraneUsed").asOpt[String])
          val membraneSize = convertOptionString((json \"membraneSize").asOpt[String])
          val membraneBrand = convertOptionString((json \"membraneBrand").asOpt[String])
          val additives = convertOptionString((json \"additives").asOpt[String])
          val additivesVolume = convertOptionString((json \"additivesVolume").asOpt[String])
          val additivesBrand = convertOptionString((json \"additivesBrand").asOpt[String])
          val finalPA = convertOptionString((json \"finalPA").asOpt[String])
          val rrpsd = (json \"rrpsd").asOpt[LocalDate]
          val failureDate = (json \"failureDate").asOpt[LocalDate]

          if(id == 0){
          var boneGraftingDataRow = BoneGraftingDataRow(None,noteId,practiceId,staffId,"","",None,None,allograftMaterialUsed,allograftSize,allograftVolume,allograftBrand,xenograftMaterialUsed,xenograftSize,xenograftVolume,xenograftBrand,alloplastMaterialUsed,alloplastSize,alloplastVolume,alloplastBrand,autograftMaterialUsed,autograftSize,autograftVolume,membraneUsed,membraneSize,membraneBrand,additives,additivesVolume,additivesBrand,finalPA,rrpsd,failureDate)
          var block = db.run {
          BoneGraftingDataTable.returning(BoneGraftingDataTable.map(_.id)) += boneGraftingDataRow
          }
          Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          } else {
            // UPDATE IMPLANT DATA
          val query = BoneGraftingDataTable.filter(_.id === id)
          db.run {
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(allograftMaterialUsed).map(value => query.map(_.allograftMaterialUsed).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(allograftSize).map(value => query.map(_.allograftSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(allograftVolume).map(value => query.map(_.allograftVolume).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(allograftBrand).map(value => query.map(_.allograftBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(xenograftMaterialUsed).map(value => query.map(_.xenograftMaterialUsed).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(xenograftSize).map(value => query.map(_.xenograftSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(xenograftVolume).map(value => query.map(_.xenograftVolume).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(xenograftBrand).map(value => query.map(_.xenograftBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(alloplastMaterialUsed).map(value => query.map(_.alloplastMaterialUsed).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(alloplastSize).map(value => query.map(_.alloplastSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(alloplastVolume).map(value => query.map(_.alloplastVolume).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(alloplastBrand).map(value => query.map(_.alloplastBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(autograftMaterialUsed).map(value => query.map(_.autograftMaterialUsed).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(autograftSize).map(value => query.map(_.autograftSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(autograftVolume).map(value => query.map(_.autograftVolume).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(membraneUsed).map(value => query.map(_.membraneUsed).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(membraneSize).map(value => query.map(_.membraneSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(membraneBrand).map(value => query.map(_.membraneBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(additives).map(value => query.map(_.additives).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(additivesVolume).map(value => query.map(_.additivesVolume).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(additivesBrand).map(value => query.map(_.additivesBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(finalPA).map(value => query.map(_.finalPA).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(rrpsd).map(value => query.map(_.rrpsd).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
            }
           }
          }
        })
    } else {
        db.run{
          BoneGraftingDataTable.filter(_.noteId === noteId).delete map {
          case 0 => NotFound
          case _ => Ok
          }
      }
    }
}


def createUpdateDeleteImplantData(implantData: Option[JsValue], noteId: Long, practiceId: Long, staffId: Long){
  var skipDeleteList: List[Long] = List()

  if(implantData != None && !implantData.isInstanceOf[JsUndefined]){
      val jsonList: List[JsValue] = implantData.get.as[List[JsValue]]
      jsonList.foreach(json => {
        val id = convertOptionLong((json \"id").asOpt[Long])
        if(id != 0){
        skipDeleteList = skipDeleteList :+ id
        }
      })
      var block = db.run{
        ImplantDataTable.filter(_.noteId === noteId).filterNot(_.id inSet skipDeleteList).delete map{
        case 0 => NotFound
        case _ => Ok
        }
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      jsonList.foreach(json => { 
          val id = convertOptionLong((json \"id").asOpt[Long])
          val implantSite = convertOptionString((json \"implantSite").asOpt[String])
          val implantBrand = convertOptionString((json \"implantBrand").asOpt[String])
          val implantSurface = convertOptionString((json \"implantSurface").asOpt[String])
          val implantReference = convertOptionString((json \ "implantReference").asOpt[String])
          val implantLot = convertOptionString((json \"implantLot").asOpt[String])
          val diameter = convertOptionString((json \"diameter").asOpt[String])
          val length = convertOptionString((json \"length").asOpt[String])
          val surgProtocol = convertOptionString((json \"surgProtocol").asOpt[String])
          val boneDensity = convertOptionString((json \"boneDensity").asOpt[String])
          val osteotomySite = convertOptionString((json \"osteotomySite").asOpt[String])
          val ncm = convertOptionString((json \"ncm").asOpt[String])
          val isqBl = convertOptionString((json \"isqBl").asOpt[String])
          val isqMd = convertOptionString((json \"isqMd").asOpt[String])
          val implantDepth = convertOptionString((json \"implantDepth").asOpt[String])
          val loadingProtocol = convertOptionString((json \"loadingProtocol").asOpt[String])
          val finalPA = convertOptionString((json \"finalPA").asOpt[String])
          val dateOfRemoval = convertOptionString((json \"dateOfRemoval").asOpt[String])
          val reasonForRemoval = convertOptionString((json \"reasonForRemoval").asOpt[String])
          val recommendedHT = convertOptionString((json \"recommendedHT").asOpt[String])
          val implantRestored = convertOptionString((json \"implantRestored").asOpt[String])

          if(id == 0){
          var implantDataRow = ImplantDataRow(None,noteId,implantSite,implantBrand,implantSurface,implantReference,implantLot,diameter,length,surgProtocol,boneDensity,osteotomySite,ncm,isqBl,isqMd,implantDepth,loadingProtocol,dateOfRemoval,reasonForRemoval,finalPA,recommendedHT,implantRestored,Some(""),Some(""),None,Some(practiceId),Some(staffId),None)
          var block = db.run {
          ImplantDataTable.returning(ImplantDataTable.map(_.id)) += implantDataRow
          }
          Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          } else {
            // UPDATE IMPLANT DATA
          val query = ImplantDataTable.filter(_.id === id)
          db.run {
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(implantSite).map(value => query.map(_.implantSite).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantBrand).map(value => query.map(_.implantBrand).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantSurface).map(value => query.map(_.implantSurface).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantReference).map(value => query.map(_.implantReference).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantLot).map(value => query.map(_.implantLot).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(diameter).map(value => query.map(_.diameter).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(length).map(value => query.map(_.length).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(surgProtocol).map(value => query.map(_.surgProtocol).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(boneDensity).map(value => query.map(_.boneDensity).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(osteotomySite).map(value => query.map(_.osteotomySite).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(ncm).map(value => query.map(_.ncm).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(isqBl).map(value => query.map(_.isqBl).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(isqMd).map(value => query.map(_.isqMd).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantDepth).map(value => query.map(_.implantDepth).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(loadingProtocol).map(value => query.map(_.loadingProtocol).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(dateOfRemoval).map(value => query.map(_.dateOfRemoval).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(reasonForRemoval).map(value => query.map(_.reasonForRemoval).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recommendedHT).map(value => query.map(_.recommendedHT).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(implantRestored).map(value => query.map(_.implantRestored).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
            }
           }
          }
        })
    } else {
        db.run{
          ImplantDataTable.filter(_.noteId === noteId).delete map {
          case 0 => NotFound
          case _ => Ok
          }
      }
    }
}

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var staffId = request.identity.id.get
    val formatter = DateTimeFormat.forPattern("hh:mm a");

    val clinicalNotes = for {
      note <- (request.body \ "note").validateOpt[String]
      timeValue <- (request.body \ "time").validateOpt[String]
      dateValue <- (request.body \ "date").validateOpt[String]
      provider <- (request.body \ "provider").validateOpt[Long]
        assistant1 <- (request.body \ "assistant1").validateOpt[String]
        assistant2 <- (request.body \ "assistant2").validateOpt[String]
        procedureCode <- (request.body \ "procedureCode").validateOpt[String]
        procedureDescription <- (request.body \ "procedureDescription").validateOpt[String]
        bp <- (request.body \ "bp").validateOpt[String]
        pulse <- (request.body \ "pulse").validateOpt[String]
        spco2 <- (request.body \ "spco2").validateOpt[String]
        co2 <- (request.body \ "co2").validateOpt[String]
        ekg <- (request.body \ "ekg").validateOpt[String]
        resp <- (request.body \ "resp").validateOpt[String]
        temp <- (request.body \ "temp").validateOpt[String]
        allergies <- (request.body \ "allergies").validateOpt[String]
        medicalConditions <- (request.body \ "medicalConditions").validateOpt[String]
        postSurgicalHistory <- (request.body \ "postSurgicalHistory").validateOpt[String]
        monitors <- (request.body \ "monitors").validateOpt[String]
        asaStatus <- (request.body \ "asaStatus").validateOpt[String]
        airwayClass <- (request.body \ "airwayClass").validateOpt[String]
        anesthesia <- (request.body \ "anesthesia").validateOpt[String]
        diagnosis <- (request.body \ "diagnosis").validateOpt[String]
        dischargeDate <- (request.body \ "dischargeDate").validateOpt[LocalDate]
        dischargeTime <- (request.body \ "dischargeTime").validateOpt[String]
        brCompleted <- (request.body \ "brCompleted").validateOpt[String]
        brMidline <- (request.body \ "brMidline").validateOpt[String]
        brLipPostion <- (request.body \ "brLipPostion").validateOpt[String]
        brUpper <- (request.body \ "brUpper").validateOpt[String]
        brLower <- (request.body \ "brLower").validateOpt[String]
        impression <- (request.body \ "impression").validateOpt[String]
        escort <- (request.body \ "escort").validateOpt[String]
        lapsPhoto <- (request.body \ "lapsPhoto").validateOpt[String]
        surgeryPhoto <- (request.body \ "surgeryPhoto").validateOpt[String]
        balancedOcclusion <- (request.body \ "balancedOcclusion").validateOpt[String]
        anteriorGuidance <- (request.body \ "anteriorGuidance").validateOpt[String]
        canineGuidance <- (request.body \ "canineGuidance").validateOpt[String]
        fullDenture <- (request.body \ "fullDenture").validateOpt[String]
        existingDenture <- (request.body \ "existingDenture").validateOpt[String]
        newDenture <- (request.body \ "newDenture").validateOpt[String]
        pmmaProsthesis <- (request.body \ "pmmaProsthesis").validateOpt[String]
        compositeProsthesis <- (request.body \ "compositeProsthesis").validateOpt[String]
        prosthesesPhoto <- (request.body \ "prosthesesPhoto").validateOpt[String]
        medicationEntry <- (request.body \ "medicationEntry").validateOpt[String]
        medicationStability <- (request.body \ "medicationStability").validateOpt[String]
        vitalsPhoto <- (request.body \ "vitalsPhoto").validateOpt[String]
        preMedicationTotal <- (request.body \ "preMedicationTotal").validateOpt[String]
        patientTolerate <- (request.body \ "patientTolerate").validateOpt[String]
        suturingMaterials <- (request.body \ "suturingMaterials").validateOpt[String]
        suturingTechniques <- (request.body \ "suturingTechniques").validateOpt[String]
        reasonForNextAppointment <- (request.body \ "reasonForNextAppointment").validateOpt[String]
        procedure <- (request.body \ "procedure").validateOpt[JsValue]
        incision <- (request.body \ "incision").validateOpt[String]
        boneGraftMaterials <- (request.body \ "boneGraftMaterials").validateOpt[String]
        membranesUsed <- (request.body \ "membranesUsed").validateOpt[String]
        fixationSystemUsed <- (request.body \ "fixationSystemUsed").validateOpt[String]
        assistant3 <- (request.body \ "assistant3").validateOpt[String]
        verbalPostopIns <- (request.body \ "verbalPostopIns").validateOpt[String]
        writtenPostopIns <- (request.body \ "writtenPostopIns").validateOpt[String]
        rxVisit <- (request.body \ "rxVisit").validateOpt[String]
        incisionMandible <- (request.body \ "incisionMandible").validateOpt[String]
        voiceMemo <- (request.body \ "voiceMemo").validateOpt[String]
        implantData <- (request.body \ "implantData").validateOpt[JsValue]
        boneGraftingData <- (request.body \ "boneGraftingData").validateOpt[JsValue]

    } yield {

      var date: Option[LocalDate] = None
      if(dateValue != None && dateValue != Some("")){
        date = Some(LocalDate.parse(convertOptionString(dateValue)))
      }

      var time: Option[LocalTime] = None
      if(timeValue != None && timeValue != Some("")){
        time = Some(LocalTime.parse(convertOptionString(timeValue), formatter))
      }

      var dischargeTimeValue: Option[LocalTime] = None
      if(dischargeTime != None && dischargeTime != Some("")){
        dischargeTimeValue = Some(LocalTime.parse(convertOptionString(dischargeTime), formatter))
      }

(ClinicalNotesRow(None, request.identity.id, 1, note, DateTime.now,date,time,provider,Some(false), assistant1,assistant2,procedureCode,procedureDescription,bp,pulse,spco2,co2,ekg,resp,temp ,allergies,medicalConditions,postSurgicalHistory,monitors,asaStatus,airwayClass,anesthesia,diagnosis,dischargeDate,dischargeTimeValue,brCompleted,brMidline,brLipPostion,brUpper,brLower,impression,escort,lapsPhoto,surgeryPhoto,balancedOcclusion,anteriorGuidance,canineGuidance,fullDenture,existingDenture,newDenture,pmmaProsthesis,compositeProsthesis,prosthesesPhoto,medicationEntry,medicationStability,vitalsPhoto,preMedicationTotal,patientTolerate,suturingMaterials,suturingTechniques,reasonForNextAppointment,procedure,incision,boneGraftMaterials,membranesUsed,fixationSystemUsed,assistant3,verbalPostopIns,writtenPostopIns,rxVisit,incisionMandible,voiceMemo),implantData,boneGraftingData)
}
    clinicalNotes match {
        case JsSuccess((clinicalNote,implantData,boneGraftingData), _) =>
        db.run {
        val query = ClinicalNotesTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            clinicalNote.staffId.map(value => query.map(_.staffId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.note.map(value => query.map(_.note).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.time.map(value => query.map(_.time).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.date.map(value => query.map(_.date).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.provider.map(value => query.map(_.provider).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.assistant1.map(value => query.map(_.assistant1).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.assistant2.map(value => query.map(_.assistant2).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.procedureCode.map(value => query.map(_.procedureCode).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.procedureDescription.map(value => query.map(_.procedureDescription).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.bp.map(value => query.map(_.bp).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.pulse.map(value => query.map(_.pulse).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.spco2.map(value => query.map(_.spco2).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.co2.map(value => query.map(_.co2).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.ekg.map(value => query.map(_.ekg).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.resp.map(value => query.map(_.resp).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.temp.map(value => query.map(_.temp).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.allergies.map(value => query.map(_.allergies).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.medicalConditions.map(value => query.map(_.medicalConditions).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.postSurgicalHistory.map(value => query.map(_.postSurgicalHistory).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.monitors.map(value => query.map(_.monitors).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.asaStatus.map(value => query.map(_.asaStatus).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.airwayClass.map(value => query.map(_.airwayClass).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.anesthesia.map(value => query.map(_.anesthesia).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.diagnosis.map(value => query.map(_.diagnosis).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.dischargeDate.map(value => query.map(_.dischargeDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.dischargeTime.map(value => query.map(_.dischargeTime).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.brCompleted.map(value => query.map(_.brCompleted).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.brMidline.map(value => query.map(_.brMidline).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.brLipPostion.map(value => query.map(_.brLipPostion).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.brUpper.map(value => query.map(_.brUpper).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.brLower.map(value => query.map(_.brLower).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.impression.map(value => query.map(_.impression).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.escort.map(value => query.map(_.escort).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.lapsPhoto.map(value => query.map(_.lapsPhoto).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.surgeryPhoto.map(value => query.map(_.surgeryPhoto).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.balancedOcclusion.map(value => query.map(_.balancedOcclusion).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.anteriorGuidance.map(value => query.map(_.anteriorGuidance).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.canineGuidance.map(value => query.map(_.canineGuidance).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.fullDenture.map(value => query.map(_.fullDenture).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.existingDenture.map(value => query.map(_.existingDenture).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.newDenture.map(value => query.map(_.newDenture).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.pmmaProsthesis.map(value => query.map(_.pmmaProsthesis).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.compositeProsthesis.map(value => query.map(_.compositeProsthesis).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.prosthesesPhoto.map(value => query.map(_.prosthesesPhoto).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.medicationEntry.map(value => query.map(_.medicationEntry).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.medicationStability.map(value => query.map(_.medicationStability).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.vitalsPhoto.map(value => query.map(_.vitalsPhoto).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.preMedicationTotal.map(value => query.map(_.preMedicationTotal).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.patientTolerate.map(value => query.map(_.patientTolerate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.suturingMaterials.map(value => query.map(_.suturingMaterials).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.suturingTechniques.map(value => query.map(_.suturingTechniques).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.reasonForNextAppointment.map(value => query.map(_.reasonForNextAppointment).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.procedure.map(value => query.map(_.procedure).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.incision.map(value => query.map(_.incision).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.boneGraftMaterials.map(value => query.map(_.boneGraftMaterials).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.membranesUsed.map(value => query.map(_.membranesUsed).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.fixationSystemUsed.map(value => query.map(_.fixationSystemUsed).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.assistant3.map(value => query.map(_.assistant3).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.verbalPostopIns.map(value => query.map(_.verbalPostopIns).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.writtenPostopIns.map(value => query.map(_.writtenPostopIns).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.rxVisit.map(value => query.map(_.rxVisit).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.incisionMandible.map(value => query.map(_.incisionMandible).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNote.voiceMemo.map(value => query.map(_.voiceMemo).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => createUpdateDeleteImplantData(implantData,id,practiceId,staffId)
            createUpdateDeleteBoneGraftData(boneGraftingData,id,practiceId,staffId)
          Ok
          }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      ClinicalNotesTable.filter(_.id === id).delete map {
        case 0 => NotFound
        case _ => db.run{
        ImplantDataTable.filter(_.noteId === id).delete map {
          case 0 => NotFound
          case _ => Ok
          }
        }
        Ok
      }
    }
  }

  def updateLock(id: Long) = silhouette.SecuredAction.async { request =>
    db.run {
      ClinicalNotesTable.filter(_.id === id).map(_.lock).update(Some(true))
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

def clinicalNotesMigration = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    val formatter = DateTimeFormat.forPattern("hh:mm a");
    var timeValue: Option[LocalTime] = None
    db.run {
      for{
          clinicalNotes <- ClinicalNotesTable.result
      } yield {
            (clinicalNotes).foreach( notes => {
              if(notes.note != None){
              var noteValue = convertOptionString(notes.note)
              val date = Some(LocalDate.parse(noteValue.substring(0, 10)))
              var time = noteValue.substring(11, 19)

              if(time contains ","){
                time = "0"+time.dropRight(1)
              }
              if((time contains "AM") || (time contains "PM")){
              noteValue = noteValue.substring(20)
              // noteValue = noteValue.split(',')(1)
              } else {
              noteValue = noteValue.substring(19)
                }

              if(time != ""){
                if((time contains "AM") || (time contains "PM")){
                  timeValue = Some(LocalTime.parse(time, formatter))
                } else {
                  timeValue = Some(LocalTime.parse(time))
                }
              }

        var block =  db.run {
          val query = ClinicalNotesTable.filter(_.id === notes.id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true => DBIO.seq[Effect.Write](
              date.map(value => query.map(_.date).update(date).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              timeValue.map(value => query.map(_.time).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(noteValue).map(value => query.map(_.note).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

            ) map {_ => Ok}
            case false => DBIO.successful(NotFound)
          }
        }
        var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
        }
      })
        Ok { Json.obj("status"->"success","message"->"Clinical Notes Migrated Successfully")}
        }
     }
  }


 def implantData(fromDate: Option[String],toDate: Option[String],providerId: Option[String],implantBrand: Option[String],loadingProtocol: Option[String],implantRemoved: Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var implantObj : List[JsValue] = List()
 var successRate: Double = 0
 var filterRecords = 0
 var totalRecords = 0
 var sFromDate = convertOptionString(fromDate)
 var sToDate = convertOptionString(toDate)
 var sImplantBrand = convertOptionString(implantBrand)
 var sLoadingProtocol = convertOptionString(loadingProtocol)
 var sImplantRemoved = convertOptionString(implantRemoved)
 var pageNumber:Int = 0
 var recPerPage:Int = 0
 var providerList = List.empty[Long]
 var patientList = List.empty[Long]
 var implantList = List.empty[Long]
 val searchTextVal: Option[String] = request.headers.get("searchText")
 val searchText:String = convertOptionString(searchTextVal)
 var pageNumberVal: Option[String] = request.headers.get("pageNumber")
 
 if ( pageNumberVal != None){
    pageNumber = convertOptionString(pageNumberVal).toInt
  }
  // if (pageNumber == 0 ){
  //   pageNumber = 1
  // }

  var recPerPageVal: Option[String] = request.headers.get("recPerPage")
  if( recPerPageVal != None){
    recPerPage = convertOptionString(recPerPageVal).toInt
  }    
 var clinicalQuery = ClinicalNotesTable.sortBy(_.date.desc)
 var implantQuery = ImplantDataTable.sortBy(_.date.desc)

 if(sFromDate != "" && sToDate != ""){
   clinicalQuery = clinicalQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
 }
 if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
   clinicalQuery = clinicalQuery.filter(_.provider === convertStringToLong(providerId))
 }
 if (searchText != ""){

  var providerIdList = db.run{
    for {
      providerData <- ProviderTable.filter(f=>((f.title.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.firstName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.lastName.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      providerData.map{provider=> providerList =  providerList :+ convertOptionLong(provider.id)}
    }
  }
  val AwaitResult = Await.ready(providerIdList, atMost = scala.concurrent.duration.Duration(30, SECONDS))


  var patientIdList = db.run{
    for {
      patientData <- TreatmentTable.filter(f=>((f.firstName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.lastName.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      patientData.map{patient=> patientList =  patientList :+ convertOptionLong(patient.id)}
    }
    }
  val AwaitResult2 = Await.ready(patientIdList, atMost = scala.concurrent.duration.Duration(30, SECONDS))


  var implantIdBlock = db.run{
    for {
      implantDataFilter <- ImplantDataTable.filter(f=>((f.patientName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.provider.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantSite.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantBrand.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantSurface.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantReference.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantLot.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.diameter.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.length.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.surgProtocol.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.boneDensity.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.osteotomySite.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.isqBl.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.isqMd.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantDepth.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.loadingProtocol.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.implantRestored.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.ncm.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.reasonForRemoval.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.dateOfRemoval.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.recommendedHT.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      implantDataFilter.map{ notes => implantList =  implantList :+ convertOptionLong(notes.id)}
    }
  }
  Await.ready(implantIdBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
 }


 if(sImplantBrand != "ALL" && sImplantBrand != ""){
   implantQuery = implantQuery.filter(_.implantBrand === sImplantBrand)
 }
 if(sLoadingProtocol != "ALL" && sLoadingProtocol != ""){
   implantQuery = implantQuery.filter(_.loadingProtocol === sLoadingProtocol)
 }
 if(sImplantRemoved != "Both" && sImplantRemoved != ""){
   if(sImplantRemoved == "yes"){
       implantQuery = implantQuery.filterNot(_.dateOfRemoval === "")
   } else if(sImplantRemoved == "no"){
       implantQuery = implantQuery.filter(_.dateOfRemoval === "")
   }
 }

if(providerList.length!=0) {
  clinicalQuery = clinicalQuery.filter(f=>((f.provider inSet providerList)))
}
if (patientList.length!=0)  {
  clinicalQuery = clinicalQuery.filter(f=>((f.patientId inSet patientList)))
}
if (implantList.length!=0)  {
  implantQuery = implantQuery.filter(f=>((f.id inSet implantList)))
}

      db.run{
        clinicalQuery
        .join(implantQuery).on(_.id === _.noteId).filter(_._2.practiceId === practiceId)
        .join(TreatmentTable).on(_._1.patientId === _.id).filterNot(_._2.deleted).result

      } map { clinicalNotesList =>
        val data = clinicalNotesList.groupBy(_._1._2.id).map {
        case (clinicalNotesId, entries) =>

          val clinicalNote = entries.head._1._1
          val s = entries.head._1._2
          val treatment = entries.head._2

          var patientName = convertOptionString(treatment.firstName) + " "+ convertOptionString(treatment.lastName)
          var birth_date = convertOptionString(treatment.dateOfBirth)
          var providerName = ""
          
          //GETTING TOTAL RECORDS
          totalRecords = totalRecords + 1

          //GETTING DATEOFREMOVAL NOT NULL RECORDS
          if(s.dateOfRemoval != null && s.dateOfRemoval != ""){
            filterRecords  = filterRecords + 1
          }

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var vDate: Date = simpleDateFormat.parse(convertOptionalLocalDate(clinicalNote.date).toString);
          val date = new SimpleDateFormat("mm/dd/yyyy").format(vDate)

          val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
          var dateCreated = dateFormatter.print(clinicalNote.dateCreated)

          var block = db.run{
            ProviderTable.filter(_.id === clinicalNote.provider).result
            } map { results =>
            results.map{ providers =>
            providerName = providers.title +" "+ providers.firstName +" "+ providers.lastName
            }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("patientName" -> JsString(patientName)) + ("dob" -> JsString(birth_date)) + ("providerName" -> JsString(providerName)) + ("date" -> JsString(date.toString)) + ("dateCreated" -> JsString(dateCreated.toString)) + ("implantSite" -> JsString(s.implantSite)) + ("implantBrand" -> JsString(s.implantBrand)) + ("implantSurface" -> JsString(s.implantSurface)) + ("implantReference" -> JsString(s.implantReference)) + ("implantLot" -> JsString(s.implantLot)) + ("diameter" -> JsString(s.diameter)) + ("length" -> JsString(s.length)) + ("surgProtocol" -> JsString(s.surgProtocol)) + ("boneDensity" -> JsString(s.boneDensity)) + ("osteotomySite" -> JsString(s.osteotomySite)) + ("ncm" -> JsString(s.ncm)) + ("isqBl" -> JsString(s.isqBl)) + ("isqMd" -> JsString(s.isqMd)) + ("implantDepth" -> JsString(s.implantDepth)) + ("loadingProtocol" -> JsString(s.loadingProtocol)) + ("dateOfRemoval" -> JsString(s.dateOfRemoval)) + ("reasonForRemoval" -> JsString(s.reasonForRemoval)) + ("finalPA" -> JsString(s.finalPA)) + ("recommendedHT" -> JsString(s.recommendedHT)) + ("implantRestored" -> JsString(s.implantRestored)) + ("practiceId" -> JsNumber(convertOptionLong(s.practiceId))) + ("staffId" -> JsNumber(convertOptionLong(s.staffId)))
        implantObj = implantObj :+ newObj
      }

      var sRate : Float = (((totalRecords - filterRecords).toFloat / totalRecords.toFloat) * 100)
      if(!sRate.isNaN){
          successRate =  Math.round(sRate * 100.0) / 100.0
      }

    //ADDING EMPTY NOTE ID RECORDS
    var emptyNoteId: Long = 0
    var providerWithTitle = ""
    var providerWithoutTitle = ""
    implantQuery =  implantQuery.filter(_.practiceId === practiceId)
    if(sFromDate != "" && sToDate != ""){
      implantQuery = implantQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
    }
    // if(searchText != ""){
    //   implantQuery = implantQuery.filter(f=>((f.patientName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.provider.toLowerCase like "%"+searchText.toLowerCase+"%")))
    // }

    // if(sProviderId != 0){
    if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
      var b = db.run{
        ProviderTable.filter(_.id === convertStringToLong(providerId)).result.head
      } map { providerResult =>
          providerWithTitle = providerResult.title+" "+ providerResult.firstName+" "+ providerResult.lastName
          providerWithoutTitle = providerResult.firstName+" "+ providerResult.lastName
      }
      Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      implantQuery = implantQuery.filter(p => p.provider === providerWithTitle || p.provider === providerWithoutTitle)
    }


      val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
      var block = db.run{
        implantQuery.filter(_.noteId === emptyNoteId).result
      } map { implant =>
        implant.foreach(s => {
          var iDate: String = null
          var iDob: String = null

          if(s.date != None){
            iDate = dateFormatter.print(s.date.get)
          }
          if(s.dob != None){
            iDob = dateFormatter.print(s.dob.get)
          }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id))) + ("noteId" -> JsNumber(s.noteId)) + ("patientName" -> JsString(convertOptionString(s.patientName))) + ("dob" -> JsString(iDob)) + ("providerName" -> JsString(convertOptionString(s.provider))) + ("date" -> JsString(iDate)) + ("dateCreated" -> JsString("")) + ("implantSite" -> JsString(s.implantSite)) + ("implantBrand" -> JsString(s.implantBrand)) + ("implantSurface" -> JsString(s.implantSurface)) + ("implantReference" -> JsString(s.implantReference)) + ("implantLot" -> JsString(s.implantLot)) + ("diameter" -> JsString(s.diameter)) + ("length" -> JsString(s.length)) + ("surgProtocol" -> JsString(s.surgProtocol)) + ("boneDensity" -> JsString(s.boneDensity)) + ("osteotomySite" -> JsString(s.osteotomySite)) + ("ncm" -> JsString(s.ncm)) + ("isqBl" -> JsString(s.isqBl)) + ("isqMd" -> JsString(s.isqMd)) + ("implantDepth" -> JsString(s.implantDepth)) + ("loadingProtocol" -> JsString(s.loadingProtocol)) + ("dateOfRemoval" -> JsString(s.dateOfRemoval)) + ("reasonForRemoval" -> JsString(s.reasonForRemoval)) + ("finalPA" -> JsString(s.finalPA)) + ("recommendedHT" -> JsString(s.recommendedHT)) + ("implantRestored" -> JsString(s.implantRestored)) + ("practiceId" -> JsNumber(convertOptionLong(s.practiceId))) + ("staffId" -> JsNumber(convertOptionLong(s.staffId)))
        implantObj = implantObj :+ newObj
        })
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      var simpleDateFormat1:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

      if (recPerPage == 0 && pageNumber == 0){
      val newObj = Json.obj() + ("implantData" -> JsArray(implantObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse)) + ("successRate" -> JsNumber(successRate))
      val pageData = newObj
      Ok(Json.toJson(pageData)).withHeaders(("totalCount" -> implantObj.length.toString()))
      }
      else{
      implantObj = implantObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse
      val newObj = Json.obj() + ("implantData" -> JsArray(implantObj.drop((pageNumber-1)*recPerPage).take(recPerPage))) + ("successRate" -> JsNumber(successRate))
      val pageData = newObj
      Ok(Json.toJson(pageData)).withHeaders(("totalCount" -> implantObj.length.toString()))
      }
      }
 }

 def boneGraftData(fromDate: Option[String],toDate: Option[String],providerId: Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var boneGraftObj : List[JsValue] = List()
 var successRate: Double = 0
 var filterRecords = 0
 var totalRecords = 0
 var sFromDate = convertOptionString(fromDate)
 var sToDate = convertOptionString(toDate)
 var pageNumber:Int = 0
 var recPerPage:Int = 0
 var providerList = List.empty[Long]
 var patientList = List.empty[Long]
 var boneGraftList = List.empty[Long]
 val searchTextVal: Option[String] = request.headers.get("searchText")
 val searchText:String = convertOptionString(searchTextVal)
 var pageNumberVal: Option[String] = request.headers.get("pageNumber")

 if ( pageNumberVal != None){
    pageNumber = convertOptionString(pageNumberVal).toInt
  }
  // if (pageNumber == 0 ){
  //   pageNumber = 1
  // }

  var recPerPageVal: Option[String] = request.headers.get("recPerPage")
  if( recPerPageVal != None){
    recPerPage = convertOptionString(recPerPageVal).toInt
  }
 var clinicalQuery = ClinicalNotesTable.sortBy(_.date.desc)
 var boneGraftQuery = BoneGraftingDataTable.sortBy(_.date.desc)

 if(sFromDate != "" && sToDate != ""){
   clinicalQuery = clinicalQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
 }
 if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
   clinicalQuery = clinicalQuery.filter(_.provider === convertStringToLong(providerId))
 }
 if (searchText != ""){
  var providerIdList = db.run{
    for {
      providerData <- ProviderTable.filter(f=>((f.title.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.firstName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.lastName.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      providerData.map{provider=> providerList =  providerList :+ convertOptionLong(provider.id)}
    }
  }
  val AwaitResult = Await.ready(providerIdList, atMost = scala.concurrent.duration.Duration(30, SECONDS))


  var patientIdList = db.run{
    for {
      patientData <- TreatmentTable.filter(f=>((f.firstName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.lastName.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      patientData.map{patient=> patientList =  patientList :+ convertOptionLong(patient.id)}
    }
    }
  val AwaitResult2 = Await.ready(patientIdList, atMost = scala.concurrent.duration.Duration(30, SECONDS))


   var boneGraftIdBlock = db.run{
    for {
      boneGraftDataFilter <- BoneGraftingDataTable.filter(f=>((f.patientName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.providerName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.allograftMaterialUsed.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.allograftSize.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.allograftVolume.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.allograftBrand.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.xenograftMaterialUsed.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.xenograftSize.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.xenograftVolume.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.xenograftBrand.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.alloplastMaterialUsed.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.alloplastSize.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.alloplastVolume.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.alloplastBrand.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.autograftMaterialUsed.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.autograftSize.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.autograftVolume.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.membraneUsed.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.membraneSize.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.membraneBrand.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.additives.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.additivesVolume.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.additivesBrand.toLowerCase like "%"+searchText.toLowerCase+"%"))).result
    } yield{
      boneGraftDataFilter.map{ notes => boneGraftList =  boneGraftList :+ convertOptionLong(notes.id)}
    }
  }
  Await.ready(boneGraftIdBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
 }

if(providerList.length!=0) {
  clinicalQuery = clinicalQuery.filter(f=>((f.provider inSet providerList)))
}
if (patientList.length!=0)  {
  clinicalQuery = clinicalQuery.filter(f=>((f.patientId inSet patientList)))
}
if (boneGraftList.length!=0)  {
  boneGraftQuery = boneGraftQuery.filter(f=>((f.id inSet boneGraftList)))
}

      db.run{
        clinicalQuery
        .join(boneGraftQuery).on(_.id === _.noteId).filter(_._2.practiceId === practiceId)
        .join(TreatmentTable).on(_._1.patientId === _.id).filterNot(_._2.deleted).result
      } map { clinicalNotesList =>
        val data = clinicalNotesList.groupBy(_._1._2.id).map {
        case (clinicalNotesId, entries) =>

          val clinicalNote = entries.head._1._1
          val s = entries.head._1._2
          val treatment = entries.head._2

          var patientName = convertOptionString(treatment.firstName) + " "+ convertOptionString(treatment.lastName)
          var birth_date = convertOptionString(treatment.dateOfBirth)
          var providerName = ""

          //GETTING TOTAL RECORDS
          totalRecords = totalRecords + 1

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var vDate: Date = simpleDateFormat.parse(convertOptionalLocalDate(clinicalNote.date).toString);
          val date = new SimpleDateFormat("mm/dd/yyyy").format(vDate)

          val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
          var dateCreated = dateFormatter.print(clinicalNote.dateCreated)

          var block = db.run{
            ProviderTable.filter(_.id === clinicalNote.provider).result
            } map { results =>
            results.map{ providers =>
            providerName = providers.title +" "+ providers.firstName +" "+ providers.lastName
            }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        var sRrpsd = ""
        var failureDate = ""
        if(s.rrpsd != None && s.rrpsd != null){
          sRrpsd = dateFormatter.print(s.rrpsd.get)
        }
        if(s.failureDate != None && s.failureDate != null){
          failureDate = dateFormatter.print(s.failureDate.get)
        }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("practiceId" -> JsNumber(s.practiceId)) + ("staffId" -> JsNumber(s.staffId)) + ("providerName" -> JsString(providerName)) + ("patientName" -> JsString(patientName)) + ("date" -> JsString(date)) + ("dob" -> JsString(birth_date)) + ("dateCreated" -> JsString(dateCreated.toString)) + ("allograftMaterialUsed" -> JsString(s.allograftMaterialUsed)) + ("allograftSize" -> JsString(s.allograftSize)) + ("allograftVolume" -> JsString(s.allograftVolume)) + ("allograftBrand" -> JsString(s.allograftBrand)) + ("xenograftMaterialUsed" -> JsString(s.xenograftMaterialUsed)) + ("xenograftSize" -> JsString(s.xenograftSize)) + ("xenograftVolume" -> JsString(s.xenograftVolume)) + ("xenograftBrand" -> JsString(s.xenograftBrand)) + ("alloplastMaterialUsed" -> JsString(s.alloplastMaterialUsed)) + ("alloplastSize" -> JsString(s.alloplastSize)) + ("alloplastVolume" -> JsString(s.alloplastVolume)) + ("alloplastBrand" -> JsString(s.alloplastBrand)) + ("autograftMaterialUsed" -> JsString(s.autograftMaterialUsed)) + ("autograftSize" -> JsString(s.autograftSize)) + ("autograftVolume" -> JsString(s.autograftVolume)) + ("membraneUsed" -> JsString(s.membraneUsed)) + ("membraneSize" -> JsString(s.membraneSize)) + ("membraneBrand" -> JsString(s.membraneBrand)) + ("additives" -> JsString(s.additives)) + ("additivesVolume" -> JsString(s.additivesVolume)) + ("additivesBrand" -> JsString(s.additivesBrand)) + ("finalPA" -> JsString(s.finalPA)) + ("rrpsd" -> JsString(sRrpsd)) + ("failureDate" -> JsString(failureDate))
        boneGraftObj = boneGraftObj :+ newObj
      }

      var sRate : Float = (((totalRecords - filterRecords).toFloat / totalRecords.toFloat) * 100)
      if(!sRate.isNaN){
          successRate =  Math.round(sRate * 100.0) / 100.0
      }

    //ADDING EMPTY NOTE ID RECORDS
    var emptyNoteId: Long = 0
    var providerWithTitle = ""
    var providerWithoutTitle = ""
    boneGraftQuery =  boneGraftQuery.filter(_.practiceId === practiceId)
    if(sFromDate != "" && sToDate != ""){
      boneGraftQuery = boneGraftQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
    }
    // if(searchText != ""){
    //   boneGraftQuery = boneGraftQuery.filter(f=>((f.patientName.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.providerName.toLowerCase like "%"+searchText.toLowerCase+"%")))
    // }
    // if(sProviderId != 0){
    if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
      var b = db.run{
        ProviderTable.filter(_.id === convertStringToLong(providerId)).result.head
      } map { providerResult =>
          providerWithTitle = providerResult.title+" "+ providerResult.firstName+" "+ providerResult.lastName
          providerWithoutTitle = providerResult.firstName+" "+ providerResult.lastName
      }
      Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      boneGraftQuery = boneGraftQuery.filter(p => p.providerName === providerWithTitle || p.providerName === providerWithoutTitle)
    }

      val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
      var block = db.run{
        boneGraftQuery.filter(_.noteId === emptyNoteId).result
      } map { implant =>
        implant.foreach(s => {
          var iDate = ""
          var iDob = ""
          var iRrpsd = ""
          var failureDate = ""

          if(s.date != None){
            iDate = dateFormatter.print(s.date.get)
          }
          if(s.dob != None){
            iDob = dateFormatter.print(s.dob.get)
          }
          if(s.rrpsd != None){
            iRrpsd = dateFormatter.print(s.rrpsd.get)
          }
          if(s.failureDate != None){
            failureDate = dateFormatter.print(s.failureDate.get)
          }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("practiceId" -> JsNumber(s.practiceId)) + ("staffId" -> JsNumber(s.staffId)) + ("providerName" -> JsString(s.providerName)) + ("patientName" -> JsString(s.patientName)) + ("date" -> JsString(iDate)) + ("dob" -> JsString(iDob)) + ("dateCreated" -> JsString("")) + ("allograftMaterialUsed" -> JsString(s.allograftMaterialUsed)) + ("allograftSize" -> JsString(s.allograftSize)) + ("allograftVolume" -> JsString(s.allograftVolume)) + ("allograftBrand" -> JsString(s.allograftBrand)) + ("xenograftMaterialUsed" -> JsString(s.xenograftMaterialUsed)) + ("xenograftSize" -> JsString(s.xenograftSize)) + ("xenograftVolume" -> JsString(s.xenograftVolume)) + ("xenograftBrand" -> JsString(s.xenograftBrand)) + ("alloplastMaterialUsed" -> JsString(s.alloplastMaterialUsed)) + ("alloplastSize" -> JsString(s.alloplastSize)) + ("alloplastVolume" -> JsString(s.alloplastVolume)) + ("alloplastBrand" -> JsString(s.alloplastBrand)) + ("autograftMaterialUsed" -> JsString(s.autograftMaterialUsed)) + ("autograftSize" -> JsString(s.autograftSize)) + ("autograftVolume" -> JsString(s.autograftVolume)) + ("membraneUsed" -> JsString(s.membraneUsed)) + ("membraneSize" -> JsString(s.membraneSize)) + ("membraneBrand" -> JsString(s.membraneBrand)) + ("additives" -> JsString(s.additives)) + ("additivesVolume" -> JsString(s.additivesVolume)) + ("additivesBrand" -> JsString(s.additivesBrand)) + ("finalPA" -> JsString(s.finalPA)) + ("rrpsd" -> JsString(iRrpsd)) + ("failureDate" -> JsString(failureDate))
        boneGraftObj = boneGraftObj :+ newObj
        })
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      var simpleDateFormat1:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

      if (recPerPage == 0 && pageNumber == 0){
      val newObj = Json.obj() + ("boneGraftData" -> JsArray(boneGraftObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse)) + ("successRate" -> JsNumber(successRate))
      Ok(Json.toJson(newObj)).withHeaders(("totalCount" -> boneGraftObj.length.toString()))
      }
      else{
      boneGraftObj = boneGraftObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse
      val newObj = Json.obj() + ("boneGraftData" -> JsArray(boneGraftObj.drop((pageNumber-1)*recPerPage).take(recPerPage))) + ("successRate" -> JsNumber(successRate))
      Ok(Json.toJson(newObj)).withHeaders(("totalCount" -> boneGraftObj.length.toString()))
      }
      }
 }

 def updateImplantData(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val implant = for {
      dateOfRemoval <- (request.body \ "dateOfRemoval").validateOpt[String]
      reasonForRemoval <- (request.body \ "reasonForRemoval").validateOpt[String]
      implantRestored <- (request.body \ "implantRestored").validateOpt[String]
      finalPA <- (request.body \ "finalPA").validateOpt[String]
    } yield {
      (dateOfRemoval, reasonForRemoval, implantRestored, finalPA)
    }
    implant match {
      case JsSuccess((dateOfRemoval, reasonForRemoval, implantRestored, finalPA), _) => db.run {
        val query = ImplantDataTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            dateOfRemoval.map(value => query.map(_.dateOfRemoval).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            reasonForRemoval.map(value => query.map(_.reasonForRemoval).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            implantRestored.map(value => query.map(_.implantRestored).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            finalPA.map(value => query.map(_.finalPA).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def implantDataMigration = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var staffId = request.identity.id.get

    db.run {
      for{
          clinicalNotes <- ClinicalNotesTable.result
      } yield {
        (clinicalNotes).foreach( notes => {
          var noteId = convertOptionLong(notes.id)
          var medicationStability = convertOptionString(notes.medicationStability)
          if(medicationStability != null && medicationStability != ""){
          var temp = medicationStability.replace('★', '"')
          val jsonObject: JsValue = Json.parse(temp)
          val mStabilityValues  =  (jsonObject \ "medicationStability").as[List[JsValue]]
          mStabilityValues.foreach(parseResult => {
            val implantSite = convertOptionString((parseResult \ "implant").asOpt[String])
            val implantBrand = convertOptionString((parseResult \ "implantBrand").asOpt[String])
            val implantSurface = convertOptionString((parseResult \ "implantSurface").asOpt[String])
            val implantReference = convertOptionString((parseResult \ "implantArticle").asOpt[String])
            val implantLot = convertOptionString((parseResult \ "implantLot").asOpt[String])
            val diameter = convertOptionString((parseResult \ "diameter").asOpt[String])
            val length = convertOptionString((parseResult \ "length").asOpt[String])
            val surgProtocol = convertOptionString((parseResult \ "surgProtocol").asOpt[String])
            val boneDensity = convertOptionString((parseResult \ "boneDensity").asOpt[String])
            val osteotomySite = convertOptionString((parseResult \ "implantSite").asOpt[String])
            val ncm = convertOptionString((parseResult \ "ncm").asOpt[String])
            val isqBl = convertOptionString((parseResult \ "isqBl").asOpt[String])
            val isqMd = convertOptionString((parseResult \ "isqMd").asOpt[String])
            val implantDepth = convertOptionString((parseResult \ "implantDepth").asOpt[String])
            val loadingProtocol = convertOptionString((parseResult \ "loadingProtocol").asOpt[String])
            val finalPA = convertOptionString((parseResult \ "finalPA").asOpt[String])
            val dateOfRemoval = convertOptionString((parseResult \ "dateOfRemoval").asOpt[String])
            val reasonForRemoval = convertOptionString((parseResult \ "reasonForRemoval").asOpt[String])

            var implantRow = ImplantDataRow(None,noteId,implantSite,implantBrand,implantSurface,implantReference,implantLot,diameter,length,surgProtocol,boneDensity,osteotomySite,ncm,isqBl,isqMd,implantDepth,loadingProtocol,dateOfRemoval,reasonForRemoval,finalPA,"","",Some(""),Some(""),None,Some(practiceId),Some(staffId),Some(LocalDate.now))
              var block = db.run {
                ImplantDataTable.returning(ImplantDataTable.map(_.id)) += implantRow
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          })
        }
        })

        Ok { Json.obj("status"->"success","message"->"Clinical Notes Migrated Successfully")}
        }
     }
  }

def basicCell(value: String, font: Font) = {
    val cell = new PdfPCell(new Phrase(value, font))
    cell.setBorder(Rectangle.BOTTOM)
    cell.setBorderColor(BaseColor.LIGHT_GRAY)
    cell.setPadding(10)
    cell
  }

def updatePdfImages(phrase: Phrase, vimgs: ListBuffer[String]) = {
    val imgLinkFont = FontFactory.getFont(FontFactory.HELVETICA, 13)
    imgLinkFont.setColor(BaseColor.BLUE)
    if(vimgs.size>0){
      for(i<-0 to vimgs.size-1){
        val img = vimgs(i)
           var fileName = img.substring(img.lastIndexOf('/') + 1)
           if(fileName.length>10){
            fileName = fileName.substring(0,10) + "... " + fileName.substring(fileName.lastIndexOf('.'))
           }
          val chunk: Chunk = new Chunk(fileName,imgLinkFont)
          chunk.setAnchor(img)
        chunk.setUnderline(0.1f, -2f)
          if(i==0){
            phrase.add(chunk)
          }else{
            phrase.add(", ")
            phrase.add(chunk)
          }
       
      }
    }
    phrase
  }


   class HeaderFooterPageEvent extends PdfPageEventHelper  {
    override def onEndPage(writer: PdfWriter, document: Document): Unit = {
      val font = FontFactory.getFont(FontFactory.HELVETICA, 11)
      font.setColor(BaseColor.LIGHT_GRAY)
      ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("www.novadontics.com", font), 435, 30, 0)
    }
  }
   

   def pdf(id: Long) = silhouette.SecuredAction.async { implicit request =>

    db.run {
      ClinicalNotesTable.filter(_.id === id).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case None =>
          DBIO.successful(NotFound)
        case Some(clinic) =>
            var assisFor1 = 0L
            var provider1 = 0L
            val assistantOne = convertOptionString(clinic.assistant1)
            if(assistantOne != null && assistantOne != None && assistantOne != ""){
              if(assistantOne.contains("S")){
              assisFor1  = assistantOne.substring(2,assistantOne.length).toLong
              }
              else{
                provider1  = assistantOne.substring(2,assistantOne.length).toLong
              }
            }
            var assisFor2 = 0L
            var provider2 = 0L
            val assistantTwo = convertOptionString(clinic.assistant2)
            if(assistantTwo != null && assistantTwo != None && assistantTwo != ""){
              if(assistantTwo.contains("S")){
              assisFor2 = assistantTwo.substring(2,assistantTwo.length).toLong
              }
              else{
                provider2  = assistantTwo.substring(2,assistantTwo.length).toLong
              }
            }
            var assisFor3 = 0L
            var provider3 = 0L
            val assistantThree = convertOptionString(clinic.assistant3)
            if(assistantThree != null && assistantThree != None && assistantThree != ""){
              if(assistantThree.contains("S")){
              assisFor3 = assistantThree.substring(2,assistantThree.length).toLong
              }
              else{
                provider3  = assistantThree.substring(2,assistantThree.length).toLong
              }
            }
            for {
              dentist <- StaffTable.filter(_.id === clinic.staffId).result.head
              treatmenttable <- TreatmentTable.filter(_.id === clinic.patientId).result
              staff <- ProviderTable.filter(_.id === clinic.provider).result
              staffForAssis1 <- ProviderTable.filter(_.id === provider1).result
              staffForAssis2 <- ProviderTable.filter(_.id === provider2).result
              staffForAssis3 <- ProviderTable.filter(_.id === provider3).result
              assis1 <- StaffMembersTable.filter(_.id === assisFor1).result
              assis2 <- StaffMembersTable.filter(_.id === assisFor2).result
              assis3 <- StaffMembersTable.filter(_.id === assisFor3).result
              implantDatas <- ImplantDataTable.filter(_.noteId === id).result
            } yield {

              val source = StreamConverters.fromInputStream { () =>
                val table_notes = new PdfPTable(1)
                var columnWidths:Array[Float] = Array(55,15,15,15);
                var table_measurement: PdfPTable = new PdfPTable(columnWidths);
                table_measurement.setWidthPercentage(100);
                val document = new Document(PageSize.A3)
                val inputStream = new PipedInputStream()
                val outputStream = new PipedOutputStream(inputStream)
                val writer = PdfWriter.getInstance(document, outputStream)

                writer.setStrictImageSequence(true)
                val event = new HeaderFooterPageEvent()
                writer.setPageEvent(event)
                document.setMargins(50, 45, 50, 60)
                Future({
                  document.open()

                  // Metadata
                  val FONT_COVER_PAGE = FontFactory.getFont(FontFactory.HELVETICA, 29)
                  val FONT_H1 = FontFactory.getFont(FontFactory.HELVETICA, 23)
                  val FONT_H2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15)
                  val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 13)
                  val FONT_BODY_LINK = FontFactory.getFont(FontFactory.HELVETICA, 13)
                  val FONT_H2_TABLE = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 13)
                  val FONT_SMALL = FontFactory.getFont(FontFactory.HELVETICA, 10)
                  val FONT_BODY_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 13)
                  val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)

                  FONT_CONTENT.setColor(new BaseColor(64,64,65));
                  FONT_BODY_LINK.setColor(BaseColor.BLUE)
                  FONT_BODY_TITLE.setColor(BaseColor.GRAY)

                  // Logo
                  try {
                    val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
                    val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
                    val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
                    val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
                    val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
                    val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
                    val g = img.createGraphics()
                    g.drawRenderedImage(bi, at)
                    val imgBytes = new ByteArrayOutputStream()
                    ImageIO.write(img, "PNG", imgBytes)
                    val image = Image.getInstance(imgBytes.toByteArray)
                    image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
                    image.setCompressionLevel(PdfStream.NO_COMPRESSION)
                    image.setAlignment(Image.MIDDLE)
                    document.add(image)
                  } catch {
                    case error: Throwable => logger.debug("error = ")
                  }
                  var assist1 = ""
                  var assist2 = ""
                  var assist3 = ""
                  if(assis1.lift(0) != null && assis1.lift(0) != None && assis1.lift(0) != ""){
                  assist1 = assis1.lift(0).get.firstName +" "+ assis1.lift(0).get.lastName
                  }
                  if(assis2.lift(0) != null && assis2.lift(0) != None && assis2.lift(0) != ""){
                  assist2 = assis2.lift(0).get.firstName +" "+ assis2.lift(0).get.lastName
                  }
                  if(assis3.lift(0) != null && assis3.lift(0) != None && assis3.lift(0) != ""){
                  assist3 = assis3.lift(0).get.firstName +" "+ assis3.lift(0).get.lastName
                  }
                  if(staffForAssis1.lift(0) != null && staffForAssis1.lift(0) != None && staffForAssis1.lift(0) != ""){
                  assist1 = staffForAssis1.lift(0).get.firstName +" "+ staffForAssis1.lift(0).get.lastName
                  }
                  if(staffForAssis2.lift(0) != null && staffForAssis2.lift(0) != None && staffForAssis2.lift(0) != ""){
                  assist2 = staffForAssis2.lift(0).get.firstName +" "+ staffForAssis2.lift(0).get.lastName
                  }
                  if(staffForAssis3.lift(0) != null && staffForAssis3.lift(0) != None && staffForAssis3.lift(0) != ""){
                  assist3 = staffForAssis3.lift(0).get.firstName +" "+ staffForAssis3.lift(0).get.lastName
                  }
                  val inputTimeFormat = DateTimeFormat.forPattern("hh:mm a")
                  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
                  var staffmem = staff.lift(0)
                  var staffMember = staffmem.get
                  val staffFirstName = staffMember.firstName
                  val staffSecondName = staffMember.lastName  
                  val staffName = staff.lift(0).get.title+" "+staff.lift(0).get.firstName+" "+staff.lift(0).get.lastName               
                  
                  var patient = treatmenttable.lift(0).get
                  val patientName = convertOptionString(patient.firstName) +" "+ convertOptionString(patient.lastName)
                  val emailAddress = patient.emailAddress
                  val phoneNumber = patient.phoneNumber
                  val patientId = patient.patientId
                  val title =  "Clinical Notes Report"
                  document.addTitle(s"$patientName, $title")
                  document.addSubject(s"$title")
                  document.addAuthor(s"${dentist.name}")
                  document.addCreator("Novadontics")
                  val dateTimeFormat = DateTimeFormat.mediumDate()
                  document.add(new Paragraph(64, s"$title", FONT_COVER_PAGE))
                  document.add(new Paragraph(32, s"Patient ID: ${patientId}", FONT_BODY))
                  document.add(new Paragraph(s"Name: $patientName", FONT_BODY))
                  document.add(new Paragraph(s"Email: ${emailAddress}", FONT_BODY))
                  document.add(new Paragraph(s"Phone: ${phoneNumber}", FONT_BODY))

                  val pageWidth = PageSize.A3.getWidth.asInstanceOf[Int]

                  val pageHeight = PageSize.A3.getHeight.asInstanceOf[Int]

                      document.add(new Paragraph("\n"));
                      
                          val table: PdfPTable = new PdfPTable(4)
                          table.setPaddingTop(20)
                          table.setWidthPercentage(100)

                          table.addCell(basicCell("Date", FONT_BODY_TITLE))
                          var cell = new PdfPCell(new Phrase(dateFormatter.print(convertOptionalLocalDate(clinic.date)), FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("DOB", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(patient.dateOfBirth).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)
                          
                          table.addCell(basicCell("Time", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(inputTimeFormat.print(convertOptionalLocalTime(clinic.time)), FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Allergies", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.allergies).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Provider", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(staffName, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Medical Conditions", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.medicalConditions).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Assistant1", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(assist1, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Current Surgical & Prosthetic Treatment Plan", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.postSurgicalHistory).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)
                          
                          table.addCell(basicCell("Assistant2", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(assist2, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)
                          

                          table.addCell(basicCell("Declined Treatment Plans Offered", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.diagnosis).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Assistant3", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(assist3, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)

                          table.addCell(basicCell("Escort", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.escort).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table.addCell(cell)
                        
                          document.add(table)

                          if(clinic.procedure.get != JsNull){
                            var tempProcedure = clinic.procedure.get
                            var procedureCode = ""
                            var procedureType = ""
                            var tooth = ""
                            var temp = tempProcedure.toString.replace('★', '"')
                            var myString = temp.substring(1, temp.length()-1);
                            val jsonObject: JsValue = Json.parse(myString)
                            val procedureJson = (jsonObject \ ("procedure")).asOpt[List[JsValue]]
                            document.add(new Paragraph("\n"));
                            var rowsPerPage = 8;
                            var recNum =  0;
                            var columnWidths:Array[Float] = Array(12,14,74);
                            var table1: PdfPTable = new PdfPTable(columnWidths);
                            table1.setWidthPercentage(100);
                            table1.setPaddingTop(10);

                            if(procedureJson != None){
                            (procedureJson.get).foreach(s => {
                            var sampleData = List((s \ "tooth").asOpt[String], (s \ "procedureCode").asOpt[String], (s \ "procedureDescription").asOpt[String])
                            var i=1

                            if(recNum == 0 ) {

                              document.add(new Paragraph(32,"Procedure", FONT_H2))

                              cell = new PdfPCell(new Phrase("Tooth",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table1.addCell(cell)

                              cell = new PdfPCell(new Phrase("Procedure Code",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table1.addCell(cell)

                              cell = new PdfPCell(new Phrase("Procedure Description",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table1.addCell(cell)
                              }
                              (sampleData).foreach(item => {
                                cell = new PdfPCell(new Phrase(convertOptionString(item).toString,FONT_BODY));
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table1.addCell(cell);
                              })
                              recNum = recNum+1;
                              if(recNum == 0 ) {
                                document.newPage();
                              }
                              })
                              document.add(table1);
                              }
                          
                          }

                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Monitors", FONT_H2))
                          var table2_1: PdfPTable = new PdfPTable(1);
                          table2_1.setWidthPercentage(100);
                          table2_1.setPaddingTop(10);
                          cell = new PdfPCell(new Phrase(" ",FONT_BODY_TITLE))
                          if(convertOptionString(clinic.monitors) != null && convertOptionString(clinic.monitors) != None && convertOptionString(clinic.monitors) != ""){
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.monitors).toString,FONT_BODY_TITLE))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_1.addCell(cell)
                          document.add(table2_1);
                          }
                          else{
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_1.addCell(cell)
                          document.add(table2_1);
                          }
                          
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Anesthesia", FONT_H2))
                          var table2_2: PdfPTable = new PdfPTable(1);
                          table2_2.setWidthPercentage(100);
                          table2_2.setPaddingTop(10);
                          cell = new PdfPCell(new Phrase(" ",FONT_BODY_TITLE))
                          if(convertOptionString(clinic.anesthesia) != null && convertOptionString(clinic.anesthesia) != None && convertOptionString(clinic.anesthesia) != ""){
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.anesthesia).toString, FONT_BODY_TITLE))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_2.addCell(cell)
                          document.add(table2_2);
                          }
                          else{
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_2.addCell(cell)
                          document.add(table2_2);
                          }
                         
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"ASA Physical Status", FONT_H2))
                          var table2_3: PdfPTable = new PdfPTable(1);
                          table2_3.setWidthPercentage(100);
                          table2_3.setPaddingTop(10);
                          cell = new PdfPCell(new Phrase(" ",FONT_BODY_TITLE))
                          if(convertOptionString(clinic.asaStatus) != null && convertOptionString(clinic.asaStatus) != None && convertOptionString(clinic.asaStatus) != ""){
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.asaStatus).toString, FONT_BODY_TITLE))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_3.addCell(cell)
                          document.add(table2_3);
                          }
                          else{
                            cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_3.addCell(cell)
                          document.add(table2_3);
                          }

                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Airway Class", FONT_H2))
                          var table2_4: PdfPTable = new PdfPTable(1);
                          table2_4.setWidthPercentage(100);
                          table2_4.setPaddingTop(10);
                          cell = new PdfPCell(new Phrase(" ",FONT_BODY_TITLE))
                          if(convertOptionString(clinic.airwayClass) != null && convertOptionString(clinic.airwayClass) != None && convertOptionString(clinic.airwayClass) != ""){
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.airwayClass).toString, FONT_BODY_TITLE))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_4.addCell(cell)
                          document.add(table2_4);
                          }
                          else{
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table2_4.addCell(cell)
                          document.add(table2_4);
                          }
                         
                          var preSurg = 0
                          var myList = Array(clinic.bp, clinic.pulse, clinic.spco2, clinic.co2,clinic.ekg,clinic.resp,clinic.temp)
                          for(i<-0 to myList.size-1){
                            if(convertOptionString(myList(i)) != null && convertOptionString(myList(i)) != None && convertOptionString(myList(i)) != ""){
                              preSurg = 1
                            }
                          }
                          if(preSurg != 0){
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Pre-Surgical Vitals Entry", FONT_H2))
                          var table3: PdfPTable = new PdfPTable(4);
                          table3.setWidthPercentage(100);
                          table3.addCell(basicCell("B/P", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.bp).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("Pulse", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.pulse).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("SpCo2", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.spco2).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("Co2", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.co2).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("EKG", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.ekg).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("Respiration Rate", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.resp).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell("Temp", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.temp).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)

                          table3.addCell(basicCell(" ", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(" ", FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table3.addCell(cell)
                          document.add(table3);
                          }

                          
                          if(clinic.vitalsPhoto != null && clinic.vitalsPhoto != None){
                          
                            var vital_photo = new ListBuffer[String]()
                            var table4: PdfPTable = new PdfPTable(1);
                            table4.setWidthPercentage(100);
                            var vitals = convertOptionString(clinic.vitalsPhoto)
                            var temperoryVital = vitals.replace('★', '"')
                            val jsonObj: JsValue = Json.parse(temperoryVital)
                            val vitalsJson = (jsonObj \ ("images")).asOpt[List[String]]
                            if(vitalsJson.get.size != 0){
                            for(i<-0 to vitalsJson.get.size-1){
                            var a1 = vitalsJson.get(i)
                            vital_photo += a1 
                            }
                            var phrase = new Phrase(" ",FONT_BODY_TITLE);
                            updatePdfImages(phrase,vital_photo)
                            document.add(new Paragraph("\n"));
                            document.add(new Paragraph(32,"Intraoperative Vitals Record", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table4.addCell(cell)
                            
                          document.add(table4);
                            }
                          }

                            if((clinic.preMedicationTotal != null)&&(clinic.preMedicationTotal != None)){
                            var tempMedication = clinic.preMedicationTotal.get
                            var tempMedi = tempMedication.toString.replace('★', '"')
                            if(tempMedi.contains("preMedicationTotal")){
                            val jsonObjectMedi: JsValue = Json.parse(tempMedi)
                            val medicationJson = (jsonObjectMedi \ ("preMedicationTotal")).asOpt[List[JsValue]]
                            var rowsPerPage = 8;
                              var recNum =  0;
                              
                            if(medicationJson != None){
                            (medicationJson.get).foreach(s => {
                            var sampleData = List((s \ "medication").asOpt[String], (s \ "dosage").asOpt[String], (s \ "amount").asOpt[String],(s \ "IVsite").asOpt[String])
                            var i=1

                            var table5: PdfPTable = new PdfPTable(4);
                            table5.setWidthPercentage(100);

                            if(recNum == 0 ) {

                              document.add(new Paragraph("\n"));
                              document.add(new Paragraph(32,"Pre-medication Total", FONT_H2))

                              cell = new PdfPCell(new Phrase("Medication",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table5.addCell(cell)

                              cell = new PdfPCell(new Phrase("Dosage",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table5.addCell(cell)

                              cell = new PdfPCell(new Phrase("Amount",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table5.addCell(cell)

                              cell = new PdfPCell(new Phrase("Route/Technique",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table5.addCell(cell)
                              }
                              (sampleData).foreach(item => {
                                cell = new PdfPCell(new Phrase(convertOptionString(item).toString,FONT_BODY));
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table5.addCell(cell);
                              })
                              document.add(table5);
                              recNum = recNum+1;
                              if(recNum == 0 ) {
                                document.newPage();
                              }
                            })
                          }}
                        }
                       
                        var noData = 0
                         if((clinic.medicationEntry != null)&&(clinic.medicationEntry != None)){
                            var tempMediEntry = clinic.medicationEntry.get
                              var tempMedi = tempMediEntry.toString.replace('★', '"')
                              val jsonObjectMedi: JsValue = Json.parse(tempMedi)
                              val mediEntryJson = (jsonObjectMedi \ ("medicationEntry")).asOpt[List[JsValue]]
                              var rowsPerPage = 8;
                              var recNum =  0;
                              
                            if(mediEntryJson != None){
                            (mediEntryJson.get).foreach(s => {
                            var sampleData = List((s \ "oralMedication").asOpt[String], (s \ "oralDosage").asOpt[String], (s \ "oralAmount").asOpt[String],(s \ "oralIVsite").asOpt[String])
                            if(sampleData == List(None,None,None,None)){
                              sampleData = List((s \ "medication").asOpt[String], (s \ "dosage").asOpt[String], (s \ "amount").asOpt[String],(s \ "IVsite").asOpt[String])
                            }
                            var i=1
                            noData = 1
                            var table6: PdfPTable = new PdfPTable(4);
                            table6.setWidthPercentage(100);

                            if(recNum == 0 ) {

                              document.add(new Paragraph("\n"));
                              document.add(new Paragraph(32,"Intraoperative Oral, IV and Local Anesthetic Total", FONT_H2))

                              cell = new PdfPCell(new Phrase("Medication",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table6.addCell(cell)

                              cell = new PdfPCell(new Phrase("Dosage",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table6.addCell(cell)

                              cell = new PdfPCell(new Phrase("Amount",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table6.addCell(cell)

                              cell = new PdfPCell(new Phrase("Route/Technique",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table6.addCell(cell)
                              }
                              (sampleData).foreach(item => {
                                cell = new PdfPCell(new Phrase(convertOptionString(item).toString,FONT_BODY));
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table6.addCell(cell);
                              })
                              document.add(table6);
                              recNum = recNum+1;
                              if(recNum == 0 ) {
                                document.newPage();
                              }
                            })
                          }
                        }
                        if(noData == 0){
                          var table6: PdfPTable = new PdfPTable(4);
                          table6.setWidthPercentage(100);

                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Intraoperative Oral, IV and Local Anesthetic Total", FONT_H2))

                          cell = new PdfPCell(new Phrase("Medication",  FONT_BODY_TITLE));
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table6.addCell(cell)

                          cell = new PdfPCell(new Phrase("Dosage",  FONT_BODY_TITLE));
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table6.addCell(cell)

                          cell = new PdfPCell(new Phrase("Amount",  FONT_BODY_TITLE));
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table6.addCell(cell)

                          cell = new PdfPCell(new Phrase("Route/Technique",  FONT_BODY_TITLE));
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table6.addCell(cell)

                          for(i<-0 to 4){
                          cell = new PdfPCell(new Phrase(" ",FONT_BODY));
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table6.addCell(cell);
                          }
                          document.add(table6);
                          
                        }
                        
                         var incision = 0
                        var myList1 = Array(clinic.incision, clinic.incisionMandible)
                        for(i<-0 to myList1.size-1){
                            if(convertOptionString(myList1(i)) != null && convertOptionString(myList1(i)) != None && convertOptionString(myList1(i)) != ""){
                              incision = 1
                            }
                          }
                        if(incision != 0){
                        var table7: PdfPTable = new PdfPTable(4);
                        table7.setWidthPercentage(100);
                        document.add(new Paragraph("\n"));
                          
                          document.add(new Paragraph(32,"Incision", FONT_H2))
                          table7.addCell(basicCell("Incision / Maxilla", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.incision).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table7.addCell(cell)

                          table7.addCell(basicCell("Incision / Mandible", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.incisionMandible).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table7.addCell(cell)
                          
                          document.add(table7)
                        }
                        if(implantDatas != null && implantDatas != None){
                              var rowsPerPage = 8;
                               var recNum =  0;
                              
                              (implantDatas).foreach(row => {
                            var medicationStableData = List(s"${row.implantSite}",s"${row.implantBrand}", s"${row.implantSurface}",s"${row.implantReference}",s"${row.implantLot}",s"${row.diameter}",s"${row.length}",s"${row.surgProtocol}",s"${row.boneDensity}")
                            
                            var i=1

                            var table8: PdfPTable = new PdfPTable(9);
                            table8.setWidthPercentage(100);

                            if(recNum == 0 ) {

                              document.add(new Paragraph("\n"));
                              document.add(new Paragraph(32,"Implant Data", FONT_H2))

                              cell = new PdfPCell(new Phrase("Implant Site",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Implant Brand",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Implant Surface",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Implant Reference",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Implant Lot",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Diameter",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                               cell = new PdfPCell(new Phrase("Length",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                              cell = new PdfPCell(new Phrase("Surgical Protocol",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)

                               cell = new PdfPCell(new Phrase("Bone Density",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table8.addCell(cell)
                              }
                              (medicationStableData).foreach(item => {
                                cell = new PdfPCell(new Phrase(item,FONT_BODY));
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table8.addCell(cell);
                              })
                              document.add(table8);
                              recNum = recNum+1;
                              if(recNum == 0 ) {
                                document.newPage();
                              }
                            })
                              
                             rowsPerPage = 8;
                                recNum =  0;
                                
                            (implantDatas).foreach(row => {
                             
                             var medicationStableData = List(row.osteotomySite,row.ncm, row.isqBl,row.isqMd,row.implantDepth,row.loadingProtocol,row.recommendedHT,row.finalPA)
                            
                            var i=1

                            var table9: PdfPTable = new PdfPTable(8);
                            table9.setWidthPercentage(100);

                            if(recNum == 0 ) {

                              document.add(new Paragraph("\n"));

                              cell = new PdfPCell(new Phrase("Osteotomy Site",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Initial Ncm",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Initial ISQ / BL",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Initial ISQ / MD",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Implant Depth",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Loading Protocol",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("RRPSD",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)

                              cell = new PdfPCell(new Phrase("Final PA",  FONT_BODY_TITLE));
                              cell.setBorder(Rectangle.BOTTOM)
                              cell.setBorderColor(BaseColor.LIGHT_GRAY)
                              cell.setPadding(10)
                              table9.addCell(cell)
                              }
                              for(i<-0 to medicationStableData.size-1){
                          
                                if(i != medicationStableData.size-1){
                                  cell = new PdfPCell(new Phrase(medicationStableData(i),FONT_BODY));
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table9.addCell(cell);
                                }
                              
                            else{
                                var phrase = new Phrase("",FONT_BODY_LINK);
                                if(medicationStableData(i) != null && medicationStableData(i) != None && medicationStableData(i) != ""){
                                var fileName = medicationStableData(i).substring(medicationStableData(i).lastIndexOf('/') + 1)
                                if(fileName.length>10){
                                fileName = fileName.substring(0,10) + "... " + fileName.substring(fileName.lastIndexOf('.'))
                                }
                                var chunk: Chunk = new Chunk(fileName,FONT_BODY_LINK)
                                chunk.setAnchor(medicationStableData(i));
                                chunk.setUnderline(0.1f, -2f)
                                phrase.add(chunk);
                                }
                                cell = new PdfPCell(phrase);
                                cell.setBorder(Rectangle.BOTTOM)
                                cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                cell.setPadding(10)
                                table9.addCell(cell);
                                }
                              }
                              document.add(table9);
                              recNum = recNum+1;
                              if(recNum == 0 ) {
                                document.newPage();
                              }
                            })
                          
                        }
                        

                          var table10_1: PdfPTable = new PdfPTable(1);
                          table10_1.setWidthPercentage(100);
                          table10_1.setPaddingTop(10);
                          if(convertOptionString(clinic.boneGraftMaterials) != None && convertOptionString(clinic.boneGraftMaterials) != null && convertOptionString(clinic.boneGraftMaterials) != ""){
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Bone Graft Material(s) Used", FONT_H2))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.boneGraftMaterials).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_1.addCell(cell)
                          document.add(table10_1)
                          }

                          var table10_2: PdfPTable = new PdfPTable(1);
                          table10_2.setWidthPercentage(100);
                          if(convertOptionString(clinic.membranesUsed) != None && convertOptionString(clinic.membranesUsed) != null && convertOptionString(clinic.membranesUsed) != ""){
                           document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Membrane(s) Used", FONT_H2))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.membranesUsed).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_2.addCell(cell)
                          document.add(table10_2)
                           }

                          var table10_3: PdfPTable = new PdfPTable(1);
                          table10_3.setWidthPercentage(100);
                          if(convertOptionString(clinic.fixationSystemUsed) != None && convertOptionString(clinic.fixationSystemUsed) != null && convertOptionString(clinic.fixationSystemUsed) != ""){
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Screw System Used", FONT_H2))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.fixationSystemUsed).toString, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_3.addCell(cell)
                          document.add(table10_3)
                          }
                          
                          var table10_4: PdfPTable = new PdfPTable(1);
                          table10_4.setWidthPercentage(100);
                          var surgery_photo = new ListBuffer[String]()
                          if(clinic.surgeryPhoto != null && clinic.surgeryPhoto != None){
                            
                            var surgery = convertOptionString(clinic.surgeryPhoto)
                            var temperoryVital = surgery.replace('★', '"')
                            val jsonObj: JsValue = Json.parse(temperoryVital)
                            val surgeryJson = (jsonObj \ ("images")).asOpt[List[String]]
                            var phrase = new Phrase(" ",FONT_BODY);
                            if(surgeryJson.get.size != 0){
                            for(i<-0 to surgeryJson.get.size-1){
                            var a1 = surgeryJson.get(i)
                            surgery_photo += a1 
                           } 
                            phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,surgery_photo)
                            document.add(new Paragraph("\n"));
                            document.add(new Paragraph(32,"Pre, During, Post Surgical Photos / Videos", FONT_H2))
                             
                          cell = new PdfPCell(phrase)
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_4.addCell(cell)
                          document.add(table10_4)
                          }
                          }
                          

                          var table10_5: PdfPTable = new PdfPTable(1);
                          table10_5.setWidthPercentage(100);
                          
                          if(convertOptionString(clinic.prosthesesPhoto) != null && convertOptionString(clinic.prosthesesPhoto) != None && convertOptionString(clinic.prosthesesPhoto) != ""){
                            var prosthes_photo = new ListBuffer[String]()
                            var prosthes = convertOptionString(clinic.prosthesesPhoto)
                            var tempProsthes = prosthes.replace('★', '"')
                            val jsonObj: JsValue = Json.parse(tempProsthes)
                            val prosthesJson = (jsonObj \ ("images")).asOpt[List[String]]
                            if(prosthesJson.get.size != 0){
                            for(i<-0 to prosthesJson.get.size-1){
                            var a1 = prosthesJson.get(i)
                            prosthes_photo += a1 
                           } 
                            var phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,prosthes_photo)
                             document.add(new Paragraph("\n"));
                            document.add(new Paragraph(32,"Temporary and Definitive Prostheses Photos / Videos", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table10_5.addCell(cell)
                            document.add(table10_5)
                            }
                          }
                          
                          
                          var table10_6: PdfPTable = new PdfPTable(1);
                          table10_6.setWidthPercentage(100);
                          table10_6.setPaddingTop(20);
                          
                          if(clinic.lapsPhoto != null && clinic.lapsPhoto != None){
                            var product_photo = new ListBuffer[String]()
                               var surgery = convertOptionString(clinic.lapsPhoto)
                            var temperoryProduct = surgery.replace('★', '"')
                            val jsonObj: JsValue = Json.parse(temperoryProduct)
                            val productJson = (jsonObj \ ("images")).asOpt[List[String]]
                            if(productJson.get.size != 0){
                            for(i<-0 to productJson.get.size-1){
                            var a1 = productJson.get(i)
                            product_photo += a1 
                           } 
                            var phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,product_photo)
                            document.add(new Paragraph("\n"));
                            document.add(new Paragraph(32,"Products Used", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table10_6.addCell(cell)
                            document.add(table10_6)
                            }
                          }
                          

                          var table10_7: PdfPTable = new PdfPTable(1);
                          table10_7.setWidthPercentage(100);
                          
                          var impressVal = "No"
                          if((clinic.impression!= null)&&(clinic.impression!=None)){
                          var impress = convertOptionString(clinic.impression)
                          var temperoryimpress = impress.replace('★', '"')
                          val jsonObj = Json.parse(temperoryimpress)
                          val impressJson  = convertOptionalBoolean((jsonObj \ "checked").asOpt[Boolean] )
                          if(impressJson){
                            impressVal = "Yes"
                          }
                          }

                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Impressions", FONT_H2))
                          cell = new PdfPCell(new Phrase(impressVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_7.addCell(cell)
                          document.add(table10_7)

                          var table10_8: PdfPTable = new PdfPTable(1);
                          table10_8.setWidthPercentage(100);
                          
                          var registerVal = "No"
                          if((clinic.brCompleted!= null)&&(clinic.brCompleted!=None)){
                          var register = convertOptionString(clinic.brCompleted)
                          var temperoryregister = register.replace('★', '"')
                          val jsonObj1 = Json.parse(temperoryregister)
                          val registerJson  = convertOptionalBoolean((jsonObj1 \ "checked").asOpt[Boolean] )
                          if(registerJson){
                            registerVal = "Yes"
                          }
                          }
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Bite Registration", FONT_H2))
                          cell = new PdfPCell(new Phrase(registerVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table10_8.addCell(cell)

                          document.add(table10_8)

                            var table11: PdfPTable = new PdfPTable(4);
                            table11.setWidthPercentage(100);
                            document.add(new Paragraph("\n"));
                            document.add(new Paragraph(32,"Occlusion Type", FONT_H2))
                            
                            var balancedVal = "No"
                            if((clinic.balancedOcclusion!= null)&&(clinic.balancedOcclusion!=None)){
                            var balanced = convertOptionString(clinic.balancedOcclusion)
                            var temperoryBalanced = balanced.replace('★', '"')
                            val jsonObjBalance = Json.parse(temperoryBalanced)
                            val balancedJson  = convertOptionalBoolean((jsonObjBalance \ "checked").asOpt[Boolean] )
                            if(balancedJson){
                              balancedVal = "Yes"
                            }
                              }
                          table11.addCell(basicCell("Balanced Occlusion", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(balancedVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table11.addCell(cell)

                          var anteriorVal = "No"
                          if((clinic.anteriorGuidance!= null)&&(clinic.anteriorGuidance!=None)){
                          var anterior = convertOptionString(clinic.anteriorGuidance)
                          var temperoryAnterior = anterior.replace('★', '"')
                          val jsonObjAnterior = Json.parse(temperoryAnterior)
                          val anteriorJson  = convertOptionalBoolean((jsonObjAnterior \ "checked").asOpt[Boolean] )
                          if(anteriorJson){
                              anteriorVal = "Yes"
                            }
                          }
                           table11.addCell(basicCell("Anterior Guidance", FONT_BODY_TITLE))
                           cell = new PdfPCell(new Phrase(anteriorVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table11.addCell(cell)

                           var canineVal = "No"
                           if((clinic.canineGuidance!=null)&&(clinic.canineGuidance!=None)){
                           var canine = convertOptionString(clinic.canineGuidance)
                           var temperoryCanine = canine.replace('★', '"')
                           val jsonObjCanine = Json.parse(temperoryCanine)
                           val canineJson  = convertOptionalBoolean((jsonObjCanine \ "checked").asOpt[Boolean] )
                            if(canineJson){
                              canineVal = "Yes"
                            }
                           }
                           table11.addCell(basicCell("Canine Guidance", FONT_BODY_TITLE))
                           cell = new PdfPCell(new Phrase(canineVal, FONT_BODY))
                           cell.setBorder(Rectangle.BOTTOM)
                           cell.setBorderColor(BaseColor.LIGHT_GRAY)
                           cell.setPadding(10)
                           table11.addCell(cell)

                          table11.addCell(basicCell(" ", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(" ", FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table11.addCell(cell)

                          document.add(table11);

                          var table12: PdfPTable = new PdfPTable(4);
                          table12.setWidthPercentage(100);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Pickup and/or Delivery", FONT_H2))

                          var fDentureVal = "No"
                          if((clinic.fullDenture!=null)&&(clinic.fullDenture!=None)){
                          var fDenture = convertOptionString(clinic.fullDenture)
                          var temperoryFull = fDenture.replace('★', '"')
                          val jsonObjF = Json.parse(temperoryFull)
                          val fDentureJson  = convertOptionalBoolean((jsonObjF \ "checked").asOpt[Boolean] )
                          if(fDentureJson){
                            fDentureVal = "Yes"
                          }
                          }
                          table12.addCell(basicCell("Delivery of removable full denture", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(fDentureVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table12.addCell(cell)

                          var eDentureVal = "No"
                          if((clinic.existingDenture!=null)&&(clinic.existingDenture!=None)){
                          var eDenture = convertOptionString(clinic.existingDenture)
                          var temperoryExisting = eDenture.replace('★', '"')
                          val jsonObjE = Json.parse(temperoryExisting)
                          val eDentureJson  = convertOptionalBoolean((jsonObjE \ "checked").asOpt[Boolean] )
                          if(eDentureJson){
                              eDentureVal = "Yes"
                            }
                           }
                          table12.addCell(basicCell("Screw-retained converted dentures (direct method)", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(eDentureVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table12.addCell(cell)

                            var pmmaVal = "No"
                            if((clinic.pmmaProsthesis!=null)&&(clinic.pmmaProsthesis!=None)){
                            var pmma = convertOptionString(clinic.pmmaProsthesis)
                            var temperoryPmma = pmma.replace('★', '"')
                            val jsonObjPmma = Json.parse(temperoryPmma)
                            val pmmaJson  = convertOptionalBoolean((jsonObjPmma \ "checked").asOpt[Boolean] )
                            if(pmmaJson){
                              pmmaVal = "Yes"
                            }
                           }
                          table12.addCell(basicCell("Screw-retained CAD/CAM PMMA prosthesis (indirect method)", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(pmmaVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table12.addCell(cell)

                          var compositeVal = "No"
                          if((clinic.compositeProsthesis!=null)&&(clinic.compositeProsthesis!=None)){
                          var composite = convertOptionString(clinic.compositeProsthesis)
                          var temperoryComposite = composite.replace('★', '"')
                          val jsonObjComposite = Json.parse(temperoryComposite)
                          val compositeJson  = convertOptionalBoolean((jsonObjComposite \ "checked").asOpt[Boolean] )
                          if(compositeJson){
                            compositeVal = "Yes"
                          }
                        }
                          table12.addCell(basicCell("Screw-retained CAD/CAM composite prosthesis (indirect method)", FONT_BODY_TITLE))
                          cell = new PdfPCell(new Phrase(compositeVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table12.addCell(cell)
                          document.add(table12);

                          if(convertOptionString(clinic.suturingMaterials)!= null && convertOptionString(clinic.suturingMaterials) != None && convertOptionString(clinic.suturingMaterials) != ""){
                          var table13_1: PdfPTable = new PdfPTable(1);
                          table13_1.setWidthPercentage(100);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Suturing Materials", FONT_H2))
                          
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.suturingMaterials), FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_1.addCell(cell)
                          document.add(table13_1);
                          }
                          
                          if(convertOptionString(clinic.suturingTechniques)!= null && convertOptionString(clinic.suturingTechniques) != None && convertOptionString(clinic.suturingTechniques) != ""){
                          var table13_2: PdfPTable = new PdfPTable(1);
                          table13_2.setWidthPercentage(100);
                          table13_2.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Suturing Techniques", FONT_H2))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.suturingTechniques), FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_2.addCell(cell)
                          document.add(table13_2);
                           }
                          
                          if(convertOptionString(clinic.patientTolerate)!= null && convertOptionString(clinic.patientTolerate) != None && convertOptionString(clinic.patientTolerate) != ""){
                          var table13_3: PdfPTable = new PdfPTable(1);
                          table13_3.setWidthPercentage(100);
                          table13_3.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Did the patient tolerate the procedure well?", FONT_H2))
                          
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.patientTolerate), FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_3.addCell(cell)
                          document.add(table13_3);
                          }
                          
                          if(convertOptionString(clinic.reasonForNextAppointment)!= null && convertOptionString(clinic.reasonForNextAppointment) != None && convertOptionString(clinic.reasonForNextAppointment) != ""){
                            var table13_4: PdfPTable = new PdfPTable(1);
                          table13_4.setWidthPercentage(100);
                          table13_4.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Reason For Next Appointment", FONT_H2))
                          cell = new PdfPCell(new Phrase(convertOptionString(clinic.reasonForNextAppointment), FONT_BODY))
                          
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_4.addCell(cell)
                          document.add(table13_4);
                          }
                          
                          var table13_5: PdfPTable = new PdfPTable(1);
                          table13_5.setWidthPercentage(100);
                          table13_5.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          var verbalVal = "No"
                          if((clinic.verbalPostopIns!=null)&&(clinic.verbalPostopIns!=None)){
                          var verbal = convertOptionString(clinic.verbalPostopIns)
                          var tempeVerbal = verbal.replace('★', '"')
                          if(verbal.contains("checked")){
                          val jsonObjVerbal = Json.parse(tempeVerbal)
                          val verbalJson  = convertOptionalBoolean((jsonObjVerbal \ "checked").asOpt[Boolean] )
                            
                            if(verbalJson){
                              verbalVal = "Yes"
                            }
                           
                          }
                          }
                          document.add(new Paragraph(32,"Verbal Postop Instructions are given", FONT_H2))
                          
                          cell = new PdfPCell(new Phrase(verbalVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_5.addCell(cell)
                          document.add(table13_5);

                          var table13_6: PdfPTable = new PdfPTable(1);
                          table13_6.setWidthPercentage(100);
                          table13_6.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                           var writtenVal = "No"
                           if((clinic.writtenPostopIns!=null)&&(clinic.writtenPostopIns!=None)){
                           var written = convertOptionString(clinic.writtenPostopIns)
                           var tempeWritten = written.replace('★', '"')
                            if(written.contains("checked")){
                            val jsonObjWritten = Json.parse(tempeWritten)
                            val writtenJson  = convertOptionalBoolean((jsonObjWritten \ "checked").asOpt[Boolean] )
                            if(writtenJson){
                              writtenVal = "Yes"
                           }
                          }
                          }
                          document.add(new Paragraph(32,"Written Postop Instructions are given", FONT_H2))
                          cell = new PdfPCell(new Phrase(writtenVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_6.addCell(cell)
                          document.add(table13_6);

                          var table13_7: PdfPTable = new PdfPTable(1);                         
                          table13_7.setWidthPercentage(100);
                          table13_7.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          var rxVal = "No"
                          if((clinic.rxVisit!=null)&&(clinic.rxVisit!=None)){
                          var rx = convertOptionString(clinic.rxVisit)
                          var tempeVisit = rx.replace('★', '"')
                             if(rx.contains("checked")){
                            val jsonObjVisite = Json.parse(tempeVisit)
                            val rxJson  = convertOptionalBoolean((jsonObjVisite \ "checked").asOpt[Boolean] )
                            if(rxJson){
                              rxVal = "Yes"
                            }
                          }
                          }
                          document.add(new Paragraph(32,"Rx is given on this visit", FONT_H2))
                          cell = new PdfPCell(new Phrase(rxVal, FONT_BODY))
                          cell.setBorder(Rectangle.BOTTOM)
                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                          cell.setPadding(10)
                          table13_7.addCell(cell)
                          document.add(table13_7);
                           
                          var table14: PdfPTable = new PdfPTable(1);
                          table14.setWidthPercentage(100);
                          table14.setPaddingTop(10);
                          document.add(new Paragraph("\n"));
                          document.add(new Paragraph(32,"Notes", FONT_H2))
                          if(convertOptionString(clinic.note)!= null && convertOptionString(clinic.note )!= None && convertOptionString(clinic.note) != ""){
                            cell = new PdfPCell(new Phrase(convertOptionString(clinic.note), FONT_BODY))
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table14.addCell(cell)
                            document.add(table14)
                          }
                          else{
                            cell = new PdfPCell(new Phrase(" ", FONT_BODY))
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table14.addCell(cell)
                            document.add(table14)
                          }

                            var table14_2: PdfPTable = new PdfPTable(1);
                            table14_2.setWidthPercentage(100);
                            table14_2.setPaddingTop(10);
                            if((convertOptionString(clinic.voiceMemo) != null) && (convertOptionString(clinic.voiceMemo) != "") && (convertOptionString(clinic.voiceMemo) != None)){
                            var voice_memo = new ListBuffer[String]()
                            var voice = convertOptionString(clinic.voiceMemo)
                            var tempVoice = voice.replace('★', '"')
                            val jsonObjVoice: JsValue = Json.parse(tempVoice)
                            if(tempVoice.contains("images")){
                            var voiceJson = (jsonObjVoice \ ("images")).asOpt[List[String]]
                            if(voiceJson.get.size != 0){
                            for(i<-0 to voiceJson.get.size-1){
                            var a1 = voiceJson.get(i)
                            voice_memo += a1
                            }
                            var phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,voice_memo)
                            document.add(new Paragraph(32,"Voice Memo", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table14_2.addCell(cell)
                            }
                            }
                            else if(tempVoice.contains("videos")){
                            var voice_memo1 = new ListBuffer[String]()
                            val voiceJson1 = (jsonObjVoice \ ("videos")).asOpt[List[JsValue]]
                            if(voiceJson1.get.size != 0){
                            for(i<-0 to voiceJson1.get.size-1){
                            var a1 = (voiceJson1.get(i)\("url")).asOpt[String]
                            voice_memo1 +=  convertOptionString(a1)
                            }
                            var phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,voice_memo1)
                            document.add(new Paragraph(32,"Voice Memo", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table14_2.addCell(cell)
                            }
                          }
                          else if(tempVoice.contains("audios")){
                            var voice_memo2 = new ListBuffer[String]()
                            val voiceJson2 = (jsonObjVoice \ ("audios")).asOpt[List[JsValue]]
                            if(voiceJson2.get.size != 0){
                            for(i<-0 to voiceJson2.get.size-1){
                            var a1 = (voiceJson2.get(i)\("url")).asOpt[String]
                            voice_memo2 +=  convertOptionString(a1)
                            }
                            var phrase = new Phrase("",FONT_BODY);
                            updatePdfImages(phrase,voice_memo2)
                            document.add(new Paragraph(32,"Voice Memo", FONT_H2))
                            cell = new PdfPCell(phrase)
                            cell.setBorder(Rectangle.BOTTOM)
                            cell.setBorderColor(BaseColor.LIGHT_GRAY)
                            cell.setPadding(10)
                            table14_2.addCell(cell)
                            }
                          }
                        }
                          document.add(table14_2)
                  document.close()
                  logger.debug("Done rendering")
                  
                })(ioBoundExecutor)

                inputStream
              }
              var patient = treatmenttable.lift(0).get
              val emailAddress = patient.emailAddress
              val first_Name = patient.firstName
              val filename = first_Name.getOrElse(emailAddress.replace("@", " at "))
              val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

              Result(
                header = ResponseHeader(200, Map.empty),
                body = HttpEntity.Streamed(source, None, Some("application/pdf"))
              ).withHeaders(
                "Content-Disposition" -> contentDisposition
              )
            }
      }
    }
  }

def implantCreate = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var staffId = request.identity.id.get
  val implants = for {
  implantSite <- (request.body \ "implantSite").validateOpt[String]
  implantBrand <- (request.body \ "implantBrand").validateOpt[String]
  implantSurface <- (request.body \ "implantSurface").validateOpt[String]
  implantReference <- (request.body \ "implantReference").validateOpt[String]
  implantLot <- (request.body \ "implantLot").validateOpt[String]
  diameter <- (request.body \ "diameter").validateOpt[String]
  length <- (request.body \ "length").validateOpt[String]
  surgProtocol <- (request.body \ "surgProtocol").validateOpt[String]
  boneDensity <- (request.body \ "boneDensity").validateOpt[String]
  osteotomySite <- (request.body \ "osteotomySite").validateOpt[String]
  ncm <- (request.body \ "ncm").validateOpt[String]
  isqBl <- (request.body \ "isqBl").validateOpt[String]
  isqMd <- (request.body \ "isqMd").validateOpt[String]
  implantDepth <- (request.body \ "implantDepth").validateOpt[String]
  loadingProtocol <- (request.body \ "loadingProtocol").validateOpt[String]
  dateOfRemoval <- (request.body \ "dateOfRemoval").validateOpt[String]
  reasonForRemoval <- (request.body \ "reasonForRemoval").validateOpt[String]
  finalPA <- (request.body \ "finalPA").validateOpt[String]
  recommendedHT <- (request.body \ "recommendedHT").validateOpt[String]
  implantRestored <- (request.body \ "implantRestored").validateOpt[String]
  patientName <- (request.body \ "patientName").validate[String]
  providerName <- (request.body \ "providerName").validate[String]
  date <- (request.body \ "date").validate[LocalDate]
  dob <- (request.body \ "dob").validateOpt[LocalDate]
  } yield {
    ImplantDataRow(None,0,convertOptionString(implantSite),convertOptionString(implantBrand),convertOptionString(implantSurface),convertOptionString(implantReference),convertOptionString(implantLot),convertOptionString(diameter),convertOptionString(length),convertOptionString(surgProtocol),convertOptionString(boneDensity),convertOptionString(osteotomySite),convertOptionString(ncm),convertOptionString(isqBl),convertOptionString(isqMd),convertOptionString(implantDepth),convertOptionString(loadingProtocol),convertOptionString(dateOfRemoval),convertOptionString(reasonForRemoval),convertOptionString(finalPA),convertOptionString(recommendedHT),convertOptionString(implantRestored),Some(patientName),Some(providerName),Some(date),Some(practiceId),Some(staffId),dob)
  }
    implants match {
      case JsSuccess(implantDataRow, _) =>
      db.run{
       ImplantDataTable.returning(ImplantDataTable.map(_.id)) += implantDataRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(implantDataRow.copy(id = Some(id))))
            }

      case JsError(_) => Future.successful(BadRequest)
    }

  }

 def migrateImplantPraticeStaff = silhouette.SecuredAction.async(parse.json) { request =>
  db.run{
    ImplantDataTable.join(ClinicalNotesTable).on(_.noteId === _.id).result
  } map {rows =>
  val data = rows.groupBy(_._1.id).map {
      case (implantId, entries) =>
      var implantData = entries.head._1
      var clinicalNotes = entries.head._2
      var block = db.run{
        StaffTable.filter(_.id === clinicalNotes.staffId).result.head
      }map { staffList =>
        val query = ImplantDataTable.filter(_.id === implantData.id)
        db.run{
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(staffList.practiceId).map(value => query.map(_.practiceId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            clinicalNotes.staffId.map(value => query.map(_.staffId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
        }
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }
  Ok { Json.obj("status"->"success","message"->"Practice and staff migrated successfully")}
  }
 }

 def cleanUpImplantDataRecommendedHT = silhouette.SecuredAction.async { request =>
  db.run{ 
    ImplantDataTable.result
  } map {rows =>
  val data = rows.map { row =>
      if(row.recommendedHT != "" && row.recommendedHT != null){
        if(row.recommendedHT.matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
        }
        else{
        
        var recommendedHTDate = new SimpleDateFormat("yyyy-MM-dd").parse(row.recommendedHT)
         var recommendedHTNew = new SimpleDateFormat("MM/dd/yyyy").format(recommendedHTDate)
        val query = ImplantDataTable.filter(_.id === row.id)
        db.run{
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(recommendedHTNew).map(value => query.map(_.recommendedHT).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
        }
      }
      }
    }
  Ok {Json.obj("status"->"success","message"->"recomendedHT Migrated successfully")}
  }
 }

 def cleanUpImplantDataDateOfRemoval = silhouette.SecuredAction.async { request =>
  db.run{ 
    ImplantDataTable.result
  } map {rows =>
  val data = rows.map { row =>
      if(row.dateOfRemoval != "" && row.dateOfRemoval != null){
        if(row.dateOfRemoval.matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")){
        }
        else{
        
        var dateOfRemovalDate = new SimpleDateFormat("yyyy-MM-dd").parse(row.dateOfRemoval)
         var dateOfRemovalNew = new SimpleDateFormat("MM/dd/yyyy").format(dateOfRemovalDate)
        val query = ImplantDataTable.filter(_.id === row.id)
        db.run{
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(dateOfRemovalNew).map(value => query.map(_.dateOfRemoval).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
        }
      
      //Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }
      }
    }
  Ok {Json.obj("status"->"success","message"->"dateOfRemoval Migrated successfully")}
  }
 }

 def implantDataPDF(fromDate: Option[String],toDate: Option[String],providerId: Option[String],implantBrand: Option[String],loadingProtocol: Option[String],implantRemoved: Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var implantObj : List[JsValue] = List()
 var successRate: Double = 0
 var filterRecords = 0
 var totalRecords = 0
 var sFromDate = convertOptionString(fromDate)
 var sToDate = convertOptionString(toDate)

 var sImplantBrand = convertOptionString(implantBrand)
 var sLoadingProtocol = convertOptionString(loadingProtocol)
 var sImplantRemoved = convertOptionString(implantRemoved)

 var clinicalQuery = ClinicalNotesTable.sortBy(_.date.desc)
 var implantQuery = ImplantDataTable.sortBy(_.date.desc)

 if(sFromDate != "" && sToDate != ""){
   clinicalQuery = clinicalQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
 }
 if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
   clinicalQuery = clinicalQuery.filter(_.provider === convertStringToLong(providerId))
 }
 if(sImplantBrand != "ALL" && sImplantBrand != ""){
   implantQuery = implantQuery.filter(_.implantBrand === sImplantBrand)
 }
 if(sLoadingProtocol != "ALL" && sLoadingProtocol != ""){
   implantQuery = implantQuery.filter(_.loadingProtocol === sLoadingProtocol)
 }
 if(sImplantRemoved != "Both" && sImplantRemoved != ""){
   if(sImplantRemoved == "yes"){
       implantQuery = implantQuery.filterNot(_.dateOfRemoval === "")
   } else if(sImplantRemoved == "no"){
       implantQuery = implantQuery.filter(_.dateOfRemoval === "")
   }
 }

      db.run{
        clinicalQuery
        .join(implantQuery).on(_.id === _.noteId).filter(_._2.practiceId === practiceId)
        .join(TreatmentTable).on(_._1.patientId === _.id).filterNot(_._2.deleted).result

      } map { clinicalNotesList =>
        val data = clinicalNotesList.groupBy(_._1._2.id).map {
        case (clinicalNotesId, entries) =>

          val clinicalNote = entries.head._1._1
          val s = entries.head._1._2
          val treatment = entries.head._2

          var patientName = convertOptionString(treatment.firstName) + " "+ convertOptionString(treatment.lastName)
          var birth_date = convertOptionString(treatment.dateOfBirth)
          var providerName = ""
          
          //GETTING TOTAL RECORDS
          totalRecords = totalRecords + 1

          //GETTING DATEOFREMOVAL NOT NULL RECORDS
          if(s.dateOfRemoval != null && s.dateOfRemoval != ""){
            filterRecords  = filterRecords + 1
          }

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var vDate: Date = simpleDateFormat.parse(convertOptionalLocalDate(clinicalNote.date).toString);
          val date = new SimpleDateFormat("mm/dd/yyyy").format(vDate)

          val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
          var dateCreated = dateFormatter.print(clinicalNote.dateCreated)

          var block = db.run{
            ProviderTable.filter(_.id === clinicalNote.provider).result
            } map { results =>
            results.map{ providers =>
            providerName = providers.title +" "+ providers.firstName +" "+ providers.lastName
            }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("patientName" -> JsString(patientName)) + ("dob" -> JsString(birth_date)) + ("providerName" -> JsString(providerName)) + ("date" -> JsString(date.toString)) + ("dateCreated" -> JsString(dateCreated.toString)) + ("implantSite" -> JsString(s.implantSite)) + ("implantBrand" -> JsString(s.implantBrand)) + ("implantSurface" -> JsString(s.implantSurface)) + ("implantReference" -> JsString(s.implantReference)) + ("implantLot" -> JsString(s.implantLot)) + ("diameter" -> JsString(s.diameter)) + ("length" -> JsString(s.length)) + ("surgProtocol" -> JsString(s.surgProtocol)) + ("boneDensity" -> JsString(s.boneDensity)) + ("osteotomySite" -> JsString(s.osteotomySite)) + ("ncm" -> JsString(s.ncm)) + ("isqBl" -> JsString(s.isqBl)) + ("isqMd" -> JsString(s.isqMd)) + ("implantDepth" -> JsString(s.implantDepth)) + ("loadingProtocol" -> JsString(s.loadingProtocol)) + ("dateOfRemoval" -> JsString(s.dateOfRemoval)) + ("reasonForRemoval" -> JsString(s.reasonForRemoval)) + ("finalPA" -> JsString(s.finalPA)) + ("recommendedHT" -> JsString(s.recommendedHT)) + ("implantRestored" -> JsString(s.implantRestored)) + ("practiceId" -> JsNumber(convertOptionLong(s.practiceId))) + ("staffId" -> JsNumber(convertOptionLong(s.staffId)))
        implantObj = implantObj :+ newObj
      }

      var sRate : Float = (((totalRecords - filterRecords).toFloat / totalRecords.toFloat) * 100)
      if(!sRate.isNaN){
          successRate =  Math.round(sRate * 100.0) / 100.0
      }

    //ADDING EMPTY NOTE ID RECORDS
    var emptyNoteId: Long = 0
    var providerWithTitle = ""
    var providerWithoutTitle = ""
    implantQuery =  implantQuery.filter(_.practiceId === practiceId)
    if(sFromDate != "" && sToDate != ""){
      implantQuery = implantQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
    }
    // if(sProviderId != 0){
    if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
      var b = db.run{
        ProviderTable.filter(_.id === convertStringToLong(providerId)).result.head
      } map { providerResult =>
          providerWithTitle = providerResult.title+" "+ providerResult.firstName+" "+ providerResult.lastName
          providerWithoutTitle = providerResult.firstName+" "+ providerResult.lastName
      }
      Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      implantQuery = implantQuery.filter(p => p.provider === providerWithTitle || p.provider === providerWithoutTitle)
    }


      val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
      var block = db.run{
        implantQuery.filter(_.noteId === emptyNoteId).result
      } map { implant =>
        implant.foreach(s => {
          var iDate: String = null
          var iDob: String = null

          if(s.date != None){
            iDate = dateFormatter.print(s.date.get)
          }
          if(s.dob != None){
            iDob = dateFormatter.print(s.dob.get)
          }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id))) + ("noteId" -> JsNumber(s.noteId)) + ("patientName" -> JsString(convertOptionString(s.patientName))) + ("dob" -> JsString(iDob)) + ("providerName" -> JsString(convertOptionString(s.provider))) + ("date" -> JsString(iDate)) + ("dateCreated" -> JsString("")) + ("implantSite" -> JsString(s.implantSite)) + ("implantBrand" -> JsString(s.implantBrand)) + ("implantSurface" -> JsString(s.implantSurface)) + ("implantReference" -> JsString(s.implantReference)) + ("implantLot" -> JsString(s.implantLot)) + ("diameter" -> JsString(s.diameter)) + ("length" -> JsString(s.length)) + ("surgProtocol" -> JsString(s.surgProtocol)) + ("boneDensity" -> JsString(s.boneDensity)) + ("osteotomySite" -> JsString(s.osteotomySite)) + ("ncm" -> JsString(s.ncm)) + ("isqBl" -> JsString(s.isqBl)) + ("isqMd" -> JsString(s.isqMd)) + ("implantDepth" -> JsString(s.implantDepth)) + ("loadingProtocol" -> JsString(s.loadingProtocol)) + ("dateOfRemoval" -> JsString(s.dateOfRemoval)) + ("reasonForRemoval" -> JsString(s.reasonForRemoval)) + ("finalPA" -> JsString(s.finalPA)) + ("recommendedHT" -> JsString(s.recommendedHT)) + ("implantRestored" -> JsString(s.implantRestored)) + ("practiceId" -> JsNumber(convertOptionLong(s.practiceId))) + ("staffId" -> JsNumber(convertOptionLong(s.staffId)))
        implantObj = implantObj :+ newObj
        })
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

       val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }
          var pageSize=new Rectangle(2400f,2400f);
          var document=new Document(pageSize);

          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setUserunit(2900f);
          writer.setPageEvent(new MyFooter())
          document.setMargins(36, 36, 55, 36)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
            val FONT_BODY_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 11)

            FONT_BODY_TITLE.setColor(BaseColor.GRAY);
            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

            var cell = new PdfPCell(new Phrase("Implant Log Report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

          document.add(new Paragraph("\n"));
          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 13)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);
          var fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
          if(sFromDate != "" && sToDate != ""){
          if(sFromDate != sToDate){
            var cell1 = new PdfPCell(new Phrase(" Implant Log From "+ fmt.print(LocalDate.parse(sFromDate)) + " To "+fmt.print(LocalDate.parse(sToDate)), FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          } else {
            var cell1 = new PdfPCell(new Phrase("Implant Log Date "+ fmt.print(LocalDate.parse(sFromDate)) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

         
          // var columnWidths:Array[Float] = Array(7,7,7,7,10,7,7,5,5,7,10,9,9,5,5,9);
          var table1: PdfPTable = new PdfPTable(24)
          table1.setWidthPercentage(100)
          var cell2 = new PdfPCell(new Phrase( "Date", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Patient Name", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "DOB", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Provider", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase("Implant Site", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant Surface", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant Reference", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant LOt", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Diameter", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Length", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Surgical Protocol", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Bone Density", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Osteotomy Site", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Initial Ncm", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Initial ISQ / BL", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Initial ISQ / MD", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant Depth", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Loading Protocol", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("RRPSD", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Final PA", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Implant Restored", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Date of Removal", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Reason for Removal", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          // }
          if(implantObj.length > 0){
          var simpleDateFormat1:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          val implantData = implantObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse
          implantData.foreach(s=>{
           var data = List((s \ "date").as[String],(s \ "patientName").as[String],(s \ "dob").as[String],(s \ "providerName").as[String],(s \ "implantSite").as[String],(s \ "implantBrand").as[String],(s \ "implantSurface").as[String],(s \ "implantReference").as[String],(s \ "implantLot").as[String],(s \ "diameter").as[String],(s \ "length").as[String],(s \ "surgProtocol").as[String],(s \ "boneDensity").as[String],(s \ "osteotomySite").as[String],(s \ "ncm").as[String],(s \ "isqBl").as[String],(s \ "isqMd").as[String],(s \ "implantDepth").as[String],(s \ "loadingProtocol").as[String],(s \ "recommendedHT").as[String],(s \ "finalPA").as[String],(s \ "implantRestored").as[String],(s \ "dateOfRemoval").as[String],(s \ "reasonForRemoval").as[String])
          data.foreach(row=>{
          cell2= new PdfPCell(new Phrase(row, FONT_BODY));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          })
          
          })
          } else {
          cell2= new PdfPCell(new Phrase("No Records Found", FONT_BODY));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          cell2.setColspan(24);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER); 
          table1.addCell(cell2);
          }
          document.add(table1);
          document.close();
          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"Implant Log Report"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
 }

 def boneGraftDataPDF(fromDate: Option[String],toDate: Option[String],providerId: Option[String]) = silhouette.SecuredAction.async { request =>
 var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 var boneGraftObj : List[JsValue] = List()
 var successRate: Double = 0
 var filterRecords = 0
 var totalRecords = 0
 var sFromDate = convertOptionString(fromDate)
 var sToDate = convertOptionString(toDate)

 var clinicalQuery = ClinicalNotesTable.sortBy(_.date.desc)
 var boneGraftQuery = BoneGraftingDataTable.sortBy(_.date.desc)

 if(sFromDate != "" && sToDate != ""){
   clinicalQuery = clinicalQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
 }
 if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
   clinicalQuery = clinicalQuery.filter(_.provider === convertStringToLong(providerId))
 }

      db.run{
        clinicalQuery
        .join(boneGraftQuery).on(_.id === _.noteId).filter(_._2.practiceId === practiceId)
        .join(TreatmentTable).on(_._1.patientId === _.id).filterNot(_._2.deleted).result

      } map { clinicalNotesList =>
        val data = clinicalNotesList.groupBy(_._1._2.id).map {
        case (clinicalNotesId, entries) =>

          val clinicalNote = entries.head._1._1
          val s = entries.head._1._2
          val treatment = entries.head._2

          var patientName = convertOptionString(treatment.firstName) + " "+ convertOptionString(treatment.lastName)
          var birth_date = convertOptionString(treatment.dateOfBirth)
          var providerName = ""

          //GETTING TOTAL RECORDS
          totalRecords = totalRecords + 1

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var vDate: Date = simpleDateFormat.parse(convertOptionalLocalDate(clinicalNote.date).toString);
          val date = new SimpleDateFormat("mm/dd/yyyy").format(vDate)

          val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
          var dateCreated = dateFormatter.print(clinicalNote.dateCreated)

          var block = db.run{
            ProviderTable.filter(_.id === clinicalNote.provider).result
            } map { results =>
            results.map{ providers =>
            providerName = providers.title +" "+ providers.firstName +" "+ providers.lastName
            }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

        var sRrpsd = ""
        if(s.rrpsd != None && s.rrpsd != null){
          sRrpsd = dateFormatter.print(s.rrpsd.get)
        }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("practiceId" -> JsNumber(s.practiceId)) + ("staffId" -> JsNumber(s.staffId)) + ("providerName" -> JsString(providerName)) + ("patientName" -> JsString(patientName)) + ("date" -> JsString(date)) + ("dob" -> JsString(birth_date)) + ("dateCreated" -> JsString(dateCreated.toString)) + ("allograftMaterialUsed" -> JsString(s.allograftMaterialUsed)) + ("allograftSize" -> JsString(s.allograftSize)) + ("allograftVolume" -> JsString(s.allograftVolume)) + ("allograftBrand" -> JsString(s.allograftBrand)) + ("xenograftMaterialUsed" -> JsString(s.xenograftMaterialUsed)) + ("xenograftSize" -> JsString(s.xenograftSize)) + ("xenograftVolume" -> JsString(s.xenograftVolume)) + ("xenograftBrand" -> JsString(s.xenograftBrand)) + ("alloplastMaterialUsed" -> JsString(s.alloplastMaterialUsed)) + ("alloplastSize" -> JsString(s.alloplastSize)) + ("alloplastVolume" -> JsString(s.alloplastVolume)) + ("alloplastBrand" -> JsString(s.alloplastBrand)) + ("autograftMaterialUsed" -> JsString(s.autograftMaterialUsed)) + ("autograftSize" -> JsString(s.autograftSize)) + ("autograftVolume" -> JsString(s.autograftVolume)) + ("membraneUsed" -> JsString(s.membraneUsed)) + ("membraneSize" -> JsString(s.membraneSize)) + ("membraneBrand" -> JsString(s.membraneBrand)) + ("additives" -> JsString(s.additives)) + ("additivesVolume" -> JsString(s.additivesVolume)) + ("additivesBrand" -> JsString(s.additivesBrand)) + ("finalPA" -> JsString(s.finalPA)) + ("rrpsd" -> JsString(sRrpsd))
        boneGraftObj = boneGraftObj :+ newObj
      }

      var sRate : Float = (((totalRecords - filterRecords).toFloat / totalRecords.toFloat) * 100)
      if(!sRate.isNaN){
          successRate =  Math.round(sRate * 100.0) / 100.0
      }

    //ADDING EMPTY NOTE ID RECORDS
    var emptyNoteId: Long = 0
    var providerWithTitle = ""
    var providerWithoutTitle = ""
    boneGraftQuery =  boneGraftQuery.filter(_.practiceId === practiceId)
    if(sFromDate != "" && sToDate != ""){
      boneGraftQuery = boneGraftQuery.filter(s => s.date >= LocalDate.parse(sFromDate) && s.date <= LocalDate.parse(sToDate))
    }
    // if(sProviderId != 0){
    if(providerId != Some("ALL") && providerId != None && providerId != Some("")){
      var b = db.run{
        ProviderTable.filter(_.id === convertStringToLong(providerId)).result.head
      } map { providerResult =>
          providerWithTitle = providerResult.title+" "+ providerResult.firstName+" "+ providerResult.lastName
          providerWithoutTitle = providerResult.firstName+" "+ providerResult.lastName
      }
      Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      boneGraftQuery = boneGraftQuery.filter(p => p.providerName === providerWithTitle || p.providerName === providerWithoutTitle)
    }


      val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
      var block = db.run{
        boneGraftQuery.filter(_.noteId === emptyNoteId).result
      } map { boneGraftList =>
        boneGraftList.foreach(s => {
          var iDate: String = ""
          var iDob: String = ""
          var iRrpsd = ""

          if(s.date != None){
            iDate = dateFormatter.print(s.date.get)
          }
          if(s.dob != None){
            iDob = dateFormatter.print(s.dob.get)
          }
          if(s.rrpsd != None){
            iRrpsd = dateFormatter.print(s.rrpsd.get)
          }

        var newObj =  Json.obj() + ("id" -> JsNumber(convertOptionLong(s.id)))+ ("noteId" -> JsNumber(s.noteId)) + ("practiceId" -> JsNumber(s.practiceId)) + ("staffId" -> JsNumber(s.staffId)) + ("providerName" -> JsString(s.providerName)) + ("patientName" -> JsString(s.patientName)) + ("date" -> JsString(iDate)) + ("dob" -> JsString(iDob)) + ("dateCreated" -> JsString("")) + ("allograftMaterialUsed" -> JsString(s.allograftMaterialUsed)) + ("allograftSize" -> JsString(s.allograftSize)) + ("allograftVolume" -> JsString(s.allograftVolume)) + ("allograftBrand" -> JsString(s.allograftBrand)) + ("xenograftMaterialUsed" -> JsString(s.xenograftMaterialUsed)) + ("xenograftSize" -> JsString(s.xenograftSize)) + ("xenograftVolume" -> JsString(s.xenograftVolume)) + ("xenograftBrand" -> JsString(s.xenograftBrand)) + ("alloplastMaterialUsed" -> JsString(s.alloplastMaterialUsed)) + ("alloplastSize" -> JsString(s.alloplastSize)) + ("alloplastVolume" -> JsString(s.alloplastVolume)) + ("alloplastBrand" -> JsString(s.alloplastBrand)) + ("autograftMaterialUsed" -> JsString(s.autograftMaterialUsed)) + ("autograftSize" -> JsString(s.autograftSize)) + ("autograftVolume" -> JsString(s.autograftVolume)) + ("membraneUsed" -> JsString(s.membraneUsed)) + ("membraneSize" -> JsString(s.membraneSize)) + ("membraneBrand" -> JsString(s.membraneBrand)) + ("additives" -> JsString(s.additives)) + ("additivesVolume" -> JsString(s.additivesVolume)) + ("additivesBrand" -> JsString(s.additivesBrand)) + ("finalPA" -> JsString(s.finalPA)) + ("rrpsd" -> JsString(iRrpsd))
        boneGraftObj = boneGraftObj :+ newObj
        })
      }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

       val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }
          var pageSize=new Rectangle(2400f,2400f);
          var document=new Document(pageSize);

          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setUserunit(2900f);
          writer.setPageEvent(new MyFooter())
          document.setMargins(36, 36, 55, 36)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
            val FONT_BODY_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 11)

            FONT_BODY_TITLE.setColor(BaseColor.GRAY);
            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

            var cell = new PdfPCell(new Phrase("BoneGraft Log Report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

          document.add(new Paragraph("\n"));
          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 13)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);
          var fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
          if(sFromDate != "" && sToDate != ""){
          if(sFromDate != sToDate){
            var cell1 = new PdfPCell(new Phrase(" BoneGraft Log From "+ fmt.print(LocalDate.parse(sFromDate)) + " To "+fmt.print(LocalDate.parse(sToDate)), FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          } else {
            var cell1 = new PdfPCell(new Phrase("BoneGraft Log Date "+ fmt.print(LocalDate.parse(sFromDate)) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          // var columnWidths:Array[Float] = Array(7,7,7,7,10,7,7,5,5,7,10,9,9,5,5,9);
          var table1: PdfPTable = new PdfPTable(27)
          table1.setWidthPercentage(100)
          var cell2 = new PdfPCell(new Phrase( "Date", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Patient Name", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "DOB", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase( "Provider", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2 = new PdfPCell(new Phrase("Allograft Material Used", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Allograft Size", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Allograft Volume", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Allograft Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Xenograft Material Used", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Xenograft Size", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Xenograft Volume", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Xenograft Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Alloplast Material Used", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Alloplast Size", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Alloplast Volume", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Alloplast Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Autograft Material Used", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Autograft Size", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Autograft Volume", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Membrane Used", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Membrane Size", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Membrane Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Additives", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Additives Volume", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Additives Brand", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("Final PA", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          cell2= new PdfPCell(new Phrase("RRPSD", FONT_BODY_TITLE));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          // }

          if(boneGraftObj.length > 0){
          var simpleDateFormat1:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          val bonegraftData = boneGraftObj.sortBy(s=>{(s \ "id").as[Long]}).sortBy(s=>{simpleDateFormat1.format(new SimpleDateFormat("mm/dd/yyyy").parse((s \ "date").as[String]))}).reverse
          bonegraftData.foreach(s=>{
           var data = List((s \ "date").as[String],(s \ "patientName").as[String],(s \ "dob").as[String],(s \ "providerName").as[String],(s \ "allograftMaterialUsed").as[String],(s \ "allograftSize").as[String],(s \ "allograftVolume").as[String],(s \ "allograftBrand").as[String],(s \ "xenograftMaterialUsed").as[String],(s \ "xenograftSize").as[String],(s \ "xenograftVolume").as[String],(s \ "xenograftBrand").as[String],(s \ "alloplastMaterialUsed").as[String],(s \ "alloplastSize").as[String],(s \ "alloplastSize").as[String],(s \ "alloplastBrand").as[String],(s \ "autograftMaterialUsed").as[String],(s \ "autograftSize").as[String],(s \ "autograftVolume").as[String],(s \ "membraneUsed").as[String],(s \ "membraneSize").as[String],(s \ "membraneBrand").as[String],(s \ "additives").as[String],(s \ "additivesVolume").as[String],(s \ "additivesBrand").as[String],(s \ "finalPA").as[String],(s \ "rrpsd").as[String])
          data.foreach(row=>{
          cell2= new PdfPCell(new Phrase(row, FONT_BODY));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          table1.addCell(cell2);
          })

          })
          } else {
          cell2= new PdfPCell(new Phrase("No Records Found", FONT_BODY));
          cell2.setBorder(Rectangle.BOTTOM)
          cell2.setBorderColor(BaseColor.LIGHT_GRAY)
          cell2.setPadding(10)
          cell2.setColspan(24);
          cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
          table1.addCell(cell2);
          }
          document.add(table1);
          document.close();
          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"BoneGraft Log Report"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
 }

def boneGraftingCreate = silhouette.SecuredAction.async(parse.json) { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var staffId = request.identity.id.get
  var noteId = 0L
  val boneGraft = for {
  providerName <- (request.body \ "providerName").validate[String]
  patientName <- (request.body \ "patientName").validate[String]
  date <- (request.body \ "date").validate[LocalDate]
  dob <- (request.body \ "dob").validate[LocalDate]
  allograftMaterialUsed <- (request.body \ "allograftMaterialUsed").validateOpt[String]
  allograftSize <- (request.body \ "allograftSize").validateOpt[String]
  allograftVolume <- (request.body \ "allograftVolume").validateOpt[String]
  allograftBrand <- (request.body \ "allograftBrand").validateOpt[String]
  xenograftMaterialUsed <- (request.body \ "xenograftMaterialUsed").validateOpt[String]
  xenograftSize <- (request.body \ "xenograftSize").validateOpt[String]
  xenograftVolume <- (request.body \ "xenograftVolume").validateOpt[String]
  xenograftBrand <- (request.body \ "xenograftBrand").validateOpt[String]
  alloplastMaterialUsed <- (request.body \ "alloplastMaterialUsed").validateOpt[String]
  alloplastSize <- (request.body \ "alloplastSize").validateOpt[String]
  alloplastVolume <- (request.body \ "alloplastVolume").validateOpt[String]
  alloplastBrand <- (request.body \ "alloplastBrand").validateOpt[String]
  autograftMaterialUsed <- (request.body \ "autograftMaterialUsed").validateOpt[String]
  autograftSize <- (request.body \ "autograftSize").validateOpt[String]
  autograftVolume <- (request.body \ "autograftVolume").validateOpt[String]
  membraneUsed <- (request.body \ "membraneUsed").validateOpt[String]
  membraneSize <- (request.body \ "membraneSize").validateOpt[String]
  membraneBrand <- (request.body \ "membraneBrand").validateOpt[String]
  additives <- (request.body \ "additives").validateOpt[String]
  additivesVolume <- (request.body \ "additivesVolume").validateOpt[String]
  additivesBrand <- (request.body \ "additivesBrand").validateOpt[String]
  finalPA <- (request.body \ "finalPA").validateOpt[String]
  rrpsd <- (request.body \ "rrpsd").validateOpt[LocalDate]
  failureDate <- (request.body \ "failureDate").validateOpt[LocalDate]
  } yield {
    BoneGraftingDataRow(None,noteId,practiceId,staffId,providerName,patientName,Some(date),Some(dob),convertOptionString(allograftMaterialUsed),convertOptionString(allograftSize),convertOptionString(allograftVolume),convertOptionString(allograftBrand),convertOptionString(xenograftMaterialUsed),convertOptionString(xenograftSize),convertOptionString(xenograftVolume),convertOptionString(xenograftBrand),convertOptionString(alloplastMaterialUsed),convertOptionString(alloplastSize),convertOptionString(alloplastVolume),convertOptionString(alloplastBrand),convertOptionString(autograftMaterialUsed),convertOptionString(autograftSize),convertOptionString(autograftVolume),convertOptionString(membraneUsed),convertOptionString(membraneSize),convertOptionString(membraneBrand),convertOptionString(additives),convertOptionString(additivesVolume),convertOptionString(additivesBrand),convertOptionString(finalPA),rrpsd,failureDate)
  }
    boneGraft match {
      case JsSuccess(boneGraftingDataRow, _) =>
      db.run{
       BoneGraftingDataTable.returning(BoneGraftingDataTable.map(_.id)) += boneGraftingDataRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(boneGraftingDataRow.copy(id = Some(id))))
            }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

def updateBoneGraftData(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val boneGraft = for {
      failureDate <- (request.body \ "failureDate").validateOpt[LocalDate]
      finalPA <- (request.body \ "finalPA").validateOpt[String]
    } yield {
      (failureDate, finalPA)
    }
    boneGraft match {
      case JsSuccess((failureDate, finalPA), _) => db.run {
        val query = BoneGraftingDataTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(failureDate).map(value => query.map(_.failureDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            finalPA.map(value => query.map(_.finalPA).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


}


