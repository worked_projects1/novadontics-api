package controllers.v3

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.ResourceSubCategoryRow
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

@Singleton
class ResourceSubCategoriesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val categoryFormat = Json.format[ResourceSubCategoryRow]

  object ResourceType {
    private val types = List("courses", "documents")
    private val defaultType = "unknown"
    def get(resourceType: String): String = (types.contains(resourceType)) match {
      case true => resourceType
      case false => defaultType
    }
  }

  object Input {
    trait ResourceCategory
    case class PlainResourceCategory(id: Long, order: Option[Int]) extends ResourceCategory
    case class NewResourceCategory(name: String) extends ResourceCategory
    case class UpdatedResourceCategory(id: Long, order: Option[Int], name: Option[String]) extends ResourceCategory

    object ResourceCategory {
      lazy val reads = Reads

      implicit object Reads extends Reads[ResourceCategory] {
        override def reads(json: JsValue) = try {
          for {
            idOpt <- (json \ "id").validateOpt[Long]
            orderOpt <- (json \ "order").validateOpt[Int]
            nameOpt <- (json \ "name").validateOpt[String]
          } yield {
            (idOpt, orderOpt, nameOpt) match {
              case (Some(id), order, name) if name.isDefined =>
                UpdatedResourceCategory(id, order, name)
              case (Some(id), order, _) =>
                PlainResourceCategory(id, order)
              case (_, _, Some(name)) =>
                NewResourceCategory(name)
            }
          }
        } catch {
          case _: MatchError => JsError()
        }
      }
    }
  }

  def readAll(resourceType: String, categoryId:Long) = silhouette.SecuredAction.async { request =>
    db.run{
      ResourceSubCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.categoryId===categoryId).sortBy(_.order).result
    } map {resourceSubCategories => Ok(Json.toJson(resourceSubCategories))}
  }

  def read(resourceType: String,id:Long) = silhouette.SecuredAction.async { request =>
    db.run{
      ResourceSubCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id===id).result.headOption
    } map {
      case Some(resourceSubCategory) => Ok(Json.toJson(resourceSubCategory))
      case None => NotFound
    }
  }

  def create(resourceType: String) = silhouette.SecuredAction(AdminOnly).async(parse.json) {
    request =>
      //Console.println("create condition == ")
      val resourceSubCategory = for {
        name <- (request.body \ "name").validate[String]
        categoryId <- (request.body \ "categoryId").validate[Long]
      } yield {
        ResourceSubCategoryRow(None,categoryId, name, ResourceType.get(resourceType), 0)
      }

      resourceSubCategory match {
        case JsSuccess(resourceSubCategoryRow, _) => db.run {
          ResourceSubCategoryTable.returning(ResourceSubCategoryTable.map(_.id)) += resourceSubCategoryRow
        } map {
          case 0L => PreconditionFailed
          case id => Created(Json.toJson(resourceSubCategoryRow.copy(id = Some(id))))
        }
        case JsError(_) => Future.successful(BadRequest)
      }
  }

  def update(resourceType: String,id:Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val resourceSubCategory = for {
      name <- (request.body \ "name").validateOpt[String]
    } yield {
      (name)
    }

    resourceSubCategory match {
      case JsSuccess((name), _) => db.run {
        val query = ResourceSubCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id===id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  

  def delete(resourceType: String,id:Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      ResourceSubCategoryTable.filter(_.resourceType === ResourceType.get(resourceType)).filter(_.id===id).delete.map {
        case 0L => NotFound
        case _ => Ok
      }
    }
  }
}
