
package controllers.v3
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import play.api.libs.json.Json
import javax.inject._
import models.daos.tables.{StaffRow,SaasAdminRow,SaasAdminPaymentRow}
import play.api.mvc.Result
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import com.github.tototoshi.csv._

class SaasAdminController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val saasRowWrites = Writes { request: (SaasAdminRow) =>
  val (saasadminRow) = request
  var subExpDate: String = null
  if(saasadminRow.subExpDate != None){
    subExpDate = DateTimeFormat.forPattern("MM/dd/YYYY").print(convertOptionalLocalDate(saasadminRow.subExpDate))
  }
  Json.obj(
      "id" -> saasadminRow.id,
      "saasType" -> saasadminRow.saasType,
      "logo" -> saasadminRow.logo,
      "name" -> saasadminRow.name,
      "subStartDate" -> DateTimeFormat.forPattern("MM/dd/YYYY").print(saasadminRow.subStartDate),
      "subExpDate" -> subExpDate,
      "subscriptionType" -> saasadminRow.subscriptionType,
      "subscriptionFee" -> Math.round(convertOptionFloat(saasadminRow.subscriptionFee) * 100.0) / 100.0,
      "notes" -> saasadminRow.notes,
      "autoRenew" -> saasadminRow.autoRenew,
      "documents" -> saasadminRow.documents
      )
  }

  def readAll = silhouette.SecuredAction.async { request =>
  var pageNumber:Int = 0
  var recPerPage:Int = 0
  val searchTextVal: Option[String] = request.headers.get("searchText")
  val searchText:String = convertOptionString(searchTextVal)
  var pageNumberVal: Option[String] = request.headers.get("pageNumber")
    if ( pageNumberVal != None){
      pageNumber = convertOptionString(pageNumberVal).toInt
    }

    if (pageNumber == 0 ){
      pageNumber = 1
    }
    
    var recPerPageVal: Option[String] = request.headers.get("recPerPage")
    if( recPerPageVal != None){
      recPerPage = convertOptionString(recPerPageVal).toInt
    }    

    var query = SaasAdminTable.filterNot(_.deleted)
    if (searchText != ""){
     query = query.filter(f=>((f.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f.saasType.toLowerCase like "%"+searchText.toLowerCase+"%")))
    }
    db.run {
            query.filterNot(_.deleted).result } map { saasList =>
        // Ok(Json.toJson(saasList)).withHeaders(
        //                       ("totalCount" -> saasList.length.toString()))
         if (recPerPage == 0){
            val pageData = saasList
            Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> saasList.length.toString()))
          }
          else{
              val pageData = saasList.drop((pageNumber-1)*recPerPage).take(recPerPage)
              Ok(Json.toJson(pageData)).withHeaders(
                              ("totalCount" -> saasList.length.toString()))
          }
      }
  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
    val saas = for {
      saasType <- (request.body \ "saasType").validate[String]
      logo <- (request.body \ "logo").validate[String]
      name <- (request.body \ "name").validate[String]
      subStartDate <- (request.body \ "subStartDate").validate[LocalDate]
      subExpDate <- (request.body \ "subExpDate").validateOpt[LocalDate]
      subscriptionType <- (request.body \ "subscriptionType").validateOpt[String]
      subscriptionFee <- (request.body \ "subscriptionFee").validateOpt[Float]
      notes <- (request.body \ "notes").validateOpt[String]
      autoRenew <- (request.body \ "autoRenew").validateOpt[String]
      documents <- (request.body \ "documents").validateOpt[String]
    } yield {

      SaasAdminRow(None,saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents,false)
    }
    
    saas match {
      case JsSuccess(saasAdminRow, _) => db.run {
        SaasAdminTable.returning(SaasAdminTable.map(_.id)) += saasAdminRow
       
      } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(saasAdminRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  
  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val saas = for {
      saasType <- (request.body \ "saasType").validateOpt[String]
      logo <- (request.body \ "logo").validateOpt[String]
      name <- (request.body \ "name").validateOpt[String]
      subStartDate <- (request.body \ "subStartDate").validateOpt[LocalDate]
      subExpDate <- (request.body \ "subExpDate").validateOpt[LocalDate]
      subscriptionType <- (request.body \ "subscriptionType").validateOpt[String]
      subscriptionFee <- (request.body \ "subscriptionFee").validateOpt[Float]
      notes <- (request.body \ "notes").validateOpt[String]
      autoRenew <- (request.body \ "autoRenew").validateOpt[String]
      documents <- (request.body \ "documents").validateOpt[String]
    } yield {
      (saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents)
    } 
    
    saas match {
      case JsSuccess((saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents), _) =>
       db.run {
          val query = SaasAdminTable.filter(_.id === id)

          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Read with Effect.Write](
                saasType.map(value => query.map(_.saasType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                logo.map(value => query.map(_.logo).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                subStartDate.map(value => query.map(_.subStartDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subExpDate).map(value => query.map(_.subExpDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subscriptionType).map(value => query.map(_.subscriptionType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(subscriptionFee).map(value => query.map(_.subscriptionFee).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(notes).map(value => query.map(_.notes).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(autoRenew).map(value => query.map(_.autoRenew).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(documents).map(value => query.map(_.documents).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ => Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  

def delete(id: Long) = silhouette.SecuredAction.async {
    db.run {
      SaasAdminTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

def saasAdminPaymentSummary = silhouette.SecuredAction.async { request =>
  var jsonObject: JsValue = null
  var dm  = List[Long]()
  db.run{
    SaasAdminPaymentTable.sortBy(_.year).result
  } map { paymentList =>
      paymentList.foreach(s => {
        if(!(dm contains s.year)){
        dm = s.year :: dm
        }
      })
      var strJson = ""
      for(a <- dm){
        var total: Float = 0
        var amount: Float = 0
        var saasPayment:scala.collection.mutable.Map[String,Float]=scala.collection.mutable.Map()
        var block = db.run{
          SaasAdminPaymentTable.filter(_.year === a).result
        } map { saasList =>
            (saasList).foreach(result => {
              total = total + result.amount
              amount = convertOptionFloat(saasPayment.get(result.month)) +  result.amount
              saasPayment.update(result.month,amount)
            })
            var temp = saasPayment += ("year" -> a,"total" -> total)
            var map = scala.util.parsing.json.JSONObject(temp.toMap)
            strJson = strJson + String.valueOf(map) + ","
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        jsonObject = Json.parse(s"[${strJson.dropRight(1)}]")
      }
       Ok(Json.toJson(jsonObject))
    }
  }

def updateSaasAdminPayment(saasAdminId: Long, year: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  val saasAdminPayment = for {
      saasAdmin <- (request.body \ "saasAdmin").validate[JsValue]
    } yield {
      (saasAdmin)
    }

  saasAdminPayment match {
      case JsSuccess((saasAdmin), _) =>
      val jsonList: List[JsValue] = Json.parse(saasAdmin.toString).as[List[JsValue]]
      (jsonList).foreach( result => {
        val month = convertOptionString((result \ ("month")).asOpt[String])
        val amount = convertOptionFloat((result \ ("amount")).asOpt[Float])
        db.run {
          val query = SaasAdminPaymentTable.filter(_.saasAdminId === saasAdminId).filter(_.year === year).filter(_.month === month)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true => DBIO.seq[Effect.Write](
              Some(amount).map(value => query.map(_.amount).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map {_ => Ok}
            case false =>
            var block = db.run{
                SaasAdminPaymentTable.returning(SaasAdminPaymentTable.map(_.id)) += SaasAdminPaymentRow(None,saasAdminId,month,year,amount)
                } map {
                case 0L => PreconditionFailed
                case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
            DBIO.successful(Ok)
          }
        }
      })
      Future(Ok)
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def getSaasAdminPayment(saasAdminId:Long, year:Long) = silhouette.SecuredAction.async { request =>
  var saasAdminPayment:scala.collection.mutable.Map[String,Float]=scala.collection.mutable.Map()
      db.run {
        SaasAdminPaymentTable.filter(_.saasAdminId === saasAdminId).filter(_.year === year).result
          } map { saasAdminList =>
          (saasAdminList).foreach(result => {
              saasAdminPayment.update(result.month,result.amount)
          })
          var map = scala.util.parsing.json.JSONObject(saasAdminPayment.toMap)
          val jsonObject: JsValue = Json.parse(String.valueOf(map))
        Ok(Json.toJson(Array(jsonObject)))
      }
  }

case class SaasAdimnCsvList(
  
  name: String,
  logo: String,
  saasType: String,
  subscriptionType: String,
  subscriptionFee: Float,
  subStartDate: LocalDate,
  subExpDate: LocalDate,
  autoRenew: String,
  documents: String,
  notes: String
)

implicit val productFormat = Json.format[SaasAdimnCsvList]

def generateCSV = silhouette.SecuredAction.async { request =>
  var dataList = List.empty[SaasAdimnCsvList]
    db.run {
       SaasAdminTable.filterNot(_.deleted).result
    } map { saasAdminata =>
      saasAdminata.map{result=>
        var subscriptionFee:Float = 0
        if(result.subscriptionFee != "" && result.subscriptionFee != None){
          subscriptionFee = convertOptionFloat(result.subscriptionFee)
        }
          var data1 = SaasAdimnCsvList(
          result.name,
          result.logo,
          result.saasType,
          convertOptionString(result.subscriptionType),
          subscriptionFee,
          result.subStartDate,
          convertOptionalLocalDate(result.subExpDate),
          convertOptionString(result.autoRenew),
          convertOptionString(result.documents),
          convertOptionString(result.notes)
          )
          dataList = dataList :+ data1
     }
    
    val file = new java.io.File("/tmp/saasAdmin.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Name", "Logo", "Saas Type", "Subscription Type", "Subscription Fee", "Subscription Start Date", "Subscription Expiration Date", "Auto Renew", "Document", "Notes"))
    writer.writeRow(List(""))
    dataList.map {
      referralList => writer.writeRow(List(referralList.name, referralList.logo, referralList.saasType, referralList.subscriptionType, referralList.subscriptionFee, referralList.subStartDate, referralList.subExpDate, referralList.autoRenew, referralList.documents, referralList.notes))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    } 
}
}


