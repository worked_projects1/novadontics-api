package controllers.v2

import co.spicefactory.util.BearerTokenEnv
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables._
import play.api.Application
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class FavouritesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
  import driver.api._

  def add(id: Long) = silhouette.SecuredAction(StaffOnly).async {
    _.identity match {
      case user: StaffRow =>
        db.run {
          CustomerFavouritesTable.filter(_.customer === user.id)
            .filter(_.product === id).exists.result.flatMap[Result, NoStream, Effect.Write] {
            case false => DBIO.seq[Effect.Write](
              CustomerFavouritesTable += CustomerFavouritesRow(None, user.id.get, id)
            ) map {_ => Ok}
            case true => DBIO.successful(NotFound)
          }
        }
      case _ => Future.successful(BadRequest)
    }
  }

  def remove(id: Long) = silhouette.SecuredAction(StaffOnly).async {
    _.identity match {
      case user: StaffRow =>
        db.run {
          CustomerFavouritesTable.filter(_.customer === user.id)
            .filter(_.product === id).delete map {
            case 0 => NotFound
            case _ => Ok
          }
        }
      case _ => Future.successful(BadRequest)
    }
  }
}
