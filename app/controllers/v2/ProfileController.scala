package controllers.v2

import co.spicefactory.util.BearerTokenEnv
import com.google.inject.Inject
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.StaffRow
import play.api.Application
import play.api.i18n.Messages
import play.api.libs.json.Json.obj
import play.api.libs.json.{JsError, JsSuccess, Json, Writes}
import play.api.mvc.Result
import play.api.Play.current
import play.api.i18n.Messages.Implicits._

import scala.concurrent.{ExecutionContext, Future}
class ProfileController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
  import driver.api._

  implicit val staffRowWrites = Writes { staff: StaffRow =>
    Json.obj(
      "name" -> staff.name,
      "email" -> staff.email,
      "phone" -> staff.phone,
      "deaNumber" -> staff.deaNumber,
      "npiNumber" -> staff.npiNumber,
      "country" -> staff.country,
      "city" -> staff.city,
      "state" -> staff.state,
      "zip" -> staff.zip,
      "address" -> staff.address
    )
  }

  def read = silhouette.SecuredAction(StaffOnly).async { request =>
    db.run {
      StaffTable.filterNot(_.archived).filter(_.id === request.identity.id).result.headOption
    } map {
      case Some(staff) => Ok(Json.toJson(staff))
      case None => NotFound
    }
  }

  def update = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    val staff = for {
      name <- (request.body \ "name").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      deaNumber <- (request.body \ "deaNumber").validateOpt[String]
      npiNumber <- (request.body \ "npiNumber").validateOpt[String]
      country <- (request.body \ "country").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
    } yield {
      (name, email, phone, deaNumber, npiNumber, country, city, state, zip, address)
    }

    staff match {
      case JsSuccess((name, email, phone, deaNumber, npiNumber, country, city, state, zip, address), _) =>
        db.run {
          val query = StaffTable.filter(_.id === request.identity.id)
          query.map(_.email).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case Some(emailAddress) =>
              val duplicateEmail = if (email.fold(false)(_ != emailAddress)) {
                for {
                  userExists <- UserTable.filter(_.email === email.get).exists.result
                  dentistExists <- StaffTable.filter(_.email === email.get).exists.result
                } yield {
                  userExists || dentistExists
                }
              } else {
                DBIO.successful(false)
              }

              duplicateEmail.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
                case true =>
                  DBIO.successful(PreconditionFailed(obj("message" -> Messages("profile.emailTaken"))))
                case false =>
                  DBIO.seq[Effect.Write](
                    name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    email.map(value => query.map(_.email).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    phone.map(value => query.map(_.phone).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    deaNumber.map(value => query.map(_.deaNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    npiNumber.map(value => query.map(_.npiNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    country.map(value => query.map(_.country).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    city.map(value => query.map(_.city).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    state.map(value => query.map(_.state).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    zip.map(value => query.map(_.zip).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    address.map(value => query.map(_.address).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) andThen {
                      StaffTable.filterNot(_.archived).filter(_.id === request.identity.id).result.headOption
                  } map { result =>
                    Ok(Json.toJson(result))
                  }
              }
            case None =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }
}
