package controllers.v2
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream}
import java.util.TimeZone
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import com.google.inject.name.Named
import play.api.{Application, Logger}
import play.api.libs.json.Json.{obj, toJson}
import akka.stream.scaladsl.StreamConverters
import play.api.http.HttpEntity
import play.api.libs.json._
import play.api.mvc.{ResponseHeader, Result}
import models.daos.tables.{PrescriptionRow}
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO

@Singleton
class PrescriptionController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
    import driver.api._

    implicit val format = Json.format[PrescriptionRow]

    case class Patient(name: Option[String], email: Option[String], phone: Option[String])

    def readAll = silhouette.SecuredAction.async {
        db.run {
            PrescriptionTable.filterNot(_.deleted).sortBy(_.medication).result
        } map { result => Ok(Json.toJson(result)) }
    }

    def create = silhouette.SecuredAction.async(parse.json) { request =>
        val prescription = for {
            medication <- (request.body \ "medication").validate[String]
            dosage <- (request.body \ "dosage").validate[String]
            count <- (request.body \ "count").validateOpt[Int]
            instructions <- (request.body \ "instructions").validateOpt[String]
            refills <- (request.body \ "refills").validate[Int]
            genericSubstituteOk <- (request.body \ "genericSubstituteOk").validateOpt[Boolean]
            category <- (request.body \ "category").validate[String]
            indications <- (request.body \ "indications").validateOpt[String]
            contraindications <- (request.body \ "contraindications").validateOpt[String]
            medicationInteraction <- (request.body \ "medicationInteraction").validateOpt[String]
            useInOralImplantology <- (request.body \ "useInOralImplantology").validateOpt[String]
        }
        yield PrescriptionRow(None, medication, dosage, count, instructions, refills, genericSubstituteOk, false,category,convertOptionString(indications),convertOptionString(contraindications),convertOptionString(medicationInteraction),convertOptionString(useInOralImplantology))

        prescription match {
            case JsSuccess(row, _) => db.run {
                PrescriptionTable.returning(PrescriptionTable.map(_.id)) += row
            } map {
                case 0L => PreconditionFailed
                case id => Created(Json.toJson(row.copy(id = Some(id))))
            }
            case JsError(_) => Future.successful(BadRequest)
        }
    }


     def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val prescription = for {
            medication <- (request.body \ "medication").validateOpt[String]
            dosage <- (request.body \ "dosage").validateOpt[String]
            count <- (request.body \ "count").validateOpt[Int]
            instructions <- (request.body \ "instructions").validateOpt[String]
            refills <- (request.body \ "refills").validateOpt[Int]
            genericSubstituteOk <- (request.body \ "genericSubstituteOk").validateOpt[Boolean]
            category <- (request.body \ "category").validateOpt[String]
            indications <- (request.body \ "indications").validateOpt[String]
            contraindications <- (request.body \ "contraindications").validateOpt[String]
            medicationInteraction <- (request.body \ "medicationInteraction").validateOpt[String]
            useInOralImplantology <- (request.body \ "useInOralImplantology").validateOpt[String]
    } yield {
      (medication,dosage,count,instructions,refills,genericSubstituteOk,category,indications,contraindications,medicationInteraction,useInOralImplantology)
    }

    prescription match {
      case JsSuccess((medication,dosage,count,instructions,refills,genericSubstituteOk,category,indications,contraindications,medicationInteraction,useInOralImplantology), _) =>
      db.run {
        val query = PrescriptionTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            medication.map(value => query.map(_.medication).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            dosage.map(value => query.map(_.dosage).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            count.map(value => query.map(_.count).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            instructions.map(value => query.map(_.instructions).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            refills.map(value => query.map(_.refills).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            genericSubstituteOk.map(value => query.map(_.genericSubstituteOk).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            category.map(value => query.map(_.category).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            indications.map(value => query.map(_.indications).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            contraindications.map(value => query.map(_.contraindications).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            medicationInteraction.map(value => query.map(_.medicationInteraction).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            useInOralImplantology.map(value => query.map(_.useInOralImplantology).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


    def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
        db.run {
            PrescriptionTable.filter(_.id === id).map(_.deleted).update(true)
        } map {
            case 0L => NotFound
            case _ => Ok
        }
    }

    def generatePdf(id: Long, treatment: String, signature: String, timezone: Option[String], providerId: Long) = silhouette.SecuredAction(SupersOnly).async { request =>
        var clientTimeZone:String=convertOptionString(timezone)
        val action = for {
            dentist <- StaffTable.filter(_.id === request.identity.id)
            practice <- PracticeTable if dentist.practiceId === practice.id
            prescription <- PrescriptionTable.filter(_.id === id)
            treatment <- TreatmentTable.filter(_.id === treatment.toLong)
            provider <- ProviderTable.filter(_.id === providerId)
            step <- StepTable.filter(_.formId === "1A") if treatment.id === step.treatmentId
        } yield (dentist, practice, prescription, provider, step)

        db.run {
            action.result
        } map {
            result =>
                val (dentist, practice, prescription, provider, _) = result.head
                val step = result.map(_._5).maxBy(_.dateCreated).value
                val patient = (for {
                    name <- (step \ "full_name").validateOpt[String]
                    lastName <- (step \ "last_name").validateOpt[String]
                    email <- (step \ "email").validateOpt[String]
                    phone <- (step \ "phone").validateOpt[String]

                } yield (convertOptionString(name)+" "+ convertOptionString(lastName), email, phone)) match {
                    case JsSuccess((name, email, phone), _) => Patient(Some(name), email, phone)

                    case _ => Patient(None, None, None)
                }

                val source = StreamConverters.fromInputStream { () =>
                    import java.io.IOException
                    import com.itextpdf.text.pdf._
                    import com.itextpdf.text.{List => _, _}
                    import com.itextpdf.text.Paragraph
                    import com.itextpdf.text.Element
                    import com.itextpdf.text.Rectangle
                    import com.itextpdf.text.pdf.PdfPTable

                    import com.itextpdf.text.pdf.PdfContentByte
                    import com.itextpdf.text.pdf.PdfPTable
                    import com.itextpdf.text.pdf.PdfPTableEvent
                    class BorderEvent extends PdfPTableEvent {
                        override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
                            val width = widths(0)
                            val x1 = width(0)
                            val x2 = width(width.length - 1)
                            val y1 = heights(0)
                            val y2 = heights(heights.length - 1)
                            val cb = canvases(PdfPTable.LINECANVAS)
                            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
                            cb.setLineWidth(0.4)
                            cb.stroke()
                            cb.resetRGBColorStroke()
                        }
                    }

                    val document = new Document(PageSize.A5)
                    val inputStream = new PipedInputStream()
                    val outputStream = new PipedOutputStream(inputStream)
                    val writer = PdfWriter.getInstance(document, outputStream)

                    document.setMargins(40, 40, 40 , 60)
                    Future({
                        document.open()
                        // Metadata
                        val FONT_COVER_PAGE = FontFactory.getFont(FontFactory.TIMES, 29)
                        val FONT_H1 = FontFactory.getFont(FontFactory.HELVETICA, 14)
                        val FONT_H2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)
                        val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 8)
                        val FONT_BODY_BOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8)
                        val FONT_MICRO = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 6)
                        val FONT_LARGE_BOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 9)


                        document.addTitle(s"Prescription for ${patient.name.getOrElse("")}")
                        document.addSubject("Prescription")
                        document.addAuthor(s"${dentist.name}")
                        document.addCreator("Novadontics")

                        // Title and patient details
                        var fmt = DateTimeFormat.forPattern("MM/dd/yy hh:mm aa");
                        var dateTimeFormat=""
                        if (clientTimeZone==""){
                            dateTimeFormat = DateTime.now.toString(fmt)
                        }else{
                            dateTimeFormat = DateTime.now.withZone(DateTimeZone.forID(clientTimeZone)).toString(fmt)
                        }
//                        document.add(new Paragraph(64, "Smile Analysis and Design Report", FONT_COVER_PAGE))
//                        document.add(new Paragraph(treatment.emailAddress, FONT_BODY))

                        // Practice and dentist info

                        val table: PdfPTable = new PdfPTable(2)
                        table.setWidthPercentage(100)
                        table.setHorizontalAlignment(Element.ALIGN_LEFT)
                        table.setWidths(Array[Int](1, 1))
                        table.getDefaultCell.setBorder(Rectangle.NO_BORDER)
                        table.setTableEvent(new BorderEvent())

                        val practiceData = List(s"${practice.name}", s"${practice.address}", s"${practice.city} ${practice.state} ${practice.zip}", "", "")
                        val prescriberData = List(s"Prescriber: ${provider.title +" "+provider.firstName+" "+provider.lastName}", s"Email: ${provider.emailAddress}", s"Telephone: ${provider.phoneNumber}", s"DEA NO: ${provider.deaNumber}", s"NPI: ${provider.npiNumber}")
                        (practiceData zip prescriberData).foreach(item => {
                            table.addCell(new Phrase(item._1, FONT_BODY_BOLD) )
                            table.addCell(new Phrase(item._2, FONT_BODY_BOLD))
                        })

                        table.setSpacingAfter(10)
                        document.add(table)

                        // Patient info
                        val table2: PdfPTable = new PdfPTable(4)
                        table2.setWidthPercentage(100)
                        table2.setHorizontalAlignment(Element.ALIGN_LEFT)
                        table2.setWidths(Array[Int](1, 2, 1, 2))
                        table2.getDefaultCell.setBorder(Rectangle.NO_BORDER)
                        table2.setTableEvent(new BorderEvent())

                        val rows = List(
                            List(s"Patient:", patient.name.getOrElse(""), s"Telephone:", patient.phone.getOrElse("")),
                            List(s"Email:", patient.email.getOrElse(""), s"Date:", dateTimeFormat)
                        )

                        rows.flatten.foreach(item => {
                            table2.addCell(new Phrase(item, FONT_BODY) )
                        })

                        def emptyCell(colspan: Int, content: Option[String], font: Option[Font]) = {
                            val empty = new PdfPCell()
                            empty.setColspan(colspan)
                            empty.setBorder(Rectangle.NO_BORDER)
                            empty.addElement(new Phrase(content.getOrElse("\n"), font.getOrElse(FONT_BODY)))
                            empty
                        }

                        table2.addCell(emptyCell(4, None, None))

                        val rxCell = emptyCell(1, Some("Rx"), Some(FONT_COVER_PAGE))
                        rxCell.setVerticalAlignment(Element.ALIGN_TOP)
                        rxCell.setRowspan(4)
                        table2.addCell(rxCell)

                        val preRows = List(s"${prescription.medication} ${prescription.dosage}", s"Disp: ${prescription.count.getOrElse(0)}",
                            prescription.instructions.getOrElse(""), s"Refills ${prescription.refills}")
                        preRows.foreach(item => {
                            val cell = new PdfPCell()
                            cell.setBorder(Rectangle.NO_BORDER)
                            cell.addElement(new Phrase(item, FONT_LARGE_BOLD))
                            cell.setColspan(3)
                            table2.addCell(cell)
                        })

                        table2.addCell(emptyCell(2, None, None))

                        if(signature.length !=0){
                            val imgLogo =  Image.getInstance(signature);
                            var imageCell = createImageCell2(imgLogo,10f, 5f,true,document);
                            imageCell.setBackgroundColor(BaseColor.WHITE);
                            imageCell.setBorder(Rectangle.TOP);
                            imageCell.setPaddingBottom(-10);
                            imageCell.setUseVariableBorders(true)
                            imageCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            imageCell.setBorderColorTop(new BaseColor(255, 255, 255));
                            table2.addCell(imageCell);
                        }

                        table2.addCell(emptyCell(2, None, None))
                        table2.addCell(emptyCell(4, Some(s"Generic Substitution ${if (!prescription.genericSubstituteOk.getOrElse(false)) "Not" else "" } Permitted"), Some(FONT_BODY)))

                        table2.addCell(emptyCell(2, None, None))

                        val signatureCell = emptyCell(2, Some("Signature of prescriber"), Some(FONT_MICRO))
                        signatureCell.setBorder(Rectangle.TOP)
                        signatureCell.setHorizontalAlignment(Element.ALIGN_CENTER)

                        table2.addCell(signatureCell)
                        table2.setSpacingAfter(10)
                        document.add(table2)

                        // val table3: PdfPTable = new PdfPTable(4)
                        // table3.setWidthPercentage(100   )
                        // table3.setHorizontalAlignment(Element.ALIGN_LEFT)
                        // table3.setWidths(Array[Int](1, 2, 1, 2))
                        // table3.getDefaultCell.setBorder(Rectangle.NO_BORDER)
                        // table3.setTableEvent(new BorderEvent())

                        // table3.addCell(new Phrase("Patient:", FONT_BODY_BOLD))
                        // table3.addCell(new Phrase(patient.name.getOrElse(""), FONT_BODY))
                        // table3.addCell(new Phrase("Date:", FONT_BODY_BOLD))
                        // table3.addCell(new Phrase(dateTimeFormat.print(DateTime.now), FONT_BODY))
                        // table3.addCell(new Phrase("Prescription:", FONT_BODY_BOLD))
                        // table3.addCell(emptyCell(3, Some(s"${prescription.medication} ${prescription.dosage}"), None))
                        // table3.addCell(new Phrase("Prescriber:", FONT_BODY_BOLD))
                        // table3.addCell(emptyCell(3, Some(s"${dentist.name} ${prescription.dosage}"), None))
                        // document.add(table3)

                        document.close()
                    })(ioBoundExecutor)

                    inputStream
                }

                val filename = s"${patient.name}_prescription_${prescription.medication}"
                val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

                Result(
                    header = ResponseHeader(200, Map.empty),
                    body = HttpEntity.Streamed(source, None, Some("application/pdf"))
                ).withHeaders(
                    "Content-Disposition" -> contentDisposition
                )
        }
    }

    def createImageCell2(img:Image,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
        import com.itextpdf.text.Element
        //    val img = Image.getInstance(path);
        //    img.scaleAbsolute(width,height);

        val indentation = 0
        //whatever
        //    val img =  Image.getInstance(path);

        val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 5

        img.scalePercent(scaler)

        //img.scaleAbsolute(height, width);
        //img.scalePercent(height);
        val cell = new PdfPCell(img);
        cell.setPadding(5);
        if (!border){
            cell.setBorder(Rectangle.NO_BORDER);
        }
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }
}
