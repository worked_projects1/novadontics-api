
package controllers.v2
import controllers.NovadonticsController
import co.spicefactory.util.BearerTokenEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.Application
import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import javax.inject._
import models.daos.tables.{PrescriptionHistoryRow, StaffRow}
import play.api.mvc.Result
import org.joda.time.{ LocalTime, LocalDate }
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import java.text.SimpleDateFormat
import akka.stream.scaladsl.StreamConverters
import java.awt.geom.AffineTransform
import java.util.{Date, Locale}
import play.api.mvc.{ResponseHeader, Result}
import play.api.http.HttpEntity
import play.api.{Application, Logger}
import java.awt.image.BufferedImage
import java.io.File
import java.awt.image.BufferedImage

import com.itextpdf.text.{Document, Element, Image, PageSize, Rectangle}
import com.itextpdf.text.pdf._
import javax.imageio.ImageIO

@Singleton
class PrescriptionHistoryController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

import driver.api._
import com.github.tototoshi.slick.PostgresJodaSupport._

 private val logger = Logger(this.getClass)

 implicit val PrescriptionHistoryRowWrites = Writes { prescriptionHistory: (PrescriptionHistoryRow) =>
    val (prescriptionHistoryRow) = prescriptionHistory
    Json.obj(   
            "id" -> prescriptionHistoryRow.id,
            "prescriptionId" -> prescriptionHistoryRow.prescriptionId,
            "patientId" -> prescriptionHistoryRow.patientId,
            "staffId" -> prescriptionHistoryRow.staffId,
            "dateCreated" -> prescriptionHistoryRow.dateCreated,
            "note" -> prescriptionHistoryRow.note
          )
  } 

    case class PrescriptionHistoryList(
        id: Option[Long],
        prescriptionId: Long,
        patientId: Long,
        staffId: Long,
        staffName: String,
        dateCreated: LocalDate,
        medication: String,
        dosage: String,
        count: Option[Int],
        instructions: Option[String],
        refills: Long,
        genericSubstituteOk: Option[Boolean],
        category: String,
        indications: String, 
        contraindications: String,
        medicationInteraction: String,
        useInOralImplantology: String,
        deleted: Boolean,
        note: Option[String]
    ) 

  implicit val PrescriptionHistoryWrites = Writes { prescriptionRow: (PrescriptionHistoryList) =>
    val (prescriptionListsRow) = prescriptionRow
    Json.obj(   
            "id" -> prescriptionListsRow.id,
            "prescriptionId" -> prescriptionListsRow.prescriptionId,
            "patientId" -> prescriptionListsRow.patientId,
            "staffId" -> prescriptionListsRow.staffId,
            "staffName" -> prescriptionListsRow.staffName,
            "dateCreated" -> prescriptionListsRow.dateCreated,

            "medication" -> prescriptionListsRow.medication,
            "dosage" -> prescriptionListsRow.dosage,
            "count" -> prescriptionListsRow.count,
            "instructions" -> prescriptionListsRow.instructions,
            "refills" -> prescriptionListsRow.refills,
            "genericSubstituteOk" -> prescriptionListsRow.genericSubstituteOk,
            "category" -> prescriptionListsRow.category,
            "indications" -> prescriptionListsRow.indications,
            "contraindications" -> prescriptionListsRow.contraindications,
            "medicationInteraction" -> prescriptionListsRow.medicationInteraction,
            "useInOralImplantology" -> prescriptionListsRow.useInOralImplantology,
            "deleted" -> prescriptionListsRow.deleted,
            "note" -> prescriptionListsRow.note

        )
  } 

    case class PrescriptionHistoryPDF(
        //patientName: String,
        dateCreated: LocalDate,
        doctorName: String,
        medication:String,
        dosage:String,
        count:Option[Int]
    ) 

   implicit val PrescriptionHistoryPDFWrites = Writes { prescriptionRowPDF: (PrescriptionHistoryPDF) =>
    val (PrescriptionRowPDF) = prescriptionRowPDF
    Json.obj(   
          //"patientName" -> PrescriptionRowPDF.patientName,
            "dateCreated" -> PrescriptionRowPDF.dateCreated,
            "doctorName" -> PrescriptionRowPDF.doctorName,
            "medication" -> PrescriptionRowPDF.medication,
            "dosage" -> PrescriptionRowPDF.dosage,
            "count" -> PrescriptionRowPDF.count
        )
  } 
 
 def readAll = silhouette.SecuredAction.async { request =>
    db.run {
        PrescriptionHistoryTable
        .join(PrescriptionTable).on(_.prescriptionId === _.id)
        .join(StaffTable).on(_._1.staffId === _.id)
        .result
 } map { prescription =>
      val data = prescription.groupBy(_._1._1.id).map {
        case (prescriptionId, entries) =>
         val entry = entries.head._1._1
         val entry1 = entries.head._1._2
         val entry2 = entries.head._2

         
         
          PrescriptionHistoryList(
            entry.id,
            entry.prescriptionId,
            entry.patientId,
            entry.staffId,
            entry2.title +" "+ entry2.name +" "+ convertOptionString( entry2.lastName),
            entry.dateCreated,
            entry1.medication,
            entry1.dosage,
            entry1.count,
            entry1.instructions,
            entry1.refills,
            entry1.genericSubstituteOk,
            entry1.category,
            entry1.indications,
            entry1.contraindications,
            entry1.medicationInteraction,
            entry1.useInOralImplantology,
            entry.deleted,
            entry.note
          )  
      }.toList.sortBy(_.id).reverse
      Ok(Json.toJson(data))
    }
  }

  def getPatient(patientId: Long) = silhouette.SecuredAction.async { request =>
    db.run {
        PrescriptionHistoryTable.filter(_.patientId === patientId)
        .join(PrescriptionTable).on(_.prescriptionId === _.id)
        .join(StaffTable).on(_._1.staffId === _.id)
        .result
 } map { prescription =>
      val data = prescription.groupBy(_._1._1.id).map {
        case (prescriptionId, entries) =>
         val entry = entries.head._1._1
         val entry1 = entries.head._1._2
         val entry2 = entries.head._2

          PrescriptionHistoryList(
            entry.id,
            entry.prescriptionId,
            entry.patientId,
            entry.staffId,
            entry2.title +" "+ entry2.name +" "+ convertOptionString( entry2.lastName),
            entry.dateCreated,
            entry1.medication,
            entry1.dosage,
            entry1.count,
            entry1.instructions,
            entry1.refills,
            entry1.genericSubstituteOk,
            
            entry1.category,
            entry1.indications,
            entry1.contraindications,
            entry1.medicationInteraction,
            entry1.useInOralImplantology ,
            entry.deleted,
            entry.note
            )  
      }.toList.sortBy(_.id).reverse
      Ok(Json.toJson(data))
    }
  }


    def create = silhouette.SecuredAction.async(parse.json) { request =>
    
    val prescriptionHistory = for {
         prescriptionId <- (request.body \ "prescriptionId").validate[Long]
            patientId <- (request.body \ "patientId").validate[Long]
            //staffId <- (request.body \ "staffId").validate[Long]
            //dateCreated <- (request.body \ "dateCreated").validate[LocalDate]
    } yield { PrescriptionHistoryRow(None, prescriptionId, patientId, convertOptionLong(request.identity.id),LocalDate.now,false,Some("")) }

    prescriptionHistory match {
      case JsSuccess(prescriptionHistoryRow, _) => db.run {
       PrescriptionHistoryTable.returning(PrescriptionHistoryTable.map(_.id)) += prescriptionHistoryRow 
       
      } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(prescriptionHistoryRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    (request.body \ "note").validateOpt[String] match {
      case JsSuccess(note, _) =>
        db.run {
          PrescriptionHistoryTable.filter(_.id === id).map(_.note).update(note)
        } map {
          case 0 => NotFound
          case _ => Ok
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def generatePdf(patientId: Option[String]) =silhouette.SecuredAction.async { request =>
    var patientName = ""
    var prescriptions =""
    val patientFullName = db.run {
      for {
        step <- StepTable.filter(_.treatmentId === convertStringToLong(patientId)).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
      } yield {
        val firstName = (step.value \ "full_name").as[String]
        val lastName = (step.value \ "last_name").as[String]
        patientName = firstName + " " + lastName
      }
    }
    var AwaitResult = Await.ready(patientFullName, atMost = scala.concurrent.duration.Duration(10, SECONDS))

      db.run {
        PrescriptionHistoryTable.filterNot(_.deleted).filter(_.patientId === convertStringToLong(patientId))
                  .join(PrescriptionTable).on(_.prescriptionId === _.id)
                  .join(StaffTable).on(_._1.staffId === _.id)
                  .result
      } map { prescription => prescription.map{prescriptionList => prescriptions = prescriptionList.toString }

           val data = prescription.groupBy(_._1._1.id).map {
              case (id,entries) => 

                val prescriptionHistory = entries.head._1._1
                val prescription = entries.head._1._2
                val staff = entries.head._2
                /*.join(StepTable).on(_._1._1.patientId === _.treatmentId)
                val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

                val firstName = convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
                val lastName =  convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))


                patientName = firstName+" "+lastName*/
                val date = prescriptionHistory.dateCreated
                val doctorName = staff.title +" "+staff.name +" "+convertOptionString(staff.lastName)
                val medication = prescription.medication
                val dosage = prescription.dosage
                val count = prescription.count

                PrescriptionHistoryPDF(
                  date,
                  doctorName,
                  medication,
                  dosage,
                  count
                )
                }.toList

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter
 
        class MyFooter extends PdfPageEventHelper {

          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

  
          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);
                
          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);

          var cell = new PdfPCell(new Phrase("Prescription History Report", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;
          if(prescriptions != null && prescriptions != ""){

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 14)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);
          var cell1 = new PdfPCell(new Phrase("Name : "+patientName, FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          //cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

            (data).foreach(row => {
              val formatIncomming = new java.text.SimpleDateFormat("yyyy-MM-dd")
              val formatOutgoing = new java.text.SimpleDateFormat("dd MMM, yyyy")    
              val dateFormated = formatOutgoing.format(formatIncomming.parse(row.dateCreated.toString))

            var sampleData = List(s"${dateFormated}", s"${row.doctorName}", s"${row.medication}", s"${row.dosage}",
             s"${convertOptionalInt(row.count)}")
            
            var columnWidths:Array[Float] = Array(15,25,19,19,19);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Prescribed By", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Medication", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Dosage", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Count", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                //cell.setPaddingLeft(5)
                cell.setPadding(13);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_LEFT);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
            })

            document.add(table);
            
            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 10
            }
          })
          } else {

            var columnWidths:Array[Float] = Array(15,25,19,19,19);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(10);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Prescribed By", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(10);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Medication", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(10);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Dosage", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(10);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Count", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(10);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              document.add(table);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(97);
          var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);
          }

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"Prescription History"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

    def delete(id: Long) = silhouette.SecuredAction.async {
    db.run {
      PrescriptionHistoryTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }


  def samplePDF(patientId: Option[String]) =silhouette.SecuredAction.async { request =>
      var patientName = ""
      var prescriptions =""
      db.run {
          PrescriptionHistoryTable.filterNot(_.deleted).filter(_.patientId === convertStringToLong(patientId))
                    .join(PrescriptionTable).on(_.prescriptionId === _.id)
                    .join(StaffTable).on(_._1.staffId === _.id)
                    .join(StepTable).on(_._1._1.patientId === _.treatmentId)
                    .result
        } map { prescription => prescription.map{prescriptionList => prescriptions = prescriptionList.toString }

             val data = prescription.groupBy(_._1._1._1.id).map {
                case (id,entries) =>

                  val prescriptionHistory = entries.head._1._1._1
                  val prescription = entries.head._1._1._2
                  val staff = entries.head._1._2
                  val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

                  val firstName = convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
                  val lastName =  convertOptionString( steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))


                  patientName = firstName+" "+lastName
                  val date = prescriptionHistory.dateCreated
                  val doctorName = staff.title +" "+staff.name +" "+convertOptionString(staff.lastName)
                  val medication = prescription.medication
                  val dosage = prescription.dosage
                  val count = prescription.count

                  PrescriptionHistoryPDF(
                    date,
                    doctorName,
                    medication,
                    dosage,
                    count
                  )
                  }.toList

        val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }

          val document = new Document(PageSize.A4.rotate(),0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(30, 30, 15 , 15)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

           val currentDirectory = new java.io.File(".").getCanonicalPath
           val FONT_TITLE = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arialbd.ttf", 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arialbd.ttf", 11)

            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

            var cell = new PdfPCell(new Phrase("Prescription History Report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

            document.add(new Paragraph("\n"));

            var rowsPerPage = 8;
            var recNum =  0;
            if(prescriptions != null && prescriptions != ""){

            val FONT_TITLE1 = FontFactory.getFont(currentDirectory+"/app/assets/fonts/Lato-Regular.ttf", 14)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(100);
            var cell1 = new PdfPCell(new Phrase("Name : "+patientName, FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            //cell1.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);

            document.add(new Paragraph("\n"));

              (data).foreach(row => {
                val formatIncomming = new java.text.SimpleDateFormat("yyyy-MM-dd")
                val formatOutgoing = new java.text.SimpleDateFormat("dd MMM, yyyy")
                val dateFormated = formatOutgoing.format(formatIncomming.parse(row.dateCreated.toString))

              var sampleData = List(s"${dateFormated}", s"${row.doctorName}", s"${row.medication}", s"${row.dosage}",
               s"${convertOptionalInt(row.count)}")

              var columnWidths:Array[Float] = Array(15,25,19,19,19);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 || recNum % rowsPerPage == 0) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Prescribed By", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Medication", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Dosage", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Count", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {

                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  //cell.setPaddingLeft(5)
                  cell.setPadding(13);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_LEFT);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
              })

              document.add(table);

              recNum = recNum+1;
              if(recNum == 0 || recNum % rowsPerPage == 0) {
                document.newPage();
              }
              if(recNum==9 && rowsPerPage!=10){
                recNum = 1
                rowsPerPage = 10
              }
            })
            } else {

              var columnWidths:Array[Float] = Array(15,25,19,19,19);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(10);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Prescribed By", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(10);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Medication", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(10);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Dosage", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(10);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Count", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(10);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                document.add(table);

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);
            }

            document.close();

          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"Prescription History"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
    }
}


