package controllers.v2

import co.spicefactory.util.BearerTokenEnv
import com.google.inject.{Inject, Singleton}
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import controllers.v1.TreatmentsController
import models.daos.tables.{ProviderTableRow, StaffRow, IdentityRow, UserRow}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.Application
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class FormsController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application, treatmentController: TreatmentsController)(implicit ec: ExecutionContext) extends NovadonticsController {
  import driver.api._

  private val serverUrl = application.configuration.getString("serverUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))

  case class Slide(title: String, rows: List[Row])
  case class Row(columns: Option[List[Column]])
  case class Column(columnItems: Option[List[ColumnItem]])
  case class ColumnItem(field: Option[Field], fieldGroup: Option[FieldGroup])
  case class FieldGroup(title: String, name: Option[String], radio: Option[Boolean], required: Option[Boolean], fields: Option[List[Field]], info: Option[InfoBlock])
  case class InfoBlock(name: Option[String], title: Option[String], required: Option[Boolean], text: Option[String], image: Option[String])
  case class OptionItem(value: Option[String], title: Option[String])
  case class Item(title: Option[String], url: Option[String], thumbnail: Option[String])
  case class FieldAttributes(options: Option[List[OptionItem]], images: Option[Boolean], printTemplate: Option[String], info: Option[InfoBlock], note: Option[Boolean], calendar: Option[Boolean], color: Option[String])

  case class Field (
    name: Option[String],
    fieldType: Option[String],
    title: Option[String],
    required: Option[Boolean],
    disabled: Option[Boolean],
    attributes: Option[FieldAttributes],
    info: Option[String],
    postfix: Option[String],
    options: Option[List[OptionItem]],
    items: Option[List[Item]],
    mediaType: Option[String],
    url: Option[String]
  )

  object Field {
    implicit val reads: Reads[Field] = (
        (__ \ "name").readNullable[String] and
        (__ \ "type").readNullable[String] and
        (__ \ "title").readNullable[String] and
        (__ \ "required").readNullable[Boolean] and
        (__ \ "disabled").readNullable[Boolean] and
        (__ \ "attributes").readNullable[FieldAttributes] and
        (__ \ "info").readNullable[String] and
        (__ \ "postfix").readNullable[String] and
        (__ \ "options").readNullable[List[OptionItem]] and
        (__ \ "items").readNullable[List[Item]] and
        (__ \ "mediaType").readNullable[String] and
        (__ \ "url").readNullable[String]
      )(Field.apply _)

    implicit val writes: Writes[Field] = (
      (__ \ "name").writeNullable[String] and
        (__ \ "type").writeNullable[String] and
        (__ \ "title").writeNullable[String] and
        (__ \ "required").writeNullable[Boolean] and
        (__ \ "disabled").writeNullable[Boolean] and
        (__ \ "attributes").writeNullable[FieldAttributes] and
        (__ \ "info").writeNullable[String] and
        (__ \ "postfix").writeNullable[String] and
        (__ \ "options").writeNullable[List[OptionItem]] and
        (__ \ "items").writeNullable[List[Item]] and
        (__ \ "mediaType").writeNullable[String] and
        (__ \ "url").writeNullable[String]
      )(unlift(Field.unapply))
  }

  implicit val formatColumn = Json.format[Column]
  implicit val formatRow = Json.format[Row]

  object FieldAttributes {
    implicit val reads: Reads[FieldAttributes] = (
      (__ \ "options").readNullable[List[OptionItem]] and
      (__ \ "images").readNullable[Boolean] and
      (__ \ "printTemplate").readNullable[String] and
      (__ \ "info").readNullable[InfoBlock] and
      (__ \ "note").readNullable[Boolean] and
      (__ \ "calendar").readNullable[Boolean] and
      (__ \ "color").readNullable[String]
    )(FieldAttributes.apply _)

    implicit val writes: Writes[FieldAttributes] = (
      (__ \ "options").writeNullable[List[OptionItem]] and
      (__ \ "images").writeNullable[Boolean] and
      (__ \ "printTemplate").writeNullable[String] and
      (__ \ "info").writeNullable[InfoBlock] and
      (__ \ "note").writeNullable[Boolean] and
      (__ \ "calendar").writeNullable[Boolean] and
      (__ \ "color").writeNullable[String]
    )(unlift(FieldAttributes.unapply))
  }

  object OptionItem {
    implicit val reads: Reads[OptionItem] = (
      (__ \ "value").readNullable[String] and
      (__ \ "title").readNullable[String]
      )(OptionItem.apply _)

    implicit val writes: Writes[OptionItem] = (
      (__ \ "value").writeNullable[String] and
      (__ \ "title").writeNullable[String]
      )(unlift(OptionItem.unapply))
  }

  object Item {
    implicit val reads: Reads[Item] = (
      (__ \ "title").readNullable[String] and
      (__ \ "url").readNullable[String] and
      (__ \ "thumbnail").readNullable[String]
    )(Item.apply _)

    implicit val writes: Writes[Item] = (
      (__ \ "title").writeNullable[String] and
      (__ \ "url").writeNullable[String] and
      (__ \ "thumbnail").writeNullable[String]
      )(unlift(Item.unapply))
  }

  object InfoBlock {
    implicit val reads: Reads[InfoBlock] = (
      (__ \ "name").readNullable[String] and
      (__ \ "title").readNullable[String] and
      (__ \ "required").readNullable[Boolean] and
      (__ \ "text").readNullable[String] and
      (__ \ "image").readNullable[String]
    )(InfoBlock.apply _)

    implicit val writes: Writes[InfoBlock] = (
      (__ \ "name").writeNullable[String] and
      (__ \ "title").writeNullable[String] and
      (__ \ "required").writeNullable[Boolean] and
      (__ \ "text").writeNullable[String] and
      (__ \ "image").writeNullable[String]
    )(unlift(InfoBlock.unapply))
  }

  object FieldGroup {
    implicit val reads: Reads[FieldGroup] = (
      (__ \ "title").read[String] and
      (__ \ "name").readNullable[String] and
      (__ \ "radio").readNullable[Boolean] and
      (__ \ "required").readNullable[Boolean] and
      (__ \ "fields").readNullable[List[Field]] and
      (__ \ "info").readNullable[InfoBlock]
    )(FieldGroup.apply _)

    implicit val writes: Writes[FieldGroup] = (
      (__ \ "title").write[String] and
      (__ \ "name").writeNullable[String] and
      (__ \ "radio").writeNullable[Boolean] and
      (__ \ "required").writeNullable[Boolean] and
      (__ \ "fields").writeNullable[List[Field]] and
      (__ \ "info").writeNullable[InfoBlock]
    )(unlift(FieldGroup.unapply))
  }

  object ColumnItem {
    implicit val reads: Reads[ColumnItem] = (
      (__ \ "field").readNullable[Field] and
      (__ \ "group").readNullable[FieldGroup]
    )(ColumnItem.apply _)

    implicit val writes: Writes[ColumnItem] = (
      (__ \ "field").writeNullable[Field] and
      (__ \ "group").writeNullable[FieldGroup]
    )(unlift(ColumnItem.unapply))
  }

  object Slide {
      implicit val reads: Reads[Slide] = (
        (__ \ "title").read[String] and
        (__ \ "rows").read[List[Row]]
      )(Slide.apply _)

      implicit val writes: Writes[Slide] = (
        (__ \ "title").write[String] and
        (__ \ "rows").write[List[Row]]
      )(unlift(Slide.unapply))
    }

  object Output {

    case class Consent(
      id: Option[String],
      title: Option[String],
      document: Option[String]
    )

    object Consent {
      implicit val writes = Json.writes[Consent]
    }

    case class Form(
      id: String,
      visit: String,
      step: String,
      consent: Option[Consent]
    )

    implicit val writes = Writes { form: Output.Form =>
      Json.obj(
        "formId" -> form.id,
        "visit" -> form.visit,
        "step" -> form.step,
        "id" -> form.consent.getOrElse(Consent(None, None, None)).id,
        "title" -> form.consent.getOrElse(Consent(None, None, None)).title,
        "document" -> form.consent.getOrElse(Consent(None, None, None)).document
      )
    }
  }

  def mapForms(forms: Seq[FormTable#TableElementType]) =
    forms.flatMap {
      form => {
        form.schema.validateOpt[List[Slide]] match {
          case JsSuccess(schema, _) =>
            schema.getOrElse(List()).flatMap(
              slide => slide.rows.flatMap(
                row => row.columns.getOrElse(List()).flatMap(
                  column => column.columnItems.getOrElse(List()).flatMap(item => item.field)
                )
              )
            ).filter(_.attributes match {
              case Some(attributes) => attributes.printTemplate.isDefined
              case None => false
            }).map(field => Output.Consent(field.name, field.title, field.attributes.get.printTemplate))
              .map(consent => Output.Form(form.id, form.section, form.title, Some(consent)))
          case JsError(_) =>
            List()
        }
      }
    }

  def readAllConsents = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      FormTable.sortBy(t => (t.id.asc, t.version.asc)).result
    } map {
      forms => Ok(Json.toJson(mapForms(forms))
      )
    }
  }

  def updateConsent(id: String) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val values = for {
      title <- (request.body \ "title").validateOpt[String]
      document <- (request.body \ "document").validateOpt[String]
    } yield {
      (title, document)
    }

    values match {
      case JsSuccess((title, document), _) =>
        db.run {
          FormTable.result
        } map {
          forms =>
            mapForms(forms).find(r => r.consent.isDefined && r.consent.get.id.get == id) match {
              case Some(consent) =>
                val form = forms.find(_.id == consent.id).get
                form.schema.validateOpt[List[Slide]] match {
                  case JsSuccess(schema, _) =>
                    val newSchema = schema.get.map(
                      slide => Slide.apply(slide.title, slide.rows.map {
                        row => Row.apply(Some(row.columns.get.map {
                          column => Column.apply(Some(column.columnItems.get.map {
                            case item if item.field.isDefined && item.field.get.name.get == id =>
                              var field = item.field.get
                              var attributes = field.attributes.get
                              if (document.isDefined) {
                                attributes = attributes.copy(printTemplate = document)
                                field = field.copy(attributes = Some(attributes))
                              }
                              if (title.isDefined) {
                                field = field.copy(title = title)
                              }
                              item.copy(Some(field), item.fieldGroup)
                            case item => item
                          }))
                        }))
                      })
                    )

                    db.run {
                      FormTable.filter(_.id === form.id).map(_.schema).update(Json.toJson(newSchema))
                    } map (_ => Ok)

                    Ok
                  case JsError(_) => NotFound
                }
              case _ => NotFound
            }
        }
      case JsError(_) => Future(BadRequest)
    }
  }

  def incrementId(id: String) = {
    val i = id.indexOf('_')
    val parts = id.splitAt(if (i > -1) {i + 1} else id.length)
    s"${parts._1}${if (i > -1) "" else "_"}${Try(parts._2.toInt).getOrElse(0) + 1}"
  }

  def getSuffix(id: String) = {
    val parts = id.splitAt(if (id.indexOf('_') > -1) id.indexOf('_') + 1 else id.length)
    Try(parts._2.toInt).getOrElse(0)
  }

  def addConsent = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val item = for {
      title <- (request.body \ "title").validate[String]
      document <- (request.body \ "document").validateOpt[String]
      step <- (request.body \ "step").validate[String]
    } yield {
      (step, Field(None, Some("Checkbox"), Some(title), Some(false), None, Some(FieldAttributes(None, Some(true), document, None, Some(false), None, None)), None, None, None, None, None, None))
    }

    item match {
      case JsSuccess((step, consent), _) =>
        db.run {
          FormTable.filter(_.title === step).result
        } map {
          forms =>
            val lastConsent = mapForms(forms).map(_.consent).filter(_.isDefined).map(_.get).maxBy(a => getSuffix(a.id.getOrElse("")))
            val newConsent = consent.copy(name = Some(incrementId(lastConsent.id.get)))
            val form = forms.head
            form.schema.validateOpt[List[Slide]] match {
              case JsSuccess(schema, _) =>
                val newSchema = schema.get.map(
                  slide => Slide.apply(slide.title, slide.rows.map {
                    row => Row.apply(Some(row.columns.get.map {
                      column => Column.apply(Some(column.columnItems.get.flatMap {
                        case colItem if colItem.field.isDefined && colItem.field.get.name == lastConsent.id =>
                          List(colItem, ColumnItem(Some(newConsent), None))
                        case colItem => List(colItem)
                      }))
                    }))
                  })
                )

                db.run {
                  FormTable.filter(_.id === form.id).map(_.schema).update(Json.toJson(newSchema))
                } map (_ => Ok)

                Created(Json.toJson(
                  Output.Form(form.id, form.section, form.title,
                    Some(Output.Consent(newConsent.name, newConsent.title, newConsent.attributes.get.printTemplate))
                  )))
              case JsError(_) => NotFound
            }
        }
      case JsError(_) =>
        Future(BadRequest)
    }
  }

  def prescriptionCallback(prescription: PrescriptionTable#TableElementType): String =
    s"$serverUrl/v2/prescription/${prescription.id.get}/pdf"

  def prescriptionInfo(prescription: PrescriptionTable#TableElementType): Option[InfoBlock] =
    if(prescription.instructions.isDefined) Some(InfoBlock(None, None, None, prescription.instructions, None)) else None

  def prescriptionTitle(prescription: PrescriptionTable#TableElementType): String =
    s"${prescription.medication} ${prescription.dosage}, ${prescription.count.getOrElse(0)} count"

  def formPrescriptionSchema(prescriptions: Seq[PrescriptionTable#TableElementType]): List[Slide] = {
    val items = prescriptions.map(p =>
      Field(Some(s"2j_2_${p.id.get}"), Some("Checkbox"), Some(prescriptionTitle(p)), Some(false), Some(p.deleted),
        Some(FieldAttributes(None, Some(false), Some(prescriptionCallback(p)), prescriptionInfo(p), Some(false), None, None)), None, None, None, None, None, None)
    ).map(field => ColumnItem(Some(field), None)).toList

    List(Slide("2H RX", List(Row(Some(List(Column(Some(items)))))))
    )
  }

  def readForms = silhouette.SecuredAction.async { request =>

    var userRole = ""
    var strPracticeId: Long = 0

    val query = request.identity match {
      case UserRow(id, _, _, _, _, _, _) =>
        UserTable.filter(_.id === id).map(_.role)
      case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
        StaffTable.filter(_.id === id).map(_.role)
    }
    var userRoleData = db.run {  query.result } map { result => userRole = result.head.toString }
    var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

    if(userRole == "Dentist") {
      strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
    }

    val obj =  Json.obj()
    var newObj : JsValue = null
    var providerFinalObj : List[JsValue] = List()
    var referralFinalObj : List[JsValue] = List()
    var referredFinalObj : List[JsValue] = List()
    var carrierNameFinalObj : List[JsValue] = List()
    var feeScheduleFinalObj : List[JsValue] = List()
    var providerJson: JsValue = null
    var referralSourceJson: JsValue = null
    var referredToJson: JsValue = null
    var carrierNameJson: JsValue = null
    var feeScheduleJson: JsValue = null
    var chartDiabledValue = """{"field":{"max":16,"name":"patient_id","type":"Input","title":"Patient ID / Chart Number","required":true,"chartDisabled":true}}"""

    val skipFormList=List("2J","1I","3G","3I","2D1","2H","3C","3D")
    db.run (
      FormTable.filterNot(_.id inSet skipFormList).sortBy(t => (t.id.asc, t.version.asc)).result zip PrescriptionTable.result
    ) map {
      case (forms, prescriptions) =>
        val data = forms.groupBy(_.section).map {
          case (title, sectionForms) =>
            val finalForms = sectionForms.map { form =>
              var schema = if (form.id == "2H") Json.toJson(formPrescriptionSchema(prescriptions)) else form.schema
              if ( form.id == "1A" ) {
                if(userRole == "Dentist"){
                  var providerList = db.run {
                    ProviderTable.filterNot(_.deleted).filter(_.practiceId === strPracticeId).sortBy(r => (r.firstName, r.lastName)).result
                  } map { providersList =>
                    providersList.foreach(result => {
                      val id = convertOptionLong(result.id).toString
                      val name = result.title + " " + result.firstName +" "+ result.lastName

                      newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                      providerFinalObj = providerFinalObj :+ newObj
                    })

                    providerJson = JsObject(Seq("options" -> JsArray(providerFinalObj)))
                  }
                  var AwaitResult = Await.ready(providerList, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                } else {
                    var providerList1 = db.run {
                      ProviderTable.filterNot(_.deleted).sortBy(r => (r.firstName, r.lastName)).result
                    } map { providersList =>
                      providersList.foreach(result => {
                        val id = convertOptionLong(result.id).toString
                        val name = result.firstName +" "+ result.lastName

                        newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                        providerFinalObj = providerFinalObj :+ newObj
                      })

                      providerJson = JsObject(Seq("options" -> JsArray(providerFinalObj)))
                    }
                    var AwaitResult1 = Await.ready(providerList1, atMost = scala.concurrent.duration.Duration(20, SECONDS))
                }

              // CARRIER NAME LIST
              newObj = obj + ("title" -> JsString("UCR")) + ("value" -> JsString("-1"))
              carrierNameFinalObj = carrierNameFinalObj :+ newObj
              newObj = obj + ("title" -> JsString("Private")) + ("value" -> JsString("0"))
              carrierNameFinalObj = carrierNameFinalObj :+ newObj

                var carrierNameList = db.run {
                    DentalCarrierTable.filterNot(_.archived).filter(_.practiceId === strPracticeId).result
                  } map { carrierList =>
                    carrierList.foreach(result => {
                      val id = convertOptionLong(result.id).toString
                      val name = result.name

                      newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                      carrierNameFinalObj = carrierNameFinalObj :+ newObj
                    })
                    carrierNameJson = JsObject(Seq("options" -> JsArray(carrierNameFinalObj)))
                  }
                  var AwaitResult2 = Await.ready(carrierNameList, atMost = scala.concurrent.duration.Duration(30, SECONDS))

            //FEE SCHEDULE LIST
                  var feeScheduleList = db.run {
                    InsuranceCompanyTable.filterNot(_.deleted).result
                  } map { scheduleList =>
                    scheduleList.foreach(result => {
                      val id = convertOptionLong(result.id).toString
                      val name = result.name

                      newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                      feeScheduleFinalObj = feeScheduleFinalObj :+ newObj
                    })
                    feeScheduleJson = JsObject(Seq("options" -> JsArray(feeScheduleFinalObj)))
                  }
                  var AwaitResult3 = Await.ready(feeScheduleList, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              //REFERRAL SOURCE LIST
                  var referralSourceList = db.run{
                        ReferralSourceTable.filterNot(_.archived).filter(_.practiceId === strPracticeId).result
                  } map { referralSourcesList => {
                      referralSourcesList.foreach(result => {
                        var id = convertOptionLong(result.id).toString
                        var name = result.value

                      newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                      referralFinalObj = referralFinalObj :+ newObj
                    })
                    referralSourceJson = JsObject(Seq("options" -> JsArray(referralFinalObj)))
                    }
                  }

                var AwaitResult1 = Await.ready(referralSourceList, atMost = scala.concurrent.duration.Duration(30, SECONDS))
            //CHART NUMBER DISABLED VALUE
               var chartValue =  db.run{
                  PracticeTable.filter(_.id === strPracticeId).result.head
                } map { practiceList =>
                      chartDiabledValue = convertOptionString(practiceList.chartNumberType)
                }
                Await.ready(chartValue, atMost = scala.concurrent.duration.Duration(30, SECONDS))

            //REFERRED TO LIST
                  var referredToList = db.run{
                        ReferredToTable.filterNot(_.deleted).filter(_.practiceId === strPracticeId).result
                  } map { referredToList => {
                      referredToList.foreach(result => {
                        var id = convertOptionLong(result.id).toString
                        var name = result.name

                      newObj =  obj + ("title" -> JsString(name)) + ("value" -> JsString(id))
                      referredFinalObj = referredFinalObj :+ newObj
                    })
                    referredToJson = JsObject(Seq("options" -> JsArray(referredFinalObj)))
                    }
                  }

                var AwaitResult4 = Await.ready(referredToList, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                
                var minifiedProvider: String = Json.stringify(providerJson)
                var providerReplace = minifiedProvider.substring(1, minifiedProvider.length() - 1)
                var minifiedSchema: String = Json.stringify(form.schema)
                var schemaReplace = minifiedSchema.replace(""""options":[{"title":"providerTitle","value":"providerValue"}]""", providerReplace)

                var minifiedReferralSource: String = Json.stringify(referralSourceJson)
                var referralSourceReplace = minifiedReferralSource.substring(1, minifiedReferralSource.length() - 1)
                var schemaReplace_1 = schemaReplace.replace(""""options":[{"title":"referralSourceTitle","value":"referralSourceValue"}]""", referralSourceReplace)

                var minifiedCarrierName: String = Json.stringify(carrierNameJson)
                var carrierNameReplace = minifiedCarrierName.substring(1, minifiedCarrierName.length() - 1)
                var schemaReplace_2 = schemaReplace_1.replace(""""options":[{"title":"carrierTitle","value":"carrierValue"}]""", carrierNameReplace)

                var minifiedReferredTo: String = Json.stringify(referredToJson)
                var referredToReplace = minifiedReferredTo.substring(1, minifiedReferredTo.length() - 1)
                var schemaReplace_3 = schemaReplace_2.replace(""""options":[{"title":"referredToTitle","value":"referredToValue"}]""", referredToReplace)

                var minifiedFeeSchedule: String  = Json.stringify(feeScheduleJson)
                var feeScheduleReplace = minifiedFeeSchedule.substring(1, minifiedFeeSchedule.length() - 1)
                var finalSchema = schemaReplace_3.replace(""""options":[{"title":"feeScheduleTitle","value":"feeScheduleValue"}]""", feeScheduleReplace)

                if(chartDiabledValue == "" || chartDiabledValue == "none"){
                    finalSchema = finalSchema.replace(""""chartDisabled":true""", """"disabled":false""").replace(""""chartRequired":true""", """"required":true""");
                  } else {
                    finalSchema = finalSchema.replace(""""chartDisabled":true""", """"disabled":true""").replace(""""chartRequired":true""", """"required":false""");
                  }
                schema = Json.parse(finalSchema);
              }
              treatmentController.Output.Form(form.id, form.version, form.title, form.icon, schema, form.print, form.listOrder,form.types)
            }
            treatmentController.Output.Section(title, finalForms.toList)
        }.toList.sortBy(_.forms.head.id)
        Ok(Json.toJson(data))
    }
  }
}
