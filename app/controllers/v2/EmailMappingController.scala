package controllers.v2

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.UserEmailMappingRow
import play.api.Application
import play.api.libs.json.{JsError, JsSuccess, Json, Writes}
import play.api.mvc.Result
import models.daos.tables._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EmailMappingController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._


  implicit val emailMappingWrites = Writes { row : (EmailMappingRow, List[Option[UserRow]]) =>
    val (emailMap, users) = row
    Json.obj(
      "id" -> emailMap.id,
      "name" -> emailMap.name,
      "defaultEmail" -> emailMap.defaultEmail,
      "title" -> emailMap.title,
      "isConsultation" -> emailMap.isConsultation,
      "emailOrder" -> emailMap.emailOrder,
      "users" -> users.map(u => {
        if(u.isDefined){
          val user = u.get
          Json.obj(
          "id" ->user.id,
          "name" -> user.name,
          "email" -> user.email
          )}
        else Json.obj()}
      )
    )
  }

  def baseRead(id: Option[Long]) = {
    db.run {
      val query = if(id.isDefined) EmailMappingTable.filter(_.id === id) else EmailMappingTable
      query
        .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
        .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
        .result
    } map { data =>
      val rows = data.groupBy(_._1._1).map {
        case (emailMap, group) => {
          val users = group.map {
            case ((_, _), user) => user
          }.toList
          (emailMap, users)
        }
      }
      Ok(Json.toJson(rows.toList.sortWith(_._1.id.get < _._1.id.get)))
    }
  }

  def readAll = silhouette.SecuredAction.async {
    baseRead(None)
  }

  def read(id: Long) = silhouette.SecuredAction.async {
    baseRead(Some(id))
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val em = for {
      title <- (request.body \ "title").validateOpt[String]
      defaultEmail <- (request.body \ "defaultEmail").validateOpt[String]
    } yield {
      (title, defaultEmail)
    }

    em match {
      case JsSuccess((title, defaultEmail), _) => db.run {
        val query = EmailMappingTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            defaultEmail.map(value => query.map(_.defaultEmail).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }

  }

  def addUser(id: Long, userId: Long) = silhouette.SecuredAction.async {
    db.run{
      UserEmailMappingTable.filter(r => r.userId === userId && r.emailMappingId === id).exists.result flatMap {
        case true =>
          DBIO.successful(PreconditionFailed)
        case false =>
          (UserEmailMappingTable.returning(UserEmailMappingTable.map(_.id)) += UserEmailMappingRow(None, userId, id)) map {
            case 0L => PreconditionFailed
            case id => Created
          }
      }
    }
  }

  def removeUser(id: Long, userId: Long) = silhouette.SecuredAction.async {
    db.run{
      UserEmailMappingTable.filter(uem => uem.emailMappingId === id && uem.userId === userId).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

  def create = silhouette.SecuredAction.async {
    Future(NotImplemented)
  }

  def delete(id: Long) = silhouette.SecuredAction.async {
    Future(NotImplemented)
  }
}
