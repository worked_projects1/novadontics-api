package controllers.v2

import co.spicefactory.services.EmailService
import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.Inject
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.{EducationRequestRow, StaffRow, CourseRow,CeHistoryRow,CeCoursePurchaseRow}
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

import scala.concurrent.{ExecutionContext, Future}
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};
import scala.collection.mutable.ListBuffer

class EducationRequestController @Inject()(ses: AmazonSimpleEmailService, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  private val replyTo = application.configuration.getString("aws.ses.education.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.education.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val from = application.configuration.getString("aws.ses.education.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))

  private val emailService = new EmailService(ses, application)

  implicit val requestStatus = new Format[EducationRequestRow.Status] {
    import EducationRequestRow.Status._
    override def writes(status: EducationRequestRow.Status) = Json.toJson {
      status match {
        case Open => "Open"
        case Closed => "Closed"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Open" => Open
      case "Closed" => Closed
    }
  }

  implicit val educationRequestFormat = Json.format[EducationRequestRow]
  implicit val ceHistoryFormat = Json.format[CeHistoryRow]

  implicit val educationRequestRowWrites = Writes { request: (EducationRequestRow, StaffRow, CourseRow) =>
    val (educationRow, staffRow, courseRow) = request
    var dentistName = staffRow.title +" "+ staffRow.name +" "+ convertOptionString(staffRow.lastName)
    Json.obj(
      "id" -> educationRow.id,
      "dateRequested" -> educationRow.dateCreated,
      "status" -> educationRow.status,
      "returnQuestionnaire" -> educationRow.returnQuestionnaire,
      "closedDate" -> educationRow.closedDate,
      "note" -> educationRow.note,
      "dentist" -> Json.obj(
        "id" -> staffRow.id,
        "name" -> dentistName,
        "phone" -> staffRow.phone,
        "email" -> staffRow.email
      ),
      "course" -> Json.obj(
        "id" -> courseRow.id,
        "courseId" -> courseRow.courseId,
        "title" -> courseRow.title,
        "category" -> courseRow.category,
        "speaker" -> courseRow.speaker
      )
    )
  }

  def readAll = silhouette.SecuredAction.async { request =>
    
    val action = EducationRequestTable
      .filterNot(_.deleted)
      .join(StaffTable).on(_.staffId === _.id)
      .join(CoursesTable).on(_._1.courseId === _.id)

    db.run {
      request.identity match {
        case row: StaffRow => action.filter(_._1._2.id === request.identity.id).result
        case _ => action.result
      }
    } map { result =>
      val data = result.groupBy(_._1._1.id).map { row =>
        val (_, group) = row
        val ((educationRow, staffRow), courseRow) = group.head
        (educationRow, staffRow, courseRow)
      }.toList.sortBy(_._1.dateCreated).reverse

      Ok(Json.toJson(data))
    }
  }


  def read(id: Long) = silhouette.SecuredAction.async {
    db.run {
      EducationRequestTable
        .filter(_.id === id)
        .filterNot(_.deleted)
        .join(StaffTable).on(_.staffId === _.id)
        .join(CoursesTable).on(_._1.courseId === _.id)
        .result
        .headOption.flatMap[Result, NoStream, Effect.Read] {
        case Some(((educationRow, staffRow), courseRow)) =>
          DBIO.successful(Ok(Json.toJson((educationRow, staffRow, courseRow))))
        case None => DBIO.successful(NotFound)
      }
    }
  }

  def update(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val educatioRequest = for {
      status <- (request.body \ "status").validateOpt[EducationRequestRow.Status]
      returnQuestionnaire <- (request.body \ "returnQuestionnaire").validateOpt[String]
    } yield {
      (status, returnQuestionnaire)
    }
    educatioRequest match {
      case JsSuccess((status, returnQuestionnaire), _) =>
      var closedDate: Option[LocalDate] = None
      if(status == Some(EducationRequestRow.Status.Closed)){
        closedDate = Some(LocalDate.now())
      }
        db.run {
          val query = EducationRequestTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true => DBIO.seq[Effect.Write](
              status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              returnQuestionnaire.map(value => query.map(_.returnQuestionnaire).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              Some(closedDate).map(value => query.map(_.closedDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
            ) map {_ => Ok}
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }

  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
    var maxRequestAllowed = 10
    var fmtYear = DateTimeFormat.forPattern("yyyy");
    var fmtDate = DateTimeFormat.forPattern("yyyy-MM-dd");
    var staffId = request.identity.id.get
    var allowCECourses = false

    var currentYear = fmtYear.print(DateTime.now);
    var CurrentYearStartDate = DateTime.parse(currentYear+"-01-01")
    var nextYear = currentYear.toInt + 1
    var NextYearStartDate = DateTime.parse(nextYear+"-01-01")
    var requestCount = 0
    var courseQuestionnaire = <p></p>
    var courseValue: Long = 0

    val payload = for {
        courseId <- (request.body \ "courseId").validate[Long]
      } yield {
        courseValue = courseId
    }

    var f=db.run{
      for {
          watchHistory <-  CeHistoryTable
                                .filter(_.dateCreated >= CurrentYearStartDate)
                                .filter(_.dateCreated < NextYearStartDate)
                                .filter(_.staffId === staffId).map(_.courseId).countDistinct.result
          staff <- StaffTable.filter(_.id === staffId).result.head
      }yield{
          // requestCount = watchHistory.length
          requestCount = watchHistory

          if(convertOptionalBoolean(staff.allowAllCECourses)){
              allowCECourses = true
          } else {
            if(staff.appSquares.toString contains "CE3"){
              maxRequestAllowed = 3
            } else if(staff.appSquares.toString contains "CE5"){
              maxRequestAllowed = 5
            }
            if(requestCount < maxRequestAllowed){
              allowCECourses = true
            } else {
             var historyBlock = db.run{
                CeHistoryTable.filter(_.staffId === staffId).filter(_.courseId === courseValue).length.result
              } map { historyLength =>
                 if(historyLength > 0) {
                   allowCECourses = true
                 }
              }
              Await.ready(historyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

             var purchaseBlock = db.run{
                CeCoursePurchaseTable.filter(_.staffId === staffId).filter(_.courseId === courseValue).length.result
              } map { purchaseLength =>
                 if(purchaseLength > 0) {
                   allowCECourses = true
                 }
              }
              Await.ready(purchaseBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          }
        }
      }
    }
    val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))

    if (!allowCECourses){
        Future.successful(Ok(Json.parse(s"""{"resultStatus":"failed","message":"You have already reached the maximum number of requests for this calendar year. Only ${maxRequestAllowed} requests can be initiated annually  per your current Novadontics plan. However, You can upgrade to a different plan to watch and earn credits from more CE courses or you can purchase this course by clicking on the purchase button"}""")))
    }else{
      val payload = for {
        courseId <- (request.body \ "courseId").validate[Long]
        note <- (request.body \ "note").validateOpt[String]
      } yield {
        EducationRequestRow(None, courseId, request.identity.id.get, note, DateTime.now, EducationRequestRow.Status.Open,None,None)
      }

      payload match {
        case JsSuccess(educationRow, _) => db.run {
          EducationRequestTable.returning(EducationRequestTable.map(_.id)) += educationRow
        } map {
          case 0L => PreconditionFailed
          case id =>
          var ceHistoryRow = CeHistoryRow(None,staffId,educationRow.courseId,DateTime.now,"Request CE")
           var block = db.run {
                  CeHistoryTable.returning(CeHistoryTable.map(_.id)) += ceHistoryRow
                } map {
                  case 0L => PreconditionFailed
                  case id => Ok
                }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

            val action = for {
              ceRequest <- EducationRequestTable if ceRequest.id === id
              dentist <- StaffTable if ceRequest.staffId === dentist.id
              course <- CoursesTable if ceRequest.courseId === course.id
            } yield (ceRequest, dentist, course)

            val addresses = db.run{
              EmailMappingTable.filter(_.name === "earnCE")
                .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                .result
            }.map { data =>
              val defaultAddresses = data.head._1._1.defaultEmail.getOrElse("").split(",").toList
              data.filter(_._2.isDefined).map(_._2.get).map(user => s"${user.name} <${user.email}>").toList ::: defaultAddresses
            }

            db.run {
              action.result.headOption
            } map {
              case Some((ceRequest, dentist, course)) =>
              var dentistName = dentist.title +" "+ dentist.name +" "+ convertOptionString(dentist.lastName)
              var questionnaireLink = convertOptionString(course.courseQuestionnaire);
                if(questionnaireLink != "") {
                  courseQuestionnaire = <div><p>Please find the questionnaire in the link below:<br /><a style='color: darkorange' href={questionnaireLink}>{questionnaireLink}</a></p><br /><p style='color: red'>Please complete the above questionnaire and email it back to us at <a style='color: red' href="mailto:team@novadontics.com">team@novadontics.com</a>. We will then generate a CE certificate with the appropriate CE credits and email it back to you.</p></div>
                }
                val fmt = DateTimeFormat.forPattern("dd MMM yyyy HH:mm:ss")
                emailService.sendEmail(List(s"${dentistName} <${dentist.email}>"), from, replyTo, subject, views.html.requestedCEConfirmation(dentistName, course.title, ceRequest.id.get, course.speaker.getOrElse("Novadontics Team"), courseQuestionnaire))

                addresses.map(
                  emailService.sendEmail(_, from, replyTo, "CE Request No. " + ceRequest.id.get, views.html.requestedCE(dentistName, dentist.email, fmt.print(ceRequest.dateCreated), course.title, course.courseId.getOrElse("N/A"), ceRequest.id.get, ceRequest.note.getOrElse("N/A"), course.category.getOrElse("N/A"), course.speaker.getOrElse("Novadontics Team")))
                )
              case _ => Future.successful(InternalServerError)
            }
            // Created(Json.toJson(educationRow.copy(id = Some(id))))
            Created(Json.parse("""{"resultStatus":"passed","message":"Your request for a CE credit has been successfully sent. A Novadontics team member will be contacting you shortly."}"""))
        }
        case JsError(_) => Future.successful(BadRequest)
      }
    }  
  }

  def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      EducationRequestTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0L => NotFound
      case _ => Ok
    }
  }

 def ceWatchHistoryMigration = silhouette.SecuredAction.async(parse.json) { request =>
  db.run{
    EducationRequestTable.filterNot(_.deleted).result
  } map { educationList =>
    educationList.foreach(rows => {
      var ceHistoryRow = CeHistoryRow(None,rows.staffId,rows.courseId,rows.dateCreated,"Request CE")
      db.run{
       CeHistoryTable.returning(CeHistoryTable.map(_.id)) += ceHistoryRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(ceHistoryRow.copy(id = Some(id))))
            }
    })

  var b = db.run{
    ConEdTrackTable.result
  } map { trackList =>
    trackList.foreach(rows => {
      var ceHistoryRow = CeHistoryRow(None,rows.staffId,rows.courseId,DateTime.parse(rows.date.toString),"Watch")
      db.run{
       CeHistoryTable.returning(CeHistoryTable.map(_.id)) += ceHistoryRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(ceHistoryRow.copy(id = Some(id))))
            }
    })
  }
    Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))

  Ok { Json.obj("status"->"success","message"->"Education request migrated successfully")}
  }
 }

 def trackCECourses(courseId: Long) = silhouette.SecuredAction.async { request =>
 var staffId = request.identity.id.get
 var allowCECourses = false
 var response = false
 var fmtYear = DateTimeFormat.forPattern("yyyy");
 var message = ""
 var maxRequestAllowed = 10

 var currentYear = fmtYear.print(DateTime.now);
 var CurrentYearStartDate = DateTime.parse(currentYear+"-01-01")
 var nextYear = currentYear.toInt + 1
 var NextYearStartDate = DateTime.parse(nextYear+"-01-01")

  db.run{
    StaffTable.filter(_.id === staffId).result.head
  } map {staffList =>
    if(convertOptionalBoolean(staffList.allowAllCECourses)){
        allowCECourses = true
    } else {
     if(staffList.appSquares.toString contains "CE3") {
        maxRequestAllowed = 3
     } else if(staffList.appSquares.toString contains "CE5"){
       maxRequestAllowed = 5
     }
     var block1 = db.run{
       CeHistoryTable.filter(_.staffId === staffId).filter(s => s.dateCreated >= CurrentYearStartDate && s.dateCreated < NextYearStartDate).map(_.courseId).countDistinct.result
     } map { ceHistory =>
        if(ceHistory < maxRequestAllowed){
          allowCECourses = true
        } else {
          var historyBlock = db.run{
            CeHistoryTable.filter(_.staffId === staffId).filter(_.courseId === courseId).length.result
          } map { historyLength =>
              if(historyLength > 0){
                allowCECourses = true
              }
          }
          Await.ready(historyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

             var purchaseBlock = db.run{
                CeCoursePurchaseTable.filter(_.staffId === staffId).filter(_.courseId === courseId).length.result
              } map { purchaseLength =>
                 if(purchaseLength > 0) {
                   allowCECourses = true
                 }
              }
              Await.ready(purchaseBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
     }
     Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    }
    if(allowCECourses){
    response = true
     var block2 = db.run{
       CeHistoryTable.filter(_.staffId === staffId).filter(_.courseId === courseId).filter(_.action === "Watch").length.result
     } map { ceHistoryLength =>
        if(ceHistoryLength == 0){
          var ceHistoryRow = CeHistoryRow(None,staffId,courseId,DateTime.now,"Watch")
      var block3 =  db.run{
        CeHistoryTable.returning(CeHistoryTable.map(_.id)) += ceHistoryRow
          } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(ceHistoryRow.copy(id = Some(id))))
            }
        }
     }
     Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    } else {
      message = s"You have already reached the maximum number of requests for this calendar year. Only ${maxRequestAllowed} requests can be initiated annually per your current Novadontics plan. However, You can upgrade to a different plan to watch and earn credits from more CE courses or you can purchase this course by clicking on the purchase button"
    }
  Ok { Json.obj("Show"-> response,"message"-> message)}
  }
 }

 implicit val courseFormat = Json.format[CeCoursePurchaseRow]

 def createCECoursePurchase = silhouette.SecuredAction.async(parse.json) { request =>
  val staffId = request.identity.id.get
  var isPurchased = false
  
      val coursePurchase = for {
          courseId <- (request.body \ "courseId").validate[Long]
          cost <- (request.body \ "cost").validate[Float]
          purchasedDate <- (request.body \ "purchasedDate").validate[String]
          customerProfileId <- (request.body \ "customerProfileId").validate[Long]
          customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validate[Long]
          transactionId <- (request.body \ "transactionId").validate[Long]
          transactionCode <- (request.body \ "transactionCode").validate[String]

      }yield{
        var cost1 = Math.round(cost)
        CeCoursePurchaseRow(None, staffId, courseId, cost1, DateTime.parse(purchasedDate),customerProfileId,customerPaymentProfileId,transactionId,transactionCode)
      }
  coursePurchase match {
      case JsSuccess(ceCoursePurchaseRow, _) => 
      db.run {
        CeCoursePurchaseTable.returning(CeCoursePurchaseTable.map(_.id)) +=  ceCoursePurchaseRow
      } map { 
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(ceCoursePurchaseRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
     
 }

 def checkCECourseLimit = silhouette.SecuredAction.async { request =>
 var maxRequestAllowed = 10
    var fmtYear = DateTimeFormat.forPattern("yyyy");
    var fmtDate = DateTimeFormat.forPattern("yyyy-MM-dd");
    var staffId = request.identity.id.get

    var currentYear = fmtYear.print(DateTime.now);
    var CurrentYearStartDate = DateTime.parse(currentYear+"-01-01")
    var nextYear = currentYear.toInt + 1
    var NextYearStartDate = DateTime.parse(nextYear+"-01-01")
    
    var limitReached = false
    db.run{
    StaffTable.filter(_.id === staffId).result.head
  } map {staffList =>
  if(!(staffList.appSquares.toString contains "CE")){
       limitReached = true
     }
    else if(convertOptionalBoolean(staffList.allowAllCECourses)){
        limitReached = false
    } else {
     if(staffList.appSquares.toString contains "CE3") {
        maxRequestAllowed = 3
     } else if(staffList.appSquares.toString contains "CE5"){
       maxRequestAllowed = 5
     }
     var block1 = db.run{
       CeHistoryTable.filter(_.staffId === staffId).filter(s => s.dateCreated >= CurrentYearStartDate && s.dateCreated < NextYearStartDate).map(_.courseId).countDistinct.result
     } map { ceHistory =>
     if(ceHistory >= maxRequestAllowed){
       limitReached = true
     }
    }
    Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    
     
    }
    Ok { Json.obj("limitReached"-> limitReached)}
  }
  
 }


case class CeCoursePurchaseList(
      courseName: String,
      category: String,
      staffName: String,
      purchasedDate: String,
      cost: Float
  )
implicit val ceCoursePurchaseList = Json.format[CeCoursePurchaseList]

def getPurchasedCECourses(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction.async { request =>
var fromDate1 = convertOptionString(fromDate)
var toDate1 = convertOptionString(toDate)
var formatter: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");
var mergeList = List.empty[CeCoursePurchaseList]

var query = CeCoursePurchaseTable.sortBy(_.purchasedDate.desc)
if(fromDate1 != "" && toDate1 != ""){
  query = query.filter(s => s.purchasedDate >= DateTime.parse(convertOptionString(fromDate)) && s.purchasedDate <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59))
}
  db.run{
    query
    .join(CoursesTable).on(_.courseId === _.id)
    .join(StaffTable).on(_._1.staffId === _.id)
    .result
  } map { ceCourseQuery =>
    val data = ceCourseQuery.groupBy(_._1._1.id).map {
    case (shareId, entries) =>
    var purchase = entries.head._1._1
    var courses = entries.head._1._2
    var staffs = entries.head._2
    var staffName = ""
    var category = ""
    var title = ""
    if(staffs != None){
    staffName = staffs.title + " " + staffs.name + " " + convertOptionString(staffs.lastName)
    }
    if(courses != None){
    category = convertOptionString(courses.category)
    title = courses.title
    }
    CeCoursePurchaseList(title, category, staffName, formatter.print(purchase.purchasedDate), purchase.cost)

    }.toList
    Ok(Json.toJson(data.sortBy(_.purchasedDate).reverse))
    }
  }

  case class MyCECourseList(
  requestId: Long,
  courseId: String,
  cost: Float,
  title: String,
  dateRequested: String,
  category: String,
  speaker: String,
  status: String,
  dateViewed: String
)
implicit val myCeCourseList = Json.format[MyCECourseList]

def getMyCECourses = silhouette.SecuredAction.async { request =>
  val staffId = request.identity.id.get
  var formatter: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");
  var mergeList = List.empty[MyCECourseList]
  var courseIdList = new ListBuffer[Long]()
  var courseList = List[Long]()

  var block = db.run{
    CeCoursePurchaseTable.filter(_.staffId === staffId).map(_.courseId).result
  } map{ s => courseList = s.toList
  }
  Await.ready(block, atMost = scala.concurrent.duration.Duration(20, SECONDS))
  
    var block1 = db.run{
      CeHistoryTable.filter(_.staffId === staffId).filterNot(_.courseId inSet courseList)
      .join(CoursesTable).on(_.courseId === _.id)
      .joinLeft(EducationRequestTable.filterNot(_.deleted).filter(_.staffId === staffId)).on(_._1.courseId === _.courseId)
      .result
    } map {result => 
      result.foreach(row=>{
      var educationRequest = row._2
      var courses = row._1._2
      var ceHistory = row._1._1
      var requestId:Long = 0
      var cost:Float = 0
      var dateRequested = ""
      var status = ""
      var repeatRecord = false
      var dateViewed = ""
      
      if(courseIdList.contains(ceHistory.courseId)){
        repeatRecord = true
      } else {
        repeatRecord = false
      }
      var test1 = db.run{
        CeHistoryTable.filter(_.staffId === staffId).filter(_.courseId=== ceHistory.courseId).filter(_.action === "Watch").result
        } map { ceHistory =>
          if(!(ceHistory.isEmpty)){
          status = "Viewed"
          ceHistory.foreach(row=>{
            dateViewed = formatter.print(row.dateCreated)
          })
          }
         }
        // 
      Await.ready(test1, atMost = scala.concurrent.duration.Duration(10, SECONDS)) 
      if(!(repeatRecord)){
      if(educationRequest != None){
      var edu = educationRequest.get
      requestId = convertOptionLong(edu.id)
      if(edu.status.toString == "Closed"){
        status = "Completed"
      }
      dateRequested = formatter.print(edu.dateCreated)
      }
      
      var ceCourseList = MyCECourseList(requestId,convertOptionString(courses.courseId),cost,courses.title,dateRequested,convertOptionString(courses.category),convertOptionString(courses.speaker),status,dateViewed)
      courseIdList += ceHistory.courseId
      mergeList = mergeList :+ ceCourseList
      
      }
    })
    }
    Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    
    var block2 = db.run{
      CeCoursePurchaseTable.filter(_.staffId === staffId)
      .join(CoursesTable).on(_.courseId === _.id)
      .joinLeft(EducationRequestTable.filterNot(_.deleted).filter(_.staffId === staffId)).on(_._1.courseId === _.courseId)
      .result
    } map {result =>
      result.foreach(row=>{
      var educationRequest = row._2
      var courses = row._1._2
      var cePurchase = row._1._1
      var requestId:Long = 0
      var cost:Float = 0
      var dateRequested = ""
      var status = ""
      var dateViewed = ""

      var test2 = db.run{
        CeHistoryTable.filter(_.staffId === staffId).filter(_.courseId=== cePurchase.courseId).filter(_.action === "Watch").result
        } map { ceHistory =>
          if(!(ceHistory.isEmpty)){
          status = "Viewed"
          ceHistory.foreach(row=>{
            dateViewed = formatter.print(row.dateCreated)
          })
          }
         }
        // }
       Await.ready(test2, atMost = scala.concurrent.duration.Duration(10, SECONDS)) 
      if(educationRequest != None){
      var edu = educationRequest.get
      requestId = convertOptionLong(educationRequest.get.id)
      if(edu.status.toString == "Closed"){
        status = "Completed"
      }
      dateRequested = formatter.print(edu.dateCreated)
      }
      var ceCourseList = MyCECourseList(requestId,convertOptionString(courses.courseId),cePurchase.cost,courses.title,dateRequested,convertOptionString(courses.category),convertOptionString(courses.speaker),status,dateViewed)
      mergeList = mergeList :+ ceCourseList
    })
    }
    
    Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    
    Future(Ok(Json.toJson(mergeList)))
  }
}
