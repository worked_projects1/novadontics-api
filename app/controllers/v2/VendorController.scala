package controllers.v2
 
import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.{ProductRow, UserRow, VendorRow, PriceChangeHistoryRow, OrderItemRow, StateTaxRow, StaffRow,CartLogRow,SaleTaxRow,StateRow,VendorCategoriesRow}
import play.api.Application
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.Result
 
import play.api.libs.json._
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.{ExecutionContext,Await, Future}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import co.spicefactory.services.EmailService
 
@Singleton
class VendorController @Inject()(ses: AmazonSimpleEmailService, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
 
  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._
 
  implicit val vendorFormat = Json.format[VendorRow]
 
  implicit val stateTaxFormat = Json.format[StateTaxRow]
 
  implicit val productFormat = Json.format[ProductRow]

  implicit val vendorCategoriesFormat = Json.format[VendorCategoriesRow]

  // implicit val saleTaxFormat = Json.format[SaleTaxRow]
   implicit val saleTaxRowWrites = Writes { request: (SaleTaxRow) =>
    val (saleTaxRow) = request

  Json.obj(
      "id" -> saleTaxRow.id,
      "state" -> saleTaxRow.state,
      "stateName" -> saleTaxRow.stateName,
      "vendorId" -> saleTaxRow.vendorId,
      "tax" -> Math.round( saleTaxRow.tax * 100.0) / 100.0
      )
}

  implicit val stateFormat = Json.format[StateRow]
 
  implicit val PriceChangeHistoryRowWrites = Json.writes[PriceChangeHistoryRow]
 
  private val emailService = new EmailService(ses, application)
 private val replyTo = application.configuration.getString("aws.ses.orderInvoice.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val from = application.configuration.getString("aws.ses.orderInvoice.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
 
 
  def readAll = silhouette.SecuredAction.async { request =>
    db.run {
      if (request.identity.isInstanceOf[UserRow]) {
        VendorTable.sortBy(_.name).distinct.result
      } else {
        VendorTable.filterNot(_.deleted).sortBy(_.name).distinct.result
      }
    } map { manufacturers =>
      Ok(Json.toJson(manufacturers))
    }
  }
 
  def read(id: Long) = silhouette.SecuredAction.async {
    db.run {
      VendorTable.filter(_.id === id).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case Some(vendor) => DBIO.successful(Ok(Json.toJson(vendor)))
        case None => DBIO.successful(NotFound)
      }
    }
  }
 
  def create = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val vendor = for {
      name <- (request.body \ "name").validate[String]
      email <- (request.body \ "email").validateOpt[String]
      logoUrl <- (request.body \ "logoUrl").validateOpt[String]
      shippingChargeLimit <- (request.body \ "shippingChargeLimit").validateOpt[Double]
      shippingCharge <- (request.body \ "shippingCharge").validateOpt[Double]
      tax <- (request.body \ "tax").validateOpt[Double]
      creditCardFee <- (request.body \ "creditCardFee").validateOpt[Double]
      group <- (request.body \ "group").validateOpt[String]
      category <- (request.body \ "category").validateOpt[String]
      image <- (request.body \ "image").validateOpt[String]
      groups <- (request.body \ "groups").validateOpt[JsValue]
    }
    yield VendorRow(None, name, email, logoUrl, false, shippingChargeLimit, shippingCharge, tax, creditCardFee, group, category, image, groups)
 
    vendor match {
      case JsSuccess(vendorRow, _) => db.run{
        VendorTable.filter(_.name === vendorRow.name).exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.successful(PreconditionFailed)
          case false => (VendorTable.returning(VendorTable.map(_.id)) += vendorRow) map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(vendorRow.copy(id = Some(id))))
            }
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
 
  }
 
  def update(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val vendor = for {
      name <- (request.body \ "name").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      logoUrl <- (request.body \ "logoUrl").validateOpt[String]
      shippingChargeLimit <- (request.body \ "shippingChargeLimit").validateOpt[Double]
      shippingCharge <- (request.body \ "shippingCharge").validateOpt[Double]
      tax <- (request.body \ "tax").validateOpt[Double]
      creditCardFee <- (request.body \ "creditCardFee").validateOpt[Double]
      group <- (request.body \ "group").validateOpt[String]
      category <- (request.body \ "category").validateOpt[String]
      image <- (request.body \ "image").validateOpt[String]
      groups <- (request.body \ "groups").validateOpt[JsValue]
    } yield {
      (name, email, logoUrl, shippingChargeLimit, shippingCharge, tax, creditCardFee, group, category, image, groups)
    }
 
    vendor match {
      case JsSuccess((name, email, logoUrl, shippingChargeLimit,shippingCharge, tax, creditCardFee, group, category, image, groups), _) => db.run {
        val query = VendorTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            email.map(value => query.map(_.email).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            logoUrl.map(value => query.map(_.logoUrl).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            shippingChargeLimit.map(value => query.map(_.shippingChargeLimit).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            shippingCharge.map(value => query.map(_.shippingCharge).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            tax.map(value => query.map(_.tax).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            creditCardFee.map(value => query.map(_.creditCardFee).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            group.map(value => query.map(_.group).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            category.map(value => query.map(_.category).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            image.map(value => query.map(_.image).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            groups.map(value => query.map(_.groups).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
 
  def archive(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      VendorTable.filterNot(_.deleted).filter(_.id === id).map(_.deleted).update(true).flatMap[Result, NoStream, Effect.Write] {
        case 0L => DBIO.successful(NotFound)
        case _ => DBIO.seq[Effect.Write](ProductTable.filter(_.vendorId === id).map(_.deleted).update(true)) map {
          _ => Ok
        }
        }.transactionally
    }
  }
  
  
  
  def deleteAllProducts(id: Long) = silhouette.SecuredAction(AdminOnly).async {

    var action=for{
        vendors <- VendorTable.filter(_.id === id)
        products <- ProductTable if products.vendorId===vendors.id
        orderItems <- OrderItemTable if orderItems.productId===products.id
    }yield (orderItems)
    
    db.run {
      action.result
    }.map{
        result => //Console.println(result)
        if (result.length>0){
            BadRequest(Json.parse("""{"error":"Could not delete as order records found for one or more product"}"""))
        }else{
            db.run{
                ProductTable.filter(_.vendorId === id).delete
            }
            Ok
        }  
    }  
  }  
  
 
  def unarchive(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      VendorTable.filter(_.deleted).filter(_.id === id).map(_.deleted).update(false).flatMap[Result, NoStream, Effect.Write] {
        case 0L => DBIO.successful(NotFound)
        case _ => DBIO.seq[Effect.Write](ProductTable.filter(_.vendorId === id).map(_.deleted).update(false)) map {
          _ => Ok
        }
      }.transactionally
    }
  }
  
  def increasePrice(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>  
    val inputData = for {
      priceIncreasePercent <- (request.body \ "priceIncreasePercent").validateOpt[String]
      listPriceIncreasePercent <- (request.body \ "listPriceIncreasePercent").validateOpt[String]
      categoryId <- (request.body \ "categoryId").validateOpt[Long]
      subCategoryId <- (request.body \ "subCategoryId").validateOpt[Long]

    } yield {
      val priceIncreasePercentVal:Double = convertOptionDouble(parseDouble(convertOptionString(priceIncreasePercent)))
      val listPriceIncreasePercentVal:Double = convertOptionDouble(parseDouble(convertOptionString(listPriceIncreasePercent)))
      (priceIncreasePercentVal,listPriceIncreasePercentVal,categoryId,subCategoryId)
    }
    inputData match {
      case JsSuccess((priceIncreasePercent, listPriceIncreasePercent, categoryId, subCategoryId), _) =>
 
          if (priceIncreasePercent==0 && listPriceIncreasePercent==0 ){
              Future.successful(BadRequest(Json.parse("""{"error":"Both price and list price percentage are blank."}""")))
          }else{
              val categoryIdVal = convertOptionLong(categoryId)
              var subCategoryIdVal = convertOptionLong(subCategoryId)
              val now = DateTime.now
              db.run {
                PriceChangeHistoryTable.returning(PriceChangeHistoryTable.map(_.id)) += PriceChangeHistoryRow(None,id,Some(priceIncreasePercent),Some(listPriceIncreasePercent),convertOptionLong(request.identity.asInstanceOf[UserRow].id),now,categoryId,subCategoryId)
              }
             db.run {
               var query = ProductTable.filter(_.vendorId === id)

               if(categoryIdVal > 0 &&  subCategoryIdVal== 0){
                 query = query.filter(_.categoryId === categoryIdVal)
               }

              if(categoryIdVal > 0 &&  subCategoryIdVal > 0){
                 query = query.filter(_.categoryId === subCategoryIdVal)
               }

               if(subCategoryIdVal > 0){
                 query = query.filter(_.categoryId === subCategoryIdVal)
               }

                 for {
                  products <- query.result
                }yield {
                  (products).foreach(product => {   
                       var price=product.price + (product.price*priceIncreasePercent/100) 
                       var listPrice=convertOptionDouble(product.listPrice) + (convertOptionDouble(product.listPrice)*listPriceIncreasePercent/100)
 
                       db.run { 
                         ProductTable.filter(_.id === convertOptionLong(product.id)).map(_.price).update(price)
                       }
                       db.run {   
                         ProductTable.filter(_.id === convertOptionLong(product.id)).map(_.listPrice).update(Some(listPrice))
                       }
                  })
                  Ok  
              }
            }
          }  
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  
  case class priceHistoryRow(
  id: Option[Long],
  vendorId:Long,
  priceIncreasePercent: Option[Double],
  listPriceIncreasePercent: Option[Double],
  userId: Long,
  dateCreated: DateTime,
  categoryName: Option[String],
  subCategoryName: Option[String]
  )
  implicit val priceHistoryRowWrites = Writes { priceHistoryRow: priceHistoryRow =>
    Json.obj(
      "id" -> priceHistoryRow.id,
      "vendorId" -> priceHistoryRow.vendorId,
      "priceIncreasePercent" -> priceHistoryRow.priceIncreasePercent,
      "listPriceIncreasePercent" -> priceHistoryRow.listPriceIncreasePercent,
      "userId" -> priceHistoryRow.userId,
      "dateCreated" -> priceHistoryRow.dateCreated,
      "categoryName" -> priceHistoryRow.categoryName,
      "subCategoryName" -> priceHistoryRow.subCategoryName
    )
  }

  def getPriceChangeHisotry(id: Long) = silhouette.SecuredAction.async { request =>
  var newObj : JsValue = null
    db.run{
      PriceChangeHistoryTable
        .filter(_.vendorId=== id)
        .sortBy(_.dateCreated.desc)
        .joinLeft(CategoryTable).on(_.categoryId === _.id)
        .joinLeft(CategoryTable).on(_._1.subCategoryId === _.id)
        .result
    } map { rows => 
    val data = rows.groupBy(_._1._1.id).map {
      case (priceHistory, entries) =>
      var priceChangeHistryRow = entries.head._1._1
      var categoryName =""
      var subCategoryName =""
      var name = entries.head._1._2.map{r => categoryName = r.name}
      var name1 = entries.head._2.map{r => subCategoryName = r.name}
      var priceHistoryObj : List[JsValue] = List()
      priceHistoryRow(
        priceChangeHistryRow.id,
        priceChangeHistryRow.vendorId,
        priceChangeHistryRow.priceIncreasePercent,
        priceChangeHistryRow.listPriceIncreasePercent,
        priceChangeHistryRow.userId,
        priceChangeHistryRow.dateCreated,
        Some(categoryName),
        Some(subCategoryName)
      )
    }.toList.sortBy(t=> (t.dateCreated)).reverse

      Ok(Json.toJson(data))
    }
  }
 
 
 def stateTaxReadAll = silhouette.SecuredAction.async { request =>
    db.run {
          StateTaxTable.sortBy(_.stateName)result
    } map { stateTax =>
      Ok(Json.toJson(stateTax))
    }
  }
 
    def stateTaxUpdate(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val stateTax = for {
      saleTax <- (request.body \ "saleTax").validateOpt[Float]
 
    } yield {
      (saleTax)
    }
 
    stateTax match {
      case JsSuccess((saleTax), _) => db.run {
        val query = StateTaxTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
 
            saleTax.map(value => query.map(_.saleTax).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
 
 
 
 def findSaleTax(shippingId: Long) = silhouette.SecuredAction.async{ request =>
  
  var finalTax: Double = 0
  var productTax: Double = 0
  var priceTotal: Double = 0
  var stateName = ""
  val dentistId = request.identity.asInstanceOf[StaffRow].id
  val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
 
        if(shippingId == -1 ){
          var block1 = db.run{
                ShippingTable.filter(_.dentist === dentistId).filter(_.primary === true).result
          }map{ shippingMapResult =>
              if(shippingMapResult.length > 0){
                  (shippingMapResult).foreach(shippingState=> { stateName = shippingState.state })
              } else{
                var block3 = db.run{
                  PracticeTable.filter(_.id === practiceId).result
                }map{ practiceMapResult =>
                  (practiceMapResult).foreach(shippingState=> { stateName = shippingState.state })
                }
                var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(3, SECONDS))
              }
          }
          var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
   } else if(shippingId == 0) {
 
                var block3 = db.run{
                  PracticeTable.filter(_.id === practiceId).result
                }map{ practiceMapResult =>
                  (practiceMapResult).foreach(shippingState=> { stateName = shippingState.state })
                }
                var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(3, SECONDS))
   } else {
 
      val block2 = db.run{
          ShippingTable.filter(_.id === shippingId).result
          }map{ state =>
            state.foreach(result=> { stateName = result.state })
          }
          var AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(3, SECONDS))
   }
 var vendorSaleTaxMap:scala.collection.mutable.Map[Long,Float]=scala.collection.mutable.Map()
 var totalTax : Double = 0
          db.run {
              CartTable.filter(_.userId === request.identity.asInstanceOf[StaffRow].id)
              .join(ProductTable).on(_.productId === _.id).filterNot(_._2.deleted)
              .join(CategoryTable).on(_._2.categoryId === _.id).filter(_._2.taxExempted === false).result
 
            } map { productMap => 
            
              val data1 = productMap.groupBy(_._1._1.id).map {
              case (cartId, entries) =>
              val entry = entries.head._1._1
              val productEntry = entries.head._1._2
              var vendorStateTax: Float = 0
 
              var productPrice :Double = 0

              if(productEntry.listPrice != None){
                productPrice = convertOptionDouble(productEntry.listPrice)
              }else{
                productPrice = productEntry.price
              }
 
        if (vendorSaleTaxMap.keySet.exists(_ == productEntry.vendorId)){
              vendorStateTax = vendorSaleTaxMap(productEntry.vendorId)
        } else {
          var saleTaxValue: Float = 0
          var calculateTax =  db.run{
            SaleTaxTable.filter(_.vendorId === productEntry.vendorId).filter(a => a.state.toLowerCase === stateName.toLowerCase.trim || a.stateName.toLowerCase === stateName.toLowerCase.trim).result
              }map { stateMap =>
              stateMap.foreach(result=> { saleTaxValue = result.tax })
          }
          var AwaitResult1 = Await.ready(calculateTax, atMost = scala.concurrent.duration.Duration(3, SECONDS))
           vendorSaleTaxMap.update(productEntry.vendorId, saleTaxValue)
           vendorStateTax = saleTaxValue
        }
        var vQuantity = convertOptionLong(entry.quantity)
        if(vQuantity < 0){
          vQuantity = 1
        }

        totalTax = totalTax + ((productPrice * vQuantity) * vendorStateTax / 100)
        }
 
        finalTax = Math.round( totalTax * 100.0) / 100.0
        val obj = Json.obj()
        val newObj = obj + ("salesTax" -> JsNumber(finalTax))
         Ok(Json.toJson(Array(newObj)))
  }
 }
 
 def saleTaxReadAll(vendorId :Long) = silhouette.SecuredAction.async { request =>
      db.run {
          SaleTaxTable.filter(_.vendorId === vendorId).sortBy(_.id).result
      } map { saleTaxMap =>
        Ok(Json.toJson(saleTaxMap))
      }
  }
 
 def saleTaxCreate = silhouette.SecuredAction.async(parse.json) { request =>
    var stateName = ""
    val saleTax = for {
      state <- (request.body \ "state").validate[String]
      vendorId <- (request.body \ "vendorId").validate[Long]
      tax <- (request.body \ "tax").validate[Float]
 
    } yield {
        var block1 = db.run{
            StateTaxTable.filter(_.state === state).result
        }map{stateTaxMap =>
        (stateTaxMap).foreach(result => {
          stateName = result.stateName
        })
        Ok
        }
        var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
        SaleTaxRow(None,state,stateName,vendorId,tax)
    }
 
    saleTax match {
      case JsSuccess(saleTaxRow, _) => db.run{
       SaleTaxTable.returning(SaleTaxTable.map(_.id)) += saleTaxRow
        } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(saleTaxRow.copy(id = Some(id))))
            }
 
      case JsError(_) => Future.successful(BadRequest)
    }
  }
 
def saleTaxUpdate(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
 
    val saleTax = for {
      tax <- (request.body \ "tax").validateOpt[Float]
 
    } yield {
      (tax)
    }
    saleTax match {
      case JsSuccess((tax), _) => db.run {
        val query = SaleTaxTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
 
            tax.map(value => query.map(_.tax).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
 
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
 
  def stateReadAll = silhouette.SecuredAction.async { request =>
      db.run {
          StateTable.result
      } map { stateMap =>
        Ok(Json.toJson(stateMap))
      }
  }

  def findProductSaleTax(addressId: Long, productId: Long, quantity: Long) = silhouette.SecuredAction.async{ request =>

  var finalTax: Double = 0
  var productTax: Double = 0
  var priceTotal: Double = 0
  var stateName = ""
  val dentistId = request.identity.asInstanceOf[StaffRow].id
  val practiceId = request.identity.asInstanceOf[StaffRow].practiceId

        if(addressId == -1 ){
          var block1 = db.run{
                ShippingTable.filter(_.dentist === dentistId).filter(_.primary === true).result
          }map{ shippingMapResult =>
              if(shippingMapResult.length > 0){
                  (shippingMapResult).foreach(shippingState=> { stateName = shippingState.state })
              } else{
                var block3 = db.run{
                  PracticeTable.filter(_.id === practiceId).result
                }map{ practiceMapResult =>
                  (practiceMapResult).foreach(shippingState=> { stateName = shippingState.state })
                }
                var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(3, SECONDS))
              }
          }
          var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          } else if(addressId == 0) {
                var block3 = db.run{
                  PracticeTable.filter(_.id === practiceId).result
                }map{ practiceMapResult =>
                  (practiceMapResult).foreach(shippingState=> { stateName = shippingState.state })
                }
                var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(3, SECONDS))
        } else {

        val block2 = db.run{
          ShippingTable.filter(_.id === addressId).result
          }map{ state =>
            state.foreach(result=> { stateName = result.state })
          }
          var AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      }

        var totalTax : Double = 0
          db.run {
            ProductTable.filterNot(_.deleted).filter(_.id === productId).join(SaleTaxTable).on(_.vendorId === _.vendorId).filter(a => a._2.stateName.toLowerCase === stateName.toLowerCase || a._2.state.toLowerCase === stateName.toLowerCase).result

            } map { productMap =>
              val data = productMap.groupBy(_._1.id).map {
              case (productId, entries) =>
              val productEntry = entries.head._1
              val saleTaxEntry = entries.head._2
          val productPrice = convertOptionDouble(productEntry.listPrice)
          totalTax = totalTax + ((productPrice * quantity) * saleTaxEntry.tax / 100)
        }
        finalTax = Math.round( totalTax * 100.0) / 100.0
        val obj = Json.obj()
        val newObj = obj + ("salesTax" -> JsNumber(finalTax))
        Ok(Json.toJson(Array(newObj)))
  }
 }

  def getVendorCategories = silhouette.SecuredAction.async { request =>
    db.run {
        VendorCategoriesTable.result } map { vendorCategories =>
      Ok(Json.toJson(vendorCategories))
    }
  }

 def updateVendorCategories(vendorId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val vendorCategories = for {
      categories <- (request.body \ "categories").validate[String]
    } yield {
      (categories)
    }

    vendorCategories match {
      case JsSuccess((categories), _) => db.run {
        val query = VendorCategoriesTable.filter(_.vendorId === vendorId)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
          Some(categories).map(value => query.map(_.categories).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
      ) map {_ => Ok}
          case false => db.run{
          VendorCategoriesTable.returning(VendorCategoriesTable.map(_.id)) += VendorCategoriesRow(None,vendorId,categories)} map {
              case 0L => PreconditionFailed
              case id => Ok
            }
            DBIO.successful(Ok)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

}
