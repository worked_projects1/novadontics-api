package controllers.v2

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasher
import controllers.NovadonticsController
import models.daos.tables.{ShippingRow, StaffRow}
import play.api.Application
import play.api.libs.json.{JsError, JsSuccess, Json, Writes}
import play.api.mvc.Result
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import play.api.libs.json._

@Singleton
class ShippingController @Inject()(passwordHasher: PasswordHasher, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._

  implicit val shippingRowWrites = Writes { shipping: ShippingRow =>
    Json.obj(
      "id" -> shipping.id,
      "name" -> shipping.name,
      "country" -> shipping.country,
      "state" -> shipping.state,
      "city" -> shipping.city,
      "zip" -> shipping.zip,
      "address" -> shipping.address,
      "phone" -> shipping.phone,
      "primary" -> shipping.primary,
      "dentist" -> shipping.dentist,
      "suite" -> shipping.suite
    )
  }

  object Output {
    case class Address(
      id: Option[Long],
      name: Option[String],
      country: String,
      city: String,
      state: String,
      zip: String,
      address: String,
      phone: String,
      primary: Boolean,
      isPractice: Boolean,
      suite: Option[String]
    )

    implicit val writes = Writes { address: Output.Address =>
      Json.obj(
        "id" -> address.id,
        "name" -> address.name,
        "country" -> address.country,
        "state" -> address.state,
        "city" -> address.city,
        "zip" -> address.zip,
        "address" -> address.address,
        "phone" -> address.phone,
        "primary" -> address.primary,
        "isPractice" -> address.isPractice,
        "suite" -> address.suite
      )
    }
  }
  
  def readAll = silhouette.SecuredAction.async { request =>
    request.identity match {
      case dentist: StaffRow =>
        val action = for {
          (practices, shipping) <- PracticeTable.filter(_.id === dentist.practiceId) joinLeft ShippingTable.filter(_.dentist === dentist.id).sortBy(_.primary.desc)
        } yield (practices, shipping)

        db.run {
            action.result
        } map { data =>
          val shipping = data.map(_._2).filter(_.isDefined).map({
            case Some(address) =>
              Output.Address(address.id, address.name, address.country, address.city, address.state, address.zip, address.address, address.phone, address.primary, false, address.suite)
          })
          val practiceAddr = data.map(_._1).map(practice =>
            Output.Address(None, Some(practice.name), practice.country, practice.city, practice.state, practice.zip, practice.address, practice.phone, !shipping.exists(_.primary), true, practice.suite)
          )
          Ok(Json.toJson(shipping :+ practiceAddr.head))
        }
      case _ => Future.successful(Ok(Json.toJson(List())))
    }
  }


  def create = silhouette.SecuredAction(DentistOnly).async(parse.json) { request =>
    val shippingAddress = for {
      name <- (request.body \ "name").validateOpt[String]
      country <- (request.body \ "country").validate[String]
      city <- (request.body \ "city").validate[String]
      state <- (request.body \ "state").validate[String]
      zip <- (request.body \ "zip").validate[String]
      address <- (request.body \ "address").validate[String]
      phone <- (request.body \ "phone").validate[String]
      primary <- (request.body \ "primary").validateOpt[String]
      suite <- (request.body \ "suite").validateOpt[String]
    } yield {
      ShippingRow(None, name, country, city, state, zip, address, phone, toBool(primary.getOrElse("No")), Some(request.identity.id.get),suite)
    }

    shippingAddress match {
      case JsSuccess(shippingRow, _) =>
      var stateFound : Boolean = true
      var countries = "us,united states,usa"
      if(countries contains shippingRow.country.toLowerCase){
        var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === shippingRow.state.toLowerCase || s.stateName.toLowerCase === shippingRow.state.toLowerCase).length.result
          } map { result =>
          if(result == 0 ){
              stateFound = false
          }
           Ok
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      }

      if(stateFound){
        db.run {
          ShippingTable.returning(ShippingTable.map(_.id)) += shippingRow
      } map {
        case 0L => BadRequest
        case id =>
          if(shippingRow.primary){
            db.run {
              ShippingTable.filter(_.dentist === request.identity.id).filterNot(_.id === id).map(_.primary).update(false)
            }
          }
         Created(Json.toJson(shippingRow.copy(id = Some(id))))
      }
      } else {
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Invalid state!"))
        Future(PreconditionFailed{Json.toJson(newObj)})
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def read(id: Long) = silhouette.SecuredAction(StaffOnly).async { request =>
    db.run {
      ShippingTable.filter(_.dentist === request.identity.id).filter(_.id === id).result.headOption
    } map {
      case Some(address) => Ok(Json.toJson(address))
      case None => NotFound
    }
  }

  def update(id: Long) = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    val shippingAddress = for {
      name <- (request.body \ "name").validateOpt[String]
      country <- (request.body \ "country").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      primary <- (request.body \ "primary").validateOpt[String]
      suite <- (request.body \ "suite").validateOpt[String]
    } yield {
      (name, country, city, state, zip, address, phone, primary, suite)
    }

    shippingAddress match {
      case JsSuccess((name, country, city, state, zip, address, phone, primary, suite), _) =>
      var stateFound : Boolean = true
      var countries = "us,united states,usa"

      if(countries contains convertOptionString(country).toLowerCase ){
      var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === convertOptionString(state).toLowerCase || s.stateName.toLowerCase === convertOptionString(state).toLowerCase).length.result
          } map { result =>
          if(result == 0 ){
              stateFound = false
          }
           Ok
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      }

      if(stateFound){
        db.run {
          val query = ShippingTable.filter(_.id === id).filter(_.dentist === request.identity.id)
          def updatePrimary(value: Boolean) = {
            if (value) {
                DBIO.seq(
                  ShippingTable.filter(_.dentist === request.identity.id).map(_.primary).update(false),
                  query.map(_.primary).update(true)
                )
            } else {
              query.map(_.primary).update(false)
            }
          }
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true =>
                DBIO.seq[Effect.Write](
                  name.map(value => query.map(_.name).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  country.map(value => query.map(_.country).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  city.map(value => query.map(_.city).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  state.map(value => query.map(_.state).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  zip.map(value => query.map(_.zip).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  address.map(value => query.map(_.address).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  phone.map(value => query.map(_.phone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  primary.map(value => updatePrimary(toBool(value))).getOrElse(DBIO.successful(Unit)),
                  suite.map(value => query.map(_.suite).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

                ) map { _ =>
                  Ok
                }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      } else {
        val obj = Json.obj()
        val newObj = obj + ("Result" -> JsString("Failed")) + ("message" -> JsString("Invalid state!"))
        Future(PreconditionFailed{Json.toJson(newObj)})
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(StaffOnly).async { request =>
    db.run {
        ShippingTable.filter(_.id === id).filter(_.dentist === request.identity.id).delete map {
          case 0 => NotFound
          case _ => Ok
        }
      }
  }
}
