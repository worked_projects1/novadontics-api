package controllers

import com.mohiva.play.silhouette.api.Authorization
import com.mohiva.play.silhouette.impl.authenticators.BearerTokenAuthenticator
import models.daos.DatabaseAccess
import models.daos.tables.{StaffRow, IdentityRow, UserRow}
import org.joda.time.format.ISODateTimeFormat
import play.api.libs.json._
import play.api.mvc.{Controller, Request}
import scala.concurrent.Future
import com.typesafe.config.{Config, ConfigFactory}
import com.twilio.Twilio
import com.twilio.`type`.PhoneNumber
import com.twilio.rest.api.v2010.account.Message
import scala.util.{Failure, Success, Try}
import org.apache.commons.codec.binary.Base64
import javax.crypto.spec.SecretKeySpec
import java.security.MessageDigest
import java.util
import javax.crypto.Cipher
import org.apache.commons.codec.binary.Base64
import scala.collection.mutable.Map

/**
  * Created by jovanercic on 5/22/17.
  */
trait NovadonticsController extends Controller with DatabaseAccess {

  object AdminOnly extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[UserRow] && identity.asInstanceOf[UserRow].role == UserRow.Role.Admin)
    }
  }

  object UsersOnly extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[UserRow])
    }
  }

  object StaffOnly extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow])
    }
  }

  object CanShop extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow] && List(StaffRow.Role.Dentist, StaffRow.Role.LabTechnician, StaffRow.Role.MasterStudent, StaffRow.Role.DentalAssistant).contains(identity.asInstanceOf[StaffRow].role))
    }
  }

  object CanRequestConsult extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow] && List(StaffRow.Role.Dentist, StaffRow.Role.ContinuingEd, StaffRow.Role.LabTechnician, StaffRow.Role.MasterStudent, StaffRow.Role.Student).contains(identity.asInstanceOf[StaffRow].role))
    }
  }

  object CanUpdateTreatment extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow] && List(StaffRow.Role.Dentist, StaffRow.Role.DentalAssistant).contains(identity.asInstanceOf[StaffRow].role))
    }
  }

  object CanCreateTreatment extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow] && List(StaffRow.Role.Dentist, StaffRow.Role.DentalAssistant, StaffRow.Role.Receptionist).contains(identity.asInstanceOf[StaffRow].role))
    }
  }

  object DentistOnly extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[StaffRow] && identity.asInstanceOf[StaffRow].role == StaffRow.Role.Dentist)
    }
  }

  object SupersOnly extends Authorization[IdentityRow, BearerTokenAuthenticator] {
    override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = {
      Future.successful(identity.isInstanceOf[UserRow] && identity.asInstanceOf[UserRow].role == UserRow.Role.Admin || identity.isInstanceOf[StaffRow] && identity.asInstanceOf[StaffRow].role == StaffRow.Role.Dentist)
    }
  }

  var medicalConditionMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      medicalConditionMap.update("medicalCondition1", "Alcohol/Drug Dependency")
      medicalConditionMap.update("medicalCondition2", "Allergies")
      medicalConditionMap.update("medicalCondition3", "Anemia")
      medicalConditionMap.update("medicalCondition4", "Angina")
      medicalConditionMap.update("medicalCondition5", "Arthritis")
      medicalConditionMap.update("medicalCondition6", "Artificial joints or implants")
      medicalConditionMap.update("medicalCondition7", "Asthma")
      medicalConditionMap.update("medicalCondition8", "Cancer")
      medicalConditionMap.update("medicalCondition9", "Diabetes")
      medicalConditionMap.update("medicalCondition10", "Epilepsy")
      medicalConditionMap.update("medicalCondition11", "Artificial Heart Valve")
      medicalConditionMap.update("medicalCondition12", "Congenital Heart Problems")
      medicalConditionMap.update("medicalCondition13", "Congestive Heart Disease")
      medicalConditionMap.update("medicalCondition14", "Heart Attack")
      medicalConditionMap.update("medicalCondition15", "Heart Disease")
      medicalConditionMap.update("medicalCondition16", "Heart Murmur")
      medicalConditionMap.update("medicalCondition17", "Heart Pacemaker")
      medicalConditionMap.update("medicalCondition18", "Heart Surgery")
      medicalConditionMap.update("medicalCondition19", "Mitral Valve Prolapse")
      medicalConditionMap.update("medicalCondition20", "Stroke")
      medicalConditionMap.update("medicalCondition21", "High Blood Pressure")
      medicalConditionMap.update("medicalCondition22", "Low Blood Pressure")
      medicalConditionMap.update("medicalCondition23", "Glaucoma")
      medicalConditionMap.update("medicalCondition24", "HIV Infection (AIDS)")
      medicalConditionMap.update("medicalCondition25", "Hay Fever")
      medicalConditionMap.update("medicalCondition26", "Frequent Headaches")
      medicalConditionMap.update("medicalCondition27", "Ulcers")
      medicalConditionMap.update("medicalCondition28", "Lung Disease Tuberculosis")
      medicalConditionMap.update("medicalCondition29", "Thyroid Problems")
      medicalConditionMap.update("medicalCondition30", "Hepatitis A")
      medicalConditionMap.update("medicalCondition31", "Hepatitis B")
      medicalConditionMap.update("medicalCondition32", "Hepatitis C")
      medicalConditionMap.update("medicalCondition33", "Kidney Disease")
      medicalConditionMap.update("medicalCondition34", "Leukemia")
      medicalConditionMap.update("medicalCondition35", "Liver Disease")
      medicalConditionMap.update("medicalCondition36", "Seizures /Fainting")
      medicalConditionMap.update("medicalCondition37", "Respiratory Problems")
      medicalConditionMap.update("medicalCondition38", "Rheumatic Fever")
      medicalConditionMap.update("medicalCondition39", "Sinus Problems")
      medicalConditionMap.update("medicalCondition40", "Smoking")
      medicalConditionMap.update("medicalCondition41", "Other")
      medicalConditionMap.update("medicalCondition42", "Taking Bisphosphonate Medication")
      medicalConditionMap.update("medicalCondition43", "Radiation Therapy")
      medicalConditionMap.update("medicalCondition44", "Chemo Therapy")
      medicalConditionMap.update("medicalCondition45", "Bleeding Disorder")
      medicalConditionMap.update("medicalCondition46", "*Pre-Med - Amox")
      medicalConditionMap.update("medicalCondition47", "Blood Disease")
      medicalConditionMap.update("medicalCondition48", "Psychological Disorder")
      medicalConditionMap.update("medicalCondition49", "Dizziness")
      medicalConditionMap.update("medicalCondition50", "*Pre-Med - Clind")

var allergyMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      allergyMap.update("allergy1", "Penicillin")
      allergyMap.update("allergy2", "Latex")
      allergyMap.update("allergy3", "Codeine")
      allergyMap.update("allergy4", "Anesthetics")
      allergyMap.update("allergy5", "Sulfa Drugs")
      allergyMap.update("allergy6", "Aspirin")
      allergyMap.update("allergy7", "Other")
      allergyMap.update("allergy8", "Erythromycin")

var referralSource:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
     referralSource.update("billboard", "Billboard")
      referralSource.update("doctor_referral", "Doctor Referral")
      referralSource.update("facebook", "Facebook")
      referralSource.update("google_search", "Google Search")
      referralSource.update("instagram", "Instagram")
      referralSource.update("insurancePlan", "Insurance Plan")
      referralSource.update("linkedIn", "LinkedIn")
      referralSource.update("magazine", "Magazine Ad")
      referralSource.update("newspaper", "Newspaper Ad")
      referralSource.update("patient_referral", "Patient Referral")
      referralSource.update("postcard", "Postcard")
      referralSource.update("printedMaterial", "Printed Material")
      referralSource.update("radio", "Radio")
      referralSource.update("television", "Television")
      referralSource.update("twitter", "Twitter")
      referralSource.update("walkIn", "Walk-In")
      referralSource.update("yelp", "Yelp")
      referralSource.update("youtube", "YouTube")
      referralSource.update("other", "Other")

var findPatientType:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      findPatientType.update("patientType1", "General Dentistry")
      findPatientType.update("patientType2", "Cosmetic Dentistry")
      findPatientType.update("patientType3", "Partial Denture Upper")
      findPatientType.update("patientType4", "Partial Denture Lower")
      findPatientType.update("patientType5", "Full Denture Upper")
      findPatientType.update("patientType6", "Full Denture Lower")
      findPatientType.update("patientType7", "Bone Grafting")
      findPatientType.update("patientType8", "Overdenture Upper")
      findPatientType.update("patientType9", "Overdenture Lower")
      findPatientType.update("patientType10", "Implant Prosthodontics Fixed")
      findPatientType.update("patientType11", "Implant Patient Single")
      findPatientType.update("patientType12", "Implant Patient Multi")
      findPatientType.update("patientType13", "All on X")
      findPatientType.update("patientType14", "Sinus Augmentation")
      findPatientType.update("patientType15", "Block Grafting")
      findPatientType.update("patientType16", "Perio")
      findPatientType.update("patientType17", "RCT")
      findPatientType.update("patientType18", "Whitening")
      findPatientType.update("patientType19", "Invisalign")
      findPatientType.update("patientType20", "Pedodontics")

var procedureMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      procedureMap.update("D1110", "Prophy - Adult")
      procedureMap.update("D1120", "Prophy - Child")
      procedureMap.update("D4910", "Perio")
      procedureMap.update("D0274", "4BW")
      procedureMap.update("D0210", "FMX")
      procedureMap.update("D0330", "Pano")
      procedureMap.update("D0367", "CT Scan")
      procedureMap.update("D1999", "Special")

var unitsMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      unitsMap.update("BG", "BAG")
      unitsMap.update("BA", "BAIL")
      unitsMap.update("BL", "BARRELS")
      unitsMap.update("BO", "BOOK")
      unitsMap.update("BT", "BOTTLES")
      unitsMap.update("BX", "BOX")
      unitsMap.update("BK", "BUCKET")
      unitsMap.update("CN", "CAN")
      unitsMap.update("CD", "CARD")
      unitsMap.update("CT", "CARTON")
      unitsMap.update("CR", "CARTRIDGE")
      unitsMap.update("CS", "CASE")
      unitsMap.update("DS", "DOSE")
      unitsMap.update("DZ", "DOZEN")
      unitsMap.update("DR", "DRUMS")
      unitsMap.update("EA", "EACH")
      unitsMap.update("FT", "FEET")
      unitsMap.update("GL", "GALLONS")
      unitsMap.update("GR", "GROSS")
      unitsMap.update("IN", "INCHES")
      unitsMap.update("JR", "JAR")
      unitsMap.update("KT", "KIT")
      unitsMap.update("LF", "LINEAR FEET")
      unitsMap.update("LT", "LOT")
      unitsMap.update("OZ", "OUNCES")
      unitsMap.update("PK", "PACK")
      unitsMap.update("PD", "PAD")
      unitsMap.update("PR", "PAIR")
      unitsMap.update("PL", "PALLET")
      unitsMap.update("PC", "PIECES")
      unitsMap.update("PT", "PINT")
      unitsMap.update("LB", "POUNDS")
      unitsMap.update("QT", "QUART")
      unitsMap.update("RM", "REAM")
      unitsMap.update("RL", "ROLL")
      unitsMap.update("ST", "SET")
      unitsMap.update("SH", "SHELF PACK")
      unitsMap.update("SL", "SLEEVE")
      unitsMap.update("SR", "STRIP")
      unitsMap.update("SP", "SUPPOSITORY")
      unitsMap.update("TB", "TABLET")
      unitsMap.update("M", "THOUSAND")
      unitsMap.update("TR", "TRAY")
      unitsMap.update("TU", "TUBE")
      unitsMap.update("VL", "VIAL")
      unitsMap.update("YD", "YARD")

var insuranceCompanyMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      insuranceCompanyMap.update("DELTA", "Delta PPO")
      insuranceCompanyMap.update("CIGNA", "Cigna PPO")
      insuranceCompanyMap.update("METLIFE", "MetLife PPO")
      insuranceCompanyMap.update("FDH EPO", "First Dental Health HMO")
      insuranceCompanyMap.update("FDH PPO", "First Dental Health PPO")
      insuranceCompanyMap.update("UHC/DBP", "United Healthcare PPO")
      insuranceCompanyMap.update("AETNA", "Aetna PPO")

var countryIdMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map()
      countryIdMap.update("USA", "+1")
      countryIdMap.update("Canada", "+1")
      countryIdMap.update("Australia", "+61")
      countryIdMap.update("India", "+91")
      countryIdMap.update("England", "+44")

var timeZoneMap: Map[String, String] = Map()
  timeZoneMap.update("AKST - ALASKA STANDARD TIME", "America/Anchorage")
  timeZoneMap.update("AST - ATLANTIC STANDARD TIME", "America/Anguilla")
  timeZoneMap.update("CST - CENTRAL STANDARD TIME", "America/Belize")
  timeZoneMap.update("EST - EASTERN TIME ZONE", "America/Cayman")
  timeZoneMap.update("HST - HAWAII–ALEUTIAN STANDARD TIME", "Pacific/Honolulu")
  timeZoneMap.update("MST - MOUNTAIN STANDARD TIME", "America/Dawson_Creek")
  timeZoneMap.update("MST - MOUNTAIN STANDARD TIME(ARIZONA)", "America/Phoenix")
  timeZoneMap.update("PST - PACIFIC STANDARD TIME", "America/Los_Angeles")
  timeZoneMap.update("STANDARD TIME", "America/Los_Angeles")
  timeZoneMap.update("IST - INDIAN STANDARD TIME", "Asia/Calcutta")
  

  implicit val dateTimeFormat = new Format[DateTime] {
    val format = ISODateTimeFormat.dateTime()

    override def reads(json: JsValue) = json.validate[String].flatMap { value =>
      try {
        JsSuccess(format.parseDateTime(value))
      } catch {
        case _: IllegalArgumentException => JsError("Invalid DateTime format")
      }
    }

    override def writes(dateTime: DateTime) = Json.toJson(format.print(dateTime))
  }

  def toFlatTuple[A, B, C](tuple: ((A, B), C)) = { val ((a, b), c) = tuple; (a, b, c) }
  def toBool(value: String) = if(value == "Yes") true else false;

  def convertStringToLong(inputValue:Option[String]):Long = {
    val outputValue: Long = inputValue match {
      case None => 0
      case Some(l: String) => l.toLong
    }
    return outputValue;
  }


    def convertOptionalInt(inputValue:Option[Int]):Int = {
    val outputValue: Int = inputValue match {
      case None => 0
      case Some(l: Int) => l //return  to set your value
    }
    return outputValue;
  }

    def convertOptionalLocalDate(inputValue:Option[LocalDate]):LocalDate = {
    val outputValue: LocalDate = inputValue match {
      case None => LocalDate.now()
      case Some(l: LocalDate) => l //return  to set your value
    }
    return outputValue;
  }

      def convertOptionalDateTime(inputValue:Option[DateTime]):DateTime = {
    val outputValue: DateTime = inputValue match {
      case None => DateTime.now()
      case Some(l: DateTime) => l //return  to set your value
    }
    return outputValue;
  }

    def convertOptionalLocalTime(inputValue:Option[LocalTime]):LocalTime = {
    val outputValue: LocalTime = inputValue match {
      case None => LocalTime.now()
      case Some(l: LocalTime) => l //return  to set your value
    }
    return outputValue;
  }

     def convertOptionalBoolean(inputValue:Option[Boolean]):Boolean = {
    val outputValue: Boolean = inputValue match {
      case None => false
      case Some(l: Boolean) => l //return  to set your value
    }
    return outputValue;
  }


    def convertLocalDateToString(inputValue:Option[LocalDate]):String = {
    val outputValue: String = inputValue match {
      case None => ""
      case Some(l: LocalDate) => l.toString //return  to set your value
    }
    return outputValue;
  }

   def convertStringToDouble(inputValue:Option[String]):Double = {
    val outputValue: Double = inputValue match {
      case None => 0
      case Some(l: String) => l.toDouble
    }
    return outputValue;
  }

 def convertStringToFloat(inputValue:Option[String]):Float = {
    val outputValue: Float = inputValue match {
      case None => 0
      case Some(l: String) => l.toFloat
    }
    return outputValue;
  }

  def convertOptionString(inputValue:Option[String]):String = {
    val outputValue: String = inputValue match {
      case None => ""//Or handle the lack of a value another way: throw an error, etc.
      case Some(s: String) => s //return  to set your value
    }

    return outputValue;
  }

  def convertOptionDouble(inputValue:Option[Double]):Double = {
    val outputValue: Double = inputValue match {
      case None => 0.0//Or handle the lack of a value another way: throw an error, etc.
      case Some(s: Double) => s //return  to set your value
    }
    
    return outputValue;
  }

  def convertOptionFloat(inputValue:Option[Float]):Float = {
    val outputValue: Float = inputValue match {
      case None => 0     //Or handle the lack of a value another way: throw an error, etc.
      case Some(s: Float) => s //return  to set your value
    }

    return outputValue;
  }

  def convertOptionLong(inputValue:Option[Long]):Long = {
    val outputValue: Long = inputValue match {
      case None => 0//Or handle the lack of a value another way: throw an error, etc.
      case Some(s: Long) => s //return  to set your value
    }
    
    return outputValue;
  }
  
  def convertOptionInt(inputValue:Option[Int]):Int = {
    val outputValue: Int = inputValue match {
      case None => 0//Or handle the lack of a value another way: throw an error, etc.
      case Some(s: Int) => s //return  to set your value
    }
    return outputValue;
  }
  
  def convertOptionListLong(inputValue:Option[List[Long]]):List[Long] = {
    val outputValue: List[Long] = inputValue match {
      case None => List()//Or handle the lack of a value another way: throw an error, etc.
      case Some(s: List[Long]) => s //return  to set your value
    }
    return outputValue;
  }
  
  def parseDouble(s: String) = try { Some(s.toDouble) } catch { case _ => None }
  
  def parseDate(input: String) = try {
    //DateTimeFormat.forPattern("DD:HH").parseDateTime("11:22")
    //val fmt = DateTimeFormat forPattern "yyyy-MM-dd"
    DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(input)
    //fmt parseDateTime input
  } catch {
    case e: IllegalArgumentException => None
  }
  
  def isValidDate(inputDate: String,format: String):Boolean = {
     var validDate=true
     
     try {

        DateTimeFormat.forPattern(format).parseDateTime(inputDate)
        //fmt parseDateTime input
      } catch {
        case e: IllegalArgumentException => validDate =false
      }
     
     
     
     /*var sampleDate = DateTime.now
     try {
        // val in = new BufferedReader(.....) -- doesn't matter
        sampleDate=org.joda.time.DateTime.parse(input)
      }
      catch {
        case _: Throwable =>{
           validDate=false
         }  
      }*/
      return validDate;
    
  }  
  
  def isAllDigits(x: String) = x forall Character.isDigit

  def sendSMS(toNum: String, body: String, country: String): String = {

  val config = ConfigFactory.load()
  var isMsgSent : String = ""
  var countryId = ""
  val smsSendEnabled = application.configuration.getString("aws.ses.appointmentReminder.smsSendEnabled").getOrElse(throw new RuntimeException("aws.ses.appointmentReminder.smsSendEnabled` property."))

  if(smsSendEnabled == "true"){
  val ACCOUNT_SID = config.getString("twilio.account_sid")
  val AUTH_TOKEN = config.getString("twilio.auth_token")
  Twilio.init(ACCOUNT_SID, AUTH_TOKEN)

  if (countryIdMap.keySet.contains(country)){
    countryId = countryIdMap(country)
  }else{
     countryId = application.configuration.getString("aws.ses.appointmentReminder.smsCountryId").getOrElse(throw new RuntimeException("aws.ses.appointmentReminder.smsCountryId` property."))
  }

  val from = new PhoneNumber(config.getString("twilio.from_number"))

  val phoneNum = countryId + toNum
  val to = new PhoneNumber(phoneNum)

  Try(Message.creator(to, from, body).create()) match {
    case Success(message) =>
      isMsgSent = "Message Sent"
    case Failure(error) =>
       isMsgSent = error.toString
  }

  return isMsgSent;
  }
  else{
    isMsgSent = "SMS send configuration is disabled in Application"
    return isMsgSent;
  }

  }   

  def encryptMethod(inputString: String) : String = {

   val key = application.configuration.getString("encodeKey").getOrElse(throw new RuntimeException("Configuration is missing `encodeKey` property."))
   val ALGO: String = "AES";

   def encodeKey(str: String): String = {
          var encoded: Array[Byte] = Base64.encodeBase64(str.getBytes());
          return new String(encoded);
    }

   var encodedBase64Key: String  = encodeKey(key);

   def keyToSpec(): SecretKeySpec = {
    var keyBytes: Array[Byte] = (key).getBytes("UTF-8")
    keyBytes = util.Arrays.copyOf(keyBytes, 16)
    new SecretKeySpec(keyBytes, ALGO)
   }

   def encrypt(text: String, encodedBase64Key: String): String = {
      val cipher: Cipher = Cipher.getInstance(ALGO)
      cipher.init(Cipher.ENCRYPT_MODE, keyToSpec())
      return Base64.encodeBase64String(cipher.doFinal(text.getBytes()))      
   }

   var encrStr: String = encrypt(inputString, encodedBase64Key);

   return encrStr;

}

def getCountryIdByCountryName(country: String) : String = {

 var countryId = ""

 if (countryIdMap.keySet.contains(country)){
    countryId = countryIdMap(country)
  }else{
     countryId = application.configuration.getString("aws.ses.appointmentReminder.smsCountryId").getOrElse(throw new RuntimeException("aws.ses.appointmentReminder.smsCountryId` property."))
  }

  return countryId;

}

def getTimeZoneIdByTimeZone(timeZoneName: String): String = {
  var timeZoneId = ""

  if(timeZoneMap.keySet.contains(timeZoneName)){
    timeZoneId = timeZoneMap(timeZoneName)
  } else {
    timeZoneId = application.configuration.getString("aws.ses.appointmentReminder.defaultTimeZOneId").getOrElse(throw new RuntimeException("aws.ses.appointmentReminder.defaultTimeZOneId` property."))
  }

  return timeZoneId
}
    
    

}
