package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasher
import controllers.NovadonticsController
import models.daos.tables.UserRow
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UsersController @Inject()(passwordHasher: PasswordHasher, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val userRoleFormat = new Format[UserRow.Role] {
    import UserRow.Role._

    override def writes(category: UserRow.Role) = Json.toJson {
      category match {
        case Admin => "Admin"
        case NovadonticsStaff => "NovadonticsStaff"
        case Unknown => "Unknown"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Admin" => Admin
      case "NovadonticsStaff" => NovadonticsStaff
      case _ => Unknown
    }
  }

  implicit val userRowWrites = Writes { user: UserRow =>
    Json.obj(
      "id" -> user.id,
      "name" -> user.name,
      "email" -> user.email,
      "phone" -> user.phone,
      "role" -> user.role
    )
  }

  def readAll = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      UserTable.filterNot(_.archived).sortBy(_.name).result
    } map { users =>
      Ok(Json.toJson(users))
    }
  }

  def create = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val user = for {
      name <- (request.body \ "name").validate[String]
      email <- (request.body \ "email").validate[String]
      password <- (request.body \ "password").validate[String]
      phone <- (request.body \ "phone").validate[String]
      role <- (request.body \ "role").validate[UserRow.Role]
    } yield {
      UserRow(None, name, email, passwordHasher.hash(password).password, phone, DateTime.now, role)
    }

    user match {
      case JsSuccess(userRow, _) =>
        db.run {
          val duplicateEmail = for {
            userExists <- UserTable.filter(_.email === userRow.email).exists.result
            dentistExists <- StaffTable.filter(_.email === userRow.email).exists.result
          } yield {
            userExists || dentistExists
          }

          duplicateEmail.flatMap {
            case true =>
              UserTable.filter(_.email === userRow.email).exists.result.flatMap {
                case false => DBIO.successful(0L)
                case true => UserTable.filter(_.email === userRow.email).map(r => (r.id, r.archived)).result.head.flatMap {
                  case (id, true) =>
                    DBIO.seq(
                      UserTable.filter(_.id === id).update(userRow.copy(id = Some(id))),
                      UserTable.filter(_.id === id).map(_.archived).update(false)
                    ).map(_ => id)
                  case (id, false) =>
                    DBIO.successful(0L)
                }
              }
            case false =>
              UserTable.returning(UserTable.map(_.id)) += userRow
          }.transactionally
        } map {
          case 0L => PreconditionFailed(Json.obj("error" -> "User email already exists"))
          case id => Created(Json.toJson(userRow.copy(id = Some(id))))
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def read(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      UserTable.filterNot(_.archived).filter(_.id === id).result.headOption
    } map {
      case Some(user) => Ok(Json.toJson(user))
      case None => NotFound
    }
  }

  def update(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val user = for {
      name <- (request.body \ "name").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      password <- (request.body \ "password").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      role <- (request.body \ "role").validateOpt[UserRow.Role]
    } yield {
      (name, email, password.map(passwordHasher.hash(_).password), phone, role)
    }

    user match {
      case JsSuccess((name, email, password, phone, role), _) =>
        db.run {
          val query = UserTable.filterNot(_.archived).filter(_.id === id)

          query.map(_.email).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case Some(emailAddress) =>
              val duplicateEmail = if (email.fold(false)(_ != emailAddress)) {
                for {
                  userExists <- UserTable.filter(_.email === email.get).exists.result
                  dentistExists <- StaffTable.filter(_.email === email.get).exists.result
                } yield {
                  userExists || dentistExists
                }
              } else {
                DBIO.successful(false)
              }

              duplicateEmail.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
                case true =>
                  DBIO.successful(PreconditionFailed(Json.obj("error" -> "User email already exists")))
                case false =>
                  DBIO.seq[Effect.Write](
                    name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    email.map(value => query.map(_.email).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    password.map(value => query.map(_.password).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    phone.map(value => query.map(_.phone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    role.map(value => query.map(_.role).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) map { _ =>
                    Ok
                  }
              }
            case None =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      SessionTable.filter(_.staffId === id).delete.flatMap { _ =>
        UserTable.filterNot(_.archived).filter(_.id === id).map(_.archived).update(true)
      }
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

}
