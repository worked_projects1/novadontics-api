package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.regions._
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model._
import com.google.inject._
import com.google.inject.name.Named
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.crypto.Base64
import com.mohiva.play.silhouette.api.util.{PasswordHasher, PasswordInfo}
import controllers.NovadonticsController
import models.daos.tables.{StaffRow, PasswordChangeTokenRow, UserRow}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.Result
import play.api.{Application, Logger, Mode}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

@Singleton
class PasswordController @Inject()(ses: AmazonSimpleEmailService, passwordHasher: PasswordHasher, silhouette: Silhouette[BearerTokenEnv], @Named("blocking") blockingExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  private val log = Logger(this.getClass)

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  private val baseUrl = application.configuration.getString("baseUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))

  private val from = application.configuration.getString("aws.ses.passwordReset.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val replyTo = application.configuration.getString("aws.ses.passwordReset.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.passwordReset.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))

  private val sesArn = application.configuration.getString("aws.ses.arn").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  private val sesRegion = application.configuration.getString("aws.ses.region").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  ses.setRegion(Region.getRegion(Regions.fromName(sesRegion)))

  def forgot(email: String) = silhouette.UnsecuredAction.async {
    db.run {
      val query = for {
        user <- UserTable.filter(_.email === email).map(r => r.id -> r.name).result.headOption
        dentist <- StaffTable.filter(_.email === email).map(r => r.id -> r.name).result.headOption
      } yield {
        (user, dentist)
      }

      lazy val randomToken = {
        val bytes = Array.ofDim[Byte](12)
        Random.nextBytes(bytes)
        Base64.encode(bytes).substring(0, 12).replace('/', '_')
      }

      query.flatMap {
        case (Some((userId, name)), _) =>
          (PasswordChangeTokenTable += PasswordChangeTokenRow(randomToken, Some(userId), None, DateTime.now)).map(_ => Some(randomToken -> name))
        case (_, Some((staffId, name))) =>
          (PasswordChangeTokenTable += PasswordChangeTokenRow(randomToken, None, Some(staffId), DateTime.now)).map(_ => Some(randomToken -> name))
        case _ =>
          DBIO.successful(None)
      }
    } flatMap {
      case None =>
        Future.successful(NotFound)
      case Some((token, name)) =>
        Future(
          ses.sendEmail(
            new SendEmailRequest()
              .withSourceArn(sesArn)
              .withSource(from)
              .withReplyToAddresses(replyTo)
              .withDestination(new Destination().withToAddresses(s"$name <$email>"))
              .withMessage {
                new Message()
                  .withSubject(new Content().withData(subject))
                  .withBody(new Body().withText(new Content().withData(views.txt.forgotPassword(name, baseUrl, token).body.trim())))
              }
          )
        )(blockingExecutor).map { _ =>
          application.mode match {
            case Mode.Dev => Ok.withHeaders("X-Forgot-Token" -> token)
            case _ => Ok
          }
        }
    }
  }

  def check(secret: String) = silhouette.UnsecuredAction.async {
    db.run {
      PasswordChangeTokenTable.filter(_.secret === secret).result.headOption.flatMap {
        case Some(PasswordChangeTokenRow(_, Some(userId), _, dateIssued)) =>
          UserTable.filter(_.id === userId).exists.result.map {
            case true => Ok
            case false => NotFound
          }
        case Some(PasswordChangeTokenRow(_, _, Some(staffId), dateIssued)) =>
          StaffTable.filter(_.id === staffId).exists.result.map {
            case true => Ok
            case false => NotFound
          }
        case _ =>
          DBIO.successful(NotFound)
      }
    }
  }

  def reset(secret: String) = silhouette.UnsecuredAction.async(parse.json) { request =>
    db.run {
      PasswordChangeTokenTable.filter(_.secret === secret).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
        case Some(PasswordChangeTokenRow(_, user, dentist, dateIssued)) =>
          PasswordChangeTokenTable.filter(_.secret === secret).delete.flatMap { _ =>
            (request.body \ "secret").asOpt[String].map { password =>
              (user, dentist) match {
              case (Some(userId), _) =>
                UserTable.filter(_.id === userId).map(_.password).update(passwordHasher.hash(password).password).map(_ => Ok(Json.obj("role" -> "User")))
              case (_, Some(staffId)) =>
                StaffTable.filter(_.id === staffId).map(_.password).update(passwordHasher.hash(password).password).map(_ => Ok(Json.obj("role" -> "Dentist")))
              case _ =>
                DBIO.successful(NotFound)
            }
            }.getOrElse {
              DBIO.successful(NotFound)
            }
          }
        case _ =>
          DBIO.successful(NotFound)
      }.transactionally
    }
  }

  def change = silhouette.SecuredAction.async(parse.json) { request =>
    val passwords = for {
      currentPassword <- (request.body \ "current").validate[String]
      newPassword <- (request.body \ "new").validate[String]
    } yield {
      (currentPassword, newPassword)
    }

    passwords match {
      case JsSuccess((currentPassword, newPassword), _) =>
        db.run {
          val query = request.identity match {
            case UserRow(id, _, _, _, _, _, _) =>
              UserTable.filter(_.id === id).map(_.password)
            case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
              StaffTable.filter(_.id === id).map(_.password)
          }
          query.result.head.flatMap {
            case pw if passwordHasher.matches(PasswordInfo("bcrypt", pw, None), currentPassword) =>
              query.update(passwordHasher.hash(newPassword).password) map { _ =>
                Ok
              }
            case _ =>
              DBIO.successful(PreconditionFailed)
          }.transactionally
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

}



