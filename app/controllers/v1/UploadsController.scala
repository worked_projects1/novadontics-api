package controllers.v1

import java.net.URL

import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.HttpMethod
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import play.api.Application
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext


import scala.io.Source
import java.io.File
import java.nio.file.attribute.PosixFilePermission._
import scala.concurrent.Future
@Singleton
class UploadsController @Inject() (s3: AmazonS3, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  private val bucketName = application.configuration.getString("aws.s3.uploads.bucket").getOrElse(throw new RuntimeException("Configuration is missing `aws.s3.uploads.bucket` property."))
  private val publicUrlProtocol = application.configuration.getString("aws.s3.uploads.publicUrl.protocol").getOrElse(throw new RuntimeException("Configuration is missing `aws.s3.uploads.publicUrl.protocol` property."))
  private val publicUrlHost = application.configuration.getString("aws.s3.uploads.publicUrl.host").getOrElse(throw new RuntimeException("Configuration is missing `aws.s3.uploads.publicUrl.host` property."))

  def signature(name: String) = silhouette.SecuredAction { request =>
    val lowercaseName = name.toLowerCase

    if (lowercaseName.contains(".")) {
      val chunks = lowercaseName.reverse.split("\\.", 2).map(_.reverse)
      val base = chunks(1)
      val ext = chunks(0)
      val key = base + "-" + DateTime.now.getMillis + "." + ext
      println("base = "+base+" ,ext = "+ext+" ,key = "+key)
      val uploadUrl = s3.generatePresignedUrl {
        new GeneratePresignedUrlRequest(bucketName, key, HttpMethod.PUT)
          .withExpiration(DateTime.now.plusMinutes(1).toDate)
          .withContentType(request.getQueryString("Content-Type").orNull)
      }
      val publicUrl = new URL(publicUrlProtocol, publicUrlHost, uploadUrl.getPath.replace(bucketName + "/", ""))

      Ok {
        Json.obj(
          "uploadUrl" -> uploadUrl.toString,
          "publicUrl" -> publicUrl.toString
        )
      }
    } else {
      BadRequest
    }
  }

def unSecuredSignature(name: String) = silhouette.UnsecuredAction { request =>
    val lowercaseName = name.toLowerCase

    if (lowercaseName.contains(".")) {
      val chunks = lowercaseName.reverse.split("\\.", 2).map(_.reverse)
      val base = chunks(1)
      val ext = chunks(0)
      val key = base + "-" + DateTime.now.getMillis + "." + ext
      val uploadUrl = s3.generatePresignedUrl {
        new GeneratePresignedUrlRequest(bucketName, key, HttpMethod.PUT)
          .withExpiration(DateTime.now.plusMinutes(1).toDate)
          .withContentType(request.getQueryString("Content-Type").orNull)
      }
      val publicUrl = new URL(publicUrlProtocol, publicUrlHost, uploadUrl.getPath.replace(bucketName + "/", ""))

      Ok {
        Json.obj(
          "uploadUrl" -> uploadUrl.toString,
          "publicUrl" -> publicUrl.toString
        )
      }
    } else {
      BadRequest
    }
  }



 
}
