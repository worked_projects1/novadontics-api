package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasher
import controllers.NovadonticsController
import models.daos.tables.{OrderRow, RenewalDetailsRow, StaffRow, PracticeRow, ConEdTrackRow}
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result
import scalaj.http.{Http, HttpOptions}
import play.api.libs.json.Json.obj
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.util.Random
import java.security.MessageDigest
import java.util.Base64
import java.net.URLEncoder
import scalaj.http.{Http, HttpOptions}
import java.nio.file.{Files, Paths}
import play.api.Logger
import play.api.libs.Files.TemporaryFile
// import play.api.mvc.MultipartFormData
import play.api.mvc.Request
import scala.io.Source
import java.nio.charset.StandardCharsets
// import org.apache.http.entity.mime._
import java.io.File

@Singleton
class StaffController @Inject()(passwordHasher: PasswordHasher, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
  val doseSpotAdminId = application.configuration.getString("doseSpotAdminId").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotAdminId` property."))
  val doseSpotClinicId = application.configuration.getString("doseSpotClinicId").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotClinicId` property."))
  val doseSpotClinicKey = application.configuration.getString("doseSpotClinicKey").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotClinicKey` property."))


  implicit val conEdTrackFormat = Json.format[ConEdTrackRow]

  implicit val staffRoleFormat = new Format[StaffRow.Role] {

    import StaffRow.Role._

    override def writes(category: StaffRow.Role) = Json.toJson {
      category match {
        case Dentist => "Dentist"
        case LabTechnician => "Lab Technician"
        case Receptionist => "Receptionist"
        case Student => "Fellowship Student"
        case MasterStudent => "Master Student"
        case DentalAssistant => "Dental Assistant"
        case ContinuingEd => "Continuing Ed"
        case Unknown => "Unknown"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Dentist" => Dentist
      case "Lab Technician" => LabTechnician
      case "Receptionist" => Receptionist
      case "Fellowship Student" => Student
      case "Master Student" => MasterStudent
      case "Dental Assistant" => DentalAssistant
      case "Continuing Ed" => ContinuingEd
      case _ => Unknown
    }
  }

  implicit val staffRowWrites = Writes { staff: StaffRow =>
  var practiceName = ""
  val block = db.run {
      PracticeTable.filter(_.id === staff.practiceId).result
    } map { practiceMap =>
      practiceMap.foreach(result => {
        practiceName = result.name
      })
    }
    val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(120, SECONDS))
    Json.obj(
      "id" -> staff.id,
      "name" -> staff.name,
      "email" -> staff.email,
      "practiceId" -> staff.practiceId,
      "practiceName" -> practiceName,
      "dateCreated" -> staff.dateCreated,
      "role" -> staff.role,
      "udid" -> staff.udid,
      "udid1" -> staff.udid1,
      "canAccessWeb" -> staff.canAccessWeb,
      "phone" -> staff.phone,
      "deaNumber" -> staff.deaNumber,
      "npiNumber" -> staff.npiNumber,
      "country" -> staff.country,
      "city" -> staff.city,
      "state" -> staff.state,
      "zip" -> staff.zip,
      "address" -> staff.address,
      "expirationDate" -> staff.expirationDate,
      "plan" -> staff.plan,
      "accountType" -> staff.accountType,
      "title" -> staff.title,
      "lastName" -> staff.lastName,
      "marketingSource" -> staff.marketingSource,
      "notes" -> staff.notes,
      "subscriptionPeriod" -> staff.subscriptionPeriod,
      "cellPhone" -> staff.cellPhone,
      "ciiPrograms" -> staff.ciiPrograms,
      "appSquares" -> staff.appSquares,
      "custToLeadFlag" -> staff.custToLeadFlag,
      "photo" -> staff.photo,
      "amountCollected" -> staff.amountCollected,
      "collectedDate" -> staff.collectedDate,
      "webModules" -> staff.webModules,
      "prime" -> staff.prime,
      "authorizeCustId" -> staff.authorizeCustId,
      "allowAllCECourses" -> staff.allowAllCECourses,
      "udid2" -> staff.udid2,
      "mckessonId" -> staff.mckessonId,
      "agreementAccepted" -> staff.agreementAccepted,
      "contract" -> staff.contract,
      "suiteNumber" -> staff.suiteNumber,
      "dentalLicense" -> staff.dentalLicense,
      "doseSpotUserId" -> staff.doseSpotUserId,
      "icoiMember" -> staff.icoiMember,
      "dateOfBirth" -> staff.dateOfBirth,
      "primaryFax" -> staff.primaryFax,
      "clinicianRoleType" -> staff.clinicianRoleType,
      "accessStartDate" -> staff.accessStartDate
    )
  }
  
  def getStaffRoleAsString(staffRole: StaffRow.Role):String = {
      staffRole match {
        case StaffRow.Role.Dentist => return "Dentist"
        case StaffRow.Role.LabTechnician => return "LabTechnician"
        case StaffRow.Role.Receptionist => return "Receptionist"
        case StaffRow.Role.Student => return "FellowshipStudent"
        case StaffRow.Role.MasterStudent => return "MasterStudent"
        case StaffRow.Role.DentalAssistant => return "DentalAssistant"
        case StaffRow.Role.ContinuingEd => return "ContinuingEd"
      }
  }  


  def readAll(practiceId: Option[Long]) = silhouette.SecuredAction(SupersOnly).async { request =>
    var query = StaffTable.filterNot(_.archived)
    request.identity match {
      case user: StaffRow =>
        query = query.filter(_.practiceId === user.practiceId)
      case _ =>
    }
    query = if (practiceId.isDefined) query.filter(_.practiceId === practiceId.get) else query

    db.run {
      query.sortBy(_.expirationDate).result
    } map { staff =>
      Ok(Json.toJson(staff))
    }
  }

  def create = silhouette.SecuredAction.async(parse.json) { request =>
    val staff = for {
      name <- (request.body \ "name").validate[String]
      email <- (request.body \ "email").validate[String]
      password <- (request.body \ "password").validate[String]
      practiceId <- (request.body \ "practiceId").validate[Long]
      role <- (request.body \ "role").validateOpt[StaffRow.Role]
      //canAccessWeb <- (request.body \ "canAccessWeb").validateOpt[String]
      deaNumber <- (request.body \ "deaNumber").validateOpt[String]
      npiNumber <- (request.body \ "npiNumber").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      country <- (request.body \ "country").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      plan <- (request.body \ "plan").validateOpt[Long]
      //      strExpirationDate <- (request.body \ "expirationDate").validateOpt[String]
      accountType <- (request.body \ "accountType").validate[String]
      title <- (request.body \ "title").validate[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
      marketingSource <- (request.body \ "marketingSource").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      subscriptionPeriod <- (request.body \ "subscriptionPeriod").validateOpt[Int]
      cellPhone <- (request.body \ "cellPhone").validateOpt[String]
      ciiPrograms <- (request.body \ "ciiPrograms").validateOpt[String]
      appSquares <- (request.body \ "appSquares").validateOpt[String]
      photo <- (request.body \ "photo").validateOpt[String]
      amountCollected <- (request.body \ "amountCollected").validateOpt[Long]
      strCollectedDate <- (request.body \ "collectedDate").validateOpt[String]
      webModules <- (request.body \ "webModules").validateOpt[String]
      prime <- (request.body \ "prime").validateOpt[Boolean]
      authorizeCustId <- (request.body \ "authorizeCustId").validateOpt[String]
      allowAllCECourses <- (request.body \ "allowAllCECourses").validateOpt[Boolean]
      udid2 <- (request.body \ "udid2").validateOpt[String]
      mckessonId <- (request.body \ "mckessonId").validateOpt[Long]
      agreementAccepted <- (request.body \ "agreementAccepted").validateOpt[Boolean]
      contract <- (request.body \ "contract").validateOpt[String]
      suiteNumber <- (request.body \ "suiteNumber").validateOpt[String]
      dentalLicense <- (request.body \ "dentalLicense").validateOpt[String]
      doseSpotUserId <- (request.body \ "doseSpotUserId").validateOpt[String]
      icoiMember <- (request.body \ "icoiMember").validateOpt[String]
      dateOfBirth <- (request.body \ "dateOfBirth").validateOpt[LocalDate]
      primaryFax <- (request.body \ "primaryFax").validateOpt[String]
      clinicianRoleType <- (request.body \ "clinicianRoleType").validateOpt[String]
      strExpirationDate <- (request.body \ "expirationDate").validate[String]
      strAccessStartDate <- (request.body \ "accessStartDate").validate[String]

    } yield {

      // val expirationDate: DateTime = {
      //   subscriptionPeriod match {
      //     case None => DateTime.now.plusYears(1) //Or handle the lack of a value another way: throw an error, etc.
      //     case Some(l: Int) => DateTime.now.plusMonths(l)
      //   }
      // }
      var expirationDate = DateTime.parse(strExpirationDate)
      var accessStartDate = Some(DateTime.parse(strAccessStartDate))


      val collectedDate: DateTime = {
        strCollectedDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }

      StaffRow(None, name, email, passwordHasher.hash(password).password, role.getOrElse(StaffRow.Role.Dentist), practiceId, DateTime.now, None,None,  true, deaNumber, npiNumber, phone, country, city, state, zip, address, expirationDate, plan, accountType, title, lastName, marketingSource, notes, subscriptionPeriod, cellPhone, ciiPrograms, appSquares, Some(false), photo, amountCollected, Some(collectedDate),Some(""),webModules, prime, Some(convertOptionString(authorizeCustId).toLong),allowAllCECourses,udid2,mckessonId,agreementAccepted,contract,suiteNumber,dentalLicense,doseSpotUserId,icoiMember,dateOfBirth,primaryFax,clinicianRoleType,accessStartDate,false)
    }

     staff match {
      case JsSuccess(staffRow, _) =>

      var stateNotFound = false
      var phoneValidation = false
      var zipCodeValidation = false
      var faxNumberValidation = false
      var lastNameValidation = false
      var emailValidation = false
      var dobValidation = false
      var addressValidation = false
      var cityValidation = false
      var npiNumberValidation = false
      var countries = "us,united states,usa"

      if(convertOptionString(staffRow.country) != "" && convertOptionString(staffRow.country) != None){
        if(countries contains convertOptionString(staffRow.country).toLowerCase){
          var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === convertOptionString(staffRow.state).toLowerCase || s.stateName.toLowerCase === convertOptionString(staffRow.state).toLowerCase).length.result
          } map { result =>
          if(result == 0 ){
              stateNotFound = true
          }
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          var phoneNumber = convertOptionString(staffRow.cellPhone).replaceAll("[^a-zA-Z0-9]", "")
          if(phoneNumber.length != 10 || phoneNumber.startsWith("0") || phoneNumber.startsWith("1") || phoneNumber.startsWith("123")){
              phoneValidation = true
          }

          if(!(convertOptionString(staffRow.zip).forall(_.isDigit) && (convertOptionString(staffRow.zip).length == 5 || convertOptionString(staffRow.zip).length == 9))){
             zipCodeValidation = true
          }

          if(staffRow.lastName == None || staffRow.lastName == Some("")){
            lastNameValidation = true
          }
          if(staffRow.email == None || staffRow.email == ""){
            emailValidation = true
          }
          if(staffRow.address == None || staffRow.address == Some("")){
            addressValidation = true
          }
          if(staffRow.city == None || staffRow.city == Some("")){
            cityValidation = true
          }
        }
      }

      // --- DOSESPOT VALIDATION
    if(staffRow.doseSpotUserId != None && staffRow.doseSpotUserId != Some("")){
        if(staffRow.dateOfBirth == None || staffRow.dateOfBirth == Some("")){
            dobValidation = true
        }
        if(staffRow.npiNumber == None || staffRow.npiNumber == Some("")){
            npiNumberValidation = true
        }
        if(staffRow.primaryFax == None || convertOptionString(staffRow.primaryFax).length != 10){
            faxNumberValidation = true
        }
    }

      if(lastNameValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the last name!"}""")))
      } else if(emailValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the email address!"}""")))
      } else if(phoneValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid phone number!"}""")))
      } else if(faxNumberValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid fax number!"}""")))
      } else if(npiNumberValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid npi number!"}""")))
      } else if(dobValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the date of birth!"}""")))
      } else if(staffRow.country == None){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the country!"}""")))
      } else if(addressValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the address!"}""")))
      } else if(cityValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the city!"}""")))
      } else if(stateNotFound){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid state name!"}""")))
      } else if(zipCodeValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid zipcode!"}""")))
      } else {
          db.run {
          val duplicateEmail = for {
            userExists <- UserTable.filter(_.email === staffRow.email).exists.result
            staffExists <- StaffTable.filter(_.email === staffRow.email).exists.result
          } yield {
          userExists || staffExists
          }

          duplicateEmail.flatMap {
            case true =>
              StaffTable.filter(_.email === staffRow.email).exists.result.flatMap {
                case false => DBIO.successful(0L)
                case true => StaffTable.filter(_.email === staffRow.email).map(r => (r.id, r.archived)).result.head.flatMap {
                  case (id, true) =>
                    DBIO.seq(
                      StaffTable.filter(_.id === id).update(staffRow.copy(id = Some(id))),
                      StaffTable.filter(_.id === id).map(_.archived).update(false)
                    ).map(_ => id)
                  case (id, false) =>
                    DBIO.successful(0L)
                }
              }
            case false =>
              StaffTable.returning(StaffTable.map(_.id)) += staffRow
          }.transactionally
        } map {
          case 0L => PreconditionFailed(Json.obj("error" -> "User email already exists"))
          case id => db.run {
              RenewalDetailsTable.returning(RenewalDetailsTable.map(_.id)) += RenewalDetailsRow(None, id, staffRow.subscriptionPeriod, staffRow.plan, DateTime.now(), DateTime.now(), staffRow.expirationDate, "new", staffRow.accountType, staffRow.amountCollected, staffRow.collectedDate, staffRow.accessStartDate)
            }
            Created(Json.toJson(staffRow.copy(id = Some(id))))
        }
      }

      case JsError(_) =>
        Future.successful(BadRequest)
    }


  }

  def read(id: Long) = silhouette.SecuredAction(SupersOnly).async { request =>
    var query = StaffTable.filterNot(_.archived)
    request.identity match {
      case user: StaffRow =>
        query = query.filter(_.practiceId === user.practiceId)
      case _ =>
    }
    db.run {
      query.filter(_.id === id).result.headOption
    } map {
      case Some(staff) => Ok(Json.toJson(staff))
      case None => NotFound
    }
  }

  def readStaffTest(id: Long) = silhouette.SecuredAction.async { request =>
    var query = StaffTable.filterNot(_.archived)
    request.identity match {
      case user: StaffRow =>
        query = query.filter(_.practiceId === user.practiceId)
      case _ =>
    }
    db.run {
      query.filter(_.id === id).result.headOption
    } map {
      case Some(staff) => Ok(Json.toJson(staff))
      case None => NotFound
    }
  }

  def readAllPractice(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      StaffTable.filterNot(_.archived).filter(_.practiceId === id).sortBy(_.name).result.map {
        staff => Ok(Json.toJson(staff))
      }
    }
  }
  
  def roleToSquareAccessMapping() = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    var roleToSquareMap:Map[String,String]=Map("Dentist"->"patients,continuingEducation,catalog,requestConsultation,documents,patientEducation,dashboard,CIICourse,newProcedure","LabTechnician"->"catalog,requestConsultation,patientEducation,dashboard","Receptionist"->"newProcedure,dashboard","Student"->"requestConsultation,documents,dashboard","MasterStudent"->"continuingEducation,catalog,requestConsultation,documents,patientEducation,dashboard","DentalAssistant"->"newProcedure,patients,catalog,patientEducation,dashboard","ContinuingEd"->"continuingEducation,documents,dashboard")
    
    db.run {
      for{
          staffs <- StaffTable.result
      }yield{
          (staffs).foreach(staff=>{
              var staffRole:String = getStaffRoleAsString(staff.role)
              var appSquareAccess = roleToSquareMap(staffRole)
              if (staff.canAccessWeb){
                appSquareAccess += ",canAccessWeb" 
              }  
              //Console.println(appSquareAccess)
              db.run{  
                StaffTable.filter(_.id === staff.id).map(_.appSquares).update(Some(appSquareAccess))
              }
          }) 
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Square access updated successfully by referring role"
            )
          }
      }    
     }
  }  

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>

    var strSubscriptionPeriod: Option[Int] = Some(0)
    var updateExpiry: Boolean = false
    var strNewExpirationDate: DateTime = DateTime.now()

    val staff = for {
      name <- (request.body \ "name").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      password <- (request.body \ "password").validateOpt[String]
      practiceId <- (request.body \ "practiceId").validateOpt[Long]
      role <- (request.body \ "role").validateOpt[StaffRow.Role]
      udid <- (request.body \ "udid").validateOpt[String]
      udid1 <- (request.body \ "udid1").validateOpt[String]
      //canAccessWeb <- (request.body \ "canAccessWeb").validateOpt[String]
      deaNumber <- (request.body \ "deaNumber").validateOpt[String]
      npiNumber <- (request.body \ "npiNumber").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      country <- (request.body \ "country").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      plan <- (request.body \ "plan").validateOpt[Long]
      //      strExpirationDate <- (request.body \ "expirationDate").validateOpt[String]
      accountType <- (request.body \ "accountType").validateOpt[String]
      title <- (request.body \ "title").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
      marketingSource <- (request.body \ "marketingSource").validateOpt[String]
      notes <- (request.body \ "notes").validateOpt[String]
      cellPhone <- (request.body \ "cellPhone").validateOpt[String]
      subscriptionPeriod <- (request.body \ "subscriptionPeriod").validateOpt[Int]
      ciiPrograms <- (request.body \ "ciiPrograms").validateOpt[String]
      appSquares <- (request.body \ "appSquares").validateOpt[String]
      photo <- (request.body \ "photo").validateOpt[String]
      amountCollected <- (request.body \ "amountCollected").validateOpt[Long]
      strCollectedDate <- (request.body \ "collectedDate").validateOpt[String]
      webModules <- (request.body \ "webModules").validateOpt[String]
      prime <- (request.body \ "prime").validateOpt[Boolean]
      authorizeCustId <- (request.body \ "authorizeCustId").validateOpt[String]
      allowAllCECourses <- (request.body \ "allowAllCECourses").validateOpt[Boolean]
      udid2 <- (request.body \ "udid2").validateOpt[String]
      mckessonId <- (request.body \ "mckessonId").validateOpt[Long]
      agreementAccepted <- (request.body \ "agreementAccepted").validateOpt[Boolean]
      contract <- (request.body \ "contract").validateOpt[String]
      suiteNumber <- (request.body \ "suiteNumber").validateOpt[String]
      dentalLicense <- (request.body \ "dentalLicense").validateOpt[String]
      doseSpotUserId <- (request.body \ "doseSpotUserId").validateOpt[String]
      icoiMember <- (request.body \ "icoiMember").validateOpt[String]
      dateOfBirth <- (request.body \ "dateOfBirth").validateOpt[LocalDate]
      primaryFax <- (request.body \ "primaryFax").validateOpt[String]
      clinicianRoleType <- (request.body \ "clinicianRoleType").validateOpt[String]
      strExpirationDate <- (request.body \ "expirationDate").validateOpt[String]
      strAccessStartDate <- (request.body \ "accessStartDate").validateOpt[String]

    } yield {

      var accessStartDate: Option[DateTime] = None
      if(strAccessStartDate != None && strAccessStartDate != Some("")){
          accessStartDate = Some(DateTime.parse(convertOptionString(strAccessStartDate)))
      }

      var expirationDate: Option[DateTime] = None
      if(strExpirationDate != None && strExpirationDate != Some("")){
          expirationDate = Some(DateTime.parse(convertOptionString(strExpirationDate)))
          strNewExpirationDate =  DateTime.parse(convertOptionString(strExpirationDate))
      }

      val collectedDate: DateTime = {
        strCollectedDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }
      
      var canAccessWeb = false
      var appSquaresStr=convertOptionString(appSquares)
      if (appSquaresStr contains "canAccessWeb"){
          canAccessWeb = true
      }

      var canAccessWebOpt= Some(canAccessWeb)
      strSubscriptionPeriod = subscriptionPeriod

      ((name, email, password.map(passwordHasher.hash(_).password), role, practiceId, udid, udid1, canAccessWebOpt, deaNumber, npiNumber, phone, country, city, state, zip, address, plan, accountType, title, lastName, marketingSource, notes), StaffRow(None, "", "", "", role.getOrElse(StaffRow.Role.Dentist), 0, DateTime.now, None, None, false, None, None, None, None, None, None, None, None, convertOptionalDateTime(expirationDate), None, "", "", None, None, None, subscriptionPeriod, cellPhone, ciiPrograms, appSquares, Some(false), photo, amountCollected, Some(collectedDate),Some(""),webModules,prime,Some(convertOptionString(authorizeCustId).toLong),allowAllCECourses,udid2,mckessonId,agreementAccepted, contract, suiteNumber, dentalLicense, doseSpotUserId, icoiMember,dateOfBirth,primaryFax,clinicianRoleType,accessStartDate,false))
    }


    staff match {
      case JsSuccess(((name, email, password, role, practiceId, udid, udid1, canAccessWebOpt, deaNumber, npiNumber, phone, country, city, state, zip, address, plan, accountType, title, lastName, marketingSource, notes), staffRow), _) =>
        //      case JsSuccess((name, email, password, role, practiceId, udid, canAccessWeb, deaNumber, npiNumber, phone, country, city, state, zip, address, plan, accountType, title, lastName, marketingSource, notes), _) =>
      var stateNotFound = false
      var phoneValidation = false
      var zipCodeValidation = false
      var faxNumberValidation = false
      var lastNameValidation = false
      var emailValidation = false
      var dobValidation = false
      var addressValidation = false
      var stateValidation = false
      var cityValidation = false
      var npiNumberValidation = false
      var countries = "us,united states,usa"

   if(convertOptionString(country) != "" && convertOptionString(country) != None){
      if(countries contains convertOptionString(country).toLowerCase ){

      var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === convertOptionString(state).toLowerCase || s.stateName.toLowerCase === convertOptionString(state).toLowerCase).length.result
          } map {  result =>
          if(result == 0 ){
              stateNotFound = true
          }
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          var phoneNumber = convertOptionString(staffRow.cellPhone).replaceAll("[^a-zA-Z0-9]", "")
          if(phoneNumber.length != 10 || phoneNumber.startsWith("0") || phoneNumber.startsWith("1") || phoneNumber.startsWith("123")){
              phoneValidation = true
          }

          if(!(convertOptionString(zip).forall(_.isDigit) && (convertOptionString(zip).length == 5 || convertOptionString(zip).length == 9))){
             zipCodeValidation = true
          }

          if(lastName == Some("")){
            lastNameValidation = true
          }
          if(email == Some("")){
            emailValidation = true
          }
          if(address == Some("")){
            addressValidation = true
          }
          if(city == Some("")){
            cityValidation = true
          }
          if(state == Some("")){
            stateValidation = true
          }
      }
    }

// --- DOSESPOT VALIDATION
    if(staffRow.doseSpotUserId != None && staffRow.doseSpotUserId != Some("")){
        if(staffRow.dateOfBirth == None || staffRow.dateOfBirth == Some("")){
           dobValidation = true
        }
        if(npiNumber == None || npiNumber == Some("")){
           npiNumberValidation = true
        }
        if(staffRow.primaryFax == None || convertOptionString(staffRow.primaryFax).length != 10){
          faxNumberValidation = true
        }
    }

      if(lastNameValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the last name!"}""")))
      } else if(emailValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the email address!"}""")))
      } else if(phoneValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid phone number!"}""")))
      } else if(faxNumberValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid fax number!"}""")))
      } else if(npiNumberValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid npi number!"}""")))
      } else if(dobValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the date of birth!"}""")))
      } else if(addressValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the address!"}""")))
      } else if(cityValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the city!"}""")))
      } else if(stateNotFound){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid state name!"}""")))
      } else if(zipCodeValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter a valid zipcode!"}""")))
      } else if(country == None){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter the country!"}""")))
      } else {
         db.run {
          val query = StaffTable.filter(_.id === id)
          query.map(row => (row.email, row.expirationDate, row.accountType, row.custToLeadFlag)).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case Some(row) =>

              var strPrevExpiry: DateTime = DateTime.now()
              var strPrevAccountType: String = row._3
              var prevCustToLeadFlag: Boolean = row._4 match {
                case None => false
                case Some(l: Boolean) => l
              }
              var custToLeadFlag: Option[Boolean] = Some(false)
              db.run {
                for {
                  prevExpiry <- query.map(_.expirationDate).result.head
                  prevAccountType <- query.map(_.accountType).result.head
                } yield {
                  prevExpiry
                  prevAccountType
                  strPrevExpiry = prevExpiry
                }
              } map { _ =>
                val lead: String = "Lead"
                val customer: Option[String] = Some("Paid")
                val query = StaffTable.filter(_.id === id)
                var renewType: String = "new"

                db.run {
                  StaffTable
                    .filter(_.id === id)
                    .joinLeft(RenewalDetailsTable.filter(_.renewalType === "new")).on(_.id === _.staffId)
                    .result
                } map { data =>
                  if(data.head._2.exists(_.renewalType=="new")){
                    renewType = "renew"
                  }

                  if (data.head._1.accountType != "Paid" && accountType == customer) {
                    
                    // if (strPrevExpiry > DateTime.now()) {
                    //   strNewExpirationDate = strPrevExpiry;
                    // }
                    // strNewExpirationDate = strSubscriptionPeriod match {
                    //   case None => DateTime.now.plusYears(1) //Or handle the lack of a value another way: throw an error, etc.
                    //   case Some(l: Int) => strNewExpirationDate.plusMonths(l)
                    // }
                    updateExpiry = true
                    db.run {
                      query.map(_.expirationDate).update(strNewExpirationDate)
                        .map { _ =>
                          db.run {
                            RenewalDetailsTable.returning(RenewalDetailsTable.map(_.id)) += RenewalDetailsRow(None, id, strSubscriptionPeriod, plan, DateTime.now(), strPrevExpiry, strNewExpirationDate, renewType, "Paid", staffRow.amountCollected, staffRow.collectedDate,staffRow.accessStartDate)
                          }
                          Ok
                        }
                    }
                  }
                }
                Ok
              }
              val leadOpt: Option[String] = Some("Lead")
              if ((strPrevAccountType.equals("Paid") || prevCustToLeadFlag) && accountType.equals(leadOpt) && row._2 < DateTime.now()) {
                custToLeadFlag = Some(true)
              }

              val duplicateEmail = if (email.fold(false)(_ != row._1)) {
                for {
                  userExists <- UserTable.filter(_.email === email.get).exists.result
                  dentistExists <- StaffTable.filter(_.email === email.get).exists.result
                } yield {
                  userExists || dentistExists
                }
              } else {
                DBIO.successful(false)
              }
              duplicateEmail.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
                case true =>
                  DBIO.successful(PreconditionFailed(Json.obj("error" -> "User email already exists")))
                case false =>
                  DBIO.seq[Effect.Write](
                    name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    email.map(value => query.map(_.email).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    password.map(value => query.map(_.password).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    role.map(value => query.map(_.role).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceId.map(value => query.map(_.practiceId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    udid.map(value => query.map(_.udid).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    udid1.map(value => query.map(_.udid1).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    // canAccessWebOpt.map(value => query.map(_.canAccessWeb).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    deaNumber.map(value => query.map(_.deaNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    npiNumber.map(value => query.map(_.npiNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    phone.map(value => query.map(_.phone).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    country.map(value => query.map(_.country).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    city.map(value => query.map(_.city).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    state.map(value => query.map(_.state).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    zip.map(value => query.map(_.zip).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    address.map(value => query.map(_.address).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    plan.map(value => query.map(_.plan).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    accountType.map(value => query.map(_.accountType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    lastName.map(value => query.map(_.lastName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    marketingSource.map(value => query.map(_.marketingSource).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    notes.map(value => query.map(_.notes).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.subscriptionPeriod.map(value => query.map(_.subscriptionPeriod).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.cellPhone.map(value => query.map(_.cellPhone).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.ciiPrograms.map(value => query.map(_.ciiPrograms).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.appSquares.map(value => query.map(_.appSquares).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    custToLeadFlag.map(value => query.map(_.custToLeadFlag).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.photo.map(value => query.map(_.photo).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.amountCollected.map(value => query.map(_.amountCollected).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.collectedDate.map(value => query.map(_.collectedDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.webModules.map(value => query.map(_.webModules).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.prime.map(value => query.map(_.prime).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.authorizeCustId.map(value => query.map(_.authorizeCustId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.allowAllCECourses.map(value => query.map(_.allowAllCECourses).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.udid2.map(value => query.map(_.udid2).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.mckessonId.map(value => query.map(_.mckessonId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.agreementAccepted.map(value => query.map(_.agreementAccepted).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.contract.map(value => query.map(_.contract).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.suiteNumber.map(value => query.map(_.suiteNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.dentalLicense.map(value => query.map(_.dentalLicense).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.doseSpotUserId.map(value => query.map(_.doseSpotUserId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.icoiMember.map(value => query.map(_.icoiMember).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.dateOfBirth.map(value => query.map(_.dateOfBirth).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.primaryFax.map(value => query.map(_.primaryFax).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.clinicianRoleType.map(value => query.map(_.clinicianRoleType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    Some(staffRow.expirationDate).map(value => query.map(_.expirationDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    staffRow.accessStartDate.map(value => query.map(_.accessStartDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) map { _ =>

                  //--Dosespot Api Called---
                  if(staffRow.doseSpotUserId != None && staffRow.doseSpotUserId != Some("")){
                    val accessToken:String = getAccessToken(doseSpotUrl,doseSpotClinicId,doseSpotClinicKey,doseSpotAdminId)

                    var clinicianId = convertOptionString(staffRow.doseSpotUserId)
                    var firstName = convertOptionString(name)
                    var sLastName = convertOptionString(lastName)
                    var sTitle = convertOptionString(title)
                    var sAddress = convertOptionString(address)
                    var sCity = convertOptionString(city)
                    var sState = convertOptionString(state)
                    var zipcode = convertOptionString(zip)
                    var sNpiNumber = convertOptionString(npiNumber)
                    var primaryPhone = convertOptionString(staffRow.cellPhone)
                    var sEmail = convertOptionString(email)
                    val sDateOfBirth = DateTimeFormat.forPattern("MM/dd/yyyy").print(convertOptionalLocalDate(staffRow.dateOfBirth))
                    var sPrimaryFax = convertOptionString(staffRow.primaryFax)
                    // var sClinicianRoleType = convertOptionString(staffRow.clinicianRoleType)

                    val json: String ="""{"Prefix": "rPrefix","FirstName": "rFirstName","LastName": "rLastName","DateOfBirth": "rDateOfBirth","Email" : "rEmail","Address1": "rAddress1","City":"rCity","State":"rState","ZipCode":"rZipCode","PrimaryPhone":"rPrimaryPhone","PrimaryPhoneType":"2","PrimaryFax": "rPrimaryFax", "ClinicianRoleType": [1],"NPINumber":"rNpiNumber"}""".stripMargin

                    var staffJson = json.replace("rPrefix",sTitle).replace("rFirstName", firstName).replace("rLastName", sLastName).replace("rDateOfBirth", sDateOfBirth).replace("rEmail", sEmail).replace("rAddress1",sAddress).replace("rCity",sCity).replace("rState", sState).replace("rZipCode", zipcode).replace("rPrimaryPhone", primaryPhone).replace("rPrimaryFax", sPrimaryFax).replace("rNpiNumber", sNpiNumber)

                    val url = s"${doseSpotUrl}webapi/api/clinicians/${clinicianId}"
                    val result = Http(url)
                    .postData(staffJson)
                    .header("Content-Type", "application/json")
                    .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
                    .header("Charset", "UTF-8")
                    .option(HttpOptions.readTimeout(10000)).asString
                  }
                    if (updateExpiry) {
                      Ok {
                        Json.obj(
                          "expirationDate" -> strNewExpirationDate
                        )
                      }
                    } else {
                      Ok
                    }
                  }
              }
            case None =>
              DBIO.successful(NotFound)
          }
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }

  }

case class DoseSpotSEncryptedCode(
  encryptedClinicId:String,
  encryptedUserId:String
  )

def generateDoseSpotEncryptedCodes(clinicCode:String,userId:String):DoseSpotSEncryptedCode = {
    var randomClinicPhrase = Random.alphanumeric take 32 mkString
    var randomPhraseClinicKey = randomClinicPhrase + clinicCode

    var sha512ClinicString = MessageDigest.getInstance("SHA-512").digest(randomPhraseClinicKey.getBytes("UTF-8"))
    var base64ClinicString = Base64.getEncoder.encodeToString(sha512ClinicString)
    var encryptedClinicId = base64ClinicString
    if (base64ClinicString.takeRight(2)=="=="){
       encryptedClinicId = base64ClinicString.dropRight(2)
    }
    encryptedClinicId = randomClinicPhrase + encryptedClinicId

    var randomUserPhrase = randomClinicPhrase.take(22)
    var randomUserKey = userId + randomUserPhrase
    var appendClinicKey = randomUserKey + clinicCode

    var sha512UserString = MessageDigest.getInstance("SHA-512").digest(appendClinicKey.getBytes("UTF-8"))
    var base64UserString = Base64.getEncoder.encodeToString(sha512UserString)
    var encryptedUserId = base64UserString
    if (base64UserString.takeRight(2)=="=="){
       encryptedUserId = base64UserString.dropRight(2)
    }

    val DoseSpotSEncryptedCodeObj = new DoseSpotSEncryptedCode(encryptedClinicId, encryptedUserId);
    return DoseSpotSEncryptedCodeObj;
}

def getAccessToken(doseSpotUrl: String,clinicId:String,clinicKey:String,userId:String):String = {
  val DoseSpotSEncryptedCodeObj:DoseSpotSEncryptedCode=generateDoseSpotEncryptedCodes(clinicKey,userId)
  val url = s"${doseSpotUrl}webapi/token"
  val encryptedClinicId = DoseSpotSEncryptedCodeObj.encryptedClinicId
  val encryptedUserId = DoseSpotSEncryptedCodeObj.encryptedUserId

  val result = Http(url)
  .header("Content-Type", "application/x-www-form-urlencoded")
  .header("Charset", "UTF-8")
  .auth(clinicId,encryptedClinicId)
  .postForm(Seq(
    "grant_type" -> "password",
    "Username" -> s"$userId",
    "Password" -> s"$encryptedUserId"))
    .option(HttpOptions.readTimeout(10000)).asString

  val jsonObject: JsValue = Json.parse(result.body)
  var accessToken:String=""

  if (result.code==200){
    accessToken = (jsonObject \ "access_token").as[String]
  }
  return accessToken;
}

  def updateAuthorizeCustId = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    (request.body \ "authorizeCustId").validateOpt[Long] match {
      case JsSuccess(authorizeCustId, _) => db.run {
        val query = StaffTable.filter(s => s.id === request.identity.id )
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true =>
            query.map(_.authorizeCustId).update(authorizeCustId)
              .map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

def updateAgreementAccepted = silhouette.SecuredAction.async(parse.json) { request =>
    (request.body \ "agreementAccepted").validateOpt[Boolean] match {
      case JsSuccess(agreementAccepted, _) => db.run {
        val query = StaffTable.filter(s => s.id === request.identity.id )
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true =>
            query.map(_.agreementAccepted).update(agreementAccepted)
              .map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def setUDID = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    (request.body \ "udid").validateOpt[String] match {
      case JsSuccess(udid, _) => db.run {
        val query = StaffTable.filter(s => s.id === request.identity.id && s.udid.getOrElse("") === "")
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true =>
            query.map(_.udid).update(udid)
              .map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }
  
  def setUDID1 = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    (request.body \ "udid1").validateOpt[String] match {
      case JsSuccess(udid1, _) => db.run {
        val query = StaffTable.filter(s => s.id === request.identity.id && s.udid1.getOrElse("") === "")
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true =>
            query.map(_.udid1).update(udid1)
              .map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def updateUdid = silhouette.SecuredAction(StaffOnly).async(parse.json) { request =>
    val udid = for {
      udid <- (request.body \ "udid").validateOpt[String]
      udid1 <- (request.body \ "udid1").validateOpt[String]
      udid2 <- (request.body \ "udid2").validateOpt[String]

    } yield {
      (udid,udid1,udid2)
    }
    udid match {
      case JsSuccess((udid,udid1,udid2), _) =>
      db.run {
        val query = StaffTable.filter(_.id === request.identity.id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            udid.map(value => query.map(_.udid).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            udid1.map(value => query.map(_.udid1).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            udid2.map(value => query.map(_.udid2).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }


  def extendExpirationDate(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>

    val extend = for {
      subscriptionPeriod <- (request.body \ "subscriptionPeriod").validateOpt[Int]
      accountType <- (request.body \ "accountType").validateOpt[String]
      strCollectedDate <- (request.body \ "collectedDate").validateOpt[String]
      amountCollected <- (request.body \ "amountCollected").validateOpt[Long]
      strExpirationDate <- (request.body \ "expirationDate").validateOpt[String]
      strAccessStartDate <- (request.body \ "accessStartDate").validateOpt[String]
    } yield {
      val collectedDate: DateTime = {
        strCollectedDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }
      val expirationDate: DateTime = {
        strExpirationDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }
      val accessStartDate: DateTime = {
        strAccessStartDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }

      (subscriptionPeriod, accountType, collectedDate, amountCollected, expirationDate, accessStartDate)
    }
    var response: StaffRow = null
    // var strNewExpirationDate: DateTime = DateTime.now()

    extend match {
      case JsSuccess((subscriptionPeriod, accountType, collectedDate, amountCollected, expirationDate, accessStartDate), _) =>
        db.run {
          for {
            preExpirationDate <- StaffTable.filter(_.id === id).map(_.expirationDate).result.head
            plan <- StaffTable.filter(_.id === id).map(_.plan).result.head
          } yield {

            // if (expirationDate > DateTime.now()) {
            //   strNewExpirationDate = expirationDate;
            // }

            // strNewExpirationDate = subscriptionPeriod match {
            //   case None => DateTime.now.plusYears(1) //Or handle the lack of a value another way: throw an error, etc.
            //   case Some(l: Int) => strNewExpirationDate.plusMonths(l)
            // }

            val strAccountType = accountType match {
              case Some(l: String) => l
            }

            db.run {
              RenewalDetailsTable.returning(RenewalDetailsTable.map(_.id)) += RenewalDetailsRow(None, id, subscriptionPeriod, plan, DateTime.now(), preExpirationDate, expirationDate, "renew", strAccountType, amountCollected, Some(collectedDate),Some(accessStartDate))
            }

            db.run {
              val query = StaffTable.filter(_.id === id).map(_.expirationDate).update(expirationDate)
              query.flatMap[Result, NoStream, Effect.Read] {
                case 0 => DBIO.successful(NotFound)
                case _ => StaffTable.filter(_.id === id).result.head map (row =>
                  Ok(Json.toJson(row)))
              }
            }
          }
        } map { _ =>
          Ok {
            Json.obj(
              "expirationDate" -> expirationDate
            )
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def extendExpirationDate1(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>

    db.run {
      val query = StaffTable.filter(_.id === id).map(_.expirationDate).update(DateTime.now.plusYears(1))
      query.flatMap[Result, NoStream, Effect.Read] {
        case 0 => DBIO.successful(NotFound)
        case _ => StaffTable.filter(_.id === id).result.head map (row => Ok(Json.toJson(row)))
      }
    }
  }


  def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      SessionTable.filter(_.staffId === id).delete.flatMap { _ =>
        StaffTable.filterNot(_.archived).filter(_.id === id).map(_.archived).update(true)
      }
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

  def clearUDID(email: String) = silhouette.UnsecuredAction.async {
    db.run {
      StaffTable.filter(_.email === email).map(_.udid).update(Some(""))
      StaffTable.filter(_.email === email).map(_.udid1).update(Some(""))
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

  implicit val renewalDetails = Json.format[RenewalDetailsRow]


  def readAllRenewalDetails(staffId: Long) = silhouette.SecuredAction(AdminOnly).async { request =>
    var query = RenewalDetailsTable.filter(_.staffId === staffId).filter(_.accountType === "Paid")
    db.run {
      query.sortBy(_.renewalDate).result
    } map { row =>
      Ok(Json.toJson(row))
    }
  }

  def updateRenewalDetails(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val details = for {
      staffId <- (request.body \ "staffId").validateOpt[Long]
      amountCollected <- (request.body \ "amountCollected").validateOpt[Long]
      strCollectedDate <- (request.body \ "collectedDate").validateOpt[String]
    } yield {
      val collectedDate: DateTime = {
        strCollectedDate match {
          case None => DateTime.now() //Or handle the lack of a value another way: throw an error, etc.
          case Some(l: String) => DateTime.parse(l)
        }
      }
      (staffId, amountCollected, collectedDate)
    }

    details match {
      case JsSuccess((staffId, amountCollected, collectedDate), _) => db.run {
        val query = RenewalDetailsTable.filter(_.id === id).filter(_.staffId === staffId)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            amountCollected.map(value => query.map(_.amountCollected).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            query.map(_.collectedDate).update(Some(collectedDate))
          ) map { _ => Ok }
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def bulkUpdate = silhouette.SecuredAction.async(parse.json) { request =>

  val marketingSource = "Dental Organization"
  val accountType = "ICOI"
  val accessType = "dpatients,catalog,documents,dashboard,newProcedure,appointments,requestConsultation,patientEducation,CIICourse,canAccessWeb,reports,continuingEducation"
  val webModules = "appointment,reports,catalog,patients,dashboard"
  val prime = false
  val subscriptionPeriod = "1"

  val url = "http://novadontics-testing.s3.amazonaws.com/test.csv"

	val response = Http(url)
          .header("Content-Type", "text/xml")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(10000)).asString

  val stream: java.io.InputStream = new java.io.ByteArrayInputStream(response.body.getBytes(java.nio.charset.StandardCharsets.UTF_8.name))
  val reader = new BufferedReader(new InputStreamReader(stream));

  var line: String = null
  var count: Int = 0
   line = reader.readLine()
      while (line != null) {

       if(count != 0) {

        if(line != null && line != None && line != ""){
          val cols = line.split(",").map(_.trim)
      var practiceName = ""

      if(cols(0) != null && cols(0) != "" && cols(0) != None){
        practiceName = cols(0)
      } else {
        practiceName = cols(8) + " " + cols(9)
      }
    
    var onlineInviteMessage = "Please fill the online registration form <<Link>>";
    var appointmentReminderMessage = "Please confirm your consultation at <<Link>>";
    var consentFormMessage = "Please fill <<Form Name>> using the below url \n <<Link>>";

    var practiceRow = PracticeRow(None, practiceName, cols(1), cols(2), cols(3), cols(4), cols(5), cols(6), DateTime.now, Some(""), Some(""), Some(""), None, Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), None, Some(""), Some(""), None, Some(""), Some(""), Some(""), Some(""), Some(""), Some(""),None,Some(0),Some(""),Some(false),Some(""),Some("Every day"),Some("3"),Some(false),Some(onlineInviteMessage),Some(appointmentReminderMessage),Some(consentFormMessage),Some(""),Some(""),Option.empty[Double],Option.empty[Double],Some(""),Option.empty[Long],Some(""),Some(""))

               var block = db.run {
                   PracticeTable.returning(PracticeTable.map(_.id)) += practiceRow
                  } map {
                case 0L => PreconditionFailed
                case id => db.run{
                  PracticeTable.sortBy(_.id.desc).result.head
                }map{practiceDetails =>

                  var staffRow = StaffRow(None,cols(8),cols(10),passwordHasher.hash(cols(8)+"23$").password,StaffRow.Role.Dentist,convertOptionLong(practiceDetails.id),DateTime.now, None,None, false, Some(""), Some(""), Some(cols(6)), Some(cols(1)), Some(cols(3)), Some(cols(2)), Some(cols(4)), Some(cols(5)), DateTime.now().plusMonths(1), Some(0), accountType, cols(7), Some(cols(9)), Some(marketingSource), Some(""),Some(1), Some(""), Some(""), Some(accessType), Some(false), Some(""), Some(0), Some(DateTime.now),Some(""),Some(webModules), Some(false), Some(0),Some(false),Some(""),Some(0),Some(false),Some(""),Some(""),Some(""),Some(""),Some(""),None,Some(""),Some(""), None,false)

                  db.run {
                      StaffTable.returning(StaffTable.map(_.id)) += staffRow
                      } map {
                    case 0L => PreconditionFailed
                    case id => Created(Json.toJson(staffRow.copy(id = Some(id))))
                  }
                }
              }
              val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(120, SECONDS))
            }
        }
    count += 1
    line = reader.readLine()
    }
    reader.close();
    Future.successful(Ok)
}


def conEdTrackCreate = silhouette.SecuredAction.async(parse.json) { request =>
 var staffId = request.identity.id.get
 var isCourseAvailable = false
 var isLimitExceed = false

    val continueEducation = for {
      courseId <- (request.body \ "courseId").validate[Long]
    } yield {
      ConEdTrackRow(None,staffId,courseId,LocalDate.now())
    }

    continueEducation match {
      case JsSuccess(conEdTrackRow, _) =>

    val count = db.run{
          for {
            conEdResult <- ConEdTrackTable.filter(_.staffId === staffId).result
          } yield {

          conEdResult.foreach(result =>{
              if(result.courseId == conEdTrackRow.courseId){
                isCourseAvailable = true
              }
            })

            if(conEdResult.length >= 10){
                isLimitExceed = true
            }
      }
    }
    var AwaitResult1 = Await.ready(count, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      if(isLimitExceed & !isCourseAvailable){
          val newObj = Json.obj() + ("Show" -> JsBoolean(false)) + ("message" -> JsString("You have already reached your maximum CE course limit for this year!"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
      } else{
        val newObj = Json.obj() + ("Show" -> JsBoolean(true)) + ("message" -> JsString(""))
          if(isCourseAvailable){
            Future(Ok{Json.toJson(Array(newObj))})
          } else{
             db.run{
                ConEdTrackTable.returning(ConEdTrackTable.map(_.id)) += conEdTrackRow
            } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(Array(newObj)))
            }
          }
      }

      case JsError(_) => Future.successful(BadRequest)
    }
}

def migrateAccessStartDate = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
  db.run{
    StaffTable.filterNot(_.archived).result
  } map { staffList =>
      (staffList).foreach(staffResult => {
        var block = db.run{
          RenewalDetailsTable.filter(_.staffId === staffResult.id).sortBy(_.renewalDate.desc).result.head
        } map { renewalList =>
            db.run{
              StaffTable.filter(_.id === staffResult.id).map(_.accessStartDate).update(Some(renewalList.renewalDate))
            }
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      })
      Ok{Json.obj("status"->"Success","message"->"Migration completed")}
  }
}

def updateCeInfo = silhouette.SecuredAction.async(parse.json) { request =>
  var staffId = request.identity.id.get
  db.run {
     StaffTable.filter(_.id === staffId).map(_.ceInfo).update(true)
      } map {
      case 0 => NotFound
      case _ => Ok{Json.obj("status"->"Success","message"->"CE info updated.")}
     }
  }


def getValue(spanId: String, htmlContent: String) = {
  val value = htmlContent.substring(htmlContent.indexOf(spanId)).slice(
htmlContent.substring(htmlContent.indexOf(spanId)).indexOf(">")+1,htmlContent.substring(htmlContent.indexOf(spanId)).indexOf("<")
  )
  println("value1 = "+value)
  value
}

  def signatureFile = silhouette.UnsecuredAction(parse.multipartFormData) { request =>
  var controlno = ""
  var patientId = ""
  // var requestDate = LocalDate.now();
  // var receivedDate = LocalDate.now();
  // var subscriberType = ""
  // var subscriberChartNumber = ""
  println("request = "+request.body)
  var eligibilityResponse = ""
  var activeCoverage = "no"
  var resultCode = ""
  var resultDesc = ""
      request.body.file("customFile") match {
        case Some(file) => {
          import java.io.File
          val filename = file.filename
          val trojen = Source.fromFile(file.ref.file).mkString
          println("trojen = "+trojen)
          val contetType = file.contentType

          if(trojen.contains("resultcode")){
            resultCode = getValue("resultcode",trojen)
            println("resultCode = "+resultCode)
          }
          if(trojen.contains("resultdesc")){
          resultDesc = getValue("resultdesc",trojen)
            println("resultdesc = "+resultDesc)
          }
          if(trojen.contains("controlno")){
          controlno = getValue("controlno",trojen)
            println("controlno = "+controlno)
          }
          if(trojen.contains("patientid")){
          patientId = getValue("patientid",trojen)
            println("patientid = "+patientId)
          }
          // if(trojen.contains("requestDate")){
          // val requestDate1 = getValue("requestDate",trojen)
          //   println("requestDate = "+requestDate1)
          // }
          if(resultCode == "G"){
            activeCoverage = "yes"
          }
          eligibilityResponse = trojen

          var block =  db.run {
                    val query = EligibilityTable.filter(_.id === controlno.toLong)
                    query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                      case true => DBIO.seq[Effect.Write](
                        query.map(_.patientId).update(patientId.toLong),
                        query.map(_.resultCode).update(resultCode),
                        query.map(_.resultDesc).update(resultDesc),
                        query.map(_.activeCoverage).update(activeCoverage),
                        query.map(_.eligibilityResponse).update(eligibilityResponse)

                      ) map {_ => Ok}
                      case false => DBIO.successful(NotFound)
                    }
                  }
        var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
          Ok("congratz you did it")
  }

  
        
        case _ => Ok
      }
  }
}



