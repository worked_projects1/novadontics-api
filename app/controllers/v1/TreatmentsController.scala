package controllers.v1

import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream}
import java.net.URL
import javax.imageio.ImageIO
import play.api.libs.json.{JsValue, Json}
import scala.util.control._

import akka.stream.scaladsl.StreamConverters
import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject._
import com.google.inject.name.Named
import com.itextpdf.text.pdf.{ColumnText, PdfPCell, PdfPageEventHelper, PdfWriter}
import com.itextpdf.text.{BaseColor, Document, Element, Font, FontFactory, Phrase, Rectangle}
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.StepRow.State
import models.daos.tables.{FormRow, StaffRow, StepRow, TreatmentRow, ClinicalNotesRow,MeasurementHistoryRow,
UserRow,ToothChartRow,PerioChartRow,MedicalConditionLogRow,CartRow,OperatorTableRow,DocumentRow,EndoChartRow,OnlineRegInviteTrackRow}
import play.api.http.HttpEntity
import play.api.libs.json.Json.{obj, toJson}
import play.api.libs.json._
import play.api.mvc.{ResponseHeader, Result}
import play.api.{Application, Logger}
import java.text.SimpleDateFormat

import java.time.format.DateTimeFormatter
import java.util.{Date, Locale}
import org.joda.time.{DateTimeZone}
import org.joda.time.format.DateTimeFormat
import java.io.File
import com.github.tototoshi.csv._
import java.text.NumberFormat
import java.text.DecimalFormat

import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import com.itextpdf.text.pdf._
import com.itextpdf.text.{List => _, _}
import com.itextpdf.text.Paragraph
import scala.collection.mutable.ListBuffer
import com.itextpdf.text.Chunk
import java.io.{ByteArrayInputStream, DataInputStream}
import com.amazonaws.services.s3.model.{ ObjectMetadata, S3ObjectInputStream, ListObjectsRequest, GetObjectRequest }
import scalaj.http.{Http, HttpOptions}
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.util.Random
import java.security.MessageDigest
import java.util.Base64
import java.net.URLEncoder
import co.spicefactory.services.EmailService


@Singleton
class TreatmentsController @Inject()(ses: AmazonSimpleEmailService , silhouette : Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext , val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  private val from = application.configuration.getString("aws.ses.orderInvoice.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val replyTo = application.configuration.getString("aws.ses.orderInvoice.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val subjectManager = application.configuration.getString("aws.ses.orderInvoice.subjectManager").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val sesArn = application.configuration.getString("aws.ses.arn").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  private val sesRegion = application.configuration.getString("aws.ses.region").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  ses.setRegion(Region.getRegion(Regions.fromName(sesRegion)))

  private val logger = Logger(this.getClass)

  implicit val MedicalConditionFormat = Json.format[MedicalConditionLogRow]
  implicit val OnlineRegInviteTrackFormat = Json.format[OnlineRegInviteTrackRow]

  implicit val DocumentRowFormat = Json.format[DocumentRow]

  implicit val treatmentRowWrites = Writes { treatment: TreatmentRow =>
    var toothChartDate=treatment.toothChartUpdatedDate
    if (toothChartDate==None){
      toothChartDate = Some(DateTime.now)
    }
    var birthDate=""
    var gender=""
    
    val f = db.run{
          for {
                step <-StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
          }yield{
               birthDate =  (step.value \ "birth_date").as[String]
               gender =  (step.value \ "gender").as[String]
          }
    }
    
    val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))        
    
    
    Json.obj(
      "id" -> treatment.id,
      "staffId" -> treatment.staffId,
      "patientId" -> treatment.patientId,
      "firstVisit" -> treatment.firstVisit,
      "lastEntry" -> treatment.lastEntry,
      "phoneNumber" -> treatment.phoneNumber,
      "emailAddress" -> treatment.emailAddress,
      "ctScanFile" -> treatment.ctScanFile,
      "contracts" -> treatment.contracts,
      "xray" -> treatment.xray,
      "scratchPad" -> treatment.scratchPad,
      "scratchPadText" -> treatment.scratchPadText,
      "toothChartUpdatedDate" -> toothChartDate,
      "birthDate" -> birthDate,
      "gender" -> gender
    ) 
  }

  case class ToothConditionsRow(
    id : Option[Long],
    missingTooth : Option[String],
    edentulousMaxilla : Option[Boolean],
    edentulousMandible : Option[Boolean],
    allPrimary : Option[Boolean],
    primaryTooth : Option[String]
  )
  implicit val ToothConditionFormat = Json.format[ToothConditionsRow]

    case class OutstandingInsurancePDF(
    patientId : Long,
    patientName : String,
    cdtCode : String,
    datePerformed : String,
    estIns : Float,
    insurancePaidAmt : Float,
    balanceAmt : Float
  )

  implicit val outstandingInsuranceRowWrites = Writes { OutstandingInsuranceList: (OutstandingInsurancePDF) =>
    val (OutstandingInsuranceRow) = OutstandingInsuranceList

  Json.obj(
      "patientId" -> OutstandingInsuranceRow.patientId,
      "patientName" -> OutstandingInsuranceRow.patientName,
      "cdtCode" -> OutstandingInsuranceRow.cdtCode,
      "datePerformed" -> OutstandingInsuranceRow.datePerformed,
      "estIns" -> Math.round(OutstandingInsuranceRow.estIns * 100.0) / 100.0,
      "insurancePaidAmt" -> Math.round(OutstandingInsuranceRow.insurancePaidAmt * 100.0) / 100.0,
      "balanceAmt" -> Math.round(OutstandingInsuranceRow.balanceAmt * 100.0) / 100.0
      )
  }

    case class TotalCollectedAmountPDF(
    patientId : Long,
    patientName : String,
    cdtCode : String,
    fee : Float,
    paymentReceivedFromInsurance : Float,
    paymentReceivedFromPatient : Float,
    totalCollected : Float,
    totalCollectedPercent : Float
  )

    implicit val totalCollectedAmountRowWrites = Writes { TotalCollectedAmountList: (TotalCollectedAmountPDF) =>
    val(totalCollectedAmountRow) = TotalCollectedAmountList

  Json.obj(
      "patientId" -> totalCollectedAmountRow.patientId,
      "patientName" -> totalCollectedAmountRow.patientName,
      "cdtCode" -> totalCollectedAmountRow.cdtCode,
      "fee" -> totalCollectedAmountRow.fee,
      "paymentReceivedFromInsurance" -> Math.round(totalCollectedAmountRow.paymentReceivedFromInsurance * 100.0) / 100.0,
      "paymentReceivedFromPatient" -> Math.round(totalCollectedAmountRow.paymentReceivedFromPatient * 100.0) / 100.0,
      "totalCollected" -> Math.round(totalCollectedAmountRow.totalCollected * 100.0) / 100.0,
      "totalCollectedPercent" -> Math.round(totalCollectedAmountRow.totalCollectedPercent * 100.0) / 100.0
      )
  }

  implicit val EndoChartRowWrites = Json.writes[EndoChartRow]

  implicit val MeasurementHistoryRowWrites = Json.writes[MeasurementHistoryRow]
  implicit val ToothChartRowWrites = Json.writes[ToothChartRow]
  // implicit val PerioChartRowWrites = Json.writes[PerioChartRow]
  implicit val perioChartRowWrites = Writes { perioChartList: (PerioChartRow) =>
    val(perioChartRow) = perioChartList
  var formatter = DateTimeFormat.forPattern("MM/dd/YYYY");
  Json.obj(
      "id" -> perioChartRow.id,
      "patientId" -> perioChartRow.patientId,
      "rowPosition" -> perioChartRow.rowPosition,
      "perioType" -> perioChartRow.perioType,
      "dateSelected" -> formatter.print(perioChartRow.dateSelected),
      "perioNumbers" -> perioChartRow.perioNumbers,
      "staffId" -> perioChartRow.staffId,
      "userId" -> perioChartRow.userId,
      "providerId" -> perioChartRow.providerId
      )
  }

  case class Step(
    value: JsValue,
    state: StepRow.State,
    formId: String,
    formVersion: Int,
    dateCreated: Option[DateTime] = None
  )

  object Step {
    implicit val format = Json.format[Step]
  }

  private def checkAndInsertMeasurementHistory(id: Long,paramFormId:String,step:Step) = {
    val now = DateTime.now
    var vsystolic = ""
    var vdiastolic = ""
    var vpulse = ""
    var voxygen = ""
    var vbodyTemp = ""
    var vglucose = ""
    var va1c = ""
    var vinr = ""
    var vpt = ""
    
    val jsonObject = Json.parse(step.value.toString())

    (jsonObject \ "systolic").validate[String] match {
      case JsSuccess(systolic, _) =>
        vsystolic =s"$systolic" 
      case e: JsError => vsystolic = ""
    }
    (jsonObject \ "diastolic").validate[String] match {
      case JsSuccess(diastolic, _) =>
        vdiastolic =s"$diastolic" 
      case e: JsError => vdiastolic = ""
    }
    (jsonObject \ "pulse").validate[String] match {
      case JsSuccess(pulse, _) =>
        vpulse =s"$pulse" 
      case e: JsError => vpulse = ""
    }
    (jsonObject \ "oxygen").validate[String] match {
      case JsSuccess(oxygen, _) =>
        voxygen =s"$oxygen" 
      case e: JsError => voxygen = "" 
    }
    (jsonObject \ "bodyTemp").validate[String] match {
      case JsSuccess(bodyTemp, _) =>
        vbodyTemp =s"$bodyTemp"
      case e: JsError => vbodyTemp = ""
    }
    (jsonObject \ "glucose").validate[String] match {
      case JsSuccess(glucose, _) =>
        vglucose =s"$glucose"
      case e: JsError => vglucose = ""
    }
    (jsonObject \ "a1c").validate[String] match {
      case JsSuccess(a1c, _) =>
        va1c =s"$a1c"
      case e: JsError => va1c = ""
    }
    (jsonObject \ "inr").validate[String] match {
      case JsSuccess(inr, _) =>
        vinr =s"$inr"
      case e: JsError => vinr = ""
    }
    (jsonObject \ "pt").validate[String] match {
      case JsSuccess(pt, _) =>
        vpt =s"$pt"
      case e: JsError => vpt = ""
    }

      
    if (vsystolic!= "" || vdiastolic != "" || vpulse!= "" || voxygen!= "" ||
          vbodyTemp!= "" || vglucose != "" || va1c!= "" || vinr!= "" || vpt!= ""){
        db.run {
          for {
                count <-MeasurementHistoryTable.filter(_.patientId === id).filter(_.formId === paramFormId).sortBy(_.dateCreated.desc).countDistinct.result
          }yield{
            if (count==0){
                db.run {
                  MeasurementHistoryTable.returning(MeasurementHistoryTable.map(_.id)) += MeasurementHistoryRow(None,id,paramFormId,parseDouble(vsystolic),parseDouble(vdiastolic),parseDouble(vpulse),parseDouble(voxygen),parseDouble(vbodyTemp),parseDouble(vglucose),parseDouble(va1c),parseDouble(vinr),parseDouble(vpt),now)
                }
            }else{
                 db.run { 
                     for {
                          measurementHistory <- MeasurementHistoryTable.filter(_.patientId === id).filter(_.formId === paramFormId).sortBy(_.dateCreated.desc).result.head
                     } yield {
                          if (convertOptionDouble(parseDouble(vsystolic)) != convertOptionDouble(measurementHistory.systolic) || convertOptionDouble(parseDouble(vdiastolic)) != convertOptionDouble(measurementHistory.diastolic) || convertOptionDouble(parseDouble(vpulse))!=convertOptionDouble(measurementHistory.pulse)
                          || convertOptionDouble(parseDouble(voxygen)) != convertOptionDouble(measurementHistory.oxygen)
                          || convertOptionDouble(parseDouble(vbodyTemp)) != convertOptionDouble(measurementHistory.bodyTemp)
                          || convertOptionDouble(parseDouble(vglucose)) != convertOptionDouble(measurementHistory.glucose)
                          || convertOptionDouble(parseDouble(va1c)) != convertOptionDouble(measurementHistory.a1c)
                          || convertOptionDouble(parseDouble(vinr)) != convertOptionDouble(measurementHistory.inr)
                          || convertOptionDouble(parseDouble(vpt)) != convertOptionDouble(measurementHistory.pt)){
                                db.run {
                                  MeasurementHistoryTable.returning(MeasurementHistoryTable.map(_.id)) += MeasurementHistoryRow(None,id,paramFormId,parseDouble(vsystolic),parseDouble(vdiastolic),parseDouble(vpulse),parseDouble(voxygen),
                                  parseDouble(vbodyTemp),parseDouble(vglucose),parseDouble(va1c),parseDouble(vinr),parseDouble(vpt),now)
                                }
                          }  
                          
                      }
                  } 
            }    
          }        
          
        }
    }  
  }  
  

  implicit val stepStateFormat = new Format[StepRow.State] {
    import StepRow.State._

    override def writes(state: State): JsValue = state match {
      case ToDo => Json.toJson("ToDo")
      case InProgress => Json.toJson("InProgress")
      case Completed => Json.toJson("Completed")
      case Sealed => Json.toJson("Sealed")
    }

    override def reads(json: JsValue): JsResult[State] = json.validate[String].map {
      case "InProgress" => InProgress
      case "Completed" => Completed
      case "Sealed" => Sealed
      case _ => ToDo
    }
  }

  case class TreatmentList(
    id: Long,
    patientId: String,
    patientName: Option[String],
    firstName: String,
    lastName: String,
    firstVisit: Option[DateTime],
    lastEntry: Option[DateTime],
    phoneNumber: String,
    emailAddress: String,
    providerName: String,
    steps: List[Step],
    ctScanFile: Option[String],
    contracts: Option[String],
    xray: Option[String],
    scratchPad: Option[String],
    scratchPadText: Option[String],
    toothChartUpdatedDate: Option[DateTime],
    patient_type: Option[String],
    patient_status: Option[String],
    referralSource : Option[String],
    treatmentStarted : Boolean,
    recallPeriod : Option[String],
    birthDate: Option[String],
    gender: Option[String],
    shared: Boolean,
    sharedBy: String,
    sharedDate: Option[LocalDate],
    mainSquares: String,
    icons: String,
    accessTo: String,
    accessLevel: String,
    recallType: Option[String],
    recallCode: Option[String],
    recallCodeDescription: Option[String],
    intervalNumber: Option[Long],
    intervalUnit: Option[String],
    recallFromDate: Option[LocalDate],
    recallOneDayPlus: Option[Boolean],
    recallDueDate: Option[LocalDate],
    recallNote: Option[String],
    family: Option[Boolean],
    examImaging: Option[String]
  )
  
  
  
  case class TreatmentSingle(
    id: Long,
    patientId: String,
    patientName: Option[String],
    firstVisit: Option[DateTime],
    lastEntry: Option[DateTime],
    phoneNumber: String,
    emailAddress: String,
    steps: List[Step],
    ctScanFile: Option[String],
    contracts: Option[String],
    xray: Option[String],
    scratchPad: Option[String],
    scratchPadText: Option[String],
    toothChartUpdatedDate: Option[DateTime],
    bp_pulse_o2: String,
    recallPeriod : String,
    shareIcon: Boolean
  )

  case class PatientOutstandingBalance(
    patientName: String,
    patientId: Long,
    cdtCode: String,
    balanceToPatient: String,
    patientPaid: String,
    outstandingBalance: String,
    datePerformed: String
  )

  case class PatientOutstandingBalanceList(
    patientName: String,
    patientId: Long,
    cdtCode: String,
    balanceToPatient: Float,
    patientPaid: Float,
    outstandingBalance: Float,
    datePerformed: String
  )

  object PatientOutstandingBalanceList {
    implicit val format = Json.format[PatientOutstandingBalanceList]
  }


    case class TreatmentListRow(
    id: Long,
    name: String,
    referredBy: String,
    medicalCondition : Boolean,
    phoneNumber : String,
    alerts: String,
    birthDate: String,
    email: String,
    insurance: String
  )

    case class PatientTypePDF(
    treatmentId : Long,
    patientName: String,
    firstVisit: String,
    phoneNumber : String,
    address : String,
    email : String,
    providerName: String,
    patientType : String
  )

implicit val PatientTypeWrites = Writes { PatientTypeRecord: (PatientTypePDF) =>
    val (patientTypeRow) = PatientTypeRecord

  Json.obj(
      "treatmentId" -> patientTypeRow.treatmentId,
      "patientName" -> patientTypeRow.patientName,
      "firstVisit" -> patientTypeRow.firstVisit,
      "phoneNumber" -> patientTypeRow.phoneNumber,
      "address" -> patientTypeRow.address,
      "email" -> patientTypeRow.email,
      "providerName" -> patientTypeRow.providerName,
      "patientType" -> patientTypeRow.patientType
      )
  }

case class PerioNumber(
            PN: String,
            B: String,
            F: String,
            R:String
      )

case class ReferralReport(
            referralSource: String,
            size: Int,
            patientName: String,
            phone: String,
            email: String,
            providerName: String
      )


implicit val ReferralReportWrites = Writes { ReferralReportRecord: (ReferralReport) =>
    val (referralReportRow) = ReferralReportRecord

  Json.obj(
      "referralSource" -> referralReportRow.referralSource,
      "size" -> referralReportRow.size,
      "patientName" -> referralReportRow.patientName,
      "phone" -> referralReportRow.phone,
      "email" -> referralReportRow.email,
      "providerName" -> referralReportRow.providerName
      )
  }


case class ReferredToRow(
    id: Option[Long],
    referredTo: String,
    size: Int,
    patientName: String,
    phone: String,
    email: String,
    providerName: String
  )


implicit val ReferredReportWrites = Writes { ReferredToRowRecord: (ReferredToRow) =>
    val (referredToRow) = ReferredToRowRecord

  Json.obj(
      "id" -> referredToRow.id,
      "referredTo" -> referredToRow.referredTo,
      "size" -> referredToRow.size,
      "patientName" -> referredToRow.patientName,
      "phone" -> referredToRow.phone,
      "email" -> referredToRow.email,
      "providerName" -> referredToRow.providerName
      )
  }


object PerioNumber {
    implicit val format = Json.format[PerioNumber]
  }


implicit val PerioNumberListWrites = Writes { PerioNumberList: (PerioNumber) =>
  val (PerioNumberRow) = PerioNumberList

  Json.obj(
      "PN" -> PerioNumberRow.PN,
      "B" -> PerioNumberRow.B,
      "F" -> PerioNumberRow.F,
      "R" -> PerioNumberRow.R
      )
  }

implicit val treatmentListRowWrites = Writes { treatmentListRecord: (TreatmentListRow) =>
    val (treatmentListsRow) = treatmentListRecord

  Json.obj(
      "id" -> treatmentListsRow.id,
      "name" -> treatmentListsRow.name,
      "referredBy" -> treatmentListsRow.referredBy,
      "medicalCondition" -> treatmentListsRow.medicalCondition,
      "phoneNumber" -> treatmentListsRow.phoneNumber,
      "alerts" -> treatmentListsRow.alerts,
      "birthDate" -> treatmentListsRow.birthDate,
      "email" -> treatmentListsRow.email,
      "insurance" -> treatmentListsRow.insurance
      )
  }

  implicit val TreatmentListWrites = Writes {TreatmentListObj: TreatmentList  =>
    var birthDate=TreatmentListObj.birthDate
    var gender = TreatmentListObj.gender
    var toothChartUpdatedDate = TreatmentListObj.toothChartUpdatedDate
    if (birthDate == None){
      birthDate = Some("")
    } 
    if (gender == None){
      gender = Some("")
    }  
    if (toothChartUpdatedDate == None){
      toothChartUpdatedDate = Some(DateTime.now)
    } 
    
    
     Json.obj(
        "id"-> TreatmentListObj.id,
        "patientId"-> TreatmentListObj.patientId,
        "patientName"-> TreatmentListObj.patientName,
        "firstName"-> TreatmentListObj.firstName,
        "lastName"-> TreatmentListObj.lastName,
        "firstVisit"-> TreatmentListObj.firstVisit,
        "lastEntry"-> TreatmentListObj.lastEntry,
        "phoneNumber"-> TreatmentListObj.phoneNumber,
        "emailAddress"-> TreatmentListObj.emailAddress,
        "providerName"-> TreatmentListObj.providerName,
        "steps"-> TreatmentListObj.steps,
        "ctScanFile"-> TreatmentListObj.ctScanFile,
        "contracts"-> TreatmentListObj.contracts,
        "xray"-> TreatmentListObj.xray,
        "scratchPad"-> TreatmentListObj.scratchPad,
        "scratchPadText"-> TreatmentListObj.scratchPadText,
        "toothChartUpdatedDate"-> toothChartUpdatedDate,
        "patient_type"-> TreatmentListObj.patient_type,
        "patient_status"-> TreatmentListObj.patient_status,
        "referralSource" -> TreatmentListObj.referralSource,
        "treatmentStarted" -> TreatmentListObj.treatmentStarted,
        "recallPeriod" -> TreatmentListObj.recallPeriod,
        "birthDate"-> birthDate,
        "gender"-> gender,
        "shared" -> TreatmentListObj.shared,
        "sharedBy" -> TreatmentListObj.sharedBy,
        "sharedDate" -> TreatmentListObj.sharedDate,
        "mainSquares" -> TreatmentListObj.mainSquares,
        "icons" -> TreatmentListObj.icons,
        "accessTo" -> TreatmentListObj.accessTo,
        "accessLevel" -> TreatmentListObj.accessLevel,
        "recallType" -> TreatmentListObj.recallType,
        "recallCode" -> TreatmentListObj.recallCode,
        "recallCodeDescription" -> TreatmentListObj.recallCodeDescription,
        "intervalNumber" -> TreatmentListObj.intervalNumber,
        "intervalUnit" -> TreatmentListObj.intervalUnit,
        "recallFromDate" -> TreatmentListObj.recallFromDate,
        "recallOneDayPlus" -> TreatmentListObj.recallOneDayPlus,
        "recallDueDate" -> TreatmentListObj.recallDueDate,
        "recallNote" -> TreatmentListObj.recallNote,
        "family" -> TreatmentListObj.family,
        "examImaging" -> TreatmentListObj.examImaging
       )
  }

    case class MedicalLogRow(
      value:String,
      dateCreated: LocalDate,
      currentlySelected: Boolean
    )

     implicit val medicalLogRowWrites = Writes { medicalLogListRecord: (MedicalLogRow) =>
      val (medicalLogListRow) = medicalLogListRecord

    Json.obj(
        "value" -> medicalLogListRow.value,
        "dateCreated" -> medicalLogListRow.dateCreated,
        "currentlySelected" -> medicalLogListRow.currentlySelected
        )
    }

    case class NewPatientListRow(
    dateOfFirstVisit : String,
    patientName : String,
    phoneNumber : String,
    email : String,
    address : String,
    providerName: String,
    referredBy : String
  )


    implicit val newPatientsRowWrites = Writes { newPatientListRecord: (NewPatientListRow) =>
      val (newPatientsRow) = newPatientListRecord

    Json.obj(
        "dateOfFirstVisit" -> newPatientsRow.dateOfFirstVisit,
        "patientName" -> newPatientsRow.patientName,
        "phoneNumber" -> newPatientsRow.phoneNumber,
        "email" -> newPatientsRow.email,
        "address" -> newPatientsRow.address,
        "providerName" -> newPatientsRow.providerName,
        "referredBy" -> newPatientsRow.referredBy
      )
    }

  implicit val TreatmentSingleWrites = Writes {TreatmentSingleObj: TreatmentSingle  =>

   var medicalConditionList: JsValue = Json.parse("{}")
    var allergyList: JsValue = Json.parse("{}")

//Medical Condition
    var medicalCondition = db.run{
          MedicalConditionLogTable.filter(_.patientId===TreatmentSingleObj.id).filter(s => s.conditionType === "Medical Condition" || s.conditionType === "Other_Medical_Condition")
          .sortBy(_.dateCreated.desc).result
    } map { results=>
    val medical = results.map { list =>
        MedicalLogRow(
          list.value,
          list.dateCreated,
          list.currentlySelected
        )
    }
      medicalConditionList = Json.toJson(medical)
      Ok
    }

//Allergy
    var allergy = db.run{
          MedicalConditionLogTable.filter(_.patientId===TreatmentSingleObj.id).filter(s => s.conditionType === "Allergy" || s.conditionType === "Other_Allergy")
          .sortBy(_.dateCreated.desc).result

    } map { results=>
        val allergy = results.map { list =>
        MedicalLogRow(
          list.value,
          list.dateCreated,
          list.currentlySelected
        )
    }
      allergyList = Json.toJson(allergy)
      Ok
    }

    val AwaitResult1 = Await.ready(medicalCondition, atMost = scala.concurrent.duration.Duration(3, SECONDS))
    val AwaitResult2 = Await.ready(allergy, atMost = scala.concurrent.duration.Duration(3, SECONDS))



    Json.obj(
        "id"-> TreatmentSingleObj.id,
        "patientId" -> TreatmentSingleObj.patientId,
        "patientName" -> TreatmentSingleObj.patientName,
        "firstVisit" -> TreatmentSingleObj.firstVisit,
        "lastEntry" -> TreatmentSingleObj.lastEntry,
        "emailAddress" -> TreatmentSingleObj.emailAddress,
        "ctScanFile" -> TreatmentSingleObj.ctScanFile,
        "contracts"-> TreatmentSingleObj.contracts,
        "xray" -> TreatmentSingleObj.xray,
        "scratchPad" -> TreatmentSingleObj.scratchPad,
        "scratchPadText" -> TreatmentSingleObj.scratchPadText,
        "bp_pulse_o2" -> TreatmentSingleObj.bp_pulse_o2,
        "recallPeriod" -> TreatmentSingleObj.recallPeriod,
        "shareIcon" -> TreatmentSingleObj.shareIcon,
        "medicalCondition" -> medicalConditionList,
        "allergy" -> allergyList
    )
  }  

  def readAll = silhouette.SecuredAction(CanCreateTreatment).async { request =>
  var mergeList = List.empty[TreatmentList]
  //First query
   db.run {
      TreatmentTable.filterNot(_.deleted).join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === request.identity.asInstanceOf[StaffRow].practiceId).join(StepTable).on(_._1.id === _.treatmentId).result
    } map { treatments =>
      val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
      var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
      val patientName = firstName+" "+lastName
      var providerName = ""
      var providerId = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "provider").asOpt[String]))
      if(providerId != ""){
      var block1 = db.run{
            ProviderTable.filter(_.id === providerId.toLong).result
        } map { results =>
        results.map{ providers => providerName = providers.title +" "+ providers.firstName +" "+providers.lastName }
        Ok
        }
        val AwaitResult4 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }

      var isFamily = false
      var familyBlock = db.run{
          FamilyMembersTable.filter(_.patientId === entry.id).length.result
      } map { familyResult =>
          if(familyResult > 0){
            isFamily = true
          }
      }
      val AwaitResult3 = Await.ready(familyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

          var dataList = TreatmentList(
            entry.id.get,
            entry.patientId,
            Some(patientName),
            firstName,
            lastName,
            entry.firstVisit,
            entry.lastEntry,
            entry.phoneNumber,
            entry.emailAddress,
            providerName,
            steps.map(step => Step(step.value, step.state, step.formId, step.formVersion, Some(step.dateCreated))),
            entry.ctScanFile,
            entry.contracts,
            entry.xray,
            entry.scratchPad,
            entry.scratchPadText,
            entry.toothChartUpdatedDate,
            entry.patientType,
            entry.patientStatus,
            entry.referralSource,
            entry.treatmentStarted,
            entry.recallPeriod,
            steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]),
            steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "gender").asOpt[String]),
            false,
            "",
            None,
            "",
            "",
            "",
            "",
            entry.recallType,
            entry.recallCode,
            entry.recallCodeDescription,
            entry.intervalNumber,
            entry.intervalUnit,
            entry.recallFromDate,
            entry.recallOneDayPlus,
            entry.recallDueDate,
            entry.recallNote,
            Some(isFamily),
            entry.examImaging
          )
          mergeList = mergeList :+ dataList
          }.toList
    //Second query
     val block = db.run {
      SharedPatientTable.filter(_.shareTo === request.identity.id.get)
      .join(TreatmentTable).on(_.patientId === _.id).filterNot(_._2.deleted).join(StepTable).on(_._2.id === _.treatmentId).result
    } map { treatments =>
      val data = treatments.groupBy(_._1._2.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._2
          val sharePatient = entries.head._1._1
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
      var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
      val patientName = firstName+" "+lastName

      var providerName = ""
      var providerId = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "provider").asOpt[String]))
      if(providerId != ""){
      var block1 = db.run{
            ProviderTable.filter(_.id === providerId.toLong).result
        } map { results =>
        results.map{ providers => providerName = providers.title +" "+ providers.firstName +" "+providers.lastName }
        Ok
        }
        val AwaitResult4 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }

      var isFamily = false
      var familyBlock = db.run{
          FamilyMembersTable.filter(_.patientId === entry.id).length.result
      } map { familyResult =>
              if(familyResult > 0){
                isFamily = true
              }
      // Ok
      }
      val AwaitResult3 = Await.ready(familyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))


        var sharedBy = ""
          var block1 = db.run{
               StaffTable.filter(_.id === sharePatient.sharedBy).result
          } map { staffResult =>
          staffResult.map{result => sharedBy = result.name + " " + convertOptionString(result.lastName)
          }
          Ok
          }
          var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(10, SECONDS))
         var dataList =  TreatmentList(
            entry.id.get,
            entry.patientId,
            Some(patientName),
            firstName,
            lastName,
            entry.firstVisit,
            entry.lastEntry,
            entry.phoneNumber,
            entry.emailAddress,
            providerName,
            steps.map(step => Step(step.value, step.state, step.formId, step.formVersion, Some(step.dateCreated))),
            entry.ctScanFile,
            entry.contracts,
            entry.xray,
            entry.scratchPad,
            entry.scratchPadText,
            entry.toothChartUpdatedDate,
            entry.patientType,
            entry.patientStatus,
            entry.referralSource,
            entry.treatmentStarted,
            entry.recallPeriod,
            steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]),
            steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "gender").asOpt[String]),
            true,
            sharedBy,
            Some(sharePatient.sharedDate),
            sharePatient.mainSquares,
            sharePatient.icons,
            sharePatient.accessTo,
            sharePatient.accessLevel,
            entry.recallType,
            entry.recallCode,
            entry.recallCodeDescription,
            entry.intervalNumber,
            entry.intervalUnit,
            entry.recallFromDate,
            entry.recallOneDayPlus,
            entry.recallDueDate,
            entry.recallNote,
            Some(isFamily),
            entry.examImaging
          )
          mergeList = mergeList :+ dataList
        }.toList
      Ok
    }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
      Ok(Json.toJson(mergeList.sortBy(_.lastEntry).reverse))
    }
  }

  def patientList = silhouette.SecuredAction.async { request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId

    db.run {
      TreatmentTable.filterNot(_.deleted).join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === practiceId).join(StepTable).on(_._1.id === _.treatmentId).result
    } map { treatments =>
      val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))

      if(phoneNumber == "" || phoneNumber == null){
        phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1L").flatMap(step => (step.value \ "phone").asOpt[String]))
      }

    var referredBy = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "patient_referer").asOpt[String]))
    var birth_date = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]))
    var email = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))

    var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
    var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
    val patientName = firstName+" "+lastName

    var medicalCondition :Boolean  = false
    var block1 = db.run{
          MedicalConditionLogTable.filter(_.patientId === entry.id.get).length.result
      } map { result =>
         if(result > 0) {
            medicalCondition = true
        }
      }
var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    var alerts :String  = ""
    var block2 = db.run{
          MedicalConditionLogTable.filter(_.patientId === entry.id.get).result
      } map { result =>
      result.foreach(alertValue => {
        alerts = alerts + alertValue.value + ", "
      })
      }

    var AwaitResult1 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          TreatmentListRow(
            entry.id.get,
            patientName,
            convertOptionString(entry.referralSource),
            medicalCondition,
            phoneNumber,
            alerts.dropRight(2),
            birth_date,
            email,
            entry.insurance
          )
      }.toList.sortBy(_.name.toLowerCase)
      Ok(Json.toJson(data))
    }
  }

def testRead(id :Long) = silhouette.SecuredAction.async { request =>
     db.run {
          TreatmentTable.filterNot(_.deleted).filter(_.id === id).result
      } map { treatmentList =>
        Ok(Json.toJson(treatmentList))
      }
  }

  def create = silhouette.SecuredAction(CanCreateTreatment).async(parse.json) { request =>
    val treatment = for {
      steps <- (request.body \ "steps").validate[List[Step]]
    } yield {
      var vpatientId = ""
      var vInitialVisitDate = ""
      var vBirthDate = ""
      var vPatientReferer = ""
      var vPatientType : StringBuilder = new StringBuilder();
      var vPatientStatus = ""
      var vCarrierName = ""
      var vFirstName = ""
      var vLastName = ""
      var vDob = ""
      var vProviderId = ""
      var vPhoneNumber = ""
      var vEmailAddress = ""
      var vZipCode = ""
      var vBusinessPhone = ""
      var vPatientRefererTo = ""
      var vReferredDate = ""
      var vCountry = ""

      (steps).foreach(step=>{
            if (step.formId == "1A"){
                val jsonObject = Json.parse(step.value.toString())
                (jsonObject \ "patient_id").validate[String] match {
                  case JsSuccess(patient_id, _) =>
                    vpatientId =s"$patient_id" 
                  case e: JsError => Future.successful(BadRequest)
                }

                (jsonObject \ "first_visit").validate[String] match {
                    case JsSuccess(first_visit, _) =>
                      vInitialVisitDate =s"$first_visit"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                 (jsonObject \ "birth_date").validate[String] match {
                    case JsSuccess(birth_date, _) =>
                      vBirthDate =s"$birth_date"
                    case JsError(_) =>  Future.successful(BadRequest)
                  } 
                 (jsonObject \ "patient_referer").validate[String] match {
                    case JsSuccess(patient_referer, _) =>
                      vPatientReferer =s"$patient_referer"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                 (jsonObject \ "patient_status").validate[String] match {
                    case JsSuccess(patient_status, _) =>
                      vPatientStatus =s"$patient_status"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "carrierName").validate[String] match {
                    case JsSuccess(carrierName, _) =>
                      vCarrierName =s"$carrierName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "full_name").validate[String] match {
                    case JsSuccess(fullName, _) =>
                      vFirstName =s"$fullName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "last_name").validate[String] match {
                    case JsSuccess(lastName, _) =>
                      vLastName =s"$lastName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "provider").validate[String] match {
                    case JsSuccess(providerId, _) =>
                      vProviderId =s"$providerId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "phone").validate[String] match {
                    case JsSuccess(phoneNumber, _) =>
                      vPhoneNumber =s"$phoneNumber"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "email").validate[String] match {
                    case JsSuccess(emailId, _) =>
                      vEmailAddress =s"$emailId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "zip").validate[String] match {
                    case JsSuccess(zipCode, _) =>
                      vZipCode =s"$zipCode"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "business_phone").validate[String] match {
                    case JsSuccess(businessPhone, _) =>
                      vBusinessPhone =s"$businessPhone"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "patientRefererTo").validate[String] match {
                    case JsSuccess(patientRefererTo, _) =>
                      vPatientRefererTo =s"$patientRefererTo"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "referredDate").validate[String] match {
                    case JsSuccess(referredDate, _) =>
                      vReferredDate =s"$referredDate"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                   (jsonObject \ "country").validate[String] match {
                    case JsSuccess(country, _) =>
                      vCountry =s"$country"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                var patientTypeValue = ""
                if(jsonObject.toString contains "patientType"){
                  for( a <- 1 to 20){
                      (jsonObject \ ("patientType"+a)).validate[String] match {
                        case JsSuccess(patientType, _) =>
                        var condition =s"$patientType"

                         var temp = condition.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         val patientTypeChecked = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])

                         if(patientTypeChecked){

                          patientTypeValue = findPatientType("patientType"+a)
                           vPatientType.append(patientTypeValue)
                           vPatientType.append(",")
                         }

                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }
            }
            if (step.formId == "1L"){
                val jsonObject1 = Json.parse(step.value.toString())
                (jsonObject1 \ "birth_date").validate[String] match {
                    case JsSuccess(birth_date, _) =>
                      vBirthDate =s"$birth_date"
                    case JsError(_) =>  Future.successful(BadRequest)
                  } 
            }  
            
      })

      (vpatientId, vPhoneNumber, vEmailAddress, steps,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType.toString.dropRight(1),vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCountry)
    }
    
    val now = DateTime.now
    
    var firstVisitdate  = DateTime.now
    var birthDate  = DateTime.now
    
    treatment match {
      case JsSuccess((vpatientId, vPhoneNumber, vEmailAddress, steps,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType,vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCountry), _) =>
          var patientExists=false
          var patientExist = false
          var patientId = vpatientId
          var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

           var pb = db.run{
              PracticeTable.filter(_.id === practiceId).result.head
            } map { practice =>
              if(convertOptionString(practice.chartNumberType).toLowerCase == "alphanumeric"){
                  var idExists = true
                  var df: DecimalFormat = new DecimalFormat("0000")
                  var i = 0

                  while(idExists){
                    i = i+1
                    patientId = vLastName.take(2).toUpperCase + df.format(i)
                     var f =  db.run{
                     for {
                        count <- TreatmentTable.filter(_.practiceId === practiceId).filter(_.patientId === patientId).length.result
                      } yield {
                        if(count == 0){
                          idExists = false
                        }
                      }
                    }
                    Await.ready(f, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }

              } else if(convertOptionString(practice.chartNumberType).toLowerCase == "numeric") {
                    var df: DecimalFormat = new DecimalFormat("000000")
                    var chartNumberSeq = (convertOptionLong(practice.chartNumberSeq))
                    patientId = df.format(chartNumberSeq + 1)

                      db.run {
                          PracticeTable.filter(_.id === practiceId).map(_.chartNumberSeq).update(Some(patientId.toLong))
                      } map {
                          case 0 => NotFound
                          case _ => Ok
                        }
                } else {
                val f = db.run{
                      for {
                        count <- TreatmentTable.filter(_.patientId === patientId).join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === practiceId) .length.result
                      } yield {
                        if (count > 0)
                          patientExists = true
                      }
                }
                val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))
              }

          }
          Await.ready(pb, atMost = scala.concurrent.duration.Duration(30, SECONDS))

          var patientBlock =  db.run{
                     for {
                        count <- TreatmentTable.filter(_.firstName === vFirstName).filter(_.lastName === vLastName).filter(_.phoneNumber === vPhoneNumber).length.result
                      } yield {
                        if(count > 0){
                          patientExist = true
                        }
                      }
                    }
          Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              var errorFound = false
              var phoneValidation = false
              var businessPhoneValidation = false
              var zipCodeValidation = false

                  (steps).foreach(step=>{
                    var countryList = List("united states of america","usa","united states","u.s","us","america")
                    var country = convertOptionString((step.value \ ("country")).asOpt[String])

                    if(countryList contains country.trim.toLowerCase){
                      var state = convertOptionString((step.value \ ("state")).asOpt[String]).toLowerCase

                      var stateBlock = db.run{
                        StateTable.filter(s=> s.state.toLowerCase === state.trim || s.stateName.toLowerCase === state.trim).result
                      } map { stateMap =>
                          if(stateMap.length == 0){
                            errorFound = true
                          }
                      }
                      var AwaitResult1 = Await.ready(stateBlock, atMost = scala.concurrent.duration.Duration(90, SECONDS))

                      var phone = vPhoneNumber.replaceAll("[^a-zA-Z0-9]", "")
                      if(phone.length != 10 || phone.startsWith("0") || phone.startsWith("1") || phone.startsWith("1234")){
                          phoneValidation = true
                        }

                      if(vBusinessPhone != "" && vBusinessPhone != None){
                      var bPhone = vBusinessPhone.replaceAll("[^a-zA-Z0-9]", "")
                        if(bPhone.length != 10 || bPhone.startsWith("0") || bPhone.startsWith("1") || bPhone.startsWith("123")){
                            businessPhoneValidation = true
                          }
                        }

                      if(vZipCode.forall(_.isDigit) && vZipCode.length == 5 || vZipCode.length == 9){
                        zipCodeValidation = false
                      } else {
                        zipCodeValidation = true
                      }
                    }
                   })

          if (patientId!="" && patientExists)
                  Future.successful(BadRequest(Json.parse("""{"error":"Patient Id already exists"}""")))
          else if(patientExist)
                  Future.successful(BadRequest(Json.parse("""{"error":"Patient already exists"}""")))
          else if (vInitialVisitDate == "" || !isValidDate(vInitialVisitDate,"MM/dd/yyyy"))
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid initial visit date"}""")))
          else if (vBirthDate == "" || !isValidDate(vBirthDate,"MM/dd/yyyy"))
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid birth date"}""")))
          else if(errorFound)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid state name!"}""")))
          else if(DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vBirthDate) > DateTime.now())
                  Future.successful(BadRequest(Json.parse("""{"error":"Birth date should not greater than today date!"}""")))
          else if(phoneValidation)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
          else if(zipCodeValidation)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid zipcode!"}""")))
          else if(businessPhoneValidation)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid business phone number!"}""")))
          else if(vPatientType == "" || vPatientType == null)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please select patient type!"}""")))
          else if(vPatientRefererTo != "" && vPatientRefererTo != None  && (vReferredDate == null || vReferredDate == ""))
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter Referred Date!"}""")))
          else if(vReferredDate != "" && vReferredDate != null  && (vPatientRefererTo == None || vPatientRefererTo == ""))
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter Referred To!"}""")))
          else if(vCarrierName == "" || vCarrierName == None)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please select carrier name!"}""")))
          else {
            var firstVisitDate = now
               
            if (vInitialVisitDate!=""){
                firstVisitDate = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vInitialVisitDate)
            } 

            var referredDate: Some[DateTime] = null

            if (vReferredDate!=""){
                referredDate = Some(DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vReferredDate))
            } 
            
            var referredTopatient: Some[Long] = null
            if(vPatientRefererTo != ""){
              referredTopatient = Some(vPatientRefererTo.toLong)
            }

            var referralSourceData = ""

            var block1 = db.run{
              ReferralSourceTable.filter(_.id === vPatientReferer.toLong).result
               } map { referralList =>
                referralList.map{ result => referralSourceData = result.value}
              }
              var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

            val practiceId = request.identity.asInstanceOf[StaffRow].practiceId

            var stepValue: JsValue = null

            db.run {
                (TreatmentTable.returning(TreatmentTable.map(_.id)) += TreatmentRow(None, patientId, Some(firstVisitDate), Some(now), vPhoneNumber, vEmailAddress, request.identity.id.get, Some(""), Some(""), Some(""),Some(""), Some(""),None,Some(referralSourceData),true,Some(""),Some(vPatientType.toString),Some(vPatientStatus),Some(false),Some(""),Some(""),Some(null),Option.empty[Long],Some(vPatientReferer.toLong),vCarrierName,Some(""),Some(""),Some(""),Option.empty[Long],Some(""),null,Some(false),null,Some(""), Some(""),Some(vFirstName),Some(vLastName),Some(vBirthDate),Some(vProviderId.toLong),Some(practiceId),Some(false),Some(false),Some(""),Some(""),referredTopatient,referredDate,Some(""),Some(false),Some(false),Some(false),Some(""),None,Some(false),Some(""),None,Some("No"),Some(""),Some(vCountry),None)).flatMap { id =>
                  (steps).foreach(step=>{
                      if (step.formId == "1A"){
                        checkAndInsertMeasurementHistory(id,"1A",step)
                      }

                   (steps).foreach(step=>{
                     stepValue = step.value
                    if(!(stepValue.toString contains "patient_id")){
                    var newObj =  Json.obj() + ("patient_id" -> JsString(patientId))
                    stepValue = stepValue.as[JsObject].deepMerge(newObj.as[JsObject])
                    }
                   })
                   })

                   DBIO.sequence(steps.map(step => StepTable += StepRow(stepValue, step.state, id, step.formId, step.formVersion, now)
                   )).flatMap { _ =>
                    TreatmentTable.filter(_.id === id).result.head
                  }
                }.transactionally
                
              } map { treatmentRow =>
                    (steps).foreach(step=>{
                      var formId = ""
                      if (step.formId == "1A"){
                          formId = "1L"
                      }else if (step.formId == "1L"){  
                          formId = "1A"
                      }
                      if (formId!=""){  
                        db.run{  
                          StepTable += StepRow(step.value, step.state, convertOptionLong(treatmentRow.id), formId, step.formVersion, now)
                        }
                      }   

                      if (step.formId == "1A" || step.formId == "1L"){
                        val jsonObject = Json.parse(step.value.toString)
                        var alerts = ""
                        var isMedicalCondition = true
                if(jsonObject.toString contains "medicalCondition"){

                  for( a <- 1 to 50){

                      (jsonObject \ ("medicalCondition"+a)).validate[String] match {
                        case JsSuccess(medicalCondition, _) =>
                        var condition =s"$medicalCondition"

                         var temp = condition.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         val medicalConditionChecked = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])

                          var medicalConditionValue = ""
                          var conditionType = ""

                          if(medicalConditionChecked == true){
                           if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }

                      if(medicalConditionValue != ""){
                        alerts = alerts + medicalConditionValue + ", "
                          var medicalConditionLogRow = MedicalConditionLogRow(None,step.formId,convertOptionLong(treatmentRow.id),medicalConditionValue,conditionType,LocalDate.now(),true)
                            db.run{
                              MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += medicalConditionLogRow
                              }map{
                                case 0L => PreconditionFailed
                                case id => Ok
                             }
                           }
                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                  }
                    //!UPDATING ISFAMILY AND ALERTS IN TREAMENT TABLE
                    db.run {
                    val query = TreatmentTable.filter(_.id === treatmentRow.id)
                    query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                      case true => DBIO.seq[Effect.Write](
                        Some(isMedicalCondition).map(value => query.map(_.isMedicalCondition).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                        Some(alerts.dropRight(2)).map(value => query.map(_.alerts).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                      ) map {_ => Ok}
                      case false => DBIO.successful(NotFound)
                      }
                    }
                  }

                  if(jsonObject.toString contains "allergy"){
                    for( a <- 1 to 8){

                      (jsonObject \ ("allergy"+a)).validate[String] match {
                          case JsSuccess(allergy, _) =>
                          var condition =s"$allergy"

                         var temp1 = condition.replace('★', '"')
                         val jsonObject2: JsValue = Json.parse(temp1)
                         val allergyValue = convertOptionalBoolean((jsonObject2 \ ("checked")).asOpt[Boolean])

                          var allergiesValue = ""
                          var conditionType = ""

                        if(allergyValue == true){
                        if(a == 7){
                          allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                          conditionType = "Other_Allergy"
                        }else{
                          allergiesValue = allergyMap("allergy"+a)
                          conditionType = "Allergy"
                          }

                            var allergyLogRow = MedicalConditionLogRow(None,step.formId,convertOptionLong(treatmentRow.id),
                            allergiesValue,conditionType,LocalDate.now(),true)

                          if(allergiesValue.trim != ""){
                          db.run{
                                MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += allergyLogRow
                              }map{
                               case 0L => PreconditionFailed
                               case id => Ok
                              }
                             }
                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

              // #---DoseSpot api called ---
              val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
              var errorFound = false
              var staffId = request.identity.id.get
              var message = ""

             var dosespotBlock = db.run{
                StaffTable.filter(_.id === staffId).join(PracticeTable).on(_.practiceId === _.id).result
              } map { results =>
                val data = results.groupBy(_._1.id).map {
                  case (staffId, entries) =>
                  val staff = entries.head._1
                  val practice = entries.head._2

              val clinicKey = convertOptionString(practice.doseSpotClinicKey)
              val clinicId = convertOptionString(practice.doseSpotClinicId)
              val userId = convertOptionString(staff.doseSpotUserId)
              var doseSpotPatientId = ""

              if(clinicKey != "" && clinicId != "" && userId != ""){

                val accessToken:String = getAccessToken(doseSpotUrl,clinicId,clinicKey,userId)

                var firstName = convertOptionString((step.value \ "full_name").asOpt[String])
                var lastName = convertOptionString((step.value \ "last_name").asOpt[String])
                var birthOfDate = convertOptionString((step.value \ "birth_date").asOpt[String])
                var strGender = convertOptionString((step.value \ "gender").asOpt[String])
                var address = convertOptionString((step.value \ "street").asOpt[String])
                var city = convertOptionString((step.value \ "city").asOpt[String])
                var state = convertOptionString((step.value \ "state").asOpt[String])
                var zip = convertOptionString((step.value \ "zip").asOpt[String])
                var phone = convertOptionString((step.value \ "phone").asOpt[String])
                var middleName = convertOptionString((step.value \ "mi").asOpt[String])


              var gender = ""
              if(strGender == "male"){
                gender = "1"
              } else if(strGender == "female"){
                gender = "2"
              } else if(strGender == "unknown"){
                gender = "3"
              }

                val json: String ="""{"FirstName": "rFirstName","LastName": "rLastName","DateOfBirth": "rDateOfBirth","Gender": rGender,"Address1":"rAddress","City":"rCity","State":"rState","ZipCode":"rZip","PrimaryPhone":"rPrimaryPhone","MiddleName":"rMiddleName","PrimaryPhoneType":2,"Active":true}""".stripMargin

                var patientJson = json.replace("rFirstName", firstName).replace("rLastName", lastName).replace("rDateOfBirth", birthOfDate).replace("rGender", gender).replace("rAddress", address).replace("rCity", city).replace("rState", state).replace("rZip", zip).replace("rPrimaryPhone", phone).replace("rMiddleName",middleName)

                val url = s"${doseSpotUrl}webapi/api/patients"
                val result = Http(url)
                .postData(patientJson)
                .header("Content-Type", "application/json")
                .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
                .header("Charset", "UTF-8")
                .option(HttpOptions.readTimeout(10000)).asString

                var response = (Json.parse(result.body) \ "Result").asOpt[JsValue]
                var resultCode = convertOptionString(response.flatMap(s => (s \ "ResultCode").asOpt[String]))

                    if(resultCode == "OK"){
                      doseSpotPatientId = convertOptionLong((Json.parse(result.body) \ "Id").asOpt[Long]).toString
                      db.run {
                      TreatmentTable.filter(_.id === treatmentRow.id).map(_.doseSpotPatientId).update(Some(doseSpotPatientId))
                      }
                    }
                  }
                }
              }
              val AwaitResult = Await.ready(dosespotBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                 }
              })
                Created(Json.toJson(treatmentRow))
              }
           }   
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

def updatePerioChartDataFormat = silhouette.SecuredAction {
      db.run{
          for {
              perioChartRecords <- PerioChartTable.result
          } yield{
            (perioChartRecords).foreach(perioChartRecord=>{
              var jsString:String=""
              var PerioNumberArr:Array[PerioNumber]=new Array[PerioNumber](16)

              var i=0
              (perioChartRecord.perioNumbers).as[JsArray].value.map { item =>
                  PerioNumberArr(i)=new PerioNumber(convertOptionString(item.asOpt[String]),"","","")

                  i=i+1
               }

              val obj2Json: JsValue = Json.toJson(PerioNumberArr)

             db.run {
                 PerioChartTable.filter(_.id === perioChartRecord.id).map(_.perioNumbers).update(obj2Json)
             } map {
               case _ =>
             }
            })
          }
      }


      Ok {
            Json.obj(
              "status"->"success",
              "message"->"updated successfully"
            )
      }

  }

def mergeAll1B1C = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
          treatment <- TreatmentTable.result
      } yield {
            (treatment).foreach( treatments=>
            {
               merge1B1C(convertOptionLong(treatments.id))
            }
            )
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Form Migrated successfully"
            )
          }
        }
     }
  }


  def migrateTreatmentCountry() = silhouette.SecuredAction.async { request =>
      var block = db.run {
        TreatmentTable.filterNot(_.deleted).result
        } map { treatmentResult =>
          println("Size = " + treatmentResult.size)
          treatmentResult.foreach(treatment => {
          var block = db.run{
                StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
                  } map { stepResult =>
                  var country =  (stepResult.value \ "country").as[String]
                   var block1 = db.run {
                    TreatmentTable.filter(_.id === treatment.id).map(_.country).update(Some(country))
                    } map {
                        case 0L => PreconditionFailed
                        case id => Ok
                      }
                   val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                  }
        })
        }
        val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        Future (Ok{Json.obj( "status"->"success","message"->"Country Migrated successfully")})
      }


   def merge1B1C(id: Long){

        var bValue : JsValue = null
        var dateCreatedbData: DateTime = null
        var cValue : JsValue = null
        var dataValue : JsValue = null
        var stateData: StepRow.State = null
        var formIdData : String = ""
        var treatmentIdData : Long = 0
        var formVersionData : Int = 0
        var dateCreatedData: DateTime = null


    db.run {
      StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps = stepRows.filter(_.formId == "1H").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep =  steps.map { step =>

                 bValue = step.value;

                 dateCreatedbData = step.dateCreated;

            }
            val steps1 = stepRows.filter(_.formId == "1I").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep1 =  steps1.map { step =>

                 cValue = step.value;

                 stateData = step.state;

                 formIdData = "1H";

                 treatmentIdData = step.treatmentId

                 formVersionData = step.formVersion;

                 dateCreatedData = step.dateCreated;

            }

            if(bValue == null ){
             dataValue = Json.toJson(cValue)
            }
            else if(cValue == null ){
              dataValue = Json.toJson(bValue)
            }
            else{
              dataValue = bValue.as[JsObject].deepMerge(cValue.as[JsObject])
            }
             db.run {
                 StepTable.filter(_.treatmentId === id).filter(_.formId==="1H").filter(_.dateCreated  === dateCreatedbData).map(_.value).update(dataValue)
             } map {
                case 0 =>
                    db.run {
                        StepTable.returning(StepTable.map(_.treatmentId)) += StepRow(dataValue,stateData,treatmentIdData,formIdData,formVersionData,dateCreatedData)
                    }
                case _ =>
               }

           Ok {
            Json.obj(
              "status"->"Success",
              "message"->"Data Migrated Successfully"
            )
          }
        }
      }
    }

    def mergeAll3B3C = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
          treatment <- TreatmentTable.filterNot(_.deleted).result
      } yield {
            (treatment).foreach( treatments=>
            {
              merge3B3C(convertOptionLong(treatments.id))
            }
            )
         Ok {
             Json.obj(
               "status"->"success",
               "message"->"Form Migrated successfully"
             )
           }
        }
     }
  }

  def merge3B3C(id: Long){

        var aValue : JsValue = null
        var dateCreatedbData: DateTime = null
        var bValue : JsValue = null
        var cValue : JsValue = null
        var dataValue : JsValue = null
        var stateData: StepRow.State = StepRow.State.InProgress
        var formIdData : String = ""
        var treatmentIdData : Long = 0
        var formVersionData : Int = 0
        var dateCreatedData: DateTime = null

  val block = db.run {
      StepTable.filter(_.treatmentId === id).result.map { stepRows => 
            val steps1 = stepRows.filter(_.formId == "3A").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep1 =  steps1.map { step =>

                 aValue = step.value;
                 
                 if(step.state != null){
                   stateData = step.state
                 }
                 dateCreatedbData = step.dateCreated;
            }

            val steps2 = stepRows.filter(_.formId == "3C").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep2 =  steps2.map { step =>
                 bValue = step.value;
            }

            val steps3 = stepRows.filter(_.formId == "3D").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep3 =  steps3.map { step =>
                 cValue = step.value;
            }

            if(aValue != null){
              dataValue = aValue
            } 

            if(bValue != null){
              if(dataValue != null){
                dataValue = dataValue.as[JsObject].deepMerge(bValue.as[JsObject])
              }else{
                dataValue = bValue
              }
             
            }
            if(cValue != null){
              if(dataValue != null){
              dataValue = dataValue.as[JsObject].deepMerge(cValue.as[JsObject])
              }else{
                dataValue = cValue
              }
            }

             db.run {
                    StepTable.returning(StepTable.map(_.treatmentId)) += StepRow(dataValue,stateData,id,"3A",1,DateTime.now())
                  }
            Ok {
            Json.obj(
              "status"->"Success",
              "message"->"Data Migrated Successfully"
            )
          }
        }
      }
  }

  def mergeAll1G3F1X = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
        treatment <- TreatmentTable.filterNot(_.deleted).result
      } yield {
        (treatment).foreach( treatments => {
          merge1G3F1X(convertOptionLong(treatments.id))
        })
        Ok {
          Json.obj(
            "status"->"success",
            "message"->"Form Migrated successfully"
          )
        }
      }
    }
  }

  def merge1G3F1X(id: Long){

        var aValue : JsValue = null
        var dateCreatedbData: DateTime = null
        var bValue : JsValue = null
        var cValue : JsValue = null
        var dataValue : JsValue = null
        var stateData: StepRow.State = null
        var formIdData : String = ""
        var treatmentIdData : Long = 0
        var formVersionData : Int = 0
        var dateCreatedData: DateTime = null

  val block = db.run {
      StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps1 = stepRows.filter(_.formId == "1G").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep1 =  steps1.map { step =>

                 aValue = step.value;

                 if(step.state != null){
                   stateData = step.state
                 }
                 dateCreatedbData = step.dateCreated;
            }

            val steps2 = stepRows.filter(_.formId == "3F").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep2 =  steps2.map { step =>
                 bValue = step.value;
            }

            val steps3 = stepRows.filter(_.formId == "1X").groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep3 =  steps3.map { step =>
                 cValue = step.value;
            }

            if(aValue != null){
              dataValue = aValue
            }

            if(bValue != null){
              if(dataValue != null){
                dataValue = dataValue.as[JsObject].deepMerge(bValue.as[JsObject])
              }else{
                dataValue = bValue
              }

            }
            if(cValue != null){
              if(dataValue != null){
              dataValue = dataValue.as[JsObject].deepMerge(cValue.as[JsObject])
              }else{
                dataValue = cValue
              }
            }

             db.run {
                    StepTable.returning(StepTable.map(_.treatmentId)) += StepRow(dataValue,stateData,id,"1AL",1,DateTime.now())
                  }
            Ok {
            Json.obj(
              "status"->"Success",
              "message"->"Data Migrated Successfully"
            )
          }
        }
      }
  }

    def mergeForms(sourceForm : String, targetForm : String) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
          treatment <- TreatmentTable.filterNot(_.deleted).result
      } yield {
            (treatment).foreach( treatments=>
            {
              mergeAllForms(convertOptionLong(treatments.id),sourceForm,targetForm)
            }
            )
         Ok {
             Json.obj(
               "status"->"success",
               "message"->"Form Migrated successfully"
             )
           }
        }
     }
  }

def mergeAllForms(id : Long, sourceForm : String, targetForm : String){

    var aValue : JsValue = null
    var dateCreatedbData: DateTime = null
    var bValue : JsValue = null
    var cValue : JsValue = null
    var dataValue : JsValue = null
    var stateData: StepRow.State = StepRow.State.InProgress
    var formIdData : String = ""
    var treatmentIdData : Long = 0
    var formVersionData : Int = 0
    var dateCreatedData: DateTime = null

  val block = db.run {
      StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps1 = stepRows.filter(_.formId == sourceForm).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep1 =  steps1.map { step =>

                 aValue = step.value;

                 if(step.state != null){
                   stateData = step.state
                 }
                 dateCreatedbData = step.dateCreated;
            }

            val steps2 = stepRows.filter(_.formId == targetForm).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList
            val dataStep2 =  steps2.map { step =>
                 bValue = step.value;

            }

            if(aValue != null){
              dataValue = aValue
            }

            if(bValue != null){
              if(dataValue != null){
                dataValue = dataValue.as[JsObject].deepMerge(bValue.as[JsObject])
              }else{
                dataValue = bValue
              }
            }

             db.run {
                    StepTable.returning(StepTable.map(_.treatmentId)) += StepRow(dataValue,stateData,id,targetForm,1,DateTime.now())
                  }
            Ok {
            Json.obj(
              "status"->"Success",
              "message"->"Data Migrated Successfully"
            )
          }
        }
      }
}

  def read(id: Long) = silhouette.SecuredAction(SupersOnly).async { implicit request =>  
    var strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
    db.run {
      TreatmentTable.filter(_.id === id).filterNot(_.deleted).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case None =>
          DBIO.successful(NotFound)
        case Some(treatment) =>
          StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps = stepRows.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
      var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
      val patientName = firstName+" "+lastName

    var providerName = ""
    var providerId = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "provider").asOpt[String]))
    if(providerId != ""){
      var block1 = db.run{
            ProviderTable.filter(_.id === providerId.toLong).result
        } map { results =>
        results.map{ providers => providerName = providers.title +" "+ providers.firstName +" "+providers.lastName }
        Ok
        }
        val AwaitResult4 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    }

        var accessTo = ""
        var accessLevel = ""
        var block = db.run{
            SharedPatientTable.filter(_.shareTo === request.identity.id.get).filter(_.patientId === id).result
          } map {treatmentResult =>
              treatmentResult.map{ result =>
                accessTo = result.accessTo
                accessLevel = result.accessLevel
              }
          }
          val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

      var isFamily = false
      var familyBlock = db.run{
          FamilyMembersTable.filter(_.patientId === treatment.id).length.result
      } map { familyResult =>
      if(familyResult > 0){
        isFamily = true
        }
      }
      val AwaitResult3 = Await.ready(familyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))


            val data = TreatmentList(
              treatment.id.get,
              treatment.patientId,
              Some(patientName),
              firstName,
              lastName,
              treatment.firstVisit,
              treatment.lastEntry,
              treatment.phoneNumber,
              treatment.emailAddress,
              providerName,
              steps.map(step => Step(step.value, step.state, step.formId, step.formVersion, Some(step.dateCreated))),
              treatment.ctScanFile,
              treatment.contracts,
              treatment.xray,
              treatment.scratchPad,
              treatment.scratchPadText,
              treatment.toothChartUpdatedDate,
              treatment.patientType,
              treatment.patientStatus,
              treatment.referralSource,
              treatment.treatmentStarted,
              treatment.recallPeriod,
              steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]),
              steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "gender").asOpt[String]),
              false,
              "",
              None,
              "",
              "",
              accessTo,
              accessLevel,
              treatment.recallType,
              treatment.recallCode,
              treatment.recallCodeDescription,
              treatment.intervalNumber,
              treatment.intervalUnit,
              treatment.recallFromDate,
              treatment.recallOneDayPlus,
              treatment.recallDueDate,
              treatment.recallNote,
              Some(isFamily),
              treatment.examImaging
            )
            Ok(Json.toJson(data))
          }
      }
    }
  }
  

def getTreatmentAdditionalData(id: Long) = silhouette.SecuredAction(SupersOnly).async { implicit request =>
    var shareIcon = false
    var bp_pulse_o2 = "normal"

    val f = db.run{
          for {
            measurementHistoryCount <- MeasurementHistoryTable.filter(_.patientId === id).length.result
            step <- StepTable.filter(_.treatmentId === id).sortBy(_.dateCreated.desc).result
            sharePatient <- SharedPatientTable.filter(_.patientId === id).length.result

          } yield {

            var vShareToAdmin = false
            val block = db.run{
              TreatmentTable.filter(_.id === id).result
            } map { result=>
            result.foreach(s => {
              vShareToAdmin = convertOptionalBoolean(s.shareToAdmin)
            })
            Ok
            }
            var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))

            if (sharePatient > 0 || vShareToAdmin){
              shareIcon = true
            }

            if (measurementHistoryCount > 0){
                var f1=db.run { 
                     for {
                          measurementHistory <- MeasurementHistoryTable.filter(_.patientId === id).sortBy(_.dateCreated.desc).result.head
                     } yield {
                           val systolic = convertOptionDouble(measurementHistory.systolic)
                           val diastolic = convertOptionDouble(measurementHistory.diastolic)
                           val pulse = convertOptionDouble(measurementHistory.pulse)
                           val oxygen = convertOptionDouble(measurementHistory.oxygen)
                           val bodyTemp = convertOptionDouble(measurementHistory.bodyTemp)
                           val glucose = convertOptionDouble(measurementHistory.glucose)
                           val a1c = convertOptionDouble(measurementHistory.a1c)
                           val inr = convertOptionDouble(measurementHistory.inr)
                           val pt = convertOptionDouble(measurementHistory.pt)
                           
                           
                           if (systolic > 129 || systolic < 90 || diastolic > 79 || diastolic < 60 ||  pulse>85 || pulse <44 || oxygen < 95 || bodyTemp > 98.6 || glucose > 195 || a1c>7 || inr <2 || inr >4 || pt < 10 || pt > 14){
                                bp_pulse_o2 = "out of range"
                           }
                     }
                }
                var AwaitResult1 = Await.ready(f1, atMost = scala.concurrent.duration.Duration(3, SECONDS))       
            }    
         }
    }   
    
    val AwaitResult2 = Await.ready(f, atMost = scala.concurrent.duration.Duration(60, SECONDS))

    db.run {
      TreatmentTable.filter(_.id === id).filterNot(_.deleted).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case None =>
          DBIO.successful(NotFound)
        case Some(treatment) =>
          StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps = stepRows.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)
            val data = TreatmentSingle(
              treatment.id.get,
              treatment.patientId,
              steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]),
              treatment.firstVisit,
              treatment.lastEntry,
              treatment.phoneNumber,
              treatment.emailAddress,
              steps.map(step => Step(step.value, step.state, step.formId, step.formVersion, Some(step.dateCreated))),
              treatment.ctScanFile,
              treatment.contracts,
              treatment.xray,
              treatment.scratchPad,
              treatment.scratchPadText,
              treatment.toothChartUpdatedDate,
              bp_pulse_o2,
              convertOptionString(treatment.recallPeriod),
              shareIcon
            )
            Ok(Json.toJson(data))
          }
      }
    }
  }
  
    private def getCheckList(steps:Option[List[Step]]):Boolean = {
      (steps).foreach(steps1 => {
              (steps1).foreach(step=>{
                if(step.formId=="1N" || step.formId=="2D" || step.formId=="3B" || step.formId=="3E"
                  || step.formId=="1O"){
                  val jsonObject = Json.parse(step.value.toString())
                  if(step.value.toString().contains("★checked★:true")){
                        return true;
                  }else{
                        //Console.println("checked false formId = "+step.formId);
                  }
                }
            })
      })

    return false;
  }
  def getMeasurementHisotry(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      MeasurementHistoryTable
        .filter(_.patientId=== id)
        .sortBy(_.dateCreated.desc)
        .result
    } map { rows =>
      Ok(Json.toJson(rows))
    }
  }
  
  def getToothChartData(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      ToothChartTable.filter(_.patientId===id)
        .sortBy(_.toothId.desc)
        .result
    } map { rows =>
      Ok(Json.toJson(rows))
    }
  }
  
  def getPerioChartData(id: Long, date: Option[String]) = silhouette.SecuredAction.async { request =>
  var vDate = convertOptionString(date)
  var query = PerioChartTable.filter(_.patientId===id)
  if(vDate != ""){
    query = query.filter(_.dateSelected === LocalDate.parse(vDate))
  }
    db.run{
      query
        .sortBy(_.perioType)
        .sortBy(_.dateSelected)
        .sortBy(_.rowPosition.desc)
        .result
    } map { rows =>
      Ok(Json.toJson(rows))
    }
  }
  
  case class PerioChartDateEntryRow(
    dateSelected: LocalDate
  )

  implicit val perioChartDateEntryFormat = Json.format[PerioChartDateEntryRow]

  def getPerioChartEntryDates(patientId: Long) = silhouette.SecuredAction.async { request =>
  var perioChartList = List.empty[PerioChartDateEntryRow]
  var dm  = List[String]()
    db.run{
      PerioChartTable.filter(_.patientId === patientId).sortBy(_.dateSelected.desc).result
    } map { rows =>
        rows.foreach(result => {
          if(!(dm contains result.dateSelected.toString)){
            dm = result.dateSelected.toString :: dm
          var dataList = PerioChartDateEntryRow(result.dateSelected)
          perioChartList = perioChartList :+ dataList
          }
        })
      Ok(Json.toJson(perioChartList))
    }
  }

  def updateToothChartData(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    var toothId:Int = 0
    var toothConditions:String = ""
    val toothChart = for {
      toothIdVal <- (request.body \ "toothId").validate[Int]
      toothConditionsVal <- (request.body \ "toothConditions").validate[String]
    } yield {
      toothId = toothIdVal
      toothConditions = toothConditionsVal
    }
    
    var userId:Long=0 
    var staffId:Long =0 
    
    request.identity match{
        case _:StaffRow=> staffId=request.identity.id.get
        case _:UserRow=> userId=request.identity.id.get
    }  

    val now = DateTime.now

    var toothChartEntryExists=false
                
          
    val f = db.run{
          for {
            count <- ToothChartTable.filter(_.patientId===id).filter(_.toothId===toothId).length.result
          } yield {
            if (count>0)
              toothChartEntryExists=true
          }
    }  
    
    
    val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))
    
    db.run{
     var query = TreatmentTable.filter(_.id === id )
      DBIO.seq[Effect.Read with Effect.Write](
        query.map(_.toothChartUpdatedDate).update(Some(now)).map(_ => Unit)
      )
    } 
    if (toothChartEntryExists){
     db.run{
        var query = ToothChartTable.filter(_.patientId === id).filter(_.toothId === toothId )
        DBIO.seq[Effect.Read with Effect.Write](
          query.map(_.dateUpdated).update(now).map(_ => Unit),
          query.map(_.toothConditions).update(toothConditions).map(_ => Unit),
          query.map(_.staffId).update(staffId).map(_ => Unit),
          query.map(_.userId).update(userId).map(_ => Unit)
       ) map { _ =>
            Ok
         }
      }    
    }else{  
        db.run{
          ToothChartTable.returning(ToothChartTable.map(_.id)) += ToothChartRow(None,id,toothId,toothConditions,staffId,userId,now,Some(""))
        }map{  
          result => Ok      
        }  
     }   
  } 
  
  def migrateToothConditions = silhouette.SecuredAction.async(parse.json) { request =>

    db.run {
      for{
          toothCondition <- ToothChartTable.result
      } yield {
            (toothCondition).foreach(toothChart => {
              var chartValue =""
              if(toothChart.toothConditions != null && toothChart.toothConditions != ""){
              toothChart.toothConditions.split(",").toList.foreach(s=> {
                chartValue = chartValue + s + "#E:F#TS:0,"
                })

              db.run{
                ToothChartTable.filter(_.id === toothChart.id).map(_.toothConditions).update(chartValue.dropRight(1))
              }
              }
            })
        Ok {Json.obj("status"->"success","message"->"ToothConditions Migrated Successfully")}
        }
     }
  }

  def updatePerioChartData(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    var userId:Long=0 
    var staffId:Long =0 

    request.identity match{
        case _:StaffRow=> staffId=request.identity.id.get
        case _:UserRow=> userId=request.identity.id.get
    }  

    val perioChartInput = for {
      rowId <- (request.body \ "id").validateOpt[Long]
      rowPosition <- (request.body \ "rowPosition").validate[String]
      perioType <- (request.body \ "perioType").validate[String]
      dateSelectedVal <- (request.body \ "dateSelected").validate[String]
      perioNumbers <- (request.body \ "perioNumbers").validate[JsValue]
      providerId <- (request.body \ "providerId").validateOpt[Long]

    } yield {
      // val dateSelected=org.joda.time.DateTime.parse(dateSelectedVal)
      val dateSelected = LocalDate.parse(dateSelectedVal)
      (rowId,rowPosition,perioType,dateSelected,perioNumbers,providerId)
    }
    
    
    
    perioChartInput match {
      case JsSuccess((rowId,rowPosition,perioType,dateSelected,perioNumbers,providerId), _) =>

          if (dateSelected == None){
            Future.successful(BadRequest(Json.parse("""{"error":"Invalid date"}""")))
          }else{
            if (rowId == None || rowId == Some(0)){
              db.run{
                PerioChartTable.returning(PerioChartTable.map(_.id)) += PerioChartRow(None,patientId,rowPosition,perioType,dateSelected,perioNumbers,staffId,userId,providerId)
              }map{
                  result => Ok
              }
            }else{  
             db.run{
                var query = PerioChartTable.filter(_.id === rowId )
                DBIO.seq[Effect.Read with Effect.Write](
                  query.map(_.rowPosition).update(rowPosition).map(_ => Unit),
                  query.map(_.perioType).update(perioType).map(_ => Unit),
                  query.map(_.dateSelected).update(dateSelected).map(_ => Unit),
                  query.map(_.perioNumbers).update(perioNumbers).map(_ => Unit),                  
                  query.map(_.staffId).update(staffId).map(_ => Unit),
                  query.map(_.userId).update(userId).map(_ => Unit),
                  query.map(_.providerId).update(providerId).map(_ => Unit)
                )map { _ =>
                    Ok
                  }
              }     
            }  
          }  
      case JsError(_) =>
        Future.successful(BadRequest)
    }    
  }  

def migrateMedicalCondtionSelected = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
          treatment <- TreatmentTable.result
      } yield {
            (treatment).foreach( treatments=>
            {
            var dateCreated: LocalDate = null

        db.run {
          StepTable.filter(_.treatmentId === treatments.id).result.map { stepRows =>
            val steps = stepRows.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)
            steps.foreach(step => {
              if (step.formId == "1A"){
                val jsonObject = Json.parse(step.value.toString)

                for( a <- 1 to 50){
                  (jsonObject \ ("medicalCondition"+a)).validate[String] match {
                    case JsSuccess(medicalCondition, _) =>
                      var condition =s"$medicalCondition"
                      var temp = condition.replace('★', '"')
                      val jsonObject1: JsValue = Json.parse(temp)
                      val medicalConditionCheck = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])
                      var medicalConditionValue = ""
                      var conditionType = ""

                      if(medicalConditionCheck == true){

                            if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }

                          if(medicalConditionValue != ""){
                            var block1 = db.run{
                               MedicalConditionLogTable.filter(_.patientId === treatments.id).filter(_.conditionType === conditionType).filter(_.value === medicalConditionValue).map(_.currentlySelected).update(true)
                              }map{
                                case 0L => PreconditionFailed
                                case id => Ok
                             }
                             val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(300, SECONDS))
                           }

                      }
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                }
                for( a <- 1 to 8){

                  (jsonObject \ ("allergy"+a)).validate[String] match {
                    case JsSuccess(allergy, _) =>
                      var condition =s"$allergy"

                      var temp1 = condition.replace('★', '"')
                      val jsonObject2: JsValue = Json.parse(temp1)

                      val allergyValue = convertOptionalBoolean((jsonObject2 \ ("checked")).asOpt[Boolean])
                      var allergiesValue = ""
                      var conditionType = ""
                      if(allergyValue == true){

                        if(a == 7){
                            allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                            conditionType = "Other_Allergy"
                            }else{
                            allergiesValue = allergyMap("allergy"+a)
                            conditionType = "Allergy"
                            }

                            var block2 = db.run{
                                MedicalConditionLogTable.filter(_.patientId === treatments.id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).map(_.currentlySelected).update(true)
                              }map{
                                case 0L => PreconditionFailed
                                case id => Ok
                              }

                        val AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(300, SECONDS))
                      }
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                }
              }
            })
          }
         }
        } )
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Form Migrated successfully"
            )
          }
        }
     }
  }


  def migrateAllMedicalCondtionLog = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>

    db.run {
      for{
          treatment <- TreatmentTable.result
      } yield {
            (treatment).foreach( treatments=>
            {
               migrateMedicalCondtionLog(convertOptionLong(treatments.id))
            }
            )
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Form Migrated successfully"
            )
          }
        }
     }
  }

    def migrateMedicalCondtionLog(id: Long){

    var dateCreated: LocalDate = null

      db.run {
          StepTable.filter(_.treatmentId === id).result.map { stepRows =>
            val steps = stepRows.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)
            steps.foreach(step => {
              if (step.formId == "1A"){
                val jsonObject = Json.parse(step.value.toString)
                var first_visit = convertOptionString((jsonObject \ ("first_visit")).asOpt[String])
                if(first_visit.toString.length == 0){
                   dateCreated = null
                } else {
                   dateCreated = LocalDate.parse(first_visit, DateTimeFormat.forPattern("MM/dd/yyyy"))
                }
                for( a <- 1 to 50){
                  (jsonObject \ ("medicalCondition"+a)).validate[String] match {
                    case JsSuccess(medicalCondition, _) =>
                      var condition =s"$medicalCondition"
                      var temp = condition.replace('★', '"')
                      val jsonObject1: JsValue = Json.parse(temp)
                      val medicalConditionValue = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])
                      if(medicalConditionValue == true){
                        var medicalConditionLogRow = MedicalConditionLogRow(None,step.formId,step.treatmentId,
                          medicalConditionMap("medicalCondition"+a),"Medical Condition",dateCreated,false)
                        var medicalConditionTrueData = db.run {
                          for{
                            medicalLogLength1 <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.value === medicalConditionMap("medicalCondition"+a)).length.result
                          } yield {
                            if(medicalLogLength1 <= 0){
                              db.run{
                                MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += medicalConditionLogRow
                              }map{
                                case 0L => PreconditionFailed
                                case id => Ok
                              }
                            }
                          }
                        }
                        val AwaitResult = Await.ready(medicalConditionTrueData, atMost = scala.concurrent.duration.Duration(300, SECONDS))
                      }else {
                        var medicalConditionFalseData = db.run {
                          for{
                            medicalLogLength2 <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.value === medicalConditionMap("medicalCondition"+a)).length.result
                          }yield {
                            if(medicalLogLength2 > 0){
                              db.run{
                                MedicalConditionLogTable.filter(_.value === medicalConditionMap("medicalCondition"+a)).delete
                              } map {
                                case 0 => NotFound
                                case _ => Ok
                              }
                            }
                          }
                        }
                        val AwaitResult1 = Await.ready(medicalConditionFalseData, atMost = scala.concurrent.duration.Duration(150, SECONDS))
                      }
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                }
                for( a <- 1 to 8){

                  (jsonObject \ ("allergy"+a)).validate[String] match {
                    case JsSuccess(allergy, _) =>
                      var condition =s"$allergy"

                      var temp1 = condition.replace('★', '"')
                      val jsonObject2: JsValue = Json.parse(temp1)

                      val allergyValue = convertOptionalBoolean((jsonObject2 \ ("checked")).asOpt[Boolean])
                      if(allergyValue == true){
                        var allergyLogRow = MedicalConditionLogRow(None,step.formId,step.treatmentId,
                          allergyMap("allergy"+a),"Allergy",dateCreated,false)
                        var allergyTrueData = db.run {
                          for{
                            allergyLogLength <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.value === allergyMap("allergy"+a)).length.result
                          }yield {
                            if(allergyLogLength <= 0){
                              db.run{
                                MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += allergyLogRow
                              }map{
                                case 0L => PreconditionFailed
                                case id => Ok
                              }
                            }
                          }
                        }
                        val AwaitResult2 = Await.ready(allergyTrueData, atMost = scala.concurrent.duration.Duration(300, SECONDS))
                      }else {
                        var allergyFalseData = db.run {
                          for{
                            allergyLogLength <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.value === allergyMap("allergy"+a)).length.result
                          }yield {
                            if(allergyLogLength > 0){
                              db.run{
                                MedicalConditionLogTable.filter(_.value === allergyMap("allergy"+a)).delete
                              } map {
                                case 0 => NotFound
                                case _ => Ok
                              }
                            }
                          }
                        }
                        val AwaitResult3 = Await.ready(allergyFalseData, atMost = scala.concurrent.duration.Duration(150, SECONDS))
                      }
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                }
              }
            })
          }
    }

  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
      var vpatientId:Option[String] = Some("")
      var vInitialVisitDate:String = ""
      var vBirthDate = ""
      var checkPatientId = false
      var vPatientReferer = ""
      var referralSourceData = ""
      var vPatientType : StringBuilder = new StringBuilder();
      var vPatientStatus = ""
      var vCarrierName = ""
      var vFirstName = ""
      var vLastName = ""
      var vDob = ""
      var vProviderId = ""
      var vPhoneNumber = ""
      var vEmailAddress = ""
      var vZipCode = ""
      var vBusinessPhone = ""
      var vPatientRefererTo = ""
      var vReferredDate = ""
      var vCallingFrom = ""
      var vCountry = ""
      var vOnlineRegStatus = ""

      var clientTimeZone:String=convertOptionString(request.headers.get("timezone"))

    val treatment = for {
      steps <- (request.body \ "steps").validateOpt[List[Step]]

    } yield {

      (steps).foreach(steps1 => {
        (steps1).foreach(step=>{
              if (step.formId == "1A"){
                  checkPatientId = true
                  val jsonObject = Json.parse(step.value.toString())
                  (jsonObject \ "patient_id").validate[String] match {
                    case JsSuccess(patient_id, _) =>
                      vpatientId =Some(s"$patient_id")
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "first_visit").validate[String] match {
                    case JsSuccess(first_visit, _) =>
                      vInitialVisitDate =s"$first_visit"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "birth_date").validate[String] match {
                    case JsSuccess(birth_date, _) =>
                      vBirthDate =s"$birth_date"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                 (jsonObject \ "patient_referer").validate[String] match {
                    case JsSuccess(patient_referer, _) =>
                      vPatientReferer =s"$patient_referer"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                  (jsonObject \ "patient_status").validate[String] match {
                    case JsSuccess(patient_status, _) =>
                      vPatientStatus =s"$patient_status"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "carrierName").validate[String] match {
                    case JsSuccess(carrierName, _) =>
                    vCarrierName =s"$carrierName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "full_name").validate[String] match {
                    case JsSuccess(fullName, _) =>
                      vFirstName =s"$fullName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "last_name").validate[String] match {
                    case JsSuccess(lastName, _) =>
                      vLastName =s"$lastName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "provider").validate[String] match {
                    case JsSuccess(providerId, _) =>
                      vProviderId =s"$providerId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "phone").validate[String] match {
                    case JsSuccess(phoneNumber, _) =>
                      vPhoneNumber =s"$phoneNumber"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "email").validate[String] match {
                    case JsSuccess(emailId, _) =>
                      vEmailAddress =s"$emailId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "zip").validate[String] match {
                    case JsSuccess(zipCode, _) =>
                      vZipCode =s"$zipCode"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "business_phone").validate[String] match {
                    case JsSuccess(businessPhone, _) =>
                      vBusinessPhone =s"$businessPhone"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "patientRefererTo").validate[String] match {
                    case JsSuccess(patientRefererTo, _) =>
                      vPatientRefererTo =s"$patientRefererTo"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "referredDate").validate[String] match {
                    case JsSuccess(referredDate, _) =>
                      vReferredDate =s"$referredDate"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "country").validate[String] match {
                    case JsSuccess(country, _) =>
                      vCountry =s"$country"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "onlineRegStatus").validate[String] match {
                    case JsSuccess(onlineRegStatus, _) =>
                      vOnlineRegStatus =s"$onlineRegStatus"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                if(jsonObject.toString contains "callingFrom"){
                  (jsonObject \ "callingFrom").validate[String] match {
                    case JsSuccess(callingFrom, _) =>
                      vCallingFrom =s"$callingFrom"
                    case JsError(_) =>  Future.successful(BadRequest)
                    }
                }

                var patientTypeValue = ""
                if(jsonObject.toString contains "patientType"){
                  for( a <- 1 to 20){
                      (jsonObject \ ("patientType"+a)).validate[String] match {
                        case JsSuccess(patientType, _) =>
                        var condition =s"$patientType"
                         var temp = condition.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         val patientTypeChecked = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])

                         if(patientTypeChecked){

                          patientTypeValue = findPatientType("patientType"+a)
                           vPatientType.append(patientTypeValue)
                           vPatientType.append(",")

                         }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

              }
               if (step.formId == "1L"){
                  val jsonObject1 = Json.parse(step.value.toString())
                  (jsonObject1 \ "birth_date").validate[String] match {
                    case JsSuccess(birth_date, _) =>
                      vBirthDate =s"$birth_date"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

              }
        })
      })
        
      val patientId:String = convertOptionString(vpatientId)
      
      (patientId, vPhoneNumber, vEmailAddress, steps, checkPatientId,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType.toString.dropRight(1),vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCallingFrom,vCountry,vOnlineRegStatus)
    }

    val now = DateTime.now
    var firstVisitdate  = DateTime.now
    
    
      treatment match {
        case JsSuccess((patientId, vPhoneNumber, vEmailAddress, stepsOpt,checkPatientId,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType,vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCallingFrom,vCountry,vOnlineRegStatus), _) =>

          if (patientId=="" && checkPatientId && vCallingFrom != "patientModule"){
              Future.successful(BadRequest(Json.parse("""{"error":"Patient Id is blank"}""")))
          }else{
                var errorFound = false
                var birthDateValidation = false
                var phoneValidation = false
                var businessPhoneValidation = false
                var zipCodeValidation = false
                var patientType = false
                var carrierNameValidation = false

                (stepsOpt).foreach(steps => {
                  (steps).foreach(step=>{
                    if(step.formId == "1A"){
                    var countryList = List("united states of america","usa","united states","u.s","us","america")
                    var country = convertOptionString((step.value \ ("country")).asOpt[String])

                    if(countryList contains country.trim.toLowerCase){
                      var state = convertOptionString((step.value \ ("state")).asOpt[String]).toLowerCase

                      var stateBlock = db.run{
                        StateTable.filter(s=> s.state.toLowerCase === state.trim || s.stateName.toLowerCase === state.trim).result
                          } map { stateMap =>
                              if(stateMap.length == 0){
                                errorFound = true
                              }
                      }
                      var AwaitResult1 = Await.ready(stateBlock, atMost = scala.concurrent.duration.Duration(90, SECONDS))

                      var phone = vPhoneNumber.replaceAll("[^a-zA-Z0-9]", "")
                      if(phone.length != 10 || phone.startsWith("0") || phone.startsWith("1") || phone.startsWith("123")){
                        phoneValidation = true
                      }


                      if(vBusinessPhone != "" && vBusinessPhone != None){
                        var bPhone = vBusinessPhone.replaceAll("[^a-zA-Z0-9]", "")
                        if(bPhone.length != 10 || bPhone.startsWith("0") || bPhone.startsWith("1") || bPhone.startsWith("123")){
                          businessPhoneValidation = true
                        }
                      }

                      if(!(vZipCode.forall(_.isDigit) && (vZipCode.length == 5 || vZipCode.length == 9))){
                        zipCodeValidation = true
                      }
                    }

                    if(vBirthDate != "" && vBirthDate != None){
                        var dob = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vBirthDate)
                        if(dob > DateTime.now()){
                        birthDateValidation = true
                      }
                    }

                    if(vPatientType == "" || vPatientType == null){
                      patientType = true
                    }

                    if(vCarrierName == "" || vCarrierName == None){
                      carrierNameValidation = true
                    }
                  }
            })
          })


            var patientExists=false
            var patientExist = false
            var count = 0
            
            var action = 
               for {
                  treatmentR <- TreatmentTable.filter(_.id===id) 
                  staff <- StaffTable.filter(_.id===treatmentR.staffId) 
                  staffsInPractice <- StaffTable.filter(_.practiceId===staff.practiceId)  
                  treatmentWithSamePatientId <- TreatmentTable.filter(_.id =!= id).filter(_.patientId===patientId) if treatmentWithSamePatientId.staffId === staffsInPractice.id
              } yield (treatmentR,staff,staffsInPractice,treatmentWithSamePatientId)
             val f = db.run { 
                 action.result }.map{result=> val data = result.groupBy(_._1.id).map { row =>
                      val (_, group) = row
                      val (treatmentR,staff,staffsInPractice,treatmentWithSamePatientId) = group.head
                      patientExists=true
                   }   
             }
             val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))

            var patientBlock =  db.run{
                     for {
                        count <- TreatmentTable.filterNot(_.id === id).filter(_.firstName === vFirstName).filter(_.lastName === vLastName).filter(_.phoneNumber === vPhoneNumber).length.result
                      } yield {
                        if(count > 0){
                          patientExist = true
                        }
                      }
                    }
            Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

            if (patientExists  && checkPatientId)
                    Future.successful(BadRequest(Json.parse("""{"error":"Patient Id already exists"}""")))
            else if(patientExist)
                  Future.successful(BadRequest(Json.parse("""{"error":"Patient already exists"}""")))
            else if (vInitialVisitDate !="" && !isValidDate(vInitialVisitDate,"MM/dd/yyyy"))
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid initial visit date"}""")))
            else if (vBirthDate !="" && !isValidDate(vBirthDate,"MM/dd/yyyy"))
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid birth date"}""")))          
            else if(errorFound)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid state name!"}""")))
            else if(birthDateValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Birth date should not greater than today date!"}""")))
            else if(phoneValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
            else if(zipCodeValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid zipcode!"}""")))
            else if(businessPhoneValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid business phone number!"}""")))
            else if(patientType)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please select patient type!"}""")))
            else if(vPatientRefererTo != "" && vPatientRefererTo != None  && (vReferredDate == null || vReferredDate == ""))
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter Referred Date!"}""")))
            else if(vReferredDate != "" && vReferredDate != null  && (vPatientRefererTo == None || vPatientRefererTo == ""))
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter Referred To!"}""")))
          else if(carrierNameValidation)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please select carrier name!"}""")))
            else {
                 
              db.run {
                val query = TreatmentTable.filter(_.id === id)
                
                (stepsOpt).foreach(steps => {
                  (steps).foreach(step=>{
                      if (step.formId == "1A"){
                        checkAndInsertMeasurementHistory(id,"1A",step)
                      }
                      if (step.formId == "3A"){
                        checkAndInsertMeasurementHistory(id,"3A",step)
                      }  
                      checkAndInsertClinicalNotes(id,step,request.identity.id,clientTimeZone)
                   }) 
                 }) 
               
                query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                  case true =>
                   
                    if (patientId!=""){
                      db.run{
                        TreatmentTable.filter(_.id === id).map(_.patientId).update(patientId)
                      }
                    }
                   
                   
                    
                    if (vInitialVisitDate!=""){
                        var intialVisitDate=DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vInitialVisitDate)
                        
                        
                        db.run{
                          TreatmentTable.filter(_.id === id).map(_.firstVisit).update(Some(intialVisitDate))
                        }
                    }

                      var block1 = db.run {
                          TreatmentTable.filter(_.id === id).map(_.patientType).update(Some(""))
                      } map {
                      case 0 => NotFound
                      case _ => Ok
                    }
                    var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                  DBIO.seq[Effect.Write](
                    query.map(_.lastEntry).update(Some(now)).map(_ => Unit)
                  ) flatMap { _ =>
                          stepsOpt.map { steps =>
                            DBIO.sequence(steps.map { step =>
                              StepTable += StepRow(step.value, step.state, id, step.formId, step.formVersion, now)
                               
                            })
                          }.getOrElse {
                            DBIO.successful(Unit)
                          }.map { _ =>


                          (stepsOpt).foreach(steps => {
                          (steps).foreach(step=> {
                      if (step.formId == "1A" || step.formId == "1L"){

                      db.run{
                          TreatmentTable.filter(_.id === id).map(_.referralSourceId).update(Some(vPatientReferer.toLong))
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }

                       if(step.formId == "1A"){
                      var referredTopatient: Some[Long] = null
                      
                      if(vPatientRefererTo != ""){
                        referredTopatient = Some(vPatientRefererTo.toLong)
                      }
                       db.run{
                          TreatmentTable.filter(_.id === id).map(_.referredTo).update(referredTopatient)
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }
                      
                       if (vReferredDate!=""){
                        var referredDate = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vReferredDate)
                        
                        
                            db.run{
                              TreatmentTable.filter(_.id === id).map(_.referredDate).update(Some(referredDate))
                            }
                        }
                        }

                     if(vOnlineRegStatus == "Completed"){
                        var staffName = ""
                        var staffId = request.identity.id.get
                         var block =  db.run{
                            StaffTable.filterNot(_.archived).filter(_.id === staffId).result.head 
                            } map{ staffData =>
                            staffName = staffData.name +" "+ convertOptionString(staffData.lastName)
                          }
                          Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))                        

                      db.run {
                        val query = TreatmentTable.filter(_.id === id)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true => DBIO.seq[Effect.Write](
                            Some(staffName).map(value => query.map(_.completedBy).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(DateTime.now()).map(value => query.map(_.completedDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                          ) map {_ => Ok}
                          case false => DBIO.successful(NotFound)
                        }
                      }
                      }
                        
                        db.run {
                        val query = TreatmentTable.filter(_.id === id)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true => DBIO.seq[Effect.Write](
                            Some(vPhoneNumber).map(value => query.map(_.phoneNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vEmailAddress).map(value => query.map(_.emailAddress).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vPatientType).map(value => query.map(_.patientType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vPatientStatus).map(value => query.map(_.patientStatus).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vBirthDate).map(value => query.map(_.dateOfBirth).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vFirstName).map(value => query.map(_.firstName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vLastName).map(value => query.map(_.lastName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vProviderId.toLong).map(value => query.map(_.providerId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vCarrierName).map(value => query.map(_.insurance).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vCountry).map(value => query.map(_.country).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vOnlineRegStatus).map(value => query.map(_.onlineRegistrationStatus).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                          ) map {_ => Ok}
                          case false => DBIO.successful(NotFound)
                        }
                      }

                    var block2 = db.run{
                      ReferralSourceTable.filter(_.id === vPatientReferer.toLong).result
                    } map { referralList =>
                    referralList.map{ result =>
                      db.run{
                          TreatmentTable.filter(_.id === id).map(_.referralSource).update(Some(result.value))
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }
                     }
                    }
                    var AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))


                  val jsonObject = Json.parse(step.value.toString)
                  var isMedicalCondition :Boolean  = false
                  var alerts = ""
                  if(jsonObject.toString contains "medicalCondition"){
                    for( a <- 1 to 50){

                      (jsonObject \ ("medicalCondition"+a)).validate[String] match {
                        case JsSuccess(medicalCondition, _) =>
                        var condition =s"$medicalCondition"

                         var temp = condition.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         val medicalConditionCheck = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])

                         var medicalConditionValue = ""
                         var conditionType = ""

                          if(medicalConditionCheck == true){
                            if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }
                            isMedicalCondition = true
                            alerts = alerts + medicalConditionValue + ", "

                            var medicalConditionLogRow = MedicalConditionLogRow(None,step.formId,id,
                            medicalConditionValue,conditionType,LocalDate.now(),true)

                                if(a == 41){
                                 var a = db.run{
                                   MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).delete map{
                                      case 0 => NotFound
                                      case _ => Ok
                                     }
                                  }
                                  val AwaitResult = Await.ready(a, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                            }

                           if (medicalConditionValue.trim != ""){

                             db.run {
                                     for{
                                     medicalLogLength1 <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType)
                                     .filter(_.value === medicalConditionValue).length.result
                                      }yield {

                                      if(medicalLogLength1 <= 0){
                                      db.run{
                                      MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += medicalConditionLogRow
                                      }map{
                                      case 0L => PreconditionFailed
                                      case id => Ok
                                    }
                                  } else {
                                    db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === medicalConditionValue).map(_.currentlySelected).update(true)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                                  }
                                }
                              }
                           }

                        } else {

                            if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }
                                 var block = db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === medicalConditionValue).map(_.currentlySelected).update(false)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                              val AwaitResultMC = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

                  if(jsonObject.toString contains "allergy"){
                    for( a <- 1 to 8){

                      (jsonObject \ ("allergy"+a)).validate[String] match {
                          case JsSuccess(allergy, _) =>
                          var condition =s"$allergy"

                         var temp1 = condition.replace('★', '"')
                         val jsonObject2: JsValue = Json.parse(temp1)
                         val allergyValue = convertOptionalBoolean((jsonObject2 \ ("checked")).asOpt[Boolean])

                         var allergiesValue = ""
                         var conditionType = ""

                         if(allergyValue == true){

                        if(a == 7){
                          allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                          conditionType = "Other_Allergy"
                        }else{
                          allergiesValue = allergyMap("allergy"+a)
                          conditionType = "Allergy"
                          }

                            var allergyLogRow = MedicalConditionLogRow(None,step.formId,id,
                            allergiesValue,conditionType,LocalDate.now(),true)

                           if(a == 7){
                            var b = db.run{
                              MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).delete map{
                                case 0 => NotFound
                                case _ => Ok
                             }
                           }
                           val AwaitResult = Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                          }

                          if (allergiesValue.trim != ""){

                          db.run {
                            for{
                              allergyLogLength <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).length.result
                            }yield {
                              if(allergyLogLength <= 0){
                             db.run{
                                MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += allergyLogRow
                              }map{
                               case 0L => PreconditionFailed
                               case id => Ok
                              }
                              } else {
                                db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).map(_.currentlySelected).update(true)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                              }
                            }
                          }
                         }

                        }else {

                           if(a == 7){
                            allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                            conditionType = "Other_Allergy"
                          }else{
                            allergiesValue = allergyMap("allergy"+a)
                            conditionType = "Allergy"
                          }

                          db.run{
                              MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).map(_.currentlySelected).update(false)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

                // UPDATE FOR ISMEDICALCONDITION AND ALERTS FIELDS IN TREATMENT TABLE.
                    db.run {
                        val query = TreatmentTable.filter(_.id === id)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true => DBIO.seq[Effect.Write](
                            Some(isMedicalCondition).map(value => query.map(_.isMedicalCondition).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(alerts.dropRight(2)).map(value => query.map(_.alerts).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                          ) map {_ => Ok}
                          case false => DBIO.successful(NotFound)
                        }
                      }

              // #---DoseSpot api called ---
          val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
          var userId = ""

                db.run{
                    TreatmentTable.filterNot(_.deleted).filter(_.id === id).join(StaffTable).on(_.staffId === _.id).join(PracticeTable).on(_._2.practiceId === _.id).result
                  } map { results =>

                  val data = results.groupBy(_._1._1.id).map {
                        case (treatmentId, entries) =>
                        val treatment = entries.head._1._1
                        val staff = entries.head._1._2
                        val practice = entries.head._2

                    var block = db.run{
                      StaffTable.filter(_.id === request.identity.id.get).result
                    } map { staffMap =>
                        userId = convertOptionString(staffMap.head.doseSpotUserId)
                    }
                    Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                    val clinicKey = convertOptionString(practice.doseSpotClinicKey)
                    val clinicId = convertOptionString(practice.doseSpotClinicId)
                    // val userId = convertOptionString(staff.doseSpotUserId)
                    var doseSpotPatientId = convertOptionString(treatment.doseSpotPatientId)

                    if(doseSpotPatientId != ""){
                      val accessToken:String = getAccessToken(doseSpotUrl,clinicId,clinicKey,userId)

                      var firstName = convertOptionString((jsonObject \ "full_name").asOpt[String])
                      var lastName = convertOptionString((jsonObject \ "last_name").asOpt[String])
                      var birthOfDate = convertOptionString((jsonObject \ "birth_date").asOpt[String])
                      var strGender = convertOptionString((jsonObject \ "gender").asOpt[String])
                      var address = convertOptionString((jsonObject \ "street").asOpt[String])
                      var city = convertOptionString((jsonObject \ "city").asOpt[String])
                      var state = convertOptionString((jsonObject \ "state").asOpt[String])
                      var zip = convertOptionString((jsonObject \ "zip").asOpt[String])
                      var phone = convertOptionString((jsonObject \ "phone").asOpt[String])
                      var middleName = convertOptionString((step.value \ "mi").asOpt[String])

                    var gender = ""
                    if(strGender == "male"){
                      gender = "1"
                    } else if(strGender == "female"){
                      gender = "2"
                    } else if(strGender == "unknown"){
                      gender = "3"
                    }

                      val json: String ="""{"FirstName": "rFirstName","LastName": "rLastName","DateOfBirth": "rDateOfBirth","Gender": rGender,"Address1":"rAddress","City":"rCity","State":"rState","ZipCode":"rZip","PrimaryPhone":"rPrimaryPhone","MiddleName":"rMiddleName","PrimaryPhoneType":2,"Active":true}""".stripMargin

                      var patientJson = json.replace("rFirstName", firstName).replace("rLastName", lastName).replace("rDateOfBirth", birthOfDate).replace("rGender", gender).replace("rAddress", address).replace("rCity", city).replace("rState", state).replace("rZip", zip).replace("rPrimaryPhone", phone).replace("rMiddleName",middleName)

                      val url = s"${doseSpotUrl}webapi/api/patients/${doseSpotPatientId}"
                      val result = Http(url)
                      .postData(patientJson)
                      .header("Content-Type", "application/json")
                      .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
                      .header("Charset", "UTF-8")
                      .option(HttpOptions.readTimeout(10000)).asString

                var response = (Json.parse(result.body) \ "Result").asOpt[JsValue]
                var resultCode = convertOptionString(response.flatMap(s => (s \ "ResultCode").asOpt[String]))
                var resultDes = convertOptionString(response.flatMap(s => (s \ "ResultDescription").asOpt[String]))

                if(resultCode == "ERROR"){
                  var message = "Could not update record in DoseSpot for this patient. Please check the below fields have valid values. First Name, MI, Last Name, Date of birth, Gender, Cell Phone, Street, City, State. If you still face issue please email us at: team@novadontics.com or call us at: 888.838.6682."
                  val newObj = Json.obj() + ("Result" -> JsString("Failed")) + ("message" -> JsString(message))
                  PreconditionFailed{Json.toJson(Array(newObj))}
                      }
                     }
                    }
                  }
                 }
              })
            })
            Ok
           }
          }
                    case false =>
                    DBIO.successful(NotFound)
                }.transactionally
              }
            // }
          }
          }
        case JsError(_) =>
          Future.successful(BadRequest)
      }
  }

def shareToAdmin(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>

    val sharePatient = for {
      shareToAdmin <- (request.body \ "shareToAdmin").validateOpt[Boolean]
      mainSquares <- (request.body \ "mainSquares").validateOpt[String]
      icons <- (request.body \ "icons").validateOpt[String]
      sharedDate <- (request.body \ "sharedDate").validateOpt[LocalDate]

    } yield {
      var sharedBy: Option[Long] = Option.empty[Long]

      if(shareToAdmin == Some(true)){
        sharedBy = Some(request.identity.id.get)
      }
      (shareToAdmin,mainSquares,icons,sharedDate,sharedBy)
    }

    sharePatient match {
      case JsSuccess((shareToAdmin,mainSquares,icons,sharedDate,sharedBy), _) => db.run {
        val query = TreatmentTable.filter(_.id === patientId)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(shareToAdmin).map(value => query.map(_.shareToAdmin).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(mainSquares).map(value => query.map(_.mainSquares).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(icons).map(value => query.map(_.icons).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(sharedDate).map(value => query.map(_.sharedDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(sharedBy).map(value => query.map(_.sharedBy).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  private def checkAndInsertClinicalNotes(id: Long, step: Step, identityId:Option[Long],clientTimeZone:String) = {
    var vClinicalNotesValue = checkFormsForNotes(step.formId, step.value.toString())
  }

  def checkFormsForNotes(formId: String,stepValue: String): String = {
    val jsonObject = Json.parse(stepValue)
    var result=""
    formId match {
      case "1D" =>
        result= getClinicalNotesValue(jsonObject,"patient_info_review")
      case "1J" =>
        result= getClinicalNotesValue(jsonObject,"1d1")
      case "2E" =>
        result= getClinicalNotesValue(jsonObject,"2d_1c")
      case "3A" =>
        result= getClinicalNotesValue(jsonObject,"3a1")
      case "4A" =>
        result= getClinicalNotesValue(jsonObject,"3i1")
      case _ => "Unknown"
        
    }
    return result;
  }

  def addPrefixForNotes(formId: String): String = {
    var result=""
    formId match {
      case "1D" =>
        result= "Health history form review comments"
      case "1J" =>
        result= "Impression notes"
      case "2E" =>
        result= "Bite registration notes"
      case "3A" =>
        result= "Surgery clinical notes"
      case "4A" =>
        result= "Definitive Prosthesis clinical notes"
      case default => result = ""  
    }
    return result;
  }

  def getClinicalNotesValue(jsonObject:JsValue,key:String):String = {
    var outputValue = "";
    (jsonObject \ key).validate[String] match {
        case JsSuccess(key,_)=> 
        var value = s"$key"
        if(value != null && value != ""){
          var temp = value.replace('★', '"')
          val jsonObj : JsValue = Json.parse(temp)
          outputValue = convertOptionString((jsonObj \ ("notes")).asOpt[String])
        }
        case JsError(_) => Future.successful(BadRequest)
      }
    return outputValue;
  }

  def delete(id: Long) = silhouette.SecuredAction(SupersOnly).async {
    db.run {
      TreatmentTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0L => NotFound
      case _ => Ok
    }
  }

  def updateCtScan(id: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    val ctScanFile: JsResult[String] = (request.body \ "ctScanFile").validate[String]

    ctScanFile match {
      case ctScanFile: JsSuccess[String] =>
        db.run {
          val query = TreatmentTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true =>
              ctScanFile.map(value => query.map(_.ctScanFile).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case e: JsError =>
        Future.successful(BadRequest)
    }
  }
  
  def updateContracts(id: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    val contracts: JsResult[String] = (request.body \ "contracts").validate[String]

    contracts match {
      case contracts: JsSuccess[String] =>
        db.run {
          val query = TreatmentTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true =>
              contracts.map(value => query.map(_.contracts).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case e: JsError =>
        Future.successful(BadRequest)
    }
  }
  
  def updateXray(id: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    val xray: JsResult[String] = (request.body \ "xray").validate[String]

    xray match {
      case xray: JsSuccess[String] =>
        db.run {
          val query = TreatmentTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true =>
              xray.map(value => query.map(_.xray).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case e: JsError =>
        Future.successful(BadRequest)
    }
  }
  

  
  def updateScratchPad(id: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    /*val scratchPad: JsResult[Option[String]] = (request.body \ "scratchPad").validateOpt[String]
    val scratchPadText: JsResult[Option[String]] = (request.body \ "scratchPadText").validateOpt[String]*/
    val scratchPadData = for {
      scratchPad <- (request.body \ "scratchPad").validateOpt[String]
      scratchPadText <- (request.body \ "scratchPadText").validateOpt[String]
    } yield {
      (scratchPad, scratchPadText)
    }  
    scratchPadData match {
      
      case JsSuccess((scratchPad, scratchPadText), _) => db.run {
          val query = TreatmentTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
            case true =>
              DBIO.seq[Effect.Write](
              scratchPad.map(value => query.map(_.scratchPad).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
              scratchPadText.map(value => query.map(_.scratchPadText).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case e: JsError =>
        Future.successful(BadRequest)
    }
  }
  
  def updateToothChartDate(id: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
    val toothChartInputDate = for {
      toothChartUpdatedDateStr <- (request.body \ "toothChartUpdatedDate").validate[String]
    } yield {
      val toothChartUpdatedDate=org.joda.time.DateTime.parse(toothChartUpdatedDateStr)
      (toothChartUpdatedDate)
    }
    toothChartInputDate match {
      case JsSuccess((toothChartUpdatedDate), _) =>   
        db.run {
          var query = TreatmentTable.filter(_.id === id )
          DBIO.seq[Effect.Read with Effect.Write](
            query.map(_.toothChartUpdatedDate).update(Some(toothChartUpdatedDate)).map(_ => Unit)
          )map { _ =>
              Ok
          }
        }
    }
  }   
  

  object Output {

    case class Form(id: String, version: Int, title: String, icon: String, schema: JsValue, print: Boolean, order: Long, types: String)

    object Form {
      implicit val writes = Json.writes[Form]
    }

    case class Section(title: String, forms: List[Form])

    object Section {
      implicit val writes = Json.writes[Section]
    }

  }

  db.run {
    FormTable.exists.result.flatMap {
      case true =>
        DBIO.successful(Unit)
      case false =>
        FormTable ++= List(
          FormRow("1A", 1, "Consultation Visit", "C1 Personal Info", "PersonalInfo", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"full_name\",\"type\":\"Input\",\"title\":\"Full Name\",\"required\":true}},{\"field\":{\"name\":\"phone\",\"type\":\"Input\",\"title\":\"Phone\",\"required\":true}}]},{\"columnItems\":[{\"field\":{\"name\":\"email\",\"type\":\"Input\",\"title\":\"Email\",\"required\":true}},{\"field\":{\"name\":\"complaint\",\"type\":\"Input\",\"title\":\"Patient's chief complaint and goal\",\"required\":true}},{\"field\":{\"url\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/C1.pdf\",\"type\":\"PrintButton\",\"title\":\"Print Patient Information Form\"}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"personal_info\",\"type\":\"Images\",\"title\":\"Personal info form photos\"}}]}]}],\"title\":\"C1 Personal Info\"}]"), false,0,""),
          FormRow("1B", 1, "Consultation Visit", "C2 CT Consent", "Consent", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"C2-1\",\"type\":\"Checkbox\",\"title\":\"Consent Form obtained after signature\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/C2.pdf\"}}}]}]}],\"title\":\"C2 CT Consent\" }]"), false,0,""),
          FormRow("1C", 1, "Consultation Visit", "C3 CT Scan", "CTScan", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"ctscan\",\"type\":\"Checkbox\",\"title\":\"CT scan is taken\",\"attributes\":{\"note\":true,\"images\":false}}}]}]}],\"title\":\"C3 CT Scan\"}]"), false,0,""),
          FormRow("1D", 1, "Consultation Visit", "C4 Clinical Exam", "ClinicalExam", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"c4-1a\",\"type\":\"Checkbox\",\"title\":\"Patient's Age\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Young\",\"value\":\"Young\"},{\"title\":\"Middle Age\",\"value\":\"Middle Age\"},{\"title\":\"Old\",\"value\":\"Old\"}]}}},{\"field\":{\"name\":\"c4-1b\",\"type\":\"Checkbox\",\"title\":\"Patient's Sex\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Male\",\"value\":\"Male\"},{\"title\":\"Female\",\"value\":\"Female\"}]}}},{\"field\":{\"name\":\"c4-1\",\"type\":\"Checkbox\",\"title\":\"Esthetic Demands\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Reasonable\",\"value\":\"reasonable\"},{\"title\":\"Unreasonable\",\"value\":\"unreasonable\"}]}}},{\"field\":{\"name\":\"c4-2\",\"type\":\"Checkbox\",\"title\":\"Availability of Patient\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Acceptable\",\"value\":\"Acceptable\"},{\"title\":\"Unacceptable\",\"value\":\"Unacceptable\"}]}}},{\"field\":{\"name\":\"c4-3\",\"type\":\"Checkbox\",\"title\":\"Periodontal Disease\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Exist\",\"value\":\"exist\"},{\"title\":\"Does not exist\",\"value\":\"doesnotexist\"}]}}},{\"field\":{\"name\":\"c4-4\",\"type\":\"Checkbox\",\"title\":\"Chronic lesion in implant site(s)\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Exist\",\"value\":\"exist\"},{\"title\":\"Does not exist\",\"value\":\"doesnotexist\"}]}}},{\"field\":{\"name\":\"c4-5\",\"type\":\"Checkbox\",\"title\":\"Jaw Opening (Use Intermaxillary Ruler)\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Normal>40mm\",\"value\":\"normal\"},{\"title\":\"Limited<40mm\",\"value\":\"limited\"}]}}},{\"field\":{\"name\":\"c4-6\",\"type\":\"Checkbox\",\"title\":\"Vestibular Concavity\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Normal\",\"value\":\"normal\"},{\"title\":\"Prominent\",\"value\":\"prominent\"}]}}},{\"field\":{\"name\":\"c4-7\",\"type\":\"Checkbox\",\"title\":\"Vertical Restorative Space\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"<17mm\",\"value\":\"less17\"},{\"title\":\">17mm\",\"value\":\"more17\"}]}}},{\"field\":{\"name\":\"c4-8\",\"type\":\"Checkbox\",\"title\":\"Provisionalization\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Fixed\",\"value\":\"fixed\"},{\"title\":\"Removable\",\"value\":\"removable\"}]}}},{\"field\":{\"name\":\"c4-9\",\"type\":\"Checkbox\",\"title\":\"Distribution of Forces\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Adequate\",\"value\":\"adequate\"},{\"title\":\"Inadequate\",\"value\":\"inadequate\"}]}}}]},{\"columnItems\":[{\"field\":{\"name\":\"c4-10a\",\"type\":\"Checkbox\",\"title\":\"Patient's Size\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Large - Heavy Occlusal Forces\",\"value\":\"Large - Heavy Occlusal Forces\"},{\"title\":\"Medium- Average Occlusal Forces\",\"value\":\"Medium- Average Occlusal Forces\"},{\"title\":\"Small - Light Occlusal Forces\",\"value\":\"Small - Light Occlusal Forces\"}]}}},{\"field\":{\"name\":\"c4-10b\",\"type\":\"Checkbox\",\"title\":\"Soft Tissue Type\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Thin Biotype\",\"value\":\"Thin Biotype\"},{\"title\":\"Thick Biotype\",\"value\":\"Thick Biotype\"}]}}},{\"field\":{\"name\":\"c4-10\",\"type\":\"Checkbox\",\"title\":\"Amount of keratinized Gingiva\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Adequate\",\"value\":\"adequate\"},{\"title\":\"Inadequate\",\"value\":\"inadequate\"}]}}},{\"field\":{\"name\":\"c4-11\",\"type\":\"Checkbox\",\"title\":\"Length of Anterior Pontic\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"4 or less units\",\"value\":\"4orless\"},{\"title\":\"More than 4 units\",\"value\":\"morethan4\"}]}}},{\"field\":{\"name\":\"c4-12\",\"type\":\"Checkbox\",\"title\":\"Length of Posterior Pontic\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"2 or less units\",\"value\":\"2orless\"},{\"title\":\"More than 2 units\",\"value\":\"morethan2\"}]}}},{\"field\":{\"name\":\"c4-13\",\"type\":\"Checkbox\",\"title\":\"Number of Implants\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"4\",\"value\":\"4\"},{\"title\":\"5\",\"value\":\"5\"},{\"title\":\"5+\",\"value\":\"5+\"}]}}},{\"field\":{\"name\":\"c4-14\",\"type\":\"Checkbox\",\"title\":\"Smaller Implant Diameters than desired in posterior (under 4.3mm)\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Yes\",\"value\":\"yes\"},{\"title\":\"No\",\"value\":\"no\"}]}}},{\"field\":{\"name\":\"c4-15\",\"type\":\"Checkbox\",\"title\":\"Shorter Implants than desired (most are under 10mm)\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"No\",\"value\":\"no\"},{\"title\":\"Yes\",\"value\":\"yes\"}]}}},{\"field\":{\"name\":\"c4-16\",\"type\":\"Checkbox\",\"title\":\"Prosthetic Extention (Cantilever)\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Necessary\",\"value\":\"necessary\"},{\"title\":\"Un-necessary\",\"value\":\"unnecessary\"}]}}},{\"field\":{\"name\":\"c4-17\",\"type\":\"Checkbox\",\"title\":\"Dependence on Newly Formed Bone\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Sinus\",\"value\":\"sinus\"},{\"title\":\"Socket graft\",\"value\":\"socketgraft\"}]}}},{\"field\":{\"name\":\"c4-18\",\"type\":\"Checkbox\",\"title\":\"Nature of Opposing Arch\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Removable\",\"value\":\"removable\"},{\"title\":\"Natural teeth\",\"value\":\"naturalteeth\"},{\"title\":\"Fixed Implant Prostheses\",\"value\":\"fixedimplantprostheses\"}]}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"<br/><br/><br/><br/>\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"C4 Implant Therapy Risk Factors\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Decisions\",\"fields\":[{\"name\":\"c4b-16\",\"type\":\"Checkbox\",\"title\":\"Is Immediate Loading Recommended? (Based on the HU value only)\",\"attributes\":{\"note\":false,\"options\":[{\"title\":\"Recommended (HU average is 250 or more)\",\"value\":\"Recommended\"},{\"title\":\"Not Recommended (HU average is below 250)\",\"value\":\"Not Recommended\"}]}},{\"name\":\"c4b-17c\",\"type\":\"Checkbox\",\"title\":\"Natural Bone Loss\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"None (Consider C & B)\",\"value\":\"None (Consider C & B)\"},{\"title\":\"Minimal (1-2 mm. Consider C & B)\",\"value\":\"Minimal (1-2 mm. Consider C & B)\"},{\"title\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-18b\",\"type\":\"Checkbox\",\"title\":\"Can Alveoloplasty Be Performed?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Only C & B option is available to the patient)\",\"value\":\"No (Only C & B option is available to the patient)\"},{\"title\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\",\"value\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\"},{\"title\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\",\"value\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-19\",\"type\":\"Checkbox\",\"title\":\"Is Posterior Placement Currently Possible?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Consider all on 4 or locator overdenture)\",\"value\":\"No (Consider all on 4 or locator overdenture)\"},{\"title\":\"Yes (all on 5. Adequate for certain mandibles)\",\"value\":\"Yes (all on 5. Adequate for certain mandibles)\"},{\"title\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\",\"value\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-20a\",\"type\":\"Checkbox\",\"title\":\"Do You Recommend Additional Implant Placement After Bone Grafting?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\",\"value\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\"},{\"title\":\"Yes (After block grafting or PSP for the mandible)\",\"value\":\"Yes (After block grafting or PSP for the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-20\",\"type\":\"Checkbox\",\"title\":\"Best Implant configuration\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"2 for Locators\",\"value\":\"2 for Locators\"},{\"title\":\"4 for Locators\",\"value\":\"4 for Locators\"},{\"title\":\"All on 4\",\"value\":\"All on 4\"},{\"title\":\"All on 5\",\"value\":\"All on 5\"},{\"title\":\"All on 6\",\"value\":\"All on 6\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-21\",\"type\":\"Checkbox\",\"title\":\"Type of Temporary Prosthesis Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Converted dentures\",\"value\":\"Converted dentures\"},{\"title\":\"CAD/CAM PMMA\",\"value\":\"C&B (FP3/With pink)\"},{\"title\":\"CAD/CAM Composite\",\"value\":\"CAD/CAM Composite\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-22\",\"type\":\"Checkbox\",\"title\":\"Type of Definitive Prostheses Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"C & B (No pink)\",\"value\":\"C & B (No pink))\"},{\"title\":\"C & B (With pink)\",\"value\":\"C & B (With pink)\"},{\"title\":\"Hybrid Acrylic\",\"value\":\"Hybrid Acrylic\"},{\"title\":\"Hybrid zirconia\",\"value\":\"Hybrid zirconia\"},{\"title\":\"Marius Bridge\",\"value\":\"Marius Bridge\"},{\"title\":\"Locator overdenture\",\"value\":\"Locator overdenture\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-22\",\"type\":\"Checkbox\",\"title\":\"Is Guided Surgery Recommended?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (With bone reduction guide)\",\"value\":\"Yes (With bone reduction guide)\"},{\"title\":\"Yes (Without bone reduction guide)\",\"value\":\"Yes (Without bone reduction guide)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]},{\"columnItems\":[{\"group\":{\"title\":\"\",\"fields\":[{\"name\":\"c4b-16b\",\"type\":\"Checkbox\",\"title\":\"Incisor Display When Lips Are in Repose\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"-3\",\"value\":\"-3\"},{\"title\":\"-2\",\"value\":\"-2\"},{\"title\":\"-1\",\"value\":\"-1\"},{\"title\":\"0\",\"value\":\"0\"},{\"title\":\"+1\",\"value\":\"+1\"},{\"title\":\"+2\",\"value\":\"+2\"},{\"title\":\"+3\",\"value\":\"+3\"},{\"title\":\"+4\",\"value\":\"+4\"},{\"title\":\"+5\",\"value\":\"+5\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-17\",\"type\":\"Checkbox\",\"title\":\"Gum in Exaggerated Smile\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No Gummy Smile\",\"value\":\"No Gummy Smile\"},{\"title\":\"1mm of gummy smile\",\"value\":\"1mm of gummy smile\"},{\"title\":\"2mm of gummy smile\",\"value\":\"2mm of gummy smile\"},{\"title\":\"3mm of gummy smile\",\"value\":\"3mm of gummy smile\"},{\"title\":\"4mm of gummy smile\",\"value\":\"4mm of gummy smile\"},{\"title\":\"5mm of gummy smile\",\"value\":\"5mm of gummy smile\"},{\"title\":\"6mm of gummy smile\",\"value\":\"6mm of gummy smile\"},{\"title\":\"7mm of gummy smile\",\"value\":\"7mm of gummy smile\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-17a\",\"type\":\"Checkbox\",\"title\":\"Lip Support\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"Not sure (must perform flangless try-in)\",\"value\":\"Not sure (must perform flangless try-in)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-17b\",\"type\":\"Checkbox\",\"title\":\"Attempt to Change the Skeletal Classification\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"c4b-18\",\"type\":\"Checkbox\",\"title\":\"Condition of Natural Soft Tissue Papillae\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Exist(Better outcome with C & B if selected)\",\"value\":\"Exist (Better outcome with C & B if selected)\"},{\"title\":\"Does not exist(Higher risk with C & B Consider Hybrid)\",\"value\":\"Does not exist (Higher risk with C & B. Consider Hybrid)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"<br/><br/><br/><br/>\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"C4 CT Scan Evaluation & Clinical Exam\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"clinicalexam\",\"type\":\"Checkbox\",\"title\":\"Clinical Exam is done\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/Novadontics%20Full%20Arch%20solutions.pdf\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"c4_3_2\",\"type\":\"Input\",\"title\":\"Recommended temporary prosthesis\",\"required\":false}},{\"field\":{\"name\":\"c4_3_3\",\"type\":\"Input\",\"title\":\"Recommended permanent prosthesis\",\"required\":false}},{\"field\":{\"name\":\"c4_3_4\",\"type\":\"Input\",\"title\":\"Recommended implant configuration\",\"required\":false}},{\"field\":{\"name\":\"c4_3_5\",\"type\":\"Input\",\"title\":\"Loading protocol (immediate Vs. Delayed)\",\"required\":false}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"<br/><br/><br/><br/>\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"C4 Initial Exam Recommendations\"}]"), false,0,""),
          FormRow("1F", 1, "Consultation Visit", "C5 Tx Coordinator", "Desktop", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/Novadontics%20Full%20Arch%20solutions.pdf\"},\"title\":\"Treatment Coordinator Notes\",\"fields\":[{\"name\":\"c5-1a\",\"type\":\"Checkbox\",\"title\":\"List Permanent Prostheses Options Discussed with the Patient\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1b\",\"type\":\"Checkbox\",\"title\":\"Patient’s Preference for the Permanent Prosthesis\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1c\",\"type\":\"Checkbox\",\"title\":\"List Temporary Prostheses Options Discussed with the Patient\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1d\",\"type\":\"Checkbox\",\"title\":\"Patient’s Preference for the Temporary Prosthesis\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1e\",\"type\":\"Checkbox\",\"title\":\"Patient Education Tools Used\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1f\",\"type\":\"Checkbox\",\"title\":\"Patient’s Expectations\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1g\",\"type\":\"Checkbox\",\"title\":\"Length of Treatments Discussed\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1h\",\"type\":\"Checkbox\",\"title\":\"General Complications Discussed\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1i\",\"type\":\"Checkbox\",\"title\":\"Hygiene Protocol Discussed\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1j\",\"type\":\"Checkbox\",\"title\":\"Special Questions and Concerns\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1k\",\"type\":\"Checkbox\",\"title\":\"Desired Start Date\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1l\",\"type\":\"Checkbox\",\"title\":\"Financial Options offered\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1m\",\"type\":\"Checkbox\",\"title\":\"Preferred Method of Payment\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1n\",\"type\":\"Checkbox\",\"title\":\"Next Step Discussed\",\"attributes\":{\"note\":true,\"images\":false}},{\"name\":\"c5-1o\",\"type\":\"Checkbox\",\"title\":\"Scheduled Appointment for Next Step\",\"attributes\":{\"note\":true,\"images\":false}}]}},{\"field\":{\"name\":\"c5-2\",\"type\":\"Gallery\",\"title\":\"Educational videos\",\"mediaType\":\"video\",\"items\":[{\"title\":\"Video1\",\"url\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/1f303352a9158225d5d5a40b00afc61f.mp4\",\"thumbnail\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/1f303352a9158225d5d5a40b00afc61f.png\"},{\"title\":\"Video2\",\"url\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/3b3898bddcbdd204af9cefec180a58e4.mp4\",\"thumbnail\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/3b3898bddcbdd204af9cefec180a58e4.png\"},{\"title\":\"Video3\",\"url\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/5f335eb3e56a892626d1d71727d1794d.mp4\",\"thumbnail\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/5f335eb3e56a892626d1d71727d1794d.png\"},{\"title\":\"Video5\",\"url\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/bb30d9cf65a899747b590f7c4c4a95eb.mp4\",\"thumbnail\":\"https://s3.amazonaws.com/ds-static.spfr.co/videos/bb30d9cf65a899747b590f7c4c4a95eb.png\"}]}}]}]}],\"title\":\"C5 Tx Coordinator\"}]"), false,0,""),

          FormRow("1G", 1, "Visit One - Data Collection", "1A Consents", "Consent", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1a1\",\"type\":\"Checkbox\",\"title\":\"Email Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1A1.pdf\"}}},{\"field\":{\"name\":\"1a2\",\"type\":\"Checkbox\",\"title\":\"Physician-Patient Arbitration Agreement\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1A2.pdf\"}}},{\"field\":{\"name\":\"1a3\",\"type\":\"Checkbox\",\"title\":\"Bisphosphonates Patients Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1A3.pdf\"}}}]},{\"columnItems\":[{\"field\":{\"name\":\"1a5\",\"type\":\"Checkbox\",\"title\":\"Necessary Follow-Up Care Informed Consent\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1A4.pdf\"}}},{\"field\":{\"name\":\"1a6\",\"type\":\"Checkbox\",\"title\":\"Tooth Extraction Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1A5.pdf\"}}}]}]}],\"title\":\"1A Consents\"}]"), false,0,""),
          FormRow("1H", 1, "Visit One - Data Collection", "1B Photographs", "Photographs", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1b1\",\"type\":\"Images\",\"title\":\"1. Full face frontal (no smile)\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo:<br/><br/> i. Facial proportions<br/> ii. Lip characteristics<br/> iii. Shape of face\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b1.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b2\",\"type\":\"Images\",\"title\":\"2.Full face frontal in smile\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo:<br/><br/> i. Facial symmetry\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b2.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b3\",\"type\":\"Images\",\"title\":\"3. Full face profile\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo:<br/><br/> i. Facial profile<br/> ii. Nasolabial angle (lip Support)<br/> iii. Vertical Dimensions <br/> iv. Skeletal classification\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b3.jpg\"},\"note\":false,\"images\":false}}}]},{\"columnItems\":[{\"field\":{\"name\":\"1b4\",\"type\":\"Images\",\"title\":\"4. Mouth in repose with Facial Meter \",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo width of teeth <br/> (Width of the nose determines size of teeth)\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b4.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b5\",\"type\":\"Images\",\"title\":\"5. Mouth in repose (use perio probe in negative display)\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo:<br/><br/> i. Incisal display: Upper anterior teeth: 1mm display<br/>ii. Incisal display: Lower anterior teeth: 0.5 mm display\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b5.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b6\",\"type\":\"Images\",\"title\":\"6. Mouth in widest smile\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo: <br/><br/> i. Anterior occlusal plane<br/> ii. Posterior occlusal plane<br/> iii. Occlusal cant (take a second close-up photo here with white perioprobe)<br/> iv. Buccal corridor<br/> v. Upper lip mobility: Normal 6-8 mm<br/>\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b6.jpg\"},\"note\":false,\"images\":false}}}]}]}],\"title\":\"1B Photographs 1\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1b7\",\"type\":\"Images\",\"title\":\"7. Picture with perio probes for gummy smiles\",\"attributes\":{\"info\":{\"text\":\"Evaluate on this photo: <br/><br/> i. Gingival display and gummy smile (This picture for patients with gummy smile only. Use white perio probe for this shot)\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b7.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b8\",\"type\":\"Images\",\"title\":\"8. Intraoral maximum intercuspation front\",\"attributes\":{\"info\":{\"text\":\"Evaluate on all THREE MIP photos: <br/><br/> i. Tooth shape <br/> ii. Tooth size <br/> iii. Shade <br/> iv. Crowding <br/> v. Arrangement <br/> vi. Gum shade <br/> vii. incisal overlap\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b8.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b9\",\"type\":\"Images\",\"title\":\"9. Intraoral maximum intercuspation right\",\"attributes\":{\"info\":{\"text\":\"Evaluate on all THREE MIP photos: <br/><br/> i. Tooth shape <br/> ii. Tooth size <br/> iii. Shade <br/> iv. Crowding <br/> v. Arrangement <br/> vi. Gum shade <br/> vii. incisal overlap\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b9.jpg\"},\"note\":false,\"images\":false}}}]},{\"columnItems\":[{\"field\":{\"name\":\"1b10\",\"type\":\"Images\",\"title\":\"10. Intraoral maximum intercuspation left\",\"attributes\":{\"info\":{\"text\":\"Evaluate on all THREE MIP photos: <br/><br/> i. Tooth shape <br/> ii. Tooth size <br/> iii. Shade <br/> iv. Crowding <br/> v. Arrangement <br/> vi. Gum shade <br/> vii. incisal overlap\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b10.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b11\",\"type\":\"Images\",\"title\":\"11. Occusal upper\",\"attributes\":{\"info\":{\"text\":\"Evaluate on both occlusal photos the arch form :<br/><br/> U, V, Square<br/> i. U is ideal<br/> ii. Square can result in linear implant arrangement<br/> iii. V-shape can result in anterior cantilever<br/>\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b11.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"1b12\",\"type\":\"Images\",\"title\":\"12. Occusal lower\",\"attributes\":{\"info\":{\"text\":\"Evaluate on both occlusal photos the arch form :<br/><br/> U, V, Square<br/> i. U is ideal<br/> ii. Square can result in linear implant arrangement<br/> iii. V-shape can result in anterior cantilever<br/>\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/1b12.jpg\"},\"note\":false,\"images\":false}}}]}]}],\"title\":\"1B Photographs 2\"}]"), false,0,"") ,
          FormRow("1I", 1, "Visit One - Data Collection", "1C Video", "Video", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1c2\",\"type\":\"Checkbox\",\"title\":\"Video is taken\",\"attributes\":{\"note\":true,\"images\":false}}},{\"field\":{\"name\":\"1c1\",\"type\":\"Videos\",\"title\":\"\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}}]}]}],\"title\":\"1C Video\"}]"), false,0,""),
          FormRow("1J", 1, "Visit One - Data Collection", "1D Impressions", "Impressions", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1d1\",\"type\":\"Checkbox\",\"title\":\"Impressions are made. Check the box when this step is completed and type any necessary notes.\",\"attributes\":{\"note\":true,\"images\":false}}},{\"field\":{\"text\":\"In the impression visit we have the following scenarios:<br /><br />\\n1. Patient with difficult scenario for proper impression making (when you can't get impression of the full width and depth of the sulcus): In this case we only order custom tray(s).<br />\\n<br />\\n2. Patient with enough teeth to hand mount the casts in the articulator predictably: In this case no need to order bite blocks. Provide the lab with the impressions only and order study/working models and diagnostic casts and immediate dentures. (For immediate dentures however; the lab will also need the completed Novadontics facial and smile analysis form).<br />\\n<br />\\n3. Patient with enough teeth to mount the casts in the articulator but the lab might need a help to be sure (adequate number of teeth but doubts about the stability and the reproducibility of the bite in the lab): In this case no need to order bite blocks. Provide the lab with the impressions and with a bite registration using Flexitime&reg; Bite material and order only study/working models and diagnostic casts and immediate dentures. (For immediate dentures, the lab needs also the completed Novadontics facial and smile analysis form). Use the bite registration material only in open areas (where no teeth contacts exist).<br />\\n<br />\\n4. Patient with not enough teeth to mount the casts in the articulator: In this case we need to order study/working models, diagnostic casts and bite blocks.<br />\\n<br />\\nImpression Techniques:<br />\\n<br />\\nIn the maxilla: Over extend the impression to include the hamular notches and extend to the full depth and width of the sulcus of the muco-gingival fold and posteriorly to the area beyond the junction of the hard and soft palate.<br />\\n\\nIn the Mandible: Over extend to include the full depth and width of the sulcus and invlcue the retro-molar pad bi-laterally and include the buccal shelf and fully extend fully down into the retro-mylohyoid area)<br />\\n<br />\\nImpression Materials:<br />\\n<br />\\nFor impressions: Use silicon material (i.e. Elite&reg; By Zhermack).<br />\\n\\nFor bite registration: Use rigid silicon 85+ shore hardness (i.e., Flexitime&reg; Bite - bite registration materials.<br />\\n\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"1D Impressions\"}]"), false,0,""),
          FormRow("1K", 1, "Visit One - Data Collection", "1E Lab Order", "LabOrder", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1e1\",\"type\":\"Checkbox\",\"title\":\"Visit one lab order form is printed, completed and sent to lab. Check the box when this step is completed and enter the date and name of the person who performed this task.\",\"attributes\":{\"note\":true,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1E.pdf\"}}}]}]}],\"title\":\"1E Lab Order\"}]"), false,0,""),
          FormRow("1L", 1, "Visit One - Data Collection", "1F Quality Survey", "Signature", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"1f1\",\"type\":\"Checkbox\",\"title\":\"Novadontics Quality Survey Form was given\",\"attributes\":{\"note\":true,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/1F.pdf\"}}}]}]}],\"title\":\"1F Quality Survey\"}]"), false,0,""),
          FormRow("2A", 1, "Visit Two - Data Analysis", "2A CT Scan Evaluation", "CTScanEvaluation", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Average Bone Density\",\"fields\":[{\"name\":\"2b1\",\"type\":\"Input\",\"title\":\"Anterior\",\"postfix\":\"HU VALUE\"},{\"name\":\"2b2\",\"type\":\"Input\",\"title\":\"Posterior Right\",\"postfix\":\"HU VALUE\"},{\"name\":\"2b3\",\"type\":\"Input\",\"title\":\"Posterior Left\",\"postfix\":\"HU VALUE\"}]}}]},{\"columnItems\":[{\"group\":{\"title\":\"Bone Volume\",\"fields\":[{\"name\":\"2b4\",\"type\":\"Input\",\"title\":\"Incisal Edge of anterior teeth to Nasal Floor\",\"postfix\":\"mm\"},{\"name\":\"2b5\",\"type\":\"Input\",\"title\":\"Crestal Ridge to Nasal Floor\",\"postfix\":\"mm\"},{\"name\":\"2b6\",\"type\":\"Select\",\"title\":\"Amount of bone loss occurred naturally \",\"options\":[{\"title\":\"1mm\",\"value\":\"1mm\"},{\"title\":\"2mm\",\"value\":\"2mm\"},{\"title\":\"3mm\",\"value\":\"3mm\"},{\"title\":\"4mm\",\"value\":\"4mm\"},{\"title\":\"5mm\",\"value\":\"5mm\"},{\"title\":\"6mm\",\"value\":\"6mm\"},{\"title\":\"7mm\",\"value\":\"7mm\"},{\"title\":\"8mm\",\"value\":\"8mm\"},{\"title\":\"9mm\",\"value\":\"9mm\"},{\"title\":\"10mm\",\"value\":\"10mm\"},{\"title\":\"10+mm\",\"value\":\"10+mm\"}]}]}}]}]}],\"title\":\"2A CT Scan Evaluation - Maxilla 1\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Bone Volume\",\"fields\":[{\"name\":\"2b7\",\"type\":\"Input\",\"title\":\"Anterior - Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b8\",\"type\":\"Input\",\"title\":\"Posterior Right - Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b9\",\"type\":\"Input\",\"title\":\"Posterior Left- Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b10\",\"type\":\"Select\",\"title\":\"Alveoplasty beyond 13mm (13mm is the minimum needed for 10mm implants)\",\"options\":[{\"title\":\"Possible\",\"value\":\"Possible\"},{\"title\":\"Not Possible\",\"value\":\"Not Possible\"}]},{\"name\":\"2b11\",\"type\":\"Input\",\"title\":\"Recommended Implant Location (in locations of teeth #)\"}]}}]}]}],\"title\":\"2A CT Scan Evaluation - Maxilla 2\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Decisions\",\"fields\":[{\"name\":\"2b3a-16\",\"type\":\"Checkbox\",\"title\":\"Is Immediate Loading Recommended? (Based on the HU value only)\",\"attributes\":{\"note\":false,\"options\":[{\"title\":\"Recommended (HU average is 250 or more)\",\"value\":\"Recommended\"},{\"title\":\"Not Recommended (HU average is below 250)\",\"value\":\"Not Recommended\"}]}},{\"name\":\"2b3a-17c\",\"type\":\"Checkbox\",\"title\":\"Natural Bone Loss\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"None (Consider C & B)\",\"value\":\"None (Consider C & B)\"},{\"title\":\"Minimal (1-2 mm. Consider C & B)\",\"value\":\"Minimal (1-2 mm. Consider C & B)\"},{\"title\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-18b\",\"type\":\"Checkbox\",\"title\":\"Can Alveoloplasty Be Performed?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Only C & B option is available to the patient)\",\"value\":\"No (Only C & B option is available to the patient)\"},{\"title\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\",\"value\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\"},{\"title\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\",\"value\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-19\",\"type\":\"Checkbox\",\"title\":\"Is Posterior Placement Currently Possible?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Consider all on 4 or locator overdenture)\",\"value\":\"No (Consider all on 4 or locator overdenture)\"},{\"title\":\"Yes (all on 5. Adequate for certain mandibles)\",\"value\":\"Yes (all on 5. Adequate for certain mandibles)\"},{\"title\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\",\"value\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-20a\",\"type\":\"Checkbox\",\"title\":\"Do You Recommend Additional Implant Placement After Bone Grafting?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\",\"value\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\"},{\"title\":\"Yes (After block grafting or PSP for the mandible)\",\"value\":\"Yes (After block grafting or PSP for the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-20\",\"type\":\"Checkbox\",\"title\":\"Best Implant configuration\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"2 for Locators\",\"value\":\"2 for Locators\"},{\"title\":\"4 for Locators\",\"value\":\"4 for Locators\"},{\"title\":\"All on 4\",\"value\":\"All on 4\"},{\"title\":\"All on 5\",\"value\":\"All on 5\"},{\"title\":\"All on 6\",\"value\":\"All on 6\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-21\",\"type\":\"Checkbox\",\"title\":\"Type of Temporary Prosthesis Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Converted dentures\",\"value\":\"Converted dentures\"},{\"title\":\"CAD/CAM PMMA\",\"value\":\"C&B (FP3/With pink)\"},{\"title\":\"CAD/CAM Composite\",\"value\":\"CAD/CAM Composite\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-22\",\"type\":\"Checkbox\",\"title\":\"Type of Definitive Prostheses Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"C & B (No pink)\",\"value\":\"C & B (No pink))\"},{\"title\":\"C & B (With pink)\",\"value\":\"C & B (With pink)\"},{\"title\":\"Hybrid Acrylic\",\"value\":\"Hybrid Acrylic\"},{\"title\":\"Hybrid zirconia\",\"value\":\"Hybrid zirconia\"},{\"title\":\"Marius Bridge\",\"value\":\"Marius Bridge\"},{\"title\":\"Locator overdenture\",\"value\":\"Locator overdenture\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-22\",\"type\":\"Checkbox\",\"title\":\"Is Guided Surgery Recommended?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (With bone reduction guide)\",\"value\":\"Yes (With bone reduction guide)\"},{\"title\":\"Yes (Without bone reduction guide)\",\"value\":\"Yes (Without bone reduction guide)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]},{\"columnItems\":[{\"group\":{\"title\":\"\",\"fields\":[{\"name\":\"2b3a-16b\",\"type\":\"Checkbox\",\"title\":\"Incisor Display When Lips Are in Repose\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"-3\",\"value\":\"-3\"},{\"title\":\"-2\",\"value\":\"-2\"},{\"title\":\"-1\",\"value\":\"-1\"},{\"title\":\"0\",\"value\":\"0\"},{\"title\":\"+1\",\"value\":\"+1\"},{\"title\":\"+2\",\"value\":\"+2\"},{\"title\":\"+3\",\"value\":\"+3\"},{\"title\":\"+4\",\"value\":\"+4\"},{\"title\":\"+5\",\"value\":\"+5\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-17\",\"type\":\"Checkbox\",\"title\":\"Gum in Exaggerated Smile\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No Gummy Smile\",\"value\":\"No Gummy Smile\"},{\"title\":\"1mm of gummy smile\",\"value\":\"1mm of gummy smile\"},{\"title\":\"2mm of gummy smile\",\"value\":\"2mm of gummy smile\"},{\"title\":\"3mm of gummy smile\",\"value\":\"3mm of gummy smile\"},{\"title\":\"4mm of gummy smile\",\"value\":\"4mm of gummy smile\"},{\"title\":\"5mm of gummy smile\",\"value\":\"5mm of gummy smile\"},{\"title\":\"6mm of gummy smile\",\"value\":\"6mm of gummy smile\"},{\"title\":\"7mm of gummy smile\",\"value\":\"7mm of gummy smile\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-17a\",\"type\":\"Checkbox\",\"title\":\"Lip Support\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"Not sure (must perform flangless try-in)\",\"value\":\"Not sure (must perform flangless try-in)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-17b\",\"type\":\"Checkbox\",\"title\":\"Attempt to Change the Skeletal Classification\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3a-18\",\"type\":\"Checkbox\",\"title\":\"Condition of Natural Soft Tissue Papillae\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Exist (Better outcome with C & B if selected)\",\"value\":\"Exist (Better outcome with C & B if selected)\"},{\"title\":\"Does not exist (Higher risk with C & B. Consider Hybrid)\",\"value\":\"Does not exist (Higher risk with C & B. Consider Hybrid)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"type\":\"InfoBlock\",\"text\":\"<br/><br/><br/><br/>\"}}]}]}],\"title\":\"2A CT Scan Evaluation - Maxilla 3\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2bnotes1\",\"type\":\"Checkbox\",\"title\":\"Doctor notes\",\"attributes\":{\"info\":{\"text\":\"Bone grafting recommendations if any,staged protocol Vs Immediate placement, and other surgical recommendations\"},\"note\":true}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2bmxfront\",\"type\":\"Images\",\"title\":\"Maxilla front\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20page%204%20Maxilla%20front.jpg\"}}}},{\"field\":{\"name\":\"2bmxright\",\"type\":\"Images\",\"title\":\"Maxilla right\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20page%204%20Maxilla%20right.jpg\"}}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2bmxleft\",\"type\":\"Images\",\"title\":\"Maxilla left\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20Page%204%20Maxilla%20left.jpg\"}}}}]}]}],\"title\":\"2A CT Scan Evaluation - Maxilla 4\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Average Bone Density\",\"fields\":[{\"name\":\"2b1b\",\"type\":\"Input\",\"title\":\"Anterior\",\"postfix\":\"HU VALUE\"},{\"name\":\"2b2b\",\"type\":\"Input\",\"title\":\"Posterior Right\",\"postfix\":\"HU VALUE\"},{\"name\":\"2b3b\",\"type\":\"Input\",\"title\":\"Posterior Left\",\"postfix\":\"HU VALUE\"}]}}]},{\"columnItems\":[{\"group\":{\"title\":\"Bone Volume\",\"fields\":[{\"name\":\"2b4b\",\"type\":\"Input\",\"title\":\"Incisal Edge of anterior teeth to Nasal Floor\",\"postfix\":\"mm\"},{\"name\":\"2b5b\",\"type\":\"Input\",\"title\":\"Crestal Ridge to Nasal Floor\",\"postfix\":\"mm\"},{\"name\":\"2b6b\",\"type\":\"Select\",\"title\":\"Amount of bone loss occurred naturally\",\"options\":[{\"title\":\"1mm\",\"value\":\"1mm\"},{\"title\":\"2mm\",\"value\":\"2mm\"},{\"title\":\"3mm\",\"value\":\"3mm\"},{\"title\":\"4mm\",\"value\":\"4mm\"},{\"title\":\"5mm\",\"value\":\"5mm\"},{\"title\":\"6mm\",\"value\":\"6mm\"},{\"title\":\"7mm\",\"value\":\"7mm\"},{\"title\":\"8mm\",\"value\":\"8mm\"},{\"title\":\"9mm\",\"value\":\"9mm\"},{\"title\":\"10mm\",\"value\":\"10mm\"},{\"title\":\"10+mm\",\"value\":\"10+mm\"}]}]}}]}]}],\"title\":\"2A CT Scan Evaluation - Mandible 1\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Bone Volume\",\"fields\":[{\"name\":\"2b7b\",\"type\":\"Input\",\"title\":\"Anterior - Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b8b\",\"type\":\"Input\",\"title\":\"Posterior Right - Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b9b\",\"type\":\"Input\",\"title\":\"Posterior Left- Available Bony Height Above Roots\",\"postfix\":\"mm\"},{\"name\":\"2b10b\",\"type\":\"Select\",\"title\":\"Alveoplasty beyond 13mm (13mm is the minimum needed for 10mm implants)\",\"options\":[{\"title\":\"Possible\",\"value\":\"Possible\"},{\"title\":\"Not Possible\",\"value\":\"Not Possible\"}]},{\"name\":\"2b11b\",\"type\":\"Input\",\"title\":\"Recommended Implant Location (in locations of teeth #)\"}]}}]}]}],\"title\":\"2A CT Scan Evaluation - Mandible 2\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"title\":\"Decisions\",\"fields\":[{\"name\":\"2b3b-16\",\"type\":\"Checkbox\",\"title\":\"Is Immediate Loading Recommended? (Based on the HU value only)\",\"attributes\":{\"note\":false,\"options\":[{\"title\":\"Recommended (HU average is 250 or more)\",\"value\":\"Recommended\"},{\"title\":\"Not Recommended (HU average is below 250)\",\"value\":\"Not Recommended\"}]}},{\"name\":\"2b3b-17c\",\"type\":\"Checkbox\",\"title\":\"Natural Bone Loss\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"None (Consider C & B)\",\"value\":\"None (Consider C & B)\"},{\"title\":\"Minimal (1-2 mm. Consider C & B)\",\"value\":\"Minimal (1-2 mm. Consider C & B)\"},{\"title\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\",\"value\":\"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-18b\",\"type\":\"Checkbox\",\"title\":\"Can Alveoloplasty Be Performed?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Only C & B option is available to the patient)\",\"value\":\"No (Only C & B option is available to the patient)\"},{\"title\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\",\"value\":\"Yes up to 2 mm (Consider C & B or Locator overdenture)\"},{\"title\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\",\"value\":\"Yes up to 5 mm or more (Consider Hybrid or Locator options)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-19\",\"type\":\"Checkbox\",\"title\":\"Is Posterior Placement Currently Possible?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No (Consider all on 4 or locator overdenture)\",\"value\":\"No (Consider all on 4 or locator overdenture)\"},{\"title\":\"Yes (all on 5. Adequate for certain mandibles)\",\"value\":\"Yes (all on 5. Adequate for certain mandibles)\"},{\"title\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\",\"value\":\"Yes (all on 6. Recommended for the Maxilla or the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-20a\",\"type\":\"Checkbox\",\"title\":\"Do You Recommend Additional Implant Placement After Bone Grafting?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\",\"value\":\"Yes (After Sinus elevation, block grafting or PSP for the maxilla)\"},{\"title\":\"Yes (After block grafting or PSP for the mandible)\",\"value\":\"Yes (After block grafting or PSP for the mandible)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-20\",\"type\":\"Checkbox\",\"title\":\"Best Implant configuration\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"2 for Locators\",\"value\":\"2 for Locators\"},{\"title\":\"4 for Locators\",\"value\":\"4 for Locators\"},{\"title\":\"All on 4\",\"value\":\"All on 4\"},{\"title\":\"All on 5\",\"value\":\"All on 5\"},{\"title\":\"All on 6\",\"value\":\"All on 6\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-21\",\"type\":\"Checkbox\",\"title\":\"Type of Temporary Prosthesis Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Converted dentures\",\"value\":\"Converted dentures\"},{\"title\":\"CAD/CAM PMMA\",\"value\":\"C&B (FP3/With pink)\"},{\"title\":\"CAD/CAM Composite\",\"value\":\"CAD/CAM Composite\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-22\",\"type\":\"Checkbox\",\"title\":\"Type of Definitive Prostheses Recommended\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"C & B (No pink)\",\"value\":\"C & B (No pink))\"},{\"title\":\"C & B (With pink)\",\"value\":\"C & B (With pink)\"},{\"title\":\"Hybrid Acrylic\",\"value\":\"Hybrid Acrylic\"},{\"title\":\"Hybrid zirconia\",\"value\":\"Hybrid zirconia\"},{\"title\":\"Marius Bridge\",\"value\":\"Marius Bridge\"},{\"title\":\"Locator overdenture\",\"value\":\"Locator overdenture\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-22\",\"type\":\"Checkbox\",\"title\":\"Is Guided Surgery Recommended?\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No\",\"value\":\"No\"},{\"title\":\"Yes (With bone reduction guide)\",\"value\":\"Yes (With bone reduction guide)\"},{\"title\":\"Yes (Without bone reduction guide)\",\"value\":\"Yes (Without bone reduction guide)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]},{\"columnItems\":[{\"group\":{\"title\":\"\",\"fields\":[{\"name\":\"2b3b-16b\",\"type\":\"Checkbox\",\"title\":\"Incisor Display When Lips Are in Repose\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"-3\",\"value\":\"-3\"},{\"title\":\"-2\",\"value\":\"-2\"},{\"title\":\"-1\",\"value\":\"-1\"},{\"title\":\"0\",\"value\":\"0\"},{\"title\":\"+1\",\"value\":\"+1\"},{\"title\":\"+2\",\"value\":\"+2\"},{\"title\":\"+3\",\"value\":\"+3\"},{\"title\":\"+4\",\"value\":\"+4\"},{\"title\":\"+5\",\"value\":\"+5\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-17\",\"type\":\"Checkbox\",\"title\":\"Gum in Exaggerated Smile\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No Gummy Smile\",\"value\":\"No Gummy Smile\"},{\"title\":\"1mm of gummy smile\",\"value\":\"1mm of gummy smile\"},{\"title\":\"2mm of gummy smile\",\"value\":\"2mm of gummy smile\"},{\"title\":\"3mm of gummy smile\",\"value\":\"3mm of gummy smile\"},{\"title\":\"4mm of gummy smile\",\"value\":\"4mm of gummy smile\"},{\"title\":\"5mm of gummy smile\",\"value\":\"5mm of gummy smile\"},{\"title\":\"6mm of gummy smile\",\"value\":\"6mm of gummy smile\"},{\"title\":\"7mm of gummy smile\",\"value\":\"7mm of gummy smile\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-17a\",\"type\":\"Checkbox\",\"title\":\"Lip Support\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"Not sure (must perform flangless try-in)\",\"value\":\"Not sure (must perform flangless try-in)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-17b\",\"type\":\"Checkbox\",\"title\":\"Attempt to Change the Skeletal Classification\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Needed\",\"value\":\"Needed\"},{\"title\":\"No needed\",\"value\":\"No needed\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}},{\"name\":\"2b3b-18\",\"type\":\"Checkbox\",\"title\":\"Condition of Natural Soft Tissue Papillae\",\"attributes\":{\"note\":true,\"options\":[{\"title\":\"Exist (Better outcome with C & B if selected)\",\"value\":\"Exist (Better outcome with C & B if selected)\"},{\"title\":\"Does not exist (Higher risk with C & B. Consider Hybrid)\",\"value\":\"Does not exist (Higher risk with C & B. Consider Hybrid)\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}]}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"type\":\"InfoBlock\",\"text\":\"<br/><br/><br/><br/>\"}}]}]}],\"title\":\"2A CT Scan Evaluation - Mandible 3\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2bnotes2\",\"type\":\"Checkbox\",\"title\":\"Doctor notes\",\"attributes\":{\"info\":{\"text\":\"Bone grafting recommendations if any, staged protocol Vs Immediate placement, and other surgical recommendations\"},\"note\":true}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2bmndlfront\",\"type\":\"Images\",\"title\":\"Mandible front\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20page%208%20Mandible%20front.jpg\"}}}},{\"field\":{\"name\":\"2bndlright\",\"type\":\"Images\",\"title\":\"Mandible right\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20page%208%20Mandible%20right.jpg\"}}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2bndlleft\",\"type\":\"Images\",\"title\":\"Mandible left\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2B%20page%208%20Mandible%20left.jpg\"}}}}]}]}],\"title\":\"2A CT Scan Evaluation - Mandible 4\"}]"), false,0,""),
          FormRow("2B", 1, "Visit Two - Data Analysis", "2B Facial Analysis", "FacialAnalysis", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"info\":{\"text\":\"On the frontal view picture symmetry can be evaluated by using the three horizontal reference lines (the eyebrow line, the interpupillary line, and the commissural line) as well as the facial midline can be evaluated. Generally, the interpupillary line runs parallel to the incisal margins of the maxillary incisors and to the line of gingival margins. If the other 2 lines run parallel to the interpupillary line, then this will emphasize the condition of symmetry and harmony. The facial midline runs through the glabella, tip of the nose, the philtrum and the tip of the chin. It runs usually perpendicular to the interpupillary line and coincides with the dental midline (this however happens only 70% of faces and a slight discrepancy does not alter esthetic harmony). ALso here the teeth vertical midline cant can be evaluated and must be corrcted.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2a1.jpg\"},\"title\":\"Symmetry\",\"fields\":[{\"name\":\"2a1\",\"type\":\"Checkbox\",\"title\":\"Horizontal lines are symmetrical (mainly occlusal plane matches inter-pupillary line)\"},{\"name\":\"2a2\",\"type\":\"Checkbox\",\"title\":\"Vertical line perpendicular to the interpupillary line\"},{\"name\":\"2a3\",\"type\":\"Checkbox\",\"title\":\"Vertical line coincides with the dental midline\"},{\"name\":\"2a4\",\"type\":\"Checkbox\",\"title\":\"Vertical Teeth midline cant\"}]}},{\"group\":{\"info\":{\"text\":\"Measured from the Full face frontal picture. This is different from the vertical dimension measurement. Mid face height should equal lower face height. <br/><br/>The 3 reference lines in the image are all on hard tissue landmarks.<br/> - Deepest part of bone top of nose<br/> - Base of the nose<br/> - Most mid prominent portion of chin \",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2AFacialProportions.jpg\"},\"title\":\"Facial proportions\",\"fields\":[{\"name\":\"2a5\",\"type\":\"Checkbox\",\"title\":\"Mid face height equal lower face height\"},{\"name\":\"2a5b\",\"type\":\"Checkbox\",\"title\":\"Middle Face Larger\",\"attributes\":{\"info\":{\"text\":\"Mid face larger indicates bone resorption and alveolar bony height loss.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2a_mid_face_larger.jpg\"},\"note\":false,\"images\":false}},{\"name\":\"2a6\",\"type\":\"Checkbox\",\"title\":\"Lower Face Larger\",\"attributes\":{\"info\":{\"text\":\"Larger lower face than mid face usually indicates excess maxilla leading to gummy smile.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2ALower_face_larger.jpg\"},\"note\":false,\"images\":false}}]}}]},{\"columnItems\":[{\"group\":{\"info\":{\"text\":\"As depicted on the photo.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/front_facial_photo.jpg\"},\"title\":\"Lip characteristics\",\"fields\":[{\"name\":\"2a7\",\"type\":\"Checkbox\",\"title\":\"Upper Lip Length\",\"attributes\":{\"info\":{\"text\":\"This should be measured clinically. Should be 22mm in length for 21-year-old female. Add 1mm per decade\"},\"note\":false,\"images\":false,\"options\":[{\"title\":\"FEMALE\",\"value\":\"FEMALE\"},{\"title\":\"Short (less than average)\",\"value\":\"Short (less than average)\"},{\"title\":\"Average 40 yr old (21-23mm)\",\"value\":\"Average 40 yr old (21-23mm)\"},{\"title\":\"Average 50 yr old (22-24mm)\",\"value\":\"Average 50 yr old (22-24mm)\"},{\"title\":\"Average 60 yr old (23-25mm)\",\"value\":\"Average 60 yr old (23-25mm)\"},{\"title\":\"Long (more than average) \",\"value\":\"Long (more than average) \"},{\"title\":\"MALE\",\"value\":\"MALE\"},{\"title\":\"Average 30 yr old (22-24mm)\",\"value\":\"Average 30 yr old (22-24mm)\"},{\"title\":\"Average 40 yr old (23-25mm)\",\"value\":\"Average 40 yr old (23-25mm)\"},{\"title\":\"Average 50 yr old (25-27mm)\",\"value\":\"Average 50 yr old (25-27mm)\"},{\"title\":\"Average 60 yr old (26-28mm)\",\"value\":\"Average 60 yr old (26-28mm)\"},{\"title\":\"Long (more than average) \",\"value\":\"Long (more than average) \"},{\"title\":\"Other, See Notes\",\"value\":\"other\"}]}},{\"name\":\"2a8\",\"type\":\"Checkbox\",\"title\":\"Upper Lip Horizontal Line in Mid Upper Lip\",\"attributes\":{\"info\":{\"text\":\"This is important to notice as it will worsen after a fixed restoration is placed ..Final prosthesis might require a flange)\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/upper_lip_horizontal_line.jpg\"},\"note\":false,\"images\":false,\"options\":[{\"title\":\"Exist\",\"value\":\"Exist\"},{\"title\":\"Does not exist \",\"value\":\"Does not exist \"},{\"title\":\"Other, See Notes\",\"value\":\"other\"}]}},{\"name\":\"2a9\",\"type\":\"Checkbox\",\"title\":\"Thickness of the upper lip\",\"attributes\":{\"info\":{\"text\":\"Thicker is better\"},\"note\":true,\"images\":false,\"options\":[{\"title\":\"Thick\",\"value\":\"Thick\"},{\"title\":\"Medium \",\"value\":\"Medium \"},{\"title\":\"Thin \",\"value\":\"Thin \"},{\"title\":\"Other, See Notes\",\"value\":\"other\"}]}}]}},{\"group\":{\"info\":{\"text\":\"Shape of face determines shape of teeth.The image is an example of a rectangular face. \",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/front_facial_photo.jpg\"},\"title\":\"Shape of face\",\"fields\":[{\"name\":\"2a10\",\"type\":\"Checkbox\",\"title\":\"Rectangle\"},{\"name\":\"2a11\",\"type\":\"Checkbox\",\"title\":\"Square\"},{\"name\":\"2a12\",\"type\":\"Checkbox\",\"title\":\"Oval\"},{\"name\":\"2a13\",\"type\":\"Checkbox\",\"title\":\"Triangle\"},{\"name\":\"2a14\",\"type\":\"Checkbox\",\"title\":\"Round\"}]}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"type\":\"InfoBlock\",\"text\":\"<br/><br/><br/><br/>\"}}]}]}],\"title\":\"2B Facial Analysis 1\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"info\":{\"text\":\"The patient in this picture must hold a natural head position and should be looking at horizon with relaxed lips. In this picture the profile is evaluated by measuring the angle formed by joining three facial reference points:the glabella, the subnasal point, and the most prominent point of the chin. Normally the angle is measured 170 degrees. If the angle is more acute, the profile is convex (this indicates Class II occlusion). Should this angle be greater than 180 degrees, the profile is concave (indicates skeletal class III).\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/facial-profile-type.jpg\"},\"title\":\"Facial profile type\",\"fields\":[{\"name\":\"2a14b\",\"type\":\"Checkbox\",\"title\":\"Normal / Flat\"},{\"name\":\"2a15\",\"type\":\"Checkbox\",\"title\":\"Convex\"},{\"name\":\"2a16\",\"type\":\"Checkbox\",\"title\":\"Concave\",\"attributes\":{\"info\":{\"text\":\"Risk factor is with Concave profile:A flange may be needed in the final restoration.\"},\"note\":false,\"images\":false}}]}},{\"group\":{\"info\":{\"text\":\"This angle is formed by the base of the nose and a line running from the subnasal point to the upper margin of the upper lip. Normally its between 85-108 depending on sex and race. This line is important in deciding whether a maxillary fixed or removable overdenture is more appropriate. If the angle is unaltered on inserting or removing the patient's complete denture, then a prosthesis that is completely fixed maybe an appropriate choice.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2a_nosalabial_angle.jpg\"},\"title\":\"Nasolabial angle (lip support)\",\"fields\":[{\"name\":\"2a17\",\"type\":\"Checkbox\",\"title\":\"Normal (between 85-105 degrees)\"},{\"name\":\"2a18\",\"type\":\"Checkbox\",\"title\":\"Inadequate (over 105)\",\"attributes\":{\"info\":{\"text\":\"A flange might be needed in concave profile. When not sure perform flanges try-in. \"},\"note\":false,\"images\":false}},{\"name\":\"2a19\",\"type\":\"Checkbox\",\"title\":\"Excessive (under 85 degrees)\"}]}}]},{\"columnItems\":[{\"group\":{\"info\":{\"text\":\"Loss of vertical dimension of the lower third indicates edentulous patient wearing worn down dentures and this shows in the form of thin lip and flattened lip (this also brings loss of the masticatory muscle tone). In contrast, increased vertical dimension often caused by inappropriate dental treatment (this causes masticatory muscles hypertention, sometimes pain because of the teeth invasion of the rest position -aka, freeway space-, and difficulty in pronunciation of the letter S)\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2a20.jpg\"},\"title\":\"Vertical dimensions\",\"fields\":[{\"name\":\"2a20\",\"type\":\"Checkbox\",\"title\":\"Normal\"},{\"name\":\"2a21\",\"type\":\"Checkbox\",\"title\":\"Decreased\"},{\"name\":\"2a22\",\"type\":\"Checkbox\",\"title\":\"Increased\"}]}},{\"group\":{\"info\":{\"text\":\"Class II patients: Place mandibular implants a bit angulated buccaly and the maxullary as lingual as possibel to bring the patient to class I \\n\\nClass III patients:Place mandibular implants as lingual as possible and maxillary implants angulated a bit buccally to bring the patient to an edge to edge occlusion.\"},\"title\":\"Skeletal classification\",\"fields\":[{\"name\":\"2a23\",\"type\":\"Checkbox\",\"title\":\"Class I\",\"attributes\":{\"info\":{\"text\":\"The class I relationship represents a normal relationship between both jaws. There is no anteroposterior discrepancy. A normal dentition is characterized by an ideal alignment of the upper and lower teeth and by an intermaxillary relationship (between both jaws) showing neither anterior-posterior nor transverse discrepancies. The upper canine is aligned with the embrasure between the lower canine and the lower first molar (blue arrows). The first corner (mesiobuccal cusp) of the upper first molar is aligned with the center (mesiobuccal groove) of the lower first molar (red arrows). The maxillary arch midline fits with the mandibular arch midline. The overjet of the upper incisors is minimal (about 2 mm), just like the overbite.<br/><br/>Occlusal View:Seen from above, the teeth do not show any rotation and every tooth touches each other, leaving no interdental space between them. The shape of the arch is a parabolic arch. You can see a wire bonded to the inside (on the lingual side) of the top and bottom teeth from canine to canine. This is often called a fixed or bonded lingual retainer.<br/><br/>Cephalometric Profile View:On this cephalometric X-ray, the upper teeth are over the lower teeth by about 2 mm. This is possible because the size of the lower jaw matches the upper jaw. The height of the face is in proportion with the size of the maxilla and allows the lips to touch without muscular contraction.<br/><br/>This profile shows a class I relationship of the teeth and a normal (class I) skeletal relationship of the maxilla and the mandible. This relationship is called “skeletal orthognathy<br/><br/>Panoramic View:An ideal dentition shows the teeth parallel from one another. You can see the interdental alveolar bone level in which the roots are located. The small black line visible in a few areas surrounding the roots represents the periodontal ligament. This ligament, composed of connective tissue fibers, holds the teeth and distributes the forces generated by chewing of foods in the bony support. A white line, the lamina dura, represents the lining of the tooth socket to which the periodontal ligament is attached. The ligament is attached to the tooth by the cementum which covers the dental root.<br/><br/>You can see 32 teeth on this X-ray. There are 4 upper and 4 lower incisors. The canines are beside the incisors. There are four of them, one per quadrant. There are 8 premolars, the first and second premolars, on each side of the canines. Humans have 12 molars, 3 on each side on each arch. They are the first molars that appear in the mouth at about 6-7 years of age, the second molars appear at about 12-14 years of age and finally, the third molars erupt, when they are able to, at about 17-20 years of age. The third molars, better known as wisdom teeth, are therefore the last teeth on the farthest left and right ends of both the upper and lower arches. The white line that is shown is the lingual retainer bonded to the lingual side of the anterior upper and lower teeth.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/class1-dentition.jpg\"},\"note\":false,\"images\":false}},{\"name\":\"2a24\",\"type\":\"Checkbox\",\"title\":\"Class II Division 1\",\"attributes\":{\"info\":{\"text\":\"The class II relationship means that the lower jaw is shifted backward with regard to the upper jaw.<br /><br />A class II Division 1 intermaxillary dental relationship represents a posterior discrepancy of the lower teeth with regard to the upper teeth. The embrasure between the lower canine and the lower first premolar is shifted backward with regard to the upper canine (blue arrows). The center of the lower first molar (mesiobuccal groove) is posterior to the first corner (mesiobuccal cusp) of the upper first molar (red arrows). The overjet reflects the anteroposterior discrepancy observed where the canines and the molars are (yellow arrow). The overbite is increased and, in the case shown above, the lower teeth are hidden completely by the upper teeth and touch the gingiva at the back of the upper teeth (green arrow). We call this type of deep dental overbite \\\"supraocclusion\\\".<br /><br />Occlusal View:Seen from above, the maxillary arch is narrower, because it needs to adapt to a more anterior portion of the mandibular arch which is positioned posteriorly with regard to the upper teeth. Interdental spaces can be seen on the maxillary arch. Note that some dental arches of patients in class II show more severe irregularities, because all categories of dental irregularities are possible. The blue arrows indicate where the lower incisors bite into the palate.<br /><br />Cephalometric Profile View:This cephalometric X-ray shows the posterior discrepancy of the lower jaw. The upper teeth are more advanced compared to the lower teeth (a 11-mm overjet) and we can easily see that the lower incisors touch the palate at the back of the upper teeth on the palatal gingiva (10-mm overbite). The length of the face is shorter (distance between the tip of the nose and the bottom of the chin). The discrepancy between the upper and lower teeth makes the lower lip hem under the upper incisors.<br /><br />The lower jaw is too short with regard to the upper jaw, making the lower teeth farther back with regard to the upper teeth. This profile shows a dental class II relationship and a skeletal class II relationship. We call such relation \\\"mandibular retrognathia\\\".<br /><br /> If we sum up the list of problems in the 3 planes in space, we find: <br />A length problem:the lower jaw is too short, <br />A height problem:the lower third of the face is shorter than normal, <br />A width problem: the upper jaw is too narrow<br /><br />Panoramic View:This young patient's dentition, for which we describe the above malocclusion, shows no particular problems. The third molars can be seen and are in development and we can predict that they will have no room to erupt normally in the mouth. They will presumably have to be extracted. The space between both upper incisors is visible and a little black radiolucent line testifies the absence of fusion of both hemimaxillae at this location. This separation is presumably caused by collagenous fibers composing the lingual frenulum (see the intraoral front view picture) which inserts into the intermaxillary suture.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/class2-division1.jpg\"},\"note\":false,\"images\":false}},{\"name\":\"2a25\",\"type\":\"Checkbox\",\"title\":\"Class II Division 2\",\"attributes\":{\"info\":{\"text\":\"The class II division 2 differs from division 1 by the following characteristic:the discrepancy between the upper and lower teeth does not match the discrepancy between the upper and lower teeth where the molars and canines are located (red and blue arrows). The upper incisors are tipped backward and hide the fact that the lower jaw is farther back. Moreover, the upper incisors hide the lower teeth completely. This type of exaggerated vertical covering is called supraocclusion (deep overbite). Notice the upper left lateral incisor which is tipped forward unlike the three other incisors. The classic class II division 2 shows the lateral incisors tipped forward and the central incisors tipped backward. A gingival recession is observed where the upper right canine is located (green circle). This recession was caused by the rubbing of a piercing jewel internal fastener for which we can see the cicatricial hole on the upper lip (blue circle). A gingival graft of subepithelial connective tissue was necessary to cover up the exposed root and increase the width of the band of keratinized gingiva.<br/><br/>Occlusal View:Seen from above, the maxillary arch shows an anterior flattened shape compared to a more oblong and narrower shape observed on the maxillary arch of a class II division 1 or a parabolic shape in normal dentition.<br/><br/>Cephalometric Profile View:This cephalometric X-ray shows a backward tipping of the upper incisors (linguoversion or palatoversion) that hides the posterior discrepancy of the lower jaw.The exaggerated overbite of the upper incisors over the lower incisors is easily recognizable.<br/>Notice that the chin projection is anterior to the lower teeth. It is as if the bone segment holding the lower teeth had been retained backward while the basal bone continued its growth forward. This situation characterized by the lower teeth located posteriorly on the basal bone is called dentoalveolar retrusion.<br/><br/>If we draw a tangent line from the tip of the nose to the tip of the chin, you notice that the lips are posterior to this line, which emphases the visual effect of a nose and chin projection. The lower lip is hemmed forward and the labiomental groove is emphasized (red arrow). <br/><br/><br/>a length problem:the lower teeth are posterior to the upper teeth, but the chin is relatively well positioned, <br/>a height problem:the lower third of the face is shorter than normal, <br/>widthwise: the maxillary arch has an adequate width. <br/><br/>Panoramic View:Let's notice on this panoramic X-ray the presence of the upper and lower third molars which have not erupted in the mouth yet. On the mandibular arch, the third molars do not have the necessary biological space to take position between the second molar and the vertical part of the mandible called the ramus. Their prophylactic removal is recommended since they will not have enough space in the mouth, but also mainly because an orthognathic surgery is planned and the cutline passes through the site of the third molar.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/class2-dentition.jpg\"},\"note\":false,\"images\":false}},{\"name\":\"2a26\",\"type\":\"Checkbox\",\"title\":\"Class III\",\"attributes\":{\"info\":{\"text\":\"A class III intermaxillary relationship means that the lower teeth are shifted forward with regard to the upper teeth. The embrasure between the lower canine and the lower first premolar is shifted forward with regard to the upper canine (blue arrows). The center of the lower first molar (mesiobuccal groove) is anterior to the first corner (mesiobuccal cusp) of the upper first molar. The overjet (yellow arrow) is reversed in a way that the upper incisors are behind the lower incisors (anterior crossbite) rather than the opposite as in a normal class I relationship. This case presents, in addition to the anteroposterior discrepancy, a width discrepancy severe enough to have the upper teeth positioned inside the lower teeth (green arrows). This type of intermaxillary relationship is called bilateral posterior crossbite. It is said bilateral because the relationship is in reverse direction both on the left and on the right, but there are situations where the reverse crossbite will be observed on one side only. The underdevelopment of the maxilla in width results in a lack of space and a misalignment of teeth in the dental arch. Occlusal View:Seen from above, the lack of space is obvious and the incisors do not have the necessary space for proper alignment. There is a lack of harmony between the size (or width) of the teeth and the size (or perimeter) of the maxillary and mandibular arches. Therefore, a tooth size jaw size discrepancy is present (arch length deficiency).Cephalometric Profile View:This cephalometric X-ray shows the anterior discrepancy of the lower jaw commonly called \\\"mandibular prognathism\\\". However, it is important to specify that this patient shows an upper jaw (upper maxilla) displaced posteriorly or \\\"maxillary retrognathia\\\".The lower incisors, much more \\\"vertically positioned\\\" than normal, describe an acute angle of 71&deg; with the tangent plane of the lower jaw (yellow arrows) whereas the normal should be 90&deg;. This kind of incisor compensation inclined posteriorly hides the real discrepancy between the maxilla and the mandible (blue line) compared to the visible discrepancy between the upper and lower incisors (green arrow). It also decreases the perimeter of the mandibular arch, which explains the lack of space and the reason why the lower incisor is stuck on the lingual side (see the lower occlusal view above, blue arrow).Panoramic View:All permanent teeth are in the mouth. The discrepancy between the maxilla and the mandible is so important that the upper third molars cannot touch the antagonist lower third molars and they are hyper-erupted, which explains why the upper third molars are not at the same level as the other upper teeth.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/class3-dentition.jpg\"},\"note\":false,\"images\":false}}]}}]}]}],\"title\":\"2B Facial Analysis 2\"}]"),false,0,""),
          FormRow("2C", 1, "Visit Two - Data Analysis", "2C Smile Analysis", "SmileAnalysis", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2e1\",\"type\":\"Checkbox\",\"title\":\"Symmetry / Midline Position\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Horizontal lines are symmetrical\",\"value\":\"Horizontal lines are symmetrical\"},{\"title\":\"Vertical line perpendicular to the interpupillary line\",\"value\":\"Vertical line perpendicular to the interpupillary line\"},{\"title\":\"Vertical line coincides with the dental midline\",\"value\":\"Vertical line coincides with the dental midline\"},{\"title\":\"Vertical Teeth midline cant\",\"value\":\"Vertical Teeth midline cant\"}]}}},{\"field\":{\"name\":\"2e2\",\"type\":\"Checkbox\",\"title\":\"Facial Proportions\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Upper Face Larger\",\"value\":\"Upper Face Larger\"},{\"title\":\"Lower Face Larger\",\"value\":\"Lower Face Larger\"}]}}},{\"field\":{\"name\":\"2e3\",\"type\":\"Checkbox\",\"title\":\"Lip Characteristcis\",\"attributes\":{\"note\":true,\"images\":false,\"attributes\":{\"info\":{\"text\":\"Check the box and type the thickness and the length of the upper lip, and if a horizontal line exist or not in mid upper lip\"}}}}},{\"field\":{\"name\":\"2e4\",\"type\":\"Checkbox\",\"title\":\"Face Shape\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Rectangular\",\"value\":\"Rectangular\"},{\"title\":\"Square\",\"value\":\"Square\"},{\"title\":\"Oval\",\"value\":\"Oval\"},{\"title\":\"Round\",\"value\":\"Round\"},{\"title\":\"Triangular\",\"value\":\"Triangular\"}]}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2e5\",\"type\":\"Checkbox\",\"title\":\"Facial Profile\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Normal / Flat\",\"value\":\"Normal / Flat\"},{\"title\":\"Convex\",\"value\":\"Convex\"},{\"title\":\"Concave\",\"value\":\"Concave\"}]}}},{\"field\":{\"name\":\"2e6\",\"type\":\"Checkbox\",\"title\":\"Nasolabial Angle / Lip Support\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Normal (85-105 Degrees)\",\"value\":\"Normal (85-105 Degrees)\"},{\"title\":\"Inadequate (Over 105)\",\"value\":\"Inadequate (Over 105)\"},{\"title\":\"Excessive (Under 85)\",\"value\":\"Excessive (Under 85)\"}]}}},{\"field\":{\"name\":\"2e7\",\"type\":\"Checkbox\",\"title\":\"Vertical Dimensions\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"NORMAL\",\"value\":\"NORMAL\"},{\"title\":\"DECREASED\",\"value\":\"DECREASED\"},{\"title\":\"INCREASED\",\"value\":\"INCREASED\"}]}}},{\"field\":{\"name\":\"2e8\",\"type\":\"Checkbox\",\"title\":\"Skeletal Classification\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"CLASS I\",\"value\":\"CLASS I\"},{\"title\":\"CLASS II DIVISION 1\",\"value\":\"CLASS II DIVISION 1\"},{\"title\":\"CLASS II DIVISION 2\",\"value\":\"CLASS II DIVISION 2\"},{\"title\":\"CLASS III\",\"value\":\"CLASS III\"}]}}}]}]}],\"title\":\"2C Facial Estetic Analysis\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2e9\",\"type\":\"Checkbox\",\"title\":\"Tooth Shade\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"A1 (Vita Classic)\",\"value\":\"A1 (Vita Classic)\"},{\"title\":\"A2 (Vita Classic)\",\"value\":\"A2 (Vita Classic)\"},{\"title\":\"A3 (Vita Classic)\",\"value\":\"A3 (Vita Classic)\"},{\"title\":\"A3.5 (Vita Classic)\",\"value\":\"A3.5 (Vita Classic)\"},{\"title\":\"A4 (Vita Classic)\",\"value\":\"A4 (Vita Classic)\"},{\"title\":\"B1 (Vita Classic)\",\"value\":\"B1 (Vita Classic)\"},{\"title\":\"B2 (Vita Classic)\",\"value\":\"B2 (Vita Classic)\"},{\"title\":\"B3 (Vita Classic)\",\"value\":\"B3 (Vita Classic)\"},{\"title\":\"B4 (Vita Classic)\",\"value\":\"B4 (Vita Classic)\"},{\"title\":\"C1 (Vita Classic)\",\"value\":\"C1 (Vita Classic)\"},{\"title\":\"C2 (Vita Classic)\",\"value\":\"C2 (Vita Classic)\"},{\"title\":\"C3 (Vita Classic)\",\"value\":\"C3 (Vita Classic)\"},{\"title\":\"C4 (Vita Classic)\",\"value\":\"C4 (Vita Classic)\"},{\"title\":\"M2\",\"value\":\"M2\"},{\"title\":\"M3\",\"value\":\"M3\"},{\"title\":\"M4\",\"value\":\"M4\"},{\"title\":\"M5\",\"value\":\"M5\"},{\"title\":\"M6\",\"value\":\"M6\"},{\"title\":\"M7\",\"value\":\"M7\"},{\"title\":\"M8\",\"value\":\"M8\"},{\"title\":\"M9\",\"value\":\"M9\"},{\"title\":\"M10\",\"value\":\"M10\"},{\"title\":\"M11\",\"value\":\"M11\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}},{\"field\":{\"name\":\"2e10\",\"type\":\"Checkbox\",\"title\":\"Tooth Size\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Small\",\"value\":\"Small\"},{\"title\":\"Medium\",\"value\":\"Medium\"},{\"title\":\"Large\",\"value\":\"Large\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}},{\"field\":{\"name\":\"2e11\",\"type\":\"Checkbox\",\"title\":\"Tooth Shape\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Square\",\"value\":\"Square\"},{\"title\":\"Oval\",\"value\":\"Oval\"},{\"title\":\"Round\",\"value\":\"Round\"},{\"title\":\"Triangular\",\"value\":\"Triangular\"},{\"title\":\"Rectangular\",\"value\":\"Rectangular\"},{\"title\":\"Same\",\"value\":\"Same\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}},{\"field\":{\"name\":\"2e12\",\"type\":\"Checkbox\",\"title\":\"Tooth Arrangement\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"None\",\"value\":\"None\"},{\"title\":\"Copy Existing\",\"value\":\"Copy Existing\"},{\"title\":\"Mild Crowding\",\"value\":\"Mild Crowding\"},{\"title\":\"Moderate Crowding\",\"value\":\"Moderate Crowding\"},{\"title\":\"Severe Spacing\",\"value\":\"Severe Spacing\"},{\"title\":\"Moderate Spacing\",\"value\":\"Moderate Spacing\"},{\"title\":\"Mild Spacing\",\"value\":\"Mild Spacing\"},{\"title\":\"Ideal, See Notes\",\"value\":\"Ideal, See Notes\"},{\"title\":\"Other, see notes\",\"value\":\"other\"}]}}},{\"field\":{\"name\":\"2e13\",\"type\":\"Checkbox\",\"title\":\"Gum Shade\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Copy existing\",\"value\":\"Copy existing\"},{\"title\":\"Light blend\",\"value\":\"Light blend\"},{\"title\":\"Middle blend\",\"value\":\"Middle blend\"},{\"title\":\"Dark blend\",\"value\":\"Dark blend\"}]}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2e14\",\"type\":\"Checkbox\",\"title\":\"Buccal Corridor\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Same\",\"value\":\"Same\"},{\"title\":\"Widen\",\"value\":\"Widen\"},{\"title\":\"Make Narrow\",\"value\":\"Make Narrow\"}]}}},{\"field\":{\"name\":\"2e15\",\"type\":\"Checkbox\",\"title\":\"Posterior Occlusal Plane\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Raise Left Side\",\"value\":\"Raise Left Side\"},{\"title\":\"Raise Right Side\",\"value\":\"Raise Right Side\"},{\"title\":\"Maintain (parallel to horizontal)\",\"value\":\"Maintan (parallel to horizontal)\"},{\"title\":\"Lower Left Side\",\"value\":\"Lower Left Side\"},{\"title\":\"Lower Right Side\",\"value\":\"Lower Right Side\"}]}}},{\"field\":{\"name\":\"2e16\",\"type\":\"Checkbox\",\"title\":\"Anterior Occlusal Plane\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Raise Left Side\",\"value\":\"Raise Left Side\"},{\"title\":\"Raise Right Side\",\"value\":\"Raise Right Side\"},{\"title\":\"Maintain (parallel to horizontal)\",\"value\":\"Maintan (parallel to horizontal)\"},{\"title\":\"Lower Left Side\",\"value\":\"Lower Left Side\"},{\"title\":\"Lower Right Side\",\"value\":\"Lower Right Side\"}]}}},{\"field\":{\"name\":\"2e17\",\"type\":\"Checkbox\",\"title\":\"Incisal Cant\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Maintain\",\"value\":\"Maintain\"},{\"title\":\"Lengthen Incisal Edge Position\",\"value\":\"Lengthen Incisal Edge Position\"},{\"title\":\"Shorten Incisal Edge Position\",\"value\":\"Shorten Incisal Edge Position\"}]}}},{\"field\":{\"name\":\"2e18\",\"type\":\"Checkbox\",\"title\":\"Incisal Edge\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"Maintain\",\"value\":\"Maintain\"},{\"title\":\"Lengthen Incisal Edge Position\",\"value\":\"Lengthen Incisal Edge Position\"},{\"title\":\"Shorten Incisal Edge Position\",\"value\":\"Shorten Incisal Edge Position\"}]}}}]}]}],\"title\":\"2C Smile Analysis\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2e19\",\"type\":\"Checkbox\",\"title\":\"Upper Lip Length\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"FEMALE\",\"value\":\"FEMALE\"},{\"title\":\"SHORT (less than average)\",\"value\":\"SHORT (less than average)\"},{\"title\":\"AVERAGE 30 yr old (20-22 mm)\",\"value\":\"AVERAGE 30 yr old (20-22 mm)\"},{\"title\":\"AVERAGE 40 yr old (21-23 mm)\",\"value\":\"AVERAGE 40 yr old (21-23 mm)\"},{\"title\":\"AVERAGE 50 yr old (22-24 mm)\",\"value\":\"AVERAGE 50 yr old (22-24 mm)\"},{\"title\":\"AVERAGE 60 yr old (23-25 mm)\",\"value\":\"AVERAGE 60 yr old (23-25 mm)\"},{\"title\":\"LONG (more than average)\",\"value\":\"LONG (more than average)\"},{\"title\":\"MALE\",\"value\":\"MALE\"},{\"title\":\"SHORT (less than average)\",\"value\":\"SHORT (less than average)\"},{\"title\":\"AVERAGE 30 yr old (22-24 mm)\",\"value\":\"AVERAGE 30 yr old (22-24 mm)\"},{\"title\":\"AVERAGE 40 yr old (23-25 mm)\",\"value\":\"AVERAGE 40 yr old (23-25 mm)\"},{\"title\":\"AVERAGE 50 yr old (25-27 mm)\",\"value\":\"AVERAGE 50 yr old (25-27 mm)\"},{\"title\":\"AVERAGE 60 yr old (26-28 mm)\",\"value\":\"AVERAGE 60 yr old (26-28 mm)\"},{\"title\":\"LONG (more than average)\",\"value\":\"LONG (more than average)\"}]}}},{\"field\":{\"name\":\"2e20\",\"type\":\"Checkbox\",\"title\":\"Upper Lip Mobility\",\"attributes\":{\"info\":{\"text\":\"6-8mm distance from repose line is normal. Use the teeth as a reference. Mark the heighest smile line on wax rim. This helps to determine the reason for the gummy smile. This is a Useful information if the patient does not want to show any pink.\"},\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"LOW\",\"value\":\"LOW\"},{\"title\":\"AVERAGE (6-8 mm)\",\"value\":\"AVERAGE (6-8 mm)\"},{\"title\":\"HIGH\",\"value\":\"HIGH \"}]}}},{\"field\":{\"name\":\"2e21\",\"type\":\"Checkbox\",\"title\":\"Height of Maxillary Prosthesis\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"STANDARD 17 mm\",\"value\":\"STANDARD 17 mm \"},{\"title\":\"MORE THAN 17 mm\",\"value\":\"MORE THAN 17 mm\"}]}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2e22\",\"type\":\"Checkbox\",\"title\":\"Incisal Edge to Upper Lip\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"IDEAL (edge cradled by lower lip)\",\"value\":\"IDEAL (edge cradled by lower lip)\"},{\"title\":\"UNIFORM NEGATIVE SPACE\",\"value\":\"UNIFORM NEGATIVE SPACE\"},{\"title\":\"UPPER INCISALS COVERED BY UPPER LIP\",\"value\":\"UPPER INCISALS COVERED BY UPPER LIP\"},{\"title\":\"REVERSE SMILE LINE\",\"value\":\"REVERSE SMILE LINE\"},{\"title\":\"IRREGULAR INCISAL EDGE\",\"value\":\"IRREGULAR INCISAL EDGE\"}]}}},{\"field\":{\"name\":\"2e23\",\"type\":\"Checkbox\",\"title\":\"Flange Needed:\",\"attributes\":{\"note\":false,\"images\":false,\"options\":[{\"title\":\"Other, see notes\",\"value\":\"other\"},{\"title\":\"YES\",\"value\":\"YES\"},{\"title\":\"NO\",\"value\":\"NO\"}]}}},{\"field\":{\"text\":\"IMPORTANT: PLEASE TRIM BITE REGISTRATION TO CUSP TIP ONLY ON ALL REGISTRATION. MAKE SURE HEELS ON CAST DON'T TOUCH.\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"2C Clinical Findings\"}]"), true,0,""),
          FormRow("2D", 1, "Visit Two - Data Analysis", "2D IL Pre-Surgical GL", "PostGuidelines", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2c1\",\"type\":\"Checkbox\",\"title\":\"Cooperative Patient\"}},{\"field\":{\"name\":\"2c2\",\"type\":\"Checkbox\",\"title\":\"Bone Density ( > 250 HU)\"}},{\"field\":{\"name\":\"2c3\",\"type\":\"Checkbox\",\"title\":\"Number of Implants (Minimum number of implants can be met)\"}},{\"field\":{\"name\":\"2c4\",\"type\":\"Checkbox\",\"title\":\"No Para-Functions\"}},{\"field\":{\"name\":\"2c5\",\"type\":\"Checkbox\",\"title\":\"Satisfactory Implant Microgeometry (optimal surface characteristics)\"}},{\"field\":{\"name\":\"2c6\",\"type\":\"Checkbox\",\"title\":\"Minimum 10mm length\"}},{\"field\":{\"name\":\"2c7\",\"type\":\"Checkbox\",\"title\":\"Appropriate width\"}},{\"field\":{\"name\":\"2c8\",\"type\":\"Checkbox\",\"title\":\"Tapered body design\"}},{\"field\":{\"name\":\"2c9\",\"type\":\"Checkbox\",\"title\":\"Agressive thread design\"}},{\"field\":{\"name\":\"2c10\",\"type\":\"Checkbox\",\"title\":\"Conical (Platform-switch) abutment connection\"}}]},{\"columnItems\":[{\"field\":{\"name\":\"2c11\",\"type\":\"Checkbox\",\"title\":\"No Cantilever\"}},{\"field\":{\"name\":\"2c12\",\"type\":\"Checkbox\",\"title\":\"Short Pontics\"}},{\"field\":{\"name\":\"2c13\",\"type\":\"Checkbox\",\"title\":\"Possiblt to Splint\",\"attributes\":{\"info\":{\"text\":\"Splinting of implants or implants to adjacent teeth if teeth are stable with a healthy periodontum, have a positive effect by reducing the mechanical stress applied to implants in terms of both force and moment.\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"2c14\",\"type\":\"Checkbox\",\"title\":\"Correct Force Distribution is Possible (Symmetrical implant placement and Far enough posteriorly)\",\"attributes\":{\"info\":{\"text\":\"Correct distribution of the functional load is necessary to produce favorable biomechanical conditions that improve stability of the prosthetic reconstruction and limit micromovements below a critical threshold so as not to interfere with the mechanism of healing.\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"2c15\",\"type\":\"Checkbox\",\"title\":\"Tripod placment (not linear) for satisfactory A-P spead\"}},{\"field\":{\"name\":\"2c16\",\"type\":\"Checkbox\",\"title\":\"Prosthesis Design for Optimal Oral Hygiene is Possible\"}},{\"field\":{\"name\":\"2c17\",\"type\":\"Checkbox\",\"title\":\"Immediate placement in sockets will be slightly deep (CONSIDERING Crestral bone resorption)\"}},{\"field\":{\"name\":\"2c18\",\"type\":\"Checkbox\",\"title\":\"Screw-Retained Temporary Prosthesis\"}}]}]}],\"title\":\"2D Al-Faraje Immediate Loading Pre-Surgical Guidelines\"}]"), false,0,""),
          FormRow("2E", 1, "Visit Two - Data Analysis", "2E Bite Registration", "BiteRegistration", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2d_1c\",\"type\":\"Checkbox\",\"title\":\"Bite Registration is done\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2E.PDF.pdf\"},\"note\":true,\"images\":false}}},{\"field\":{\"name\":\"2d_2c\",\"type\":\"Checkbox\",\"title\":\"Midline is marked on the bite block\"}},{\"field\":{\"name\":\"2d_3c\",\"type\":\"Checkbox\",\"title\":\"Lip position is marked on the bite block(s)\"}},{\"field\":{\"name\":\"2d_4c\",\"type\":\"Checkbox\",\"title\":\"Amount to be shown in mm in repose for upper\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2d_5c\",\"type\":\"Checkbox\",\"title\":\"Amount to be shown in mm in repose for lower\",\"attributes\":{\"note\":true}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2dphoto1\",\"type\":\"Images\",\"title\":\"Upper Cast\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2D%20Upper%20Cast.jpg\"}}}},{\"field\":{\"name\":\"2dphoto2\",\"type\":\"Images\",\"title\":\"Lower Cast\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2D%20Lower%20Cast.jpg\"}}}}]},{\"columnItems\":[{\"field\":{\"name\":\"2dphoto3\",\"type\":\"Images\",\"title\":\"Bite Blocks intra oral\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2D%20Bite%20Blocks%20Intra%20Oral.jpg\"}}}},{\"field\":{\"name\":\"2dphoto4\",\"type\":\"Images\",\"title\":\"Bite Blocks extra oral\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/2D%20Bite%20Blocks%20Extra%20Oral.jpg\"}}}}]}]}],\"title\":\"2E Bite Registration\"}]"), false ,0,""),
          FormRow("2F", 1, "Visit Two - Data Analysis", "2F Lab Order", "LabOrder", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2h1\",\"type\":\"Checkbox\",\"title\":\"Visit two lab order form is printed, completed and sent to lab along the Smile Analysis Report (2E Square). Check this box when this step is completed and enter the date and the name of the person who performed this task.\",\"attributes\":{\"note\":true,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2F.pdf\"}}}]}]}],\"title\":\"2F Lab Order\"}]"), false,0,""),
          FormRow("2G", 1, "Visit Two - Data Analysis", "2G Consents", "Signature", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2i1\",\"type\":\"Checkbox\",\"title\":\"Biopsy and Soft Tissue Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G1.pdf\"}}},{\"field\":{\"name\":\"2i2\",\"type\":\"Checkbox\",\"title\":\"Block Bone Graft Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G2.pdf\"}}},{\"field\":{\"name\":\"2i3\",\"type\":\"Checkbox\",\"title\":\"Bone Grafting and Barrier Mmembrane (GBR) Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G3.pdf\"}}},{\"field\":{\"name\":\"2i4\",\"type\":\"Checkbox\",\"title\":\"Halcion Information and Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G4.pdf\"}}},{\"field\":{\"name\":\"2i5\",\"type\":\"Checkbox\",\"title\":\"ICOI Implant Patient Information and Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G5.pdf\"}}},{\"field\":{\"name\":\"2i6\",\"type\":\"Checkbox\",\"title\":\"Implant Surgery Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G6.pdf\"}}},{\"field\":{\"name\":\"2i7\",\"type\":\"Checkbox\",\"title\":\"Maxillary Ridge Expansion Surgery with Bone Grafting Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G7.pdf\"}}},{\"field\":{\"name\":\"2i8\",\"type\":\"Checkbox\",\"title\":\"Oral Surgery and Dental Extractions Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G8.pdf\"}}},{\"field\":{\"name\":\"2i10\",\"type\":\"Checkbox\",\"title\":\"Preoperative Sedation instructions\",\"attributes\":{\"note\":false,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G9.pdf\"}}},{\"field\":{\"name\":\"2i11\",\"type\":\"Checkbox\",\"title\":\"Prosthodontic Treatment Information and Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G10.pdf\"}}},{\"field\":{\"name\":\"2i12\",\"type\":\"Checkbox\",\"title\":\"Sinus Lift Procedure with Bone Replacement Grafting Consent Form\",\"attributes\":{\"note\":false,\"images\":true,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2G11.pdf\"}}}]}]}],\"title\":\"2G Consents\"}]"), false,0,""),
          FormRow("2H", 1, "Visit Two - Data Analysis", "2H RX", "IconPrescriptions", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"Check the appropriate circle(s) below and type the name of the medication given, dosage, and instructions.\",\"type\":\"InfoBlock\"}},{\"field\":{\"name\":\"2j2\",\"type\":\"Checkbox\",\"title\":\"Antibiotics\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j3\",\"type\":\"Checkbox\",\"title\":\"Nonsteroidal Anti-inflammatory Drugs (NSAIDs)\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j4\",\"type\":\"Checkbox\",\"title\":\"Steroidal Anti-inflammatory Drugs\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j5\",\"type\":\"Checkbox\",\"title\":\"Pain Killers\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j6\",\"type\":\"Checkbox\",\"title\":\"Antibacterial Mouth rinse\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j7\",\"type\":\"Checkbox\",\"title\":\"Antianxiety (Oral Sedation)\",\"attributes\":{\"note\":true}}},{\"field\":{\"name\":\"2j8\",\"type\":\"Checkbox\",\"title\":\"Additional Notes\",\"attributes\":{\"note\":true}}}]}]}],\"title\":\"2H RX\"}]"), false,0,""),
          FormRow("2I", 1, "Visit Two - Data Analysis", "2I Treatment Planning", "IconChart", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"2f1\",\"type\":\"Checkbox\",\"title\":\"Prosthodontic Treatment Planning is done\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2I1.pdf\"},\"note\":true,\"images\":false}}},{\"field\":{\"name\":\"2g1\",\"type\":\"Checkbox\",\"title\":\"Profiles review is done\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/2I2.pdf\"},\"note\":false,\"images\":false}}}]}]}],\"title\":\"2I Treatment Planning\"}]"), false,0,""),
          FormRow("2J", 1, "Visit Two - Data Analysis", "2J Evaluation Report", "IconStepPrinter", Json.parse("null"), false,0,""),
          FormRow("3A", 1, "Visit Three - Surgery and Prosthetic Delivery", "3A Executing the Surgery", "ExecutionProtocol", Json.parse("[{\"title\":\"3A Executing the Surgery\",\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"In the clinical notes please record the date, type and amount of the anesthetic used, type of flap technique used, alveoloplasty amount performed if any, the brand and model of implants, sizes of the implants placed, location of implants, initial stability in HU or ISQ values, type of bone graft material used if any, type and number of x rays taken during surgery, describe the prosthetic phase performed at surgery if any, type and technique of suturing, how patient tolerated the surgery, type of temporization, postop instructions given, when and what to do at the next postop visit, postop medications to continue on and other related information if any.\",\"type\":\"InfoBlock\"}},{\"field\":{\"name\":\"3a1\",\"type\":\"Checkbox\",\"title\":\"The surgery is performed. Please check the box to type the clinical notes.\",\"attributes\":{\"note\":true}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3a2\",\"type\":\"Images\",\"title\":\"Pre surgical\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a2.jpg\"}}}},{\"field\":{\"name\":\"3a3\",\"type\":\"Images\",\"title\":\"After Extractions\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a3.jpg\"}}}},{\"field\":{\"name\":\"3a4\",\"type\":\"Images\",\"title\":\"After Flap Elevation\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a4.jpg\"}}}}]},{\"columnItems\":[{\"field\":{\"name\":\"3a5\",\"type\":\"Images\",\"title\":\"After Alveoloplasty\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a5.jpg\"}}}},{\"field\":{\"name\":\"3a6\",\"type\":\"Images\",\"title\":\"After Implant Insertion\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a6.jpg\"}}}},{\"field\":{\"name\":\"3a7\",\"type\":\"Images\",\"title\":\"After CS or HA with Sutures When Not Loading\",\"attributes\":{\"note\":false,\"images\":false,\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3a7.jpg\"}}}}]}]}]}]"), false,0,""),
          FormRow("3B", 1, "Visit Three - Surgery and Prosthetic Delivery", "3B IL Post Surgical GL", "PostGuidelines", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3b1\",\"type\":\"Checkbox\",\"title\":\"Initial stability was achieved on all implants ( >40 Ncm or >70 ISQ)\"}},{\"field\":{\"name\":\"3b2\",\"type\":\"Checkbox\",\"title\":\"Optimal Positioning was achieved\"}},{\"field\":{\"name\":\"3b3\",\"type\":\"Checkbox\",\"title\":\"Optimal Angulation was achieved\"}},{\"field\":{\"name\":\"3b4\",\"type\":\"Checkbox\",\"title\":\"Impression can be taken at the day of placement or within one to four days\"}},{\"field\":{\"name\":\"3b5\",\"type\":\"Checkbox\",\"title\":\"Rigid and strong restorative material\"}}]}]}],\"title\":\"3B Al-Faraje Immediate Loading Post Surgical Guidelines\"}]"), false,0,""),
          FormRow("3C", 1, "Visit Three - Surgery and Prosthetic Delivery", "3C Occlusion Type", "OcclusionType", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3c1\",\"type\":\"Checkbox\",\"title\":\"Balanced Occlusion\",\"attributes\":{\"info\":{\"text\":\"Balanced Occlusion Balanced occlusion should be the occlusal scheme of choice in full-arch immediate loading when none of the arches are loaded or when only the lower is immediately loaded vs opposing removable denture or only the upper is loaded vs opposing removable denture. Balanced occlusion can be defined as simultaneous occlusal contacts in centric occlusion and eccentric articulation. Balanced occlusion should be used when either the maxillary and mandibular prosthesis is a complete removable denture. This type of occlusion helps stabilize a complete denture. During eccentric movements, the balancing contacts will help with denture stability. The same balancing contacts used for fixed restorations has been shown to cause ceramic fractures. Balancing contacts are also called non-working side interferences. In fixed implant restorations they can be very destructive as a result of the class 1 lever system created (Masseter is the effort, the non-working or balancing side is the fulcrum, and the working canine is the load) . Balancing or NWS contacts can be very destructive when both arches have fixed restorations.\",\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3c.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3c2\",\"type\":\"Checkbox\",\"title\":\"Anterior Guidance\",\"attributes\":{\"info\":{\"text\":\"Anterior Contact/Guidance occlusion. This would be the occlusal scheme of choice when both arches are immediately loaded. A simple anterior teeth arrangement should be aimed for. All anterior teeth should have simultaneous contacts during maximum intercuspation (MIP) or centric occlusion (CO). The anterior teeth should have a 1mm vertical overlap and a 1mm horizontal overlap. This arrangement translates to a shallow (reduced force) anterior guidance. The posterior teeth should be devoid of contacts during the osseointegration phase. Posterior implants will therefore have lesser loads during this critical phase.\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3c3\",\"type\":\"Checkbox\",\"title\":\"Canine Guidance\",\"attributes\":{\"info\":{\"text\":\"Canine Guidance. This would be the choice for the permanent prosthesis. It eliminates posterior eccentric contacts in the majority of situations. Hence, simplifying occlusal management of full mouth reconstructions. Canine guidance must be shallow. This reduces forces on anterior teeth during protrusive movements. If the maxillary definitive fixed prosthesis is acrylic resin based, the anterior occlusal scheme should be designed in the following way: there should be a 1 mm vertical incisal overlap with a 1 mm horizontal open bite only. This will reduce the incidence of maxillary anterior tooth fractures.\"},\"note\":false,\"images\":false}}}]}]}],\"title\":\"3C Selecting the occlusion type\"}]"), false,0,""),
          FormRow("3D", 1, "Visit Three - Surgery and Prosthetic Delivery", "3D Pickup and Adjust", "IconFinalProthesis", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"group\":{\"info\":{\"text\":\"Please note that cement-retained prosthesis is not given as an option for the temporaray immediate load prosthesis. Cement will add a lot of risk and complications during loading (i.e., when the screws are loosening before integration with temporary prosthesis is cemented). For the defenitive prosthesis cement-retained prosthesis is an option in some cases.\"},\"title\":\"Pickup and adjustments\",\"fields\":[{\"name\":\"3d1\",\"type\":\"Checkbox\",\"title\":\"Delivery of removable full denture (when deciding not to load)\"},{\"name\":\"3d2\",\"type\":\"Checkbox\",\"title\":\"Screw-retained converted existing dentures (Direct Method) \"},{\"name\":\"3d3\",\"type\":\"Checkbox\",\"title\":\"Screw-retained converted pre-made new dentures (Direct Method) \"},{\"name\":\"3d4\",\"type\":\"Checkbox\",\"title\":\"Screw-retained CAD/CAM PMMA Prosthesis (Indirect Method)\"},{\"name\":\"3d5\",\"type\":\"Checkbox\",\"title\":\"Screw-retained CAD/CAM composite Prosthesis (Indirect Method)\"},{\"text\":\"\",\"type\":\"InfoBlock\"}]}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3d6\",\"type\":\"Images\",\"title\":\"Multi-unit abutments with handles in place\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Multi-unit%20abutments%20with%20handles%20in%20place.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d7\",\"type\":\"Images\",\"title\":\"Multi-unit abutments with white caps on\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Multi-unit%20abutments%20with%20white%20caps%20on.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d8\",\"type\":\"Images\",\"title\":\"White caps markings on the Blu-mousse\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/White%20caps%20markings%20on%20the%20blue%20mousse.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d9\",\"type\":\"Images\",\"title\":\"Denture with holes \",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Dentures%20with%20holes.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d10\",\"type\":\"Images\",\"title\":\"Temporary cylinders intra-oral\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Temporary%20cylinders%20intra-oral.jpg\"},\"note\":false,\"images\":false}}}]},{\"columnItems\":[{\"field\":{\"name\":\"3d11\",\"type\":\"Images\",\"title\":\"Temporary cylinders through the denture holes\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Temporary%20cylinders%20through%20the%20denture%20holes%20A.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d12\",\"type\":\"Images\",\"title\":\"Finished prosthesis extra-oral photo\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Finished%20prosthesis%20extra-oral%20photo%20A.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d13\",\"type\":\"Images\",\"title\":\"Finished prosthesis intra-oral photo\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20Finished%20prosthesis%20intra-oral%20photo.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d14\",\"type\":\"Images\",\"title\":\"After delivery repose\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20After%20delivery%20repose.jpg\"},\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3d15\",\"type\":\"Images\",\"title\":\"After delivery wide smile\",\"attributes\":{\"info\":{\"image\":\"https://s3.amazonaws.com/ds-static.spfr.co/img/3D%20After%20delivery%20wide%20smile.jpg\"},\"note\":false,\"images\":false}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"<br/><br/><b><font color=\\\"orangered\\\"/>DELIVERY TECHNIQUE INSTRUCTIONS</font></b><br/><br/><b>Prosthetic checklist for immediate-load provisional prosthesis delivery</b><br /><br/> <b>Maxilla</b><br /> <br/> 1) Check alveoloplasty level and bone reduction amount from the CT scan prior to start of surgery.<br /> 2) Right after extraction, check denture seating. The palate must be fully seated. <br /> 3) Right after implant placement, verify the proper angulations of the pins (the extensions of the abutment screws).<br />4) Place white caps and confirm white caps are not interfering with seating of denture. <br />5) Apply and check with Blu-Mousse&reg; and grind out these areas<br />6) Make holes over white caps.<br />7) Check seating on palate again.<br />8) Place temporary cylinders and adjust denture accordingly to ensure palatal seating.<br />9) If in the anterior region interference is present, swap straight anterior abutments for 17- or 30-degree abutments. <br />10) Verify the seating with radiographs.<br />11) Apply Vaseline&reg; on soft tissues.<br /> 12) If abutment is visible, place rubber dam material around it before connection.<br />13) Inject resin (GC UNIFAST TRAD&reg;*) with Monojet&reg; or use salt and pepper technique to connect temporary copings to denture.<br />14) Wait for full set and remove.<br /> 15) Check stability of copings. If any of them exhibits movement, add more resin, reseat, and set.<br />16) Rinse, spray, label, and box with patient name, date, delivery date and time, and your name on the lab slip and hand over to the lab.<br />17) Place white caps back on abutments.<br/><br/>After the temporary prosthesis comes back from the lab with the acrylic portion of it completely restored using ProBase&reg;,** check to make sure the following is done properly:<br />- No cantilever in the prosthesis.<br />- No areas of thin acrylic. <br />- Convex undersurface (important to enable good hygiene).<br /> - Smooth (important to minimize plaque accumulation). <br />- No acrylic over the fit surface or the intaglio of the temporary cylinders.<br /> - 2 mm of the temporary cylinders should be visible.<br />- Ensure upon delivery of the temporary prosthesis that a 2-mm gap exists between the prosthesis and the gingiva. (We do not want to compress the tissue at this point. We must allow a space for the inflammatory response after the surgery. Patient must be informed of the purpose of this gap and that of course such a gap will not be present in the permanent prosthesis.)<br />- Proper occlusion. (For simultaneous dual-arch immediate loading cases, there should be shim stock hold in the anterior and shim stock drag in the posterior, so the occlusion is canine to canine: no premature contacts and no interferences in lateral excursions in the posterior with canine guidance for lateral excursions.)<br />- Postoperative instructions should include soft diet, medications, immediate reporting of complications, and maintenance of good hygiene. <br /><br /><br/><strong>Mandible</strong> (after delivery of maxillary prosthesis)<br /><br />1. Check with Blu-Mousse&reg; that the white caps are not interfering with seating of denture.<br />2. Make sure maxillary silicone index fully seats on the maxillary immediate-load prosthesis.<br />3. Ensure a good fit of the mandibular denture in the silicone index.<br />4. Keep the posterior Blu-Mousse&reg; intact in molar regions for stability.<br />5. Connect the two anterior temporary copings.<br />6. Verify the seating with radiographs. <br />7. Inject resin (GC UNIFAST TRAD&reg;*) with Monojet&reg; or use salt and pepper technique to connect temporary copings to denture. <br />8. Remove mandibular denture.<br />9. Confirm that temporary cylinders are stable in the denture. If not, reline again.<br />10. Now connect the mandibular posterior temporary cylinders, but take x-rays for verification before connection.<br />11. After connection, remove mandibular appliance.<br />12. Check stability of copings. If any of them exhibits movement, add more resin, reseat, and set.<br />13. Rinse, spray, label, and box with patient name, date, delivery date and time, and your name on lab slip and hand over to the lab.<br />14. Place white caps back on abutments.<br /><br />After the temporary prosthesis comes back from the lab with the acrylic portion of it completely restored using ProBase&reg;,** check to make sure the following is done properly:<br />- No cantilever in the prosthesis.<br />- No areas of thin acrylic. <br />- Convex undersurface (important to enable good hygiene). <br />- Smooth (important to minimize plaque accumulation). <br />- - No acrylic over the fit surface or the intaglio of the temporary cylinders. <br />- 2 mm of the temporary cylinders should be visible.<br />- Ensure upon delivery of the temporary prosthesis that a 2-mm gap exists between the prosthesis and the gingiva. (We do not want to compress the tissue at this point. We must allow a space for the inflammatory response after the surgery. Patient must be informed of the purpose of this gap and that of course such a gap will not be present in the permanent prosthesis.)<br />- Proper occlusion. (For simultaneous dual-arch immediate loading cases, there should be shim stock hold in the anterior and shim stock drag in the posterior, so the occlusion is canine to canine: no premature contacts and no interferences in lateral excursions in the posterior with canine guidance for lateral excursions.)<br />- Postoperative instructions should include soft diet, medications, immediate reporting of complications, and maintenance of good hygiene. <br /><br />Although it is possible to convert both dentures on the day of surgery, it is always better to restore one arch at a time: the maxilla on day one and the mandible on day two. The patient will not be numb on day two, and you will have a better chance of obtaining an accurate occlusal record.<br /><br />*UNIFAST Trad by GC America, Inc. is a self-cured, methylmethacrylate resin designed to offer speed of treatment and problem-free removal during the setting phase at an economical price. This product is highly recommended for temporary inlays, onlays, crowns, and short-span partial dentures. UNIFAST Trad can be used in combination with an impression and prefabricated crowns. <br /><br />- For pressure and brush techniques<br />- Quick setting<br />- High-dimensional stability<br />- Suitable as long-term temporary<br /><br />**ProBase Cold by Ivoclar Vivadent, Inc. is suitable for both the pouring and the packing techniques. Its excellent properties ensure long-lasting denture bases. ProBase Cold is characterized by its outstanding flow and molding properties. The material complies with ISO EN 20795-1.<br /><br />Advantages:<br />- Easy to use and accurate<br />- Stability of shape and color<br /><br /><br />Indications:<br />- Repairs of conventional and implant dentures<br />- Partial and combination dentures<br />- Relines<br />- Complete dentures<br />\",\"type\":\"InfoBlock\"}}]}]}],\"title\":\"3D Temp prosthesis pickup and occlusion adjustment\"}]"), false,0,""),
          FormRow("3E", 1, "Visit Three - Surgery and Prosthetic Delivery", "3E IL Post Delivery GL", "PostGuidelines", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3e1\",\"type\":\"Checkbox\",\"title\":\"Passive fit of prosthesis is verified\"}},{\"field\":{\"name\":\"3e2\",\"type\":\"Checkbox\",\"title\":\"Appropriate occlusal scheme is followed\"}},{\"field\":{\"name\":\"3e3\",\"type\":\"Checkbox\",\"title\":\"Lateral excursion contacts should be avoided\"}},{\"field\":{\"name\":\"3e4\",\"type\":\"Checkbox\",\"title\":\"The provisional restoration should remain well attached to the implant abutments throughout the healing process\"}},{\"field\":{\"name\":\"3e5\",\"type\":\"Checkbox\",\"title\":\"If the implant-abutment connection becomes loose, the abutment should be tightened with the final prosthetic torque asap\"}},{\"field\":{\"name\":\"3e6\",\"type\":\"Checkbox\",\"title\":\"Removal of the provisional prosthesis should be done with care to avoid excessive forces at the implant-abutment interface and subsequent implant failure\"}},{\"field\":{\"name\":\"3e7\",\"type\":\"Checkbox\",\"title\":\"Instructions for Soft Diet given\"}},{\"field\":{\"name\":\"3e8\",\"type\":\"Checkbox\",\"title\":\"Instructions for plaque control given\"}},{\"field\":{\"name\":\"3e9\",\"type\":\"Checkbox\",\"title\":\"Strict follow-up schedule (Next day, 2W for S/R, 1M, 3M, Then every 6M thereafter)\"}}]}]}],\"title\":\"3E Al-Faraje Immediate Loading Post Delivery Guidelines\"}]"), false,0,""),
          FormRow("3F", 1, "Visit Three - Surgery and Prosthetic Delivery", "3F Postop     Instructions", "PostopInstructions", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3f1\",\"type\":\"Checkbox\",\"title\":\"Postoperative Instructions After Implant Placement with Immediate Loading\",\"attributes\":{\"note\":false,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/3f1.pdf\"}}},{\"field\":{\"name\":\"3f2\",\"type\":\"Checkbox\",\"title\":\"Postoperative Instructions After Implant Placement without Loading and After GBR\",\"attributes\":{\"note\":false,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/3f2.pdf\"}}},{\"field\":{\"name\":\"3f3\",\"type\":\"Checkbox\",\"title\":\"Postoperative Instructions After Socket Grafting\",\"attributes\":{\"note\":false,\"images\":false,\"printTemplate\":\"https://s3.amazonaws.com/ds-static.spfr.co/PDF/3f3.pdf\"}}},{\"field\":{\"name\":\"3f4\",\"type\":\"Checkbox\",\"title\":\"Postop instructions are printed and given\"}}]}]}],\"title\":\"3F Postop Instructions\"}]"), false,0,""),
          FormRow("3G", 1, "Visit Three - Surgery and Prosthetic Delivery", "3G Postop    Appointment", "IconAppointment", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3g1\",\"type\":\"Checkbox\",\"title\":\"Patient is given postop appointment on:\",\"attributes\":{\"note\":true, \"calendar\":true}}}]}]}],\"title\":\"3G Postop Appointment\"}]"), false,0,""),
          FormRow("3H", 1, "Visit Three - Surgery and Prosthetic Delivery", "3H Products Used", "IconProductsUsed", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"Please take pictures of all product labels used in the surgery and upload to the app in this step.\",\"type\":\"InfoBlock\"}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3h1\",\"type\":\"Images\",\"title\":\"Product 1\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h3\",\"type\":\"Images\",\"title\":\"Product 3\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h5\",\"type\":\"Images\",\"title\":\"Product 5\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h7\",\"type\":\"Images\",\"title\":\"Product 7\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h9\",\"type\":\"Images\",\"title\":\"Product 9\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}}]},{\"columnItems\":[{\"field\":{\"name\":\"3h2\",\"type\":\"Images\",\"title\":\"Product 2\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h4\",\"type\":\"Images\",\"title\":\"Product 4\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h6\",\"type\":\"Images\",\"title\":\"Product 6\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h8\",\"type\":\"Images\",\"title\":\"Product 8\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}},{\"field\":{\"name\":\"3h10\",\"type\":\"Images\",\"title\":\"Product 10\",\"attributes\":{\"info\":false,\"note\":false,\"images\":false}}}]}]}],\"title\":\"3H Products used\"}]"), false,0,""),
          FormRow("3I", 1, "Visit Three - Surgery and Prosthetic Delivery", "3I Final prosthesis", "IconFinalProthesis", Json.parse("[{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"text\":\"Please check the box to type the clinical notes for all visits related to the final prosthesis fabrication and delivery (Impression visit, Bite registration visit if performed, Try-in framework visit if performed, Delivery visit, Post delivery check, etc)\",\"type\":\"InfoBlock\"}},{\"field\":{\"name\":\"3i1\",\"type\":\"Checkbox\",\"title\":\"Final Prosthesis clinical notes\",\"attributes\":{\"note\":true,\"images\":false}}}]}]},{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3i2\",\"type\":\"Images\",\"title\":\"1. Impressions for Final Prosthesis\"}},{\"field\":{\"name\":\"3i3\",\"type\":\"Images\",\"title\":\"2. Bite Registration for Final Prosthesis\"}},{\"field\":{\"name\":\"3i4\",\"type\":\"Images\",\"title\":\"3. Verification Jig\"}}]},{\"columnItems\":[{\"field\":{\"name\":\"3i5\",\"type\":\"Images\",\"title\":\"4. Framework Try-in \"}},{\"field\":{\"name\":\"3i6\",\"type\":\"Images\",\"title\":\"5. Prosthesis Try-in\"}},{\"field\":{\"name\":\"3i7\",\"type\":\"Images\",\"title\":\"6. Final Prosthesis (Extraoral)\"}}]}]}],\"title\":\"3I Final protesis 1\"},{\"rows\":[{\"columns\":[{\"columnItems\":[{\"field\":{\"name\":\"3i7\",\"type\":\"Images\",\"title\":\"7. Final Prosthesis Frontal View in Repose (Intraoral)\"}},{\"field\":{\"name\":\"3i8\",\"type\":\"Images\",\"title\":\"8. Final Prosthesis Frontal View in Exaggerated Smile (Intraoral)\"}},{\"field\":{\"name\":\"3i9\",\"type\":\"Images\",\"title\":\"9. Final Prosthesis Lateral View Right (Intraoral)\"}}]},{\"columnItems\":[{\"field\":{\"name\":\"3i10\",\"type\":\"Images\",\"title\":\"10. Final Prosthesis Lateral View Left (Intraoral)\"}},{\"field\":{\"name\":\"3i11\",\"type\":\"Images\",\"title\":\"11. Final Prosthesis Occlusal View (Intraoral)\"}},{\"field\":{\"name\":\"3i12\",\"type\":\"Images\",\"title\":\"12. Post delivery X-rays\"}}]}]}],\"title\":\"3I Final prothesis 2\"}]"), false,0,""))
    }
} recover {
  case e: Throwable =>
  e.printStackTrace()
  Unit
}
  def readForms = silhouette.SecuredAction.async {
    db.run (
      FormTable.sortBy(t => (t.id.asc, t.version.asc)).result
    ) map { forms =>
        val data = forms.groupBy(_.section).map {
          case (title, sectionForms) =>
            val finalForms = sectionForms.map { form =>
             Output.Form(form.id, form.version, form.title, form.icon, form.schema, form.print, form.listOrder, form.types)
            }
            Output.Section(title, finalForms.toList)
        }.toList.sortBy(_.forms.head.id)
        Ok(Json.toJson(data))
      }
  }

  def basicCell(value: String, font: Font) = {
    val cell = new PdfPCell(new Phrase(value, font))
    cell.setBorder(Rectangle.BOTTOM)
    cell.setBorderColor(BaseColor.LIGHT_GRAY)
    cell.setPadding(10)
    cell
  }

  def updatePdfLinks(phrase: Phrase, vpdfs: ListBuffer[String]) = {
    val pdfLinkFont = FontFactory.getFont(FontFactory.HELVETICA, 11)
    pdfLinkFont.setColor(BaseColor.BLUE)
    if(vpdfs.size>0){
      phrase.add("\n\n")
      for(i<-0 to vpdfs.size-1){
        val pdf = vpdfs(i)
        if(pdf contains ".pdf"){
          val fileName = pdf.substring(pdf.lastIndexOf('/') + 1)
          val chunk: Chunk = new Chunk(fileName,pdfLinkFont)
          chunk.setAnchor(pdf)
          chunk.setUnderline(0.1f, -2f)
          if(i==0){
            phrase.add(chunk)
          }else{
            phrase.add(", ")
            phrase.add(chunk)
          }
        }else{
        }
      }
    }
    phrase
  }

  class HeaderFooterPageEvent extends PdfPageEventHelper  {
    override def onEndPage(writer: PdfWriter, document: Document): Unit = {
      val font = FontFactory.getFont(FontFactory.HELVETICA, 11)
      font.setColor(BaseColor.LIGHT_GRAY)
      ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("www.novadontics.com", font), 435, 30, 0)
    }
  }
  

  def generatePerioChart(patientId:Long):PdfPTable = {

    var columnWidths:Array[Float] = Array(9,6,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f,6.25f);
    var imageScalePercentage = 60
    var table_periochart: PdfPTable = new PdfPTable(columnWidths);
    
    var f = db.run {
      for {
          toothChartData <- ToothChartTable.filter(_.patientId === patientId).result
          perioChartData <- PerioChartTable.filter(_.patientId === patientId).sortBy(_.rowPosition.desc)
          .sortBy(_.dateSelected)
          .sortBy(_.perioType).result
      } yield {

        var perioChartDataTopRow = perioChartData.filter(_.rowPosition == "top")
        var perioChartDataBottomRow = perioChartData.filter(_.rowPosition == "bottom")
        val FONT_SMALL = FontFactory.getFont(FontFactory.HELVETICA, 10)
        table_periochart.setWidthPercentage(100);
        var blankRowCell = new PdfPCell(new Phrase(""))
        blankRowCell.setFixedHeight(8f);
        blankRowCell.setBorder(Rectangle.NO_BORDER);
        blankRowCell.setColspan(18);
        //add blank row
        table_periochart.addCell(blankRowCell);
        table_periochart.addCell(blankRowCell);


        //get toothconditions for each tooth and save in map variable
        var toothConditionMap:scala.collection.mutable.Map[Int,String]=scala.collection.mutable.Map()
        for( i <- 1 to 32){
          toothConditionMap.update(i,"")
          var toothData = toothChartData.find(_.toothId == i)
          if (toothData != None){
            var toothConditionString = ""
            toothData.map(row=>toothConditionMap.update(i,row.toothConditions))
          }  
        }  
        
        //add blank row
        table_periochart.addCell(blankRowCell);
        table_periochart.addCell(blankRowCell);
        var s:String = " "
        var blankCell = new PdfPCell(new Phrase(s))
        blankCell.setBorder(Rectangle.NO_BORDER);
        
        var blankCellWithBorder = new PdfPCell(new Phrase(s))
        blankCellWithBorder.setBorderColor(BaseColor.GRAY);
        
        
        var fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        var topRowCounter=1
        
        
        table_periochart.addCell(blankCellWithBorder);
        table_periochart.addCell(blankCellWithBorder);
        
        for (i<- 1 to 16){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table_periochart.addCell(cell);
        }  
        
        
        for (row <- perioChartDataTopRow) {
        
          topRowCounter = topRowCounter + 1
          var cell = new PdfPCell(new Phrase(fmt.print(row.dateSelected),FONT_SMALL))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          cell.setPaddingTop(10);

          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table_periochart.addCell(cell);
          cell = new PdfPCell(new Phrase(row.perioType, FONT_SMALL))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          cell.setPaddingTop(10);
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table_periochart.addCell(cell);

          val jsonList: List[JsValue] = Json.parse(row.perioNumbers.toString).as[List[JsValue]]

          (jsonList).foreach(chart=>{
            var result = ""
            var resultPN = ""
            var resultB = ""
            var resultF = ""
            var resultR = ""
            var datePhrase:Phrase = new Phrase()

            val ValueB = (chart \ ("B")).as[String]
            val ValueF = (chart \ ("F")).as[String]
            val ValueR = (chart \ ("R")).as[String]
            val ValuePN = (chart \ ("PN")).as[String]

              if(ValueB == "Yes"){
              resultPN = "  "+ValuePN
              val FONT_SMALL_RED = FontFactory.getFont(FontFactory.HELVETICA, 10)
              FONT_SMALL_RED.setColor(BaseColor.RED)
              cell = new PdfPCell();
              cell.setPaddingLeft(3);
              datePhrase = new Phrase(new Chunk( resultPN , FONT_SMALL));
              datePhrase.add(new Phrase(new Chunk(" •", FONT_SMALL_RED)));
              }else{
              resultPN = ValuePN
              datePhrase = new Phrase(new Chunk( resultPN , FONT_SMALL));
              cell = new PdfPCell();
              cell.setPaddingLeft(10);
              }

            if(ValueB == "Yes"){
              resultB = "B"
            }
            if( ValueF != "" && ValueF != null){
                  if(resultB != null && resultB != ""){
                    resultF = "/F"+ValueF
                  }else{
                    resultF = "F"+ValueF
                  }
            }
            if(ValueR != "" && ValueR != null){
                  if(resultF != "" || resultB != ""){
                        resultR = "/R"+ValueR
                  }else{
                        resultR = "R"+ValueR
                  }
            }

            result = "\n"+resultB+resultF+resultR

              FONT_SMALL.setColor(BaseColor.BLACK)
              datePhrase.add(new Phrase(new Chunk(result, FONT_SMALL)));
              cell.addElement(datePhrase)

              cell.setBorderColor(BaseColor.GRAY);
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);

              cell.setPaddingBottom(6);
              table_periochart.addCell(cell);
          })
        }
        
        for (i <- topRowCounter to 4){
            for (j <- 1 to 18){
              table_periochart.addCell(blankCellWithBorder);
            }  
        } 
        table_periochart.addCell(blankRowCell);
        table_periochart.addCell(blankCell);
        table_periochart.addCell(blankCell);
        for( i <- 1 to 16){
            var toothConditions=toothConditionMap(i).split(",")
            if (toothConditions contains "MT"){
                var cell = new PdfPCell(new Phrase(""))
                cell.setBorder(Rectangle.NO_BORDER);
                table_periochart.addCell(cell);
            }else{
              var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/side-view"+i+".png"
              var image1 = Image.getInstance(imageURL)
              
              if (toothConditions contains "DT"){
                imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-side-view"+i+".png"
                image1 = Image.getInstance(imageURL)
              }     
              image1.setAlignment(Element.ALIGN_CENTER)
              image1.scalePercent(imageScalePercentage)
              var cell = new PdfPCell(image1);
              cell.setBorder(Rectangle.NO_BORDER);
              cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
              table_periochart.addCell(cell);
            }  
        }

        table_periochart.addCell(blankCell);
        table_periochart.addCell(blankCell);
        for( i <- 1 to 16){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setBorder(Rectangle.NO_BORDER);
          table_periochart.addCell(cell);
        }

        //add blank row
        table_periochart.addCell(blankRowCell);
        
        table_periochart.addCell(blankCell);
        table_periochart.addCell(blankCell);
        
        for (i <- (17 to 32).reverse){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setBorder(Rectangle.NO_BORDER);
          table_periochart.addCell(cell);
        }

        //add blank row
        table_periochart.addCell(blankRowCell);
        
        table_periochart.addCell(blankCell);
        table_periochart.addCell(blankCell);
        
        for (i <- (17 to 32).reverse){
            var toothConditions=toothConditionMap(i).split(",")
            if (toothConditions contains "MT"){
                var cell = new PdfPCell(new Phrase(""))
                cell.setBorder(Rectangle.NO_BORDER);
                table_periochart.addCell(cell);
            }else{
              var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/side-view"+i+".png"
              var image1 = Image.getInstance(imageURL)
              if (toothConditions contains "DT"){
                imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-side-view"+i+".png"
                image1 = Image.getInstance(imageURL)
              }     
              image1.setAlignment(Element.ALIGN_CENTER)
              image1.scalePercent(imageScalePercentage)
              var cell = new PdfPCell(image1);
              cell.setBorder(Rectangle.NO_BORDER);
              cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
              table_periochart.addCell(cell);
            }   
        }
        
        table_periochart.addCell(blankRowCell);
        
        table_periochart.addCell(blankCellWithBorder);
        table_periochart.addCell(blankCellWithBorder);
        for (i <- (17 to 32).reverse){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table_periochart.addCell(cell);
        }  
        
        
        var bottomRowCounter = 1
        for (row <- perioChartDataBottomRow) {
          bottomRowCounter = bottomRowCounter + 1;
          var cell = new PdfPCell(new Phrase(fmt.print(row.dateSelected)))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table_periochart.addCell(cell);
          cell = new PdfPCell(new Phrase(row.perioType))
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(5)
          table_periochart.addCell(cell);

          val jsonList: List[JsValue] = Json.parse(row.perioNumbers.toString).as[List[JsValue]]

          (jsonList).foreach(chart=>{
            var result = ""
            var resultPN = ""
            var resultB = ""
            var resultF = ""
            var resultR = ""
            var datePhrase:Phrase = new Phrase()

            val ValueB = (chart \ ("B")).as[String]
            val ValueF = (chart \ ("F")).as[String]
            val ValueR = (chart \ ("R")).as[String]
            val ValuePN = (chart \ ("PN")).as[String]

            if(ValueB == "Yes"){
              resultPN = "  "+ValuePN
              val FONT_SMALL_RED = FontFactory.getFont(FontFactory.HELVETICA, 10)
              FONT_SMALL_RED.setColor(BaseColor.RED)
              cell = new PdfPCell();
              cell.setPaddingLeft(3);
              datePhrase = new Phrase(new Chunk( resultPN , FONT_SMALL));
              datePhrase.add(new Phrase(new Chunk(" •", FONT_SMALL_RED)));
            }else{
              resultPN = ValuePN
              datePhrase = new Phrase(new Chunk( resultPN , FONT_SMALL));
              cell = new PdfPCell();
              cell.setPaddingLeft(10);
            }

            if(ValueB == "Yes"){
              resultB = "B"
            }
            if( ValueF != "" && ValueF != null){
              if(resultB != null && resultB != ""){
                resultF = "/F"+ValueF
              }else{
                resultF = "F"+ValueF
              }
            }
            if(ValueR != "" && ValueR != null){
              if(resultF != "" || resultB != ""){
                resultR = "/R"+ValueR
              }else{
                resultR = "R"+ValueR
              }
            }

            result = "\n"+resultB+resultF+resultR

            FONT_SMALL.setColor(BaseColor.BLACK)
            datePhrase.add(new Phrase(new Chunk(result, FONT_SMALL)));
            cell.addElement(datePhrase)

            cell.setBorderColor(BaseColor.GRAY);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            cell.setPaddingBottom(6);
            table_periochart.addCell(cell);
          })
        }
        
        for (i <- bottomRowCounter to 4){
            for (j <- 1 to 18){
              table_periochart.addCell(blankCellWithBorder);
            }  
        }

        //add blank row
        table_periochart.addCell(blankRowCell);
      }
    }
    
    val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    table_periochart    
  }
  
  def getImageFromURL(imageURL:String):Image={
    
      val bi = ImageIO.read(new URL(imageURL))
      val scaleFactor = 1
      val img = new BufferedImage(bi.getWidth, bi.getHeight, BufferedImage.TYPE_INT_RGB)
      val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
      val g = img.createGraphics()
      g.drawRenderedImage(bi, at)
      val imgBytes = new ByteArrayOutputStream()
      ImageIO.write(img, "PNG", imgBytes)
      val image = Image.getInstance(imgBytes.toByteArray)
      return image

    
  }
  
  
  
  
  def generateToothChart(patientId:Long):PdfPTable = {
    var columnWidths:Array[Float] = Array(6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6);
    var table_toothchart: PdfPTable = new PdfPTable(columnWidths);
    val currentDirectory = new java.io.File(".").getCanonicalPath
    //Console.println(currentDirectory)
    var imageScalePercentage = 60
    var f = db.run {
      for {
          toothChartData <- ToothChartTable.filter(_.patientId === patientId).result
      } yield {
        
        val FONT_SMALL = FontFactory.getFont(FontFactory.HELVETICA, 10)
    
        table_toothchart.setWidthPercentage(90);
        //create blank row 
        var blankRowCell = new PdfPCell(new Phrase(""))
        blankRowCell.setFixedHeight(8f);
        blankRowCell.setBorder(Rectangle.NO_BORDER);
        blankRowCell.setColspan(16);
        
        //add blank row
        table_toothchart.addCell(blankRowCell);
    
        //get toothconditions for each tooth and save in map variable
        var toothConditionMap:scala.collection.mutable.Map[Int,String]=scala.collection.mutable.Map()
        for( i <- 1 to 32){
          toothConditionMap.update(i,"")
          var toothData = toothChartData.find(_.toothId == i)
          var toothConditionFlag = false
          if (toothData != None){
            var toothConditionString = ""
            toothData.map(row=>toothConditionMap.update(i,row.toothConditions))
          }  
        }
        //show top row tooth numbers
       for( i <- 1 to 16){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(10)
          table_toothchart.addCell(cell);
        }  
        
        
        val font = FontFactory.getFont(FontFactory.HELVETICA, 9)
        font.setColor(BaseColor.GRAY)
        //Show conditions for top row
        for( i <- 1 to 16){
          if (toothConditionMap(i)!=""){
            var toothConditions=toothConditionMap(i).split(",")
            var cell = new PdfPCell()
            cell.setMinimumHeight(100)    
            for(toothCondition<-toothConditions){
                var para = new Paragraph(toothCondition, font);
                para.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(para);
            }
            cell.setPadding(10)
            
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            table_toothchart.addCell(cell);  
          }else{
            var cell = new PdfPCell(new Phrase(""))
            cell.setBorderColor(BaseColor.GRAY);
            cell.setPadding(5)
            table_toothchart.addCell(cell);  
          }    
        }
        //add blank row
        table_toothchart.addCell(blankRowCell);

        for( i <- 1 to 16){
            
            var toothConditions=toothConditionMap(i).split(",")
            if (toothConditions contains "MT"){
                var cell = new PdfPCell(new Phrase(""))
                cell.setBorder(Rectangle.NO_BORDER);
                table_toothchart.addCell(cell);
            }else{
              var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/side-view"+i+".png"
              var image1 = Image.getInstance(imageURL)

              if (toothConditions contains "DT"){
                imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-side-view"+i+".png"
                image1 = Image.getInstance(imageURL)
              }
              image1.scalePercent(imageScalePercentage)
              var cell = new PdfPCell(image1);
              cell.setBorder(Rectangle.NO_BORDER);
              cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
              table_toothchart.addCell(cell);
            }  
        }

        //add blank row
        table_toothchart.addCell(blankRowCell);

        for( i <- 1 to 16){
            var toothConditions=toothConditionMap(i).split(",")
            if (toothConditions contains "MT"){
                var cell = new PdfPCell(new Phrase(""))
                cell.setBorder(Rectangle.NO_BORDER);
                table_toothchart.addCell(cell);
            }else{
              var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/top-view"+i+".png"
              var image1 = Image.getInstance(imageURL)
              
              if (toothConditions contains "DT"){
                imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-top-view"+i+".png"
                image1 = Image.getInstance(imageURL)
              }
              image1.scalePercent(imageScalePercentage)
              var cell = new PdfPCell(image1);
              cell.setBorder(Rectangle.NO_BORDER);
              cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
              table_toothchart.addCell(cell);
            }
        }
        for( i <- 1 to 16){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setPadding(5)
          cell.setBorder(Rectangle.NO_BORDER);
          table_toothchart.addCell(cell);
        }

        //add blank row
        table_toothchart.addCell(blankRowCell);

        for (i <- (17 to 32).reverse){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setPadding(5)
          cell.setBorder(Rectangle.NO_BORDER);
          table_toothchart.addCell(cell);
        }

        

        //add blank row
        table_toothchart.addCell(blankRowCell);

        for (i <- (17 to 32).reverse){
          var toothConditions=toothConditionMap(i).split(",")
          if (toothConditions contains "MT"){
              var cell = new PdfPCell(new Phrase(""))
              cell.setBorder(Rectangle.NO_BORDER);
              table_toothchart.addCell(cell);
          }else{
            var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/top-view"+i+".png"
            var image1 = Image.getInstance(imageURL)
            
            if (toothConditions contains "DT"){
              imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-top-view"+i+".png"
              image1 = Image.getInstance(imageURL)
            }
            image1.scalePercent(imageScalePercentage)
            var cell = new PdfPCell(image1);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            table_toothchart.addCell(cell);
          }  
        }
        
        //add blank row
        table_toothchart.addCell(blankRowCell);

        for (i <- (17 to 32).reverse){
            var toothConditions=toothConditionMap(i).split(",")
            if (toothConditions contains "MT"){
                var cell = new PdfPCell(new Phrase(""))
                cell.setBorder(Rectangle.NO_BORDER);
                table_toothchart.addCell(cell);
            }else{
              var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/side-view"+i+".png"
              var image1 = Image.getInstance(imageURL)
              
              if (toothConditions contains "DT"){
                imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-side-view"+i+".png"
                image1 = Image.getInstance(imageURL)
              }
              image1.scalePercent(imageScalePercentage)
              var cell = new PdfPCell(image1);
              cell.setBorder(Rectangle.NO_BORDER);
              cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
              table_toothchart.addCell(cell);
            }   
        }

        //add blank row
        table_toothchart.addCell(blankRowCell);
        
        //show bottom row tooth numbers
       for( i <- 17 to 32){
          var cell = new PdfPCell(new Phrase(i.toString))
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          cell.setBorderColor(BaseColor.GRAY);
          cell.setPadding(10)
          table_toothchart.addCell(cell);
        }
        //Show conditions for bottom row
        for (i <- (17 to 32).reverse){
          if (toothConditionMap(i)!=""){
            var toothConditions=toothConditionMap(i).split(",")
            var cell = new PdfPCell()
            cell.setMinimumHeight(100)    
            for(toothCondition<-toothConditions){
                var para = new Paragraph(toothCondition, font);
                para.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(para);
            }
            cell.setPadding(10)
            
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            table_toothchart.addCell(cell);  
          }else{
            var cell = new PdfPCell(new Phrase(""))
            cell.setMinimumHeight(100)    
            cell.setBorderColor(BaseColor.GRAY);
            cell.setPadding(5)
            table_toothchart.addCell(cell);  
          }      
        }
        
       
        
      }
    }
    
    val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    table_toothchart    
  } 


  def pdf(id: Long, step: Option[String]) = silhouette.SecuredAction.async { implicit request =>
    val skipFormList=List("1I","1L","1M","2H","3G","3I","1N","1O","2D","2D1","3D","3C")
    var showFormsList = List("1A","1H","1G")
    
    if (step.getOrElse("2J") == "2C") {
      showFormsList = List("1A","1H","1G","2C")
     } 

    db.run {
      TreatmentTable.filter(_.id === id).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case None =>
          DBIO.successful(NotFound)
        case Some(treatment) =>
          StepTable.filter(_.treatmentId === id).sortBy(_.dateCreated).result.flatMap[Result, NoStream, Effect.Read] { stepRows =>
            for {
              dentist <- StaffTable.filter(_.id === treatment.staffId).result.head
              forms <- FormTable.filter(_.id inSet showFormsList).sortBy(t => (t.id.asc, t.version.asc)).result
              clinicalNotes <- ClinicalNotesTable.filter(_.patientId === id).sortBy(_.note).result
              measurementHistory <- MeasurementHistoryTable.filter(_.patientId === id).sortBy(_.dateCreated.desc).result
            } yield {
              
              val f = step match {
                case Some(desiredStep) if desiredStep == "2C" =>
                  forms.filter(step => step.id == desiredStep || step.id == "1H")
                    .map(form => if(form.id == "1H") FormRow(form.id, form.version, "Patient’s Photographs", form.title, form.icon, form.schema, form.print, form.listOrder, form.types) else form)
                case Some(desiredStep) =>
                  forms.filter(_.id == desiredStep)
                case _ =>
                  forms.filterNot(_.id == "2J")
              }

              var schema = f.groupBy(_.section).map {
                case (title, sectionForms) =>
                  val finalForms = sectionForms.map { form =>
                    Output.Form(form.id, form.version, form.title, form.icon, form.schema, form.print, form.listOrder,form.types)
                  }

                  Output.Section(title, finalForms.toList)
              }.toList.sortBy(_.forms.head.id)

              var is2C = false;
              if(step.isDefined && step.get == "2C") { 
                is2C = true;
                schema = schema.reverse
              }  

              val source = StreamConverters.fromInputStream { () =>
                val table_notes = new PdfPTable(1)
                var columnWidths:Array[Float] = Array(40,15,15,15,15);
                var table_measurement: PdfPTable = new PdfPTable(columnWidths);
                table_measurement.setWidthPercentage(100);
                val document = new Document(PageSize.A3)
                val inputStream = new PipedInputStream()
                val outputStream = new PipedOutputStream(inputStream)
                val writer = PdfWriter.getInstance(document, outputStream)

                writer.setStrictImageSequence(true)

                val event = new HeaderFooterPageEvent()
                writer.setPageEvent(event)
                document.setMargins(10, 10, 10 , 40)
                Future({
                  document.open()

                  // Metadata
                  val FONT_COVER_PAGE = FontFactory.getFont(FontFactory.HELVETICA, 29)
                  val FONT_H1 = FontFactory.getFont(FontFactory.HELVETICA, 23)
                  val FONT_H2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 15)
                  val FONT_BODY = FontFactory.getFont(FontFactory.HELVETICA, 13)
                  val FONT_SMALL = FontFactory.getFont(FontFactory.HELVETICA, 10)
                  val FONT_BODY_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 13)
                  
                  FONT_BODY_TITLE.setColor(BaseColor.GRAY)

                  // Logo
                  try {
                    val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
                    val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
                    val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
                    val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
                    val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
                    val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
                    val g = img.createGraphics()
                    g.drawRenderedImage(bi, at)
                    val imgBytes = new ByteArrayOutputStream()
                    ImageIO.write(img, "PNG", imgBytes)
                    val image = Image.getInstance(imgBytes.toByteArray)
                    image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
                    image.setCompressionLevel(PdfStream.NO_COMPRESSION)
                    image.setAlignment(Image.MIDDLE)
                    document.add(image)
                  } catch {
                    case error: Throwable => logger.debug(error.toString)
                  }
                  
                  var c1FormData = stepRows.filter(row => row.formId == "1A" && row.formVersion == 1 &&
                                                row.state == StepRow.State.Completed).last

                  val patientFirstName = convertOptionString((c1FormData.value \ "full_name").asOpt[String])
                  val patientLastName = convertOptionString((c1FormData.value \ "last_name").asOpt[String])
                  val patientName = patientFirstName +" "+ patientLastName
                  
                  val title = if (step.getOrElse("2J") == "2C") "Smile Analysis and Design Report" else "Evaluation & Diagnostic Report"
                  document.addTitle(s"$patientName, $title")
                  document.addSubject(s"$title")
                  document.addAuthor(s"${dentist.name}")
                  document.addCreator("Novadontics")
                  // Title and patient details 
                  val dateTimeFormat = DateTimeFormat.mediumDate()
                  document.add(new Paragraph(64, s"$title", FONT_COVER_PAGE))
                  document.add(new Paragraph(32, s"Patient ID: ${treatment.patientId}", FONT_BODY))
                  document.add(new Paragraph(s"Name: $patientName", FONT_BODY))
                  document.add(new Paragraph(s"Email: ${treatment.emailAddress}", FONT_BODY))
                  document.add(new Paragraph(s"Phone: ${treatment.phoneNumber}", FONT_BODY))
                  // Steps…
                  var nextImageNumber = 1
                  var noteStr = ""
                  var dateCreatedStr = ""
                  var systolicStr = ""
                  var diastolicStr = ""
                  var pulseStr = ""
                  var oxygenStr = ""
                  var bodyTempStr = ""
                  var glucoseStr = ""
                  var a1cStr = ""
                  var inrStr = ""
                  var ptStr = ""

                  val pageWidth = PageSize.A3.getWidth.asInstanceOf[Int]

                  val pageHeight = PageSize.A3.getHeight.asInstanceOf[Int]


                  schema.take(3).foreach { section =>
                    
                    if (is2C && section.title == "Visit Two - Data Analysis"){
                      //don't include title 
                    }else if (is2C && section.title == "Personal & Diagnostic Data"){
                      //don't include title
                      document.add(new Paragraph("\n"));
                    }else{
                      document.add(new Paragraph(64, section.title, FONT_H1))
                    }
                    section.forms.foreach { form =>
                      if (is2C && form.id =="2C"){
                         //don't include title 
                      }else{
                        document.add(new Paragraph(32, form.title, FONT_H2))
                      }
                      val s = stepRows.filter(row => row.formId == form.id && row.formVersion == form.version &&
                                                row.state == StepRow.State.Completed)
                      if (s.isEmpty) {
                        document.add(new Paragraph(24, "No data yet.", FONT_BODY_TITLE))
                      } else {
                        val stepRow = s.last
                        logger.debug(s"Rendering ${stepRow.formId}")
                        document.add(new Phrase())
                        try {
                          var pictures = List.empty[(Int, String)]


                          var pictureMaxWidths:scala.collection.mutable.Map[Int,Int]=scala.collection.mutable.Map()
                          var pictureMaxHeights:scala.collection.mutable.Map[Int,Int]=scala.collection.mutable.Map()

                          val table = new PdfPTable(2)
                          table.setPaddingTop(20)
                          table.setWidthPercentage(95)

                          form.schema.as[List[JsValue]].foreach { entry =>

                            (entry \ "rows").as[List[JsValue]].foreach { row =>
                              (row \ "columns").as[List[JsValue]].foreach { column =>
                                (column \ "columnItems").as[List[JsValue]].foreach { columnItem =>
                                  def renderView(prefix: Option[String])(columnItem: JsValue): Unit = {
                                    var fieldType = (columnItem \ "field" \ "type").asOpt[String]

                                    if(form.id == "1G"){
                                      val cName = (columnItem \ "field" \ "name").asOpt[String]
                                      val cTitle = prefix.map(_ + ": ").getOrElse("") + (columnItem \ "field" \ "title").as[String]
                                      val cImages = (Json.parse((stepRow.value \ cName.getOrElse("")).asOpt[String].getOrElse("{}").replaceAll("★", "\"")) \ "images").asOpt[List[String]].getOrElse(List.empty)

                                      if(!cImages.isEmpty){

                                          fieldType = Option("Images")
                                      }

                                    }

                                    fieldType match {
                                      case Some("Images") =>
                                        val name = (columnItem \ "field" \ "name").asOpt[String]
                                        val title = prefix.map(_ + ": ").getOrElse("") + (columnItem \ "field" \ "title").as[String]
                                        val vImages = (Json.parse((stepRow.value \ name.getOrElse("")).asOpt[String].getOrElse("{}").replaceAll("★", "\"")) \ "images").asOpt[List[String]].getOrElse(List.empty)
                                        var images = List[String]()
                                        var pdfs = List[String]()
                                        var vpdfs = new ListBuffer[String]()
                                        var vimgs = new ListBuffer[String]()

                                        for(i<-0 to vImages.size-1){
                                          val img = vImages(i)
                                          if(img contains ".pdf"){
                                            vpdfs += img
                                          }else{
                                            vimgs += img
                                          }
                                        }
                                        pdfs = vpdfs.toList
                                        images = vimgs.toList
                                        table.addCell(basicCell(title, FONT_BODY_TITLE))
                                        if (images.isEmpty) {
                                          var phrase = new Phrase("No pictures taken.", FONT_BODY)
                                          updatePdfLinks(phrase,vpdfs)
                                          val cell = new PdfPCell(phrase)
                                          cell.setBorder(Rectangle.BOTTOM)
                                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                          cell.setPadding(10)
                                          table.addCell(cell)
                                        } else if (images.size == 1) {
                                          var phrase = new Phrase(s"See Picture $nextImageNumber.", FONT_BODY)
                                          updatePdfLinks(phrase,vpdfs)
                                          val cell = new PdfPCell(phrase)
                                          cell.setBorder(Rectangle.BOTTOM)
                                          cell.setBorderColor(BaseColor.LIGHT_GRAY)
                                          cell.setPadding(10)
                                          table.addCell(cell)

                                          pictures = pictures :+ (nextImageNumber, images.head)
                                          if (convertOptionString(name)=="patient_photo"){
                                            pictureMaxWidths.update(nextImageNumber,300)
                                            pictureMaxHeights.update(nextImageNumber,300)
                                          }else{
                                            pictureMaxWidths.update(nextImageNumber,pageWidth)
                                            pictureMaxHeights.update(nextImageNumber,pageHeight)
                                          }

                                          nextImageNumber += 1
                                        } else {
                                          val imageNumbers = Range(nextImageNumber, nextImageNumber + images.size)
                                          for (i<-nextImageNumber to nextImageNumber+images.size){
                                              pictureMaxWidths.update(i,pageWidth)
                                              pictureMaxHeights.update(i,pageHeight)
                                          }

                                          val imageNumbersString = imageNumbers.reverse.toList.map(_.toString) match {
                                            case head :: tail =>
                                              tail.reverse.reduce(_ + ", " + _) + ", and " + head
                                            case Nil => ""
                                          }

                                          var pic = s"See Pictures $imageNumbersString."
                                          var phrase = new Phrase(pic)
                                          updatePdfLinks(phrase,vpdfs)
                                          table.addCell(phrase)

                                          pictures = pictures ++ imageNumbers.zip(images)
                                          nextImageNumber += images.size
                                        }
                                      case Some("Input") =>
                                        val name = (columnItem \ "field" \ "name").asOpt[String]
                                        val title = prefix.map(_ + ": ").getOrElse("") + (columnItem \ "field" \ "title").as[String]
                                        val value = (stepRow.value \ name.getOrElse("")).asOpt[String].getOrElse("")
                                        val postfix = (columnItem \ "field" \ "postfix").asOpt[String]
                                        table.addCell(basicCell(title, FONT_BODY_TITLE))
                                        table.addCell(basicCell(Seq(Some(value), postfix).flatten.reduceLeft(_ + ", " + _), FONT_BODY))
                                      case Some("Checkbox") =>
                                        val name = (columnItem \ "field" \ "name").asOpt[String]
                                        val title = prefix.map(_ + ": ").getOrElse("") + (columnItem \ "field" \ "title").as[String]
                                        val value = Json.parse((stepRow.value \ name.getOrElse("")).asOpt[String].getOrElse("{}").replaceAll("★", "\""))

                                        val selected = (value \ "selectedOption").asOpt[String].getOrElse("")
                                        val notes = (value \ "notes").asOpt[String].getOrElse("")
                                        val hasOptions = (columnItem \ "field" \ "attributes" \ "options").toOption.isDefined
                                        val valueString = if(selected != "" && selected != "other") selected
                                         else if (notes != "") notes
                                         else if (!hasOptions)
                                          if ((value \ "checked").asOpt[Boolean].getOrElse(false)) "Yes" else "No"
                                        else ""
                                        table.addCell(basicCell(title, FONT_BODY_TITLE))
                                        table.addCell(basicCell(valueString, FONT_BODY))
                                      case Some("Select") =>
                                        val name = (columnItem \ "field" \ "name").asOpt[String]
                                        val title = prefix.map(_ + ": ").getOrElse("") + (columnItem \ "field" \ "title").as[String]
                                        table.addCell(basicCell(title, FONT_BODY_TITLE))
                                        table.addCell(basicCell((stepRow.value \ name.getOrElse("")).asOpt[String].getOrElse(""), FONT_BODY))
                                      case Some("PrintButton") =>
                                      case Some("InfoBlock") =>
                                      case Some("WebForm") =>
                                      case None =>
                                        val group = (columnItem \ "group").asOpt[JsValue]
                                        var title = group.flatMap(g => (g \ "title").asOpt[String].map(g => prefix.map(_ + ": ").getOrElse("") + g))
                                        
                                        val label = group.flatMap(g => (g \ "label").asOpt[String].map(g => prefix.map(_ + ": ").getOrElse("") + g))
                                        
                                        if (title == Some("") && label!= None){
                                          title = label
                                        }
                                        
                                        val fields = group.flatMap(g => (g \ "fields").asOpt[List[JsValue]])
                                        
                                        //select appropriate value if radio group
                                      group.flatMap(g => (g \ "radio").asOpt[Boolean]) match {
                                        case Some(true) =>
                                          val groupName = group.flatMap(g => (g \ "name").asOpt[String])
                                          val value = Json.parse((stepRow.value \ groupName.getOrElse("")).asOpt[String].getOrElse("{}").replaceAll("★", "\""))
                                          val selected = (value \ "selectedRadioTitle").asOpt[String].getOrElse("")
                                          
                                          table.addCell(basicCell(title.getOrElse("Value"), FONT_BODY_TITLE))
                                          
                                          table.addCell(basicCell(selected, FONT_BODY))
                                        case _ => fields.getOrElse(List()).foreach(field => renderView(title)(Json.obj("field" -> field)))
                                      }
                                      case Some(a) =>
                                        logger.warn(s"Unknown field type found… $a")
                                    }
                                  }

                                  renderView(None)(columnItem)
                                }
                              }
                            }
                          }

                          document.add(table)

                          if (pictures.nonEmpty) {
                            document.newPage()
                          }

                          val pics = pictures.par

                          pics.tasksupport = new ForkJoinTaskSupport()

                          pics.map {
                            case (number, imageUrl) =>
                              var image = Image.getInstance(imageUrl)

                              if (image.getWidth>pictureMaxWidths(number) || image.getHeight>pictureMaxHeights(number)){
                                   image.scaleToFit(pictureMaxWidths(number)-100, pictureMaxHeights(number)-100);
                              }
                              (number, image)
                          }.seq.foreach {
                            case (number, image) =>
                              try {
                                image.setAlignment(Image.MIDDLE)
                                document.add(image)
                                val p = new Paragraph(s"Picture $number", FONT_BODY)
                                p.setAlignment(Element.ALIGN_CENTER)
                                document.add(p)
                              } catch {
                                case e: Throwable =>
                                  logger.warn(s"Failed to render Picture $number", e)
                              }
                          }
                        } catch {
                          case e: Throwable =>
                            logger.warn(s"Failed to render form ${form.title}", e)
                        }
                      }
                    }
                  }
                  
           
                  
                  
            if(!is2C) { //skipping for 2C
                  document.add( Chunk.NEWLINE );
                 
                  document.newPage()
                  
                  //Add tooth chart
                  document.add(new Paragraph(64, "Tooth Chart", FONT_H1))
                  
                  val table_toothchart = generateToothChart(id)
                   //val f:Float = 6
                   
                  document.add(table_toothchart)
                  
                  document.newPage()
                  //Add perio chart
                  document.add(new Paragraph(64, "Perio Chart", FONT_H1))
                  document.add(new Paragraph(64, "B: Bleeding, F: Furcation Class, R: Gum Recession, P: Perio Pocket Depth", FONT_BODY))

                  val table_periochart = generatePerioChart(id)
                  document.add(table_periochart)
              }    
                  
                  document.close()
                  
                  //Close the document
                  logger.debug("Done rendering")
                  
                })(ioBoundExecutor)

                inputStream
              }

              val filename = stepRows.find(_.formId == "1A").flatMap(r => (r.value \ "full_name").asOpt[String]).getOrElse(treatment.emailAddress.replace("@", " at "))
              val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

              Result(
                header = ResponseHeader(200, Map.empty),
                body = HttpEntity.Streamed(source, None, Some("application/pdf"))
              ).withHeaders(
                "Content-Disposition" -> contentDisposition
              )
            }
          }
      }
    }
  }
  
  
  
  
  
  /* this is for test tooth images */
  def testpdf(id: Long, step: Option[String]) = silhouette.SecuredAction.async { implicit request =>
    val skipFormList=List("1L","1M")
    
    db.run {
      TreatmentTable.filter(_.id === id).result.headOption.flatMap[Result, NoStream, Effect.Read] {
        case None =>
          DBIO.successful(NotFound)
        case Some(treatment) =>
        
        for {
              
              measurementHistory <- MeasurementHistoryTable.filter(_.patientId === id).sortBy(_.dateCreated.desc).result
            } yield {
          
              val source = StreamConverters.fromInputStream { () =>
                
                
                
                val document = new Document(PageSize.A3)
                val inputStream = new PipedInputStream()
                val outputStream = new PipedOutputStream(inputStream)
                val writer = PdfWriter.getInstance(document, outputStream)
                var imageScalePercentage = 60

                writer.setStrictImageSequence(true)

                val event = new HeaderFooterPageEvent()
                writer.setPageEvent(event)
                document.setMargins(40, 40, 40 , 60)
                Future({
                  document.open()
                  var columnWidths:Array[Float] = Array(6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6);
                  
                  var table_toothchart: PdfPTable = new PdfPTable(columnWidths);
                  for( i <- 1 to 16){
                    
                    var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/side-view"+i+".png"
                    var image1 = Image.getInstance(imageURL)
                    image1.scalePercent(imageScalePercentage)
                    var cell = new PdfPCell(image1);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                    table_toothchart.addCell(cell);
                    
                  }  
                  for( i <- 1 to 16){
                    
                    var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-side-view"+i+".png"
                    var image1 = Image.getInstance(imageURL)
                    image1.scalePercent(imageScalePercentage)
                    var cell = new PdfPCell(image1);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                    table_toothchart.addCell(cell);
                    
                  }  
                  for( i <- 1 to 16){
                    
                    var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/baby-top-view"+i+".png"
                    var image1 = Image.getInstance(imageURL)
                    image1.scalePercent(imageScalePercentage)
                    var cell = new PdfPCell(image1);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                    table_toothchart.addCell(cell);
                    
                  }  
                  for( i <- 1 to 16){
                    
                    var imageURL="https://s3.amazonaws.com/static.novadonticsllc.com/img/toothChart/top-view"+i+".png"
                    var image1 = Image.getInstance(imageURL)
                    image1.scalePercent(imageScalePercentage)
                    var cell = new PdfPCell(image1);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                    table_toothchart.addCell(cell);
                    
                  }  
                  document.add(table_toothchart)
               
                  
                  document.close()
                  
                  //Close the document
                  logger.debug("Done rendering")
                  
                })(ioBoundExecutor)

                inputStream
              }

              val filename = "test"
              val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

              Result(
                header = ResponseHeader(200, Map.empty),
                body = HttpEntity.Streamed(source, None, Some("application/pdf"))
              ).withHeaders(
                "Content-Disposition" -> contentDisposition
              )
          }
      }
    }
  }

def migrateTreatment = silhouette.SecuredAction.async(parse.json) { request =>
      db.run {
        for{
         treatment <- TreatmentTable.filterNot(_.deleted).result
        } yield {
            (treatment).foreach( treatments=> {
                var count = db.run{
                  StepTable.filter(_.treatmentId === treatments.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result
                } map{ steps=>

                   val patient_referer = steps.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).flatMap(step => (step.value \ "patient_referer").asOpt[String])
                   patient_referer.map{result=>
                      if(result != null && result != ""){
                         var temp = result.toString.replace('★', '"')
                         val jsonObject: JsValue = Json.parse(temp)
                         var selectedOption: String = convertOptionString((jsonObject \ ("selectedOption")).asOpt[String])
                        db.run{
                            TreatmentTable.filter(_.id === treatments.id).map(_.referralSource).update(Some(referralSource(selectedOption)))
                        }map{step =>
                              Ok
                        }
                      }
                   }
                }
                val AwaitResult = Await.ready(count, atMost = scala.concurrent.duration.Duration(60, SECONDS))

              var treatmentStarted: Boolean = false
                //Treatment Started
                db.run{
                  StepTable.filter(_.treatmentId === treatments.id).filter(_.formId === "2D1").sortBy(_.dateCreated.desc).result
                }map{stepResult =>
                  val treatment = stepResult.groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).flatMap(step => (step.value \ "treatment").asOpt[String])
                  treatment.map{ treatment =>

                      if(treatment != null && treatment != ""){
                         var temp = treatment.toString.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         var selectedOption: String = convertOptionString((jsonObject1 \ ("selectedRadio")).asOpt[String])
                        if(selectedOption != null && selectedOption != ""){
                          treatmentStarted = true
                      }
                    }
                      if(!treatmentStarted){
                       var count1 =  db.run{
                            TreatmentPlanTable.filterNot(_.deleted).filter(_.id === treatments.id).length.result
                        } map { treatmentPlan =>
                            if(treatmentPlan > 0){
                              treatmentStarted = true
                            }
                        }
                  val AwaitResult = Await.ready(count1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                  }

                      if(treatmentStarted) {
                        db.run{
                            TreatmentTable.filterNot(_.deleted).filter(_.id === treatments.id).map(_.treatmentStarted).update(treatmentStarted)
                        }map{step =>
                            Ok
                        }
                      }
                  }
                }
            })
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Form Migrated successfully"
            )
          }
        }
     }
  }

    def recallPeriod (id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val vendor = for {
      recallPeriod <- (request.body \ "recallPeriod").validateOpt[String]
      recallType <- (request.body \ "recallType").validateOpt[String]
      recallCode <- (request.body \ "recallCode").validateOpt[String]
      recallCodeDescription <- (request.body \ "recallCodeDescription").validateOpt[String]
      intervalNumber <- (request.body \ "intervalNumber").validateOpt[Long]
      intervalUnit <- (request.body \ "intervalUnit").validateOpt[String]
      recallFromDate <- (request.body \ "recallFromDate").validateOpt[String]
      recallOneDayPlus <- (request.body \ "recallOneDayPlus").validateOpt[Boolean]
      recallDueDate <- (request.body \ "recallDueDate").validateOpt[String]
      recallNote <- (request.body \ "recallNote").validateOpt[String]
    } yield {

      var vRecallFromDate: Option[LocalDate] = None
      if(recallFromDate != None && recallFromDate != Some("")){
        vRecallFromDate = Some(LocalDate.parse(convertOptionString(recallFromDate)))
      }

      var vRecallDueDate: Option[LocalDate] = None
      if(recallDueDate != None && recallDueDate != Some("")){
        vRecallDueDate = Some(LocalDate.parse(convertOptionString(recallDueDate)))
      }
      (recallPeriod,recallType,recallCode,recallCodeDescription,intervalNumber,intervalUnit,vRecallFromDate,recallOneDayPlus,vRecallDueDate,recallNote)
    }

    vendor match {
      case JsSuccess((recallPeriod,recallType,recallCode,recallCodeDescription,intervalNumber,intervalUnit,vRecallFromDate,recallOneDayPlus,vRecallDueDate,recallNote), _) => db.run {
        val query = TreatmentTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(recallPeriod).map(value => query.map(_.recallPeriod).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recallType).map(value => query.map(_.recallType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recallCode).map(value => query.map(_.recallCode).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recallCodeDescription).map(value => query.map(_.recallCodeDescription).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(intervalNumber).map(value => query.map(_.intervalNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(intervalUnit).map(value => query.map(_.intervalUnit).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(vRecallFromDate).map(value => query.map(_.recallFromDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recallOneDayPlus).map(value => query.map(_.recallOneDayPlus).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(vRecallDueDate).map(value => query.map(_.recallDueDate).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(recallNote).map(value => query.map(_.recallNote).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
            ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def newPatientsList(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  val FromDate = DateTime.parse(convertOptionString(fromDate))
  val ToDate = DateTime.parse(convertOptionString(toDate))

      db.run{
          TreatmentTable.filterNot(_.deleted).filter(s => s.firstVisit >= FromDate && s.firstVisit <= ToDate).filter(_.practiceId === practiceId)
          .join(ProviderTable).on(_.providerId === _.id)
          .join(StepTable).on(_._1.id === _.treatmentId).result

      } map { treatments =>
        val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val provider = entries.head._1._2
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
      var email =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
      var address =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))
      if(phoneNumber == "" || phoneNumber == null){
        phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1L").flatMap(step => (step.value \ "phone").asOpt[String]))
      }

      var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
      var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
      val patientName = firstName+" "+lastName
      val providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

      var firstVisit = ""
      if(entry.firstVisit != "" && entry.firstVisit != null && entry.firstVisit != None){
        firstVisit = new DateTime(entry.firstVisit.get).toString("yyyy-MM-dd")
      }
          NewPatientListRow(
            firstVisit.toString,
            patientName,
            phoneNumber,
            email,
            address,
            providerName,
            convertOptionString(entry.referralSource)
          )
      }.toList.sortBy(_.dateOfFirstVisit)
      Ok(Json.toJson(data))
      }
  }

def generateNewPatientPDF(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  val FromDate = DateTime.parse(convertOptionString(fromDate))
  val ToDate = DateTime.parse(convertOptionString(toDate))

      db.run{
          TreatmentTable.filterNot(_.deleted).filter(s => s.firstVisit >= FromDate && s.firstVisit <= ToDate).filter(_.practiceId === practiceId)
          .join(ProviderTable).on(_.providerId === _.id)
          .join(StepTable).on(_._1.id === _.treatmentId).result

      } map { treatments =>

        val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val provider = entries.head._1._2
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

      var phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
      var email =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
      var address =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))

      if(phoneNumber == "" || phoneNumber == null){
        phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1L").flatMap(step => (step.value \ "phone").asOpt[String]))
      }

      var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
      var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
      val patientName = firstName+" "+lastName
      val providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"

      var firstVisit = ""
      if(entry.firstVisit != "" && entry.firstVisit != null && entry.firstVisit != None){
        firstVisit = new DateTime(entry.firstVisit.get).toString("yyyy-MM-dd")
      }

          NewPatientListRow(
            firstVisit.toString,
            patientName,
            phoneNumber,
            email ,
            address,
            providerName,
            convertOptionString(entry.referralSource)
          )
      }.toList.sortBy(_.dateOfFirstVisit)

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {

          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL


          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);

          var cell = new PdfPCell(new Phrase("New Patient Report", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

        var cell1 = new PdfPCell
        var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
        var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

        if(fromDate != toDate){
          cell1 = new PdfPCell(new Phrase("From Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);

          var cell2 = new PdfPCell(new Phrase("To Date     : "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_TITLE1));
          cell2.setBorder(Rectangle.NO_BORDER);
          cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell2);
        } else {
          cell1 = new PdfPCell(new Phrase("Date : "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);
        }
          document.add(headerTextTable1);

          var rowsPerPage = 8;
          var recNum =  0;

          if(treatments.length > 0){

            (data).foreach(row => {
            var sampleData = List(s"${row.dateOfFirstVisit}", s"${row.patientName}", s"${row.phoneNumber}",s"${row.email}",s"${row.address}",s"${row.providerName}",s"${row.referredBy}")

            var columnWidths:Array[Float] = Array(16,14,14,16,14,12,14);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date of first visit", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                //cell.setPaddingLeft(5)
                cell.setPadding(13);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_LEFT);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
            })

            document.add(table);

            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 10
            }
          })
          } else {

            var columnWidths:Array[Float] = Array(14,14,14,14,14,14,16);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Date of first visit", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Referred By", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              document.add(table);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(97);
          var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);
          }

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"New patients Report"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

def generateNewPatientCSV(fromDate: Option[String],toDate: Option[String]) = silhouette.SecuredAction { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  val FromDate = DateTime.parse(convertOptionString(fromDate))
  val ToDate = DateTime.parse(convertOptionString(toDate))
  var csvList = List.empty[NewPatientListRow]

       var block1 = db.run{
          TreatmentTable.filterNot(_.deleted).filter(s => s.firstVisit >= FromDate && s.firstVisit <= ToDate).filter(_.practiceId === practiceId)
          .join(ProviderTable).on(_.providerId === _.id)
          .join(StepTable).on(_._1.id === _.treatmentId).result

      } map { treatments =>

        val data = treatments.groupBy(_._1._1.id).map {
          case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val provider = entries.head._1._2
          val steps = entries.map(_._2).groupBy(_.formId).map(_._2.maxBy(_.dateCreated)).toList.sortBy(_.formId)

          var phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "phone").asOpt[String]))
          var email =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "email").asOpt[String]))
          var address =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "street").asOpt[String]))

          if(phoneNumber == "" || phoneNumber == null){
            phoneNumber =  convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1L").flatMap(step => (step.value \ "phone").asOpt[String]))
          }

          var firstName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String]))
          var lastName = convertOptionString(steps.sortBy(_.dateCreated).reverse.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
          val patientName = firstName+" "+lastName
          val providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

      var firstVisit = ""
      if(entry.firstVisit != "" && entry.firstVisit != null && entry.firstVisit != None){
        firstVisit = new DateTime(entry.firstVisit.get).toString("yyyy-MM-dd")
      }

            var dataList = NewPatientListRow(firstVisit.toString,patientName,phoneNumber,email,address,providerName,convertOptionString(entry.referralSource))
            csvList = csvList :+ dataList
              }
          }
        val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))

    val file = new File("/tmp/new patients.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Date of first visit", "Patient Name", "Phone Number","Email","Address","Provider","Referred By"))
    csvList.sortBy(_.dateOfFirstVisit).map {
      newPatientList => writer.writeRow(List(newPatientList.dateOfFirstVisit,newPatientList.patientName,newPatientList.phoneNumber,newPatientList.email,newPatientList.address,newPatientList.providerName,newPatientList.referredBy ))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    }

def referredToList(fromDate: Option[String],toDate: Option[String],referredTo: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var dataList = List.empty[ReferredToRow]
  var query = TreatmentTable.filterNot(_.deleted)
  if(referredTo != None && convertOptionString(referredTo).toLowerCase != "all"){
      query = query.filter(_.referredTo === convertStringToLong(referredTo))
  }
    db.run {
      query.filter(s => s.referredDate >= DateTime.parse(convertOptionString(fromDate)) && s.referredDate <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59)).filter(_.practiceId === practiceId).result
    } map { referralSourcedata =>
      val data = referralSourcedata.groupBy(_.referredTo).mapValues(_.size)
      data.map{result=>
        val refererId  = convertOptionLong(result._1)
        val size  = result._2
        var referer = ""
        var block1 = db.run{
          ReferredToTable.filter(_.id === refererId).result
        } map { result=>
          result.map{referred =>
            referer = referred.name
          var data1 = ReferredToRow(Some(refererId),referer,size,"","","","")
          dataList = dataList :+ data1
          }
      }
      val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(20, SECONDS))
     }
     Ok(Json.toJson(dataList.sortBy(_.size).reverse))
    }
}


def viewReferredToList(fromDate: Option[String],toDate: Option[String],referredTo: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var query = TreatmentTable.filterNot(_.deleted)
  if(referredTo != None && convertOptionString(referredTo).toLowerCase != "all"){
      query = query.filter(_.referredTo === convertStringToLong(referredTo))
  }
    db.run {
      query.filter(s => s.referredDate >= DateTime.parse(convertOptionString(fromDate)) && s.referredDate <= DateTime.parse(convertOptionString(toDate))).filter(_.practiceId === practiceId).join(ReferredToTable).on(_.referredTo === _.id).join(ProviderTable).on(_._1.providerId === _.id).result
    } map { referralSourcedata =>
      val data = referralSourcedata.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val treatment = entries.head._1._1
          val referredTo = entries.head._1._2
          val provider = entries.head._2

          var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
          var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"

          ReferredToRow(treatment.referredTo,referredTo.name,0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
     }.toList.sortBy(_.referredTo)
     Ok(Json.toJson(data))
    }
}


def generateReferredToPdf(fromDate: Option[String],toDate: Option[String],referredTo: Option[String])=silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var dataList = List.empty[ReferredToRow]
  var query = TreatmentTable.filterNot(_.deleted)
  if(referredTo != None && convertOptionString(referredTo).toLowerCase != "all"){
      query = query.filter(_.referredTo === convertStringToLong(referredTo))
  }

    db.run {
      query.filter(s => s.referredDate >= DateTime.parse(convertOptionString(fromDate)) && s.referredDate <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59)).filter(_.practiceId === practiceId).join(ReferredToTable).on(_.referredTo === _.id).join(ProviderTable).on(_._1.providerId === _.id).result
    } map { referralSourcedata =>
      // val data = referralSourcedata.groupBy(_.referredTo).mapValues(_.size)
      val data = referralSourcedata.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val treatment = entries.head._1._1
          val referred = entries.head._1._2
          val provider = entries.head._2

          var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
          var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"

          ReferredToRow(treatment.referredTo,referred.name,0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
        }.toList
      val sample = data.sortBy(_.referredTo)

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }



        val document = new Document(PageSize.A4,0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


          var cell = new PdfPCell(new Phrase("Referred To Report", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var cell1 = new PdfPCell

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
          var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

          if(fromDate != toDate){
            cell1 = new PdfPCell(new Phrase(" Referred Date From "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) + " To "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          } else {
            cell1 = new PdfPCell(new Phrase("Referred Date "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;

          if(sample.length != 0){
            (sample).foreach(row => {
              var sampleData = List(s"${row.patientName}", s"${row.phone}", s"${row.email}",s"${row.referredTo}", s"${row.providerName}")
              var i=1

              var columnWidths:Array[Float] = Array(20,20,20,20,20);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("ReferredTo", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {
                if(item != ""){
                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  cell.setPaddingLeft(5);
                  cell.setPaddingTop(16);
                  cell.setPaddingBottom(15);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
                }
              })

              document.add(table);
              recNum = recNum+1;
              if(recNum == 0 ) {
                document.newPage();
              }

            })
          } else {
            var columnWidths:Array[Float] = Array(20,20,20,20,20);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 ) {

              document.add(new Paragraph("\n"));

            cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)
            cell = new PdfPCell(new Phrase("Phone", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)
            cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)
            cell = new PdfPCell(new Phrase("ReferredTo", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)
            cell = new PdfPCell(new Phrase("Provider Name", FONT_COL_HEADER));
            cell.setPadding(5);
            cell.setPaddingBottom(16);
            cell.setBackgroundColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell)

            document.add(table);

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);
            }
          }
          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"referredToReport"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
     
     
    }   
}

def generateReferredToCsv(fromDate: Option[String],toDate: Option[String],referredTo: Option[String])=silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var csvList = List.empty[ReferredToRow]
  var query = TreatmentTable.filterNot(_.deleted)
  if(referredTo != None && convertOptionString(referredTo).toLowerCase != "all"){
      query = query.filter(_.referredTo === convertStringToLong(referredTo))
  }

  db.run {
      query.filter(s => s.referredDate >= DateTime.parse(convertOptionString(fromDate)) && s.referredDate <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59)).filter(_.practiceId === practiceId).join(ReferredToTable).on(_.referredTo === _.id).join(ProviderTable).on(_._1.providerId === _.id).result
    } map { referralSourcedata =>
      val data = referralSourcedata.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val treatment = entries.head._1._1
          val referred = entries.head._1._2
          val provider = entries.head._2

          var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
          var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"

          val dataList = ReferredToRow(treatment.referredTo,referred.name,0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
          csvList = csvList :+ dataList
      }
    var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
    var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
    var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));
    val file = new java.io.File("/tmp/referredToReport.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Referred To Report " +new SimpleDateFormat("mm/dd/yyyy").format(FromDate) +" to " +new SimpleDateFormat("mm/dd/yyyy").format(ToDate) ))
    writer.writeRow(List(""))
    writer.writeRow(List("Patient Name","Phone","Email","ReferredTo","Provider"))
    csvList.sortBy(_.size).reverse.map {
      referralList => writer.writeRow(List(referralList.patientName,referralList.phone,referralList.email,referralList.referredTo,referralList.providerName))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    } 
}

  def generateReferralSourcePdf(fromDate: Option[String],toDate: Option[String],referralSource: Option[String])=silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var pdfList = List.empty[ReferralReport]
  var vReferralSource = convertOptionString(referralSource).toLowerCase
  var query = TreatmentTable.filterNot(_.deleted)
  if(vReferralSource != "" && vReferralSource != "all"){
      query = query.filter(_.referralSource.toLowerCase === vReferralSource)
  }

  db.run {
    query
      .filter(s => s.firstVisit >= DateTime.parse(convertOptionString(fromDate)) && s.firstVisit <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59))
      .filter(_.practiceId === practiceId).join(ProviderTable).on(_.providerId === _.id).result
    } map { referralSourcedata =>
      val data = referralSourcedata.groupBy(_._1.id).map {
        case (treatmentId, entries) =>
        val treatment = entries.head._1
        val provider = entries.head._2
        var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
        var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"
        var dataList = ReferralReport(convertOptionString(treatment.referralSource),0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
        pdfList = pdfList :+ dataList
      }
        val sample = pdfList.sortBy(_.referralSource)

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4,0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


          var cell = new PdfPCell(new Phrase("Referral Report", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var cell1 = new PdfPCell

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
          var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

          if(fromDate != toDate){
            cell1 = new PdfPCell(new Phrase(" First Visit From "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) + " To "+new SimpleDateFormat("mm/dd/yyyy").format(ToDate), FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          } else {
            cell1 = new PdfPCell(new Phrase("First Visit Date "+ new SimpleDateFormat("mm/dd/yyyy").format(FromDate) , FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1);
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;

          if(sample.length != 0){
            (sample).foreach(row => {
              var sampleData = List(s"${row.patientName}", s"${row.phone}",s"${row.email}",s"${row.referralSource}",s"${row.providerName}")
              var i=1

              var columnWidths:Array[Float] = Array(20,20,20,20,20);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Referral Source", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {
                if(item != ""){
                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  cell.setPaddingLeft(5);
                  cell.setPaddingTop(16);
                  cell.setPaddingBottom(15);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
                }
              })

              document.add(table);
              recNum = recNum+1;
              if(recNum == 0 ) {
                document.newPage();
              }

            })
          } else {
            var columnWidths:Array[Float] = Array(20,20,20,20,20);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 ) {

              document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Phone", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Referral Source", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

              document.add(table);

              val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
              FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
              var headerTextTable1: PdfPTable = new PdfPTable(1)
              headerTextTable1.setWidthPercentage(97);
              var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1)
              document.add(headerTextTable1);

            }
          }
          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"referralReport"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

  def generateReferralSourceCsv(fromDate: Option[String],toDate: Option[String],referralSource: Option[String])=silhouette.SecuredAction { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var csvList = List.empty[ReferralReport]
    var query = TreatmentTable.filterNot(_.deleted)
    var vReferralSource = convertOptionString(referralSource).toLowerCase
    if(vReferralSource != "" && vReferralSource != "all"){
       query = query.filter(_.referralSource.toLowerCase === vReferralSource)
    }

   var referralReportList = db.run {
      query
        .filter(s => s.firstVisit >= DateTime.parse(convertOptionString(fromDate)) && s.firstVisit <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59)).filter(_.practiceId === practiceId)
        .join(ProviderTable).on(_.providerId === _.id).result
      } map { referralSourcedata =>
      val data = referralSourcedata.groupBy(_._1.id).map {
        case (treatmentId, entries) =>
        val treatment = entries.head._1
        val provider = entries.head._2
        var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
        var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"
        var dataList = ReferralReport(convertOptionString(treatment.referralSource),0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
        csvList = csvList :+ dataList
    }
  }
    val AwaitResult = Await.ready(referralReportList, atMost = scala.concurrent.duration.Duration(120, SECONDS))

    var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
    var  FromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
    var  ToDate: Date = simpleDateFormat.parse(convertOptionString(toDate));
    val file = new java.io.File("/tmp/referralReport.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Referral Report " +new SimpleDateFormat("mm/dd/yyyy").format(FromDate) +" to " +new SimpleDateFormat("mm/dd/yyyy").format(ToDate) ))
    writer.writeRow(List(""))
    writer.writeRow(List("patient Name","Phone","Email","Referral Source","Provider"))
    csvList.sortBy(_.referralSource).map {
      referralList => writer.writeRow(List(referralList.patientName,referralList.phone,referralList.email,referralList.referralSource, referralList.providerName))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
  }

def patientTypePdf(patientType: Option[String]) = silhouette.SecuredAction.async { request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val patientValue = convertOptionString(patientType).split(',')
    var patientName = ""
    var phoneNumber = ""
    var address = ""
    var emailAddress = ""
    var data = List.empty[PatientTypePDF]
    var treatmentIdList = List.empty[Long]

    db.run {
      TreatmentTable.filterNot(_.deleted).filterNot(_.patientType === "").filter(_.practiceId === practiceId)
      .join(ProviderTable).on(_.providerId === _.id).result
      } map { treatmentMap =>
      var value = treatmentMap.groupBy(_._1.id).map {
      case (treatmentId, entries) =>
      val result = entries.head._1
      val provider = entries.head._2
      var patientName = ""
      var phoneNumber = ""
      var address = ""
      var emailAddress = ""
      var firstVisit = ""
      var providerName = ""
         for (v <- patientValue) {
             if(convertOptionString(result.patientType).toLowerCase contains v.toLowerCase){

              val block1 = db.run{
                StepTable.filter(_.formId === "1A").filter(_.treatmentId === result.id).sortBy(_.dateCreated.desc).result.head
              } map { stepMap =>

                    val first_name =  (stepMap.value \ "full_name").as[String]
                    val last_name =  (stepMap.value \ "last_name").as[String]

                    patientName = first_name +" "+ last_name
                    phoneNumber = (stepMap.value \ "phone").as[String]
                    address =  (stepMap.value \ "street").as[String]
                    emailAddress =  (stepMap.value \ "email").as[String]
                    firstVisit = (stepMap.value \ "first_visit").as[String]
                    providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

            }
              val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))

              val treatmentId = convertOptionLong(result.id)

                  if(!treatmentIdList.contains(treatmentId)){
                  var dataList = PatientTypePDF(treatmentId,patientName,firstVisit,result.phoneNumber,address,result.emailAddress,providerName,convertOptionString(result.patientType))
                    data = data :+ dataList
                    treatmentIdList = List(treatmentId)
                }
              }
            }
        }

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {

          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL


          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);

          var cell = new PdfPCell(new Phrase("Patient Type Report", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;
          if(data.length > 0){

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 14)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);
          var cell1 = new PdfPCell(new Phrase("Selected Options : "+convertOptionString(patientType) , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));
            (data.sortBy(_.treatmentId)).foreach(row => {
             var sampleData = List(s"${row.patientName}",s"${row.firstVisit}", s"${row.phoneNumber}", s"${row.address}", s"${row.email}",s"${row.providerName}",s"${row.patientType}")

            var columnWidths:Array[Float] = Array(14,14,14,14,17,13,14);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("First Visit Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Type", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                //cell.setPaddingLeft(5)
                cell.setPadding(13);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_LEFT);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
            })

            document.add(table);

            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 10
            }
          })
          } else {

            var columnWidths:Array[Float] = Array(14,14,14,14,14,14,14);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("First Visit Date", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Phone Number", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Address", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Email", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Type", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              document.add(table);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(97);
          var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1)
          document.add(headerTextTable1);
          }

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"Patient Type"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

def patientTypeCSV(patientType: Option[String]) = silhouette.SecuredAction{ request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val patientValue = convertOptionString(patientType).split(',')
    var csvList = List.empty[PatientTypePDF]
    var treatmentIdList = List.empty[Long]

   val block1 = db.run {
      TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId).filterNot(_.patientType === "")
      .join(ProviderTable).on(_.providerId === _.id).result
      } map { treatmentMap =>
      //  treatmentMap.map{result=>
      var value = treatmentMap.groupBy(_._1.id).map {
      case (treatmentId, entries) =>
        val result = entries.head._1
        val provider = entries.head._2
        var patientName = ""
        var phoneNumber = ""
        var address = ""
        var emailAddress = ""
        var firstVisit = ""
        var providerName = ""

            for (v <- patientValue) {
                    if(convertOptionString(result.patientType).toLowerCase contains v.toLowerCase){

              val block2 = db.run{
                StepTable.filter(_.formId === "1A").filter(_.treatmentId === result.id).sortBy(_.dateCreated.desc).result.head
              } map { stepMap =>

                    val first_name =  (stepMap.value \ "full_name").as[String]
                    val last_name =  (stepMap.value \ "last_name").as[String]

                    patientName = first_name +" "+ last_name
                    phoneNumber = (stepMap.value \ "phone").as[String]
                    address =  (stepMap.value \ "street").as[String]
                    emailAddress =  (stepMap.value \ "email").as[String]
                    firstVisit = (stepMap.value \ "first_visit").as[String]
                    providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

            }
              val AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(120, SECONDS))

              val treatmentId = convertOptionLong(result.id)

                  if(!treatmentIdList.contains(treatmentId)){
                  var dataList = PatientTypePDF(treatmentId,patientName,firstVisit,result.phoneNumber,address,result.emailAddress,providerName,convertOptionString(result.patientType))
                    csvList = csvList :+ dataList
                    treatmentIdList = List(treatmentId)
                }
            }
          }
        }
      }
      val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))

      val file = new File("/tmp/patientType Report.csv")
      val writer = CSVWriter.open(file)
      writer.writeRow(List("Selected Options: "+ convertOptionString(patientType)))
      writer.writeRow(List(""))
      writer.writeRow(List("Patient Name", "First Visit Date", "Phone Number", "Address", "Email", "Provider", "Patient Type"))
      csvList.sortBy(_.treatmentId).map {
        patientReport => writer.writeRow(List(patientReport.patientName,patientReport.firstVisit,patientReport.phoneNumber,patientReport.address,patientReport.email,patientReport.providerName,patientReport.patientType))
      }
      writer.close()
      Ok.sendFile(file, inline = false, _ => file.getName)
      }


def generatePatientsOutstandingBalancePdf(provider: Option[String]) = silhouette.SecuredAction.async { request =>

  import java.text.NumberFormat
  val locale = new Locale("en", "US")
  val formatter = NumberFormat.getCurrencyInstance(locale)
  var data = List.empty[PatientOutstandingBalance]
  val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
  var patientId: Long = 0
  var planId: Long = 0
  var patientName = ""
  var totalOustandingBalances: Float = 0
  var totalOustandingBalance: String = " "
  var currentDate: LocalDate = new LocalDate()
  var datePerformed: String = " "
  var provider_Name = ""

      var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
      var providerId = convertOptionString(provider)
      if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
        treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
        val block = db.run{
              ProviderTable.filter(_.id === convertStringToLong(Some(providerId))).result.head
        }map{providerName =>
            val title = providerName.title
            val first_name = providerName.firstName
            val last_name = providerName.lastName
            provider_Name = title + " " + first_name + " "+ last_name
        }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }
      db.run {
      treatmentPlanTableQuery
      .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
      .result
      } map { treatmentMap =>
      treatmentMap.map{result => {
      var treatmentDetails = result._1
      var treatments = result._2

      val cdtCode =  treatmentDetails.cdcCode
      val balanceToPatients = (Math.round((treatmentDetails.fee - convertOptionFloat(treatmentDetails.insurancePaidAmt)) * 100.0) / 100.0).toFloat
      val patientPaids = (Math.round(convertOptionFloat(treatmentDetails.patientPaidAmt) * 100.0) / 100.0).toFloat
      val outstandingBalances =  balanceToPatients - patientPaids
      patientId = convertOptionLong(treatmentDetails.patientId)

      if(treatmentDetails.datePerformed == None){
        datePerformed = "-"
      } else {
        datePerformed = dateFormatter.print(convertOptionalLocalDate(treatmentDetails.datePerformed))
      }

      val balanceToPatient = formatter.format(balanceToPatients)
      val patientPaid = formatter.format(patientPaids)
      val outstandingBalance = formatter.format(outstandingBalances)

      if(outstandingBalances > 0){

        totalOustandingBalances = totalOustandingBalances + outstandingBalances

        val first_name = convertOptionString(treatments.firstName)
        val last_name = convertOptionString(treatments.lastName)
        patientName = first_name + " "+ last_name

        val dataList = PatientOutstandingBalance(patientName,patientId,cdtCode,balanceToPatient,patientPaid,outstandingBalance,datePerformed)
        data = data :+ dataList
      }
    }
    }
      val sample = data

      totalOustandingBalance = formatter.format(totalOustandingBalances)

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));
          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          // Logo
          try {
            val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
            val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
            val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
            val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
            val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
            val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
            val g = img.createGraphics()
            g.drawRenderedImage(bi, at)
            val imgBytes = new ByteArrayOutputStream()
            ImageIO.write(img, "PNG", imgBytes)
            val image = Image.getInstance(imgBytes.toByteArray)
            image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
            image.setCompressionLevel(PdfStream.NO_COMPRESSION)
            image.setAlignment(Image.RIGHT)
            val cell = new PdfPCell(image);
            cell.setPadding(8);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBorder(Rectangle.NO_BORDER);
            headerImageTable.addCell(cell)
            document.add(headerImageTable);
          } catch {
            case error: Throwable => logger.debug(error.toString)
          }

          document.add(new Paragraph("\n"));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


          var cell = new PdfPCell(new Phrase("Patient's Outstanding Balance", FONT_TITLE));
          cell.setBorder(Rectangle.NO_BORDER);
          cell.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
          headerTextTable.addCell(cell)
          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var cell1 = new PdfPCell

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var Date: Date = simpleDateFormat.parse(currentDate.toString);

          var cell2 = new PdfPCell(new Phrase("Date : "+new SimpleDateFormat("mm/dd/yyyy").format(Date), FONT_TITLE1));
          cell2.setBorder(Rectangle.NO_BORDER);
          cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell2);

          cell1 = new PdfPCell(new Phrase("Total Amount : " +totalOustandingBalance , FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);

          if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
            var cell3= new PdfPCell(new Phrase("Provider Name : "+ provider_Name, FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell3)
          }
          else{
            var cell3 = new PdfPCell(new Phrase("all Providers", FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell3)
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 8;
          var recNum =  0;

          if(sample.length != 0){
            (sample).foreach(planDetailRow => {
              var sampleData = List(s"${planDetailRow.patientName}", s"${planDetailRow.patientId}",s"${planDetailRow.cdtCode}",s"${planDetailRow.datePerformed}", s"${planDetailRow.balanceToPatient}", s"${planDetailRow.patientPaid}", s"${planDetailRow.outstandingBalance}")
              var i=1

              var columnWidths:Array[Float] = Array(13,13,12,15,13,13,13);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 ) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);

                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Balance to Patient", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient Paid", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Outstanding Balance", FONT_COL_HEADER));
                cell.setPadding(5);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {
                cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                cell.setPaddingLeft(8);
                cell.setPaddingTop(16);
                cell.setPaddingBottom(16);
                cell.setBorder(Rectangle.TOP);
                cell.setBorderColorTop(new BaseColor(221, 221, 221));
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setUseVariableBorders(true);
                table.addCell(cell);
              })

              document.add(table);
              recNum = recNum+1;
              if(recNum == 0 ) {
                document.newPage();
              }

            })
          } else {
              var columnWidths:Array[Float] = Array(13,13,12,15,13,13,13);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            if(recNum == 0 ) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);

              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Balance to Patient", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Patient Paid", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Outstanding Balance", FONT_COL_HEADER));
              cell.setPadding(5);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              document.add(table);

              val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
              FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
              var headerTextTable1: PdfPTable = new PdfPTable(1)
              headerTextTable1.setWidthPercentage(97);
              var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1)
              document.add(headerTextTable1);

            }
          }
          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"outstandingBalance"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
  }
}


 def generatePatientsOutstandingBalanceCsv(provider: Option[String])=silhouette.SecuredAction { request =>


    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    var data = List.empty[PatientOutstandingBalanceList]
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var patientId: Long = 0
    var planId: Long = 0
    var patientName = ""
    var datePerformed: String = " "


      var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
      var providerId = convertOptionString(provider)
      if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
      treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
      }
      val balanceReportList = db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      treatmentMap.map{result => {
        var treatmentDetails = result._1
        var treatments = result._2
            val cdtCode =  treatmentDetails.cdcCode
            val balanceToPatient = (Math.round((treatmentDetails.fee - convertOptionFloat(treatmentDetails.insurancePaidAmt)) * 100.0) / 100.0).toFloat
            val patientPaid = (Math.round(convertOptionFloat(treatmentDetails.patientPaidAmt) * 100.0) / 100.0).toFloat
            val outstandingBalance = balanceToPatient - patientPaid
            patientId = convertOptionLong(treatmentDetails.patientId)

            if (treatmentDetails.datePerformed == None) {
              datePerformed = "-"
            } else {
              datePerformed = convertOptionalLocalDate(treatmentDetails.datePerformed).toString
            }
            if (outstandingBalance > 0) {

              val first_name = convertOptionString(treatments.firstName)
              val last_name = convertOptionString(treatments.lastName)
              patientName = first_name + " "+ last_name
              val dataList = PatientOutstandingBalanceList(patientName, patientId, cdtCode, balanceToPatient, patientPaid, outstandingBalance, datePerformed)
              data = data :+ dataList
            }
          }
        }
      }

    val AwaitResult = Await.ready(balanceReportList, atMost = scala.concurrent.duration.Duration(120, SECONDS))
    
    val file = new java.io.File("/tmp/patientOustandingBalance.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Patient Name", "Patient ID", "CDT Code", "Date Performed", "Balance to Patient", "Patient Paid", "Outstanding Balance"))
    data.sortBy(_.datePerformed).map {
      balanceList => writer.writeRow(List(balanceList.patientName, balanceList.patientId,  balanceList.cdtCode, balanceList.datePerformed, balanceList.balanceToPatient, balanceList.patientPaid, balanceList.outstandingBalance))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
  
  
 }




  def patientsOutstandingBalanceList(provider: Option[String]) = silhouette.SecuredAction.async { request =>
    var userAgent = convertOptionString(request.headers.get("User-Agent"))
    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    var data = List.empty[PatientOutstandingBalanceList]
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var patientId: Long = 0
    var planId: Long = 0
    var patientName = ""
    var totalOustandingBalance: Float = 0
    var datePerformed: String = " "


    var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
      treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
    }
    db.run {
      treatmentPlanTableQuery
      .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
      .result
      } map { treatmentMap =>
    treatmentMap.map{result => {
    var treatmentDetails = result._1
    var treatments = result._2
    val cdtCode =  treatmentDetails.cdcCode
    val balanceToPatient = (Math.round((treatmentDetails.fee - convertOptionFloat(treatmentDetails.insurancePaidAmt)) * 100.0) / 100.0).toFloat
    val patientPaid = (Math.round(convertOptionFloat(treatmentDetails.patientPaidAmt) * 100.0) / 100.0).toFloat
    val outstandingBalance = balanceToPatient - patientPaid
    patientId = convertOptionLong(treatmentDetails.patientId)

    if (treatmentDetails.datePerformed == None) {
      datePerformed = "-"
    } else {
      datePerformed = convertOptionalLocalDate(treatmentDetails.datePerformed).toString
    }

    if (outstandingBalance > 0) {

      totalOustandingBalance = totalOustandingBalance + outstandingBalance

      val first_name = convertOptionString(treatments.firstName)
      val last_name = convertOptionString(treatments.lastName)
      patientName = first_name + " "+ last_name
      val dataList = PatientOutstandingBalanceList(patientName, patientId, cdtCode, balanceToPatient, patientPaid, outstandingBalance, datePerformed)
      data = data :+ dataList
    }
  }
  }
      if(userAgent.toLowerCase contains "ios"){
        val users = Json.obj("data" -> data.sortBy(_.datePerformed), "totalBalance" -> totalOustandingBalance)
        Ok(Json.toJson(Array(users)))
      } else {
        Ok(Json.toJson(data.sortBy(_.datePerformed)))
      }
    }
  }



  def outstandingInsurancePDF(provider: Option[String]) = silhouette.SecuredAction.async { request =>
    var data = List.empty[OutstandingInsurancePDF]
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var outstandingAmt: Float = 0
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
    var provider_Name = ""

      var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
         val block = db.run{
              ProviderTable.filter(_.id === convertStringToLong(Some(providerId))).result.head
        }map{providerName =>
            val title = providerName.title
            val first_name = providerName.firstName
            val last_name = providerName.lastName
            provider_Name = title + " " + first_name + " "+ last_name
        }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      treatmentMap.map{result => {
        var treatmentDetails = result._1
        var treatments = result._2
        var patientName = ""
        var ptDatePerformed = ""
        var balanceAmt = (treatmentDetails.estIns - convertOptionFloat(treatmentDetails.insurancePaidAmt))

        if(treatmentDetails.datePerformed.toString != "None"){
            ptDatePerformed = dateFormatter.print(convertOptionalLocalDate(treatmentDetails.datePerformed))
        }else{
            ptDatePerformed = "-"
        }

        val first_name = convertOptionString(treatments.firstName)
        val last_name = convertOptionString(treatments.lastName)
        patientName = first_name + " "+ last_name

      if(balanceAmt > 0){
         outstandingAmt = outstandingAmt + balanceAmt
         var dataList = OutstandingInsurancePDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,ptDatePerformed,treatmentDetails.estIns,convertOptionFloat(treatmentDetails.insurancePaidAmt),balanceAmt)
         data = data :+ dataList
      }
     }
    }

        val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }

          val document = new Document(PageSize.A4.rotate(),0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(30, 30, 15 , 15)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

          val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
          FONT_TITLE1.setColor(new BaseColor(64, 64, 65));

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);

            var cell = new PdfPCell(new Phrase("Insurance outstanding report", FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var headerTextTable1: PdfPTable = new PdfPTable(1)
          headerTextTable1.setWidthPercentage(100);

          var simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var Date: Date = simpleDateFormat.parse(LocalDate.now().toString);

          var cell1 = new PdfPCell(new Phrase("Date : "+new SimpleDateFormat("mm/dd/yyyy").format(Date), FONT_TITLE1));
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell1);

          var cell2= new PdfPCell(new Phrase("Total amount: "+ formatter.format(Math.round(outstandingAmt * 100.0) / 100.0), FONT_TITLE1));
          cell2.setBorder(Rectangle.NO_BORDER);
          cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell2)

          if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
            var cell3= new PdfPCell(new Phrase("Provider Name : "+ provider_Name, FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell3)
          }
          else{
            var cell3= new PdfPCell(new Phrase("all Providers", FONT_TITLE1));
          cell3.setBorder(Rectangle.NO_BORDER);
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable1.addCell(cell3)
          }
          document.add(headerTextTable1);

          document.add(new Paragraph("\n"));

            var rowsPerPage = 5;
            var recNum =  0;
            if(treatmentMap.length > 0){

            (data).foreach(row => {
              var sampleData = List(s"${row.patientName}", s"${row.patientId}", s"${row.cdtCode}", s"${row.datePerformed}",s"${formatter.format(row.estIns)}",s"${formatter.format(row.insurancePaidAmt)}",s"${formatter.format(row.balanceAmt)}")

              var columnWidths:Array[Float] = Array(15,13,15,15,15,15,15);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 || recNum % rowsPerPage == 0) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Insurance Paid", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Outstanding Balance", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {

                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  //cell.setPaddingLeft(5)
                  cell.setPadding(13);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_LEFT);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
              })

              document.add(table);

              recNum = recNum+1;
              if(recNum == 0 || recNum % rowsPerPage == 0) {
                document.newPage();
              }
              if(recNum==7 && rowsPerPage!=8){
                recNum = 1
                rowsPerPage = 8
              }
            })
            } else {

              var columnWidths:Array[Float] = Array(15,13,15,15,15,15,15);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);

                document.add(new Paragraph("\n"));


                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Date Performed", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Insurance Paid", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Outstanding Balance", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                document.add(table);

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);
            }

            document.close();

          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"Outstanding Insurance"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
    }


def outstandingInsuranceCSV(provider: Option[String])  = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var csvList = List.empty[OutstandingInsurancePDF]

    var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      treatmentMap.map{result => {
      var treatmentDetails = result._1
      var treatments = result._2
      var patientName = ""
      var ptDatePerformed = ""
      var balanceAmt = (treatmentDetails.estIns - convertOptionFloat(treatmentDetails.insurancePaidAmt))

      if(treatmentDetails.datePerformed.toString != "None"){
          ptDatePerformed = convertOptionalLocalDate(treatmentDetails.datePerformed).toString
      }else{
          ptDatePerformed = "-"
      }

      val first_name = convertOptionString(treatments.firstName)
      val last_name = convertOptionString(treatments.lastName)
      patientName = first_name + " "+ last_name

    if(balanceAmt > 0){
       var dataList = OutstandingInsurancePDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,ptDatePerformed,treatmentDetails.estIns,convertOptionFloat(treatmentDetails.insurancePaidAmt),balanceAmt)
       csvList = csvList :+ dataList
    }
   }
  }

    val file = new File("/tmp/Outstanding Insurance.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Patient Name", "Patient ID", "CDT Code", "Date Performed", "Est.Ins","Insurance Paid","Outstanding Balance"))
    csvList.map {
      outstandingInsList => writer.writeRow(List(outstandingInsList.patientName, outstandingInsList.patientId, outstandingInsList.cdtCode, outstandingInsList.datePerformed,outstandingInsList.estIns,outstandingInsList.insurancePaidAmt,outstandingInsList.balanceAmt))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    }
  }


  def totalCollectedAmountPDF(fromDate: Option[String],toDate: Option[String],provider: Option[String]) = silhouette.SecuredAction.async { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var FromDate = LocalDate.parse(convertOptionString(fromDate))
    var ToDate = LocalDate.parse(convertOptionString(toDate))
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
    var totalInsuranceAmt: Float = 0
    var totalPatientAmt : Float = 0
    var provider_Name = ""

    var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(s => s.insurancePaidDate >= FromDate && s.insurancePaidDate <= ToDate || s.patientPaidDate >= FromDate && s.patientPaidDate <= ToDate).sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
         val block = db.run{
              ProviderTable.filter(_.id === convertStringToLong(Some(providerId))).result.head
        }map{providerName =>
            val title = providerName.title
            val first_name = providerName.firstName
            val last_name = providerName.lastName
            provider_Name = title + " " + first_name + " "+ last_name
        }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      var data = treatmentMap.map{result => {
      var treatmentDetails = result._1
      var treatments = result._2
      var patientName = ""
      var ptDatePerformed = ""
      var patientPaidDate = convertOptionalLocalDate(treatmentDetails.patientPaidDate)
      var insurancePaidDate = convertOptionalLocalDate(treatmentDetails.insurancePaidDate)
      var paymentReceivedFromInsurance: Float = 0
      var paymentReceivedFromPatient: Float = 0

        if(insurancePaidDate >= FromDate && insurancePaidDate <= ToDate){
            paymentReceivedFromInsurance =  convertOptionFloat(treatmentDetails.insurancePaidAmt)
            totalInsuranceAmt = totalInsuranceAmt + paymentReceivedFromInsurance
        }
        if(patientPaidDate >= FromDate && patientPaidDate <= ToDate){
            paymentReceivedFromPatient = convertOptionFloat(treatmentDetails.patientPaidAmt)
            totalPatientAmt = totalPatientAmt + paymentReceivedFromPatient
        }

        val first_name = convertOptionString(treatments.firstName)
        val last_name = convertOptionString(treatments.lastName)
        patientName = first_name + " "+ last_name
        val totalCollected = paymentReceivedFromInsurance + paymentReceivedFromPatient
        var totalCollectedPercent = ((paymentReceivedFromInsurance + paymentReceivedFromPatient)/ treatmentDetails.fee)*100
        var totalCollectedPercentage = Math.round(totalCollectedPercent*100.0)/100.0
         TotalCollectedAmountPDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,treatmentDetails.fee,paymentReceivedFromInsurance,paymentReceivedFromPatient,totalCollected,totalCollectedPercentage.toFloat)

     }
    }
    
        val source = StreamConverters.fromInputStream { () =>
          import java.io.IOException
          import com.itextpdf.text.pdf._
          import com.itextpdf.text.{List => _, _}
          import com.itextpdf.text.Paragraph
          import com.itextpdf.text.Element
          import com.itextpdf.text.Rectangle
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.Font;
          import com.itextpdf.text.Font.FontFamily;
          import com.itextpdf.text.BaseColor;
          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPTable
          import com.itextpdf.text.pdf.PdfPTableEvent

          import com.itextpdf.text.Element
          import com.itextpdf.text.Phrase
          import com.itextpdf.text.pdf.ColumnText
          import com.itextpdf.text.pdf.PdfContentByte
          import com.itextpdf.text.pdf.PdfPageEventHelper
          import com.itextpdf.text.pdf.PdfWriter

          class MyFooter extends PdfPageEventHelper {

            val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
            ffont.setColor(new BaseColor(64, 64, 65));
            override def onEndPage(writer: PdfWriter, document: Document): Unit = {
              val cb = writer.getDirectContent
              val footer = new Phrase(""+writer.getPageNumber(), ffont)
              var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
              val footerDate = new Phrase(submittedAt, ffont)

              ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
              ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
            }
          }

          val document = new Document(PageSize.A4.rotate(),0,0,0,0)
          val inputStream = new PipedInputStream()
          val outputStream = new PipedOutputStream(inputStream)
          val writer = PdfWriter.getInstance(document, outputStream)
          writer.setPageEvent(new MyFooter())
          document.setMargins(30, 30, 15 , 15)
          Future({
            document.open()

            import com.itextpdf.text.BaseColor
            import com.itextpdf.text.Font
            import com.itextpdf.text.FontFactory
            import com.itextpdf.text.pdf.BaseFont

            import com.itextpdf.text.FontFactory

            val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)

            FONT_TITLE.setColor(new BaseColor(64, 64, 65));
            val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
            val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)

            FONT_CONTENT.setColor(new BaseColor(64,64,65));
            FONT_COL_HEADER.setColor(new BaseColor(64,64,65));
            import com.itextpdf.text.FontFactory
            import java.net.URL


            var headerImageTable: PdfPTable = new PdfPTable(1)
            headerImageTable.setWidthPercentage(100);

            // Logo
            try {
              val bi = ImageIO.read(new URL("https://s3.amazonaws.com/static.novadonticsllc.com/img/logo.png"))
              val width = PageSize.NOTE.getWidth.asInstanceOf[Int]
              val scaleFactor = width.asInstanceOf[Double] / bi.getWidth
              val height = (bi.getHeight * scaleFactor).asInstanceOf[Int]
              val img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
              val at = AffineTransform.getScaleInstance(scaleFactor, scaleFactor)
              val g = img.createGraphics()
              g.drawRenderedImage(bi, at)
              val imgBytes = new ByteArrayOutputStream()
              ImageIO.write(img, "PNG", imgBytes)
              val image = Image.getInstance(imgBytes.toByteArray)
              image.scaleAbsolute((width * 0.3).asInstanceOf[Float], (height * 0.3).asInstanceOf[Float])
              image.setCompressionLevel(PdfStream.NO_COMPRESSION)
              image.setAlignment(Image.RIGHT)
              val cell = new PdfPCell(image);
              cell.setPadding(8);
              cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell.setBorder(Rectangle.NO_BORDER);
              headerImageTable.addCell(cell)
              document.add(headerImageTable);
            } catch {
              case error: Throwable => logger.debug(error.toString)
            }

            document.add(new Paragraph("\n"));

            var headerTextTable: PdfPTable = new PdfPTable(1)
            headerTextTable.setWidthPercentage(100);

          var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
          var  _fromDate: Date = simpleDateFormat.parse(convertOptionString(fromDate));
          var  _toDate: Date = simpleDateFormat.parse(convertOptionString(toDate));

            var cell = new PdfPCell(new Phrase("Collected amount from " + new SimpleDateFormat("mm/dd/yyyy").format(_fromDate) + " to "+ new SimpleDateFormat("mm/dd/yyyy").format(_toDate), FONT_TITLE));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
            headerTextTable.addCell(cell)
            document.add(headerTextTable);

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(100);

           var cell2= new PdfPCell(new Phrase("Date: "+ dateFormatter.print(LocalDate.now()), FONT_TITLE1));
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell2)

            var cell3= new PdfPCell(new Phrase("Total amount collected from insurance: "+ formatter.format(totalInsuranceAmt), FONT_TITLE1));
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell3)

            var cell4 = new PdfPCell(new Phrase("Total amount collected from patient: "+ formatter.format(totalPatientAmt), FONT_TITLE1));
            cell4.setBorder(Rectangle.NO_BORDER);
            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell4)

            if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
            var cell5= new PdfPCell(new Phrase("Provider Name : "+ provider_Name, FONT_TITLE1));
            cell5.setBorder(Rectangle.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell5)
            }
            else{
              var cell5 = new PdfPCell(new Phrase("all Providers", FONT_TITLE1));
            cell5.setBorder(Rectangle.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell5)
            }
            document.add(headerTextTable1);

            var rowsPerPage = 8;
            var recNum =  0;
            if(treatmentMap.length > 0){

            (data).foreach(row => {
              var sampleData = List(s"${row.patientName}",s"${row.patientId}", s"${row.cdtCode}", s"${formatter.format(row.fee)}", s"${formatter.format(row.paymentReceivedFromInsurance)}",s"${formatter.format(row.paymentReceivedFromPatient)}", s"${formatter.format(row.totalCollected)}", s"${ row.totalCollectedPercent}")

              var columnWidths:Array[Float] = Array(12,12,12,12,12,12,12,12);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);
              if(recNum == 0 || recNum % rowsPerPage == 0) {

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Paid By Insurance", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Paid By Patient", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Total Collected", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Collection Percentage", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)
              }

              (sampleData).foreach(item => {

                  cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                  //cell.setPaddingLeft(5)
                  cell.setPadding(13);
                  cell.setBorder(Rectangle.TOP);
                  cell.setBorderColorTop(new BaseColor(221, 221, 221));
                  cell.setBackgroundColor(BaseColor.WHITE);
                  cell.setVerticalAlignment(Element.ALIGN_LEFT);
                  cell.setUseVariableBorders(true);
                  table.addCell(cell);
              })

              document.add(table);

              recNum = recNum+1;
              if(recNum == 0 || recNum % rowsPerPage == 0) {
                document.newPage();
              }
              if(recNum==9 && rowsPerPage!=10){
                recNum = 1
                rowsPerPage = 10
              }
            })
            } else {

              var columnWidths:Array[Float] = Array(12,12,12,12,12,12,12,12);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(100);

                document.add(new Paragraph("\n"));

                cell = new PdfPCell(new Phrase("Patient Name", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Patient ID", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("CDT Code", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Paid By Insurance", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Paid By Patient", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Total Collected", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                cell = new PdfPCell(new Phrase("Collection Percentage", FONT_COL_HEADER));
                cell.setPadding(13);
                cell.setPaddingBottom(16);
                cell.setBackgroundColor(BaseColor.WHITE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell)

                document.add(table);

            document.add(new Paragraph("\n"));

            val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 12)
            FONT_TITLE1.setColor(new BaseColor(64, 64, 65));
            var headerTextTable1: PdfPTable = new PdfPTable(1)
            headerTextTable1.setWidthPercentage(97);
            var cell1 = new PdfPCell(new Phrase("No records found! ", FONT_TITLE1));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            headerTextTable1.addCell(cell1)
            document.add(headerTextTable1);
            }

            document.close();

          })(ioBoundExecutor)

          inputStream
        }

        val filename = s"Total collected amount"
        val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

        Result(
          header = ResponseHeader(200, Map.empty),
          body = HttpEntity.Streamed(source, None, Some("application/pdf"))
        ).withHeaders(
          "Content-Disposition" -> contentDisposition
        )
      }
    }

def totalCollectedAmountCSV(fromDate: Option[String],toDate: Option[String],provider: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var FromDate = LocalDate.parse(convertOptionString(fromDate))
  var ToDate = LocalDate.parse(convertOptionString(toDate))
  var csvList = List.empty[TotalCollectedAmountPDF]
  var totalInsuranceAmt: Float = 0
  var totalPatientAmt : Float = 0

    var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(s => s.insurancePaidDate >= FromDate && s.insurancePaidDate <= ToDate || s.patientPaidDate >= FromDate && s.patientPaidDate <= ToDate).sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      treatmentMap.map{result => {
      var treatmentDetails = result._1
      var treatments = result._2

      var patientName = ""
      var ptDatePerformed = ""
      var patientPaidDate = convertOptionalLocalDate(treatmentDetails.patientPaidDate)
      var insurancePaidDate = convertOptionalLocalDate(treatmentDetails.insurancePaidDate)
      var paymentReceivedFromInsurance: Float = 0
      var paymentReceivedFromPatient: Float = 0

      if(insurancePaidDate >= FromDate && insurancePaidDate <= ToDate){
          paymentReceivedFromInsurance =  convertOptionFloat(treatmentDetails.insurancePaidAmt)
          totalInsuranceAmt = totalInsuranceAmt + paymentReceivedFromInsurance

      }
      if(patientPaidDate >= FromDate && patientPaidDate <= ToDate){
          paymentReceivedFromPatient = convertOptionFloat(treatmentDetails.patientPaidAmt)
          totalPatientAmt = totalPatientAmt + paymentReceivedFromPatient

      }

      val first_name = convertOptionString(treatments.firstName)
      val last_name = convertOptionString(treatments.lastName)
      patientName = first_name + " "+ last_name
      val totalCollected = paymentReceivedFromInsurance + paymentReceivedFromPatient
      var totalCollectedPercent = ((paymentReceivedFromInsurance + paymentReceivedFromPatient)/ treatmentDetails.fee)*100 
      var totalCollectedPercentage = Math.round(totalCollectedPercent*100.0)/100.0
      //totalCollectedPercent
       var dataList = TotalCollectedAmountPDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,treatmentDetails.fee,paymentReceivedFromInsurance,paymentReceivedFromPatient,totalCollected,totalCollectedPercentage.toFloat)
       csvList = csvList :+ dataList
   }
  }
  
    val file = new File("/tmp/Total Collected amount.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("Patient Name", "Patient ID", "CDT Code", "Fee",  "Paid By Insurance", "Paid By Patient", "Total Collected", "Collection Percentage"))
    csvList.map {
      collectedAmtList => writer.writeRow(List(collectedAmtList.patientName,collectedAmtList.patientId, collectedAmtList.cdtCode,collectedAmtList.fee,collectedAmtList.paymentReceivedFromInsurance, collectedAmtList.paymentReceivedFromPatient,collectedAmtList.totalCollected,collectedAmtList.totalCollectedPercent))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
    }
  }

  def totalCollectedAmount(fromDate: Option[String],toDate: Option[String],provider: Option[String]) = silhouette.SecuredAction.async { request =>
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var userAgent = convertOptionString(request.headers.get("User-Agent"))
    var FromDate = LocalDate.parse(convertOptionString(fromDate))
    var ToDate = LocalDate.parse(convertOptionString(toDate))
    var totalInsuranceAmt: Float = 0
    var totalPatientAmt : Float = 0

      var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(s => s.insurancePaidDate >= FromDate && s.insurancePaidDate <= ToDate || s.patientPaidDate >= FromDate && s.patientPaidDate <= ToDate).sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      var data = treatmentMap.map{result => {
      var treatmentDetails = result._1
      var treatments = result._2

        var patientName = ""
        var ptDatePerformed = ""
        var patientPaidDate = convertOptionalLocalDate(treatmentDetails.patientPaidDate)
        var insurancePaidDate = convertOptionalLocalDate(treatmentDetails.insurancePaidDate)
        var paymentReceivedFromInsurance: Float = 0
        var paymentReceivedFromPatient: Float = 0

        if(insurancePaidDate >= FromDate && insurancePaidDate <= ToDate){
            paymentReceivedFromInsurance =  convertOptionFloat(treatmentDetails.insurancePaidAmt)
            totalInsuranceAmt = totalInsuranceAmt + paymentReceivedFromInsurance
        }
        if(patientPaidDate >= FromDate && patientPaidDate <= ToDate){
            paymentReceivedFromPatient = convertOptionFloat(treatmentDetails.patientPaidAmt)
            totalPatientAmt = totalPatientAmt + paymentReceivedFromPatient
        }

        val first_name = convertOptionString(treatments.firstName)
        val last_name = convertOptionString(treatments.lastName)
        patientName = first_name + " "+ last_name
        val totalCollected = paymentReceivedFromInsurance + paymentReceivedFromPatient
        val totalCollectedPercent = ((paymentReceivedFromInsurance + paymentReceivedFromPatient)/ treatmentDetails.fee)*100

         TotalCollectedAmountPDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,treatmentDetails.fee,paymentReceivedFromInsurance,paymentReceivedFromPatient,totalCollected,totalCollectedPercent)
      }
     }
    
    if(userAgent.toLowerCase contains "ios"){
     val users = Json.obj("data" -> data, "insuranceTotal" -> totalInsuranceAmt,"patientTotal" -> totalPatientAmt)
     Ok(Json.toJson(Array(users)))
    } else{
     Ok(Json.toJson(data))
    }
    }
   }


  def outstandingInsurance(provider: Option[String]) = silhouette.SecuredAction.async { request =>
    var data = List.empty[OutstandingInsurancePDF]
    var userAgent = convertOptionString(request.headers.get("User-Agent"))
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    var totalAmt: Float = 0

      var treatmentPlanTableQuery = TreatmentPlanDetailsTable.filter(_.status.toLowerCase === "completed").sortBy(_.datePerformed)
    var providerId = convertOptionString(provider)
    if(providerId.toLowerCase != "all" && providerId.toLowerCase != None && providerId.toLowerCase != "") {
         treatmentPlanTableQuery = treatmentPlanTableQuery.filter(_.providerId === convertStringToLong(Some(providerId)))
      }
      db.run {
        treatmentPlanTableQuery
        .join(TreatmentTable).on(_.patientId === _.id).filter(_._2.practiceId === practiceId)
        .result
        } map { treatmentMap =>
      treatmentMap.map{result => {
        var treatmentDetails = result._1
        var treatments = result._2
        var patientName = ""
        var ptDatePerformed = ""
        var balanceAmt = (treatmentDetails.estIns - convertOptionFloat(treatmentDetails.insurancePaidAmt))

        if(treatmentDetails.datePerformed.toString != "None"){
            ptDatePerformed = convertOptionalLocalDate(treatmentDetails.datePerformed).toString
        }else{
            ptDatePerformed = "-"
        }

            val first_name = convertOptionString(treatments.firstName)
            val last_name = convertOptionString(treatments.lastName)
            patientName = first_name + " "+ last_name
        
      if(balanceAmt > 0){
         totalAmt = totalAmt + balanceAmt
         var dataList = OutstandingInsurancePDF(convertOptionLong(treatmentDetails.patientId),patientName,treatmentDetails.cdcCode,ptDatePerformed,treatmentDetails.estIns,convertOptionFloat(treatmentDetails.insurancePaidAmt),balanceAmt)
         data = data :+ dataList
      }
     }
    }
        
       
      if(userAgent.toLowerCase contains "ios"){
         val users = Json.obj("data" -> data , "totalBalance" -> Math.round(totalAmt * 100.0) / 100.0)
         Ok(Json.toJson(Array(users)))
       } else {
         Ok(Json.toJson(data))
        }
      }
  }


  case class PatientEstimateRowPDF(
   datePlanned : String,
   cdtCode : String,
   procedureDes : String,
   tooth : String,
   surface : String,
   providerName : String,
   fee : String,
   estIns : String,
   patientBalance : String,
   status : String
   )


  def patientEstimatePDF (patientId : Option[Long]) = silhouette.SecuredAction.async { request =>
    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)

    var data = List.empty[PatientEstimateRowPDF]
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val currentDirectory = new java.io.File(".").getCanonicalPath
    val amountFormat = new DecimalFormat("0.00")
    val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")

    db.run {
      // TreatmentPlanTable.filterNot(_.deleted).filter(_.id === treatmentPlanId).join
      TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(s => s.status.toLowerCase === "proposed" || s.status.toLowerCase === "accepted").sortBy(_.datePerformed)
        .join(ProviderTable).on(_.providerId === _.id)
        .join(PracticeTable).filter(_._2.id === practiceId).result
    } map { treatmentMap =>

      var bxPatientName = ""
      // var bxTreatmentPlanName = ""
      var bxEstDate = dateFormatter.print(LocalDate.now())
      var bxFee : Float =  0
      var strFee : String =  ""
      var bxEstIns : Float = 0
      var strEstIns : String =  ""
      var bxPatientBalance : Float = 0
      var strPatientBalance : String =  ""

      treatmentMap.map{result => {
        // val treatmentPlan = result._1._1._1
        val treatmentDetails = result._1._1
        val providerValue = result._1._2
        val practiceValue = result._2

        val fee = amountFormat.format(treatmentDetails.fee).toFloat
        val estIns  = amountFormat.format(treatmentDetails.estIns).toFloat
        val balanceAmt   = amountFormat.format((fee - estIns)).toFloat
        var providerName  = providerValue.firstName +" "+ providerValue.lastName

        // bxTreatmentPlanName = treatmentPlan.planName
        bxFee = bxFee + fee
        bxEstIns = bxEstIns + estIns
        bxPatientBalance = bxPatientBalance + balanceAmt
        if(bxFee == 0){
          strFee = ""
        } else {
          strFee = formatter.format(bxFee)
        }
        if(bxEstIns == 0){
          strEstIns = ""
        } else {
          strEstIns = formatter.format(bxEstIns)
        }
        if(bxPatientBalance == 0){
          strPatientBalance = ""
        } else {
          strPatientBalance = formatter.format(bxPatientBalance)
        }

        val block = db.run{
          StepTable.filter(_.treatmentId === treatmentDetails.patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
        }map{stepResult =>
          val first_name = convertOptionString((stepResult.value \ "full_name").asOpt[String])
          val last_name = convertOptionString((stepResult.value \ "last_name").asOpt[String])
          bxPatientName = first_name + " "+ last_name
        }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

        var dataList =
          PatientEstimateRowPDF(
            dateFormatter.print(treatmentDetails.datePlanned),
            treatmentDetails.cdcCode,
            treatmentDetails.procedureName,
            treatmentDetails.toothNumber,
            treatmentDetails.surface,
            providerName,
            formatter.format(fee),
            formatter.format(estIns),
            formatter.format(balanceAmt),
            treatmentDetails.status
          )
        data = data :+ dataList
      }
      }

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter
        import com.itextpdf.text.pdf.PdfPCellEvent

        class MyFooter extends PdfPageEventHelper {

          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }
        }

        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.1)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont
          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arialbd.ttf", 25)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));

          val FONT_WHITE = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arial.ttf", 11)
          FONT_WHITE.setColor(BaseColor.WHITE);

          val FONT_BLACK = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arial.ttf", 11)
          FONT_BLACK.setColor(new BaseColor(64,64,65));

          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));

          val FONT_COL_HEADER = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arialbd.ttf", 11)
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));

          import com.itextpdf.text.FontFactory
          import java.net.URL

          document.add(new Paragraph("\n"));


          var columnWidths:Array[Float] = Array(40,60);
          var detailsTable: PdfPTable = new PdfPTable(columnWidths);
          detailsTable.setWidthPercentage(100);

          var cell = new PdfPCell(new Phrase(s"Patient Name \n\nEstimate Date \n\nFee \n\nEst. Ins \n\nBalance to Patient", FONT_WHITE ));
          cell.setBackgroundColor(new BaseColor(239,139,93));
          cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
          cell.setPaddingRight(5)
          cell.setPadding(5)
          cell.setUseVariableBorders(true);
          cell.setBorderColor(new BaseColor(239,139,93));
          cell.setBorderWidth(1.5f)
          detailsTable.addCell(cell)

          var cell3 = new PdfPCell(new Phrase(s"${bxPatientName} \n\n${bxEstDate}\n\n${strFee}\n\n${strEstIns}\n\n${strPatientBalance}", FONT_BLACK));
          cell3.setBackgroundColor(BaseColor.WHITE);
          cell3.setPaddingLeft(5)
          cell3.setPadding(5)
          cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
          cell3.setUseVariableBorders(true);
          cell3.setBorderWidth(1.5f)
          cell3.setBorderColor(new BaseColor(239,139,93));
          detailsTable.addCell(cell3)


          var columnWidths1:Array[Float] = Array(60,40);
          var headerTextTable: PdfPTable = new PdfPTable(columnWidths1)
          headerTextTable.setWidthPercentage(100);

          var cell2 = new PdfPCell(new Phrase("Treatment Plan Estimate",FONT_TITLE));
          cell2.setBorder(Rectangle.NO_BORDER);
          cell2.setPaddingTop(100);
          cell2.setPaddingLeft(15);
          cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
          headerTextTable.addCell(cell2)

          var cell1 = new PdfPCell(detailsTable);
          cell1.setBorder(Rectangle.NO_BORDER);
          cell1.setPaddingTop(80);
          cell1.setPaddingLeft(15);
          cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
          headerTextTable.addCell(cell1)

          document.add(headerTextTable);

          document.add(new Paragraph("\n"));

          var rowsPerPage = 5;
          var recNum =  0;

          (data).foreach(row => {
            // var sampleData = List(s"${row.datePlanned}", s"${row.cdtCode}", s"${row.procedureDes}", s"${row.tooth}",s"${row.surface}",s"${row.providerName}",s"${row.fee}",s"${row.estIns}",s"${row.patientBalance}")
            var sampleData = List(s"${row.datePlanned}", s"${row.cdtCode}", s"${row.tooth}",
              s"${row.surface}",s"${row.providerName}",s"${row.fee}",s"${row.estIns}",s"${row.patientBalance}",s"${row.status}")

            //var columnWidths:Array[Float] = Array(12f,11f,8f,9f,12.5f,12f,12f,12.5f,11f);
            var columnWidths:Array[Float] = Array(11,11,8,9,12,12,12,13,12);
            var table: PdfPTable = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            // table.setTableEvent(new BorderEvent())


            if(recNum == 0 || recNum % rowsPerPage == 0) {

              document.add(new Paragraph("\n"));

              cell = new PdfPCell(new Phrase(" Date Planned", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("CDT code", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              // cell = new PdfPCell(new Phrase("Procedure Description", FONT_COL_HEADER));
              // cell.setPadding(13);
              // cell.setPaddingBottom(16);
              // cell.setBackgroundColor(new BaseColor(242, 244, 247));
              // cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              // cell.setBorder(Rectangle.NO_BORDER);
              // table.addCell(cell)

              cell = new PdfPCell(new Phrase("Tooth", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Surface", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Balance to Patient", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)

              cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
              cell.setPadding(13);
              cell.setPaddingBottom(16);
              cell.setBackgroundColor(new BaseColor(242, 244, 247));
              cell.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell.setBorder(Rectangle.NO_BORDER);
              table.addCell(cell)
            }

            (sampleData).foreach(item => {

              cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
              //cell.setPaddingLeft(5)
              cell.setPadding(13);
              cell.setBorder(Rectangle.TOP);
              cell.setBorderColorTop(new BaseColor(221, 221, 221));
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setVerticalAlignment(Element.ALIGN_LEFT);
              cell.setUseVariableBorders(true);
              table.addCell(cell);
            })

              document.add(table);


            recNum = recNum+1;
            if(recNum == 0 || recNum % rowsPerPage == 0) {
              document.newPage();
            }
            if(recNum==9 && rowsPerPage!=10){
              recNum = 1
              rowsPerPage = 7
            }
          })

            var table1: PdfPTable = new PdfPTable(1);
            table1.setWidthPercentage(100);

              var cell5 = new PdfPCell(new Phrase("I understand that I am responsible for the entire treatment cost of my dental work if my insurance declines payment for any reason.", FONT_COL_HEADER));
              cell5.setPadding(13);
              cell5.setPaddingTop(50);
              cell5.setPaddingBottom(16);
              cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell5.setBorder(Rectangle.NO_BORDER);
              table1.addCell(cell5)

              document.add(table1);

            var columnWidths5:Array[Float] = Array(60,40);
            var table2: PdfPTable = new PdfPTable(columnWidths5);
            table1.setWidthPercentage(100);

              cell5 = new PdfPCell(new Phrase("Date", FONT_COL_HEADER));
              cell5.setPadding(13);
              cell5.setPaddingTop(80);
              cell5.setPaddingBottom(16);
              cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell5.setBorder(Rectangle.NO_BORDER);
              table2.addCell(cell5)

              cell5 = new PdfPCell(new Phrase("Signature", FONT_COL_HEADER));
              cell5.setPadding(13);
              cell5.setPaddingTop(80);
              cell5.setPaddingBottom(16);
              cell5.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell5.setBorder(Rectangle.NO_BORDER);
              table2.addCell(cell5)

              document.add(table2);

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"Patient Estimate"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }

  case class PatientStatementRowPDF(
      datePlanned : String,
      cdtCode : String,
      tooth : String,
      surface : String,
      providerName : String,
      fee : String,
      estIns : String,
      patientBalance : String,
      status : String
    )

  def generatePatientsStatementPdf(patientId : Option[Long]) = silhouette.SecuredAction.async { request =>

    import java.text.NumberFormat
    val locale = new Locale("en", "US")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    var data = List.empty[PatientStatementRowPDF]
    var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val currentDirectory = new java.io.File(".").getCanonicalPath
    val dateFormatter = DateTimeFormat.forPattern("MM/dd/yyyy")
    var  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
    val imgLogo =  Image.getInstance("https://s3.amazonaws.com/static.novadonticsllc.com/img/payment_slip_part1.png");
    val imgLogo1 =  Image.getInstance("https://s3.amazonaws.com/static.novadonticsllc.com/img/payment_slip_part2.png");

    db.run {
      // TreatmentPlanTable.filterNot(_.deleted).filter(_.id === treatmentPlanId).join
      TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(s=> s.status.toLowerCase === "accepted" || s.status.toLowerCase === "completed").sortBy(_.datePerformed)
        .join(ProviderTable).on(_.providerId === _.id)
        .join(PracticeTable).filter(_._2.id === practiceId).result
    } map { treatmentMap =>

        var bxPatientName = ""
        var bxPatientAddress = ""
        // var bxTreatmentPlanName = ""
        var bxPracticeName = ""
        var bxPracticeAddress = ""
        var bxPracticeState = ""
        var bxPracticeCity = ""
        var bxPracticeCountry = ""
        var bxPracticeZip = ""
        var bxEstDates = LocalDate.now()
        var bxEstDate = dateFormatter.print(bxEstDates)
        var bxDueDates = bxEstDates.plusDays(14)
        var bxDueDate = dateFormatter.print(bxDueDates)
        var bxFee : Float =  0
        var bxEstIns : Float = 0
        var bxBalanceAmt : Float = 0
        var bxPatientBalance : Float = 0

      treatmentMap.map{ result => {
        // val treatmentPlan = result._1._1._1
        val treatmentDetails = result._1._1
        val providerValue = result._1._2
        val practiceValue = result._2

        var fee  = (Math.round(treatmentDetails.fee * 100.0) / 100.0).toFloat
        var estIns  = (Math.round(treatmentDetails.estIns * 100.0) / 100.0).toFloat
        var balanceAmt   = (Math.round((fee - estIns) * 100.0) / 100.0).toFloat
        var patientPaids = (Math.round(convertOptionFloat(treatmentDetails.patientPaidAmt) * 100.0) / 100.0).toFloat
        var providerName  = providerValue.firstName +" "+ providerValue.lastName
        // bxTreatmentPlanName = treatmentPlan.planName
        bxPracticeName = practiceValue.name
        bxPracticeAddress = practiceValue.address
        bxPracticeState = practiceValue.state
        bxPracticeCity = practiceValue.city
        bxPracticeCountry = practiceValue.country
        bxPracticeZip = practiceValue.zip
        bxFee = bxFee + fee
        bxEstIns = bxEstIns + estIns
        bxPatientBalance = bxPatientBalance + balanceAmt
        //bxBalanceAmt = bxPatientBalance + balanceAmt
        //bxPatientBalance = bxBalanceAmt - patientPaids

        val block = db.run{
          StepTable.filter(_.treatmentId === treatmentDetails.patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
        }map{stepResult =>
          val first_name = convertOptionString((stepResult.value \ "full_name").asOpt[String])
          val last_name = convertOptionString((stepResult.value \ "last_name").asOpt[String])
          bxPatientName = first_name + " "+ last_name
          bxPatientAddress = convertOptionString((stepResult.value \ "street").asOpt[String])
        }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

        var dataList = PatientStatementRowPDF(dateFormatter.print(treatmentDetails.datePlanned),treatmentDetails.cdcCode,treatmentDetails.toothNumber,treatmentDetails.surface,providerName,formatter.format(fee),formatter.format(estIns),formatter.format(balanceAmt),treatmentDetails.status)
        data = data :+ dataList
      }
      }

      val source = StreamConverters.fromInputStream { () =>
        import java.io.IOException
        import com.itextpdf.text.pdf._
        import com.itextpdf.text.{List => _, _}
        import com.itextpdf.text.Paragraph
        import com.itextpdf.text.Element
        import com.itextpdf.text.Rectangle
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.Font;
        import com.itextpdf.text.Font.FontFamily;
        import com.itextpdf.text.BaseColor;
        import com.itextpdf.text.BaseColor
        import com.itextpdf.text.Font
        import com.itextpdf.text.FontFactory
        import com.itextpdf.text.pdf.BaseFont

        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPTable
        import com.itextpdf.text.pdf.PdfPTableEvent

        import com.itextpdf.text.Element
        import com.itextpdf.text.Phrase
        import com.itextpdf.text.pdf.ColumnText
        import com.itextpdf.text.pdf.PdfContentByte
        import com.itextpdf.text.pdf.PdfPageEventHelper
        import com.itextpdf.text.pdf.PdfWriter

        class MyFooter extends PdfPageEventHelper {
          val ffont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL)
          ffont.setColor(new BaseColor(64, 64, 65));
          override def onEndPage(writer: PdfWriter, document: Document): Unit = {
            val cb = writer.getDirectContent
            val footer = new Phrase(""+writer.getPageNumber(), ffont)
            var submittedAt = new SimpleDateFormat("MM/dd/yyyy").format(new Date())
            val footerDate = new Phrase(submittedAt, ffont)

            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer, (document.right - document.left) / 2 + document.leftMargin, document.bottom - 2, 0)
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footerDate, document.right, document.bottom - 2, 0)
          }

        }


        class BorderEvent extends PdfPTableEvent {
          override def tableLayout(table: PdfPTable, widths: Array[Array[Float]], heights: Array[Float], headerRows: Int, rowStart: Int, canvases: Array[PdfContentByte]): Unit = {
            val width = widths(0)
            val x1 = width(0)
            val x2 = width(width.length - 1)
            val y1 = heights(0)
            val y2 = heights(heights.length - 1)
            val cb = canvases(PdfPTable.LINECANVAS)
            cb.rectangle(x1, y1, x2 - x1, y2 - y1)
            cb.setLineWidth(0.4)
            cb.stroke()
            cb.resetRGBColorStroke()
          }
        }

        val document = new Document(PageSize.A4.rotate(),0,0,0,0)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        writer.setPageEvent(new MyFooter())
        document.setMargins(30, 30, 15 , 15)
        Future({
          document.open()

          import com.itextpdf.text.BaseColor
          import com.itextpdf.text.Font
          import com.itextpdf.text.FontFactory
          import com.itextpdf.text.pdf.BaseFont

          import com.itextpdf.text.FontFactory

          val FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 17)
          FONT_TITLE.setColor(new BaseColor(64, 64, 65));

          val FONT_CONTENT = FontFactory.getFont(FontFactory.HELVETICA, 11)
          FONT_CONTENT.setColor(new BaseColor(64,64,65));

          val FONT_COL_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 9)
          FONT_COL_HEADER.setColor(new BaseColor(64,64,65));

          val FONT_TITLE_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 7)
          FONT_TITLE_HEADER.setColor(BaseColor.GRAY);

          val FONT_COL_HEADER_WHITE = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10)
          FONT_COL_HEADER_WHITE.setColor(new BaseColor(255,255,255));

          val FONT_NAME_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 11)
          FONT_NAME_HEADER.setColor(new BaseColor(64,64,65));

          val FONT_ADDRESS_HEADER = FontFactory.getFont(FontFactory.HELVETICA, 10)
          FONT_ADDRESS_HEADER.setColor(new BaseColor(64,64,65));

          val FONT_WHITE = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arial.ttf", 11)
          FONT_WHITE.setColor(BaseColor.WHITE);

          val FONT_BLACK = FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arial.ttf", 11)
          FONT_BLACK.setColor(new BaseColor(64,64,65));

          import com.itextpdf.text.FontFactory
          import java.net.URL

          var headerImageTable: PdfPTable = new PdfPTable(1)
          headerImageTable.setWidthPercentage(100);

          var headerTextTable: PdfPTable = new PdfPTable(1)
          headerTextTable.setWidthPercentage(100);


              val FONT_TITLE1 = FontFactory.getFont(FontFactory.HELVETICA, 14)
              FONT_TITLE1.setColor(new BaseColor(64, 64, 65));


              var table3: PdfPTable = new PdfPTable(1);
              table3.setWidthPercentage(100);
              var cell3 = new PdfPCell

              cell3 = new PdfPCell
              cell3.addElement(new Chunk(s"${bxPracticeName}", FONT_NAME_HEADER));
              cell3.addElement(new Chunk(s"${bxPracticeAddress}", FONT_ADDRESS_HEADER));
              cell3.addElement(new Chunk(s"${bxPracticeState}, ${bxPracticeCity}", FONT_ADDRESS_HEADER));
              cell3.addElement(new Chunk(s"${bxPracticeCountry}", FONT_ADDRESS_HEADER));
              cell3.addElement(new Chunk(s"${bxPracticeZip}", FONT_ADDRESS_HEADER));
              cell3.setPaddingLeft(20);
              cell3.setPaddingRight(2);
              cell3.setPaddingTop(0);
              cell3.setPaddingBottom(16);
              cell3.setBorder(Rectangle.NO_BORDER);
              cell3.setBackgroundColor(BaseColor.WHITE);
              cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
              table3.addCell(cell3)


              var table4: PdfPTable = new PdfPTable(1);
              table4.setWidthPercentage(100);
              var cell4 = new PdfPCell

              cell4 = new PdfPCell
              cell4.addElement(new Chunk(s"${bxPatientName}", FONT_ADDRESS_HEADER));
              cell4.addElement(new Chunk(s"${bxPatientAddress}", FONT_ADDRESS_HEADER));
              cell4.setPaddingLeft(20);
              cell4.setPaddingTop(60);
              cell4.setPaddingRight(2);
              cell4.setPaddingBottom(16);
              cell4.setBorder(Rectangle.NO_BORDER);
              cell4.setBackgroundColor(BaseColor.WHITE);
              cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
              table4.addCell(cell4)

              var columnWidthsMain:Array[Float] = Array(50,50);
              var tableMain: PdfPTable = new PdfPTable(columnWidthsMain);
              tableMain.setWidthPercentage(100);

              var cellMain = new PdfPCell
              cellMain.setBorder(Rectangle.NO_BORDER);
              cellMain.setPaddingLeft(15);
              cellMain.setPaddingTop(15);
              cellMain.addElement(table3);
              cellMain.addElement(table4);
              tableMain.addCell(cellMain)


              var table2: PdfPTable = new PdfPTable(1);
              table2.setWidthPercentage(100);
              var cell2 = new PdfPCell

              cell2 = createImageCell2(imgLogo, 100f, 100f, true, document);
              cell2.setBackgroundColor(BaseColor.WHITE);
              cell2.setBorder(Rectangle.TOP);
              cell2.setUseVariableBorders(true)
              cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell2.setBorderColorTop(new BaseColor(255, 255, 255));
              table2.addCell(cell2);

              document.add(new Paragraph("\n"));

              var columnWidths:Array[Float] = Array(10,10,10);
              var table: PdfPTable = new PdfPTable(columnWidths);
              table.setWidthPercentage(55);
              var cell = new PdfPCell

              cell = new PdfPCell
              var para = new Paragraph("Balance Due", FONT_TITLE_HEADER);
              para.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para);
              var para1 = new Paragraph(s"${formatter.format(bxFee)}", FONT_COL_HEADER);
              para1.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para1);
              cell.setPaddingBottom(5);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell.setUseVariableBorders(true)
              cell.setBorderColor(new BaseColor(239, 139, 93));
              table.addCell(cell)

              cell = new PdfPCell
              para = new Paragraph("Insurance Est.", FONT_TITLE_HEADER);
              para.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para);
              para1 = new Paragraph(s"${formatter.format(bxEstIns)}", FONT_COL_HEADER);
              para1.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para1);
              cell.setPaddingBottom(5);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell.setUseVariableBorders(true)
              cell.setBorderColor(new BaseColor(239, 139, 93));
              table.addCell(cell)

              cell = new PdfPCell;
              para = new Paragraph("Your Portion", FONT_TITLE_HEADER);
              para.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para);
              para1 = new Paragraph(s"${formatter.format(bxPatientBalance)}", FONT_COL_HEADER);
              para1.setAlignment(Element.ALIGN_CENTER);
              cell.addElement(para1);
              cell.setPaddingBottom(5);
              cell.setBackgroundColor(BaseColor.WHITE);
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell.setUseVariableBorders(true)
              cell.setBorderColor(new BaseColor(239, 139, 93));
              table.addCell(cell)


              var columnWidths1:Array[Float] = Array(15,15);
              var table1: PdfPTable = new PdfPTable(columnWidths1);
              table1.setWidthPercentage(55);
              var cell1 = new PdfPCell

              cell1 = new PdfPCell(new Phrase("Amount Enclosed ", FONT_COL_HEADER_WHITE));
              cell1.setPadding(8);
              cell1.setBackgroundColor(new BaseColor(239, 139, 93));
              cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell1.setUseVariableBorders(true)
              cell1.setBorderColor(new BaseColor(239, 139, 93));
              table1.addCell(cell1)

              cell1 = new PdfPCell(new Phrase("", FONT_COL_HEADER));
              cell1.setPadding(10);
              cell1.setBackgroundColor(BaseColor.WHITE);
              cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell1.setUseVariableBorders(true)
              cell1.setBorderColor(new BaseColor(239, 139, 93));
              table1.addCell(cell1)


              var cellMain1 = new PdfPCell;
              cellMain1.setBorder(Rectangle.NO_BORDER);
              cellMain1.setPaddingLeft(15);
              cellMain1.addElement(table2);
              cellMain1.addElement(table);
              cellMain1.addElement(table1);
              tableMain.addCell(cellMain1)

              document.add(tableMain);

              var table5: PdfPTable = new PdfPTable(1);
              table5.setWidthPercentage(100);
              var cell5 = new PdfPCell

              cell5 = createImageCell3(imgLogo1, 100f, 100f, true, document);
              cell5.setBackgroundColor(BaseColor.WHITE);
              cell5.setBorder(Rectangle.TOP);
              cell5.setUseVariableBorders(true)
              cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
              cell5.setBorderColorTop(new BaseColor(255, 255, 255));
              table5.addCell(cell5);

              document.add(table5);


              var columnWidths5:Array[Float] = Array(30,22);
              var detailsTable: PdfPTable = new PdfPTable(columnWidths5);
              detailsTable.setWidthPercentage(20);

              var cell6 = new PdfPCell(new Phrase(s"Patient Name  \n\nStatement Date \n\nDue Date  \n\nOutstanding Balance", FONT_WHITE ));
              cell6.setBackgroundColor(new BaseColor(239, 139, 93));
              cell6.setHorizontalAlignment(Element.ALIGN_RIGHT);
              cell6.setPaddingRight(5)
              cell6.setPadding(5)
              cell6.setUseVariableBorders(true);
              cell6.setBorderColor(new BaseColor(239, 139, 93));
              cell6.setBorderWidth(1.5f)
              detailsTable.addCell(cell6)

              var cell7 = new PdfPCell(new Phrase(s"${bxPatientName}\n\n${bxEstDate}\n\n${bxDueDate}\n\n${formatter.format(bxPatientBalance)}", FONT_BLACK));
              cell7.setBackgroundColor(BaseColor.WHITE);
              cell7.setPaddingLeft(5)
              cell7.setPaddingTop(6)
              cell7.setPadding(5)
              cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
              cell7.setUseVariableBorders(true);
              cell7.setBorderWidth(1.5f)
              cell7.setBorderColor(new BaseColor(239, 139, 93));
              detailsTable.addCell(cell7)


              var columnWidths6:Array[Float] = Array(60,40);
              var headerTextTable1: PdfPTable = new PdfPTable(columnWidths6)
              headerTextTable1.setWidthPercentage(100);

              cell2 = new PdfPCell(new Phrase("Statement", FontFactory.getFont("https://s3.amazonaws.com/static.novadonticsllc.com/font/arialbd.ttf", 20)));
              cell2.setBorder(Rectangle.NO_BORDER);
              cell2.setPaddingTop(20);
              cell2.setPaddingLeft(30);
              cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell2)

              cell1 = new PdfPCell(detailsTable);
              cell1.setBorder(Rectangle.NO_BORDER);
              cell1.setPaddingTop(0);
              cell1.setPaddingLeft(0);
              cell1.setPaddingRight(80);
              cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              headerTextTable1.addCell(cell1)

              document.add(headerTextTable1);

              document.add(new Paragraph("\n"));


              var rowsPerPage = 8;
              var recNum =  0;
              var pageNum = 1

              (data).foreach(row => {

                  var sampleData = List(s"${row.datePlanned}", s"${row.cdtCode}", s"${row.tooth}",
                    s"${row.surface}",s"${row.providerName}",s"${row.fee}",s"${row.estIns}",s"${row.patientBalance}",s"${row.status}")

                  var columnWidths:Array[Float] = Array(11,11,8,8,12,12,12,15,11);
                  var table: PdfPTable = new PdfPTable(columnWidths);
                  table.setWidthPercentage(100);

                    if(pageNum == 1 || pageNum == 2 || recNum % rowsPerPage == 0) {

                    document.add(new Paragraph("\n"));

                    cell = new PdfPCell(new Phrase(" Date Planned", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("CDT code", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Tooth", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Surface", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Provider", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Fee", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Est.Ins", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Balance to Patient", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)

                    cell = new PdfPCell(new Phrase("Status", FONT_COL_HEADER));
                    cell.setPadding(13);
                    cell.setPaddingBottom(16);
                    cell.setBackgroundColor(new BaseColor(242, 244, 247));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBorder(Rectangle.NO_BORDER);
                    table.addCell(cell)
                  }

                  (sampleData).foreach(item => {

                    cell = new PdfPCell(new Phrase(item,FONT_CONTENT));
                    cell.setPadding(13);
                    cell.setBorder(Rectangle.TOP);
                    cell.setBorderColorTop(new BaseColor(221, 221, 221));
                    cell.setBackgroundColor(BaseColor.WHITE);
                    cell.setVerticalAlignment(Element.ALIGN_LEFT);
                    cell.setUseVariableBorders(true);
                    table.addCell(cell);
                  })

                  document.add(table);

                  if(pageNum > 1) {
                  recNum = recNum+1;
                  }
                  if((pageNum == 1 || recNum % rowsPerPage == 0) && (data.length != recNum+1)) {
                    document.newPage();
                  }
                  pageNum = pageNum + 1
                })


              document.add(new Paragraph("\n"));

          document.close();

        })(ioBoundExecutor)

        inputStream
      }

      val filename = s"Patient Statement"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
    }
  }
  def createImageCell2(img:Image,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element
    val indentation = 0
    //whatever
    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 27

    img.scalePercent(scaler)

    val cell = new PdfPCell(img);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }
  def createImageCell3(img:Image,height:Float,width:Float,border:Boolean, document:Document):PdfPCell ={
    import com.itextpdf.text.Element

    val indentation = 0
    //whatever
    val scaler = ((document.getPageSize.getHeight - document.leftMargin - document.rightMargin - indentation) / img.getHeight) * 13

    img.scalePercent(scaler)

    val cell = new PdfPCell(img);
    cell.setPadding(5);
    if (!border){
      cell.setBorder(Rectangle.NO_BORDER);
    }
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    return cell;
  }

  def patientTypeList(patientType: Option[String]) = silhouette.SecuredAction.async { request =>
    val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
    val patientValue = convertOptionString(patientType).split(',')
    var data = List.empty[PatientTypePDF]
    var treatmentIdList = List.empty[Long]

    db.run {
      TreatmentTable.filterNot(_.deleted).filterNot(_.patientType === "").filter(_.practiceId === practiceId)
      .join(ProviderTable).on(_.providerId === _.id).result
      } map { treatmentMap =>

    if(convertOptionString(patientType) != "" && convertOptionString(patientType) != None){
      var value = treatmentMap.groupBy(_._1.id).map {
      case (treatmentId, entries) =>
      val result = entries.head._1
      val provider = entries.head._2
      var patientName = ""
      var phoneNumber = ""
      var address = ""
      var emailAddress = ""
      var firstVisit = ""
      var providerName = ""

         for (v <- patientValue) {
             if(convertOptionString(result.patientType).toLowerCase contains v.toLowerCase){

              val block1 = db.run{
                StepTable.filter(_.formId === "1A").filter(_.treatmentId === result.id).sortBy(_.dateCreated.desc).result.head
              } map { stepMap =>

                    val first_name =  (stepMap.value \ "full_name").as[String]
                    val last_name =  (stepMap.value \ "last_name").as[String]

                    patientName = first_name +" "+ last_name
                    phoneNumber = (stepMap.value \ "phone").as[String]
                    address =  (stepMap.value \ "street").as[String]
                    emailAddress =  (stepMap.value \ "email").as[String]
                    firstVisit = (stepMap.value \ "first_visit").as[String]
                    providerName = s" ${provider.title} ${provider.firstName} ${provider.lastName}"

            }
              val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(120, SECONDS))

              val treatmentId = convertOptionLong(result.id)

                  if(!treatmentIdList.contains(treatmentId)){
                  var dataList = PatientTypePDF(treatmentId,patientName,firstVisit,result.phoneNumber,address,result.emailAddress,providerName,convertOptionString(result.patientType))
                    data = data :+ dataList
                    treatmentIdList = List(treatmentId)
                }
              }
            }
        }
      }
        Ok(Json.toJson(data.sortBy(_.treatmentId)))
      }
  }

  def referralSourceList(fromDate: Option[String],toDate: Option[String],referralSource: Option[String])=silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var query = TreatmentTable.filterNot(_.deleted)
  var vReferralSource = convertOptionString(referralSource).toLowerCase
  if(vReferralSource != "" && vReferralSource != "all"){
      query = query.filter(_.referralSource.toLowerCase === vReferralSource)
  }
    db.run {
      query
        .filter(s => s.firstVisit >= DateTime.parse(convertOptionString(fromDate)) && s.firstVisit <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59))
        .join(StaffTable).on(_.staffId === _.id).filter(_._2.practiceId === practiceId).result
    } map { referralSourcedata =>

      val data = referralSourcedata.groupBy(_._1.referralSource).mapValues(_.size).map{result =>
        val referer  = convertOptionString(result._1)
        val size  = result._2

            ReferralReport(referer,size,"","","","")

        }.toList.sortBy(_.size).reverse
      Ok(Json.toJson(data))
    }
  }


def viewReferralSource(fromDate: Option[String],toDate: Option[String],referralSource: Option[String])=silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var query = TreatmentTable.filterNot(_.deleted)
  var vReferralSource = convertOptionString(referralSource).toLowerCase
  if(vReferralSource != "" && vReferralSource != "all"){
     query = query.filter(_.referralSource.toLowerCase === vReferralSource)
  }
    db.run {
      query
        .filter(s => s.firstVisit >= DateTime.parse(convertOptionString(fromDate)) && s.firstVisit <= DateTime.parse(convertOptionString(toDate)).plusHours(23).plusMinutes(59).plusSeconds(59)).filter(_.practiceId === practiceId).join(ProviderTable).on(_.providerId === _.id).result
    } map { referralSourcedata => println(referralSourcedata.length)
      val data = referralSourcedata.groupBy(_._1.id).map {
        case (treatmentId, entries) =>
        val treatment = entries.head._1
        val provider = entries.head._2
        var patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
        var providerName = s"${provider.title} ${provider.firstName} ${provider.lastName}"
        ReferralReport(convertOptionString(treatment.referralSource),0,patientName,treatment.phoneNumber,treatment.emailAddress,providerName)
        // pdfList = pdfList :+ dataList
      }.toList.sortBy(_.referralSource)
      Ok(Json.toJson(data))
    }
  }


  def patientTypeStatus = silhouette.SecuredAction.async { request =>
      var patientStatus = "Active,Prospect,Inactive,NonPatient,Archived,Deceased"
      var patientType = "General Dentistry,Cosmetic Dentistry,Partial Denture Upper,Partial Denture Lower,Full Denture Upper,Full Denture Lower,Bone Grafting,Overdenture Upper,Overdenture Lower,Implant Prosthodontics Fixed,Implant Patient Single,Implant Patient Multi,All on X,Sinus Augmentation,Block Grafting,Perio,RCT,Whitening,Invisalign,Pedodontics,Implant Consultation Patient,General Dentistry Consultation Patient,Cosmetic Dentistry Consultation Patient,Orthodontics Consultation Patient"

        val obj = Json.obj()
        val newObj = obj + ("patientStatus" -> JsString(patientStatus)) + ("patientType" -> JsString(patientType))
        Future(Ok{Json.toJson(Array(newObj))})

  }

  def migratePatientType = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
      db.run{
          TreatmentTable.result
      } map { treatmentMap =>
      treatmentMap.foreach(result => {

        var s = convertOptionString(result.patientType).replace(", ",",")
        db.run {
         TreatmentTable.filter(_.id === result.id).map(_.patientType).update(Some(s))
        } map {
        case 0 => NotFound
        case _ => Ok
        }
      })
      Ok { Json.obj("status"->"success","message"->"Patient type migrated successfully")}
      }
  }

def migrateTreatmentInsurance = silhouette.SecuredAction.async { request =>
   db.run {
     TreatmentTable.filterNot(_.deleted).result
    } map { treatmentResult =>
    treatmentResult.foreach(treatment => {
      var block = db.run{
            StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepResult =>
              var insurance =  (stepResult.value \ "carrierName").as[String]

              db.run{
             TreatmentTable.filter(_.id === treatment.id).map(_.insurance).update(insurance)
                    }map{
                    case 0L => PreconditionFailed
                    case id => Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
                  }
              }
          val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    })
    Ok{Json.obj( "status"->"success","message"->"Form Migrated successfully")}
    }
}

case class ProcedureRow(
  recallType: String,
  code: String,
  description: String
)
implicit val ProcedureFormat = Json.format[ProcedureRow]

def recallProcedure = silhouette.SecuredAction.async { request =>
var procedureList = List.empty[ProcedureRow]
var procedureCodeList = List("D1110","D1120","D4910","D0274","D0210","D0330","D0367","D1999")

           var block =  db.run {
                  ProcedureTable.filterNot(_.deleted).filter(_.procedureCode inSet procedureCodeList).result
                } map { procedureResult =>
                procedureResult.foreach(result => {
                var dataList = ProcedureRow(procedureMap(result.procedureCode),result.procedureCode,result.procedureType)
                procedureList = procedureList :+ dataList
                })
              }
              val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

          Future(Ok(Json.toJson(procedureList)))
}


def importPatient = silhouette.SecuredAction.async(parse.json) { request =>
val url = "https://s3.amazonaws.com/uploads.novadonticsllc.com/dtxlm22_csv_medalerts.csv"
var count: Int = 0
var line: String = null
val practiceId = 37
var providerName = "Dr.Ehsan Nasery"
var providerId = 98

	val response = Http(url)
          .header("Content-Type", "text/xml")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(10000)).asString


  val stream: java.io.InputStream = new java.io.ByteArrayInputStream(response.body.getBytes(java.nio.charset.StandardCharsets.UTF_8.name))
  val reader = new BufferedReader(new InputStreamReader(stream));

      var feeScheduleMap:scala.collection.mutable.Map[String,Long]=scala.collection.mutable.Map()
      var feeBlock = db.run{
        InsuranceCompanyTable.sortBy(_.id).result
      } map { icMap => icMap.map{r => feeScheduleMap.update(r.name,convertOptionLong(r.id))}}
      var AwaitResult1 = Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

  line = reader.readLine()
    while (line  != null) {

    if(count != 0) {
      if(line != null && line != None && line != ""){

      val cols = line.split(",").map(_.trim)

      var patientValue = """{"mi":"rMi","ss#":"rSs","zip":"rZip","city":"rCity","email":"rEmail","phone":"rPhone","state":"rState","gender":"rGender","street":"rStreet","position":"rPosition","provider":"rProvider","full_name":"rFullName","last_name":"rLastName","birth_date":"rBirthDate","patient_id":"rPatientId","feeSchedule":"rFeeSchedule","first_visit":"rFirstVisit","preferredName":"rPreferredName","business_phone":"rBusinessPhone","chiefComplaint":"rChiefComplaint","driversLicense":"rDriversLicense","patient_status":"rPatient_status","patientRefererTo":"rPatientRefererTo","referredDate":"rReferredDate","patient_referer":"rPatient_referer",medicalCondition,allergies}"""

          var gender = "unknown"
          if(cols(13) == "F"){
              gender = "female"
          } else if(cols(13) == "M"){
              gender = "male"
          }

      var allergyList: String = ""
      var medicalConditionList: String = ""
      var mcJson = """"medCondition": "{★checked★:true}""""
      cols(15).split(";").map{s=>
      var mcMappedValue = medicalConditionMap.find(_._2 == s).map(_._1)
      if(mcMappedValue != None && mcMappedValue != null){
      var mcReplace = mcJson.replace("medCondition", convertOptionString(mcMappedValue))
      medicalConditionList =  medicalConditionList + mcReplace + ","
      }

      var allergyJson = """"allergy": "{★checked★:true}""""
      var allergyMappedValue = allergyMap.find(_._2 == s).map(_._1)
      if(allergyMappedValue != None && allergyMappedValue != null){
      var allergyReplace = allergyJson.replace("allergy", convertOptionString(allergyMappedValue))
      allergyList = allergyList + allergyReplace + ","
      }
    }
      var feeSchedule = ""
      if(insuranceCompanyMap contains cols(30)){
        feeSchedule = insuranceCompanyMap(cols(30))
      } else {
        feeSchedule = cols(30)
      }

      if (feeScheduleMap.keySet.exists(_ == feeSchedule)){
          feeSchedule = feeScheduleMap(feeSchedule).toString
      }

      patientValue = patientValue.replace("rMi", cols(2)).replace("rSs", cols(11)).replace("rZip", cols(7)).replace("rCity", cols(5)).replace("rEmail", cols(25)).replace("rPhone", cols(8)).replace("rState", cols(6)).replace("rGender", gender).replace("rStreet", cols(3)).replace("rPosition", cols(16)).replace("rProvider", providerId.toString).replace("rFullName", cols(1)).replace("rLastName", cols(0)).replace("rBirthDate", cols(17)).replace("rPatientId", cols(34)).replace("rFeeSchedule", feeSchedule).replace("rFirstVisit", cols(19)).replace("rPreferredName", cols(18)).replace("rBusinessPhone", cols(9)).replace("rChiefComplaint", "").replace("rDriversLicense", cols(26)).replace("rPatient_status", "Active").replace("rPatient_referer", cols(21)).replace("rPatientRefererTo",cols(35)).replace("rReferredDate",cols(20))

      var mcReplace = ""
      var allergyReplace = ""
      if(medicalConditionList != ""){
        mcReplace = patientValue.replace("medicalCondition", medicalConditionList.dropRight(1))
      } else {
        mcReplace = patientValue.replace(",medicalCondition", medicalConditionList)
      }

      if(allergyList != ""){
        allergyReplace = mcReplace.replace("allergies", allergyList.dropRight(1))
      } else {
        allergyReplace = mcReplace.replace(",allergies", allergyList)
      }

        var firstVisit: Some[DateTime] = null
        if(cols(19) != null && cols(19) != ""){
          firstVisit = Some(DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(cols(19)))
        }

        var referredDate: Some[DateTime] = null
        if(cols(20) != null && cols(20) != ""){
          referredDate = Some(DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(cols(20)))
        }

        var treatmentRow = TreatmentRow(None, cols(34), firstVisit, null, cols(8), cols(25), request.identity.id.get, Some(""), Some(""), Some(""),Some(""), Some(""), None, Some(""),true,Some(""),Some(""),Some("Active"),Some(false),Some(""),Some(""),null,Option.empty[Long],null,"",Some(""),Some(""),Some(""),Option.empty[Long],Some(""),null,Some(false),null,Some(""),Some(""),Some(cols(1)),Some(cols(0)),Some(cols(17)),Some(providerId),Some(practiceId),Some(false),Some(false),Some(""),Some(""),Some(cols(35).toLong),referredDate,Some(""),Some(false),Some(false),Some(false),Some(""),None,Some(false),Some(""),None,Some("No"),Some(""),Some(""),None)

         var block = db.run{
            TreatmentTable.returning(TreatmentTable.map(_.id)) += treatmentRow
          } map {
              case 0L => PreconditionFailed
              case id =>
              var block = db.run {
                StepTable += StepRow(Json.parse(allergyReplace), StepRow.State.Completed, id, "1A", 1, DateTime.now)
              }
            }
          val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
         }
      }
    count += 1
    line = reader.readLine()
    }
    reader.close();
    Future.successful(Ok{Json.obj("status"->"success","message"->"Patients imported successfully")})
  }

case class DoseSpotSEncryptedCode(encryptedClinicId:String,encryptedUserId:String)

def generateDoseSpotEncryptedCodes(clinicCode:String,userId:String):DoseSpotSEncryptedCode = {
    var randomClinicPhrase = Random.alphanumeric take 32 mkString
    var randomPhraseClinicKey = randomClinicPhrase + clinicCode


    var sha512ClinicString = MessageDigest.getInstance("SHA-512").digest(randomPhraseClinicKey.getBytes("UTF-8"))
    var base64ClinicString = Base64.getEncoder.encodeToString(sha512ClinicString)
    var encryptedClinicId = base64ClinicString
    if (base64ClinicString.takeRight(2)=="=="){
       encryptedClinicId = base64ClinicString.dropRight(2)
    }
    encryptedClinicId = randomClinicPhrase + encryptedClinicId

    var randomUserPhrase = randomClinicPhrase.take(22)
    var randomUserKey = userId + randomUserPhrase
    var appendClinicKey = randomUserKey + clinicCode

    var sha512UserString = MessageDigest.getInstance("SHA-512").digest(appendClinicKey.getBytes("UTF-8"))
    var base64UserString = Base64.getEncoder.encodeToString(sha512UserString)
    var encryptedUserId = base64UserString
    if (base64UserString.takeRight(2)=="=="){
       encryptedUserId = base64UserString.dropRight(2)
    }


    val DoseSpotSEncryptedCodeObj = new DoseSpotSEncryptedCode(encryptedClinicId, encryptedUserId);
    return DoseSpotSEncryptedCodeObj;
}

def getAccessToken(doseSpotUrl: String,clinicId:String,clinicKey:String,userId:String):String = {
  val DoseSpotSEncryptedCodeObj:DoseSpotSEncryptedCode=generateDoseSpotEncryptedCodes(clinicKey,userId)
  val url = s"${doseSpotUrl}webapi/token"
  val encryptedClinicId = DoseSpotSEncryptedCodeObj.encryptedClinicId
  val encryptedUserId = DoseSpotSEncryptedCodeObj.encryptedUserId

  val result = Http(url)
  .header("Content-Type", "application/x-www-form-urlencoded")
  .header("Charset", "UTF-8")
  .auth(clinicId,encryptedClinicId)
  .postForm(Seq(
    "grant_type" -> "password",
    "Username" -> s"$userId",
    "Password" -> s"$encryptedUserId"))
    .option(HttpOptions.readTimeout(10000)).asString

  val jsonObject: JsValue = Json.parse(result.body)
  var accessToken:String=""

  if (result.code==200){
    accessToken = (jsonObject \ "access_token").as[String]
  }
  return accessToken;
}

def getDoseSpotUrl(doseSpotUrl:String, clinicId:String, clinicKey:String, userId:String, patientId:String):String = {
  val DoseSpotSEncryptedCodeObj:DoseSpotSEncryptedCode = generateDoseSpotEncryptedCodes(clinicKey,userId)
  val encryptedClinicId = DoseSpotSEncryptedCodeObj.encryptedClinicId
  val encryptedUserId = DoseSpotSEncryptedCodeObj.encryptedUserId

  var value = ""
  if(patientId != "0"){
      value = "&PatientID="+patientId
  } else {
      value = "&RefillsErrors=1"
  }
  val url = s"${doseSpotUrl}LoginSingleSignOn.aspx?SingleSignOnClinicId="+clinicId+"&SingleSignOnUserId="+userId+"&SingleSignOnPhraseLength=32&SingleSignOnCode="+URLEncoder.encode(encryptedClinicId)+"&SingleSignOnUserIdVerify="+URLEncoder.encode(encryptedUserId)+value;
  return url;
}

def getDoseSpotUIUrl(patientId : Long) = silhouette.SecuredAction.async { request =>
    val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
    var errorFound = false
    var message = ""
    var doseSpotMessage = ""
    var userId = ""

    db.run{
        TreatmentTable.filter(_.id === patientId).join(StaffTable).on(_.staffId === _.id).join(PracticeTable).on(_._2.practiceId === _.id).join(StepTable).on(_._1._1.id === _.treatmentId).filter(_._2.formId === "1A").sortBy(_._2.dateCreated.desc).result
    } map { results =>

      val data = results.groupBy(_._1._1._1.id).map {
        case (treatmentId, entries) =>
        val treatment = entries.head._1._1._1
        val staff = entries.head._1._1._2
        val practice = entries.head._1._2
        val step = entries.head._2

    var block = db.run{
      StaffTable.filter(_.id === request.identity.id.get).result
    } map { staffMap =>
        userId = convertOptionString(staffMap.head.doseSpotUserId)
    }
    Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    val clinicKey = convertOptionString(practice.doseSpotClinicKey)
    val clinicId = convertOptionString(practice.doseSpotClinicId)
    var doseSpotPatientId = convertOptionString(treatment.doseSpotPatientId)

    if(doseSpotPatientId == ""){
      val accessToken:String = getAccessToken(doseSpotUrl,clinicId,clinicKey,userId)
      var firstName = convertOptionString((step.value \ "full_name").asOpt[String])
      var lastName = convertOptionString((step.value \ "last_name").asOpt[String])
      var birthOfDate = convertOptionString((step.value \ "birth_date").asOpt[String])
      var strGender = convertOptionString((step.value \ "gender").asOpt[String])
      var address = convertOptionString((step.value \ "street").asOpt[String])
      var city = convertOptionString((step.value \ "city").asOpt[String])
      var state = convertOptionString((step.value \ "state").asOpt[String])
      var zip = convertOptionString((step.value \ "zip").asOpt[String])
      var phone = convertOptionString((step.value \ "phone").asOpt[String])
      var middleName = convertOptionString((step.value \ "mi").asOpt[String])


    var gender = ""
    if(strGender == "male"){
      gender = "1"
    } else if(strGender == "female"){
      gender = "2"
    } else if(strGender == "unknown"){
      gender = "3"
    }

      val json: String ="""{"FirstName": "rFirstName","LastName": "rLastName","DateOfBirth": "rDateOfBirth","Gender": rGender,"Address1":"rAddress","City":"rCity","State":"rState","ZipCode":"rZip","PrimaryPhone":"rPrimaryPhone","MiddleName":"rMiddleName","PrimaryPhoneType":2,"Active":true}""".stripMargin

      var patientJson = json.replace("rFirstName", firstName).replace("rLastName", lastName).replace("rDateOfBirth", birthOfDate).replace("rGender", gender).replace("rAddress", address).replace("rCity", city).replace("rState", state).replace("rZip", zip).replace("rPrimaryPhone", phone).replace("rMiddleName",middleName)

      val url = s"${doseSpotUrl}webapi/api/patients"
      val result = Http(url)
      .postData(patientJson)
      .header("Content-Type", "application/json")
      .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(10000)).asString

      doseSpotMessage = result.body
      var response = (Json.parse(result.body) \ "Result").asOpt[JsValue]
      var resultCode = convertOptionString(response.flatMap(s => (s \ "ResultCode").asOpt[String]))

          if(resultCode == "OK"){
            doseSpotPatientId = convertOptionLong((Json.parse(result.body) \ "Id").asOpt[Long]).toString
            message = getDoseSpotUrl(doseSpotUrl,clinicId,clinicKey,userId,doseSpotPatientId)
            db.run {
            TreatmentTable.filter(_.id === patientId).map(_.doseSpotPatientId).update(Some(doseSpotPatientId))
            }
          } else {
                errorFound = true
                message = "Could not create record in DoseSpot for this patient. Please check the below fields have valid values. First Name, MI, Last Name, Date of birth, Gender, Cell Phone, Street, City, State. If you still face issue please email us at: team@novadontics.com or call us at: 888.838.6682."
          }
      } else {
          message = getDoseSpotUrl(doseSpotUrl,clinicId,clinicKey,userId,doseSpotPatientId)
      }
    }
    if(errorFound){
        val newObj = Json.obj() + ("Result" -> JsString("Failed")) + ("message" -> JsString(message)) + ("doseSpotMessage" -> JsString(doseSpotMessage))
        PreconditionFailed{Json.toJson(Array(newObj))}
    } else {
        val newObj = Json.obj() + ("Result" -> JsString("Success")) + ("message" -> JsString(message))
        Ok{Json.toJson(Array(newObj))}
      }
    }
  }

  def getDoseSpotNotificationCount = silhouette.SecuredAction.async { request =>
    val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
    var staffId = request.identity.id.get
    var message = ""
    var totalCount: Long = 0
    var errorFound = false

    db.run{
        StaffTable.filter(_.id === staffId).join(PracticeTable).on(_.practiceId === _.id).result
    } map { results =>

      val data = results.groupBy(_._1.id).map {
        case (treatmentId, entries) =>

        val staff = entries.head._1
        val practice = entries.head._2

      val clinicKey = convertOptionString(practice.doseSpotClinicKey)
      val clinicId = convertOptionString(practice.doseSpotClinicId)
      val userId = convertOptionString(staff.doseSpotUserId)

      val accessToken:String = getAccessToken(doseSpotUrl,clinicId,clinicKey,userId)

      val url = s"${doseSpotUrl}webapi/api/notifications/counts"
      val result = Http(url)
      .header("Content-Type", "application/json")
      .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(10000)).asString

      if(result.code == 200){
        message = getDoseSpotUrl(doseSpotUrl,clinicId,clinicKey,userId,"0")
        var refillRequestsCount = (Json.parse(result.body) \ "RefillRequestsCount").asOpt[Long].get
        var transactionErrorsCount = (Json.parse(result.body) \ "TransactionErrorsCount").asOpt[Long].get
        var pendingPrescriptionsCount = (Json.parse(result.body) \ "PendingPrescriptionsCount").asOpt[Long].get
        var pendingRxChangeCount = (Json.parse(result.body) \ "PendingRxChangeCount").asOpt[Long].get
        totalCount = refillRequestsCount + transactionErrorsCount + pendingPrescriptionsCount + pendingRxChangeCount
      } else {
        message = result.body
        errorFound = true
      }
    }

    if(errorFound){
      val newObj = Json.obj() + ("Result" -> JsString("Failed")) + ("message" -> JsString(message))
      PreconditionFailed{Json.toJson(Array(newObj))}
    } else{
      val newObj = Json.obj() + ("Result" -> JsString("Success")) + ("totalCount" -> JsString(totalCount.toString)) + ("message" -> JsString(message))
      Ok{Json.toJson(Array(newObj))}
    }
  }
  }

  def migrateMcFamilyAlert(startId: Long, endId: Long) = silhouette.SecuredAction.async { request =>
    var block1 = db.run{
      TreatmentTable.filterNot(_.deleted).filter(s => s.id >= startId && s.id <= endId).result
    } map { treatmentMap =>
        treatmentMap.foreach(treatmentResult => {
          var isMedicalCondition = false
          var isFamily = false
          var alerts = ""
           var block2 = db.run{
              MedicalConditionLogTable.filter(_.patientId === treatmentResult.id).filter(_.currentlySelected === true).result
            } map { mcLog =>
                if(mcLog.length > 0)  {
                  isMedicalCondition = true
                }
                mcLog.foreach(m => { alerts = alerts + m.value + ", "})
            }
            val AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))
            //IS FAMILY MIGRATION
                var block3 = db.run{
                    FamilyMembersTable.filter(_.patientId === treatmentResult.id).sortBy(_.familyId).result.head
                  } map { familyMap =>
                      db.run{
                        FamilyMembersTable.filter(_.familyId === familyMap.familyId).result
                      } map { familyLength =>
                      familyLength.foreach(r => {
                          if(familyLength.length > 1){
                         var b = db.run{
                            TreatmentTable.filter(_.id === r.patientId).map(_.isFamily).update(Some(true))
                          }
                          val AwaitResult = Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                        }
                      })
                      }
                    }
            val AwaitResult3 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var block4 = db.run {
          val query = TreatmentTable.filter(_.id === treatmentResult.id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            Some(isFamily).map(value => query.map(_.isFamily).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(isMedicalCondition).map(value => query.map(_.isMedicalCondition).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(alerts.dropRight(2)).map(value => query.map(_.alerts).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))


          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      val AwaitResult4 = Await.ready(block4, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        })
    }
    val AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(90, SECONDS))
    Future(Ok{Json.obj("status"->"success","message"->"Migration successful")})
  }

def updateExamImaging(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val implant = for {
      examImaging <- (request.body \ "examImaging").validate[String]
    } yield {
      (examImaging)
    }
    implant match {
      case JsSuccess((examImaging), _) => db.run {
        val query = TreatmentTable.filter(_.id === patientId)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            Some(examImaging).map(value => query.map(_.examImaging).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def migrateCarrierName(startId: Long, endId: Long) = silhouette.SecuredAction.async { request =>
   db.run {
      TreatmentTable.filterNot(_.deleted).filter(s => s.id >= startId && s.id <= endId).result
    } map { treatmentResult =>
      treatmentResult.map{ treatment =>

          var block = db.run{
            StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepResult =>
              var carrierName =  (stepResult.value \ "carrierName").as[String]

              var carrierId = ""
                var carrierNameList = db.run {
                  DentalCarrierTable.filter(_.name === carrierName).result
                    } map { carrierList =>
                    if(carrierList.length > 0){
                      carrierList.foreach(result => {
                        carrierId = convertOptionLong(result.id).toString
                      })
                    } else {
                      carrierId = "-1"
                    }
                  }
                Await.ready(carrierNameList, atMost = scala.concurrent.duration.Duration(30, SECONDS))

               var stepSchema: String = Json.stringify(stepResult.value)
               var s = stepSchema.replace(carrierName,carrierId)
               var schema = Json.parse(s);

               db.run{
                 StepTable.filter(_.treatmentId === treatment.id).filter(_.formId === "1A").filter(_.dateCreated === stepResult.dateCreated).map(_.value).update(schema)
               } map {
                  case 0L => PreconditionFailed
                  case id => Ok
               }
            }
            val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
          }
          Ok{Json.obj( "status"->"success","message"->"Carrier name Migrated successfully")}
    }
  }

def migrateContracts = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
      for{
          treatmentList <- TreatmentTable.filterNot(_.deleted).result
      } yield {
            (treatmentList).foreach(treatment => {
              if(convertOptionString(treatment.contracts) != ""){
                db.run{
                  DocumentTable.returning(DocumentTable.map(_.id)) += DocumentRow(None,convertOptionLong(treatment.id),"Contracts",convertOptionString(treatment.contracts))
                }
              }
            })
        Ok {Json.obj("status"->"success","message"->"Contracts Migrated Successfully")}
        }
     }
  }

def updateDocument(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
  val document = for {
      documentType <- (request.body \ "documentType").validate[String]
      documents <- (request.body \ "documents").validate[String]
    } yield {
      (documentType,documents)
    }

  document match {
      case JsSuccess((documentType,documents), _) => db.run {
        val query = DocumentTable.filter(_.patientId === patientId).filter(_.documentType.toLowerCase === documentType.toLowerCase)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
          Some(documents).map(value => query.map(_.documents).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
            ) map {_ => Ok}
          case false => db.run{
          DocumentTable.returning(DocumentTable.map(_.id)) += DocumentRow(None,patientId,documentType,documents)} map {
              case 0L => PreconditionFailed
              case id => Ok
            }
            DBIO.successful(Ok)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

def getDocuments(patientId: Long) = silhouette.SecuredAction.async { request =>
     db.run {
          DocumentTable.filter(_.patientId === patientId).result
      } map { documentList =>
        Ok(Json.toJson(documentList))
      }
  }

  def getEndoChartData(patientId: Long, date: Option[String]) = silhouette.SecuredAction.async { request =>
  var vDate = convertOptionString(date)
  var query = EndoChartTable.filter(_.patientId === patientId)
  if(vDate != ""){
    query = query.filter(_.dateSelected === LocalDate.parse(vDate))
  }
    db.run{
      query.sortBy(_.dateSelected).sortBy(_.rowPosition.desc).result
    } map { rows =>
      Ok(Json.toJson(rows))
    }
  }

  case class EndoChartDateEntryRow(
    dateSelected: LocalDate
  )

  implicit val EndoChartDateEntryFormat = Json.format[EndoChartDateEntryRow]

  def getEndoChartEntryDates(patientId: Long) = silhouette.SecuredAction.async { request =>
  var endoChartList = List.empty[EndoChartDateEntryRow]
  var dm  = List[String]()
    db.run{
      EndoChartTable.filter(_.patientId === patientId).sortBy(_.dateSelected.desc).result
    } map { rows =>
        rows.foreach(result => {
          if(!(dm contains result.dateSelected.toString)){
            dm = result.dateSelected.toString :: dm
          var dataList = EndoChartDateEntryRow(result.dateSelected)
          endoChartList = endoChartList :+ dataList
          }
        })
      Ok(Json.toJson(endoChartList))
    }
  }
  def updateEndoChartData(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    var staffId:Long = request.identity.id.get

    val perioChartInput = for {
      rowId <- (request.body \ "id").validateOpt[Long]
      rowPosition <- (request.body \ "rowPosition").validate[String]
      dateSelectedVal <- (request.body \ "dateSelected").validate[String]
      data <- (request.body \ "data").validate[JsValue]
      providerId <- (request.body \ "providerId").validateOpt[Long]

    } yield {
      val dateSelected = LocalDate.parse(dateSelectedVal)
      (rowId,rowPosition,dateSelected,data,providerId)
    }

    perioChartInput match {
      case JsSuccess((rowId,rowPosition,dateSelected,data,providerId), _) =>
          if (dateSelected == None){
            Future.successful(BadRequest(Json.parse("""{"error":"Invalid date"}""")))
          }else{
            if (rowId == None || rowId == Some(0)){
              db.run{
                EndoChartTable.returning(EndoChartTable.map(_.id)) += EndoChartRow(None,patientId,rowPosition,dateSelected,data,staffId,providerId)
              }map{
                  result => Ok
              }
            }else{
             db.run{
                var query = EndoChartTable.filter(_.id === rowId )
                DBIO.seq[Effect.Read with Effect.Write](
                  query.map(_.rowPosition).update(rowPosition).map(_ => Unit),
                  query.map(_.dateSelected).update(dateSelected).map(_ => Unit),
                  query.map(_.data).update(data).map(_ => Unit),
                  query.map(_.staffId).update(staffId).map(_ => Unit),
                  query.map(_.providerId).update(providerId).map(_ => Unit)
                )map { _ =>
                    Ok
                  }
              }
            }
          }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

def addToothCondition(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    var staffId:Long = request.identity.id.get
    var now = DateTime.now()
    val addTooth = for {
      toothId <- (request.body \ "toothId").validate[List[Int]]
      condition <- (request.body \ "condition").validate[String]
    } yield {
      (toothId).foreach(id => {
        var b = db.run{
          ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === id).result
        } map { toothChart =>
        if(toothChart.length > 0){
          var vExistingConditions = toothChart.head.toothConditions+ "," + condition
            db.run{
              ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === id).map(_.existingConditions).update(Some(vExistingConditions))
            }
            db.run{
              ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === id).map(_.dateUpdated).update(now)
            }
        } else {
            db.run {
              ToothChartTable.returning(ToothChartTable.map(_.id)) += ToothChartRow(None,patientId,id,"",staffId,0,now,Some(condition))
            }
          }
        }
        Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      })
    }
Future(Ok)
  }

def removeToothCondition(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    var staffId:Long = request.identity.id.get
    var now = DateTime.now()
    val addTooth = for {
      toothId <- (request.body \ "toothId").validate[List[Int]]
      condition <- (request.body \ "condition").validate[String]
    } yield {
      (toothId).foreach(id => {
        var b = db.run{
          ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === id).result.head
        } map { toothChart =>
        if(toothChart != None){
          if(convertOptionString(toothChart.existingConditions) contains condition){
            var vExistingConditions = convertOptionString(toothChart.existingConditions).replaceAll(condition+",", "").replaceAll(","+condition, "").replaceAll(condition, "")
            db.run{
              ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === id).map(_.existingConditions).update(Some(vExistingConditions))
            }
          }
        }
        }
        Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      })
    }
Future(Ok)
  }

def readExistingConditions(patientId: Long, toothId: Int) = silhouette.SecuredAction.async { request =>
  db.run{
    ToothChartTable.filter(_.patientId === patientId).filter(_.toothId === toothId).result
} map { toothList =>
  Ok(Json.toJson(toothList))
  }
}

def getToothConditions(patientId: Long) = silhouette.SecuredAction.async { request =>
var toothList = List.empty[ToothConditionsRow]
  db.run{
    TreatmentTable.filter(_.id === patientId).result.head
} map { result =>
          var dataList = ToothConditionsRow(result.id,result.missingTooth,result.edentulousMaxilla,result.edentulousMandible,result.allPrimary,result.primaryTooth)
          toothList = toothList :+ dataList
  Ok(Json.toJson(toothList))
  }
}

def setMissingTooth(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
var query = TreatmentTable.filter(_.id === patientId)
var toothList: List[Any] = List()
var regex = "[0-9, /,]+";

    val setTooth = for {
      toothNumber <- (request.body \ "toothNumber").validate[String]
    } yield {
        db.run{
          query.result.head
        } map { results =>
            var eMissingTooth = convertOptionString(results.missingTooth)
            if(eMissingTooth != ""){ toothList = List(eMissingTooth) }

            (toothNumber).split(",").map{ toothValue =>
                if(!eMissingTooth.split(",").contains(toothValue)){
                  toothList = toothList :+ toothValue
              }
        }
        var b = db.run{
        query.map(_.missingTooth).update(Some(toothList.mkString(",")))
        }
        Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      }
    }
  Future(Ok{Json.obj("result" -> "Updated successfully.")})
  }

def unsetMissingTooth(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
var query = TreatmentTable.filter(_.id === patientId)
var toothList: List[String] = List()
var regex = "[0-9, /,]+";

      val unsetTooth = for {
      toothNumber <- (request.body \ "toothNumber").validate[String]
    } yield {
        db.run{
          query.result.head
        } map { results =>
        var eMissingTooth = convertOptionString(results.missingTooth)
        (eMissingTooth).split(",").map{ toothValue =>
            if(!toothNumber.split(",").contains(toothValue)){
              toothList = toothList :+ toothValue
          }
        }
           var b = db.run{
                query.map(_.missingTooth).update(Some(toothList.mkString(",")))
                }
            Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
    }
  Future(Ok{Json.obj("result" -> "Updated successfully.")})
  }


def setPrimaryTooth(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
var query = TreatmentTable.filter(_.id === patientId)
var toothList: List[String] = List()
var toothArray: Array[Any] = Array()
    val setTooth = for {
      toothNumber <- (request.body \ "toothNumber").validate[String]
    } yield {
        db.run{
          query.result.head
        } map { results =>
        var ePrimaryTooth = convertOptionString(results.primaryTooth)
          if(ePrimaryTooth != ""){ toothList = List(ePrimaryTooth) }
          (toothNumber).split(",").map{ toothValue =>
              if(!ePrimaryTooth.split(",").contains(toothValue)){
                toothList = toothList :+ toothValue
            }
        }
            var b = db.run{
              query.map(_.primaryTooth).update(Some(toothList.mkString(",")))
            }
            Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
    }
  Future(Ok{Json.obj("result" -> "Updated successfully.")})
  }


def unsetPrimaryTooth(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
var query = TreatmentTable.filter(_.id === patientId)
var toothList: List[String] = List()
    val unsetTooth = for {
      toothNumber <- (request.body \ "toothNumber").validate[String]
    } yield {
        db.run{
          query.result.head
        } map { results =>
        var ePrimaryTooth = convertOptionString(results.primaryTooth)
        (ePrimaryTooth).split(",").map{ toothValue =>
            if(!toothNumber.split(",").contains(toothValue)){
              toothList = toothList :+ toothValue
          }
        }
            var b = db.run{
                query.map(_.primaryTooth).update(Some(toothList.mkString(",")))
                }
            Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))
        }
    }
  Future(Ok{Json.obj("result" -> "Updated successfully.")})
  }


def setEdentulousMaxilla(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.edentulousMaxilla).update(Some(true))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }


def unsetEdentulousMaxilla(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.edentulousMaxilla).update(Some(false))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }


def setEdentulousMandible(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.edentulousMandible).update(Some(true))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }


def unsetEdentulousMandible(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.edentulousMandible).update(Some(false))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }


def setAllPrimary(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.allPrimary).update(Some(true))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }


def unsetAllPrimary(patientId: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    db.run {
        TreatmentTable.filter(_.id === patientId).map(_.allPrimary).update(Some(false))
      } map {
        case 0 => NotFound
        case _ => Ok{Json.obj("result" -> "Updated successfully.")}
      }
  }

  def onlinePatientCreate = silhouette.SecuredAction(CanCreateTreatment).async(parse.json) { request =>
    var vFirstName = ""
    var vLastName = ""
    var vPhoneNumber = ""
    var vEmailAddress = ""
    var vCountry = ""
    var patientExist = false
    val treatment = for {
      steps <- (request.body \ "steps").validate[List[Step]]
    } yield {
      (steps).foreach(step=>{
        
            if (step.formId == "1A"){
                val jsonObject = Json.parse(step.value.toString())

                  (jsonObject \ "full_name").validate[String] match {
                    case JsSuccess(fullName, _) =>
                      vFirstName =s"$fullName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "last_name").validate[String] match {
                    case JsSuccess(lastName, _) =>
                      vLastName =s"$lastName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "phone").validate[String] match {
                    case JsSuccess(phoneNumber, _) =>
                      vPhoneNumber =s"$phoneNumber"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "email").validate[String] match {
                    case JsSuccess(emailId, _) =>
                      vEmailAddress =s"$emailId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "country").validate[String] match {
                    case JsSuccess(country, _) =>
                      vCountry =s"$country"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
            }
      })
      (vPhoneNumber, vEmailAddress, vFirstName,vLastName,vCountry,steps)
    }
    
    val now = DateTime.now
    
    treatment match {
      case JsSuccess((vPhoneNumber, vEmailAddress, vFirstName,vLastName, vCountry,steps), _) =>
          var patientExists=false
          var practiceId = request.identity.asInstanceOf[StaffRow].practiceId

          var patientId = ""
          var patientCreation = false
           var pb = db.run{
              PracticeTable.filter(_.id === practiceId).result.head
            } map { practice =>
              if(convertOptionString(practice.chartNumberType).toLowerCase == "alphanumeric"){
                  var idExists = true
                  var df: DecimalFormat = new DecimalFormat("0000")
                  var i = 0

                  while(idExists){
                    i = i+1
                    patientId = vLastName.take(2).toUpperCase + df.format(i)
                     var f =  db.run{
                     for {
                        count <- TreatmentTable.filter(_.practiceId === practiceId).filter(_.patientId === patientId).length.result
                      } yield {
                        if(count == 0){
                          idExists = false
                        }
                      }
                    }
                    Await.ready(f, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }

              } else if(convertOptionString(practice.chartNumberType).toLowerCase == "numeric") {
                    var df: DecimalFormat = new DecimalFormat("000000")
                    var chartNumberSeq = (convertOptionLong(practice.chartNumberSeq))
                    patientId = df.format(chartNumberSeq + 1)

                      db.run {
                          PracticeTable.filter(_.id === practiceId).map(_.chartNumberSeq).update(Some(patientId.toLong))
                      } map {
                          case 0 => NotFound
                          case _ => Ok
                        }
                }

              if(convertOptionalBoolean(practice.onlineRegistrationModule) == true){
                  patientCreation = true
              }

          }
          Await.ready(pb, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        if(patientCreation){
              var phoneValidation = false
              var phone = vPhoneNumber.replaceAll("[^a-zA-Z0-9]", "")
              if(phone.length != 10 || phone.startsWith("0") || phone.startsWith("1") || phone.startsWith("1234")){
                  phoneValidation = true
                }

               var patientBlock =  db.run{
                     for {
                        count <- TreatmentTable.filter(_.firstName === vFirstName).filter(_.lastName === vLastName).filter(_.phoneNumber === vPhoneNumber).length.result
                      } yield {
                        if(count > 0){
                          patientExist = true
                        }
                      }
                    }
              Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

          if(phoneValidation)
                  Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
          else if(patientExist)
                  Future.successful(BadRequest(Json.parse("""{"error":"Patient already exists"}""")))
          else {
            val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
            var stepValue: JsValue = null
            db.run {
                (TreatmentTable.returning(TreatmentTable.map(_.id)) += TreatmentRow(None, patientId, None, Some(now), vPhoneNumber, vEmailAddress, request.identity.id.get, Some(""), Some(""), Some(""),Some(""), Some(""),None,Some(""),true,Some(""),Some(""),Some(""),Some(false),Some(""),Some(""),Some(null),Option.empty[Long],None,"",Some(""),Some(""),Some(""),Option.empty[Long],Some(""),null,Some(false),null,Some(""), Some(""),Some(vFirstName),Some(vLastName),Some(""),None,Some(practiceId),Some(false),Some(false),Some(""),Some(""),None,None,Some(""),Some(false),Some(false),Some(false),Some(""),Some(DateTime.now),Some(false),Some(""),None,Some("Yes"),Some("Invitation Sent"),Some(vCountry),None)).flatMap { id =>

                   (steps).foreach(step=>{
                     stepValue = step.value
                    var newObj =  Json.obj() + ("onlineRegStatus" -> JsString("Invitation Sent")) + ("patient_id" -> JsString(patientId))
                    stepValue = stepValue.as[JsObject].deepMerge(newObj.as[JsObject])
                   })
                      var pb = db.run{
                      PracticeTable.filter(_.id === practiceId).result.head
                      } map { practice =>

                      var msgContent = practice.onlineInviteMessage 
                      var patientName = vFirstName + " " +vLastName
                      val emailService = new EmailService(ses, application)

                      val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
                      val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
                      var subject = "Online Registration form"
                      val patientSiteBaseUrl = application.configuration.getString("aws.ses.appointmentReminder.patientSiteBaseUrl").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.patientSiteBaseUrl` property."))

                      val reminderUrlParams = "patientId=" + id
                      var encrStr: String = encryptMethod(reminderUrlParams);

                      var formLink = patientSiteBaseUrl + "/onlineRegistration" + "/" + encrStr
                      var content = convertOptionString(msgContent)
                      var finalContent = content.replaceAll("<<link>>", formLink).replaceAll("<<patient name>>", patientName)

                      if(msgContent != " " && vPhoneNumber != "" && vEmailAddress != ""){
                        var smsResponse = sendSMS(vPhoneNumber,finalContent,vCountry)
                        emailService.sendReminder(List(vEmailAddress), from, replyTo,subject, finalContent)

                        var onlineRegInviteTrackRow = OnlineRegInviteTrackRow(None,id,DateTime.now)  
                           db.run {
                            OnlineRegInviteTrackTable.returning(OnlineRegInviteTrackTable.map(_.id)) += onlineRegInviteTrackRow              
                          } map {
                            case 0L => PreconditionFailed
                            case id => Created(Json.toJson(onlineRegInviteTrackRow.copy(id = Some(id))))
                          }
                      }
                    }
                     Await.ready(pb, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                   DBIO.sequence(steps.map(step => StepTable += StepRow(stepValue, step.state, id, step.formId, step.formVersion, now)
                   )).flatMap { _ =>
                    TreatmentTable.filter(_.id === id).result.head
                  }
                }.transactionally
              } map { treatmentRow =>
                    (steps).foreach(step=>{
                      var formId = ""
                      if (step.formId == "1A"){
                          formId = "1L"
                      }else if (step.formId == "1L"){  
                          formId = "1A"
                      }
                      if (formId!=""){  
                        db.run{  
                          StepTable += StepRow(step.value, step.state, convertOptionLong(treatmentRow.id), formId, step.formVersion, now)
                        }
                      }
              })
                Created(Json.toJson(treatmentRow))
              }
        }
    } else {
          val newObj = Json.obj() + ("Result" -> JsString("Failed")) + ("message" -> JsString("Online registration module is not enabled. To enable please send mail to team@novadontics.com"))
          Future(PreconditionFailed{Json.toJson(Array(newObj))})
          }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  } 

  def onlinePatientUpdate(id: Long) = silhouette.UnsecuredAction.async(parse.json) { request =>
      var vInitialVisitDate:String = ""
      var vBirthDate = ""
      var checkPatientId = false
      var vPatientReferer = ""
      var referralSourceData = ""
      var vPatientType : StringBuilder = new StringBuilder();
      var vPatientStatus = ""
      var vCarrierName = ""
      var vFirstName = ""
      var vLastName = ""
      var vDob = ""
      var vProviderId = ""
      var vPhoneNumber = ""
      var vEmailAddress = ""
      var vZipCode = ""
      var vBusinessPhone = ""
      var vPatientRefererTo = ""
      var vReferredDate = ""
      var vCallingFrom = ""
      var vCountry = ""
      var vOnlineRegStatus = ""
      var vPatientId = ""
      var vPracticeId : Long = 0

      val emailService = new EmailService(ses, application)
      val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
      val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))

      //var clientTimeZone:String=convertOptionString(request.headers.get("timezone"))
    val treatment = for {
      steps <- (request.body \ "steps").validateOpt[List[Step]]

    } yield {
      (steps).foreach(steps1 => {
        (steps1).foreach(step=>{
              if (step.formId == "1A"){
                  checkPatientId = true
                  val jsonObject = Json.parse(step.value.toString())
                  (jsonObject \ "first_visit").validate[String] match {
                    case JsSuccess(first_visit, _) =>
                      vInitialVisitDate =s"$first_visit"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "birth_date").validate[String] match {
                    case JsSuccess(birth_date, _) =>
                      vBirthDate =s"$birth_date"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                 (jsonObject \ "patient_referer").validate[String] match {
                    case JsSuccess(patient_referer, _) =>
                      vPatientReferer =s"$patient_referer"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                  (jsonObject \ "patient_status").validate[String] match {
                    case JsSuccess(patient_status, _) =>
                      vPatientStatus =s"$patient_status"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "full_name").validate[String] match {
                    case JsSuccess(fullName, _) =>
                      vFirstName =s"$fullName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "last_name").validate[String] match {
                    case JsSuccess(lastName, _) =>
                      vLastName =s"$lastName"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "provider").validate[String] match {
                    case JsSuccess(providerId, _) =>
                      vProviderId =s"$providerId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "phone").validate[String] match {
                    case JsSuccess(phoneNumber, _) =>
                      vPhoneNumber =s"$phoneNumber"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "email").validate[String] match {
                    case JsSuccess(emailId, _) =>
                      vEmailAddress =s"$emailId"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "zip").validate[String] match {
                    case JsSuccess(zipCode, _) =>
                      vZipCode =s"$zipCode"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "business_phone").validate[String] match {
                    case JsSuccess(businessPhone, _) =>
                      vBusinessPhone =s"$businessPhone"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "patientRefererTo").validate[String] match {
                    case JsSuccess(patientRefererTo, _) =>
                      vPatientRefererTo =s"$patientRefererTo"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "referredDate").validate[String] match {
                    case JsSuccess(referredDate, _) =>
                      vReferredDate =s"$referredDate"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "country").validate[String] match {
                    case JsSuccess(country, _) =>
                      vCountry =s"$country"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }
                  (jsonObject \ "onlineRegStatus").validate[String] match {
                    case JsSuccess(onlineRegStatus, _) =>
                      vOnlineRegStatus =s"$onlineRegStatus"
                    case JsError(_) =>  Future.successful(BadRequest)
                  }

                if(jsonObject.toString contains "callingFrom"){
                  (jsonObject \ "callingFrom").validate[String] match {
                    case JsSuccess(callingFrom, _) =>
                      vCallingFrom =s"$callingFrom"
                    case JsError(_) =>  Future.successful(BadRequest)
                    }
                }

              }
        })
      })
        
      
      (vPhoneNumber, vEmailAddress, steps, checkPatientId,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType.toString.dropRight(1),vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCallingFrom,vCountry,vOnlineRegStatus)
    }

    val now = DateTime.now
    var firstVisitdate  = DateTime.now
    
    
      treatment match {
        case JsSuccess((vPhoneNumber, vEmailAddress, stepsOpt,checkPatientId,vInitialVisitDate,vBirthDate,vPatientReferer,vPatientType,vPatientStatus,vCarrierName,vFirstName,vLastName,vProviderId,vZipCode,vBusinessPhone,vPatientRefererTo,vReferredDate,vCallingFrom,vCountry,vOnlineRegStatus), _) =>
          if (checkPatientId && vCallingFrom != "patientModule"){
              Future.successful(BadRequest(Json.parse("""{"error":"Patient Id is blank"}""")))
          }else{
                var errorFound = false
                var birthDateValidation = false
                var phoneValidation = false
                var businessPhoneValidation = false
                var zipCodeValidation = false
                var patientType = false
                var patientExist = false

                (stepsOpt).foreach(steps => {
                  (steps).foreach(step=>{
                    if(step.formId == "1A"){
                    var countryList = List("united states of america","usa","united states","u.s","us","america")
                    var country = convertOptionString((step.value \ ("country")).asOpt[String])

                    if(countryList contains country.trim.toLowerCase){
                      var state = convertOptionString((step.value \ ("state")).asOpt[String]).toLowerCase

                      var stateBlock = db.run{
                        StateTable.filter(s=> s.state.toLowerCase === state.trim || s.stateName.toLowerCase === state.trim).result
                          } map { stateMap =>
                              if(stateMap.length == 0){
                                errorFound = true
                              }
                      }
                      var AwaitResult1 = Await.ready(stateBlock, atMost = scala.concurrent.duration.Duration(90, SECONDS))

                      var phone = vPhoneNumber.replaceAll("[^a-zA-Z0-9]", "")
                      if(phone.length != 10 || phone.startsWith("0") || phone.startsWith("1") || phone.startsWith("123")){
                        phoneValidation = true
                      }


                      if(vBusinessPhone != "" && vBusinessPhone != None){
                        var bPhone = vBusinessPhone.replaceAll("[^a-zA-Z0-9]", "")
                        if(bPhone.length != 10 || bPhone.startsWith("0") || bPhone.startsWith("1") || bPhone.startsWith("123")){
                          businessPhoneValidation = true
                        }
                      }

                      if(!(vZipCode.forall(_.isDigit) && (vZipCode.length == 5 || vZipCode.length == 9))){
                        zipCodeValidation = true
                      }
                    }

                    if(vBirthDate != "" && vBirthDate != None){
                        var dob = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vBirthDate)
                        if(dob > DateTime.now()){
                        birthDateValidation = true
                      }
                    }

                    if(vPatientType == "" || vPatientType == null){
                      patientType = true
                    }
                  }
            })
          })

          var patientBlock =  db.run{
                     for {
                        count <- TreatmentTable.filterNot(_.id === id).filter(_.firstName === vFirstName).filter(_.lastName === vLastName).filter(_.phoneNumber === vPhoneNumber).length.result
                      } yield {
                        if(count > 0){
                          patientExist = true
                        }
                      }
                    }
          Await.ready(patientBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                        
            if(vBirthDate !="" && !isValidDate(vBirthDate,"MM/dd/yyyy"))
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid birth date"}""")))   
            else if(patientExist)
                    Future.successful(BadRequest(Json.parse("""{"error":"Patient already exists"}""")))       
            else if(errorFound)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid state name!"}""")))
            else if(birthDateValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Birth date should not greater than today date!"}""")))
            else if(phoneValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
            else if(zipCodeValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid zipcode!"}""")))
            else if(businessPhoneValidation)
                    Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid business phone number!"}""")))
            else {
                 
              db.run {
                val query = TreatmentTable.filter(_.id === id)
                
               
                query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                  case true =>
                   
                    var block = db.run {
                      TreatmentTable.filterNot(_.deleted).filter(_.id === id).result.head
                    } map { result => 
                        vPracticeId = convertOptionLong(result.practiceId)
                      if(result.patientId != ""){
                        vPatientId = result.patientId
                      }
                    }
                    val Await1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(20, SECONDS))
                    
                    if (vInitialVisitDate!=""){
                        var intialVisitDate=DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vInitialVisitDate)
                        
                        
                        db.run{
                          TreatmentTable.filter(_.id === id).map(_.firstVisit).update(Some(intialVisitDate))
                        }
                    }

                      var block1 = db.run {
                          TreatmentTable.filter(_.id === id).map(_.patientType).update(Some(""))
                      } map {
                      case 0 => NotFound
                      case _ => Ok
                    }
                    var AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                  DBIO.seq[Effect.Write](
                    query.map(_.lastEntry).update(Some(now)).map(_ => Unit)
                  ) flatMap { _ =>
                          stepsOpt.map { steps =>
                            DBIO.sequence(steps.map { step =>

                            var stepValue = step.value
                            var newObj =  Json.obj()  + ("patient_id" -> JsString(vPatientId))
                            stepValue = stepValue.as[JsObject].deepMerge(newObj.as[JsObject])
                            StepTable += StepRow(stepValue, step.state, id, step.formId, step.formVersion, now)
                            })
                          }.getOrElse {
                            DBIO.successful(Unit)
                          }.map { _ =>
                          (stepsOpt).foreach(steps => {
                          (steps).foreach(step=> {
                      if (step.formId == "1A" || step.formId == "1L"){

                      db.run{
                          TreatmentTable.filter(_.id === id).map(_.referralSourceId).update(Some(vPatientReferer.toLong))
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }

                       if(step.formId == "1A"){
                      var referredTopatient: Some[Long] = null
                      
                      if(vPatientRefererTo != ""){
                        referredTopatient = Some(vPatientRefererTo.toLong)
                      }
                       db.run{
                          TreatmentTable.filter(_.id === id).map(_.referredTo).update(referredTopatient)
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }
                      
                       if (vReferredDate!=""){
                        var referredDate = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(vReferredDate)
                        
                        
                            db.run{
                              TreatmentTable.filter(_.id === id).map(_.referredDate).update(Some(referredDate))
                            }
                        }
                        }
                        
                        db.run {
                        val query = TreatmentTable.filter(_.id === id)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true => DBIO.seq[Effect.Write](
                            Some(vPhoneNumber).map(value => query.map(_.phoneNumber).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vEmailAddress).map(value => query.map(_.emailAddress).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vPatientType).map(value => query.map(_.patientType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vPatientStatus).map(value => query.map(_.patientStatus).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vBirthDate).map(value => query.map(_.dateOfBirth).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vFirstName).map(value => query.map(_.firstName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vLastName).map(value => query.map(_.lastName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            // Some(vProviderId.toLong).map(value => query.map(_.providerId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vCarrierName).map(value => query.map(_.insurance).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vCountry).map(value => query.map(_.country).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(vOnlineRegStatus).map(value => query.map(_.onlineRegistrationStatus).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                          ) map {_ => Ok}
                          case false => DBIO.successful(NotFound)
                        }
                      }

                    var block2 = db.run{
                      ReferralSourceTable.filter(_.id === vPatientReferer.toLong).result
                    } map { referralList =>
                    referralList.map{ result =>
                      db.run{
                          TreatmentTable.filter(_.id === id).map(_.referralSource).update(Some(result.value))
                            } map {
                                  case 0 => NotFound
                                  case _ => Ok
                              }
                     }
                    }
                    var AwaitResult2 = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                    var practiceBlock = db.run {
                      PracticeTable.filter(_.id === vPracticeId).result.head
                    } map { result => 
                      var emailContent = "Hi,\n" + vFirstName + " " + vLastName + " " + " filled and submitted the online registration form"
                      var subject = "Confirmation Email"
                      emailService.sendReminder(List(convertOptionString(result.email)), from, replyTo, subject, emailContent)
                    }
                    val Await1 = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(20, SECONDS))


                  val jsonObject = Json.parse(step.value.toString)
                  var isMedicalCondition :Boolean  = false
                  var alerts = ""
                  if(jsonObject.toString contains "medicalCondition"){
                    for( a <- 1 to 50){

                      (jsonObject \ ("medicalCondition"+a)).validate[String] match {
                        case JsSuccess(medicalCondition, _) =>
                        var condition =s"$medicalCondition"

                         var temp = condition.replace('★', '"')
                         val jsonObject1: JsValue = Json.parse(temp)
                         val medicalConditionCheck = convertOptionalBoolean((jsonObject1 \ ("checked")).asOpt[Boolean])

                         var medicalConditionValue = ""
                         var conditionType = ""

                          if(medicalConditionCheck == true){
                            if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }
                            isMedicalCondition = true
                            alerts = alerts + medicalConditionValue + ", "

                            var medicalConditionLogRow = MedicalConditionLogRow(None,step.formId,id,
                            medicalConditionValue,conditionType,LocalDate.now(),true)

                                if(a == 41){
                                 var a = db.run{
                                   MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).delete map{
                                      case 0 => NotFound
                                      case _ => Ok
                                     }
                                  }
                                  val AwaitResult = Await.ready(a, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                            }

                           if (medicalConditionValue.trim != ""){

                             db.run {
                                     for{
                                     medicalLogLength1 <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType)
                                     .filter(_.value === medicalConditionValue).length.result
                                      }yield {

                                      if(medicalLogLength1 <= 0){
                                      db.run{
                                      MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += medicalConditionLogRow
                                      }map{
                                      case 0L => PreconditionFailed
                                      case id => Ok
                                    }
                                  } else {
                                    db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === medicalConditionValue).map(_.currentlySelected).update(true)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                                  }
                                }
                              }
                           }

                        } else {

                            if(a == 41){
                            medicalConditionValue = convertOptionString((jsonObject1 \ ("notes")).asOpt[String])
                            conditionType = "Other_Medical_Condition"
                            }else{
                            medicalConditionValue = medicalConditionMap("medicalCondition"+a)
                            conditionType = "Medical Condition"
                            }
                                 var block = db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === medicalConditionValue).map(_.currentlySelected).update(false)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                              val AwaitResultMC = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

                  if(jsonObject.toString contains "allergy"){
                    for( a <- 1 to 8){

                      (jsonObject \ ("allergy"+a)).validate[String] match {
                          case JsSuccess(allergy, _) =>
                          var condition =s"$allergy"

                         var temp1 = condition.replace('★', '"')
                         val jsonObject2: JsValue = Json.parse(temp1)
                         val allergyValue = convertOptionalBoolean((jsonObject2 \ ("checked")).asOpt[Boolean])

                         var allergiesValue = ""
                         var conditionType = ""

                         if(allergyValue == true){

                        if(a == 7){
                          allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                          conditionType = "Other_Allergy"
                        }else{
                          allergiesValue = allergyMap("allergy"+a)
                          conditionType = "Allergy"
                          }

                            var allergyLogRow = MedicalConditionLogRow(None,step.formId,id,
                            allergiesValue,conditionType,LocalDate.now(),true)

                           if(a == 7){
                            var b = db.run{
                              MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).delete map{
                                case 0 => NotFound
                                case _ => Ok
                             }
                           }
                           val AwaitResult = Await.ready(b, atMost = scala.concurrent.duration.Duration(60, SECONDS))
                          }

                          if (allergiesValue.trim != ""){

                          db.run {
                            for{
                              allergyLogLength <- MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).length.result
                            }yield {
                              if(allergyLogLength <= 0){
                             db.run{
                                MedicalConditionLogTable.returning(MedicalConditionLogTable.map(_.id)) += allergyLogRow
                              }map{
                               case 0L => PreconditionFailed
                               case id => Ok
                              }
                              } else {
                                db.run{
                                      MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).map(_.currentlySelected).update(true)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                              }
                            }
                          }
                         }

                        }else {

                           if(a == 7){
                            allergiesValue = convertOptionString((jsonObject2 \ ("notes")).asOpt[String])
                            conditionType = "Other_Allergy"
                          }else{
                            allergiesValue = allergyMap("allergy"+a)
                            conditionType = "Allergy"
                          }

                          db.run{
                              MedicalConditionLogTable.filter(_.patientId === id).filter(_.conditionType === conditionType).filter(_.value === allergiesValue).map(_.currentlySelected).update(false)
                                  } map {
                                      case 0 => NotFound
                                      case _ => Ok
                                    }
                        }
                        case JsError(_) =>  Future.successful(BadRequest)
                      }
                    }
                  }

                // UPDATE FOR ISMEDICALCONDITION AND ALERTS FIELDS IN TREATMENT TABLE.
                    db.run {
                        val query = TreatmentTable.filter(_.id === id)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true => DBIO.seq[Effect.Write](
                            Some(isMedicalCondition).map(value => query.map(_.isMedicalCondition).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                            Some(alerts.dropRight(2)).map(value => query.map(_.alerts).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                          ) map {_ => Ok}
                          case false => DBIO.successful(NotFound)
                        }
                      }

                 }
              })
            })
            Ok
           }
          }
                    case false =>
                    DBIO.successful(NotFound)
                }.transactionally
              }
            // }
          }
          }
        case JsError(_) =>
          Future.successful(BadRequest)
      }
  }

}
