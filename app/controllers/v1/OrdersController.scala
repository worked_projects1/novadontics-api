package controllers.v1
import java.text.DecimalFormat
import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables._
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result
import co.spicefactory.services.EmailService
import models.daos.tables
import scala.concurrent.duration._
import java.io.File
import com.github.tototoshi.csv._
import play.api.mvc.Action
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import scalaj.http.{Http, HttpOptions}
import scala.util.matching.Regex
import org.joda.time.{ LocalTime, LocalDate }
import scala.concurrent.{ExecutionContext, Future,Await}

object OrdersController {

  implicit val orderStatusFormat = new Format[OrderRow.Status] {

    import OrderRow.Status._


    override def writes(status: OrderRow.Status) = Json.toJson {
      status match {
        case Received => "Received"
        case Fulfilled => "Fulfilled"
        case Canceled => "Canceled"
        case Void => "Void"
        case Returned => "Returned"
        case Unknown => "Unknown"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Received" => Received
      case "Fulfilled" => Fulfilled
      case "Canceled" => Canceled
      case "Void" => Void
      case "Returned" => Returned
      case _ => Unknown
    }
  }

  object Input {

    case class Order(
                      items: List[Order.Item],
                      vendors: Option[JsValue],
                      shipping: Option[Long],
                      shippingCharge: Option[Float],
                      tax: Option[Float],
                      creditCardFee: Option[Float],
                      grandTotal: Option[Float],
                      totalAmountSaved: Option[Float],
                      customerProfileID: Option[Long],
                      customerPaymentProfileId: Option[Long],
                      transId: Option[Long],
                      transactionCode: Option[String],
                      mckessonOrder: Option[Boolean],
                      mckessonStatus: Option[String],
                      paymentMethod: Option[String],
                      device: Option[String]
                    )

    object Order {

    case class Item(
                      amount: Int,
                      productId: Long,
                      overnight: Option[Boolean]
                     )


      object Item {
        implicit val reads = Reads { json =>
          for {
            amount <- (json \ "amount").validate[Int]
            productId <- (json \ "productId").validate[Long]
            overnight <- (json \ "overnight").validateOpt[Boolean]
          } yield {
            Item(amount, productId, overnight)
          }
        }
      }

      implicit val reads = Reads { json =>
        for {
          items <- (json \ "items").validate[List[Item]]
          vendors <- (json \ "vendors").validateOpt[JsValue]
          shipping <- (json \ "shippingAddress").validateOpt[Long]
          shippingCharge <- (json \ "shippingCharge").validateOpt[Float]
          tax <- (json \ "tax").validateOpt[Float]
          creditCardFee <- (json \ "creditCardFee").validateOpt[Float]
          grandTotal <- (json \ "grandTotal").validateOpt[Float]
          totalAmountSaved <- (json \ "totalAmountSaved").validateOpt[Float]
          customerProfileID <- (json \ "customerProfileID").validateOpt[Long]
          customerPaymentProfileId <- (json \ "customerPaymentProfileId").validateOpt[Long]
          transId <- (json \ "transId").validateOpt[Long]
          transactionCode <- (json \ "transactionCode").validateOpt[String]
          mckessonOrder <- (json \ "mckessonOrder").validateOpt[Boolean]
          mckessonStatus <- (json \ "mckessonStatus").validateOpt[String]
          paymentMethod <- (json \ "paymentMethod").validateOpt[String]
          device <- (json \ "device").validateOpt[String]
        } yield {
          Order(items, vendors, shipping, shippingCharge, tax, creditCardFee,grandTotal,totalAmountSaved,customerProfileID,customerPaymentProfileId,transId,transactionCode,mckessonOrder,mckessonStatus,paymentMethod,device)
        }
      }
    }

  }

  object Output {

    case class Order(
                      id: Long,
                      status: OrderRow.Status,
                      dateCreated: DateTime,
                      dateFulfilled: Option[DateTime],
                      approvedBy: Option[String],
                      items: List[Order.Item],
                      dentist: Order.Dentist,
                      address: Option[OrderRow.Address],
                      paymentType: Option[String],
                      checkNumber: Option[String],
                      allVendorsPaid: Option[Boolean],
                      shippingCharge: Option[Float],
                      tax: Option[Float],
                      creditCardFee: Option[Float],
                      grandTotal: Option[Float],
                      totalAmountSaved: Option[Float],
                      customerProfileID: Option[Long],
                      customerPaymentProfileId: Option[Long],
                      transId: Option[Long],
                      transactionCode: Option[String],
                      mckessonOrder: Option[Boolean],
                      mckessonStatus: Option[String],
                      note: Option[String],
                      paymentCompleted: Option[Boolean]
                    )

    object Order {

      case class Item(
                       amount: Int,
                       id: Long,
                       name: String,
                       description: String,
                       serialNumber: String,
                       imageUrl: Option[String],
                       manufacturer: String,
                       vendorId: Option[Long],
                       category: Option[Item.Category],
                       price: Double,
                       packageQuantity: Option[Long],
                       overnight: Option[Boolean],
                       itemPaid: Option[Boolean],
                       paymentType: Option[String],
                       checkNumber: Option[String],
                       amountPaid: Option[Double],
                       invoice: Option[String],
                       itemOrderId: Long,
                       unit: Option[String],
                       vendorInvoiceNum: Option[String],
                       paymentDate: Option[LocalDate]
                     )

      object Item {

        case class Category(id: Long, name: String)

        object Category {
          implicit val writes = Writes { item: Category =>
            Json.obj(
              "id" -> Json.toJson(item.id),
              "name" -> Json.toJson(item.name)
            )
          }
        }

        implicit val writes = Writes { item: Item =>
          Json.obj(
            "amount" -> Json.toJson(item.amount),
            "id" -> Json.toJson(item.id),
            "name" -> Json.toJson(item.name),
            "description" -> Json.toJson(item.description),
            "serialNumber" -> Json.toJson(item.serialNumber),
            "imageUrl" -> Json.toJson(item.imageUrl),
            "manufacturer" -> Json.toJson(item.manufacturer),
            "vendorId" -> Json.toJson(item.vendorId),
            "category" -> Json.toJson(item.category),
            "price" -> Math.round(item.price * 100.0) / 100.0,
            "packageQuantity" -> Json.toJson(item.packageQuantity),
            "overnight" -> Json.toJson(item.overnight),
            "itemPaid" -> Json.toJson(item.itemPaid),
            "paymentType" -> Json.toJson(item.paymentType),
            "checkNumber" -> Json.toJson(item.checkNumber),
            "amountPaid" -> Json.toJson(item.amountPaid),
            "invoice" -> Json.toJson(item.invoice),
            "orderId" -> Json.toJson(item.itemOrderId),
            "unit" -> Json.toJson(item.unit),
            "vendorInvoiceNum" -> Json.toJson(item.vendorInvoiceNum),
            "paymentDate" -> Json.toJson(item.paymentDate)
          )
        }
      }

      case class Dentist(
                          id: Long,
                          // title: String,
                          // name: String,
                          // lastName: Option[String],
                          name: String,
                          email: String,
                          practice: String,
                          country: String,
                          city: String,
                          state: String,
                          zip: String,
                          address: String,
                          phone: String
                        )

      object Dentist {
        implicit val writes = Json.writes[Dentist]
      }

      implicit val writes = Writes { order: Order =>

       val grandTotalFloat: Float = order.grandTotal match {
                         case None => 0
                         case Some(l: Float) => l
                        }

       val totalAmountSavedFloat: Float = order.totalAmountSaved match {
                         case None => 0
                         case Some(l: Float) => l
                        }

        val taxFloat: Float = order.tax match {
                                 case None => 0
                                 case Some(l: Float) => l
                                }

        val creditCardFeeFloat: Float = order.creditCardFee match {
                                 case None => 0
                                 case Some(l: Float) => l
                                }
        Json.obj(
          "id" -> Json.toJson(order.id),
          "status" -> Json.toJson(order.status),
          "dateCreated" -> Json.toJson(order.dateCreated),
          "dateFulfilled" -> Json.toJson(order.dateFulfilled),
          "approvedBy" -> Json.toJson(order.approvedBy),
          "items" -> Json.toJson(order.items),
          "dentist" -> Json.toJson(order.dentist),
          "address" -> Json.toJson(order.address),
          "paymentType" -> Json.toJson(order.paymentType),
          "checkNumber" -> Json.toJson(order.checkNumber),
          "allVendorsPaid" -> Json.toJson(order.allVendorsPaid),
          "shippingCharge" -> Json.toJson(order.shippingCharge),
          "tax" -> Math.round(taxFloat * 100.0) / 100.0,
          "creditCardFee" -> Math.round(creditCardFeeFloat * 100.0) / 100.0,
          "grandTotal" -> Math.round(grandTotalFloat * 100.0) / 100.0,
          "totalAmountSaved" -> Json.toJson(order.totalAmountSaved),
          "customerProfileID" -> Json.toJson(order.customerProfileID),
          "customerPaymentProfileId" -> Json.toJson(order.customerPaymentProfileId),
          "transId" -> Json.toJson(order.transId),
          "transactionCode" -> Json.toJson(order.transactionCode),
          "mckessonOrder" -> Json.toJson(order.mckessonOrder),
          "mckessonStatus" -> Json.toJson(order.mckessonStatus),
          "note" -> Json.toJson(order.note),
          "paymentCompleted" -> Json.toJson(order.paymentCompleted)
        )
      }
    }

    object Orders {


      def from(pairs: Iterable[((((OrderRow, OrderItemRow), StaffRow), PracticeRow))]) = pairs.groupBy {

        case ((((order, _), _), _)) => order.id.get
      } map {
        case (id, collection) =>
          val order = collection.head._1._1._1
          val items = collection.map(_._1._1._2)
          val dentist = collection.head._1._2
          val practice = collection.head._2


      val lastName: String = dentist.lastName match {
      case None => ""
      case Some(s: String) => s
      }

          Output.Order(
            id,
            order.status,
            order.dateCreated,
            order.dateFulfilled,
            order.approvedBy,
            items.toList.map { item =>
              val itemOrderId:Long = id
              Output.Order.Item(
                item.amount,
                item.productId,
                item.product.name,
                item.product.description,
                item.product.serialNumber,
                item.product.imageUrl,
                item.product.productVendorName,
                item.product.productVendorId,
                item.product.category.map { category =>
                  Output.Order.Item.Category(category.id, category.name)
                },
                item.product.price,
                item.product.packageQuantity,
                item.overnightShipping,
                item.itemPaid,
                item.paymentType,
                item.checkNumber,
                item.amountPaid,
                item.invoice,
                itemOrderId,
                item.product.unit,
                item.vendorInvoiceNum,
                item.paymentDate
              )
            },
            Output.Order.Dentist(
              dentist.id.get,
              s"${dentist.title} ${dentist.name} ${lastName}",
              dentist.email,
              practice.name,
              practice.country,
              practice.city,
              practice.state,
              practice.zip,
              practice.address,
              practice.phone
            ),
            order.address,
            order.payment.get.paymentType,
            order.checkNumber,
            order.payment.get.allVendorsPaid,
            order.shippingCharge,
            order.tax,
            order.creditCardFee,
            order.grandTotal,
            order.totalAmountSaved,
            order.payment.get.customerProfileID,
            order.payment.get.customerPaymentProfileId,
            order.payment.get.transId,
            order.payment.get.transactionCode,
            order.mckessonOrder,
            order.mckessonStatus,
            order.note,
            order.payment.get.paymentCompleted
          )
      }

    }

  }

}


@Singleton
class OrdersController @Inject()(ses: AmazonSimpleEmailService, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import OrdersController._
  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  private val replyTo = application.configuration.getString("aws.ses.orderInvoice.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val from = application.configuration.getString("aws.ses.orderInvoice.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val devEmail = application.configuration.getString("devEmail").getOrElse(throw new RuntimeException("Configuration is missing `devEmail` property."))

  private val emailService = new EmailService(ses, application)
  
  //A METHOD THAT THROWS EXCEPTION
  def throwsException() {
      throw new IllegalStateException("Exception thrown");
  }

        case class OrderCSVList(
                                orderId: String,
                                staffName: String, 
                                practiceName: String, 
                                orderDate: String, 
                                orderFullfilled: String, 
                                total: String, 
                                status: String
                              )

        case class OrderHistoryReport(
                                orderId: String,
                                staffName: String,
                                practiceName: String,
                                orderDate: String,
                                status: String,
                                grandTotal: String,
                                totalSavings: String
                              )

  implicit val OrderHistoryReportWrites = Writes { OrderHistoryReportRecord: (OrderHistoryReport) =>
    val (orderHistoryReportRow) = OrderHistoryReportRecord

    Json.obj(
      "orderId" -> orderHistoryReportRow.orderId,
      "staffName" -> orderHistoryReportRow.staffName,
      "practiceName" -> orderHistoryReportRow.practiceName,
      "orderDate" -> orderHistoryReportRow.orderDate,
      "status" -> orderHistoryReportRow.status,
      "grandTotal" -> orderHistoryReportRow.grandTotal,
      "totalSavings" -> orderHistoryReportRow.totalSavings
    )
  }

        case class CatalogReportCSVList(
                                vendorName: String,
                                startDate: String,
                                endDate: String,
                                totalNumber: Long,
                                totalRevenue: Double,
                                totalShipping: Double,
                                totalTax: Double,
                                totalNetGross: Double,
                                totalNetProfit: Double
                              )



  def fromWithoutItems(pairs: Iterable[((OrderRow , StaffRow), PracticeRow)], isStaff: Boolean) = pairs.groupBy(_._1._1.id). map {

        case (id, collection) =>

          val order = collection.head._1._1
          var orderStatus: OrderRow.Status = order.status

          val dentist = collection.head._1._2
          val practice = collection.head._2
          
          var items = List.empty[OrderItemRow]
          var f1=db.run { 
                 for {
                      itemsList <- OrderItemTable.filter(_.orderId === id).result
                 } yield {
                      items = itemsList.toList
                 }
            }
            var AwaitResult1 = Await.ready(f1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
          if(isStaff == true && order.status != OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] && order.status != OrderRow.Status.Returned.asInstanceOf[OrderRow.Status]) {
            orderStatus =  OrderRow.Status.Received.asInstanceOf[OrderRow.Status]
          }
          Output.Order(
            convertOptionLong(id),
            orderStatus,
            order.dateCreated,
            order.dateFulfilled,
            order.approvedBy,
            items.map { item =>
              val itemOrderId:Long = convertOptionLong(id)
              Output.Order.Item(
                item.amount,
                item.productId,
                item.product.name,
                item.product.description,
                item.product.serialNumber,
                item.product.imageUrl,
                item.product.productVendorName,
                item.product.productVendorId,
                item.product.category.map { category =>
                  Output.Order.Item.Category(category.id, category.name)
                },
                item.product.price,
                item.product.packageQuantity,
                item.overnightShipping,
                item.itemPaid,
                item.paymentType,
                item.checkNumber,
                item.amountPaid,
                item.invoice,
                itemOrderId,
                item.product.unit,
                item.vendorInvoiceNum,
                item.paymentDate
              )
            },
            Output.Order.Dentist(
              dentist.id.get,
              // dentist.title,
              // dentist.name,
              // dentist.lastName,
              s"${dentist.title} ${dentist.name} ${convertOptionString(dentist.lastName)}",
              dentist.email,
              practice.name,
              practice.country,
              practice.city,
              practice.state,
              practice.zip,
              practice.address,
              practice.phone
            ),
            order.address,
            order.payment.get.paymentType,
            order.checkNumber,
            order.payment.get.allVendorsPaid,
            order.shippingCharge,
            order.tax,
            order.creditCardFee,
            order.grandTotal,
            order.totalAmountSaved,
            order.payment.get.customerProfileID,
            order.payment.get.customerPaymentProfileId,
            order.payment.get.transId,
            order.payment.get.transactionCode,
            order.mckessonOrder,
            order.mckessonStatus,
            order.note,
            order.payment.get.paymentCompleted
          )
      }.toList.sortBy(_.id).reverse





  def readAll = silhouette.SecuredAction.async { request =>


    //try{
    //throwsException();
    var userRole = ""
    var isStaff = false

    val query1 = request.identity match {
      case UserRow(id, _, _, _, _, _, _) =>
        UserTable.filter(_.id === id).map(_.role)
      case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ , _, _, _) =>
        StaffTable.filter(_.id === id).map(_.role)
    }
    var userRoleData = db.run {  query1.result } map { result => userRole = result.head.toString }
    var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

    if(userRole == "Dentist") {
      isStaff = true
    }


    var pageNumber:Int = 0
    var recPerPage:Int = 0

    val searchTextVal: Option[String] = request.headers.get("searchText")
    val searchText:String = convertOptionString(searchTextVal)
    
    val statusVal: Option[String] = request.headers.get("status")
    val status:String = convertOptionString(statusVal)

    var pageNumberVal: Option[String] = request.headers.get("pageNumber")
    if ( pageNumberVal != None){
      pageNumber = convertOptionString(pageNumberVal).toInt
    }

    if (pageNumber == 0 ){
      pageNumber = 1
    }

    var recPerPageVal: Option[String] = request.headers.get("recPerPage")
    if( recPerPageVal != None){
      recPerPage = convertOptionString(recPerPageVal).toInt
    } 
    
  
      
    var query = OrderTable
                  .join(StaffTable).on(_.staffId === _.id)
                  .join(PracticeTable).on(_._2.practiceId === _.id)

    if (searchText!="" || status!=""){
         
        if (searchText!=""){


          if (isAllDigits(searchText)){
             query = query.filter(f=>(f._1._1.id === searchText.toLong))
              
          }else{
             query = query.filter(f=>((f._2.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f._1._2.name.toLowerCase like "%"+searchText.toLowerCase+"%") ))
          }    
        }

        if(isStaff ==true) {
          if (status=="Received"){
            query = query.filterNot(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] ))).filterNot(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
          } else if (status=="Canceled"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )))
          } else if (status=="Returned"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
          }
        } else {
          if (status=="Received"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )))
          }else if (status=="Fulfilled"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status] )))
          }else if (status=="Canceled"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )))
          }else if (status=="Closed"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )))
          }else if (status=="Returned"){
            query = query.filter(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
          }
        }

        db.run {
          val l: Long = 1

          val orders = query

          request.identity match {
            case row: StaffRow =>
              orders.filter(_._1._2.practiceId === row.practiceId).sortBy(_._1._1.id.desc).result
            case _ => orders.sortBy(_._1._1.id.desc).result
          }

        } map { pairs =>
          //Ok(Json.toJson(fromWithoutItems(pairs, isStaff)))
            if (recPerPage == 0){
                  val pageData = pairs
                  Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
                }
                else{
                    val pageData = pairs.drop((pageNumber-1)*recPerPage).take(recPerPage)
                    Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
                }
            }
    } 
    else{
        
      db.run {
          val l: Long = 1

          val orders = query

          request.identity match {
            case row: StaffRow =>
              orders.filter(_._1._2.practiceId === row.practiceId).sortBy(_._1._1.id.desc).result
            case _ => orders.sortBy(_._1._1.id.desc).result
          }

      } map { pairs =>
        //Ok(Json.toJson(fromWithoutItems(pairs, isStaff)))
          if (recPerPage == 0){
              
                val pageData = pairs
                Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
          }
            else{
                val pageData = pairs.drop((pageNumber-1)*recPerPage).take(recPerPage)
                Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
            }
        }
    }
  }

def catalogOrders = silhouette.SecuredAction.async { request =>

  var userRole = ""
  var isStaff = false

  val query1 = request.identity match {
    case UserRow(id, _, _, _, _, _, _) =>
      UserTable.filter(_.id === id).map(_.role)
    case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
      StaffTable.filter(_.id === id).map(_.role)
  }
  var userRoleData = db.run {  query1.result } map { result => userRole = result.head.toString }
  var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

  if(userRole == "Dentist") {
    isStaff = true
  }

    var pageNumber:Int = 0
    var recPerPage:Int = 0

    val searchTextVal: Option[String] = request.headers.get("searchText")
    val searchText:String = convertOptionString(searchTextVal)

    val statusVal: Option[String] = request.headers.get("status")
    val status:String = convertOptionString(statusVal)

    var pageNumberVal: Option[String] = request.headers.get("pageNumber")
    if ( pageNumberVal != None){
      pageNumber = convertOptionString(pageNumberVal).toInt
    }

    if (pageNumber == 0 ){
      pageNumber = 1
    }

    var recPerPageVal: Option[String] = request.headers.get("recPerPage")
    if( recPerPageVal != None){
      recPerPage = convertOptionString(recPerPageVal).toInt
    }


    var query = OrderTable
                  .join(StaffTable).on(_.staffId === _.id)
                  .join(PracticeTable).on(_._2.practiceId === _.id)
    if (searchText!="" || status!=""){

        if (searchText!=""){
          if (isAllDigits(searchText)){
             query = query.filter(f=>(f._1._1.id === searchText.toLong))

          }else{
             query = query.filter(f=>((f._2.name.toLowerCase like "%"+searchText.toLowerCase+"%") || (f._1._2.name.toLowerCase like "%"+searchText.toLowerCase+"%") ))
          }
        }

      if(isStaff ==true) {
        if (status=="Received"){
          query = query.filterNot(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] ))).filterNot(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
        } else if (status=="Canceled"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )))
        } else if (status=="Returned"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
        }
      } else {
        if (status=="Received"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )))
        }else if (status=="Fulfilled"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status] )))
        }else if (status=="Canceled"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )))
        }else if (status=="Closed"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )))
        }else if (status=="Returned"){
          query = query.filter(f=>((f._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status] )))
        }
      }

        db.run {
          val l: Long = 1
          //val orders = query
          query.filter(_._1._2.practiceId === request.identity.asInstanceOf[StaffRow].practiceId).sortBy(_._1._1.id.desc).result

        } map { pairs =>
          //Ok(Json.toJson(fromWithoutItems(pairs, isStaff)))
            if (recPerPage == 0){
                  val pageData = pairs
                  Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
                }
                else{
                    val pageData = pairs.drop((pageNumber-1)*recPerPage).take(recPerPage)
                    Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
                }
            }
    }
    else{

      db.run {
          val l: Long = 1

          //val orders = query
          query.filter(_._1._2.practiceId === request.identity.asInstanceOf[StaffRow].practiceId).sortBy(_._1._1.id.desc).result

      } map { pairs =>
        //Ok(Json.toJson(fromWithoutItems(pairs, isStaff)))
          if (recPerPage == 0){

                val pageData = pairs
                Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
          }
            else{
                val pageData = pairs.drop((pageNumber-1)*recPerPage).take(recPerPage)
                Ok(Json.toJson(fromWithoutItems(pageData, isStaff))).withHeaders(
                              ("totalCount" -> pairs.length.toString()))
            }
        }
    }
  }

  def create = silhouette.SecuredAction(CanShop).async(parse.json) { request =>
  var userAgent =  request.headers.get("User-Agent")
  var mckessonOrderId: Long = 0

    request.body.validate[Input.Order] match {
      case JsSuccess(order, _) =>

      val productIds = order.items.foldLeft(Set.empty[Long])(_ + _.productId)

      var mckessonOrder = false
      var block = db.run{
          VendorTable.filter(_.name === "McKesson").result
      }map{  vendorList =>
        vendorList.foreach(vendorResult => {

          var block1 = db.run{
                ProductTable.filter(_.id inSet productIds).filter(_.vendorId === vendorResult.id).result
            } map { productMap =>
            if(productMap.length > 0){
              mckessonOrder = true
            }
          }
            var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        })
      }
      var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

        db.run {

          ProductTable.filter(_.id inSet productIds).joinLeft(CategoryTable).on(_.categoryId === _.id).join(VendorTable).on(_._1.vendorId === _.id).result.flatMap { products =>
            if (products.lengthCompare(productIds.size) < 0) {
              DBIO.successful(BadRequest)
            } else {
              val orderStatement = (
                order.shipping match {
                  case Some(addressId) =>
                    ShippingTable.filter(_.id === addressId).result.head.map(address =>
                      OrderRow.Address(address.name, address.country, address.city, address.state, address.zip, address.address, address.phone, order.device, address.suite)

                    )
                  case None =>
                    PracticeTable.filter(_.id === request.identity.asInstanceOf[StaffRow].practiceId).result.head.map(
                      practice => OrderRow.Address(Some(practice.name), practice.country, practice.city, practice.state, practice.zip, practice.address, practice.phone,order.device, practice.suite)
                    )
                }) flatMap {
                address =>

                var vDateFulfilled: Some[DateTime] = Some(DateTime.now())
                var orderStatus: OrderRow.Status = OrderRow.Status.Fulfilled
                if(convertOptionString(order.transactionCode) == "4" || convertOptionString(order.paymentMethod) == "check" || convertOptionString(order.paymentMethod) == "finance"){
                  orderStatus = OrderRow.Status.Received
                }

                if(orderStatus == OrderRow.Status.Received){
                  vDateFulfilled = null
                }
                var paymentCompleted = false
                if(orderStatus == OrderRow.Status.Fulfilled){
                  paymentCompleted = true
                }

                var payment = OrderRow.Payment(order.paymentMethod,Some(false),order.customerProfileID,order.customerPaymentProfileId,order.transId,order.transactionCode,Some(paymentCompleted))

                OrderTable.returning(OrderTable.map(_.id)) += OrderRow(None, orderStatus, DateTime.now, request.identity.id.get, vDateFulfilled, None, Some(address), Some(""),order.shippingCharge,order.tax,order.creditCardFee,order.grandTotal,order.totalAmountSaved,userAgent,Some(mckessonOrder),Some(""),Some(""),Some(payment))
              }
              orderStatement.flatMap { orderId =>
                  mckessonOrderId = orderId
        if(convertOptionFloat(order.shippingCharge) < 0 || convertOptionFloat(order.tax) < 0 || convertOptionFloat(order.creditCardFee) < 0 || convertOptionFloat(order.grandTotal) < 0){
           emailService.sendEmail(List(devEmail), from, replyTo, "Order - got negative value", views.html.orderLog(orderId,convertOptionString(userAgent)) )
           db.run{
              OrderLogTable.returning(OrderLogTable.map(_.id)) += OrderLogRow(None,Some(orderId),userAgent)
              } map {
              case 0L => PreconditionFailed
              case id => Ok
            }
        }

         var deviceUsed = "desktop"
           if(order.device != None && order.device != Some("")) {
             deviceUsed = convertOptionString(order.device)
          }

        var vendorNoteMap:scala.collection.mutable.Map[Option[Long],String] = scala.collection.mutable.Map()

        if(order.vendors != None && !order.vendors.isInstanceOf[JsUndefined]){
        var vendorsList = order.vendors.get
          var jsonList = vendorsList.as[List[JsValue]]

          jsonList.foreach(result => {
              val vendorId = (result \ "id").as[Long]
              var note = (result \ "note").as[String]

              vendorNoteMap.update(Some(vendorId),note)

             var block = db.run{
              OrderVendorDetailsTable.returning(OrderVendorDetailsTable.map(_.id)) += OrderVendorDetailsRow(None,orderId,vendorId,note)
              } map {
              case 0L => PreconditionFailed
              case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
          })
      }


                val itemsStatement = OrderItemTable ++= products.map {
                  case ((ProductRow(productId, productName, description, serialNumber, imageUrl, manufacturer, _, price, packageQuantity, listPrice, document, video,stockAvailable,unit,financeEligible), categoryRow), VendorRow(vendorId, vendorName, vendoEmail, vendorLogoUrl, deleted, vendorShippingChargeLimit, vendorShippingCharge, vendorTax, vendorCreditCardFee, vendorGroup, vendorCategory, vendorImage, vendorGroups)) =>


                    val actual_price: Double = listPrice match {
                      case None => price.toFloat//Or handle the lack of a value another way: throw an error, etc.
                      case Some(f: Double) => f //return  to set your value
                    }
                    val image_Url = convertOptionString(imageUrl).split(',')(0);

                    OrderItemRow(
                      order.items.filter(_.productId == productId.get).map(_.amount).sum,
                      productId.get,
                      orderId,
                      order.items.find(_.productId == productId.get).head.overnight,
                      Some(false),
                      Some(""),
                      Some(""),
                      None,
                      Some(""),
                      None,
                      None,
                      OrderItemRow.Product(
                        productName,
                        description.getOrElse(""),
                        serialNumber,
                        //imageUrl,
			                  Some(image_Url),
                        vendorId,
                        vendorName,
                        categoryRow.map { category =>
                          OrderItemRow.Product.Category(
                            category.id.get,
                            category.name
                          )
                        },
                        actual_price,
                        packageQuantity,
                        unit
                      )
                    )
                }



                var vendorIdArr = new Array[Long](100)
                var vendorIdIndx: Int = 0;
                products.map {
                  case ((ProductRow(productId, productName, description, serialNumber, imageUrl, manufacturer, _, price, packageQuantity, listPrice, document, video,stockAvailable,unit,financeEligible), categoryRow), VendorRow(vendorId, vendorName, vendoEmail, vendorLogoUrl, deleted, vendorShippingChargeLimit,vendorShippingCharge, vendorTax, vendorCreditCardFee, vendorGroup, vendorCategory, vendorImage, vendorGroups)) =>
                    val lvendorId: Long = vendorId match {
                      case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                      case Some(l: Long) => l //return  to set your value
                    }
                    if (!(vendorIdArr contains lvendorId)) {
                      vendorIdArr(vendorIdIndx) = lvendorId
                      vendorIdIndx = vendorIdIndx + 1
                      db.run {
                        VendorPaymentTrackTable.returning(VendorPaymentTrackTable.map(_.id)) += VendorPaymentTrackRow(None, orderId, vendorId, false, Some(""), Some(""),None)
                      }
                    }
                }

                var paymentMethod = order.paymentMethod
                itemsStatement.flatMap { _ =>
                  OrderTable.filter(_.id === orderId)
                    .join(OrderItemTable).on(_.id === _.orderId)
                    .join(StaffTable).on(_._1.staffId === _.id)
                    .join(PracticeTable).on(_._2.practiceId === _.id)
                    .result
                } map { pairs => {
                (orderId,convertOptionLong(request.identity.id))
                  val order = Output.Orders.from(pairs).head
                  // val total = order.items.map { i => i.price * i.amount }.sum
                  // val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                  //   item => (item._2, item._2.map(i => i.price * i.amount).sum, item._1)
                  // }.toList.partition(_._3 == "NobelBiocare")
                  val total = order.items.map { i => f"${i.price}%.2f".toDouble * i.amount }.sum
                  val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                    item => (item._2,item._2.map(i => f"${i.price}%.2f".toDouble * i.amount).sum , item._1)
                  }.toList.partition(_._3 == "NobelBiocare")
                  val items = nobel ::: other.sortWith(_._3.capitalize < _._3.capitalize)

                  val shippingCharge: Float = order.shippingCharge match {
                    case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                    case Some(l: Float) => l //return  to set your value
                  }

                  val tax: Float = order.tax match {
                    case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                    case Some(l: Float) => l //return  to set your value
                  }

                  val creditCardFee: Float = order.creditCardFee match {
                    case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                    case Some(l: Float) => l //return  to set your value
                  }

                   val grandTotalFloat: Float = order.grandTotal match {
                                      case None => 0
                                      case Some(l: Float) => l
                                    }

                   val totalAmountSavedFloat: Float = order.totalAmountSaved match {
                      case None => 0
                      case Some(l: Float) => l
                    }

                  var suiteNumber = ""
                  var staffId = request.identity.id.get
                   var block =  db.run{
                      StaffTable.filterNot(_.archived).filter(_.id === staffId).result} map{ staffMap =>
                      suiteNumber = convertOptionString(staffMap.head.suiteNumber)
                    }
                    Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))


                  // var dentistName = order.dentist.title + order.dentist.name + " " + convertOptionString(order.dentist.lastName)
                  var dentistName = order.dentist.name

                  // Send email to the customer
                  emailService.sendEmail(List(s"${dentistName} <${order.dentist.email}>"), from, replyTo, subject, views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, None, order.address, shippingCharge, tax, creditCardFee,totalAmountSavedFloat,"",deviceUsed,vendorNoteMap))

                  // send to admins and assigned email addresses
                  db.run {
                    EmailMappingTable.filter(_.name === "order")
                      .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                      .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                      .result
                  }.map { data =>
                    val defaultAddresses = data.head._1._1.defaultEmail.getOrElse("").split(",").toList
                    val addresses = data.filter(_._2.isDefined).map(_._2.get).map(user => s"${user.name} <${user.email}>").toList ::: defaultAddresses

                    emailService.sendEmail(addresses, from, replyTo, s"New Order(id: ${order.id}) - ${dentistName}", views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, Some(s""), order.address, shippingCharge, tax, creditCardFee,totalAmountSavedFloat,"",s"Device Used: ${deviceUsed}",vendorNoteMap))
                  }

                  //send emails to vendors
                  if(convertOptionString(paymentMethod) != "check"){
                  db.run {
                    VendorTable.result
                  } map {
                    vendors =>
                      items.foreach(
                        productGroup => {
                          val vendor = vendors.find(_.id == productGroup._1.head.vendorId)
                          if (vendor.isDefined && vendor.get.email.isDefined) {
                            emailService.sendEmail(List(vendor.get.email.get), from, replyTo, s"New Order(id: ${order.id}) - Novadontics", views.html.vendorOrder(total, order.dentist, productGroup, order.address,vendorNoteMap))
                          }
                        }
                      )
                  }
                }

                  db.run {
                    CartTable.filter(_.userId === request.identity.id).delete map {
                      case 0 => NotFound
                      case _ => Ok
                    }
                  }
                  Ok(Json.toJson(order))
                }
                }
              }
            }
          }.transactionally

        }

      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }


  def updateGrandTotal = silhouette.SecuredAction.async(parse.json) { request =>

    db.run {
      for{
          orders <- OrderTable.result
      }yield{

            (orders).foreach(order=>{
            grandTotalUpdate(convertOptionLong(order.id))

          })
        Ok {
            Json.obj(
              "status"->"success",
              "message"->"Grand Total updated successfully"
            )
          }
      }
     }
  }



def grandTotalUpdate(id: Long) {

var grandTotal : Float = 0

 db.run {
      OrderTable.filter(_.id === id)
       .join(OrderItemTable).on(_.id === _.orderId)
        .join(StaffTable).on(_._1.staffId === _.id)
        .join(PracticeTable).on(_._2.practiceId === _.id)
        .result

    } map { pairs =>

     val order = Output.Orders.from(pairs).head
     val total = order.items.map { i => i.price * i.amount }.sum

                  var overnightCount = 0
                  var vendorIdTemp = new Array[Long](1000)
                  var vendorIdIndx: Int = 0;
                  val overnightCharge = order.items.map { i =>

                    var lvendorId: Long = convertOptionLong(i.vendorId)

                    if (!(vendorIdTemp contains lvendorId)) {
                      vendorIdTemp(vendorIdIndx) = lvendorId
                      vendorIdIndx = vendorIdIndx + 1

                      val overnight: Boolean = i.overnight match {
                        case None => false
                        case Some(l: Boolean) => l
                      }
                      if(overnight == true){
                        overnightCount = overnightCount + 1
                      }
                    }
                  }

                  val overnightAmount   = overnightCount * 52


                  val shippingCharge: Float = order.shippingCharge match {
                    case None => 0
                    case Some(l: Float) => l
                  }

                  val tax: Float = order.tax match {
                    case None => 0
                    case Some(l: Float) => l
                  }

                  val creditCardFee: Float = order.creditCardFee match {
                    case None => 0
                    case Some(l: Float) => l
                  }

                 val totalFloat = total.toFloat
                 grandTotal = shippingCharge + tax + creditCardFee + totalFloat + overnightAmount
                  //Console.println("Grand Total1 :" + grandTotal)

                db.run {
                      OrderTable.filter(_.id === id).map(_.grandTotal).update(Some(grandTotal))
                    } map {
                      case 0 => NotFound
                      case _ => // Console.println("Grand Total2 : "+ grandTotal)
                    }

      Ok(Json.toJson(grandTotal))
    }
  }


  def read(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      OrderTable.filter(_.id === id)
        .join(OrderItemTable).on(_.id === _.orderId)
        .join(StaffTable).on(_._1.staffId === _.id)
        .join(PracticeTable).on(_._2.practiceId === _.id)
        .result
    } map { pairs =>
      Ok(Json.toJson(Output.Orders.from(pairs).head))
    }
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val orders = for {
      status <- (request.body \ "status").validate[OrderRow.Status]
      paymentType <- (request.body \ "paymentType").validateOpt[String]
      checkNumber <- (request.body \ "checkNumber").validateOpt[String]
      shippingCharge <- (request.body \ "shippingCharge").validateOpt[Float]
      tax <- (request.body \ "tax").validateOpt[Float]
      creditCardFee <- (request.body \ "creditCardFee").validateOpt[Float]
      customerProfileID <- (request.body \ "customerProfileID").validateOpt[Long]
      customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validateOpt[Long]
      transId <- (request.body \ "transId").validateOpt[Long]
      paymentCompleted <- (request.body \ "paymentCompleted").validateOpt[Boolean]
      note <- (request.body \ "note").validateOpt[String]
    } yield {
      (status, paymentType, checkNumber, shippingCharge, tax, creditCardFee, customerProfileID, customerPaymentProfileId, transId, paymentCompleted,note)
    }
    orders match {
      case JsSuccess((status, paymentType, checkNumber, shippingCharge, tax, creditCardFee, customerProfileID, customerPaymentProfileId, transId, paymentCompleted,note), _) =>

        db.run {
          val query = OrderTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              query.filter(_.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status]).exists.result.flatMap[Result, NoStream, Effect.Write] {
                case true if status == OrderRow.Status.Fulfilled =>
                  // query.map(row => (row.status, row.dateFulfilled, row.approvedBy)).update((status, Some(DateTime.now), Some(request.identity.name))).map(_ => Ok)
                  DBIO.seq[Effect.Write](
                    query.map(row => (row.status, row.dateFulfilled, row.approvedBy)).update((status, Some(DateTime.now), Some(request.identity.name))),
                    //query.map(_.paymentType).update(paymentType),
                    paymentType.map(value => query.map(_.paymentType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    checkNumber.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    shippingCharge.map(value => query.map(_.shippingCharge).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    tax.map(value => query.map(_.tax).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    creditCardFee.map(value => query.map(_.creditCardFee).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    customerProfileID.map(value => query.map(_.customerProfileID).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    customerPaymentProfileId.map(value => query.map(_.customerPaymentProfileId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    transId.map(value => query.map(_.transId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    paymentCompleted.map(value => query.map(_.paymentCompleted).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    note.map(value => query.map(_.note).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) map { _ =>
                    Ok
                  }
                case _ =>
                  DBIO.seq[Effect.Write](
                    query.map(_.status).update(status),
                    paymentType.map(value => query.map(_.paymentType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    checkNumber.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    shippingCharge.map(value => query.map(_.shippingCharge).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    tax.map(value => query.map(_.tax).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    creditCardFee.map(value => query.map(_.creditCardFee).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    customerProfileID.map(value => query.map(_.customerProfileID).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    customerPaymentProfileId.map(value => query.map(_.customerPaymentProfileId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    transId.map(value => query.map(_.transId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    paymentCompleted.map(value => query.map(_.paymentCompleted).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    note.map(value => query.map(_.note).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) map { _ =>
                    Ok
                  }
              }
            case false =>
              DBIO.successful(NotFound)
          }

        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def updateVendorPayment(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val vendor = for {
      orderId <- (request.body \ "orderId").validateOpt[Long]
      vendorId <- (request.body \ "vendorId").validateOpt[Long]
      paymentType <- (request.body \ "paymentType").validateOpt[String]
      checkNumber <- (request.body \ "checkNumber").validateOpt[String]
      amountPaid <- (request.body \ "amountPaid").validateOpt[Double]
      invoice <- (request.body \ "invoice").validateOpt[String]
      vendorInvoiceNum <- (request.body \ "vendorInvoiceNum").validateOpt[String]
      paymentDate <- (request.body \ "paymentDate").validateOpt[String]
    } yield {
      var vPaymentDate: Option[LocalDate] = None
      if(paymentDate != None && paymentDate != Some("")){
        vPaymentDate = Some(LocalDate.parse(convertOptionString(paymentDate)))
      }
      (orderId, vendorId, paymentType, checkNumber, amountPaid,invoice,vendorInvoiceNum,vPaymentDate)
    }

    vendor match {
      case JsSuccess((orderId, vendorId, paymentType, checkNumber,amountPaid, invoice, vendorInvoiceNum, vPaymentDate), _) =>
        db.run {
          val query = OrderItemTable.filter(_.productVendorId === vendorId).filter(_.orderId === orderId)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Write](
                paymentType.map(value => query.map(_.paymentType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                checkNumber.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                amountPaid.map(value => query.map(_.amountPaid).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                invoice.map(value => query.map(_.invoice).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                query.map(_.itemPaid).update(Some(true)),
                vendorInvoiceNum.map(value => query.map(_.vendorInvoiceNum).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                vPaymentDate.map(value => query.map(_.paymentDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>

                db.run {
                  for {
                    unPaidCount <- OrderItemTable.filter(_.orderId === orderId).filterNot(_.itemPaid).map(_.orderId).countDistinct.result
                  } yield {
                    if (unPaidCount == 0) {
                      val lorderId: Long = orderId match {
                        case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                        case Some(l: Long) => l //return  to set your value
                      }
                      db.run {
                        val query = OrderTable.filter(_.id === lorderId)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true =>
                             query.map(_.allVendorsPaid).update(Some(true)).map(_ => Ok)

                          case false =>
                            DBIO.successful(NotFound)
                        }
                      }

                      db.run {
                        val query = OrderTable.filter(_.id === lorderId).filterNot(_.paymentType === "").filterNot(_.dateFulfilled.isEmpty).filter(_.dateFulfilled.isDefined)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true =>
                            query.map(_.status).update(OrderRow.Status.Void).map(_ => Ok)
                          case false =>
                            DBIO.successful(NotFound)
                        }
                      }
                    }
                  }
                }

                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  /*def update(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val consultation = for {
      status <- (request.body \ "status").validateOpt[OrderRow.Status]
      paymentType <- (request.body \ "paymentType").validateOpt[String]
      checkNumber <- (request.body \ "checkNumber").validateOpt[String]
    } yield {
      (status, paymentType, checkNumber)
    }
    consultation match {
      case JsSuccess((status,paymentType,checkNumber), _) =>
        db.run {
          val query = OrderTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Write](
                query.filter(_.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status]).exists.result.flatMap[Result, NoStream, Effect.Write] {
                  case true if status == OrderRow.Status.Fulfilled =>
                    query.map(row => (row.status, row.dateFulfilled, row.approvedBy)).update((status, Some(DateTime.now), Some(request.identity.name))).map(_ => Ok)
                  case _ =>

                    query.map(_.status).update(status).map(_ => Ok)
                }
                status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                paymentType.map(value => query.map(_.paymentType).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                checkNumber.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map { _ =>
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }*/

  /*def updateVendor(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val vendor = for {
      orderId <- (request.body \ "orderId").validateOpt[Long]
      vendorId <- (request.body \ "vendorId").validateOpt[Long]
      paymentType <- (request.body \ "paymentType").validateOpt[String]
      checkNumber <- (request.body \ "checkNumber").validateOpt[String]
      invoice <- (request.body \ "invoice").validateOpt[String]
    } yield {
      (orderId, vendorId, paymentType, checkNumber,invoice)
    }

    vendor match {
      case JsSuccess((orderId, vendorId, paymentType, checkNumber,invoice), _) =>
        db.run {
          val query = VendorPaymentTrackTable.filter(_.vendorId === vendorId).filter(_.orderId === orderId)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              DBIO.seq[Effect.Write](
                orderId.map(value => query.map(_.orderId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                vendorId.map(value => query.map(_.vendorId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                paymentType.map(value => query.map(_.paymentType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                checkNumber.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                invoice.map(value => query.map(_.checkNumber).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                query.map(_.paid).update(true)
              ) map { _ =>

                db.run {
                  for {
                    unPaidCount <- VendorPaymentTrackTable.filter(_.orderId === orderId).filterNot(_.paid).map(_.id).countDistinct.result
                  } yield {
                    if (unPaidCount == 0) {
                      val lorderId: Long = orderId match {
                        case None => 0 //Or handle the lack of a value another way: throw an error, etc.
                        case Some(l: Long) => l //return  to set your value
                      }
                      db.run {
                        val query = OrderTable.filter(_.id === lorderId)
                        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
                          case true =>
//                            query.map(_.status).update(OrderRow.Status.Void).map(_ => Ok)
                            DBIO.seq[Effect.Write](
                              query.map(_.status).update(OrderRow.Status.Void),
                              query.map(_.allVendorsPaid).update(Some(true))
                            ) map { _ =>
                              Ok
                            }
                          case false =>
                            DBIO.successful(NotFound)
                        }
                      }
                    }
                  }
                }

                db.run {
                  val query = OrderItemTable.filter(_.productVendorId === vendorId).filter(_.orderId === orderId)
                  query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
                    case true =>
                      query.map(_.itemPaid).update(Some(true)).map(_ => Ok)
                    case false =>
                      DBIO.successful(NotFound)
                  }
                }
                Ok
              }
            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }*/


   def generateCSV(statusVal: Option[String]) = silhouette.SecuredAction { request =>

    var staffName: String = ""
    var practiceName: String = ""
    var orderId: String = ""
    var orderDate: String = ""
    var orderFullfilled: String = ""
    var status: String = ""
    var total: String = ""
    var vstatus: String = convertOptionString(statusVal)
    var csvList = List.empty[OrderCSVList]

    
    var formatter = DateTimeFormat.forPattern("MMMMMMMMM dd, yyyy hh:mm a");

    var query = OrderTable
                  .join(OrderItemTable).on(_.id === _.orderId)
                  .join(StaffTable).on(_._1.staffId === _.id)
                  .join(PracticeTable).on(_._2.practiceId === _.id)


        if (vstatus=="Received"){
          request.identity match {
            case row: StaffRow => query = query.filter(s => s._1._1._1.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] || s._1._1._1.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status]  || s._1._1._1.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )
            case row: UserRow => query = query.filter(_._1._1._1.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )
            }
          // query = query.filter(_._1._1._1.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )
        }else if (vstatus=="Fulfilled"){
          query = query.filter(_._1._1._1.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status])
        }else if (vstatus=="Canceled"){
          query = query.filter(_._1._1._1.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )
        }else if (vstatus=="Closed"){
          query = query.filter(_._1._1._1.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )
        }else if (vstatus=="Returned"){
          query = query.filter(_._1._1._1.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status])
        }                  

      var orderDataList = db.run {

        val orders = query

          request.identity match {
            case row: StaffRow =>
              orders.filter(_._1._2.practiceId === row.practiceId).map({
                case (((orders, orderItems), staff), practice) => (orders, orderItems, staff.name, practice.name)
              })
              .result
            case _ => orders.map({
                case (((orders, orderItems), staff), practice) => (orders, orderItems, staff.name, practice.name)
              })
              .result
          }

      } map { orders => 

      val data = orders.groupBy(_._1.id).map {
          case (id, items) => {
              val item = items.head._1
              // val orderItems = items.map(i =>  i._2.product.price * i._2.amount).sum
              val orderItems = Math.round(convertOptionFloat(item.grandTotal) * 100.0) / 100.0
              val staffDetail = items.head._3
              val practiceDetail = items.head._4


              staffName = staffDetail
              practiceName = practiceDetail
              orderId = convertOptionLong(item.id).toString
              orderDate = formatter.print(item.dateCreated)
              orderFullfilled = item.dateFulfilled match {
                case None => "-"//Or handle the lack of a value another way: throw an error, etc.
                case Some(s: DateTime) => formatter.print(s) //return  to set your value
              }
              total = orderItems.toString
              status = item.status.toString

              if(status == "Void"){
                status = "Closed"
              }

              request.identity match {
            case row: StaffRow =>
              if(status == "Fulfilled" || status == "Closed"){
                status = "Received"
              }
            case row: UserRow =>
            }

              var dataList = OrderCSVList(orderId, staffName, practiceName, orderDate, orderFullfilled, total, status)
              csvList = csvList :+ dataList
          }
      }
   }

    val AwaitResult = Await.ready(orderDataList, atMost = scala.concurrent.duration.Duration(120, SECONDS))
    val file = new File("/tmp/orders.csv")
    val writer = CSVWriter.open(file)
    writer.writeRow(List("#", "Dentist", "Practice", "Order Date", "Fulfill Date", "Total", "Status"))
    csvList.sortBy(_.orderId).reverse.map {
      orderList => writer.writeRow(List(orderList.orderId, orderList.staffName, orderList.practiceName, orderList.orderDate, orderList.orderFullfilled, orderList.total, orderList.status))
    }
    writer.close()
    Ok.sendFile(file, inline = false, _ => file.getName)
}

def orderHistoryList(startDate: String, endDate: String, statusVal: Option[String], staffId: Option[String])=silhouette.SecuredAction.async { request =>

  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var staffName: String = ""
  var practiceName: String = ""
  var orderId: String = ""
  var orderDate: String = ""
  var status: String = convertOptionString(statusVal)
  var vStatus: String = ""
  var grandTotal: String = ""
  var totalSavings: String = ""
  var formatter = DateTimeFormat.forPattern("MM/dd/YYYY");
  var pageNumber:Int = 0
  var recPerPage:Int = 0

  var pageNumberVal: Option[String] = request.headers.get("pageNumber")
  if ( pageNumberVal != None){
    pageNumber = convertOptionString(pageNumberVal).toInt
  }

  if (pageNumber == 0 ){
    pageNumber = 1
  }

  var recPerPageVal: Option[String] = request.headers.get("recPerPage")
  if( recPerPageVal != None){
    recPerPage = convertOptionString(recPerPageVal).toInt
  }

  var query = OrderTable.filter(a => a.dateCreated >= strStartDate && a.dateCreated < strEndTime)

  if(staffId != None && staffId != Some("all")){
    query = query.filter(_.staffId === convertOptionString(staffId).toLong)
  }

  if(status != None && status != Some("all")){
    if (status=="Received") {
      query = query.filter(_.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )
    } else if (status=="Fulfilled") {
      query = query.filter(_.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status])
    } else if (status=="Canceled") {
      query = query.filter(_.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )
    } else if (status=="Closed") {
      query = query.filter(_.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )
    } else if (status=="Returned") {
      query = query.filter(_.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status])
    }
  }

  db.run{
    query
      .join(StaffTable).on(_.staffId === _.id)
      .join(PracticeTable).on(_._2.practiceId === _.id).
      map({
        case ((orders, staff), practice) => (orders, staff.name, practice.name)
      })
      .result
  } map { ordersMap =>
    val data = ordersMap.groupBy(_._1.id).map{
      case (id, items) => {
        val item = items.head._1
        val staffDetail = items.head._2
        val practiceDetail = items.head._3
        staffName = staffDetail
        practiceName = practiceDetail
        orderId = convertOptionLong(item.id).toString
        vStatus = item.status.toString
        if(vStatus == "Void"){
          vStatus = "Closed"
        }
        orderDate = formatter.print(item.dateCreated)
        grandTotal = Math.round(convertOptionFloat(item.grandTotal)).toString
        totalSavings = Math.round(convertOptionFloat(item.totalAmountSaved)).toString
        OrderHistoryReport(
          orderId,
          staffName,
          practiceName,
          orderDate,
          vStatus,
          grandTotal,
          totalSavings
        )
      }
    }.toList.sortBy(_.orderId)
    Ok(Json.toJson(data))
      if (recPerPage == 0){
        val pageData = data
        Ok(Json.toJson(pageData)).withHeaders(
          ("totalCount" -> data.length.toString()))
      }
      else{
        val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
        Ok(Json.toJson(pageData)).withHeaders(
          ("totalCount" -> data.length.toString()))
      }
  }


}


  def delete(id: Long) = silhouette.SecuredAction {
    MethodNotAllowed
  }

def catalogOrdersRevenue(startDate: String,endDate: String,vendorId: Option[String]) = silhouette.SecuredAction.async { request =>
  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var query = OrderItemTable.sortBy(_.orderId)
  var totalNumber: Long = 0
  var totalRevenue: Double = 0
  var totalTax: Double = 0
  var totalShipping: Double = 0
  var totalSaving: Double = 0
  var totalNetGross: Double = 0
  var totalNetProfit: Double = 0


  if(vendorId != None && vendorId != Some("all")){
    query = query.filter(_.productVendorId === convertOptionString(vendorId).toLong)
  }

  var block = db.run {
        for {
          orderItemlength <- query.join(OrderTable).on(_.orderId === _.id).filter(s=> s._2.dateCreated >= strStartDate && s._2.dateCreated < strEndTime).map(_._1.orderId).countDistinct.result
        } yield{
          totalNumber = orderItemlength
        }
  }
  val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  db.run{
      query.join(OrderTable).on(_.orderId === _.id).filter(s=> s._2.dateCreated >= strStartDate && s._2.dateCreated < strEndTime).result
  } map { orderItemsMap =>

        val data = orderItemsMap.map { orderItems =>
         totalRevenue = totalRevenue + (orderItems._1.product.price * orderItems._1.amount)
        }

      val data1 = orderItemsMap.groupBy(_._2.id).map {
      case (orderItem, entries) =>
       val orders = entries.head._2
        totalTax = totalTax + convertOptionFloat(orders.tax)
        totalSaving = totalSaving + convertOptionFloat(orders.totalAmountSaved)
        totalShipping = totalShipping + convertOptionFloat(orders.shippingCharge)
        totalNetGross = totalRevenue - (totalTax + totalShipping)
        totalNetProfit = (totalNetGross * 6) / 100
      }

    Ok(Json.obj(
    "totalOrders" -> totalNumber,
    "totalRevenue" -> Math.round(totalRevenue * 100.0) / 100.0,
    "totalTax" -> Math.round(totalTax * 100.0) / 100.0,
    "totalShipping" -> Math.round(totalShipping * 100.0) / 100.0,
    "totalSaving" -> Math.round(totalSaving * 100.0) / 100.0,
    "totalNetGross" -> Math.round(totalNetGross * 100.0) / 100.0,
    "totalNetProfit" -> Math.round(totalNetProfit * 100.0) / 100.0
    ))
  }
}

def generateCatalogRevenueCSV(startDate: String,endDate: String,vendorId: Option[String]) = silhouette.SecuredAction { request =>
  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var query = OrderItemTable.sortBy(_.orderId)
  var totalNumber: Long = 0
  var totalRevenue: Double = 0
  var totalTax: Double = 0
  var totalShipping: Double = 0
  var totalSaving: Double = 0
  var totalNetGross: Double = 0
  var totalNetProfit: Double = 0
  var vendorName: String = "All"
  var csvList = List.empty[CatalogReportCSVList]


  if(vendorId != None && vendorId != Some("all")){
    query = query.filter(_.productVendorId === convertOptionString(vendorId).toLong)

    var vendorData =  db.run {
      VendorTable.filter(_.id === convertOptionString(vendorId).toLong).result
    } map { vendors =>
      (vendors).foreach(vendor => {
        vendorName = vendor.name
      })
    }
  }

  var block = db.run {
        for {
          orderItemlength <- query.join(OrderTable).on(_.orderId === _.id).filter(s=> s._2.dateCreated >= strStartDate && s._2.dateCreated < strEndTime).map(_._1.orderId).countDistinct.result
        } yield{
          totalNumber = orderItemlength
        }
  }
  val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  var catalogRevenueReportList = db.run{
      query.join(OrderTable).on(_.orderId === _.id).filter(s=> s._2.dateCreated >= strStartDate && s._2.dateCreated < strEndTime).result
  } map { orderItemsMap =>

        val data = orderItemsMap.map { orderItems =>
         totalRevenue = totalRevenue + (orderItems._1.product.price * orderItems._1.amount)
        }

      val data1 = orderItemsMap.groupBy(_._2.id).map {
      case (orderItem, entries) =>
       val orders = entries.head._2
        totalTax = totalTax + convertOptionFloat(orders.tax)
        totalSaving = totalSaving + convertOptionFloat(orders.totalAmountSaved)
        totalShipping = totalShipping + convertOptionFloat(orders.shippingCharge)
        totalNetGross = totalRevenue - (totalTax + totalShipping)
        totalNetProfit = (totalNetGross * 6) / 100
      }

    var dataList = CatalogReportCSVList(vendorName, startDate, endDate, totalNumber, Math.round(totalRevenue * 100.0) / 100.0, Math.round(totalShipping * 100.0) / 100.0, Math.round(totalTax * 100.0) / 100.0, Math.round(totalNetGross * 100.0) / 100.0, Math.round(totalNetProfit * 100.0) / 100.0)
    csvList = csvList :+ dataList
  }

  val AwaitResult1 = Await.ready(catalogRevenueReportList, atMost = scala.concurrent.duration.Duration(120, SECONDS))

  val file = new java.io.File("/tmp/catalogRevenueReport.csv")
  val writer = CSVWriter.open(file)
  writer.writeRow(List("Name of vendor", "From Date", "To Date", "Number of Orders", "Total Gross Revenue", "Total Shipping", "Total Tax", "Total Net Gross", "Net Profit"))
  csvList.map {
    catalogRevenueList => writer.writeRow(List(catalogRevenueList.vendorName, catalogRevenueList.startDate, catalogRevenueList.endDate, catalogRevenueList.totalNumber, catalogRevenueList.totalRevenue, catalogRevenueList.totalShipping, catalogRevenueList.totalTax, catalogRevenueList.totalNetGross, catalogRevenueList.totalNetProfit))
  }
  writer.close()
  Ok.sendFile(file, inline = false, _ => file.getName)
}

def orderHistoryCsv(startDate: String, endDate: String, statusVal: Option[String], staffId: Option[String]) = silhouette.SecuredAction { request =>
  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var staffName: String = ""
  var practiceName: String = ""
  var orderId: String = ""
  var orderDate: String = ""
  var status: String = convertOptionString(statusVal)
  var vStatus: String = ""
  var grandTotal: String = ""
  var totalSavings: String = ""
  var formatter = DateTimeFormat.forPattern("MM/dd/YYYY");
  var pageNumber:Int = 0
  var recPerPage:Int = 0
  var csvList = List.empty[OrderHistoryReport]

  var pageNumberVal: Option[String] = request.headers.get("pageNumber")
  if ( pageNumberVal != None){
    pageNumber = convertOptionString(pageNumberVal).toInt
  }

  if (pageNumber == 0 ){
    pageNumber = 1
  }

  var recPerPageVal: Option[String] = request.headers.get("recPerPage")
  if( recPerPageVal != None){
    recPerPage = convertOptionString(recPerPageVal).toInt
  }

  var query = OrderTable.filter(a => a.dateCreated >= strStartDate && a.dateCreated < strEndTime)

  if(staffId != None && staffId != Some("all")){
    query = query.filter(_.staffId === convertOptionString(staffId).toLong)
  }

    if(status != None && status != Some("all")){
    if (status=="Received") {
      query = query.filter(_.status === OrderRow.Status.Received.asInstanceOf[OrderRow.Status] )
    } else if (status=="Fulfilled") {
      query = query.filter(_.status === OrderRow.Status.Fulfilled.asInstanceOf[OrderRow.Status])
    } else if (status=="Canceled") {
      query = query.filter(_.status === OrderRow.Status.Canceled.asInstanceOf[OrderRow.Status] )
    } else if (status=="Closed") {
      query = query.filter(_.status === OrderRow.Status.Void.asInstanceOf[OrderRow.Status] )
    } else if (status=="Returned") {
      query = query.filter(_.status === OrderRow.Status.Returned.asInstanceOf[OrderRow.Status])
    }
  }

var block = db.run{
    query.join(StaffTable).on(_.staffId === _.id).join(PracticeTable).on(_._2.practiceId === _.id).map({
        case ((orders, staff), practice) => (orders, staff.name, practice.name)
      }).result
  } map { ordersMap =>
    val data = ordersMap.groupBy(_._1.id).map{
      case (id, items) => {
        val item = items.head._1
        val staffDetail = items.head._2
        val practiceDetail = items.head._3
        staffName = staffDetail
        practiceName = practiceDetail
        orderId = convertOptionLong(item.id).toString
        vStatus = item.status.toString
        if(vStatus == "Void"){
          vStatus = "Closed"
        }
        orderDate = formatter.print(item.dateCreated)
        grandTotal = Math.round(convertOptionFloat(item.grandTotal)).toString
        totalSavings = Math.round(convertOptionFloat(item.totalAmountSaved)).toString

        var dataList = OrderHistoryReport(orderId,staffName,practiceName,orderDate,vStatus,grandTotal,totalSavings)
        csvList = csvList :+ dataList
      }
    }
  }

  val AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  val file = new java.io.File("/tmp/Order Report.csv")
  val writer = CSVWriter.open(file)
  writer.writeRow(List("Orders", "Dentist", "Practice", "Order Date", "Total", "Total Savings", "Status"))
  csvList.sortBy(_.orderId).map {
    orderList => writer.writeRow(List(orderList.orderId,orderList.staffName,orderList.practiceName,orderList.orderDate,orderList.grandTotal,orderList.totalSavings,orderList.status))
  }
  writer.close()
  Ok.sendFile(file, inline = false, _ => file.getName)
}

}
