package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.CourseRow
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by stefan on 5/15/17.
  */

@Singleton
class CoursesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._

  implicit val educationType = new Format[CourseRow.EducationType] {
    import CourseRow.EducationType._
    override def writes(educationType: CourseRow.EducationType) = Json.toJson {
      educationType match {
        case DoctorEducation => "Doctor"
        case PatientEducation => "Patient"
        case ResourceCenter => "Resource"
        case _ => "Unknown"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Doctor" => DoctorEducation
      case "Patient" => PatientEducation
      case "Resource" => ResourceCenter
      case _ => Unknown
    }
  }

  implicit val courseFormat = Json.format[CourseRow]

  object Output {
    case class Course(
      id: Option[Long],
      title: String,
      url: String,
      thumbnailUrl: Option[String],
      courseQuestionnaire: Option[String],
      dateCreated: DateTime,
      category: Option[String],
      courseId: Option[String],
      isRequested: Option[Boolean],
      speaker: Option[String],
      educationType: CourseRow.EducationType
    )
    implicit val courseFormatRowWrites = Json.format[Output.Course]

    def output(course: CourseRow, isRequested: Boolean) = Output.Course(course.id, course.title, course.url, course.thumbnailUrl, course.courseQuestionnaire, course.dateCreated, course.category, course.courseId, Some(isRequested), course.speaker, course.educationType)
  }

  def readAll = silhouette.SecuredAction.async { request =>
    db.run{
      CoursesTable
        .sortBy(_.title).sortBy(_.category)
        .joinLeft(EducationRequestTable.filter(_.staffId === request.identity.id))
        .on(_.id === _.courseId)
        .result
    } map { rows =>
      val courses = rows.map(row => Output.output(row._1, row._2.isDefined))
      Ok(Json.toJson(courses))
    }
  }

  def read(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      CoursesTable
        .filter(_.id === id)
        .sortBy(_.title).sortBy(_.category)
        .joinLeft(EducationRequestTable.filter(_.staffId === request.identity.id))
        .on(_.id === _.courseId)
        .result
        .headOption
    } map {
      case Some(row) => Ok(Json.toJson(Output.output(row._1, row._2.isDefined)))
      case None => NotFound
    }
  }

  def create = silhouette.SecuredAction.async(parse.json) {
    request =>
    val course = for {
      title <- (request.body \ "title").validate[String]
      url <- (request.body \ "url").validate[String]
      thumbnailUrl <- (request.body \ "thumbnailUrl").validateOpt[String]
      courseQuestionnaire <- (request.body \ "courseQuestionnaire").validateOpt[String]
      category <- (request.body \ "category").validateOpt[String]
      courseId <- (request.body \ "courseId").validateOpt[String]
      speaker <- (request.body \ "speaker").validateOpt[String]
      educationType <- (request.body \ "type").validateOpt[CourseRow.EducationType]
    } yield {
      CourseRow(None, title, url, thumbnailUrl, courseQuestionnaire, DateTime.now, category, courseId, speaker, educationType.getOrElse(CourseRow.EducationType.DoctorEducation))
    }

    course match {
      case JsSuccess(courseRow, _) => db.run {
        CoursesTable.returning(CoursesTable.map(_.id)) += courseRow
      } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(courseRow.copy(id = Some(id))))
      }
      case JsError(_) => Future.successful(BadRequest)
    }
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val course = for {
      title <- (request.body \ "title").validateOpt[String]
      url <- (request.body \ "url").validateOpt[String]
      thumbnailUrl <- (request.body \ "thumbnailUrl").validateOpt[String]
      courseQuestionnaire <- (request.body \ "courseQuestionnaire").validateOpt[String]
      category <- (request.body \ "category").validateOpt[String]
      courseId <- (request.body \ "courseId").validateOpt[String]
      speaker <- (request.body \ "speaker").validateOpt[String]
    } yield {
      (title, url, thumbnailUrl, courseQuestionnaire, category, courseId, speaker)
    }

    course match {
      case JsSuccess((title, url, thumbnailUrl, courseQuestionnaire, category, courseId, speaker), _) => db.run {
        val query = CoursesTable.filter(_.id === id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            title.map(value => query.map(_.title).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            url.map(value => query.map(_.url).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            thumbnailUrl.map(value => query.map(_.thumbnailUrl).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            courseQuestionnaire.map(value => query.map(_.courseQuestionnaire).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            category.map(value => query.map(_.category).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            courseId.map(value => query.map(_.courseId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            speaker.map(value => query.map(_.speaker).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      case JsError(_) => Future.successful(BadRequest)
    }

  }
  
  def delete(id: Long) = silhouette.SecuredAction.async { request =>
    db.run{
      CoursesTable.filter(_.id === id).delete map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

}
