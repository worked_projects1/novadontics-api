package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasher
import controllers.NovadonticsController
import models.daos.tables.{PracticeRow, StaffRow,UserRow,ReferralSourceRow,WorkingHoursRow,InsuranceFeeRow,OperatorTableRow}
import play.api.Application
import play.api.libs.json.{JsError, JsSuccess, Json, Writes}
import play.api.mvc.Result
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.libs.json._
import scala.util.Random
import java.security.MessageDigest
import java.util.Base64
import java.net.URLEncoder
import scalaj.http.{Http, HttpOptions}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PracticeController @Inject()(passwordHasher: PasswordHasher, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  val doseSpotUrl = application.configuration.getString("doseSpotUrl").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotUrl` property."))
  val doseSpotAdminId = application.configuration.getString("doseSpotAdminId").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotAdminId` property."))
  val doseSpotClinicId = application.configuration.getString("doseSpotClinicId").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotClinicId` property."))
  val doseSpotClinicKey = application.configuration.getString("doseSpotClinicKey").getOrElse(throw new RuntimeException("Configuration is missing `doseSpotClinicKey` property."))

  implicit val practiceRowWrites = Writes { practice: PracticeRow =>
    Json.obj(
      "id" -> practice.id,
      "name" -> practice.name,
      "country" -> practice.country,
      "city" -> practice.city,
      "state" -> practice.state,
      "zip" -> practice.zip,
      "address" -> practice.address,
      "phone" -> practice.phone,
      "xvWebURL" -> practice.xvWebURL,
      "xvWebUserName" -> practice.xvWebUserName,
      "shortId" -> practice.shortId,
      "timeZone" -> practice.timeZone,
      "phoneExt" -> practice.phoneExt,
      "phone1" -> practice.phone1,
      "phoneExt1" -> practice.phoneExt1,
      "fax" -> practice.fax,
      "email" -> practice.email,
      "taxId" -> practice.taxId,
      "insBillingProvider" -> practice.insBillingProvider,
      "billingLicense" -> practice.billingLicense,
      "openingDate" -> practice.openingDate,
      "officeGroup" -> practice.officeGroup,
      "ucrFeeSchedule" -> practice.ucrFeeSchedule,
      "feeSchedule" -> practice.feeSchedule,
      "officeId" -> practice.officeId,
      "doseSpotClinicId" -> practice.doseSpotClinicId,
      "doseSpotClinicKey" -> practice.doseSpotClinicKey,
      "chartNumberType" -> practice.chartNumberType,
      "chartNumberSeq" -> practice.chartNumberSeq,
      "suite" -> practice.suite,
      "appointmentReminder" -> practice.appointmentReminder,
      "appReminderSetting" -> practice.appReminderSetting,
      "onlineRegReminderFrequency" -> practice.onlineRegReminderFrequency,
      "onlineRegMaxReminder" -> practice.onlineRegMaxReminder,
      "onlineRegistrationModule" -> practice.onlineRegistrationModule,
      "onlineInviteMessage" -> practice.onlineInviteMessage,
      "appointmentReminderMessage" -> practice.appointmentReminderMessage,
      "consentFormMessage" -> practice.consentFormMessage,
      "amenities" -> practice.amenities,
      "photos" -> practice.photos,
      "latitude" -> practice.latitude,
      "longitude" -> practice.longitude,
      "clinicDescription" -> practice.clinicDescription,
      "trojanAccountNo" -> practice.trojanAccountNo,
      "postopInstructionsMessage" -> practice.postopInstructionsMessage,
      "recallReminderMessage" -> practice.recallReminderMessage
    )
  }

  def readAll = silhouette.SecuredAction.async { request =>

    var userRole = ""
    var practiceQuery = PracticeTable.filterNot(_.archived)
    val query = request.identity match {
      case UserRow(id, _, _, _, _, _, _) =>
        UserTable.filter(_.id === id).map(_.role)
      case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
        StaffTable.filter(_.id === id).map(_.role)
    }
    var userRoleData = db.run {  query.result } map { result => userRole = result.head.toString }
    var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

    if(userRole == "Dentist") {
      var strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
      practiceQuery = practiceQuery.filter(_.id === strPracticeId)
    }

    db.run {
      practiceQuery.sortBy(_.name).result
    } map { practices =>
      Ok(Json.toJson(practices))
    }
  }

  def create = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
  val formatter = DateTimeFormat.forPattern("hh:mm a");

    val practice = for {
      name <- (request.body \ "name").validate[String]
      country <- (request.body \ "country").validate[String]
      city <- (request.body \ "city").validate[String]
      state <- (request.body \ "state").validate[String]
      zip <- (request.body \ "zip").validate[String]
      address <- (request.body \ "address").validate[String]
      phone <- (request.body \ "phone").validate[String]
      xvWebURL <- (request.body \ "xvWebURL").validateOpt[String]
      xvWebUserName <- (request.body \ "xvWebUserName").validateOpt[String]
      xvWebPassword <- (request.body \ "xvWebPassword").validateOpt[String]
      doseSpotClinicId <- (request.body \ "doseSpotClinicId").validateOpt[String]
      doseSpotClinicKey <- (request.body \ "doseSpotClinicKey").validateOpt[String]
      chartNumberSeq <- (request.body \ "chartNumberSeq").validateOpt[Long]
      suite <- (request.body \ "suite").validateOpt[String]
      appointmentReminder <- (request.body \ "appointmentReminder").validateOpt[Boolean]
      onlineRegistrationModule <- (request.body \ "onlineRegistrationModule").validateOpt[Boolean]
      amenities <- (request.body \ "amenities").validateOpt[String]
      photos <- (request.body \ "photos").validateOpt[String]
      latitude <- (request.body \ "latitude").validateOpt[Double]
      longitude <- (request.body \ "longitude").validateOpt[Double]
      clinicDescription <- (request.body \ "clinicDescription").validateOpt[String]
      trojanAccountNo <- (request.body \ "trojanAccountNo").validateOpt[Long]
    } yield {
      var appReminderSetting = ""
      var appReminder = false
      if(convertOptionalBoolean(appointmentReminder) == true){
          appReminder = true
          appReminderSetting = "One Day Prior"
      }

      var onlineInviteMessage = "Hello <<patient name>>, your dental office would like you to fill and submit the following online form below. A verification code will be sent to you in a separate text. You will need the code to proceed with the form. <<link>>";
      var appointmentReminderMessage = "Hello <<patient name>>, <<practice name>> would like to remind you with your dental appointment on <<date>> <<time>>. We look forward to seeing you. Please click on the link below to manage your appointment <<link>>";
      var consentFormMessage = "Hello <<patient name>>, your dental office would like you to sign and submit the following <<form name>> below. <<link>>";
      var postopInstructionsMessage = "Hello <<patient name>>, \\nPlease find the below <<form name>>.\\n<<link>>"
      var recallReminderMessage = "Dear <<patient name>>, Your <<interval>> checkup appointment with <<practice name>> is coming up soon. Please call our practice at: <<practice phone>> to schedule your appointment. We can’t wait to see you!"

      PracticeRow(None, name, country, city, state, zip, address, phone, DateTime.now, xvWebURL, xvWebUserName, xvWebPassword, None, Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), None, Some(""), Some(""), None, Some(""), Some(""), Some(""), Some(""),doseSpotClinicId, doseSpotClinicKey,None,chartNumberSeq,suite,Some(appReminder),Some(appReminderSetting),Some("Every day"),Some("3"),onlineRegistrationModule,Some(onlineInviteMessage),Some(appointmentReminderMessage),Some(consentFormMessage),amenities,photos,latitude,longitude,clinicDescription,trojanAccountNo,Some(""),Some(""))
    }

    practice match {
      case JsSuccess(practiceRow, _) =>

      var stateNotFound = false
      var phoneValidation = false
      var zipCodeValidation = false
      var countries = "us,united states,usa"

      if(practiceRow.country != "" && practiceRow.country != None){
        if(countries contains practiceRow.country.toLowerCase){
          var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === practiceRow.state.toLowerCase || s.stateName.toLowerCase === practiceRow.state.toLowerCase).length.result
          } map { result =>
          if(result == 0 ){
              stateNotFound = true
          }
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          var phoneNumber = practiceRow.phone.replaceAll("[^a-zA-Z0-9]", "")
          if(phoneNumber.length != 10 || phoneNumber.startsWith("0") || phoneNumber.startsWith("1") || phoneNumber.startsWith("123")){
              phoneValidation = true
          }

          if(!(practiceRow.zip.forall(_.isDigit) && (practiceRow.zip.length == 5 || practiceRow.zip.length == 9))){
             zipCodeValidation = true
          }
        }
      }

      if(stateNotFound){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid state name!"}""")))
      } else if(phoneValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
      } else if(zipCodeValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid zipcode!"}""")))
      } else {
        db.run {
          val duplicate = for {
            exists <- PracticeTable.filter(_.name === practiceRow.name).exists.result
          } yield {
            exists
          }
          duplicate.flatMap {
            case true => DBIO.successful(0L)
            case false => PracticeTable.returning(PracticeTable.map(_.id)) += practiceRow
          }.transactionally
        } map {
          case 0L => PreconditionFailed(Json.obj("error" -> "Practice already exists"))
          case id =>
          var ucrId = -1
          var privateId = 0
         var block = db.run{
            DefaultFeeTable.result
          } map { defaulFeeList =>
            (defaulFeeList).foreach(result => {
                defaultInsFeeCreation(InsuranceFeeRow(None, id, privateId, result.procedureCode, result.privateFee))
                defaultInsFeeCreation(InsuranceFeeRow(None, id, ucrId, result.procedureCode, result.ucrFee))
              })
          }
          Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          val weekDays = List("Monday","Tuesday","Wednesday","Thursday","Friday")
          weekDays.foreach(weekDaysValue => {
          workingHoursCreation(WorkingHoursRow(None,id,weekDaysValue,LocalTime.parse("07:00 AM",formatter),LocalTime.parse("07:00 PM",formatter),Some(LocalTime.parse("12:00 PM",formatter)),Some(LocalTime.parse("01:00 PM",formatter))))
          })

          val mediaList = List("Billboard","Facebook","Google Search","Instagram","LinkedIn","Magazine Ad","Newspaper Ad","Postcard","Printed Material","Radio","Television","Twitter","Yelp","YouTube")
          mediaList.foreach(listValue => {
          migrateReferralSource(ReferralSourceRow(None,id,listValue,"media","","","",false))
          })
          val walkInList = List("Walk-In")
          walkInList.foreach(listValue => {
            migrateReferralSource(ReferralSourceRow(None,id,listValue,"walk-in","","","",false))
          })
          val operatoriesList = List("Operatory 1","Operatory 2","Operatory 3","Operatory 4","Operatory 5","Operatory 6","Operatory 7")
          var order = 0
          operatoriesList.foreach(operatoryName => {
            order+=1
            operatoriesCreation(OperatorTableRow(None,id,operatoryName,false,order))
          })
          Created(Json.toJson(practiceRow.copy(id = Some(id))))
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

def workingHoursCreation(workingHoursRow : WorkingHoursRow){
        var block = db.run {
            WorkingHoursTable.returning(WorkingHoursTable.map(_.id)) += (workingHoursRow)
             } map {
                case 0L => PreconditionFailed
                case id => Ok(Json.obj( "status"->"success","message"->"Working hours created successfully"))
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }

def defaultInsFeeCreation(insuranceFeeRow : InsuranceFeeRow){
        var block = db.run {
            InsuranceFeeTable.returning(InsuranceFeeTable.map(_.id)) += (insuranceFeeRow)
             } map {
                case 0L => PreconditionFailed
                case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }

def migrateReferralSource(referralSourceRow : ReferralSourceRow){
        var block = db.run {
            ReferralSourceTable.returning(ReferralSourceTable.map(_.id)) += (referralSourceRow)
             } map {
                case 0L => PreconditionFailed
                case id => Ok(Json.obj( "status"->"success","message"->"Form Migrated successfully"))
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }

def operatoriesCreation(operatorRow : OperatorTableRow){
        var block = db.run {
            OperatorTable.returning(OperatorTable.map(_.id)) += (operatorRow)
             } map {
                case 0L => PreconditionFailed
                case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
  }

  def read(id: Long) = silhouette.SecuredAction.async {
    db.run {
      PracticeTable.filterNot(_.archived).filter(_.id === id).result.headOption
    } map {
      case Some(practice) => Ok(Json.toJson(practice))
      case None => NotFound
    }
  }

  def update(id: Long) = silhouette.SecuredAction.async(parse.json) { request =>
    val dentist = for {
      name <- (request.body \ "name").validateOpt[String]
      country <- (request.body \ "country").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      phone <- (request.body \ "phone").validateOpt[String]
      xvWebURL <- (request.body \ "xvWebURL").validateOpt[String]
      xvWebUserName <- (request.body \ "xvWebUserName").validateOpt[String]
      xvWebPassword <- (request.body \ "xvWebPassword").validateOpt[String]
      shortId <- (request.body \ "shortId").validateOpt[String]
      timeZone <- (request.body \ "timeZone").validateOpt[String]
      phoneExt <- (request.body \ "phoneExt").validateOpt[String]
      phone1 <- (request.body \ "phone1").validateOpt[String]
      phoneExt1 <- (request.body \ "phoneExt1").validateOpt[String]
      fax <- (request.body \ "fax").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      taxId <- (request.body \ "taxId").validateOpt[Long]
      insBillingProvider <- (request.body \ "insBillingProvider").validateOpt[String]
      billingLicense <- (request.body \ "billingLicense").validateOpt[String]
      openingDate <- (request.body \ "openingDate").validateOpt[LocalDate]
      officeGroup <- (request.body \ "officeGroup").validateOpt[String]
      ucrFeeSchedule <- (request.body \ "ucrFeeSchedule").validateOpt[String]
      feeSchedule <- (request.body \ "feeSchedule").validateOpt[String]
      officeId <- (request.body \ "officeId").validateOpt[String]
      doseSpotClinicId <- (request.body \ "doseSpotClinicId").validateOpt[String]
      doseSpotClinicKey <- (request.body \ "doseSpotClinicKey").validateOpt[String]
      chartNumberType <- (request.body \ "chartNumberType").validateOpt[String]
      chartNumberSeq <- (request.body \ "chartNumberSeq").validateOpt[Long]
      suite <- (request.body \ "suite").validateOpt[String]
      appointmentReminder <- (request.body \ "appointmentReminder").validateOpt[Boolean]
      appReminderSetting <- (request.body \ "appReminderSetting").validateOpt[String]
      onlineRegReminderFrequency <- (request.body \ "onlineRegReminderFrequency").validateOpt[String]
      onlineRegMaxReminder <- (request.body \ "onlineRegMaxReminder").validateOpt[String]
      onlineRegistrationModule <- (request.body \ "onlineRegistrationModule").validateOpt[Boolean]
      onlineInviteMessage <- (request.body \ "onlineInviteMessage").validateOpt[String]
      appointmentReminderMessage <- (request.body \ "appointmentReminderMessage").validateOpt[String]
      consentFormMessage <- (request.body \ "consentFormMessage").validateOpt[String]
      amenities <- (request.body \ "amenities").validateOpt[String]
      photos <- (request.body \ "photos").validateOpt[String]
      latitude <- (request.body \ "latitude").validateOpt[Double]
      longitude <- (request.body \ "longitude").validateOpt[Double]
      clinicDescription <- (request.body \ "clinicDescription").validateOpt[String]
      trojanAccountNo <- (request.body \ "trojanAccountNo").validateOpt[Long]
      postopInstructionsMessage <- (request.body \ "postopInstructionsMessage").validateOpt[String]
      recallReminderMessage <- (request.body \ "recallReminderMessage").validateOpt[String]

    } yield {

      (name, country, city, state, zip, address, phone, xvWebURL, xvWebUserName, xvWebPassword, PracticeRow(None, "", "", "", "", "", "", "", DateTime.now, Some(""), Some(""), Some(""), shortId, timeZone, phoneExt, phone1, phoneExt1, fax, email, taxId, insBillingProvider, billingLicense, openingDate, officeGroup, ucrFeeSchedule, feeSchedule, officeId, doseSpotClinicId, doseSpotClinicKey,chartNumberType,chartNumberSeq,suite,appointmentReminder,appReminderSetting,onlineRegReminderFrequency,onlineRegMaxReminder,onlineRegistrationModule,onlineInviteMessage,appointmentReminderMessage,consentFormMessage,amenities,photos,latitude,longitude,clinicDescription,trojanAccountNo,postopInstructionsMessage,recallReminderMessage))
    }

    dentist match {
      case JsSuccess((name, country, city, state, zip, address, phone, xvWebURL, xvWebUserName, xvWebPassword, practiceRow), _) =>
      var stateNotFound = false
      var phoneValidation = false
      var zipCodeValidation = false
      var countries = "us,united states,usa"

    if(convertOptionString(country) != "" && convertOptionString(country) != None){
      if(countries contains convertOptionString(country).toLowerCase ){

      var block1 = db.run {
          StateTaxTable.filter(s => s.state.toLowerCase === convertOptionString(state).toLowerCase || s.stateName.toLowerCase === convertOptionString(state).toLowerCase).length.result
          } map {  result =>
          if(result == 0 ){
              stateNotFound = true
          }
        }
        var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          var phoneNumber = convertOptionString(phone).replaceAll("[^a-zA-Z0-9]", "")
          if(phoneNumber.length != 10 || phoneNumber.startsWith("0") || phoneNumber.startsWith("1") || phoneNumber.startsWith("123")){
              phoneValidation = true
          }

          if(!(convertOptionString(zip).forall(_.isDigit) && (convertOptionString(zip).length == 5 || convertOptionString(zip).length == 9))){
             zipCodeValidation = true
          }
      }
    }

      if(stateNotFound){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid state name!"}""")))
      } else if(phoneValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid phone number!"}""")))
      } else if(zipCodeValidation){
            Future.successful(BadRequest(Json.parse("""{"error":"Please enter valid zipcode!"}""")))
      } else {
        db.run {
          val query = PracticeTable.filter(_.id === id)

          query.map(_.name).result.headOption.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case Some(practiceName) =>
              val duplicate = if (name.fold(false)(_ != practiceName)) {
                for {
                  exists <- PracticeTable.filter(_.name === name.get).exists.result
                } yield {
                  exists
                }
              } else {
                DBIO.successful(false)
              }

              duplicate.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
                case true =>
                  DBIO.successful(PreconditionFailed(Json.obj("error" -> "Practice already exists")))
                case false =>
                  DBIO.seq[Effect.Write](
                    name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    country.map(value => query.map(_.country).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    city.map(value => query.map(_.city).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    state.map(value => query.map(_.state).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    zip.map(value => query.map(_.zip).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    address.map(value => query.map(_.address).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    phone.map(value => query.map(_.phone).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    xvWebURL.map(value => query.map(_.xvWebURL).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    xvWebUserName.map(value => query.map(_.xvWebUserName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    xvWebPassword.map(value => query.map(_.xvWebPassword).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.shortId.map(value => query.map(_.shortId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.timeZone.map(value => query.map(_.timeZone).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.phoneExt.map(value => query.map(_.phoneExt).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.phone1.map(value => query.map(_.phone1).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.phoneExt1.map(value => query.map(_.phoneExt1).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.fax.map(value => query.map(_.fax).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.email.map(value => query.map(_.email).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.taxId.map(value => query.map(_.taxId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.insBillingProvider.map(value => query.map(_.insBillingProvider).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.billingLicense.map(value => query.map(_.billingLicense).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.openingDate.map(value => query.map(_.openingDate).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.officeGroup.map(value => query.map(_.officeGroup).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.ucrFeeSchedule.map(value => query.map(_.ucrFeeSchedule).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.feeSchedule.map(value => query.map(_.feeSchedule).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.officeId.map(value => query.map(_.officeId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.doseSpotClinicId.map(value => query.map(_.doseSpotClinicId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.doseSpotClinicKey.map(value => query.map(_.doseSpotClinicKey).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.chartNumberType.map(value => query.map(_.chartNumberType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.chartNumberSeq.map(value => query.map(_.chartNumberSeq).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.suite.map(value => query.map(_.suite).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.appointmentReminder.map(value => query.map(_.appointmentReminder).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.onlineRegReminderFrequency.map(value => query.map(_.onlineRegReminderFrequency).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.onlineRegMaxReminder.map(value => query.map(_.onlineRegMaxReminder).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.onlineRegistrationModule.map(value => query.map(_.onlineRegistrationModule).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.onlineInviteMessage.map(value => query.map(_.onlineInviteMessage).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.appointmentReminderMessage.map(value => query.map(_.appointmentReminderMessage).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.consentFormMessage.map(value => query.map(_.consentFormMessage).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.amenities.map(value => query.map(_.amenities).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.photos.map(value => query.map(_.photos).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.latitude.map(value => query.map(_.latitude).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.longitude.map(value => query.map(_.longitude).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.clinicDescription.map(value => query.map(_.clinicDescription).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.trojanAccountNo.map(value => query.map(_.trojanAccountNo).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.postopInstructionsMessage.map(value => query.map(_.postopInstructionsMessage).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                    practiceRow.recallReminderMessage.map(value => query.map(_.recallReminderMessage).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                  ) map { _ =>

                var pAppReminderSetting = ""
                if(convertOptionalBoolean(practiceRow.appointmentReminder) == true && convertOptionString(practiceRow.appReminderSetting) != ""){
                      pAppReminderSetting = convertOptionString(practiceRow.appReminderSetting)
                  } else if(convertOptionalBoolean(practiceRow.appointmentReminder) == true && convertOptionString(practiceRow.appReminderSetting) == "") {
                    pAppReminderSetting = "One Day Prior"
                  } else if(convertOptionalBoolean(practiceRow.appointmentReminder) == false){
                      pAppReminderSetting = ""
                  }

                  db.run{
                      PracticeTable.filter(_.id === id).map(_.appReminderSetting).update(Some(pAppReminderSetting))
                  }
                     //--Dosespot Api Called---
                    val accessToken:String = getAccessToken(doseSpotUrl,doseSpotClinicId,doseSpotClinicKey,doseSpotAdminId)

                    var clinicId = convertOptionString(practiceRow.doseSpotClinicId)
                    var pName = convertOptionString(name)
                    var pAddress = convertOptionString(address)
                    var pCity = convertOptionString(city)
                    var pState = convertOptionString(state)
                    var zipcode = convertOptionString(zip)
                    var primaryPhone = convertOptionString(phone)

                    val json: String ="""{"ClinicName": "rClinicName","Address1": "rAddress1","City": "rCity","State": "rState","ZipCode":"rZipCode","PrimaryPhone":"rPrimaryPhone","PrimaryPhoneType":"7"}""".stripMargin

                    var practiceJson = json.replace("rClinicName", pName).replace("rAddress1", pAddress).replace("rCity", pCity).replace("rState", pState).replace("rZipCode", zipcode).replace("rPrimaryPhone", primaryPhone)

                    val url = s"${doseSpotUrl}webapi/api/clinics/${clinicId}"
                    val result = Http(url)
                    .postData(practiceJson)
                    .header("Content-Type", "application/json")
                    .headers(Seq("Authorization" -> ("Bearer " + accessToken), "Accept" -> "application/json"))
                    .header("Charset", "UTF-8")
                    .option(HttpOptions.readTimeout(10000)).asString
                    Ok(Json.parse("""{"Success":"Practice updated successfully."}"""))
                  }
              }
            case None =>
              DBIO.successful(NotFound)
          }
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      StaffTable.filterNot(_.archived).filter(_.practiceId === id).exists.result.flatMap {
        case true => DBIO.successful(BadRequest("Could not delete as there are still accounts exists under this practice!"))
        case false => PracticeTable.filter(_.id === id).map(_.archived).update(true) map {
          case 0 => NotFound
          case _ => Ok
        }
      }
    }
  }

case class DoseSpotSEncryptedCode(encryptedClinicId:String,encryptedUserId:String)

def generateDoseSpotEncryptedCodes(clinicCode:String,userId:String):DoseSpotSEncryptedCode = {
    var randomClinicPhrase = Random.alphanumeric take 32 mkString
    var randomPhraseClinicKey = randomClinicPhrase + clinicCode


    var sha512ClinicString = MessageDigest.getInstance("SHA-512").digest(randomPhraseClinicKey.getBytes("UTF-8"))
    var base64ClinicString = Base64.getEncoder.encodeToString(sha512ClinicString)
    var encryptedClinicId = base64ClinicString
    if (base64ClinicString.takeRight(2)=="=="){
       encryptedClinicId = base64ClinicString.dropRight(2)
    }
    encryptedClinicId = randomClinicPhrase + encryptedClinicId

    var randomUserPhrase = randomClinicPhrase.take(22)
    var randomUserKey = userId + randomUserPhrase
    var appendClinicKey = randomUserKey + clinicCode

    var sha512UserString = MessageDigest.getInstance("SHA-512").digest(appendClinicKey.getBytes("UTF-8"))
    var base64UserString = Base64.getEncoder.encodeToString(sha512UserString)
    var encryptedUserId = base64UserString
    if (base64UserString.takeRight(2)=="=="){
       encryptedUserId = base64UserString.dropRight(2)
    }

    val DoseSpotSEncryptedCodeObj = new DoseSpotSEncryptedCode(encryptedClinicId, encryptedUserId);
    return DoseSpotSEncryptedCodeObj;
}

def getAccessToken(doseSpotUrl: String,clinicId:String,clinicKey:String,userId:String):String = {
  val DoseSpotSEncryptedCodeObj:DoseSpotSEncryptedCode=generateDoseSpotEncryptedCodes(clinicKey,userId)
  val url = s"${doseSpotUrl}webapi/token"
  val encryptedClinicId = DoseSpotSEncryptedCodeObj.encryptedClinicId
  val encryptedUserId = DoseSpotSEncryptedCodeObj.encryptedUserId

  val result = Http(url)
  .header("Content-Type", "application/x-www-form-urlencoded")
  .header("Charset", "UTF-8")
  .auth(clinicId,encryptedClinicId)
  .postForm(Seq(
    "grant_type" -> "password",
    "Username" -> s"$userId",
    "Password" -> s"$encryptedUserId"))
    .option(HttpOptions.readTimeout(10000)).asString

  val jsonObject: JsValue = Json.parse(result.body)
  var accessToken:String=""

  if (result.code==200){
    accessToken = (jsonObject \ "access_token").as[String]
  }
  return accessToken;
}
}