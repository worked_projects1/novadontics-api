package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables.CategoryRow
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

@Singleton
class CategoriesController @Inject()(silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val categoryFormat = Json.format[CategoryRow]

  object Input {
    trait Category
    case class PlainCategory(id: Long, order: Option[Int], taxExempted: Boolean) extends Category
    case class NewCategory(name: String, taxExempted: Boolean, children: Option[List[Category]]) extends Category
    case class UpdatedCategory(id: Long, order: Option[Int], name: Option[String], taxExempted: Boolean, children: Option[List[Category]]) extends Category

    object Category {
      lazy val reads = Reads

      implicit object Reads extends Reads[Category] {
        override def reads(json: JsValue) = try {
          for {
              idOpt <- (json \ "id").validateOpt[Long]
              orderOpt <- (json \ "order").validateOpt[Int]
              nameOpt <- (json \ "name").validateOpt[String]
              taxOpt <- (json \ "taxExempted").validate[Boolean]
              childrenOpt <- (json \ "children").validateOpt[List[Category]]
            } yield {
              (idOpt, orderOpt, nameOpt, taxOpt, childrenOpt) match {
                case (Some(id), order, name, taxExempted, children) if name.isDefined =>
                  UpdatedCategory(id, order, name, taxExempted, children)
                case (Some(id), order, _, taxExempted, _) =>
                  PlainCategory(id, order, taxExempted)
                case (_, _, Some(name), taxExempted, children) =>
                  NewCategory(name, taxExempted, children)
              }
            }
          } catch {
            case _: MatchError => JsError()
          }
        }
    }
  }

  object Output {
    case class Category(id: Long, name: String, taxExempted: Boolean, order: Int, children: Option[List[Category]], prodCount: Int)

    object Category {
      implicit val writes: Writes[Category] = Writes { category =>
        Json.obj(
          "id" -> category.id,
          "name" -> category.name,
          "taxExempted" -> category.taxExempted,
          "order" -> category.order,
          "productCount" -> category.prodCount
        ) ++ (category.children match {
          case Some(children) =>
            Json.obj(
              "children" -> JsArray(children.map(c => writes.writes(c)))
            )
          case None =>
            Json.obj()
        })
      }
    }
  }

  def readAll = silhouette.SecuredAction.async { request =>
      val action = (for {
        (category, p) <- CategoryTable joinLeft ProductTable.filter(_.deleted === false) on(_.id === _.categoryId)
      } yield (category, p.map(_.id))).groupBy({
        case (category, _) => category
      }).map({
        case ((category), products) => {
          (category, products.map(_._2).countDistinct)
        }
      }).sortBy(c => c._1.order.desc.nullsFirst)

      db.run{ action.result } map {categories =>
        val childrenOf = categories.foldLeft(Map.empty[Long, List[(CategoryRow, Int)]].withDefaultValue(Nil)) { (acc, category) =>
          val id = category._1.parent.getOrElse(0L)
          acc.updated(id, (category._1, category._2) :: acc(id))
        }

        def buildTrees(rows: List[(CategoryRow, Int)]): List[Output.Category] = {
          rows.map { (row: (CategoryRow, Int)) =>
            val id: Long = row._1.id.get
            val children: scala.List[CategoriesController.this.Output.Category] = buildTrees(childrenOf(id))
            Output.Category.apply(id, row._1.name, convertOptionalBoolean(row._1.taxExempted), row._1.order, if (children.isEmpty) scala.None else Some.apply(children), row._2)
          }
        }

        Ok(Json.toJson(buildTrees(childrenOf(0))))
      }
  }

  def update(id: Long) = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>

      val categories = for {
        name <- (request.body \ "name").validateOpt[String]
        taxExempted <- (request.body \ "taxExempted").validateOpt[Boolean]
      } yield {
        (name, taxExempted)
      }
      categories match {
          case JsSuccess((name, taxExempted), _) => db.run {
            val query = CategoryTable.filter(_.id === id)
            query.exists.result.flatMap[Result, NoStream, Effect.Write] {
              case true => DBIO.seq[Effect.Write](

                name.map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                taxExempted.map(value => query.map(_.taxExempted).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

              ) map {_ => Ok}
              case false => DBIO.successful(NotFound)
            }
          }
          case JsError(_) => Future.successful(BadRequest)
      }
  }

  def bulkUpdate = silhouette.SecuredAction(UsersOnly).async(parse.json) { request =>
    request.body.validate[List[Input.Category]] match {
      case JsSuccess(categories, _) => {

        def parse(categories: List[Input.Category], parent: Option[Long]): List[DBIOAction[Unit, NoStream, Effect.All]] = {
          for (cat <- categories) yield {
            cat match {
              case cat: Input.PlainCategory => for {
                _ <- CategoryTable.filter(_.id === cat.id).map(a => (a.order, a.taxExempted))
                  .update((cat.order.get, Some(cat.taxExempted)))
              } yield ()

              case cat: Input.UpdatedCategory => for {
                _ <- CategoryTable.filter(_.id === cat.id).map(a => (a.order, a.parent, a.name, a.taxExempted))
                  .update((cat.order.getOrElse(categories.indexOf(cat)), parent, cat.name.get, Some(cat.taxExempted)))
                _ <- cat.children.nonEmpty && cat.children.get.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Option(cat.id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()

              case cat: Input.NewCategory => for {
                cat_id <- CategoryTable returning CategoryTable.map(_.id) += CategoryRow(None, cat.name, Some(cat.taxExempted), parent, 0)
                _ <- cat.children.nonEmpty match {
                  case true => parse(cat.children.getOrElse(List()), Some(cat_id)).reduceLeft(_ andThen _)
                  case false => DBIO.successful(Unit)
                }
              } yield ()
            }
          }
        }

        val actions = parse(categories, None: Option[Long])
        if (actions.nonEmpty) {
          db.run(actions.reduceLeft(_ andThen _).asTry.transactionally) map (s => s match {
            case Success(()) => Ok
            case _ => BadRequest
          })
        }
        else {
          Future.successful(BadRequest)
        }
      }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(UsersOnly).async {
    db.run {
      val query = ProductTable.filter(a => a.categoryId === id && a.deleted === false)
      query.exists.result.flatMap[Result, NoStream, Effect.Write] {
        case true => DBIO.successful(BadRequest("There are still products in this category"))
        case false => CategoryTable.filter(_.id === id).delete map {
          case 0 => NotFound
          case _ => Ok
        }
      }
    }
  }
}
