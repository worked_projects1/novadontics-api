package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.google.inject.name.Named
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables._
import play.api.Application
import play.api.libs.json._
import scala.concurrent.{Await, Future}
import akka.stream.scaladsl.StreamConverters
import play.api.mvc.{ResponseHeader, Result}
import scala.concurrent.ExecutionContext
import java.io.{ByteArrayOutputStream, PipedInputStream, PipedOutputStream,File}
import play.api.http.HttpEntity
import java.io.{ByteArrayInputStream,ByteArrayOutputStream, PipedInputStream, PipedOutputStream}

import javax.imageio.ImageIO
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.FileNotFoundException;
import com.itextpdf.text.Document
import java.util.NoSuchElementException

import java.net.{URL}
import com.itextpdf.text.DocumentException
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.tool.xml.{XMLWorker,XMLWorkerHelper,NoCustomContextException,Tag, WorkerContext, Pipeline}
import com.itextpdf.text.log.Level;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.tool.xml.exceptions.LocaleMessages;
import com.itextpdf.tool.xml.exceptions.RuntimeWorkerException;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.text.{BaseColor, Document, Element, Font, FontFactory, Phrase, Rectangle, Paragraph,Image,Chunk,PageSize}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.CssFilesImpl;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import java.nio.charset.Charset;
import com.itextpdf.tool.xml.parser.XMLParser;
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import scala.collection.Map
import scala.concurrent.duration._
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.ZonedDateTime;
import java.time.ZoneId;



object PatientsController {
  object Output {

    case class HealthHisotryObj(
                  currentDate:String,
                  full_name: String,
                  preferredName: String,
                  address: String,
                  city: String,
                  homePhone: String,
                  phone: String,
                  email: String,
                  gender: String,
                  birth_date: String,
                  marital_status: String,
                  socialSecurity: String,
                  driverLicense: String,
                  employer: String,
                  occupation: String,
                  businessAddress: String,
                  businessPhone: String,
                  nameOfSpouse: String,
                  spouseOccupaton: String,
                  spousePhone: String,
                  personToContact: String,
                  emergencyContact: String,
                  referredBy: String,
                  responsibleParty: String,
                  patientRelationship: String,
                  differentAddress: String,
                  phoneNumber: String,
                  medicalInsurance: String,
                  physicianName: String,
                  medicalInsurancePhone: String,
                  lastExam: String,
                  medicalTreatment: String,
                  hospitalizedLastFiveYrs: String,
                  medicationList: String,                  
                  takenRedux: String,
                  controlSubstances: String,
                  tobaccoProducts: String,
                  pregnant: String,
                  numberOfWeeks: String,
                  nursing: String,
                  birthControlPills: String,
                  allergicTo: String,
                  medicalConditionList: String,
                  previousDentist: String,
                  lastExamXray: String,
                  changingDentists: String,
                  gumsBleed: String,
                  sensitiveTeethSweetSour: String,
                  sensitiveTeethHotCold: String,
                  head_neck_jaw_injuries: String,
                  feelAnythingUnusual: String,
                  orthodonticTreatment: String,
                  wearDentures: String,
                  placementDate: String,
                  careTeeth: String,
                  likeSmile: String,
                  clenchTeeth : String,
                  problemsInJaw : String,
                  medicalHealth: String,
                  speakDoctorPrivate: String,
                  date1: String,
                  signature1: String,
                  date2: String,
                  signature2: String,
                  date3: String,
                  signature3: String,
                  patientDate: String,
                  patientSignature: String,
                  patientSignature1: String,
                  patientDate1: String,
                  name2: String,
                  minor: String,
                  patientSignature2: String,
                  patientDate2: String
                )

	case class ConsentFormObj(
		  patientName: String,
		  gender: String,
		  dateOfBirth: String,
		  requestingCTScan: String,
		  address: String,
		  cell: String,
		  phone: String,
		  emailAddress: String,
		  work: String,
		  Name: String,
		  relationship: String,
		  emergencyPhone: String,
		  patientBefore: String,
		  diagnosticScan: String,
		  diagnosticList: String,
		  where: String,
		  when: String,
		  pacemaker: String,
		  latexAllergies: String,
		  pregnant: String,
		  treatmentName: String,
		  doctorName: String,
		  treatmentName2: String,
		  treatmentAmount: String,
		  patientDate: String,
		  patientSignature: String
		  )	
  }
}


class ImageTagProcessor extends com.itextpdf.tool.xml.html.Image {

  private val logger: Logger = LoggerFactory.getLogger(getClass)
  

  /*
   * (non-Javadoc)
   *
   * @see com.itextpdf.tool.xml.TagProcessor#endElement(com.itextpdf.tool.xml.Tag, java.util.List, com.itextpdf.text.Document)
   */
  import java.util.List;
  import java.util.ArrayList;
  import java.util.Map;
  override def end(ctx: WorkerContext,
                   tag: Tag,
                   currentContent: List[Element]): List[Element] = {

    val attributes: Map[String, String] = tag.getAttributes
    val src: String = attributes.get(HTML.Attribute.SRC)
    var elements: List[Element] = new ArrayList[Element](1)
    if (null != src && src.length > 0) {
      var img: Image = null
      if (src.startsWith("data:image/")) {

        val base64Data: String = src.substring(src.indexOf(",") + 1)
        try img = Image.getInstance(Base64.decode(base64Data))
        catch {
          case e: Exception =>
            /*if (logger.isLogging(Level.ERROR)) {
              logger.error(
                String.format(LocaleMessages.getInstance.getMessage(
                                LocaleMessages.HTML_IMG_RETRIEVE_FAIL),
                              src),
                e)
            }*/

        }
        if (img != null) {
          val htmlPipelineContext: HtmlPipelineContext =
            getHtmlPipelineContext(ctx)
          elements.add(
            getCssAppliers.apply(
              new Chunk(getCssAppliers
                          .apply(img, tag, htmlPipelineContext)
                          .asInstanceOf[com.itextpdf.text.Image],
                        0,
                        0,
                        true),
              tag,
              htmlPipelineContext))
        } 
      }

      if (img == null) {
        elements = super.end(ctx, tag, currentContent)
        
      }
    }
    elements
  }

}

/**
  * Created by stefan on 2/6/17.
  */

@Singleton
//class PatientsController @Inject()(silhouette: Silhouette[BearerTokenEnv],@Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {
class PatientsController @Inject()(silhouette: Silhouette[BearerTokenEnv], @Named("ioBound") ioBoundExecutor: ExecutionContext, val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  case class Step(
    value: JsValue,
    state: StepRow.State,
    formId: String,
    formVersion: Int,
    dateCreated: DateTime
  )
  
   case class shareData(
    shared: Boolean,
    sharedBy: String,
    sharedDate: Option[LocalDate],
    mainSquares: String,
    icons: String,
    accessTo: String,
    accessLevel: String
  )

  case class BirthDayListRow(
    id: Option[Long],
    chartId: String,
    firstName: Option[String],
    lastName: Option[String],
    fullName: String,
    dateOfBirth: Option[String],
    phoneNumber: String,
    email: String,
    birthday: String
  )

  implicit val BirthDayListFormat = Json.format[BirthDayListRow]


 case class PatientDetailsRow(
   full_name: Option[String],
   last_name: Option[String],
   email: String,
   phone: String,
   country: Option[String]
 )

 implicit val PatientDetailsFormat = Json.format[PatientDetailsRow]

 implicit val medicalLogRowWrites = Writes { medicalLogListRecord: (MedicalConditionLogRow) =>
    val (medicalLogListRow) = medicalLogListRecord

  Json.obj(
      "value" -> medicalLogListRow.value,
      "date" -> medicalLogListRow.dateCreated,
      "currentlySelected" -> medicalLogListRow.currentlySelected
      )
  }

case class TreatmentPlanProcedureRow(
    id : Long,
    procedureCode : String,
    procedureDescription : String,
    status: String,
    tooth : Long,
    fee : Double
  )


    implicit val patientProcedureRowWrites = Writes { patientProcedureRecord: (TreatmentPlanProcedureRow) =>
      val (patientProcedureRow) = patientProcedureRecord

    Json.obj(
        "id" -> patientProcedureRow.id,
        "procedureCode" -> patientProcedureRow.procedureCode,
        "procedureDescription" -> patientProcedureRow.procedureDescription,
        "status" -> patientProcedureRow.status,
        "tooth" -> patientProcedureRow.tooth,
        "fee" -> patientProcedureRow.fee
      )
    }

case class PatientListRow(
  id: Option[Long],
  patientId: String,
  name: Option[String],
  firstName: Option[String],
  lastName: Option[String],
  birthDate: Option[String],
  patientStatus: Option[String],
  family: Option[Boolean],
  firstVisit: Option[String],
  providerId: Option[Long],
  providerName: Option[String],
  phoneNumber: String,
  email: String,
  recallDueDate: Option[String],
  sharedByYou: Option[String],
  shared: Boolean,
  sharedBy: Option[String],
  sharedDate: Option[String],
  referredBy: Option[String],
  medicalCondition: Option[Boolean],
  alerts: Option[String],
  insurance: String,
  patientType: Option[String],
  examImaging: Option[String],
  onlineRegistration: Option[String],
  onlineRegistrationStatus: Option[String],
  lastInvitationSent: Option[DateTime]
)

 implicit val patientListWrites = Writes { request: (PatientListRow) =>
    val (patientRow) = request
    Json.obj(
      "id" -> patientRow.id,
      "patientId" -> patientRow.patientId,
      "name" -> patientRow.name,
      "firstName" -> patientRow.firstName,
      "lastName" -> patientRow.lastName,
      "birthDate" -> patientRow.birthDate,
      "patientStatus" -> patientRow.patientStatus,
      "family" -> patientRow.family,
      "firstVisit" -> patientRow.firstVisit,
      "providerId" -> patientRow.providerId,
      "providerName" -> patientRow.providerName,
      "phoneNumber" -> patientRow.phoneNumber,
      "email" -> patientRow.email,
      "recallDueDate" -> patientRow.recallDueDate,
      "sharedByYou" -> patientRow.sharedByYou,
      "shared" -> patientRow.shared,
      "sharedBy" -> patientRow.sharedBy,
      "sharedDate" -> patientRow.sharedDate,
      "referredBy" -> patientRow.referredBy,
      "medicalCondition" -> patientRow.medicalCondition,
      "alerts" -> patientRow.alerts,
      "insurance" -> patientRow.insurance,
      "patientType" -> patientRow.patientType,
      "examImaging" -> patientRow.examImaging,
      "onlineRegistration" -> patientRow.onlineRegistration,
      "onlineRegistrationStatus" -> patientRow.onlineRegistrationStatus,
      "lastInvitationSent" -> patientRow.lastInvitationSent
      )
  }

  object Step {
    implicit val writes = Json.writes[Step]
  }

  implicit val stepStateFormat = new Format[StepRow.State] {
    import StepRow.State._

    override def writes(state: StepRow.State): JsValue = state match {
      case ToDo => Json.toJson("ToDo")
      case InProgress => Json.toJson("InProgress")
      case Completed => Json.toJson("Completed")
      case Sealed => Json.toJson("Sealed")
    }

    override def reads(json: JsValue): JsResult[StepRow.State] = json.validate[String].map {
      case "InProgress" => InProgress
      case "Completed" => Completed
      case "Sealed" => Sealed
      case _ => ToDo
    }
  }
  
  private def getSelectedList(listMap:scala.collection.mutable.Map[String,String],stepValue:JsValue):String = {
      var selectedList = ""
      var tempVar = ""
      listMap.foreach {
        case (listMapKey,listMapValue) => tempVar = convertOptionString((stepValue \ listMapKey).asOpt[String])
                                          if (tempVar contains "true"){
                                              if (listMapValue=="Other"){
                                                 tempVar = tempVar.replace('★', '"')
                                                 val otherObject: JsValue = Json.parse(tempVar)
                                                 val otherValue = convertOptionString((otherObject \ "notes").asOpt[String])
                                                 selectedList=selectedList+","+otherValue
                                              }else{  
                                                selectedList=selectedList+","+listMapValue
                                              }
                                          }
      }
      selectedList = selectedList.stripPrefix(",")
      
      return selectedList
  }  
  
    
  private def getCheckBoxValue(checkBoxName:String,stepValue:JsValue):Boolean = { 
          var ctValue = convertOptionString((stepValue \ checkBoxName).asOpt[String])
          var ctCheckValue = ""
          if (ctValue != ""){
            val checkBoxFieldValue = ctValue.replace('★', '"')
            val checkBoxFieldObject: JsValue = Json.parse(checkBoxFieldValue)
            val checkBoxValue = (checkBoxFieldObject \ "checked").asOpt[Boolean]

            if (checkBoxValue==Some(true))
              return true;
            else
              return false;  
          }else
            return false;
  }


  
 // used by 'time' method
  
  case class ShareDetailsRow(practice : PracticeRow, dentist : StaffRow, treatment : TreatmentRow, steps : Option[List[Step]],includeBPFlag : Boolean,sharedDetails :shareData)

  implicit val patientRowsWrites = Writes {patient: ShareDetailsRow =>
    val patientDetails = patient

    var bp_pulse_o2 = "normal"
    var shareIcon = false
    var staffNotesIcon = "off"

    var currentDate = LocalDate.now()
    var startOfLastWeek = currentDate.minusWeeks(1)


    var last_name=patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "last_name").asOpt[String]))
    
    if (last_name==None){
      last_name=Some("")
    }  
    
    var birth_date=patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "birth_date").asOpt[String]))
    
    if (birth_date==None){
      birth_date=Some("")
    }
    
    var gender=patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "gender").asOpt[String]))
    
    if (gender==None){
      gender=Some("")
    }
    
    var patient_status=patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "patient_status").asOpt[String]))
    
    if (patient_status==None){
      patient_status=Some("")
    }
    if (patientDetails.includeBPFlag){
      val f = db.run { 
         for {
            measurementHistoryCount <- MeasurementHistoryTable.filter(_.patientId === patientDetails.treatment.id).length.result
         }yield {

            if (measurementHistoryCount > 0){
                var f1=db.run { 
                     for {
                          measurementHistory <- MeasurementHistoryTable.filter(_.patientId === patientDetails.treatment.id).sortBy(_.dateCreated.desc).result.head
                     } yield {
                           val systolic = convertOptionDouble(measurementHistory.systolic)
                           val diastolic = convertOptionDouble(measurementHistory.diastolic)
                           val pulse = convertOptionDouble(measurementHistory.pulse)
                           val oxygen = convertOptionDouble(measurementHistory.oxygen)
                           val bodyTemp = convertOptionDouble(measurementHistory.bodyTemp)
                           val glucose = convertOptionDouble(measurementHistory.glucose)
                           val a1c = convertOptionDouble(measurementHistory.a1c)
                           val inr = convertOptionDouble(measurementHistory.inr)
                           val pt = convertOptionDouble(measurementHistory.pt)
                           
                          if (systolic > 129 || systolic < 90 || diastolic > 79 || diastolic < 60 ||  pulse>85 || pulse <44 || oxygen < 95 || bodyTemp > 98.6 || glucose > 195 || a1c>7 || inr <2 || inr >4 || pt < 10 || pt > 14){
                                bp_pulse_o2 = "out of range"
                           }
                     }
                }
                var AwaitResult1 = Await.ready(f1, atMost = scala.concurrent.duration.Duration(3, SECONDS))       
            }
          }
      }
      
      val AwaitResult = Await.ready(f, atMost = scala.concurrent.duration.Duration(3, SECONDS))
    }
    var toothChartDate=patientDetails.treatment.toothChartUpdatedDate
    if (toothChartDate==None){
      toothChartDate = Some(DateTime.now)
    }

    var medicalConditionList: JsValue = Json.parse("{}")
    var allergyList: JsValue = Json.parse("{}")

//Medical Condition
    var medicalCondition = db.run{
          MedicalConditionLogTable.filter(_.patientId === patientDetails.treatment.id).filter(s => s.conditionType === "Medical Condition" || s.conditionType === "Other_Medical_Condition")
          .sortBy(_.dateCreated.desc).result
    } map { results=>
      medicalConditionList = Json.toJson(results)
      Ok
    }

//Staff Notes
    var staffNotes = db.run{
          StaffNotesTable.filter(_.patientId === patientDetails.treatment.id)
          .result
    } map { results=>
      results.foreach(row=>{
        if(startOfLastWeek <= row.date && currentDate >= row.date){
          staffNotesIcon = "on"
        }
      })
      
      Ok
    }
  
//Allergy
    var allergy = db.run{
          MedicalConditionLogTable.filter(_.patientId=== patientDetails.treatment.id).filter(s => s.conditionType === "Allergy" || s.conditionType === "Other_Allergy")
          .sortBy(_.dateCreated.desc).result
    } map { results=>
      allergyList = Json.toJson(results)
      Ok
    }

    val AwaitResult1 = Await.ready(medicalCondition, atMost = scala.concurrent.duration.Duration(3, SECONDS))
    val AwaitResult2 = Await.ready(allergy, atMost = scala.concurrent.duration.Duration(3, SECONDS))

    var sharePatientBlock = db.run{
        SharedPatientTable.filter(_.patientId === convertOptionLong(patientDetails.treatment.id)).length.result
    } map { shareResult =>
    if(shareResult > 0 || convertOptionalBoolean(patientDetails.treatment.shareToAdmin)){
      shareIcon = true }
    Ok
    }
    val AwaitResult3 = Await.ready(sharePatientBlock, atMost = scala.concurrent.duration.Duration(3, SECONDS))

    var isFamily = false
    var familyBlock = db.run{
        FamilyMembersTable.filter(_.patientId === convertOptionLong(patientDetails.treatment.id)).length.result
    } map { familyResult =>
    if(familyResult > 0){
      isFamily = true
    }
    }
    val AwaitResult4 = Await.ready(familyBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    var providerName = ""
    var providerId = convertOptionString(patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "provider").asOpt[String])))
    if(providerId != ""){
      var block1 = db.run{
            ProviderTable.filter(_.id === providerId.toLong).result
        } map { results =>
        results.map{ providers => providerName = providers.title +" "+ providers.firstName +" "+providers.lastName }
        Ok
        }
        val AwaitResult4 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(3, SECONDS))
    }
var dentistName = patientDetails.dentist.title + " " +patientDetails.dentist.name + " " + convertOptionString(patientDetails.dentist.lastName)

    Json.obj(
      "id" -> patientDetails.treatment.id,
      "practiceId" -> patientDetails.dentist.practiceId,
      "dentist" -> Json.obj(
        "id" -> patientDetails.dentist.id,
        "name" -> dentistName,
        "email" -> patientDetails.dentist.email,
        "practice" -> patientDetails.practice.name,
        "country" -> patientDetails.practice.country,
        "city" -> patientDetails.practice.city,
        "state" -> patientDetails.practice.state,
        "zip" -> patientDetails.practice.zip,
        "address" -> patientDetails.practice.address,
        "phone" -> patientDetails.practice.phone
      ),
      "patient" -> Json.obj(
        "patientId" -> patientDetails.treatment.patientId,
        "name" ->  patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "full_name").asOpt[String])),
        "last_name" -> last_name,
        "providerName" -> providerName,
        "birthDate" -> birth_date,
        "gender" -> gender,
        "patient_status" -> patient_status,
        "patient_type" -> patientDetails.treatment.patientType,
        "referredBy" -> patientDetails.treatment.referralSource,
        "phone" -> patientDetails.treatment.phoneNumber,
        "lastEntry" -> patientDetails.treatment.lastEntry,
        "email" -> patientDetails.treatment.emailAddress,
        "firstVisit" -> patientDetails.treatment.firstVisit,
        "ctScanFile" -> patientDetails.treatment.ctScanFile,
        "contracts" -> patientDetails.treatment.contracts,
        "xray" -> patientDetails.treatment.xray,
        "scratchPad" -> patientDetails.treatment.scratchPad,
        "scratchPadText" -> patientDetails.treatment.scratchPadText,
        "toothChartUpdatedDate" -> toothChartDate,
        "bp_pulse_o2" -> bp_pulse_o2,
        "recallPeriod" -> patientDetails.treatment.recallPeriod,
        "recallType" -> patientDetails.treatment.recallType,
        "recallCode" -> patientDetails.treatment.recallCode,
        "recallCodeDescription" -> patientDetails.treatment.recallCodeDescription,
        "intervalNumber" -> patientDetails.treatment.intervalNumber,
        "intervalUnit" -> patientDetails.treatment.intervalUnit,
        "recallFromDate" -> patientDetails.treatment.recallFromDate,
        "recallOneDayPlus" -> patientDetails.treatment.recallOneDayPlus,
        "recallDueDate" -> patientDetails.treatment.recallDueDate,
        "recallNote" -> patientDetails.treatment.recallNote,
        "examImaging" -> patientDetails.treatment.examImaging,
        "lastInvitationSent" -> patientDetails.treatment.lastInvitationSent,
        "family" -> isFamily,
        "currenActiveMedication" ->   patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "pharmacy_information1").asOpt[String])),
        "preferredPharmacyInformation" ->  patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "pharmacy_information2").asOpt[String])),
        "country" ->   patientDetails.steps.flatMap(_.find(_.formId == "1A").flatMap(step => (step.value \ "country").asOpt[String])),
        "mainSquares" -> patientDetails.sharedDetails.mainSquares,
        "icons" -> patientDetails.sharedDetails.icons,
        "shared" -> patientDetails.sharedDetails.shared,
        "sharedBy" -> patientDetails.sharedDetails.sharedBy,
        "sharedDate" -> patientDetails.sharedDetails.sharedDate,
        "shareIcon" -> shareIcon,
        "staffNotesIcon" -> staffNotesIcon,
        "accessTo" -> patientDetails.sharedDetails.accessTo,
        "accessLevel" -> patientDetails.sharedDetails.accessLevel
      ),
      "steps" -> (Json.obj() ++ patientDetails.steps.map(s => Json.obj("steps" -> s)).getOrElse(Json.obj())),
      "medicalCondition" -> medicalConditionList,
      "allergy" -> allergyList
    )
  }

  def readAll = silhouette.SecuredAction.async { request =>
  var mergeList = List.empty[ShareDetailsRow]

   var userRole = ""
    val query = request.identity match {
      case UserRow(id, _, _, _, _, _, _) =>
        UserTable.filter(_.id === id).map(_.role)
      case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
        StaffTable.filter(_.id === id).map(_.role)
    }
    var userRoleData = db.run {  query.result } map { result => userRole = result.head.toString }
    var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

if(userRole == "Admin"){

  var action = for {

      practice <- PracticeTable
      dentist <- StaffTable if dentist.practiceId === practice.id
      treatment <- TreatmentTable.filterNot(_.deleted).filter(_.shareToAdmin === true) if treatment.staffId === dentist.id
      step <- StepTable if step.treatmentId === treatment.id && step.formId === "1A"
    } yield (practice, dentist, treatment, step)

    db.run {
      action.result } map { data =>
        val patients = data.groupBy(_._3.id).map { row =>
          val (_, group) = row
          val (practice, dentist, treatment, step) = group.head

          val steps = group
            .map(_._4)
            .groupBy(_.formId)
            .map(_._2.maxBy(_.dateCreated))
            .toList
            .sortBy(_.formId)
            .map(step => Step(step.value, step.state, step.formId, step.formVersion, step.dateCreated))

          var sharedBy = ""
          var block = db.run{
                StaffTable.filter(_.id === treatment.sharedBy).result
          } map {staffResult =>
          staffResult.map{result =>
            sharedBy = result.name + " " + convertOptionString(result.lastName)
          }
            Ok
          }
          val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var shareDate : Option[LocalDate] = None
          if(treatment.sharedDate != None){
            shareDate = treatment.sharedDate
          }

          var accessTo = ""
          var accessLevel = ""
          var block1 = db.run{
                SharedPatientTable.filter(_.patientId === treatment.id).result
          } map {treatmentResult =>
          treatmentResult.map{result =>
            accessTo = result.accessTo
            accessLevel = result.accessLevel
          }
          }
          val AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          val shareObj = new shareData(true,sharedBy,shareDate,convertOptionString(treatment.mainSquares),convertOptionString(treatment.icons),accessTo,accessLevel)
          ShareDetailsRow (practice, dentist, treatment, Some(steps),true,shareObj)

        }.toList

      Ok(Json.toJson(patients))
    }

} else {

  var action = for {

      practice <- PracticeTable
      dentist <- StaffTable if dentist.practiceId === practice.id
      treatment <- TreatmentTable.filterNot(_.deleted) if treatment.staffId === dentist.id
      step <- StepTable if step.treatmentId === treatment.id && step.formId === "1A"
    } yield (practice, dentist, treatment, step)

    action = request.identity match {
      case staff: StaffRow => action.filter(_._1.id === staff.practiceId)
      case UserRow(id, _, _, _, _, _, _) => action
    }

    db.run {
      action.result } map { data =>
        val patients = data.groupBy(_._3.id).map { row =>
          val (_, group) = row
          val (practice, dentist, treatment, step) = group.head

          val steps = group
            .map(_._4)
            .groupBy(_.formId)
            .map(_._2.maxBy(_.dateCreated))
            .toList
            .sortBy(_.formId)
            .map(step => Step(step.value, step.state, step.formId, step.formVersion, step.dateCreated))

          val shareObj = new shareData(false,"",None,"","","","")
          val dataList = ShareDetailsRow (practice, dentist, treatment, Some(steps),true,shareObj)
          mergeList = mergeList :+ dataList

          }.toList

var action1 = for {

      sharePatient <- SharedPatientTable.filter(_.shareTo === request.identity.id.get)
      practice <- PracticeTable if practice.id === sharePatient.practiceId
      dentist <- StaffTable if dentist.id === sharePatient.sharedBy
      treatment <- TreatmentTable.filterNot(_.deleted) if treatment.id === sharePatient.patientId
      step <- StepTable if step.treatmentId === treatment.id && step.formId === "1A"
    } yield (sharePatient,practice, dentist, treatment, step)

    var block = db.run {
      action1.result } map { data =>
        val patients = data.groupBy(_._4.id).map { row =>
          val (_, group) = row
          val (sharePatient,practice, dentist, treatment, step) = group.head

          val steps = group
            .map(_._5)
            .groupBy(_.formId)
            .map(_._2.maxBy(_.dateCreated))
            .toList
            .sortBy(_.formId)
            .map(step => Step(step.value, step.state, step.formId, step.formVersion, step.dateCreated))

          var sharedBy = ""
          var block1 = db.run{
                StaffTable.filter(_.id === sharePatient.sharedBy).result
          } map { staffResult => staffResult.map{result =>
            sharedBy = result.name + " " + convertOptionString(result.lastName)
          }
            Ok
          }
          val AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          val shareObj = new shareData(true,sharedBy,Some(sharePatient.sharedDate),sharePatient.mainSquares,sharePatient.icons,sharePatient.accessTo,sharePatient.accessLevel)
          val dataList = ShareDetailsRow(practice, dentist, treatment, Some(steps),true,shareObj)
          mergeList = mergeList :+ dataList
        }.toList
        Ok
    }
val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))
       // }.toList.sortBy(_._3.id)
      Ok(Json.toJson(mergeList.sortBy(_.treatment.id)))
    }

}
}

  def read(id: Long) = silhouette.SecuredAction.async { request =>
    
   var userRole = ""
    val query = request.identity match {
      case UserRow(id, _, _, _, _, _, _) =>
        UserTable.filter(_.id === id).map(_.role)
      case StaffRow(id, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
        StaffTable.filter(_.id === id).map(_.role)
    }
    var userRoleData = db.run {  query.result } map { result => userRole = result.head.toString }
    var AwaitResult3 = Await.ready(userRoleData, atMost = scala.concurrent.duration.Duration(10, SECONDS))

    if(userRole == "Admin"){
    var action = for {
      practice <- PracticeTable
      dentist <- StaffTable if dentist.practiceId === practice.id
      treatment <- TreatmentTable.filterNot(_.deleted) if treatment.staffId === dentist.id && treatment.id === id

      step <- StepTable if step.treatmentId === treatment.id
    } yield (practice, dentist, treatment, step)

    db.run {
      action.result
    } map {
      case data if data.nonEmpty =>
        val patients = data.groupBy(_._1).map { row =>
          val (_, group) = row
          val (practice, dentist, treatment, step) = group.head

          val steps = group
            .map(_._4)
            .groupBy(_.formId)
            .map(_._2.maxBy(_.dateCreated))
            .toList
            .sortBy(_.formId)
            .map(step => Step(step.value, step.state, step.formId, step.formVersion, step.dateCreated)
            )
          var shareDate : Option[LocalDate] = None
          if(treatment.sharedDate != None){
            shareDate = treatment.sharedDate
          }
          var sharedBy = ""
          var block = db.run{
                StaffTable.filter(_.id === treatment.sharedBy).result
          } map {staffResult =>
          staffResult.map{result =>
            sharedBy = result.name + " " + convertOptionString(result.lastName)
          }
          }
          val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          var accessTo = ""
          var accessLevel = ""
          var block1 = db.run{
                SharedPatientTable.filter(_.patientId === treatment.id).result
          } map {treatmentResult =>
          treatmentResult.map{result =>
            accessTo = result.accessTo
            accessLevel = result.accessLevel
          }
          }
          val AwaitResult1 = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))

          val shareObj = new shareData(true,sharedBy,shareDate,convertOptionString(treatment.mainSquares),convertOptionString(treatment.icons),accessTo,accessLevel)
         ShareDetailsRow(practice, dentist, treatment, Some(steps),true,shareObj)
        }.toList
        Ok(Json.toJson(patients.head))
      case _ =>
        NotFound
    }

    } else {

    var action = for {
      practice <- PracticeTable
      dentist <- StaffTable if dentist.practiceId === practice.id
      treatment <- TreatmentTable.filterNot(_.deleted) if treatment.staffId === dentist.id && treatment.id === id
      // sharePatient <- SharedPatientTable.filter(_.shareTo === request.identity.id.get) if sharePatient.patientId === treatment.id
      step <- StepTable if step.treatmentId === treatment.id
    } yield (practice, dentist, treatment, step)

    db.run {
      action.result
    } map {
      case data if data.nonEmpty =>
        val patients = data.groupBy(_._1).map { row =>
          val (_, group) = row
          val (practice, dentist, treatment, step) = group.head
          
          val steps = group
            .map(_._4)
            .groupBy(_.formId)
            .map(_._2.maxBy(_.dateCreated))
            .toList
            .sortBy(_.formId)
            .map(step => Step(step.value, step.state, step.formId, step.formVersion, step.dateCreated)
            )

           var vShareDate : Option[LocalDate] = None
           var vShare = false
           var vMainSquare = ""
           var vIcons = ""
           var vAccessTo = ""
           var vAccessLevel = ""
           var vSharedBy = ""
           var vStaffName = dentist.name +" "+ convertOptionString(dentist.lastName)

        val block = db.run{
              SharedPatientTable.filter(_.shareTo === request.identity.id.get).filter(_.patientId === id).result
            } map { shareMap =>
              if(shareMap.length > 0){
                shareMap.map{row =>
                vShareDate = Some(row.sharedDate)
                vShare = true
                vMainSquare = row.mainSquares
                vIcons = row.icons
                vAccessTo = row.accessTo
                vAccessLevel = row.accessLevel

           var block1 = db.run{
                StaffTable.filter(_.id === row.sharedBy).result
          } map { staffResult =>
          staffResult.map{result => vSharedBy = result.name + " " + convertOptionString(result.lastName)
          }
          Ok
          }
          var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(10, SECONDS))

                }
              }
          Ok
          }
        var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(10, SECONDS))
          val shareObj = new shareData(vShare,vSharedBy,vShareDate,vMainSquare,vIcons,vAccessTo,vAccessLevel)
         ShareDetailsRow(practice, dentist, treatment, Some(steps),true,shareObj)
        }.toList
        Ok(Json.toJson(patients.head))
      case _ =>
        NotFound
    }
    }

  }
  
  
   //def generateHealthHistoryForm(id:Long)=silhouette.UserAwareAction.async { request =>
   def generateHealthHistoryForm(id: Long) = silhouette.SecuredAction.async { implicit request =>   
    
    db.run {
      for {
        steps <- StepTable.filter(_.treatmentId === id).filter(_.formId==="1L").sortBy(_.dateCreated).result
      } yield {
        
        var healthHistoryFields:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map("full_name"->"","preferredName"->"","address"->"","city"->"","homePhone"->"","phone"->"","email"->"","gender"->"","birth_date"->"","marital_status"->"","socialSecurity"->"","driverLicense"->"","employer"->"","occupation"->"","businessAddress"->"","businessPhone"->"","nameOfSpouse"->"","spouseOccupaton"->"","spousePhone"->"","personToContact"->"","emergencyContact"->"","referredBy"->"","responsibleParty"->"","patientRelationship"->"","differentAddress"->"","phoneNumber"->"","medicalInsurance"->"","physicianName"->"","medicalInsurancePhone"->"","lastExam"->"","medicalTreatment"->"","hospitalizedLastFiveYrs"->"","medicationList"->"","takenRedux"->"","controlSubstances"->"","tobaccoProducts"->"","pregnant"->"","numberOfWeeks"->"","nursing"->"","birthControlPills"->"","allergicTo"->"","medicalConditionList"->"","previousDentist"->"","lastExamXray"->"","changingDentists"->"","gumsBleed"->"","sensitiveTeethSweetSour"->"","sensitiveTeethHotCold"->"","head_neck_jaw_injuries"->"","feelAnythingUnusual"->"","orthodonticTreatment"->"","wearDentures"->"","placementDate"->"","careTeeth"->"","likeSmile"->"","clenchTeeth"->"","problemsInJaw"->"","medicalHealth"->"","speakDoctorPrivate"->"","date1"->"","signature1"->"","date2"->"","signature2"->"","date3"->"","signature3"->"","patientDate"->"","patientSignature"->"","patientSignature1"->"","patientDate1"->"","name2"->"","minor"->"","patientSignature2"->"","patientDate2"->"")
        
        
        
        var currentDate=DateTime.now
        val dateTimeFormat = DateTimeFormat.mediumDate()
        var stepAvailable=false
        
        var allergyMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map("allergy1"->"Penicillin","allergy2"->"Latex","allergy3"->"Codeine","allergy4"->"Anesthetics","allergy5"->"Sulfa Drugs","allergy6"->"Aspirin","allergy7"->"Other")
        
        var medicalConditionMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map("medicalCondition1"->"Alcohol/Drug Dependency","medicalCondition2"->"Allergies","medicalCondition3"->"Anemia","medicalCondition4"->"Angina","medicalCondition5"->"Arthritis","medicalCondition6"->"Artificial joints or implants","medicalCondition7"->"Asthma","medicalCondition8"->"Cancer","medicalCondition9"->"Diabetes","medicalCondition10"->"Epilepsy","medicalCondition11"->"Artificial Heart Valve","medicalCondition12"->"Congenital Heart Problems","medicalCondition13"->"Congestive Heart Disease","medicalCondition14"->"Heart Attack","medicalCondition15"->"Heart Disease","medicalCondition16"->"Heart Murmur","medicalCondition17"->"Heart Pacemaker","medicalCondition18"->"Heart Surgery","medicalCondition19"->"Mitral Valve Prolapse","medicalCondition20"->"Stroke","medicalCondition21"->"High Blood Pressure","medicalCondition22"->"Low Blood Pressure","medicalCondition23"->"Glaucoma","medicalCondition24"->"HIV Infection (AIDS)","medicalCondition25"->"Hay Fever","medicalCondition26"->"Frequent Headaches","medicalCondition27"->"Ulcers","medicalCondition28"->"Lung Disease Tuberculosis","medicalCondition29"->"Thyroid Problems","medicalCondition30"->"Hepatitis A","medicalCondition31"->"Hepatitis B","medicalCondition32"->"Hepatitis C","medicalCondition33"->"Kidney Disease","medicalCondition34"->"Leukemia","medicalCondition35"->"Liver Disease","medicalCondition36"->"Seizures /Fainting","medicalCondition37"->"Respiratory Problems","medicalCondition38"->"Rheumatic Fever","medicalCondition39"->"Sinus Problems")
        
        var problemsInJawMap:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map("clickingPain"->"Clicking Pain","difficultyOpening"->"Difficulty in opening","difficultyClosing"->"Difficulty in closing","difficultyChewing"->"Difficulty in chewing")
         
        var currentDateStr = s"${dateTimeFormat.print(currentDate)}"
        var i=1
        var allergyList = ""
        var medicalConditionList = ""
        var problemsInJaw = ""
        
        (steps).foreach(step => {
          
          healthHistoryFields.foreach {
            case (key,v) => healthHistoryFields.update(key, convertOptionString((step.value \ key).asOpt[String]))
          }
          allergyList = getSelectedList(allergyMap,step.value)
          medicalConditionList = getSelectedList(medicalConditionMap,step.value)
          problemsInJaw = getSelectedList(problemsInJawMap,step.value)

        })
        
        healthHistoryFields.update("allergicTo",allergyList)
        healthHistoryFields.update("medicalConditionList",medicalConditionList)
        healthHistoryFields.update("problemsInJaw",problemsInJaw)
        
        
        val source = StreamConverters.fromInputStream { () =>
        
        val data1: PatientsController.Output.HealthHisotryObj = PatientsController.Output.HealthHisotryObj(currentDateStr,healthHistoryFields("full_name"),healthHistoryFields("preferredName"),healthHistoryFields("address"),healthHistoryFields("city"),healthHistoryFields("homePhone"),healthHistoryFields("phone"),healthHistoryFields("email"),healthHistoryFields("gender"),healthHistoryFields("birth_date"),healthHistoryFields("marital_status"),healthHistoryFields("socialSecurity"),healthHistoryFields("driverLicense"),healthHistoryFields("employer"),healthHistoryFields("occupation"),healthHistoryFields("businessAddress"),healthHistoryFields("businessPhone"),healthHistoryFields("nameOfSpouse"),healthHistoryFields("spouseOccupaton"),healthHistoryFields("spousePhone"),healthHistoryFields("personToContact"),healthHistoryFields("emergencyContact"),healthHistoryFields("referredBy"),healthHistoryFields("responsibleParty"),healthHistoryFields("patientRelationship"),healthHistoryFields("differentAddress"),healthHistoryFields("phoneNumber"),healthHistoryFields("medicalInsurance"),healthHistoryFields("physicianName"),healthHistoryFields("medicalInsurancePhone"),healthHistoryFields("lastExam"),healthHistoryFields("medicalTreatment"),healthHistoryFields("hospitalizedLastFiveYrs"),healthHistoryFields("medicationList"),healthHistoryFields("takenRedux"),healthHistoryFields("controlSubstances"),healthHistoryFields("tobaccoProducts"),healthHistoryFields("pregnant"),healthHistoryFields("numberOfWeeks"),healthHistoryFields("nursing"),healthHistoryFields("birthControlPills"),healthHistoryFields("allergicTo"),healthHistoryFields("medicalConditionList"),healthHistoryFields("previousDentist"),healthHistoryFields("lastExamXray"),healthHistoryFields("changingDentists"),healthHistoryFields("gumsBleed"),healthHistoryFields("sensitiveTeethSweetSour"),healthHistoryFields("sensitiveTeethHotCold"),healthHistoryFields("head_neck_jaw_injuries"),healthHistoryFields("feelAnythingUnusual"),healthHistoryFields("orthodonticTreatment"),healthHistoryFields("wearDentures"),healthHistoryFields("placementDate"),healthHistoryFields("careTeeth"),healthHistoryFields("likeSmile"),healthHistoryFields("clenchTeeth"),healthHistoryFields("problemsInJaw"),healthHistoryFields("medicalHealth"),healthHistoryFields("speakDoctorPrivate"),healthHistoryFields("date1"),healthHistoryFields("signature1"),healthHistoryFields("date2"),healthHistoryFields("signature2"),healthHistoryFields("date3"),healthHistoryFields("signature3"),healthHistoryFields("patientDate"),healthHistoryFields("patientSignature"),healthHistoryFields("patientSignature1"),healthHistoryFields("patientDate1"),healthHistoryFields("name2"),healthHistoryFields("minor"),healthHistoryFields("patientSignature2"),healthHistoryFields("patientDate2"))

        //val data1: PatientsController.Output.HealthHisotryObj = PatientsController.Output.HealthHisotryObj(currentDateStr,healthHistoryFields("patientName"),healthHistoryFields("preferredName"),healthHistoryFields("address"),healthHistoryFields("city"))
        
        val document = new Document(PageSize.A2,30, 30, 15 , 15)
        //document.setMargins(15, 15, 15 , 15)
        val FONT_COVER_PAGE = FontFactory.getFont(FontFactory.HELVETICA, 29)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        
        
        Future({
          
          val tagProcessorFactory:TagProcessorFactory = Tags.getHtmlTagProcessorFactory();
          
          tagProcessorFactory.removeProcessor("img");
          tagProcessorFactory.addProcessor(new ImageTagProcessor(), "img");

          
          
          val cssFiles:CssFilesImpl  = new CssFilesImpl();
          cssFiles.add(XMLWorkerHelper.getInstance().getDefaultCSS());
          
          
          val cssResolver:StyleAttrCSSResolver  = new StyleAttrCSSResolver(cssFiles);
          
          
          /*
           * below code is to add css
           * val  CSS:String = "td { font-size: 10pt; border-color: gray; border-bottom: 3px}";
          val  cssFile:CssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSS.getBytes()));
          cssResolver.addCss(cssFile);*/
          
          val hpc:HtmlPipelineContext  = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
          //val hpc:HtmlPipelineContext  = new HtmlPipelineContext();
          hpc.setAcceptUnknown(true).autoBookmark(true).setTagFactory(tagProcessorFactory);
          val htmlPipeline:HtmlPipeline  = new HtmlPipeline(hpc, new PdfWriterPipeline(document, writer));
          val pipeline  = new CssResolverPipeline(cssResolver, htmlPipeline);
              
          document.open()
          
          val worker:XMLWorker  = new XMLWorker(pipeline, true)
          val charset:Charset  = Charset.forName("UTF-8")
          val xmlParser:XMLParser  = new XMLParser(true, worker, charset);
          //final InputStream is = new FileInputStream("C:\\test.html");
          val is  = new ByteArrayInputStream(views.html.healthHistoryForm(data1).toString().getBytes("UTF-8"));
          
          xmlParser.parse(is, charset);
        
          
          
          
          document.addTitle(s"Health History Form")
          document.close()
          is.close();
        })(ioBoundExecutor)
        

        inputStream
      }

      val filename = s"test"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
   }
 }  
  
  
}


def generateConsentForm(id: Long) = silhouette.SecuredAction.async { implicit request =>   
    
    db.run {
      
      for {
        steps <- StepTable.filter(_.treatmentId === id).filter(_.formId==="1M").sortBy(_.dateCreated).result
      } yield {
        var consentFormFields:scala.collection.mutable.Map[String,String]=scala.collection.mutable.Map("patientName"->"","gender"->"","dateOfBirth"->"","requestingCTScan"->"","address"->"","cell"->"","phone"->"","emailAddress"->"","work"->"","Name"->"","relationship"->"","emergencyPhone"->"","patientBefore"->"","diagnosticScan"->"","diagnosticList"->"","where"->"","when"->"","pacemaker"->"","latexAllergies"->"","pregnant"->"","treatmentName"->"","doctorName"->"","treatmentName2"->"","treatmentAmount"->"","patientDate"->"","patientSignature"->"")
      var diagnosticList = ""
	    (steps).foreach(step => {
          diagnosticList = ""
          consentFormFields.foreach {
            case (key,v) => consentFormFields.update(key, convertOptionString((step.value \ key).asOpt[String]))
          }
          
          if (getCheckBoxValue("CT",step.value)){
            diagnosticList = diagnosticList + ", CT";
          }  
          if (getCheckBoxValue("MRI",step.value)){
            diagnosticList = diagnosticList + ", MRI";
          }
          if (getCheckBoxValue("CT",step.value)){
            diagnosticList = diagnosticList + ", CT";
          }

          
        })
        diagnosticList = diagnosticList.stripPrefix(", ")
        consentFormFields.update("diagnosticList",diagnosticList)

    val source = StreamConverters.fromInputStream { () =>

	 val data1: PatientsController.Output.ConsentFormObj = PatientsController.Output.ConsentFormObj(consentFormFields("patientName"),consentFormFields("gender"),consentFormFields("dateOfBirth"),consentFormFields("requestingCTScan"),consentFormFields("address"),consentFormFields("cell"),consentFormFields("phone"),consentFormFields("emailAddress"),consentFormFields("work"),consentFormFields("Name"),consentFormFields("relationship"),consentFormFields("emergencyPhone"),consentFormFields("patientBefore"),consentFormFields("diagnosticScan"),consentFormFields("diagnosticList"),consentFormFields("where"),consentFormFields("when"),consentFormFields("pacemaker"),consentFormFields("latexAllergies"),consentFormFields("pregnant"),consentFormFields("treatmentName"),consentFormFields("doctorName"),consentFormFields("treatmentName2"),consentFormFields("treatmentAmount"),consentFormFields("patientDate"),consentFormFields("patientSignature"))
   
   


	  val document = new Document(PageSize.A2,30, 30, 15 , 15)
        //document.setMargins(15, 15, 15 , 15)
        val FONT_COVER_PAGE = FontFactory.getFont(FontFactory.HELVETICA, 29)
        val inputStream = new PipedInputStream()
        val outputStream = new PipedOutputStream(inputStream)
        val writer = PdfWriter.getInstance(document, outputStream)
        
        
        Future({
          
          val tagProcessorFactory:TagProcessorFactory = Tags.getHtmlTagProcessorFactory();
          
          tagProcessorFactory.removeProcessor("img");
          tagProcessorFactory.addProcessor(new ImageTagProcessor(), "img");

          
          
          val cssFiles:CssFilesImpl  = new CssFilesImpl();
          cssFiles.add(XMLWorkerHelper.getInstance().getDefaultCSS());
          
          
          val cssResolver:StyleAttrCSSResolver  = new StyleAttrCSSResolver(cssFiles);
          
          
          /*
           * below code is to add css
           * val  CSS:String = "td { font-size: 10pt; border-color: gray; border-bottom: 3px}";
          val  cssFile:CssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSS.getBytes()));
          cssResolver.addCss(cssFile);*/
          
          val hpc:HtmlPipelineContext  = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
          //val hpc:HtmlPipelineContext  = new HtmlPipelineContext();
          hpc.setAcceptUnknown(true).autoBookmark(true).setTagFactory(tagProcessorFactory);
          val htmlPipeline:HtmlPipeline  = new HtmlPipeline(hpc, new PdfWriterPipeline(document, writer));
          val pipeline  = new CssResolverPipeline(cssResolver, htmlPipeline);
              
          document.open()
          
          val worker:XMLWorker  = new XMLWorker(pipeline, true)
          val charset:Charset  = Charset.forName("UTF-8")
          val xmlParser:XMLParser  = new XMLParser(true, worker, charset);
          //final InputStream is = new FileInputStream("C:\\test.html");
          val is  = new ByteArrayInputStream(views.html.consentForm(data1).toString().getBytes("UTF-8"));
          
          xmlParser.parse(is, charset);
        
          
          
          
          document.addTitle(s"Consent Form")
          document.close()
          is.close();
        })(ioBoundExecutor)
        

        inputStream
      }
       
      val filename = s"test"
      val contentDisposition = "inline; filename=\"" + filename + ".pdf\""

      Result(
        header = ResponseHeader(200, Map.empty),
        body = HttpEntity.Streamed(source, None, Some("application/pdf"))
      ).withHeaders(
        "Content-Disposition" -> contentDisposition
      )
      }
    }
}



def readPatients(shared: Option[Boolean]) = silhouette.SecuredAction.async { request =>
val practiceId = request.identity.asInstanceOf[StaffRow].practiceId
var mergeList = List.empty[PatientListRow]
var pageNumber:Int = 0
var recPerPage:Int = 0
var idLimit:Long = 180

  var pageNumberVal: Option[String] = request.headers.get("pageNumber")
  if ( pageNumberVal != None){
    pageNumber = convertOptionString(pageNumberVal).toInt
  }

  if (pageNumber == 0 ){
    pageNumber = 1
  }

  var recPerPageVal: Option[String] = request.headers.get("recPerPage")
  if( recPerPageVal != None){
    recPerPage = convertOptionString(recPerPageVal).toInt
  }

   var block = db.run {
      TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId).joinLeft(ProviderTable.filterNot(_.deleted)).on(_.providerId === _.id).joinLeft(SharedPatientTable).on(_._1.id === _.patientId).result
    } map { treatments =>
      val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val providerEntry = entries.head._1._2
          val sharedByYouEntry = entries.head._2

          var providerName = ""
          var providerId = Option.empty[Long]
          if (providerEntry != None){
             var provider = entries.head._1._2.get
             providerId = provider.id
             providerName = provider.title +" "+ provider.firstName +" "+provider.lastName
          }
          var sharedByYou = Some("No")
          if(sharedByYouEntry != None){
            sharedByYou = Some("Yes")
          }
          var yearMonthDay: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
          var yearMonthDayTime: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
          var outputFormat: SimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
          
          var firstVisit = ""
          if(entry.firstVisit != Some("") && entry.firstVisit != null && entry.firstVisit != None){
          var date: Date = yearMonthDayTime.parse(entry.firstVisit.get.toString);
          firstVisit = outputFormat.format(date);
          }

          var recallDueDate = ""
          if(entry.recallDueDate != Some("") && entry.recallDueDate != null && entry.recallDueDate != None){
          recallDueDate = outputFormat.format(yearMonthDay.parse(entry.recallDueDate.get.toString));
          }

          var name = convertOptionString(entry.firstName)+ " " + convertOptionString(entry.lastName)
          var vPhoneNumber = entry.phoneNumber

          if(vPhoneNumber.startsWith("+1")){
            vPhoneNumber = vPhoneNumber.drop(2)
          }
          var phoneNumber = vPhoneNumber.replaceAll("[\\D]", "")

          var dataList = PatientListRow(
            entry.id,
            entry.patientId,
            Some(name),
            entry.firstName,
            entry.lastName,
            entry.dateOfBirth,
            entry.patientStatus,
            entry.isFamily,
            Some(firstVisit),
            providerId,
            Some(providerName),
            phoneNumber,
            entry.emailAddress,
            Some(recallDueDate),
            sharedByYou,
            false,
            Some(""),
            Some(""),
            entry.referralSource,
            entry.isMedicalCondition,
            entry.alerts,
            entry.insurance,
            entry.patientType,
            entry.examImaging,
            entry.onlineRegistration,
            entry.onlineRegistrationStatus,
            entry.lastInvitationSent
          )
          mergeList = mergeList :+ dataList
          }.toList

    //Second query
    if(convertOptionalBoolean(shared)){
      var shareBlock = db.run {
      TreatmentTable.filterNot(_.deleted).join(SharedPatientTable).on(_.id === _.patientId).filter(_._2.shareTo === request.identity.id.get).joinLeft(ProviderTable).on(_._1.providerId === _.id).result
      } map { treatments =>
      val data = treatments.groupBy(_._1._1.id).map {
        case (treatmentId, entries) =>
          val entry = entries.head._1._1
          val sharedPatient = entries.head._1._2
          var providerEntry = entries.head._2

          var providerName = ""
          var providerId = Option.empty[Long]
          if (providerEntry != None){
             var provider = entries.head._2.get
             providerId = provider.id
             providerName = provider.title +" "+ provider.firstName +" "+provider.lastName
          }

          var sharedBy = ""
          var staffBlock = db.run{
                StaffTable.filter(_.id === sharedPatient.sharedBy).result
          } map { staffResult => staffResult.map{result =>
            sharedBy = result.name + " " + convertOptionString(result.lastName)
            }
          }
          val AwaitResult = Await.ready(staffBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

          var yearMonthDay: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
          var yearMonthDayTime: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
          var outputFormat: SimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");

          var firstVisit = ""
          if(entry.firstVisit != Some("") && entry.firstVisit != null && entry.firstVisit != None){
          var date: Date = yearMonthDayTime.parse(entry.firstVisit.get.toString);
          firstVisit = outputFormat.format(date);
          }

          var recallDueDate = ""
          if(entry.recallDueDate != Some("") && entry.recallDueDate != null && entry.recallDueDate != None){
          recallDueDate = outputFormat.format(yearMonthDay.parse(entry.recallDueDate.get.toString));
          }

          var sharedDate = ""
          if(sharedPatient.sharedDate != Some("") && sharedPatient.sharedDate != null && sharedPatient.sharedDate != None){
          sharedDate = outputFormat.format(yearMonthDay.parse(sharedPatient.sharedDate.toString));
          }

          var name = convertOptionString(entry.firstName)+ " " + convertOptionString(entry.lastName)
          var vPhoneNumber = entry.phoneNumber
          if(vPhoneNumber.startsWith("+1")){
            vPhoneNumber = vPhoneNumber.drop(2)
          }
          var phoneNumber = vPhoneNumber.replaceAll("[\\D]", "")

          var dataList = PatientListRow(
            entry.id,
            entry.patientId,
            Some(name),
            entry.firstName,
            entry.lastName,
            entry.dateOfBirth,
            entry.patientStatus,
            entry.isFamily,
            Some(firstVisit),
            providerId,
            Some(providerName),
            phoneNumber,
            entry.emailAddress,
            Some(recallDueDate),
            Some("No"),
            true,
            Some(sharedBy),
            Some(sharedDate),
            entry.referralSource,
            entry.isMedicalCondition,
            entry.alerts,
            entry.insurance,
            entry.patientType,
            entry.examImaging,
            entry.onlineRegistration,
            entry.onlineRegistrationStatus,
            entry.lastInvitationSent           
          )
          mergeList = mergeList :+ dataList
          }.toList
    }
    val AwaitResult = Await.ready(shareBlock, atMost = scala.concurrent.duration.Duration(180, SECONDS))
    }
    // Ok(Json.toJson(mergeList))
      //   if (recPerPage == 0){
      //   val pageData = data
      //   Ok(Json.toJson(pageData)).withHeaders(("totalCount" -> data.length.toString()))
      //   }
      //   else{
      //   val pageData = data.drop((pageNumber-1)*recPerPage).take(recPerPage)
      //   Ok(Json.toJson(pageData)).withHeaders(("totalCount" -> data.length.toString()))
      // }
  }
  val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(180, SECONDS))
  Future(Ok(Json.toJson(mergeList)))
}

def migratePatient(startId: Long, endId: Long) = silhouette.SecuredAction(SupersOnly).async(parse.json) { request =>
  var b = db.run{
    TreatmentTable.filter(s => s.id >= startId && s.id <= endId).result
  } map { treatmentMap =>
    treatmentMap.foreach(treatmentList => {

      var staffBlock = db.run{
        StaffTable.filter(_.id === treatmentList.staffId).result
      } map { staffMap =>
        staffMap.foreach(staffList => {

            var practiceBlock = db.run{
              PracticeTable.filter(_.id === staffList.practiceId).result
            } map { practiceMap =>
            practiceMap.foreach(practiceList => {

               var stepBlock = db.run{
                StepTable.filter(_.treatmentId === treatmentList.id).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
              } map { stepMap =>
                var providerId = Option.empty[Long]
                var firstName = (stepMap.value \ "full_name").asOpt[String]
                var lastName = (stepMap.value \ "last_name").asOpt[String]
                var dateOfBirth = (stepMap.value \ "birth_date").asOpt[String]

                var strProviderId = convertOptionString((stepMap.value \ "provider").asOpt[String])
                if(strProviderId != "" && strProviderId.forall(_.isDigit)){
                  providerId = Some(strProviderId.toLong)
                }
                var practiceId = practiceList.id

          var updateBlock = db.run {
          val query = TreatmentTable.filter(_.id === treatmentList.id)
          query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](
            firstName.map(value => query.map(_.firstName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            lastName.map(value => query.map(_.lastName).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            dateOfBirth.map(value => query.map(_.dateOfBirth).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            practiceId.map(value => query.map(_.practiceId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            providerId.map(value => query.map(_.providerId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
            ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      val AwaitResult = Await.ready(updateBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
              }
              val AwaitResult = Await.ready(stepBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
            })
          }
          val AwaitResult = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        })
      }
      val AwaitResult = Await.ready(staffBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    })

  }

    val AwaitResult = Await.ready(b, atMost = scala.concurrent.duration.Duration(120, SECONDS))
     Future(Ok{Json.obj("status"->"Success","message"->"Patients migrated successfully")})
  }


def birthdayList(zoneOf: Option[String]) = silhouette.SecuredAction.async { request =>
  var birthdayDetails = List.empty[BirthDayListRow]
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var currentDateTime = ZonedDateTime.now()
  var inputZone = convertOptionString(zoneOf)

   if(inputZone != ""){
     try{
      var zoneId: ZoneId = ZoneId.of(inputZone)
      currentDateTime = ZonedDateTime.now(zoneId)
     } catch {
       case e: Exception => // currentDateTime is server time.
     }
   }
  var tomorrowDate = currentDateTime.plusDays(1)
  var currentMonth = currentDateTime.getMonthValue()
  var currentYear = currentDateTime.getYear()
  
    db.run {
          TreatmentTable.filterNot(_.deleted).filter(_.practiceId === practiceId).result
      } map { treatmentMap =>
          treatmentMap.foreach(treatmentList => {
            var patientName = convertOptionString(treatmentList.firstName) +" "+ convertOptionString(treatmentList.lastName)
            var dob = convertOptionString(treatmentList.dateOfBirth)
            if(dob != null && dob != ""){
            var s = DateTimeFormat.forPattern("MM/dd/yyyy").parseDateTime(dob)
            var tempDate = s"${currentYear}-${s.getMonthOfYear}-${s.getDayOfMonth}"
            var birthDay = getCustomDateString(dob)
            
            var patientDob = DateTime.parse(tempDate)
            var pTomorrowDate = DateTime.parse(tomorrowDate.toLocalDate().toString)

            if(patientDob >= pTomorrowDate && patientDob <= pTomorrowDate.plusDays(6)){
              var dataList = BirthDayListRow(treatmentList.id,treatmentList.patientId,treatmentList.firstName,treatmentList.lastName,patientName,treatmentList.dateOfBirth,treatmentList.phoneNumber,treatmentList.emailAddress, birthDay)
              birthdayDetails = birthdayDetails :+ dataList
            }
          }
        })
        Ok(Json.toJson(birthdayDetails.sortBy(_.dateOfBirth)))
      }
  }

// --- 1st 2nd 3rd 4th 
def getCustomDateString(inputDate: String):String = {
  var format: SimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
  var outputFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
  var strDate: Date = format.parse(inputDate);
  
  var date: String = outputFormat.format(strDate);

  if(date.endsWith("1") && !date.endsWith("11"))
    format = new SimpleDateFormat("d'st' MMMM");
  else if(date.endsWith("2") && !date.endsWith("12"))
    format = new SimpleDateFormat("d'nd' MMMM");
  else if(date.endsWith("3") && !date.endsWith("13"))
    format = new SimpleDateFormat("d'rd' MMMM");
  else
    format = new SimpleDateFormat("d'th' MMMM");

  var dateFormat = format.format(strDate);
  return dateFormat;
}

def getUcrInsuranceId :Long = {
  var ucrId: Long = 0
  var block = db.run{
      InsuranceCompanyTable.filterNot(_.deleted).filter(_.name === "UCR").result.head
        } map { insCompanyMap =>
          ucrId = convertOptionLong(insCompanyMap.id)
       }
      Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  return ucrId;
}

def getInsuranceFee(patientId: Option[Long], insuranceCompanyId: Option[Long], procedureCode: Option[String]) = silhouette.SecuredAction.async { request =>
  var practiceId = request.identity.asInstanceOf[StaffRow].practiceId
  var vInsuranceCompanyId:Long= -1
  var vProcedureCode = convertOptionString(procedureCode)
  var vFeeSchedule: Float = 0
  var vCompanyId = "-1"
  var carrierName = ""
  // GETTING INSURANCE COMPANY ID
  if(insuranceCompanyId == None){
    if(patientId != None && patientId != Some(0)){
      var insIdBlock = db.run{
        StepTable.filter(_.treatmentId === patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
      } map { steps =>
        carrierName = convertOptionString((steps.value \ "carrierName").asOpt[String])
        if (carrierName!=""){
            vInsuranceCompanyId = carrierName.toLong
        }
        
       }
        Await.ready(insIdBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      }
  }else{
    vInsuranceCompanyId = convertOptionLong(insuranceCompanyId)
  }
    

   
     
        
// GETTING FEE AND PROCEDURE CODE VALUE
  if(vProcedureCode != None && vProcedureCode != ""){
    var feeBlock =  db.run{
      InsuranceFeeTable.filter(_.practiceId === practiceId).filter(_.insuranceId === vInsuranceCompanyId).result
    } map { insuranceList =>

      insuranceList.foreach(result => {
        if(vProcedureCode contains result.procedureCode){
          vFeeSchedule = vFeeSchedule + result.fee
        }
      })
    }
    Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  } else {
    var feeBlock = db.run{
      TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(s=> s.status === "Accepted" || s.status === "Proposed").sortBy(_.id).result
    } map { planDetailsList =>
    var cdcCode = ""
        planDetailsList.foreach(result => {
            vFeeSchedule = vFeeSchedule + result.fee
            cdcCode = cdcCode + result.cdcCode + ","
        })
        vProcedureCode = cdcCode.dropRight(1)

    }
    Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  }
    val newObj = Json.obj() + ("insuranceCompanyId" -> JsString(vInsuranceCompanyId.toString))  + ("procedureCode" -> JsString(vProcedureCode)) + ("fee" -> JsNumber(Math.round(vFeeSchedule * 100.0) / 100.0))
    Future(Ok(Json.toJson(newObj)))
  }

def getTreatmentPlanProcedures(patientId: Long,forWhich: String)= silhouette.SecuredAction.async { request =>
var dataList = List.empty[TreatmentPlanProcedureRow]
if(forWhich == "appointment"){
var feeBlock = db.run{
  TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(s=> s.status === "Accepted" || s.status === "Proposed").sortBy(_.id).joinLeft(AppointmentTreatmentPlanTable).on(_.id === _.treatmentPlanId).join(ProcedureTable).on(_._1.cdcCode === _.procedureCode).result
    } map { planDetailsList =>
   
    planDetailsList.foreach(treatmentPlan=>{
      if(!(treatmentPlan._1._2.isDefined)){
      var planDetail = treatmentPlan._1._1
      var procedure = treatmentPlan._2
      var vFeeSchedule: Double = 0
      var tooth: Long = 0

      if(planDetail.fee != ""){
      vFeeSchedule = f"${planDetail.fee}%.2f".toDouble
      }
      if(planDetail.toothNumber!=""){
      tooth = planDetail.toothNumber.toLong
      }

      var data1 = TreatmentPlanProcedureRow(
      convertOptionLong(planDetail.id),
      planDetail.cdcCode,
      procedure.procedureType,
      planDetail.status,
      tooth,
      vFeeSchedule
      )
      dataList = dataList :+ data1
      }
      })
      

  }
  Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  
} else if(forWhich == "clinicalNotes"){
  var block = db.run{
  TreatmentPlanDetailsTable.filter(_.patientId === patientId).filter(_.status === "Accepted").sortBy(_.id)
  .join(ProcedureTable).on(_.cdcCode === _.procedureCode).result
    } map { planDetailsList =>
    planDetailsList.foreach(treatmentPlan=>{

      var planDetail = treatmentPlan._1
      var procedure = treatmentPlan._2
      var vFeeSchedule: Double = 0
      var tooth: Long = 0

      if(planDetail.fee != ""){
      vFeeSchedule = f"${planDetail.fee}%.2f".toDouble
      }
      if(planDetail.toothNumber!=""){
      tooth = planDetail.toothNumber.toLong
      }

      var data1 = TreatmentPlanProcedureRow(
      convertOptionLong(planDetail.id),
      planDetail.cdcCode,
      procedure.procedureType,
      planDetail.status,
      tooth,
      vFeeSchedule
      )
      dataList = dataList :+ data1
      
    })
    }
    Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
}
Future(Ok(Json.toJson(dataList)))

}

def getFee(patientId: Option[Long],procedureCode: String, insuranceCompanyId: Option[Long])= silhouette.SecuredAction.async { request => 
var strPracticeId = request.identity.asInstanceOf[StaffRow].practiceId
var vFeeSchedule: Float = 0
var carrierName = ""
var vInsuranceCompanyId:Long= -1
if(insuranceCompanyId == None){
   if(patientId != None && patientId != Some(0)){
     var insIdBlock = db.run{
        StepTable.filter(_.treatmentId === patientId).filter(_.formId === "1A").sortBy(_.dateCreated.desc).result.head
      } map { steps =>
        carrierName = convertOptionString((steps.value \ "carrierName").asOpt[String])
        if (carrierName!=""){
            vInsuranceCompanyId = carrierName.toLong
        }
        
       }
        Await.ready(insIdBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
   }
   }else{
    vInsuranceCompanyId = convertOptionLong(insuranceCompanyId)
  }
  var feeBlock =  db.run{
      InsuranceFeeTable.filter(_.practiceId === strPracticeId).filter(_.insuranceId === vInsuranceCompanyId).filter(_.procedureCode === procedureCode).result
    } map { insuranceList =>

      insuranceList.foreach(result => {
          vFeeSchedule = vFeeSchedule + result.fee
        
      })
    }
    Await.ready(feeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  
    val newObj = Json.obj() + ("fee" -> JsNumber(Math.round(vFeeSchedule * 100.0) / 100.0)) + ("insuranceCompanyId" -> JsString(vInsuranceCompanyId.toString))
    Future(Ok(Json.toJson(newObj)))
}

def patientDetails (patientId :Long) = silhouette.UnsecuredAction.async { request =>
     db.run {
          TreatmentTable.filterNot(_.deleted).filter(_.id === patientId).result.head
      } map { patientList =>
      var data = PatientDetailsRow(patientList.firstName,patientList.lastName,patientList.emailAddress,patientList.phoneNumber,patientList.country)
        Ok(Json.toJson(data))
      }
  }
}
  
  
  
  
  



