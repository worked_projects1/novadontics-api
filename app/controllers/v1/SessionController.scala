package controllers.v1

import co.spicefactory.services.{EmailService, UserIdentityService}
import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model._
import com.google.inject._
import com.google.inject.name.Named
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util.{Credentials => SilhouetteCredentials}
import com.mohiva.play.silhouette.impl.exceptions.{IdentityNotFoundException, InvalidPasswordException}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import controllers.NovadonticsController
import controllers.v1.Output.Account
import models.daos.tables.{IdentityRow, StaffRow, UserRow, DevicesRow}
import org.joda.time.DateTime
import play.api.{Application, Mode}
import play.api.i18n.Messages
import play.api.i18n.Messages.Implicits._
import play.api.libs.json.Json.{obj, toJson}
import play.api.libs.json._
import play.api.mvc._
import security.models.Token
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

import scala.concurrent.{ExecutionContext, Future}

case class Credentials(identifier: String, secret: String, application: String, appVersion: Option[String], deviceType: Option[String], deviceToken: Option[String])

object Credentials {
  implicit val format = Json.format[Credentials]

  implicit def toSilhouetteCredentials(credentials: Credentials): SilhouetteCredentials = SilhouetteCredentials(credentials.identifier, credentials.secret)
}

object Output {

  case class Account(
                      title: Option[String],
                      firstName: Option[String],
                      lastName: Option[String],
                      address: Option[String],
                      city: Option[String],
                      state: Option[String],
                      zip: Option[String],
                      county: Option[String],
                      email: Option[String],
                      businessPhone: Option[String],
                      cellPhone: Option[String],
                      creditCardNumber: Option[String],
                      creditCardExpiry: Option[String]
                    )

}

/**
  * SessionController, provides check, logIn and logOut methods.
  */
@Singleton
class SessionController @Inject()(@Named("blocking") blockingExecutor: ExecutionContext, ses: AmazonSimpleEmailService, userIdentityService: UserIdentityService,
                                  credentialsProvider: CredentialsProvider,
                                  silhouette: Silhouette[BearerTokenEnv],
                                  val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  implicit val app = application

  implicit val tokenFormat = Json.format[Token]
  private val emailService = new EmailService(ses, application)
  private val from = application.configuration.getString("aws.ses.signUpRequest.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.from` property."))
  private val subject = application.configuration.getString("aws.ses.signUpRequest.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.subject` property."))

  private val replyTo = application.configuration.getString("aws.ses.signUpRequest.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.replyTo` property."))

  //private val address = application.configuration.getString("aws.ses.signUpRequest.address").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.signUpRequest.address` property."))
  private val baseUrl = application.configuration.getString("baseUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))
  private val minAppVersion = application.configuration.getString("minimumAppVersion").getOrElse(throw new RuntimeException("Configuration is missing `minimumAppVersion` property."))
  private val reviewAppVersion = application.configuration.getString("reviewAppVersion").getOrElse(throw new RuntimeException("Configuration is missing `reviewAppVersion` property."))
  private val testFlightAppVersion = application.configuration.getString("testFlightAppVersion").getOrElse(throw new RuntimeException("Configuration is missing `testFlightAppVersion` property."))

  /**
    * Checks info about provided session.
    */
  def check = silhouette.UserAwareAction.async { implicit request =>

    lazy val anonymousResult = Future.successful(NotFound(obj("Anonymous" -> obj())))
    var webAppVersion = "";
    var cartQuantity: Long = 0

    var configBlock = db.run{
      ConfigTable.filter(_.label === "webAppVersion").result
    } map { configMap =>
        webAppVersion = configMap.head.value
    }
    Await.ready(configBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))

    request.identity.fold(anonymousResult) { identity =>
      Future.successful {
        Ok {
          identity match {
            case UserRow(id, name, _, _, _, _, role) =>
              obj(
                role.toString -> obj(
                  "id" -> id,
                  "name" -> name,
                  "webAppVersion" -> webAppVersion
                )
              )
            case StaffRow(id, name, email, _, role, practiceId, _, _, canAccessWeb, _, _, _, _, _, _, _, _, _, _, _, _, title, lastName, _, _, _, _, ciiPrograms, appSquares, _, _, _, _, _, webModules,prime,authorizeCustId, allowAllCECourses, _, _, agreementAccepted, _, _, _, doseSpotUserId, _, _, _ ,_, _, ceInfo) =>

     var xvWebURL: String = ""
     var xvWebUserName: String = ""
     var xvWebPassword: String = ""
     var sName= title +" "+ name +" "+ convertOptionString(lastName)
     var practiceName = ""
     var onlineRegistrationModule = false

         var block = db.run{
            PracticeTable.filter(_.id === practiceId).result
          } map {result=> result.map{s =>
                      xvWebURL = convertOptionString(s.xvWebURL)
                      xvWebUserName = convertOptionString(s.xvWebUserName)
                      xvWebPassword = convertOptionString(s.xvWebPassword)
                      practiceName = s.name
                      onlineRegistrationModule = convertOptionalBoolean(s.onlineRegistrationModule)
                      }
          }
          var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))

          var cart = db.run{
                CartTable.filter(_.userId === id).map(_.quantity).sum.result
              } map { cartCount =>
                      cartQuantity = convertOptionLong(cartCount)
              }
            Await.ready(cart, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              obj(
                role.toString -> obj(
                  "id" -> id,
                  "firstName" -> name,
                  "lastName" -> convertOptionString(lastName),
                  "name" -> sName,
                  "practiceName" -> practiceName,
                  "email" -> email,
                  "allowAllCECourses" -> allowAllCECourses,
                  "practiceId" -> practiceId,
                  "webAppVersion" -> webAppVersion,
                  "webModules" -> webModules,
                  "ciiPrograms" -> ciiPrograms,
                  "prime" -> prime,
                  "xvWebURL" -> xvWebURL,
                  "xvWebUserName" -> xvWebUserName,
                  "xvWebPassword" -> xvWebPassword,
                  "authorizeCustId" -> authorizeCustId,
                  "appSquares" -> appSquares,
                  "agreementAccepted" -> agreementAccepted,
                  "doseSpotUserId" -> doseSpotUserId,
                  "cartQuantity" -> cartQuantity,
                  "onlineRegistrationModule" -> onlineRegistrationModule,
                  "ceInfo" -> ceInfo
                )
              )
          }
        }
      }
    }
  }


  // Business decision is that the expired accounts will still be usable
  val expiryEnabled = true
  var toAddress = List[String]()

  def accountExpired(identity: IdentityRow): Boolean =
    identity match {
      case UserRow(_, _, _, _, _, _, _) => false
      case StaffRow(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, expirationDate, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) => (expirationDate < DateTime.now) && expiryEnabled
    }
  def accountType(identity: IdentityRow): String =
    identity match {
      case UserRow(_, _, _, _, _, _, _) => ""
      case StaffRow(_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, accountType, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) => (accountType)
    }

  /**
    * Authenticate a user through the Credentials‘ Provider.
    */


  def logIn = Action.async(parse.json[Credentials]) { implicit request =>

     var appointmentSetUp : Boolean = false
     var operatories: Long = 0
     var providers: Long = 0
     var workingHours: Long = 0
     var practiceName: String = ""
     var xvWebURL: String = ""
     var xvWebUserName: String = ""
     var xvWebPassword: String = ""
     var versionAlert:Boolean = true
     var cartQuantity: Long = 0

     var minAppVersion = ""
     var reviewAppVersion = ""
     var testFlightAppVersion = ""
     var onlineRegistrationModule = false

     var block = db.run{
       ConfigTable.result } map { configMap =>
        configMap.foreach(result => {
          if(result.label == "testFlightAppVersion"){
            testFlightAppVersion = result.value
          } else if(result.label == "reviewAppVersion"){
            reviewAppVersion = result.value
          } else if(result.label == "minimumAppVersion") {
            minAppVersion = result.value
          }
        })
     }
     Await.ready(block, atMost = scala.concurrent.duration.Duration(3, SECONDS))

     if (request.body.appVersion != None){
        if (convertOptionString(request.body.appVersion) == minAppVersion || convertOptionString(request.body.appVersion) == reviewAppVersion || convertOptionString(request.body.appVersion) == testFlightAppVersion){
            versionAlert = false
        }
     }else{
        versionAlert = false
     }
     
    if (versionAlert){

       Future.successful(Status(426) (obj("message" -> Messages("You are using older version! Please upgrade"))))

    }else{

       credentialsProvider
      .authenticate(request.body)
      .flatMap(userIdentityService.retrieve)
      .flatMap {
        case Some(identity) =>
          val authorized = (request.body.application == "web" && identity.isInstanceOf[UserRow]) || (request.body.application == "ios" && identity.isInstanceOf[StaffRow]) ||
            (request.body.application == "web" && identity.isInstanceOf[StaffRow])
          if (authorized && !accountExpired(identity) && (accountType(identity)!= "Lead")) {

            silhouette.env.authenticatorService.create(LoginInfo("credentials", identity.email)).flatMap { authenticator =>
              silhouette.env.eventBus.publish(LoginEvent(identity, request))
              silhouette.env.authenticatorService.init(authenticator).flatMap { token =>
                val response =
                  Created {
                    toJson(security.models.Token(token = token, expiresOn = authenticator.expirationDateTime)) match {
                      case body: JsObject =>
                        body ++ obj(
                          identity match {
                            case UserRow(_, name, _, _, _, _, role) =>
                              "user" -> obj(
                                "name" -> name,
                                "role" -> role.toString.toLowerCase()
                              )
                            case StaffRow(id, name, email, _, role, practiceId, _, udid,udid1, canAccessWeb, _, _, _, _, _, _, _, _, _, _, _, title, lastName, _, _, _, _, ciiPrograms,appSquares, _, _, _, _, _, webModules,prime, authorizeCustId, allowAllCECourses, udid2, _, agreementAccepted, _, _, _, doseSpotUserId, _, _, _, _, _, ceInfo) =>

              db.run{
                DeviceTable.filter(_.staffId === id).result
              } map { deviceMap =>
                if(deviceMap.length == 0){
                var block = db.run{
                DeviceTable.returning(DeviceTable.map(_.id)) += DevicesRow(None,convertOptionLong(id),"","","",convertOptionString(request.body.deviceToken),LocalDate.now)
                } map {
                case 0L => PreconditionFailed
                case id => Ok
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                }
              }
              val count = db.run{
                      for {
                        operatoriesLength <- OperatorTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result
                        providersLength <- ProviderTable.filterNot(_.deleted).filter(_.practiceId === practiceId).length.result
                        workingHoursLength <- WorkingHoursTable.filter(_.practiceId === practiceId).length.result
                        practice <- PracticeTable.filter(_.id === practiceId).result
                      } yield {
                      (operatories = operatoriesLength,
                      providers = providersLength,
                      workingHours = workingHoursLength,
                      practice.map{s=>
                      practiceName = s.name
                      onlineRegistrationModule = convertOptionalBoolean(s.onlineRegistrationModule)
                      xvWebURL = convertOptionString(s.xvWebURL)
                      xvWebUserName = convertOptionString(s.xvWebUserName)
                      xvWebPassword = convertOptionString(s.xvWebPassword)
                      })
                      }
              }
              var AwaitResult = Await.ready(count, atMost = scala.concurrent.duration.Duration(3, SECONDS))

              var cart = db.run{
                CartTable.filter(_.userId === id).map(_.quantity).sum.result
              } map { cartCount =>
                      cartQuantity = convertOptionLong(cartCount)
              }
              Await.ready(cart, atMost = scala.concurrent.duration.Duration(30, SECONDS))

              if(operatories > 0 && providers > 0 && workingHours > 0){
                  appointmentSetUp = true
              }
              var sName= title +" "+ name +" "+ convertOptionString(lastName)

                              "user" -> obj(
                                "id" -> id,
                                "firstName" -> name,
                                "lastName" -> convertOptionString(lastName),
                                "name" -> sName,
                                "email" -> email,
                                "role" -> role.toString.toLowerCase(),
                                "udid" -> udid,
                                "udid1" -> udid1,
                                "udid2" -> udid2,
                                "canAccessWeb" -> canAccessWeb,
                                "ciiPrograms" -> ciiPrograms,
                                "appSquares" -> appSquares,
                                "catalogRecPerPage" -> 50,
                                "practiceId" -> practiceId,
                                "practiceName" -> practiceName,
                                "allowAllCECourses" -> allowAllCECourses,
                                "appointmentSetup" -> appointmentSetUp,
                                "webModules" -> webModules,
                                "prime" -> prime,
                                "xvWebURL" -> xvWebURL,
                                "xvWebUserName" -> xvWebUserName,
                                "xvWebPassword" -> xvWebPassword,
                                "authorizeCustId" -> authorizeCustId,
                                "payment" -> true,
                                "agreementAccepted" -> agreementAccepted,
                                "doseSpotUserId" -> doseSpotUserId,
                                "cartQuantity" -> cartQuantity,
                                "onlineRegistrationModule" -> onlineRegistrationModule,
                                "ceInfo" -> ceInfo
                              )
                          }
                        )
                      case other =>
                        other
                    }
                  }
                silhouette.env.authenticatorService.embed(token, response)


              }
            }
          } else if (accountExpired(identity)) {
            Future.successful(PreconditionFailed(obj("message" -> Messages("login.accountExpired"))))
          } else {
            Future.successful(BadRequest(obj("message" -> Messages("login.notAuthorized"))))
          }
        case None =>
          Future.successful(BadRequest(obj("message" -> Messages("login.wrongCredentials"))))
      }.recover {
      case _: InvalidPasswordException =>
        BadRequest(obj("message" -> Messages("login.wrongCredentials")))
      case _: IdentityNotFoundException =>
        BadRequest(obj("message" -> Messages("login.wrongCredentials")))
    }
    }

  }


  /**
    * Handles the logout action.
    */
  def logOut = silhouette.SecuredAction.async { implicit request =>
    silhouette.env.eventBus.publish(LoginEvent(request.identity, request))
    silhouette.env.authenticatorService.discard(request.authenticator, Ok)
  }

  def signUpRequest = silhouette.UnsecuredAction.async(parse.json) { request =>

    val signUp = for {
      title <- (request.body \ "title").validateOpt[String]
      firstName <- (request.body \ "firstName").validateOpt[String]
      lastName <- (request.body \ "lastName").validateOpt[String]
      address <- (request.body \ "address").validateOpt[String]
      city <- (request.body \ "city").validateOpt[String]
      state <- (request.body \ "state").validateOpt[String]
      zip <- (request.body \ "zip").validateOpt[String]
      county <- (request.body \ "county").validateOpt[String]
      email <- (request.body \ "email").validateOpt[String]
      businessPhone <- (request.body \ "businessPhone").validateOpt[String]
      cellPhone <- (request.body \ "cellPhone").validateOpt[String]
      creditCardNumber <- (request.body \ "creditCardNumber").validateOpt[String]
      creditCardExpiry <- (request.body \ "creditCardExpiry").validateOpt[String]

    } yield {
      Account(title, firstName, lastName, address, city, state, zip, county, email, businessPhone, cellPhone, creditCardNumber, creditCardExpiry)
    }
    signUp match {
      case JsSuccess(account, _) =>
        val data1: Output.Account = Output.Account(account.title, account.firstName, account.lastName, account.address, account.city, account.state, account.zip, account.county, account.email, account.businessPhone, account.cellPhone, account.creditCardNumber, account.creditCardExpiry)

        db.run {
          EmailMappingTable.filter(_.name === "signupRequest").result
        }.map {
          data =>
            if (request.host.contains("api.novadonticsllc.com")) {
              toAddress = data.head.defaultEmail.getOrElse("").split(",").toList
            } else {
              toAddress = List("k.sivashanmugam@rifluxyss.com", "m.arun@rifluxyss.com")
            }
            Future(emailService.sendEmail(toAddress, from, replyTo, s"Sign Up Request Form", views.html.signUpRequest(data1, baseUrl)))(blockingExecutor).map { _ =>
              Ok
            }
            Ok
        }
    }

  }

  def updateSettings = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    (request.body \ "creditCardFee").validateOpt[String] match {
      case JsSuccess(creditCardFee, _) =>
        db.run {
          SettingsTable.map(_.creditCardFee).update(creditCardFee)
        } map {
          case 0 => NotFound
          case _ => Ok
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def getAuthorizeNetDetails = silhouette.SecuredAction.async { request =>

  val apiUrl = application.configuration.getString("aws.authorizeNet.apiURL").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiURL` property."))

  val apiLoginId = application.configuration.getString("aws.authorizeNet.apiLoginId").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiLoginId` property."))

  val transactionKey = application.configuration.getString("aws.authorizeNet.transactionKey").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.transactionKey` property."))

  val validationMode = application.configuration.getString("aws.authorizeNet.validationMode").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.validationMode` property."))

  val newObj = Json.obj() + ("apiUrl" -> JsString(apiUrl)) + ("apiLoginId" -> JsString(apiLoginId)) + ("transactionKey" -> JsString(transactionKey)) + ("validationMode" -> JsString(validationMode))
  Future(Ok{Json.toJson(Array(newObj))})

  }

  def updateConfig = silhouette.SecuredAction.async(parse.json) { request =>
    val config = for {
      label <- (request.body \ "label").validate[String]
      value <- (request.body \ "value").validate[String]
    } yield {
      (label, value)
    }
    config match {
      case JsSuccess((label, value), _) =>
      db.run {
        ConfigTable.filter(_.label === label).map(_.value).update(value)
      } map {
        case 0 => NotFound
        case _ => Ok
      }
    }
  }

}
