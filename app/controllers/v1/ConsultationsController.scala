package controllers.v1

import co.spicefactory.util.BearerTokenEnv
import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model._
import com.google.inject._
import com.mohiva.play.silhouette.api.Silhouette
import controllers.NovadonticsController
import models.daos.tables._
import play.api.Application
import play.api.libs.json._
import play.api.mvc.Result
import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future, Await}
import scala.concurrent.duration._
import com.github.tototoshi.csv._

@Singleton
class ConsultationsController @Inject()(ses: AmazonSimpleEmailService, silhouette: Silhouette[BearerTokenEnv], val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._

  private val baseUrl = application.configuration.getString("baseUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))

  private val from = application.configuration.getString("aws.ses.consultation.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val replyTo = application.configuration.getString("aws.ses.consultation.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val subject = application.configuration.getString("aws.ses.consultation.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))

  private val sesArn = application.configuration.getString("aws.ses.arn").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  private val sesRegion = application.configuration.getString("aws.ses.region").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  ses.setRegion(Region.getRegion(Regions.fromName(sesRegion)))

  implicit val consultationStatusFormat = new Format[ConsultationRow.Status] {
    import ConsultationRow.Status._

    override def writes(status: ConsultationRow.Status) = Json.toJson {
      status match {
        case Open => "Open"
        case Closed => "Closed"
        case Unknown => "Unknown"
      }
    }

    override def reads(json: JsValue) = json.validate[String].map {
      case "Open" => Open
      case "Closed" => Closed
      case _ => Unknown
    }
  }

  implicit val consultationRowWrites = Writes { consultationRow: ConsultationRow =>
    Json.obj(
      "id" -> consultationRow.id,
      "data" -> consultationRow.data,
      "dateRequested" -> consultationRow.dateRequested,
      "amount" -> consultationRow.amount,
      "mode" -> consultationRow.mode
    )
  }

  case class Step(
    value: JsValue,
    state: StepRow.State,
    formId: String,
    formVersion: Int,
    dateCreated: DateTime
  )

  case class Consultation(
    id: Long,
    requested: Boolean,
    consultType: String,
    consultantName: Option[String],
    consultantId: Option[Long],
    patient: Option[String]
  )

  object Consultation {
    lazy val reads = Reads
    implicit object Reads extends Reads[Consultation] {
      override def reads(json: JsValue) = for {
          id <- (json \ "id").validate[Long]
          requested <- (json \ "requested").validate[Boolean]
          consultType <- (json \ "type").validate[String]
          consultantName <- (json \ "consultantName").validateOpt[String]
          consultantId <- (json \ "consultantId").validateOpt[Long]
          patient <- (json \ "patient").validateOpt[String]
        } yield {
        Consultation(id, requested, consultType, consultantName, consultantId, patient)
      }
    }
  }

  object Step {
    implicit val writes = Json.writes[Step]
  }

  implicit val stepStateFormat = new Format[StepRow.State] {
    import StepRow.State._

    override def writes(state: StepRow.State): JsValue = state match {
      case ToDo => Json.toJson("ToDo")
      case InProgress => Json.toJson("InProgress")
      case Completed => Json.toJson("Completed")
      case Sealed => Json.toJson("Sealed")
    }

    override def reads(json: JsValue): JsResult[StepRow.State] = json.validate[String].map {
      case "InProgress" => InProgress
      case "Completed" => Completed
      case "Sealed" => Sealed
      case _ => ToDo
    }
  }

  implicit val consultationWrites = Writes { consultation: (ConsultationRow, Option[TreatmentRow], StaffRow, PracticeRow) =>
    val (consultationRow, treatmentRow, dentistRow, practice) = consultation

    Json.obj(
      "id" -> consultationRow.id,
      "data" -> consultationRow.data,
      "dateRequested" -> consultationRow.dateRequested,
      "status" -> consultationRow.status,
      "amount" -> consultationRow.amount,
      "mode" ->  consultationRow.mode,
      "customerProfileID" ->  consultationRow.customerProfileID,
      "customerPaymentProfileId" ->  consultationRow.customerPaymentProfileId,
      "transId" ->  consultationRow.transId,
      "transactionCode" ->  consultationRow.transactionCode,
      "majorType" ->  consultationRow.majorType,
      "consultantId" ->  consultationRow.consultantId,
      "consultArea" ->  consultationRow.consultArea,
      "treatment" -> (
        treatmentRow match {
          case Some(treatment) =>
          var patientName = convertOptionString(treatment.firstName) +" "+ convertOptionString(treatment.lastName)
            Json.obj(
              "id" -> treatment.id,
              "patientId" -> treatment.patientId,
              "patientName" -> patientName,
              "firstVisit" -> treatment.firstVisit,
              "lastEntry" -> treatment.lastEntry,
              "phoneNumber" -> treatment.phoneNumber,
              "emailAddress" -> treatment.emailAddress
              // "steps" -> steps
              ) //++ steps.map(s => Json.obj("steps" -> s)).getOrElse(Json.obj())
          case None => JsNull
        }),
      "dentist" -> Json.obj(
        "id" -> dentistRow.id,
        "name" -> dentistRow.name,
        "email" -> dentistRow.email,
        "practice" -> practice.name,
        "country" -> practice.country,
        "city" -> practice.city,
        "state" -> practice.state,
        "zip" -> practice.zip,
        "address" -> practice.address,
        "phone" -> practice.phone
      )
    )
  }

  def readAll(majorType: String) = silhouette.SecuredAction.async { request =>
    val action = ConsultationTable.filter(_.majorType === majorType)
      .filterNot(_.deleted)
      .joinLeft(TreatmentTable).on(_.treatmentId === _.id)
      .join(StaffTable).on(_._1.staffId === _.id)
      .join(PracticeTable).on(_._2.practiceId === _.id)

    db.run {
      request.identity match {
        case StaffRow(id, _, _, _, _, practiceId, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _) => action.filter(_._1._2.practiceId === practiceId).result
        case UserRow(_, _, _, _, _, _, _) => action.result
              }
    } map { data =>
      val consultations = data.groupBy(_._1._1._1.id).map { row =>
        val (_, group) = row
        val ((((consultation, treatment), dentist), practice)) = group.head
        (consultation, treatment, dentist, practice)
      }.toList.sortBy(_._1.dateRequested).reverse
      Ok(Json.toJson(consultations))
    }
  }


  def create = silhouette.SecuredAction(CanRequestConsult).async(parse.json) { request =>
    val closedDate:DateTime = DateTime.parse("1900-01-01")

    val consultation = for {
      data <- (request.body \ "data").validate[JsValue]
      treatmentId <- (request.body \ "treatmentId").validateOpt[Long]
      customerProfileID <- (request.body \ "customerProfileID").validateOpt[Long]
      customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validateOpt[Long]
      transId <- (request.body \ "transId").validateOpt[Long]
      transactionCode <- (request.body \ "transactionCode").validateOpt[String]
      majorType <- (request.body \ "majorType").validateOpt[String]
      consultantId <- (request.body \ "consultantId").validateOpt[Long]
      consultArea <- (request.body \ "consultArea").validateOpt[String]
      amount <- (request.body \ "amount").validateOpt[Float]
    } yield {
      var vAmount: Option[Float] = Some(0f)
      if(amount != None && amount != null){
        vAmount = amount
      }
      ConsultationRow(None, data, DateTime.now, ConsultationRow.Status.Open, treatmentId, request.identity.id.get,vAmount,Some(""),Some(closedDate),customerProfileID,customerPaymentProfileId,transId,transactionCode,majorType,consultantId,consultArea)
    }
    consultation match {
      case JsSuccess(consultationRow, _) =>
        val consultData = (request.body \ "data" \ "consultations").validate[List[Consultation]]
        consultData match {
          case JsSuccess(data, _) => {

            db.run {
              ConsultationTable.returning(ConsultationTable.map(_.id)) += consultationRow
            } map { id =>
              db.run {
                EmailMappingTable.filter(_.id inSetBind data.map(_.id))
                  .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                  .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                  .map(r => (r._1._1, r._2))
                  .result
              } map { d =>
                    val selectedConsultant = data.filter(_.id == d.head._1.id.get).head match {
                      case c: Consultation => if (c.consultantName.getOrElse("") != "") c.consultantName.get else "Anyone"
                      case _ => "Anyone"
                    }
                    val patient = data.head.patient.getOrElse("")
                    val defaultAddresses = d.head._1.defaultEmail.getOrElse("").split(",")
                    val fmt = DateTimeFormat.forPattern("dd MMM yyyy")
                    // send the email to default address
                    var dentistName = request.identity.name
                    request.identity match {
                    case UserRow(id, name, _, _, _, _, role) =>
                    case StaffRow(id, name, email, _, role, practiceId, _, _, canAccessWeb, _, _, _, _, _, _, _, _, _, _, _, _, title, lastName, _, _, _, _, ciiPrograms, _, _, _, _, _, _, webModules,prime,authorizeCustId, allowAllCECourses, _, _, _, _, _, _, _, _, _, _, _, _, _) =>
                    dentistName = title +" "+ name +" "+ convertOptionString(lastName)
                    }
                    Future(
                      ses.sendEmail(
                        new SendEmailRequest()
                          .withSourceArn(sesArn)
                          .withSource(from)
                          .withReplyToAddresses(replyTo)
                          .withDestination(new Destination().withToAddresses(defaultAddresses.toList))
                          .withMessage {
                            new Message()
                              .withSubject(new Content().withData(subject+s"(id: ${id})"))
                              .withBody(new Body().withHtml(new Content().withData(views.html.requestedConsult("Admin", dentistName, request.identity.email, fmt.print(consultationRow.dateRequested), consultationRow.treatmentId, id, d.head._1.title, selectedConsultant, patient,convertOptionString(consultationRow.consultArea)).body.trim())))
                          }
                      )
                    )
                    //send emails to the consultant
                    d.filter(r => r._2.isDefined && r._2.get.name == selectedConsultant) match {
                      case Seq() =>
                      case row =>
                        val (meta, admin) = row.head
                        Future(
                          ses.sendEmail(
                            new SendEmailRequest()
                              .withSourceArn(sesArn)
                              .withSource(from)
                              .withReplyToAddresses(replyTo)
                              .withDestination(new Destination().withToAddresses(s"${admin.get.name} <${admin.get.email}>"))
                              .withMessage {
                                new Message()
                                  .withSubject(new Content().withData(subject+s"(id: ${id})"))
                                  .withBody(new Body().withHtml(new Content().withData(views.html.requestedConsult(admin.get.name, dentistName, request.identity.email, fmt.print(consultationRow.dateRequested), consultationRow.treatmentId, id, meta.title, selectedConsultant, patient, convertOptionString(consultationRow.consultArea)).body.trim())))
                              }
                          )
                        )
                    }
                }
              Created(Json.toJson(consultationRow.copy(id = Some(id))))

            }
          }
        case JsError(e) => {
          //println(e)
          Future.successful(BadRequest)
        }
        }
      case JsError(e) =>
       // println(e)
        Future.successful(BadRequest)
    }
  }

  def read(id: Long) = silhouette.SecuredAction(SupersOnly).async {
    db.run {
      ConsultationTable.filter(_.id === id)
        .filterNot(_.deleted)
        .joinLeft(TreatmentTable).on(_.treatmentId === _.id)
        .join(StaffTable).on(_._1.staffId === _.id)
        .join(PracticeTable).on(_._2.practiceId === _.id)
        .result
    } map {
      case data if data.nonEmpty =>
        val consultations = data.groupBy(_._1._1._1).map { row =>
          val (_, group) = row
          val ((((consultation, treatment), dentist), practice)) = group.head
          (consultation, treatment, dentist, practice)
        }.toList

        Ok(Json.toJson(consultations.head))
      case _ =>
        NotFound
    }
  }

  def update(id: Long) = silhouette.SecuredAction(AdminOnly).async(parse.json) { request =>
    val consultation = for {
      status <- (request.body \ "status").validateOpt[ConsultationRow.Status]
      amount <- (request.body \ "amount").validateOpt[Long]
      mode <- (request.body \ "mode").validateOpt[String]
      customerProfileID <- (request.body \ "customerProfileID").validateOpt[Long]
      customerPaymentProfileId <- (request.body \ "customerPaymentProfileId").validateOpt[Long]
      transId <- (request.body \ "transId").validateOpt[Long]
      transactionCode <- (request.body \ "transactionCode").validateOpt[String]
      majorType <- (request.body \ "majorType").validateOpt[String]
      consultantId <- (request.body \ "consultantId").validateOpt[Long]
      consultArea <- (request.body \ "consultArea").validateOpt[String]
    } yield {
      (status, amount, mode, customerProfileID, customerPaymentProfileId, transId, transactionCode, majorType, consultantId, consultArea)
    }
    consultation match {
      case JsSuccess((status,amount,mode,customerProfileID, customerPaymentProfileId, transId, transactionCode, majorType, consultantId, consultArea), _) =>
        db.run {
          val query = ConsultationTable.filter(_.id === id)
          query.exists.result.flatMap[Result, NoStream, Effect.Read with Effect.Write] {
            case true =>
              if(status == Some(ConsultationRow.Status.Closed)){
                DBIO.seq[Effect.Write](
                  status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  amount.map(value => query.map(_.amount).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  mode.map(value => query.map(_.mode).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  query.map(_.closedDate).update(Some(DateTime.now)),
                  customerProfileID.map(value => query.map(_.customerProfileID).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  customerPaymentProfileId.map(value => query.map(_.customerPaymentProfileId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  transId.map(value => query.map(_.transId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  transactionCode.map(value => query.map(_.transactionCode).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  majorType.map(value => query.map(_.majorType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  consultantId.map(value => query.map(_.consultantId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  consultArea.map(value => query.map(_.consultArea).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                ) map { _ =>
                  Ok
                }
              }else{
                val closedDate:DateTime = DateTime.parse("1900-01-01")
                DBIO.seq[Effect.Write](
                  status.map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  amount.map(value => query.map(_.amount).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  mode.map(value => query.map(_.mode).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  query.map(_.closedDate).update(Some(closedDate)),
                  customerProfileID.map(value => query.map(_.customerProfileID).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  customerPaymentProfileId.map(value => query.map(_.customerPaymentProfileId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  transId.map(value => query.map(_.transId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                  transactionCode.map(value => query.map(_.transactionCode).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
                ) map { _ =>
                  Ok
                }
              }

            case false =>
              DBIO.successful(NotFound)
          }
        }
      case JsError(_) =>
        Future.successful(BadRequest)
    }
  }

  def consultationsRevenue(startDate: String, endDate: String, consultantId: Option[String]) = silhouette.SecuredAction.async { request =>
    var strStartDate = DateTime.parse(startDate)
    var strEndTime = DateTime.parse(endDate).plusDays(1)
    var num: Long = 0
    var totalRevenue: Float = 0
    var query = ConsultationTable.filterNot(_.deleted)

    if(consultantId != None && consultantId != Some("all")){
        query = query.filter(_.consultantId === convertOptionString(consultantId).toLong)
    }

    db.run{
        query.filter(a => a.dateRequested >= strStartDate && a.dateRequested <= strEndTime).filter(_.majorType === "consultation").filter(_.status === ConsultationRow.Status.Closed.asInstanceOf[ConsultationRow.Status]).result
    } map { consultationMap =>

      num = consultationMap.length
      consultationMap.map { result =>
          totalRevenue = totalRevenue + convertOptionFloat(result.amount)
      }
      Ok(Json.obj("totalNumberOfConsultations" -> num,"totalRevenue" -> totalRevenue))
    }
  }

  def inOfficeSupportRevenue(startDate: String, endDate: String) = silhouette.SecuredAction.async { request =>
    var strStartDate = DateTime.parse(startDate)
    var strEndTime = DateTime.parse(endDate).plusDays(1)
    var num: Long = 0
    var totalRevenue: Float = 0
    var query = ConsultationTable.filterNot(_.deleted)

    db.run{
        query.filter(a => a.dateRequested >= strStartDate && a.dateRequested < strEndTime).filter(_.majorType === "inOfficeSupport").filter(_.status === ConsultationRow.Status.Closed.asInstanceOf[ConsultationRow.Status]).result
    } map { ioSupportMap =>

      num = ioSupportMap.length
      ioSupportMap.map{ result =>
              totalRevenue = totalRevenue + convertOptionFloat(result.amount)
      }
      Ok(Json.obj("totalInOfficeSupport" -> num,"totalRevenue" -> totalRevenue))
    }
  }

  def delete(id: Long) = silhouette.SecuredAction(AdminOnly).async {
    db.run {
      ConsultationTable.filter(_.id === id).map(_.deleted).update(true)
    } map {
      case 0 => NotFound
      case _ => Ok
    }
  }

  def consultantMigration = silhouette.SecuredAction.async(parse.json) { request =>
    db.run{
        ConsultationTable.result
    } map { consultantMap =>
    consultantMap.foreach(consultantResult =>{

      val consultationsList = (consultantResult.data \ "consultations").asOpt[List[JsValue]].getOrElse(List())
      var majorType = "inOfficeSupport"
      var consultantId = Option.empty[Long]
      consultationsList.foreach(results => {
        val obj: JsValue = Json.parse(results.toString)
        var types = convertOptionString((obj \ "type").asOpt[String])
        consultantId = (obj \ "consultantId").asOpt[Long]

        if(types == "overThePhone"){
          majorType = "consultation"
        }

      })
      var block = db.run {
        val query = ConsultationTable.filter(_.id === consultantResult.id)
        query.exists.result.flatMap[Result, NoStream, Effect.Write] {
          case true => DBIO.seq[Effect.Write](

            Some(majorType).map(value => query.map(_.majorType).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            consultantId.map(value => query.map(_.consultantId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

          ) map {_ => Ok}
          case false => DBIO.successful(NotFound)
        }
      }
      var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
    })
      Ok { Json.obj("status"->"success", "message"->"Consultation migrated successfully") }
    }
  }


def consultationsRevenueCsv(startDate: String, endDate: String, consultantId: Option[String])=silhouette.SecuredAction { request =>
  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var num: Long = 0
  var totalRevenue: Float = 0

  var query = ConsultationTable.filterNot(_.deleted)
    if(consultantId != None && consultantId != Some("all")){
        query = query.filter(_.consultantId === convertOptionString(consultantId).toLong)
    }

  var block = db.run{
        query.filter(a => a.dateRequested >= strStartDate && a.dateRequested < strEndTime).filter(_.majorType === "consultation").filter(_.status === ConsultationRow.Status.Closed.asInstanceOf[ConsultationRow.Status]).result
    } map { consultationMap =>

      num = consultationMap.length
      consultationMap.map{ result =>
              totalRevenue = totalRevenue + convertOptionFloat(result.amount)
      }
    }

  val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  val file = new java.io.File("/tmp/Consultations Revenue.csv")
  val writer = CSVWriter.open(file)
  writer.writeRow(List("Start Date","End Date","Total Number Of Consultations", "Total Revenue"))
  writer.writeRow(List(startDate,endDate,num,totalRevenue))
  writer.close()
  Ok.sendFile(file, inline = false, _ => file.getName)
}


def inOfficeSupportRevenueCsv(startDate: String, endDate: String)=silhouette.SecuredAction { request =>
  var strStartDate = DateTime.parse(startDate)
  var strEndTime = DateTime.parse(endDate).plusDays(1)
  var num: Long = 0
  var totalRevenue: Float = 0

  var query = ConsultationTable.filterNot(_.deleted)

  var block = db.run{
        query.filter(a => a.dateRequested >= strStartDate && a.dateRequested < strEndTime).filter(_.majorType === "inOfficeSupport").filter(_.status === ConsultationRow.Status.Closed.asInstanceOf[ConsultationRow.Status]).result
    } map { ioSupportMap =>

      num = ioSupportMap.length
      ioSupportMap.map{ result =>
              totalRevenue = totalRevenue + convertOptionFloat(result.amount)
      }
    }
  val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

  val file = new java.io.File("/tmp/In-Office Support Revenue.csv")
  val writer = CSVWriter.open(file)
  writer.writeRow(List("Start Date","End Date","Total In-Office Support", "Total Revenue"))
  writer.writeRow(List(startDate,endDate,num,totalRevenue))
  writer.close()
  Ok.sendFile(file, inline = false, _ => file.getName)
}
}


