package controllers.v1

import com.google.inject._
import controllers.NovadonticsController
import play.api.Application
import play.api.mvc._
import play.api.{Application, Logger}

import scala.concurrent.ExecutionContext

@Singleton
class HealthCheckController @Inject() (val application: Application)(implicit ec: ExecutionContext) extends NovadonticsController {

  import driver.api._
  private val logger = Logger(this.getClass)
  def check = Action.async {
    //logger.debug(s"test logger debug")
    db.run(sql"SELECT 42;".as[Long].head).map {
      case 42 => Ok
      case _ => InternalServerError
    }
  }

}
