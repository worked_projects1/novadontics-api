package co.spicefactory.tasks

import play.api.libs.json.Json.{obj, toJson}
import play.api.libs.json._
import scala.concurrent.duration._
import javax.inject.Singleton
import akka.actor.Actor
import com.google.inject.Inject
import play.api.Application
import scala.concurrent.{ExecutionContext, Future,Await}
import controllers.NovadonticsController
import scala.concurrent.ExecutionContext
import scalaj.http.{Http, HttpOptions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import models.daos.tables.{ OnlineRegInviteTrackRow }
import models.daos.tables.ExpiryNotificationRow
import co.spicefactory.services.EmailService
import models.daos.tables.StepRow.State
import models.daos.tables.{ StepRow }
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json.JSONObject
import org.json.JSONArray
import play.api.libs.json._
import scala.util.control.Breaks._


@Singleton
class OnlineRegReminderService @Inject()(ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends Actor with NovadonticsController {

  import com.github.tototoshi.slick.PostgresJodaSupport._
  import driver.api._
  implicit val reminderTrackFormat = Json.format[OnlineRegInviteTrackRow]
  private val emailService = new EmailService(ses, application)
  

  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  val subject = application.configuration.getString("aws.ses.appointmentReminder.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  val patientSiteBaseUrl = application.configuration.getString("aws.ses.appointmentReminder.patientSiteBaseUrl").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.patientSiteBaseUrl` property."))

override def receive: Receive = {

  
  case _ => Ok

  def updateLastInviteSentInTreatments(treatmentTableId: Long, lastInviteSent: DateTime) = {
     var tb = db.run {
      TreatmentTable.filter(_.id === treatmentTableId).map(_.lastInvitationSent)
      .update(Some(lastInviteSent))
     }
     val AwaitResult = Await.ready(tb, atMost = scala.concurrent.duration.Duration(20, SECONDS))
  }

  def insertRegInviteTrackRow(patientId: Long, sentDateTime: DateTime) = {
    var onlineRegInviteTrackRow = OnlineRegInviteTrackRow(None,patientId,DateTime.now)
     db.run {
       OnlineRegInviteTrackTable.returning(OnlineRegInviteTrackTable.map(_.id)) += onlineRegInviteTrackRow              
     } map {
        case 0L => PreconditionFailed
        case id => Created(Json.toJson(onlineRegInviteTrackRow.copy(id = Some(id))))
      }
  }

  def getOnlineRegTrackTableCount(patientId: Long): Int = {
    var count: Int = 0
    var onlineBlock =  db.run {
      OnlineRegInviteTrackTable.filter(_.patientId === patientId) .result
    } map { resultData => 
      count = resultData.length
    }
    Await.ready(onlineBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    count
  }

  
  var b = db.run{
      TreatmentTable.filterNot(_.deleted).filter(_.onlineRegistration === "Yes")
      .filter(_.onlineRegistrationStatus === "Invitation Sent")
      .join(PracticeTable).on(_.practiceId === _.id)
      .result
    } map { resultData =>
       resultData.foreach(data => {
          val treatmentsTableData = data._1
          val practiceTableData = data._2
          var scopeDate = new DateTime
          val patientId = convertOptionLong(treatmentsTableData.id)
          var onlineRegInviteCount = getOnlineRegTrackTableCount(patientId)
          var patientName = convertOptionString(treatmentsTableData.firstName) +" "+ convertOptionString(treatmentsTableData.lastName)
          var canSendReminder = false

          var lastInvitationSent = 
            new DateTime(convertOptionalDateTime(treatmentsTableData.lastInvitationSent))

          var tempCount = onlineRegInviteCount - 1
          if (tempCount < convertOptionString(practiceTableData
          .onlineRegMaxReminder).toInt && (convertOptionalBoolean(practiceTableData.onlineRegistrationModule)))  {  
            canSendReminder = true
            if(tempCount.equals(convertOptionString(practiceTableData.onlineRegMaxReminder).toInt)) {
              var tb = db.run{
                  TreatmentTable.filter(_.id === treatmentsTableData.id).map(_.inviteMaxLimitReached).update(Some(true))
                }
              val AwaitResult = Await.ready(tb, atMost = scala.concurrent.duration.Duration(20, SECONDS))
            }
          }

          var toNumber = treatmentsTableData.phoneNumber
          var country = convertOptionString(treatmentsTableData.country)
          var toAddress = treatmentsTableData.emailAddress
          var subject = "Online Registration Reminder"
          var messageContent = convertOptionString(practiceTableData.onlineInviteMessage)
          var stringToEncrypt = "patientId=" + patientId + "/practiceId=" + convertOptionLong(practiceTableData.id)
          var registrationFormLink = patientSiteBaseUrl + "/onlineRegistration/" + encryptMethod(stringToEncrypt)
          if(canSendReminder) {
             if (messageContent != null && messageContent != "") {
              messageContent = messageContent.replace("<<link>>", registrationFormLink).replace("<<patient name>>", patientName)
            }

            if ("Every day".equals(convertOptionString(practiceTableData.onlineRegReminderFrequency))) {
              scopeDate = DateTime.now().minusDays(1)
              if(treatmentsTableData.lastInvitationSent == None || lastInvitationSent.isBefore(scopeDate)) {
                sendSMS(toNumber, messageContent, country)
                emailService.sendReminder(List(toAddress), from, replyTo,subject, messageContent)
                insertRegInviteTrackRow(patientId, DateTime.now)
                updateLastInviteSentInTreatments(convertOptionLong(treatmentsTableData.id),DateTime.now)

            } 
            }else if("Every two days".equals(convertOptionString(practiceTableData.onlineRegReminderFrequency))) {
              scopeDate = DateTime.now().minusDays(2)
              if(treatmentsTableData.lastInvitationSent == None || (lastInvitationSent.isBefore(scopeDate))) {
                sendSMS(toNumber, messageContent, country)
                emailService.sendReminder(List(toAddress), from, replyTo,subject, messageContent)
                insertRegInviteTrackRow(patientId, DateTime.now)
                updateLastInviteSentInTreatments(convertOptionLong(treatmentsTableData.id),DateTime.now)
                }
              }
          }

       })  
      }

      
    }
  
  }