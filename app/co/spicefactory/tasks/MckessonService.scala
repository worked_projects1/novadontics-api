package co.spicefactory.tasks

import javax.inject.Singleton
import akka.actor.Actor
import co.spicefactory.services.EmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.Inject
import controllers.NovadonticsController
import models.daos.tables.ExpiryNotificationRow
import org.joda.time.DateTime
import play.api.Application
import scala.concurrent.{ExecutionContext, Future,Await}
import scala.concurrent.duration._
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import scala.concurrent.ExecutionContext
import scalaj.http.{Http, HttpOptions}
import models.daos.tables.{ProductRow, VendorRow, CategoryRow, StaffRow, McKessonImportLogRow, MckessonProductsRow}
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

object NonEmpty1 {
  def unapply(l: List[_]) = l.headOption.map(_ => l)
}

@Singleton
class MckessonService @Inject()(ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends Actor with NovadonticsController {

  import com.github.tototoshi.slick.PostgresJodaSupport._
  import driver.api._

  private val emailService = new EmailService(ses, application)
  val fmt = DateTimeFormat.forPattern("dd MMM yy HH:mm")

  implicit val productFormat = Json.format[ProductRow]
  implicit val mckessonImportFormat = Json.format[McKessonImportLogRow]

  override def receive: Receive = {
    case _ =>

// Mckesson import
var offset: Long = 0
var pageSize: Long = 100
var recordExists = false
var completed = false
var importId: Long = 0
var date = LocalDate.now()
    db.run{
      McKessonImportLogTable.filter(_.date === date).result
    } map { mckessonMap =>
      if(mckessonMap.length > 0){
          mckessonMap.map{ mckessonResult =>
          recordExists = true
          importId = convertOptionLong(mckessonResult.id)
          offset = mckessonResult.offset + pageSize

          if(mckessonResult.status == "completed"){
            completed = true
          }
        }
    }

   if(completed != true){
   val vSource = application.configuration.getString("mckessonFeedSource").getOrElse(throw new RuntimeException("Configuration is missing `mckessonFeedSource` property."))

    val url = "https://mms.mckesson.com/services/xml/3B7BxHPxCcPkndhipBsynwN4Id7sBR/ItemFeed"

    val xmlString = "<ItemFeedRequest itemType='detail extra availability'><Credentials><Identity>NOVB2B</Identity><SharedSecret>Vgb21ed8f78</SharedSecret><Account>64848730</Account><ShipTo>64848731</ShipTo></Credentials><Feed><Source>"+vSource+"</Source><PageSize>"+pageSize+"</PageSize><Offset>"+offset+"</Offset></Feed></ItemFeedRequest>"

	  val response = Http(url).postData(xmlString.toString)
          .header("Content-Type", "text/xml")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(100000)).asString

          val itemList = scala.xml.XML.loadString(response.body)

          val itemFeedResult = (itemList \ "FeedResult")
          var feedSize = ""
          itemFeedResult.foreach{ unit =>
          feedSize = (unit \ "@feedSize").text
          }

          val items = (itemList \ "FeedResult" \ "ItemOut")
          items.foreach{
            itemRecord =>

               val itemId = (itemRecord \ "ItemId").text
               val itemDesc = (itemRecord \ "ItemDetail" \ "Description").text
               val itemExtra = (itemRecord \ "ItemExtra" \ "ItemImage").text
               var imageUrl = "https://mms.image.mckesson.com/CumulusWeb/Images/High_Res/"+itemExtra

               var value = ""
               var minorValue = ""
               var majorValue = ""
               var stockAvailable = true
               var categoryId: Long = 0
               var parentCategoryId: Long = 0
               val categoryList = itemRecord \ "ItemDetail" \ "Category"

               categoryList.foreach { unit =>
                if((unit \ "@type").text == "Minor"){
                  minorValue = unit.text
                }
                if((unit \ "@type").text == "Major"){
                  majorValue = unit.text
                }
               }
              // --- FINDING CATEGORY ID
              if(majorValue != ""){
                var block = db.run{
                  CategoryTable.filter(_.parent.isEmpty).filter(_.name === majorValue).result
                } map { majorCategoryList =>
                  if(majorCategoryList.length > 0){
                      parentCategoryId = convertOptionLong(majorCategoryList.head.id)
                  } else {
                      var block2 = db.run{
                          CategoryTable.returning(CategoryTable.map(_.id)) += CategoryRow(None,majorValue,Some(false),null,0)
                        } map {
                            case 0L => PreconditionFailed
                            case id => parentCategoryId = id
                        }
                      var AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              }

              if(minorValue != ""){
                var block = db.run{
                  CategoryTable.filter(_.parent === parentCategoryId).filter(_.name === minorValue).result
                } map { minorCategoryList =>
                  if(minorCategoryList.length > 0){
                      categoryId = convertOptionLong(minorCategoryList.head.id)
                  } else {
                      var block2 = db.run{
                          CategoryTable.returning(CategoryTable.map(_.id)) += CategoryRow(None,minorValue,Some(false),Some(parentCategoryId),0)
                        } map {
                            case 0L => PreconditionFailed
                            case id => categoryId = id
                        }
                      var AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                  }
                }
                Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
              }

              //  if(minorValue != ""){
              //    value = minorValue
              //  }else{
              //    value = majorValue
              //  }

              //     var block = db.run{
              //       CategoryTable.filter(_.name === "McKesson").result
              //     } map{ categoriesList => categoriesList.map{categories =>

              //       var block1 = db.run{
              //         CategoryTable.filter(_.parent === categories.id).filter(_.name === value).result
              //       }map{result =>
              //         if(result.length >= 1){
              //             result.map{categoryId =>  categoryID = convertOptionLong(categoryId.id) }
              //         } else {
              //           var block2 = db.run{
              //                 CategoryTable.returning(CategoryTable.map(_.id)) += CategoryRow(None,value,Some(false),categories.id,0)
              //               } map {
              //               case 0L => PreconditionFailed
              //               case id => categoryID = id
              //             }
              //             var AwaitResult = Await.ready(block2, atMost = scala.concurrent.duration.Duration(60, SECONDS))
              //         }
              //       }
              //       var AwaitResult = Await.ready(block1, atMost = scala.concurrent.duration.Duration(60, SECONDS))
              //     }
              //   }
              //   var AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(120, SECONDS))

               // --- FETCH UNIT PRICE
               val unitsList = itemRecord \ "ItemAvailability" \ "ItemUom"

               val itemStatus = itemRecord \ "ItemAvailability" \ "ItemStatus"
               val itemStock = itemRecord \ "ItemAvailability" \ "ItemStock"

              var statusCode = ""
              var stockCode = ""
              var available = ""
               itemStatus.foreach{
                 unit=>
                 statusCode = (unit \ "@code").text
                 available = (unit \ "@available").text
               }

               itemStock.foreach{
                 unit=>
                 stockCode = (unit \ "@code").text
               }

               if(statusCode == "ER" || statusCode == "O"){
                 var block = db.run {
                    ProductTable.filter(_.serialNumber === itemId).delete.map {
                      case 0L => NotFound
                      case _ => Ok
                    }
                  }
                  var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
               }

               if(available == "false" || statusCode == "BO" && available == "true" || statusCode == "PB" && available == "true" || statusCode == "SH" && stockCode == "U"){
                  stockAvailable = false
               }

               var vendorID: Long = 0
               var block3 = db.run{
                 VendorTable.filter(_.name === "McKesson").map(_.id).result
               }map{vendorList =>
                vendorList.map{vendor => vendorID = vendor}
               }
               var AwaitResult1 = Await.ready(block3, atMost = scala.concurrent.duration.Duration(60, SECONDS))

               unitsList.foreach{ unit =>
                var mckessonPrice = (unit \ "Price").text.toFloat
                var price = mckessonPrice + (mckessonPrice / 100 * 35)
                var listPrice = mckessonPrice + (mckessonPrice / 100 * 8)
                var quantity = (unit \ "@per").text.toLong
                var units = (unit \ "@units").text

              var unitsValue = units
              if(unitsMap.contains(units)){
                unitsValue = unitsMap(units)
              }

                  var block = db.run{
                        MckessonProductsTable.returning(MckessonProductsTable.map(_.itemId)) += MckessonProductsRow(itemId,unitsValue)
                          } map {
                          case _ => PreconditionFailed
                          case id => Ok
                        }
                  Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))

            if(mckessonPrice != 0 ){                      //No need to create and update when price comes zero
            var block4 = db.run{
              ProductTable.filter(_.serialNumber === itemId).filter(s=> s.unit === unitsValue || s.unit === units).result
            } map { productResult =>
            var productId: Long = 0
            productResult.map{ result =>
              productId = convertOptionLong(result.id)
            }

            if(productResult.length > 0){

              var block = db.run {
              val query = ProductTable.filter(_.id === productId)
              query.exists.result.flatMap[Result, NoStream, Effect.Write] {
              case true => DBIO.seq[Effect.Write](

            Some(itemDesc).map(value => query.map(_.name).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(categoryId).map(value => query.map(_.categoryId).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(imageUrl).map(value => query.map(_.imageUrl).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(price).map(value => query.map(_.price).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(vendorID).map(value => query.map(_.vendorId).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(quantity).map(value => query.map(_.quantity).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(listPrice).map(value => query.map(_.listPrice).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(stockAvailable).map(value => query.map(_.stockAvailable).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
            Some(unitsValue).map(value => query.map(_.unit).update(Some(value)).map(_ => Unit)).getOrElse(DBIO.successful(Unit))
              ) map {_ => Ok}
              case false => DBIO.successful(NotFound)
              }
            }
            var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(60, SECONDS))
            } else {

              if(statusCode != "ER" || statusCode != "O"){                    // No need to create for code ER and O
              var productRow = ProductRow(None,itemDesc,Some(""),itemId,Some(imageUrl),vendorID,Some(categoryId),price,Some(quantity),Some(listPrice),Some(""),Some(null),Some(stockAvailable),Some(unitsValue),Some(false))

            var block5 = db.run{
                              ProductTable.returning(ProductTable.map(_.id)) += productRow
                            } map {
                            case 0L => PreconditionFailed
                            case id => Created(Json.toJson(productRow.copy(id = Some(id))))
                          }
              var AwaitResult = Await.ready(block5, atMost = scala.concurrent.duration.Duration(180, SECONDS))
            }
            }
          }
            var AwaitResult1 = Await.ready(block4, atMost = scala.concurrent.duration.Duration(60, SECONDS))
        }
        }
      }

        var status = "in progress"
        if(offset > feedSize.toLong && feedSize.toLong > 0){
          status = "completed"
         }

          if(recordExists){
         db.run {
            val query = McKessonImportLogTable.filter(_.id === importId)
            query.exists.result.flatMap[Result, NoStream, Effect.Write] {
              case true => DBIO.seq[Effect.Write](

                Some(offset).map(value => query.map(_.offset).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(feedSize.toLong).map(value => query.map(_.feedSize).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit)),
                Some(status).map(value => query.map(_.status).update(value).map(_ => Unit)).getOrElse(DBIO.successful(Unit))

              ) map {_ => Ok}
              case false => DBIO.successful(NotFound)
            }
          }
          } else {
           var block = db.run{
                MckessonProductsTable.delete map{
                  case 0 => NotFound
                  case _ => Ok
                }
            }
            Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

            val mckessonLogRow = McKessonImportLogRow(None,LocalDate.now(),offset,feedSize.toLong,status)
            db.run{
               McKessonImportLogTable.returning(McKessonImportLogTable.map(_.id)) += mckessonLogRow
            } map {
              case 0L => PreconditionFailed
              case id => Created(Json.toJson(mckessonLogRow.copy(id = Some(id))))
            }
          }
        Future(Ok {Json.obj("offset"-> offset,"pageSize"-> pageSize,"feedSize"-> feedSize)})
  }
    }

}
}

