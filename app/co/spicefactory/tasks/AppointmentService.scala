package co.spicefactory.tasks

import javax.inject.Singleton
import akka.actor.Actor
import co.spicefactory.services.EmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.Inject
import controllers.NovadonticsController
import models.daos.tables.ExpiryNotificationRow
import org.joda.time.DateTime
import play.api.Application
import scala.concurrent.{ExecutionContext, Future,Await}
import scala.concurrent.duration._
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import scala.concurrent.ExecutionContext
import scalaj.http.{Http, HttpOptions}
import models.daos.tables.{ProductRow, VendorRow, CategoryRow, StaffRow, McKessonImportLogRow, MckessonProductsRow}
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import java.time.ZonedDateTime;
import java.time.ZoneId;
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter};

//package controllers.v3

import com.typesafe.config.{Config, ConfigFactory}
import scala.io.StdIn.readLine
import com.twilio.Twilio
import com.twilio.`type`.PhoneNumber
import com.twilio.rest.api.v2010.account.Message
import com.mohiva.play.silhouette.api.Silhouette
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import co.spicefactory.util.BearerTokenEnv
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import play.api.{Application, Logger}
import com.google.inject.name.Named
import com.google.inject._
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.json.Json.{obj, toJson}
import play.api.libs.json._
import controllers.NovadonticsController
import scala.util.{Failure, Success, Try}
import models.daos.tables.{ StaffRow,AppointmentRow, AppointmentReminderTrackRow}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import co.spicefactory.services.EmailService
import java.text.SimpleDateFormat

import java.security.MessageDigest
import java.util
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.codec.binary.Base64


@Singleton
class AppointmentService @Inject()(ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends Actor with NovadonticsController {

  import com.github.tototoshi.slick.PostgresJodaSupport._
  import driver.api._
  import java.net.{URLDecoder, URLEncoder}
  implicit val reminderTrackFormat = Json.format[AppointmentReminderTrackRow]

  private val emailService = new EmailService(ses, application)
  val fmt = DateTimeFormat.forPattern("dd MMM yy HH:mm")

  implicit val productFormat = Json.format[ProductRow]
  implicit val mckessonImportFormat = Json.format[McKessonImportLogRow]

  override def receive: Receive = {
    case _ =>
  var nextDate = LocalDate.now().plusDays(1)
  val obj = Json.obj()
  var smsResponse = "SMS Not sent"
  var mailResponse = "Mail Not sent"
 

 //Email params
   val emailService = new EmailService(ses, application)  
   val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
   val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))
   val subject = application.configuration.getString("aws.ses.appointmentReminder.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.subject` property."))
   val patientSiteBaseUrl = application.configuration.getString("aws.ses.appointmentReminder.patientSiteBaseUrl").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.patientSiteBaseUrl` property."))
   val patientAcionUrl = "/confirmAppointment"
   val smsSendEnabled = application.configuration.getString("aws.ses.appointmentReminder.smsSendEnabled").getOrElse(throw new RuntimeException("aws.ses.appointmentReminder.smsSendEnabled` property."))

   

    var b = db.run{
            AppointmentTable.filterNot(_.deleted).filter(_.status === "Scheduled")
            .join(PracticeTable).on(_.practiceId === _.id)
            .join(TreatmentTable).on(_._1.patientId === _.id).result
          }map{ resultData =>

          resultData.foreach(data =>{

          val appointmentTableData = data._1._1
          val practiceTableData = data._1._2
          val treatmentsTableData = data._2
           var currentDateTime = ZonedDateTime.now()
        
          if(practiceTableData.appointmentReminder.get){

          var practiceReminderSetting = convertOptionString(practiceTableData.appReminderSetting)

         

            try{
              var timezone = convertOptionString(practiceTableData.timeZone)
              var zoneId: ZoneId = ZoneId.of(getTimeZoneIdByTimeZone(timezone))
              currentDateTime = ZonedDateTime.now(zoneId)
            } catch {
              case e: Exception => // currentDateTime is server time.
            }
         
          val zoneCurrentDateTime = java.time.LocalDateTime.parse(currentDateTime.toLocalDateTime.toString)
          // var currentDate = currentDateTime.toLocalDate()
          var currentDate = LocalDate.parse(currentDateTime.toLocalDate().toString)
          val reminderSentDate = convertOptionalLocalDate(appointmentTableData.reminderSentDate)
          var patientName = convertOptionString(treatmentsTableData.firstName) +" "+ convertOptionString(treatmentsTableData.lastName)
          practiceReminderSetting.split(",").map{ s =>
          
          var filterDate = LocalDate.now()
          def sendSMSEmail(){

          val inputTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS")
          val outputTimeFormat = new SimpleDateFormat("hh:mm a")
          val inputStartTime = inputTimeFormat.parse(appointmentTableData.startTime.toString)
          val outputStartTime = outputTimeFormat.format(inputStartTime)

          var formatter: DateTimeFormatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy");
          var appointmentDate = formatter.print(appointmentTableData.date)

          val reminderUrlParams = "practiceName="+ practiceTableData.name + "&date=" + appointmentDate + "&time=" + outputStartTime + "&appointmentId=" + convertOptionLong(appointmentTableData.id) + "&patientId=" + appointmentTableData.patientId
          var temp = reminderUrlParams
          var encrStr: String = encryptMethod(temp);

          var encryptedUrl = patientSiteBaseUrl + patientAcionUrl + "/" + encrStr

              if(appointmentTableData.phoneNumber != null && smsSendEnabled == "true"){
                  val config = ConfigFactory.load()

                  val ACCOUNT_SID = config.getString("twilio.account_sid")
                  val AUTH_TOKEN = config.getString("twilio.auth_token")
                  Twilio.init(ACCOUNT_SID, AUTH_TOKEN)

                  val from = new PhoneNumber(config.getString("twilio.from_number"))
                  val phNum = appointmentTableData.phoneNumber
                  val countryId =  getCountryIdByCountryName(convertOptionString(treatmentsTableData.country))
                  val phoneNum = countryId + phNum
                  val to = new PhoneNumber(phoneNum)
                  // val body = "Please confirm your upcoming consultation at" + " " + encryptedUrl
                  val body = convertOptionString(practiceTableData.appointmentReminderMessage).replaceAll("<<patient name>>",patientName).replaceAll("<<practice name>>",practiceTableData.name).replaceAll("<<date>>",(appointmentTableData.date).toString).replaceAll("<<time>>",outputStartTime).replaceAll("<<link>>", encryptedUrl)


                  Try(Message.creator(to, from, body).create()) match {
                    case Success(message) =>
                      smsResponse = "SMS sent"
                     var updateBlock1 = db.run{
                        AppointmentTable.filter(_.id === appointmentTableData.id).map(_.reminderSentDate).update(Some(currentDate))
                      }

                  val AwaitResult = Await.ready(updateBlock1, atMost = scala.concurrent.duration.Duration(20, SECONDS))    

                      var appointmentReminderTrackRow = AppointmentReminderTrackRow(None,convertOptionLong(appointmentTableData.id),DateTime.now,"reminder sent","","","")
                      
                         db.run {
                            AppointmentReminderTrackTable.returning(AppointmentReminderTrackTable.map(_.id)) += appointmentReminderTrackRow
                          
                          } map {
                            case 0L => PreconditionFailed
                            case id => Created(Json.toJson(appointmentReminderTrackRow.copy(id = Some(id))))
                          }
          
                    case Failure(error) =>
                      smsResponse = "SMS not sent"
                  }
              }

            if(appointmentTableData.email != null){
              var messageContent: String = convertOptionString(practiceTableData.appointmentReminderMessage).replaceAll("<<patient name>>",patientName ).replaceAll("<<practice name>>", practiceTableData.name).replaceAll("<<date>>",(appointmentTableData.date).toString).replaceAll("<<time>>",outputStartTime).replaceAll("<<link>>", encryptedUrl) +"\n\n Regards, \n Novadontics Team"
              
              var toAddress: String = convertOptionString(appointmentTableData.email)

           emailService.sendReminder(List(toAddress), from, replyTo,subject, messageContent)
                    var updateBlock = db.run{
                        AppointmentTable.filter(_.id === appointmentTableData.id).map(_.reminderSentDate).update(Some(currentDate)) 
                    }

                  Await.ready(updateBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))

                   mailResponse = "Email sent"

            }
          }
  

          val appDataBlock = db.run{
            AppointmentTable.filter(_.id === appointmentTableData.id).result.head
          } map { appData =>
            
          val appReminderSentDate = convertOptionalLocalDate(appData.reminderSentDate)
          
          // send a reminder (Two days & One day Prior)

         if((s == "One Day Prior" || s == "Two Days Prior") &&  (appData.reminderSentDate == None || appReminderSentDate != currentDate)){

          if(s == "One Day Prior" && (appData.reminderSentDate == None || appReminderSentDate != currentDate)){
          filterDate = currentDate.plusDays(1)
          } 

          if(s == "Two Days Prior" && (appData.reminderSentDate == None || appReminderSentDate != currentDate )){
          filterDate = currentDate.plusDays(2)
          }

          if(appointmentTableData.date == filterDate){
                sendSMSEmail();
          }
        }

          // send a reminder (Two Hours Prior)
          if(s == "Two Hours Prior" && (appData.reminderSentDate == None || appReminderSentDate != currentDate)){

              val twoHoursAfterTime = java.time.LocalDateTime.parse(zoneCurrentDateTime.plusHours(2).plusMinutes(30).toString)       
              val appDataAppDate: java.time.LocalDate = java.time.LocalDate.parse(appointmentTableData.date.toString)
              val appDataStartTime: java.time.LocalTime = java.time.LocalTime.parse(appointmentTableData.startTime.toString)

              val appStartTime: java.time.LocalDateTime = appDataStartTime.atDate(appDataAppDate)

               if((appStartTime.isAfter(zoneCurrentDateTime) && appStartTime.isBefore(twoHoursAfterTime))){
                  sendSMSEmail();
              }
               

          }
          }

       Await.ready(appDataBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))


          }             
        
        }
        
      })

    }
          Await.ready(b, atMost = scala.concurrent.duration.Duration(30, SECONDS))

      
        val newObj = obj + ("SMS" -> JsString(smsResponse)) + ("Email" -> JsString(mailResponse))
        Future(Ok{Json.toJson(Array(newObj))})  

  }
    }

