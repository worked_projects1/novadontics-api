package co.spicefactory.tasks

import javax.inject.{Inject, Named}
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import play.api.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class MckessonScheduler @Inject() (val system: ActorSystem, @Named("scheduler-actor1") val schedulerActor: ActorRef, configuration: Configuration)(implicit ec: ExecutionContext) {
  // val frequency: Int = configuration.getInt("tasks.AccountNotification.frequency.minutes").get

  val start = 30
  val end   = 60
  val rnd = new scala.util.Random
  var frequency = start + rnd.nextInt( (end - start) + 1)

  var actor: Cancellable = system.scheduler.schedule(
    0.microseconds, frequency.minutes, schedulerActor, "update")

}
