package co.spicefactory.tasks

import javax.inject.{Inject, Named}
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import play.api.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class AppointmentScheduler @Inject() (val system: ActorSystem, @Named("scheduler-appointment") val schedulerActor: ActorRef, configuration: Configuration)(implicit ec: ExecutionContext) {

  var actor: Cancellable = system.scheduler.schedule(
    0.microseconds, 30.minutes, schedulerActor, "update")

}
