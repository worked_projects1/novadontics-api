package co.spicefactory.tasks

import javax.inject.Singleton
import akka.actor.Actor
import co.spicefactory.services.EmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.Inject
import controllers.NovadonticsController
import models.daos.tables.ExpiryNotificationRow
import org.joda.time.DateTime
import play.api.Application
import scala.concurrent.{ExecutionContext, Future,Await}
import scala.concurrent.duration._
import java.text.SimpleDateFormat
import java.util.{Date, Locale}
import models.daos.tables.{ProductRow}
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}


@Singleton
class RecallReminderService @Inject()(ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends Actor with NovadonticsController {

  import com.github.tototoshi.slick.PostgresJodaSupport._
  import driver.api._
  import java.net.{URLDecoder, URLEncoder}
  
  private val emailService = new EmailService(ses, application)
  val from = application.configuration.getString("aws.ses.appointmentReminder.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.from` property."))
  val replyTo = application.configuration.getString("aws.ses.appointmentReminder.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.appointmentReminder.replyTo` property."))

  override def receive: Receive = {
    case _ =>
    
    var currentDate = LocalDate.now()
    db.run{
     TreatmentTable.filter(_.recallDueDate === currentDate).filterNot(_.recallReminderSentDate === currentDate).join(PracticeTable).on(_.practiceId === _.id).result 
    } map { treatmentMap => 
      (treatmentMap).foreach(result => {
        val treatment =  result._1
        val practice =  result._2
        var message = convertOptionString(practice.recallReminderMessage)
        val patientName = s"${convertOptionString(treatment.firstName)} ${convertOptionString(treatment.lastName)}"
        val practiceName = s"${practice.name}"
        val interval = s"${convertOptionLong(treatment.intervalNumber)} ${convertOptionString(treatment.intervalUnit)}"        
        message = message.replaceAll("<<patient name>>",patientName).replaceAll("<<interval>>",interval).replaceAll("<<practice name>>",practiceName).replaceAll("<<practice phone>>",practice.phone)
        emailService.sendReminder(List(treatment.emailAddress), from, replyTo, "Recall Reminder", message)
        sendSMS(treatment.phoneNumber, message, convertOptionString(treatment.country))
        var block =  db.run{
          TreatmentTable.filter(_.id === treatment.id).map(_.recallReminderSentDate).update(Some(currentDate))
        }
        Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))
      })
    }
    
  }
}

