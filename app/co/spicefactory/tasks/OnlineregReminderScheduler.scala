package co.spicefactory.tasks

import javax.inject.{Inject, Named}
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import play.api.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class OnlineRegReminderScheduler @Inject() (val system: ActorSystem, @Named("scheduler-onlineReg") val schedulerActor: ActorRef, configuration: Configuration)(implicit ec: ExecutionContext) {

 var actor: Cancellable = system.scheduler.schedule(0.seconds, 30.minutes, schedulerActor, "update")


}

