package co.spicefactory.tasks

import javax.inject.Singleton
import akka.actor.Actor
import co.spicefactory.services.EmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.google.inject.Inject
import controllers.NovadonticsController
import models.daos.tables.ExpiryNotificationRow
import org.joda.time.DateTime
import play.api.Application
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.libs.json._
import scalaj.http.{Http, HttpOptions}
import models.daos.tables.{StaffRow,SubscribeOrderFailureHistoryRow,SubscribeOrderHistoryRow,OrderRow,OrderItemRow}
import controllers.v1.OrdersController

import scala.concurrent.ExecutionContext

object NonEmpty {
  def unapply(l: List[_]) = l.headOption.map(_ => l)
}

object Output {

  case class Account(
    id: Long,
    name: String,
    phone: String,
    email: String,
    practice: String,
    expiry: DateTime,
    expirationDate: String
  )

}

@Singleton
class AccountNotification @Inject()(ses: AmazonSimpleEmailService, val application: Application)(implicit ec: ExecutionContext) extends Actor with NovadonticsController {

  import com.github.tototoshi.slick.PostgresJodaSupport._
  import driver.api._

  private val from = application.configuration.getString("aws.ses.consultation.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))
  private val replyTo = application.configuration.getString("aws.ses.consultation.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val baseUrl = application.configuration.getString("baseUrl").getOrElse(throw new RuntimeException("Configuration is missing `baseUrl` property."))
  private val subject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))

  // Order Invoice email
  private val orderReplyTo = application.configuration.getString("aws.ses.orderInvoice.replyTo").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.replyTo` property."))
  private val orderSubject = application.configuration.getString("aws.ses.orderInvoice.subject").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.subject` property."))
  private val orderFrom = application.configuration.getString("aws.ses.orderInvoice.from").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.passwordReset.from` property."))

  private val emailService = new EmailService(ses, application)
  val fmt = DateTimeFormat.forPattern("dd MMM yy HH:mm")

  override def receive: Receive = {
    case _ =>
		// Console.println("running scheudler");

    implicit val SubscribeOrderHistoryFormat = Json.format[SubscribeOrderHistoryRow]
    implicit val SubscribeFailureHistoryFormat = Json.format[SubscribeOrderFailureHistoryRow]

      db.run {
        StaffTable
          .filterNot(_.archived)
          .filter(_.expirationDate < DateTime.now.plusDays(7))
          .filter(_.expirationDate > DateTime.now)
          .join(PracticeTable).on(_.practiceId === _.id)
          .joinLeft(ExpiryNotificationTable)
          .on(_._1.id === _.staffId)
          .result
      } map { rows =>
		//Console.println("rows"+rows);  
        val data: List[Option[Output.Account]] = rows.groupBy(_._1._1.id).map {
          case (_, group) =>
            val (user, practice) = group.head._1
            val notifications = group.map(_._2).filter(_.isDefined).map(_.get).toList
            if (!notifications.exists(n => fmt.print(n.expiryDate) == fmt.print(user.expirationDate))) {
              Some(Output.Account(user.id.get, user.name, user.phone.getOrElse(""), user.email, practice.name, user.expirationDate, fmt.print(user.expirationDate)))
            } else {
              None
            }
        }.toList

        data.filter(_.isDefined).map(_.get) match {
          case users if users.nonEmpty =>
            db.run {
              EmailMappingTable.filter(_.name === "expiryNotifications")
                .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                .result
            }.map { data =>
              val defaultAddresses = data.head._1._1.defaultEmail.getOrElse("").split(",").toList
              val addresses = data.filter(_._2.isDefined).map(_._2.get).map(user => s"${user.name} <${user.email}>").toList ::: defaultAddresses
			  //Console.println("addresses"+addresses); 
              emailService.sendEmail(addresses, from, replyTo, s"Expiring Accounts", views.html.accountExpiry(users, baseUrl))
              val actions = for (user <- users) yield {
                ExpiryNotificationTable += ExpiryNotificationRow(None, user.id, user.expiry, DateTime.now)
              }
              db.run(DBIO.sequence(actions))
            }
          case _ => ;
        }
      }

      // Subscribe product history
      var currentDate = LocalDate.now()
      var orderDate: LocalDate = null

      var block = db.run{
        SubscribeProductsTable.filterNot(_.unsubscribed).result
      } map { subscribeProducts =>
        subscribeProducts.foreach(result => {

          var frequency = result.frequency.filter(_.isDigit).toInt
          if(result.frequency contains ("month")){
              orderDate = result.lastOrderedDate.plusMonths(frequency)
          } else if(result.frequency contains ("week")){
              orderDate = result.lastOrderedDate.plusWeeks(frequency)
          }

    if(currentDate == orderDate){
    val apiLoginId = application.configuration.getString("aws.authorizeNet.apiLoginId").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiLoginId` property."))
    val transactionKey = application.configuration.getString("aws.authorizeNet.transactionKey").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.transactionKey` property."))
    val apiURL = application.configuration.getString("aws.authorizeNet.apiURL").getOrElse(throw new RuntimeException("Configuration is missing `aws.authorizeNet.apiURL` property."))

    var practiceName = ""
    var firstName = ""
    var lastName = ""
    var name: Option[String] = Some("")
    var company = ""
    var address = ""
    var city = ""
    var state = ""
    var zip = ""
    var country = ""
    var phone = ""
    var suite: Option[String] = Some("")
    var customerProfileId: Long = 0

   var block =  db.run{
          StaffTable.filter(_.id === result.staffId).result
        } map { staffMap =>
          staffMap.map { staffResult =>
            customerProfileId = convertOptionLong(staffResult.authorizeCustId)
            firstName = staffResult.name
            lastName = convertOptionString(staffResult.lastName)

    var practiceBlock =  db.run{
          PracticeTable.filter(_.id === staffResult.practiceId).result
        } map { practiceMap =>
          practiceMap.map { practiceResult =>
            practiceName = practiceResult.name
          }
        }
      var AwaitResult = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))


    if(result.shippingAddressId == 0){
      var practiceBlock =  db.run{
          PracticeTable.filter(_.id === staffResult.practiceId).result
        } map { practiceMap =>
          practiceMap.map { practiceResult =>
            name = Some(practiceResult.name)
            city = practiceResult.city
            state = practiceResult.state
            zip = practiceResult.zip
            country = practiceResult.country
            address = practiceResult.address
            phone = practiceResult.phone
            suite = practiceResult.suite
          }
        }
      var AwaitResult = Await.ready(practiceBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
  } else {
      var shippingBlock = db.run{
      ShippingTable.filter(_.id === result.shippingAddressId).result
    } map { shippingMap=>
      shippingMap.foreach(shippingResult => {
        name = shippingResult.name
        city = shippingResult.city
        state = shippingResult.state
        zip = shippingResult.zip
        country = shippingResult.country
        address = shippingResult.address
        phone = shippingResult.phone
        suite = shippingResult.suite
      })
    }
    var AwaitResult = Await.ready(shippingBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
    }
    }
  }
   var AwaitResult1 = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))


    var chargeProfileJson = """{ "createTransactionRequest": { "merchantAuthentication": { "name": "strApiLoginId", "transactionKey": "strTransactionkey" }, "transactionRequest": { "transactionType": "authCaptureTransaction", "amount": strGrandTotal, "profile": { "customerProfileId": strCustomerProfileId, "paymentProfile": { "paymentProfileId": strPaymentProfileId } },"order": {"description": "SubscribeOrder"},"shipTo": { "firstName": "strFirstName", "lastName": "strLastName", "company": "strCompany", "address": "strAddress", "city": "strCity", "state": "strState", "zip": strZip, "country": "strCountry" } } } }"""

    var loginIdReplace = chargeProfileJson.replace("strApiLoginId", apiLoginId)
    var transactionKeyReplace = loginIdReplace.replace("strTransactionkey", transactionKey)
    var customerProfileIdReplace = transactionKeyReplace.replace("strCustomerProfileId", customerProfileId.toString)
    var cityReplace = customerProfileIdReplace.replace("strCity", city)
    var stateReplace = cityReplace.replace("strState", state)
    var zipReplace = stateReplace.replace("strZip", zip)
    var countryReplace = zipReplace.replace("strCountry", country)
    var companyReplace = countryReplace.replace("strCompany", practiceName)
    var addressReplace = companyReplace.replace("strAddress", address)
    var grandTotalReplace = addressReplace.replace("strGrandTotal", result.grandTotal.toString)
    var paymentProfileIdReplace = grandTotalReplace.replace("strPaymentProfileId", result.customerPaymentProfileId.toString)
    var firstNameReplace = paymentProfileIdReplace.replace("strFirstName", firstName)
    var lastNameReplace = firstNameReplace.replace("strLastName", lastName)

    var finalJson = lastNameReplace

    val obj = Json.obj()
    val response = Http(apiURL).postData(finalJson)
    .header("Content-Type", "application/json")
    .header("Charset", "UTF-8")
    .option(HttpOptions.readTimeout(100000)).asString

    var s = response.body.trim().replaceFirst("\ufeff", "");
    val jsonObject: JsValue = Json.parse(s)

    val transactionResponse = (jsonObject \ ("transactionResponse")).as[JsValue]
    val responseCode = (transactionResponse \ ("responseCode")).as[String]
    val transId = (transactionResponse \ ("transId")).as[String]

    var description = ""
    if(responseCode == "1" || responseCode == "4"){
      val messages = (transactionResponse \ ("messages")).as[List[JsValue]]

      messages.foreach(result => {
        description = (result \ ("description")).as[String]
          })
            var orderAddress = OrderRow.Address(name, country, city, state, zip, address, phone,Some("Scheduler"),suite)

            var mckessonOrder = false
            var block = db.run{
                        VendorTable.filter(_.name === "McKesson").result
                    }map{  vendorList =>
                      vendorList.foreach(vendorResult => {
                        if(vendorResult.id == result.productId){
                          mckessonOrder = true
                        }
                      })
                    }
                    val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

                  var status: OrderRow.Status = OrderRow.Status.Fulfilled
                  var paymentCompleted = true
                    if(responseCode == "4"){
                      status = OrderRow.Status.Received
                      paymentCompleted = false
                    }

            var payment = OrderRow.Payment(Some("Credit Card"),Some(false),Some(customerProfileId),Some(result.customerPaymentProfileId),Some(transId.toLong),Some(responseCode),Some(paymentCompleted))

            var orderRow = OrderRow(None, status, DateTime.now, result.staffId, null, None, Some(orderAddress), Some(""),Some(result.shippingCharge),Some(result.salesTax),Option.empty[Float],Some(result.grandTotal),Some(result.totalAmountSaved),Some("Scheduler Job"),Some(false),Some(""),Some(""),Some(payment))

            var orderBlock = db.run {
                OrderTable.returning(OrderTable.map(_.id)) += orderRow
                } map {
                  case 0L => PreconditionFailed
                  case id =>

            var subscribeOrderHistoryRow = SubscribeOrderHistoryRow(None,convertOptionLong(result.id),LocalDate.now(),id,customerProfileId,result.customerPaymentProfileId,transId.toLong,responseCode, result.grandTotal)

            var historyblock = db.run {
            SubscribeOrderHistoryTable.returning(SubscribeOrderHistoryTable.map(_.id)) += subscribeOrderHistoryRow
             } map {
                  case 0L => PreconditionFailed
                  case id =>
                  }
            val AwaitResult = Await.ready(historyblock, atMost = scala.concurrent.duration.Duration(120, SECONDS))

             var block = db.run{
                ProductTable.filter(_.id === result.productId).join(CategoryTable).on(_.categoryId === _.id).join(VendorTable).on(_._1.vendorId  === _.id).result
              } map { productsMap =>
                val data = productsMap.groupBy(_._1._1.id).map {
                case (productId, entries) =>
                  val product = entries.head._1._1
                  val category = entries.head._1._2
                  val vendor = entries.head._2

                  var imageUrl = convertOptionString(product.imageUrl).split(',')(0)

                  var orderItemRow =  OrderItemRow((result.quantity).toInt,result.productId,id,Some(false),Some(false),Some(""),Some(""),None,Some(""),None,None,
                      OrderItemRow.Product(
                        product.name,
                        product.description.getOrElse(""),
                        product.serialNumber,
                        Some(imageUrl),
                        Some(product.vendorId),
                        vendor.name,
                        Some(OrderItemRow.Product.Category(
                        category.id.get,
                        category.name)),
                        result.productPrice,
                        product.quantity,
                        product.unit
                      ))

               var orderItemBlock = db.run {
                OrderItemTable.returning(OrderItemTable.map(_.amount)) += orderItemRow
                } map {

                  case 0L => PreconditionFailed
                  case id =>
                      var subscribeBlock = db.run {
                              SubscribeProductsTable.filter(_.id === result.id).map(_.lastOrderedDate).update(LocalDate.now())
                              } map {
                              case 0 => NotFound
                              case _ => Ok
                              }
                      val AwaitResult2 = Await.ready(subscribeBlock, atMost = scala.concurrent.duration.Duration(30, SECONDS))
                }
              // Email Portion
                var emailBlock = db.run{
                    OrderTable.filter(_.id === id)
                    .join(OrderItemTable).on(_.id === _.orderId)
                    .join(StaffTable).on(_._1.staffId === _.id)
                    .join(PracticeTable).on(_._2.practiceId === _.id)
                    .result
                } map { pairs => {
                  (id,result.staffId)

                  val order = OrdersController.Output.Orders.from(pairs).head
                  // val total = order.items.map { i => i.price * i.amount }.sum
                  // val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                  //   item => (item._2, item._2.map(i => i.price * i.amount).sum, item._1)
                  // }.toList.partition(_._3 == "NobelBiocare")
                  val total = order.items.map { i => f"${i.price}%.2f".toDouble * i.amount }.sum
                  val (nobel, other) = order.items.groupBy(_.manufacturer).map {
                    item => (item._2,item._2.map(i => f"${i.price}%.2f".toDouble * i.amount).sum , item._1)
                  }.toList.partition(_._3 == "NobelBiocare")
                  val items = nobel ::: other.sortWith(_._3.capitalize < _._3.capitalize)

                  var dentistName = order.dentist.name
                  var shippingCharge = convertOptionFloat(order.shippingCharge)
                  var tax = convertOptionFloat(order.tax)
                  var creditCardFee = convertOptionFloat(order.creditCardFee)
                  var totalAmountSaved = convertOptionFloat(order.totalAmountSaved)
                  var vendorNoteMap:scala.collection.mutable.Map[Option[Long],String] = scala.collection.mutable.Map()

                  // Send email to the customer
                  emailService.sendEmail(List(s"${dentistName} <${order.dentist.email}>"), orderFrom, orderReplyTo, orderSubject, views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, None, order.address, shippingCharge, tax, creditCardFee,totalAmountSaved," subscription","Subscription",vendorNoteMap))

                  // send to admins and assigned email addresses
                  db.run {
                    EmailMappingTable.filter(_.name === "order")
                      .joinLeft(UserEmailMappingTable).on(_.id === _.emailMappingId)
                      .joinLeft(UserTable).on(_._2.map(_.userId) === _.id)
                      .result
                  }.map { data =>
                    val defaultAddresses = data.head._1._1.defaultEmail.getOrElse("").split(",").toList
                    val addresses = data.filter(_._2.isDefined).map(_._2.get).map(user => s"${user.name} <${user.email}>").toList ::: defaultAddresses

                    emailService.sendEmail(addresses, orderFrom, orderReplyTo, s"New Order(id: ${order.id}) - ${dentistName}", views.html.orderConfirmation(dentistName, order.id, total, order.dentist, items, Some(s""), order.address, shippingCharge, tax, creditCardFee,totalAmountSaved," subscription","Subscription",vendorNoteMap))
                  }

                  // Send email to the vendor
                  db.run {
                    VendorTable.result
                  } map { vendors =>
                      items.foreach(productGroup => {
                          val vendor = vendors.find(_.id == productGroup._1.head.vendorId)
                          if (vendor.isDefined && vendor.get.email.isDefined) {
                            emailService.sendEmail(List(vendor.get.email.get), from, replyTo, s"New Order(id: ${order.id}) - Novadontics", views.html.vendorOrder(total, order.dentist, productGroup, order.address, vendorNoteMap))
                          }
                        })
                    }
                 }
                }
                val AwaitResult2 = Await.ready(orderItemBlock, atMost = scala.concurrent.duration.Duration(60, SECONDS))
              }
              }
             }
             val AwaitResult2 = Await.ready(orderBlock, atMost = scala.concurrent.duration.Duration(120, SECONDS))
        } else {
            var subscribeOrderFailureHistoryRow = SubscribeOrderFailureHistoryRow(None,convertOptionLong(result.id),LocalDate.now(),customerProfileId,result.customerPaymentProfileId,transId.toLong,responseCode, result.grandTotal)

            var historyFailureblock = db.run {
            SubscribeOrderFailureHistoryTable.returning(SubscribeOrderFailureHistoryTable.map(_.id)) += subscribeOrderFailureHistoryRow
             } map {
                  case 0L => PreconditionFailed
                  case id => Created(Json.toJson(subscribeOrderFailureHistoryRow.copy(id = Some(id))))
                  }
            val AwaitResult = Await.ready(historyFailureblock, atMost = scala.concurrent.duration.Duration(90, SECONDS))
        }
      }
    })
  }
      val AwaitResult = Await.ready(block, atMost = scala.concurrent.duration.Duration(30, SECONDS))

      // SETTING ACCOUNT TYPE FOR EXPIRED ACCOUNTS
      var yesterdayDate = DateTime.now().minusDays(1)
      db.run{
        StaffTable.filter(_.expirationDate <= yesterdayDate).map(_.accountType).update("Lead")
      }
  }

}
