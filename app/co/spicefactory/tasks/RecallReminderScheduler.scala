package co.spicefactory.tasks

import javax.inject.{Inject, Named}
import akka.actor.{ActorRef, ActorSystem, Cancellable}
import play.api.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class RecallReminderScheduler @Inject() (val system: ActorSystem, @Named("scheduler-recallReminder") val schedulerActor: ActorRef, configuration: Configuration)(implicit ec: ExecutionContext) {
  val frequency: Int = 10

  var actor: Cancellable = system.scheduler.schedule(
    0.microseconds, frequency.hours, schedulerActor, "update")

}