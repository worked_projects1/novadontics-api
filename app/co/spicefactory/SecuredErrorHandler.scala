package co.spicefactory

import com.google.inject._
import com.mohiva.play.silhouette.api.actions.{SecuredErrorHandler => SilhouetteSecuredErrorHandler}
import play.api.Application
import play.api.i18n.Messages
import play.api.i18n.Messages.Implicits._
import play.api.libs.json.Json
import play.api.mvc.{RequestHeader, Result, Results}

import scala.concurrent.Future

@Singleton
class SecuredErrorHandler @Inject() (implicit application: Application) extends SilhouetteSecuredErrorHandler with Results {

  override def onNotAuthorized(implicit request: RequestHeader): Future[Result] = {
    Future.successful(Unauthorized(Json.obj("message" -> Messages("permissionsMissing"))))
  }

  override def onNotAuthenticated(implicit request: RequestHeader): Future[Result] = {
    val authToken: Option[String] = request.headers.get("X-Auth-Token")
    val message = authToken.fold("session.authTokenMissing")(_ => "session.authTokenIncorrect")
    Future.successful(Unauthorized(Json.obj("message" -> Messages(message))))
  }

}
