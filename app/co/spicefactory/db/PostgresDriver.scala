package co.spicefactory.db

import com.github.tminglei.slickpg._
import play.api.libs.json.{JsValue, Json}
import slick.driver.JdbcProfile
import slick.profile.Capability

trait PostgresDriver extends ExPostgresDriver
                     with PgArraySupport
                     with PgRangeSupport
                     with PgHStoreSupport
                     with PgPlayJsonSupport
                     with PgDateSupportJoda
                     with PgSearchSupport
                     with PgNetSupport
                     with PgLTreeSupport {

  def pgjson = "jsonb"

  override protected def computeCapabilities: Set[Capability] =
    super.computeCapabilities + JdbcProfile.capabilities.insertOrUpdate

  override val api = MyAPI

  object MyAPI extends API
               with ArrayImplicits
               with JsonImplicits
               with NetImplicits
               with LTreeImplicits
               with RangeImplicits
               with HStoreImplicits
               with SearchImplicits
               with SearchAssistants {

    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
    implicit val playJsonArrayTypeMapper =
      new AdvancedArrayJdbcType[JsValue](pgjson,
        (s) => utils.SimpleArrayUtils.fromString[JsValue](Json.parse)(s).orNull,
        (v) => utils.SimpleArrayUtils.mkString[JsValue](_.toString())(v)
      ).to(_.toList)

  }

}

object PostgresDriver extends PostgresDriver
