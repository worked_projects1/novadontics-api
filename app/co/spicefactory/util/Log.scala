package co.spicefactory.util

import play.api.Logger

/**
 * Provides logger for classes which mix in this trait.
 */
trait Log { self =>

  protected val log = Logger(self.getClass)

}
