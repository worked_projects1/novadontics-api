package co.spicefactory.util

import scala.annotation.tailrec
import scala.util.Random

abstract class Arbitrary[+A] {
  def generate: A
}


final class ArbitraryValue[+A](a: => A) extends Arbitrary[A] {
  def generate = a
}

final class ArbitraryOneOf[+A](a: Arbitrary[A]*) extends Arbitrary[A] {
  def generate = a(Random.nextInt(a.length)).generate
}

final class ArbitraryList[+A](e: Int)(implicit a: Arbitrary[A]) extends Arbitrary[List[A]] {
  override def generate = {
    val n = Math.round(- e * Math.log(Random.nextDouble))

    @tailrec
    def loop(i: Int = 0, list: List[A] = Nil): List[A] = {
      if (i < n) {
        loop(i + 1, a.generate :: list)
      } else {
        list
      }
    }

    loop()
  }
}

final class ArbitraryOption[+A](p: Double)(implicit a: Arbitrary[A]) extends Arbitrary[Option[A]] {
  override def generate = {
    if (Random.nextDouble < p) {
      Some(a.generate)
    } else {
      None
    }
  }
}

final class ArbitraryUniqueOption[A](p: Double)(a: Arbitrary[A]*) extends Arbitrary[Option[A]] {
  private var values = a

  override def generate = {
    if (values.nonEmpty && Random.nextDouble < p) {
      val shuffled = Random.shuffle(values)
      val head = shuffled.head
      val tail = shuffled.tail

      values = tail

      Some(head.generate)
    } else {
      None
    }
  }
}

final class ArbitrarySubset[+A](a: Arbitrary[A]*) extends Arbitrary[List[A]] {
  def generate = {
    val n: Int = Math.floor((a.length - 1) / 2.0).toInt + 1

    Random.shuffle(a).take(n).map(_.generate).toList
  }
}

final class LazyArbitrary[A] extends Arbitrary[Option[A]] {
  private var underlying: Option[Arbitrary[Option[A]]] = None

  def set(a: Option[Arbitrary[Option[A]]]) = underlying = a

  override def generate: Option[A] = underlying.flatMap(_.generate)
}

object Arbitrary {

  def apply[A](a: => A): Arbitrary[A] = new ArbitraryValue(a)

  def of[A](a: Arbitrary[A]*): Arbitrary[A] = new ArbitraryOneOf(a: _*)

  def list[A](e: Int = 4)(implicit a: Arbitrary[A]): Arbitrary[List[A]] = new ArbitraryList[A](e)(a)

  def option[A](p: Double = 0.5)(implicit a: Arbitrary[A]): Arbitrary[Option[A]] = new ArbitraryOption[A](p)(a)

  def uniqueOption[A](p: Double = 0.5)(a: Arbitrary[A]*): Arbitrary[Option[A]] = new ArbitraryUniqueOption[A](p)(a: _*)

  def subset[A](a: Arbitrary[A]*): Arbitrary[List[A]] = new ArbitrarySubset(a: _*)

  def `lazy`[A] = new LazyArbitrary[A]

  implicit def implicitArbitrary[A](a: => A): Arbitrary[A] = apply(a)

}
