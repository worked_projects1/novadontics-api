package co.spicefactory

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Awaitable}

package object util {

  def await[T](awaitable: Awaitable[T])
              (implicit duration: Duration = Duration(4, TimeUnit.SECONDS)) =
    Await.result[T](awaitable, duration)

}
