package co.spicefactory.util

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.BearerTokenAuthenticator
import models.daos.tables.IdentityRow

trait BearerTokenEnv extends Env {
  type I = IdentityRow
  type A = BearerTokenAuthenticator
}
