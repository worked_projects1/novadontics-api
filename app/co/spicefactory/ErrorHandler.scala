package co.spicefactory

import com.google.inject._
import play.api.http.DefaultHttpErrorHandler
import play.api.libs.json.Json._
import play.api.mvc._
import play.api.routing.Router
import play.api.{Configuration, Environment, OptionalSourceMapper, UsefulException}

import scala.concurrent.Future

@Singleton
class ErrorHandler @Inject() (
  environment: Environment,
  configuration: Configuration,
  sourceMapper: OptionalSourceMapper,
  router: Provider[Router])
  extends DefaultHttpErrorHandler(environment, configuration, sourceMapper, router)
  with Rendering with AcceptExtractors with Results {

  override protected def onDevServerError(request: RequestHeader, exception: UsefulException): Future[Result] =
    respond(request, super.onDevServerError(request, exception)) {
      Future.successful {
        InternalServerError(obj("message" -> exception.getMessage))
      }
    }

  override protected def onProdServerError(request: RequestHeader, exception: UsefulException): Future[Result] =
    respond(request, super.onProdServerError(request, exception)) {
      Future.successful {
        InternalServerError
      }
    }

  override protected def onBadRequest(request: RequestHeader, message: String): Future[Result] =
    respond(request, super.onBadRequest(request, message)) {
      Future.successful {
        BadRequest
      }
    }

  override protected def onForbidden(request: RequestHeader, message: String): Future[Result] =
    respond(request, super.onForbidden(request, message)) {
      Future.successful {
        Forbidden
      }
    }

  override protected def onNotFound(request: RequestHeader, message: String): Future[Result] =
    respond(request, super.onNotFound(request, message)) {
      Future.successful {
        NotFound
      }
    }

  override protected def onOtherClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] =
    respond(request, super.onOtherClientError(request, statusCode, message)) {
      Future.successful {
        Results.Status(statusCode)
      }
    }

  private def respond(request: RequestHeader, htmlResult: => Future[Result])(otherResult: => Future[Result]) = {
    implicit val rh = request

    render.async {
      case Accepts.Html() => htmlResult
      case _ => otherResult
    }
  }

}
