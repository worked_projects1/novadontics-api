package co.spicefactory.modules

import co.spicefactory.tasks.{RecallReminderService, RecallReminderScheduler}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class RecallReminderModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[RecallReminderService]("scheduler-recallReminder")
    bind(classOf[RecallReminderScheduler]).asEagerSingleton()
  }
}