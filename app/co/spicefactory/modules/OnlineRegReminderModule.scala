package co.spicefactory.modules

import co.spicefactory.tasks.{OnlineRegReminderService, OnlineRegReminderScheduler}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class OnlineRegReminderModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[OnlineRegReminderService]("scheduler-onlineReg")
    bind(classOf[OnlineRegReminderScheduler]).asEagerSingleton()
  }
}

