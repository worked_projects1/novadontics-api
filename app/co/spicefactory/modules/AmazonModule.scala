package co.spicefactory.modules

import com.amazonaws.auth._
import com.amazonaws.services.s3.{AmazonS3, AmazonS3Client, S3ClientOptions}
import com.amazonaws.services.simpleemail.{AmazonSimpleEmailService, AmazonSimpleEmailServiceClient}
import com.amazonaws.services.sqs.{AmazonSQS, AmazonSQSAsync, AmazonSQSAsyncClient, AmazonSQSClient}
import com.google.inject._
import net.codingwell.scalaguice.ScalaModule
import play.api.Configuration
import play.api.libs.concurrent.AkkaGuiceSupport

class AmazonModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {

  def configure(): Unit = {
  }

  @Provides
  def provideAmazonSES(credentialsProvider: AWSCredentialsProvider): AmazonSimpleEmailService = new AmazonSimpleEmailServiceClient(credentialsProvider)

  @Provides
  def provideAmazonS3(credentialsProvider: AWSCredentialsProvider): AmazonS3 = {
    val client = new AmazonS3Client(credentialsProvider)

    client.setS3ClientOptions {
      S3ClientOptions.builder().setPathStyleAccess(true).build()
    }

    client
  }

  @Provides
  def provideAmazonSQS(credentialsProvider: AWSCredentialsProvider): AmazonSQS = new AmazonSQSClient(credentialsProvider)

  @Provides
  def provideAmazonSQSAsync(credentialsProvider: AWSCredentialsProvider): AmazonSQSAsync = new AmazonSQSAsyncClient(credentialsProvider)

  @Provides
  def provideCredentialsProvider(credentials: AWSCredentials): AWSCredentialsProvider = new AWSCredentialsProviderChain(
    new EnvironmentVariableCredentialsProvider(),
    new AWSStaticCredentialsProvider(credentials)
  )

  @Provides
  def provideAWSCredentials(configuration: Configuration): AWSCredentials = new BasicAWSCredentials(
    configuration.getString("aws.accessKey").getOrElse(throw new RuntimeException("Configuration is missing `aws.accessKey` property.")),
    configuration.getString("aws.secretKey").getOrElse(throw new RuntimeException("Configuration is missing `aws.secretKey` property."))
  )

}
