package co.spicefactory.modules

import co.spicefactory.tasks.{AppointmentService, AppointmentScheduler}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class AppointmentModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[AppointmentService]("scheduler-appointment")
    bind(classOf[AppointmentScheduler]).asEagerSingleton()
  }
}
