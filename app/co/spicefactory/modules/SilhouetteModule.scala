package co.spicefactory.modules

import co.spicefactory.SecuredErrorHandler
import co.spicefactory.services.UserIdentityService
import co.spicefactory.util.BearerTokenEnv
import com.google.inject._
import com.mohiva.play.silhouette.api.actions.{SecuredErrorHandler => SilhouetteSecuredErrorHandler}
import com.mohiva.play.silhouette.api.repositories.{AuthInfoRepository, AuthenticatorRepository}
import com.mohiva.play.silhouette.api.services.AuthenticatorService
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.{Environment, EventBus, Silhouette, SilhouetteProvider}
import com.mohiva.play.silhouette.impl.authenticators.{BearerTokenAuthenticator, BearerTokenAuthenticatorService, BearerTokenAuthenticatorSettings}
import com.mohiva.play.silhouette.impl.util._
import com.mohiva.play.silhouette.password.BCryptPasswordHasher
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import com.mohiva.play.silhouette.persistence.repositories.DelegableAuthInfoRepository
import models.daos.silhouette.{DefaultAuthenticatorRepository, PasswordInfoDAO}
import net.codingwell.scalaguice.ScalaModule
import play.api.Configuration

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext

class SilhouetteModule extends AbstractModule with ScalaModule {

  def configure(): Unit = {
    bind[DelegableAuthInfoDAO[PasswordInfo]].to[PasswordInfoDAO]
    bind[AuthenticatorRepository[BearerTokenAuthenticator]].to[DefaultAuthenticatorRepository]
    bind[PasswordHasher].toInstance(new BCryptPasswordHasher())
    bind[FingerprintGenerator].toInstance(new DefaultFingerprintGenerator(false))
    bind[EventBus].toInstance(EventBus())
    bind[Clock].toInstance(Clock())
    bind[Silhouette[BearerTokenEnv]].to[SilhouetteProvider[BearerTokenEnv]].in[Singleton]
    bind[SilhouetteSecuredErrorHandler].to[SecuredErrorHandler].in[Singleton]
  }

  @Provides
  def provideEnvironment(userService: UserIdentityService,
                         authenticatorService: AuthenticatorService[BearerTokenAuthenticator],
                         eventBus: EventBus)(implicit ec: ExecutionContext): Environment[BearerTokenEnv] = {
    Environment[BearerTokenEnv](
      userService,
      authenticatorService,
      Seq(),
      eventBus
    )
  }

  @Provides
  def provideAuthenticatorService(configuration: Configuration,
                                  repository: AuthenticatorRepository[BearerTokenAuthenticator],
                                  idGenerator: IDGenerator,
                                  clock: Clock)(implicit ec: ExecutionContext): AuthenticatorService[BearerTokenAuthenticator] = {
    new BearerTokenAuthenticatorService(BearerTokenAuthenticatorSettings(requestParts = Some(Seq(RequestPart.QueryString, RequestPart.Headers))), repository, idGenerator, clock)
  }

  @Provides
  def providePasswordHasherRegistry(passwordHasher: PasswordHasher): PasswordHasherRegistry = PasswordHasherRegistry(passwordHasher)

  @Provides @Singleton
  def provideIDGenerator(implicit ec: ExecutionContext): IDGenerator = new SecureRandomIDGenerator()

  @Provides @Singleton
  def provideAuthInfoRepository(passwordInfoDAO: DelegableAuthInfoDAO[PasswordInfo])(implicit ec: ExecutionContext): AuthInfoRepository = new DelegableAuthInfoRepository(passwordInfoDAO)

  @Provides @Singleton
  def providePasswordHashers(passwordHasher: PasswordHasher): scala.collection.Seq[PasswordHasher] = Seq(passwordHasher)

}
