package co.spicefactory.modules

import co.spicefactory.tasks.{MckessonService, MckessonScheduler}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class MckessonModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[MckessonService]("scheduler-actor1")
    bind(classOf[MckessonScheduler]).asEagerSingleton()
  }
}
