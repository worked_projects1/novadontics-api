package co.spicefactory.modules

import java.util.concurrent.{ExecutorService, Executors}

import com.google.inject._
import com.google.inject.name.Named
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class ThreadPoolModule extends AbstractModule with ScalaModule {

  def configure(): Unit = {
  }

  @Provides @Named("ioBound")
  def provideIoBoundExecutorService(): ExecutorService = Executors.newFixedThreadPool(8)

  @Provides @Named("ioBound")
  def provideIoBoundExecutionContext(@Named("ioBound") executorService: ExecutorService): ExecutionContext = ExecutionContext.fromExecutorService(executorService)

  @Provides @Named("cpuBound")
  def provideCpuBoundExecutorService(): ExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors)

  @Provides @Named("cpuBound")
  def provideCpuBoundExecutionContext(@Named("cpuBound") executorService: ExecutorService): ExecutionContext = ExecutionContext.fromExecutorService(executorService)

  @Provides @Named("blocking")
  def provideBlockingExecutionContext(@Named("ioBound") executorService: ExecutorService): ExecutionContext = ExecutionContext.fromExecutorService(executorService)

}
