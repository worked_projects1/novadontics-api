package co.spicefactory.modules

import co.spicefactory.tasks.{AccountNotification, Scheduler}
import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport

class AccountNotificationModule extends AbstractModule with AkkaGuiceSupport {
  def configure() = {
    bindActor[AccountNotification]("scheduler-actor")
    bind(classOf[Scheduler]).asEagerSingleton()
  }
}