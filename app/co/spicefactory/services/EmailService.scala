package co.spicefactory.services

import com.amazonaws.regions.{Region, Regions}
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model._
import com.google.inject.Inject
import play.api.Application
import play.twirl.api.Html
import scala.collection.JavaConversions._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class EmailService @Inject()(ses: AmazonSimpleEmailService, val application: Application) {
  private val sesArn = application.configuration.getString("aws.ses.arn").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  private val sesRegion = application.configuration.getString("aws.ses.region").getOrElse(throw new RuntimeException("Configuration is missing `aws.ses.arn` property."))
  ses.setRegion(Region.getRegion(Regions.fromName(sesRegion)))

  def sendEmail(addresses: List[String], from: String, replyTo: String, subject: String, template: Html) = {
    Future(
      ses.sendEmail(
        new SendEmailRequest()
          .withSourceArn(sesArn)
          .withSource(from)
          .withReplyToAddresses(replyTo)
          .withDestination(new Destination().withToAddresses(addresses))
          .withMessage {
            new Message()
              .withSubject(new Content().withData(subject))
              .withBody(new Body().withHtml(new Content().withData(template.body.trim())))
          }
      )
    )
  }

  def sendReminder(addresses: List[String], from: String, replyTo: String, subject: String, content: String) {
    Future(
      ses.sendEmail(
        new SendEmailRequest()
          .withSourceArn(sesArn)
          .withSource(from)
          .withReplyToAddresses(replyTo)
          .withDestination(new Destination().withToAddresses(addresses))
          .withMessage {
            new Message()
              .withSubject(new Content().withData(subject))
              .withBody(new Body().withText(new Content(content)))
          }
      )
    )
  }
}
