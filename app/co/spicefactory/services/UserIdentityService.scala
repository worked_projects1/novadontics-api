package co.spicefactory.services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import models.daos.DatabaseAccess
import models.daos.tables.IdentityRow
import play.api.Application

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[DefaultUserIdentityService])
trait UserIdentityService extends IdentityService[IdentityRow] {

  def retrieve(l: LoginInfo): Future[Option[IdentityRow]]

}

@Singleton
class DefaultUserIdentityService @Inject() (
  val application: Application)(implicit ec: ExecutionContext) extends UserIdentityService with DatabaseAccess {

  import driver.api._

  def retrieve(l: LoginInfo): Future[Option[IdentityRow]] = l match {
    case LoginInfo("credentials", email) =>
      db.run {
        UserTable.filter(_.email === email).filterNot(_.archived).take(1).result.headOption.flatMap[Option[IdentityRow], NoStream, Nothing] {
          case result @ Some(_) =>
            DBIO.successful(result)
          case None =>
            StaffTable.filter(_.email === email).filterNot(_.archived).take(1).result.headOption
        }
      }
    case _ =>
      Future.successful(None)
  }

}
