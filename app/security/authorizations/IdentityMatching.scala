package security.authorizations

import com.mohiva.play.silhouette.api.Authorization
import com.mohiva.play.silhouette.impl.authenticators.BearerTokenAuthenticator
import models.daos.tables.IdentityRow
import play.api.mvc.Request

import scala.concurrent.Future

case class IdentityMatching(f: IdentityRow => Boolean) extends Authorization[IdentityRow, BearerTokenAuthenticator] {

  override def isAuthorized[B](identity: IdentityRow, authenticator: BearerTokenAuthenticator)(implicit request: Request[B]) = Future.successful {
    f(identity)
  }

}
