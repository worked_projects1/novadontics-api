package models

import com.github.nscala_time.time.Imports._

case class Token(token: String, expiresOn: DateTime)
