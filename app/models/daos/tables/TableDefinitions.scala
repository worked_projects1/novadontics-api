
package models.daos.tables
import org.joda.time.{ LocalTime, LocalDate }

import co.spicefactory.db.PostgresDriver
import com.github.nscala_time.time.Imports
import com.github.nscala_time.time.Imports.DateTime
import com.mohiva.play.silhouette.api.Identity
import models.daos.DatabaseAccess
import models.daos.tables.CourseRow.EducationType
import models.forms.Slide
import play.api.libs.json.{JsValue, Json}
import slick.driver.H2Driver.api._
import java.util.Date;


sealed trait IdentityRow extends Identity {
  def id: Option[Long]
  def name: String
  def email: String
  def password: String
}

case class UserRow(
  id: Option[Long],
  name: String,
  email: String,
  password: String,
  phone: String,
  dateCreated: DateTime,
  role: UserRow.Role
) extends IdentityRow

object UserRow {
  sealed trait Role
  object Role {
    case object Admin extends Role
    case object NovadonticsStaff extends Role
    case object Unknown extends Role

    trait ColumnType {self: DatabaseAccess =>
      import driver.api._

      implicit val userRoleColumnType = MappedColumnType.base[UserRow.Role, String](
        {
          case UserRow.Role.Admin => "Admin"
          case UserRow.Role.NovadonticsStaff => "NovadonticsStaff"
          case UserRow.Role.Unknown => "Unknown"
        },
        {
          case "Admin" => UserRow.Role.Admin
          case "NovadonticsStaff" => UserRow.Role.NovadonticsStaff
          case _ => UserRow.Role.Unknown
        }
      )
    }
  }
}

case class StaffRow(
  id: Option[Long],
  name: String,
  email: String,
  password: String,
  role: StaffRow.Role,
  practiceId: Long,
  dateCreated: DateTime,
  udid: Option[String],
  udid1: Option[String],
  canAccessWeb: Boolean,
  deaNumber: Option[String],
  npiNumber: Option[String],
  phone: Option[String],
  country: Option[String],
  city: Option[String],
  state: Option[String],
  zip: Option[String],
  address: Option[String],
  expirationDate: DateTime,
  plan: Option[Long],
  accountType: String,
  title: String,
  lastName: Option[String],
  marketingSource: Option[String],
  notes: Option[String],
  subscriptionPeriod: Option[Int],
  cellPhone: Option[String],
  ciiPrograms: Option[String],
  appSquares: Option[String],
  custToLeadFlag: Option[Boolean],
  photo: Option[String],
  amountCollected: Option[Long],
  collectedDate: Option[DateTime],
  providersFilter: Option[String],
  webModules: Option[String],
  prime: Option[Boolean],
  authorizeCustId: Option[Long],
  allowAllCECourses: Option[Boolean],
  udid2: Option[String],
  mckessonId: Option[Long],
  agreementAccepted: Option[Boolean],
  contract: Option[String],
  suiteNumber: Option[String],
  dentalLicense: Option[String],
  doseSpotUserId: Option[String],
  icoiMember: Option[String],
  dateOfBirth: Option[LocalDate],
  primaryFax: Option[String],
  clinicianRoleType: Option[String],
  accessStartDate: Option[DateTime],
  ceInfo: Boolean
) extends IdentityRow

object StaffRow {
  sealed trait Role
  object Role {
    case object Dentist extends Role
    case object LabTechnician extends Role
    case object Receptionist extends Role
    case object Student extends Role
    case object MasterStudent extends Role
    case object DentalAssistant extends Role
    case object ContinuingEd extends Role
    case object Unknown extends Role

    trait ColumnType {self: DatabaseAccess =>
      import driver.api._

      implicit val staffRoleColumnType = MappedColumnType.base[StaffRow.Role, String](
        {
          case StaffRow.Role.Dentist => "Dentist"
          case StaffRow.Role.LabTechnician => "LabTechnician"
          case StaffRow.Role.Receptionist => "Receptionist"
          case StaffRow.Role.Student => "Student"
          case StaffRow.Role.MasterStudent => "MasterStudent"
          case StaffRow.Role.DentalAssistant => "DentalAssistant"
          case StaffRow.Role.ContinuingEd => "ContinuingEd"
          case StaffRow.Role.Unknown => "Unknown"
        },
        {
          case "Dentist" => StaffRow.Role.Dentist
          case "LabTechnician" => StaffRow.Role.LabTechnician
          case "Receptionist" => StaffRow.Role.Receptionist
          case "Student" => StaffRow.Role.Student
          case "MasterStudent" => StaffRow.Role.MasterStudent
          case "DentalAssistant" => StaffRow.Role.DentalAssistant
          case "ContinuingEd" => StaffRow.Role.ContinuingEd
          case _ => StaffRow.Role.Unknown
        }
      )
    }
  }
}

case class PracticeRow(
  id: Option[Long],
  name: String,
  country: String,
  city: String,
  state: String,
  zip: String,
  address: String,
  phone: String,
  dateCreated: DateTime,
  xvWebURL: Option[String],
  xvWebUserName: Option[String],
  xvWebPassword: Option[String],
  shortId: Option[String],
  timeZone: Option[String],
  phoneExt: Option[String],
  phone1: Option[String],
  phoneExt1: Option[String],
  fax: Option[String],
  email: Option[String],
  taxId: Option[Long],
  insBillingProvider: Option[String],
  billingLicense: Option[String],
  openingDate: Option[LocalDate],
  officeGroup: Option[String],
  ucrFeeSchedule: Option[String],
  feeSchedule: Option[String],
  officeId: Option[String],
  doseSpotClinicId: Option[String],
  doseSpotClinicKey: Option[String],
  chartNumberType: Option[String],
  chartNumberSeq: Option[Long],
  suite: Option[String],
  appointmentReminder: Option[Boolean],
  appReminderSetting: Option[String],
  onlineRegReminderFrequency: Option[String],
  onlineRegMaxReminder: Option[String],
  onlineRegistrationModule: Option[Boolean],
  onlineInviteMessage: Option[String],
  appointmentReminderMessage: Option[String],
  consentFormMessage: Option[String],
  amenities: Option[String],
  photos: Option[String],
  latitude: Option[Double],
  longitude: Option[Double],
  clinicDescription: Option[String],
  trojanAccountNo: Option[Long],
  postopInstructionsMessage: Option[String],
  recallReminderMessage: Option[String]
)

case class ShippingRow(
  id: Option[Long],
  name: Option[String],
  country: String,
  city: String,
  state: String,
  zip: String,
  address: String,
  phone: String,
  primary: Boolean,
  dentist: Option[Long],
  suite: Option[String]
)

case class TreatmentRow( 
  id: Option[Long],
  patientId: String,
  firstVisit: Option[DateTime],
  lastEntry: Option[DateTime],
  phoneNumber: String,
  emailAddress: String,
  staffId: Long,
  ctScanFile: Option[String],
  contracts: Option[String],
  xray: Option[String],
  scratchPad: Option[String],
  scratchPadText: Option[String],
  toothChartUpdatedDate: Option[DateTime],
  referralSource : Option[String],
  treatmentStarted : Boolean,
  recallPeriod : Option[String],
  patientType : Option[String],
  patientStatus: Option[String],
  shareToAdmin: Option[Boolean],
  mainSquares: Option[String],
  icons: Option[String],
  sharedDate: Option[LocalDate],
  sharedBy: Option[Long],
  referralSourceId: Option[Long],
  insurance: String,
  recallType: Option[String],
  recallCode: Option[String],
  recallCodeDescription: Option[String],
  intervalNumber: Option[Long],
  intervalUnit: Option[String],
  recallFromDate: Option[LocalDate],
  recallOneDayPlus: Option[Boolean],
  recallDueDate: Option[LocalDate],
  recallNote: Option[String],
  doseSpotPatientId: Option[String],
  firstName: Option[String],
  lastName: Option[String],
  dateOfBirth: Option[String],
  providerId: Option[Long],
  practiceId: Option[Long],
  isFamily: Option[Boolean],
  isMedicalCondition: Option[Boolean],
  alerts: Option[String],
  examImaging: Option[String],
  referredTo: Option[Long],
  referredDate: Option[DateTime],
  missingTooth: Option[String],
  edentulousMaxilla: Option[Boolean],
  edentulousMandible: Option[Boolean],
  allPrimary: Option[Boolean],
  primaryTooth: Option[String],
  lastInvitationSent: Option[DateTime],
  inviteMaxLimitReached: Option[Boolean],
  completedBy: Option[String],
  completedDate: Option[DateTime],
  onlineRegistration: Option[String],
  onlineRegistrationStatus: Option[String],
  country: Option[String],
  recallReminderSentDate: Option[LocalDate]
)

case class FormRow(
  id: String,
  version: Int,
  section: String,
  title: String,
  icon: String,
  schema: JsValue,
  print: Boolean,
  listOrder : Long,
  types : String)

object FormRow {
  def apply(id: String, version: Int, section: String, title: String, icon: String)(slides: Slide*) (listOrder : Long, types : String) : FormRow = apply(id, version, section, title, icon, Json.toJson(slides.toList), false,listOrder,types)
}

case class StepRow(
  value: JsValue,
  state: StepRow.State,
  treatmentId: Long,
  formId: String,
  formVersion: Int,
  dateCreated: DateTime
)

object StepRow {
  sealed trait State
  object State {
    case object ToDo extends State
    case object InProgress extends State
    case object Completed extends State
    case object Sealed extends State

    trait ColumnType { self: DatabaseAccess =>
      import driver.api._

      implicit val stepStateColumnType = MappedColumnType.base[StepRow.State, String](
        {
          case StepRow.State.ToDo => "ToDo"
          case StepRow.State.InProgress => "InProgress"
          case StepRow.State.Completed => "Completed"
          case StepRow.State.Sealed => "Sealed"
        },
        {
          case "InProgress" => StepRow.State.InProgress
          case "Completed" => StepRow.State.Completed
          case "Sealed" => StepRow.State.Sealed
          case _ => StepRow.State.ToDo
        }
      )
    }
  }
}

case class VendorRow(
  id: Option[Long],
  name: String,
  email: Option[String],
  logoUrl: Option[String],
  deleted: Boolean,
  shippingChargeLimit: Option[Double],
  shippingCharge: Option[Double],
  tax: Option[Double],
  creditCardFee: Option[Double],
  group: Option[String],
  category : Option[String],
  image: Option[String],
  groups: Option[JsValue]
)

case class CategoryRow(
  id: Option[Long],
  name: String,
  taxExempted: Option[Boolean],
  parent: Option[Long],
  order: Int
)

case class ProductRow(
  id: Option[Long],
  name: String,
  description: Option[String],
  serialNumber: String,
  imageUrl: Option[String],
  vendorId: Long,
  categoryId: Option[Long],
  price: Double,
  quantity: Option[Long],
  listPrice: Option[Double],
  document : Option[String],
  video : Option[JsValue],
  stockAvailable: Option[Boolean],
  unit: Option[String],
  financeEligible: Option[Boolean]
)

case class OrderRow(
  id: Option[Long],
  status: OrderRow.Status,
  dateCreated: DateTime,
  staffId: Long,
  dateFulfilled: Option[DateTime],
  approvedBy: Option[String],
  address: Option[OrderRow.Address],
  // paymentType: Option[String],
  checkNumber: Option[String],
  // allVendorsPaid: Option[Boolean],
  shippingCharge: Option[Float],
  tax: Option[Float],
  creditCardFee: Option[Float],
  grandTotal: Option[Float],
  totalAmountSaved: Option[Float],
  // customerProfileID: Option[Long],
  // customerPaymentProfileId: Option[Long],
  // transId: Option[Long],
  platform: Option[String],
  // transactionCode: Option[String],
  mckessonOrder: Option[Boolean],
  mckessonStatus: Option[String],
  note: Option[String],
  payment: Option[OrderRow.Payment]

)

object OrderRow {
  case class Address(
    name: Option[String],
    country: String,
    city: String,
    state: String,
    zip: String,
    address: String,
    phone: String,
    device: Option[String],
    suite: Option[String]
  )
  object Address {
    implicit val writes = Json.writes[OrderRow.Address]
  }

  case class Payment(
    paymentType: Option[String],
    allVendorsPaid: Option[Boolean],
    customerProfileID: Option[Long],
    customerPaymentProfileId: Option[Long],
    transId: Option[Long],
    transactionCode: Option[String],
    paymentCompleted: Option[Boolean]
  )
  object Payment {
    implicit val writes = Json.writes[OrderRow.Payment]
  }

  sealed trait Status
  object Status {
    case object Unknown extends Status
    case object Received extends Status
    case object Fulfilled extends Status
    case object Canceled extends Status
    case object Void extends Status
    case object Returned extends Status

    trait ColumnType { self: DatabaseAccess =>
      import driver.api._

      implicit val orderStatusColumnType = MappedColumnType.base[OrderRow.Status, String](
        {
          case OrderRow.Status.Unknown => "Unknown"
          case OrderRow.Status.Received => "Received"
          case OrderRow.Status.Fulfilled => "Fulfilled"
          case OrderRow.Status.Canceled => "Canceled"
          case OrderRow.Status.Void => "Void"
          case OrderRow.Status.Returned => "Returned"
        },
        {
          case "Received" => OrderRow.Status.Received
          case "Fulfilled" => OrderRow.Status.Fulfilled
          case "Canceled" => OrderRow.Status.Canceled
          case "Void" => OrderRow.Status.Void
          case "Returned" => OrderRow.Status.Returned
          case _ => OrderRow.Status.Unknown
        }
      )
    }
  }
}

case class OrderItemRow(
  amount: Int,
  productId: Long,
  orderId: Long,
  overnightShipping: Option[Boolean],
  itemPaid: Option[Boolean],
  paymentType: Option[String],
  checkNumber: Option[String],
  amountPaid: Option[Double],
  invoice: Option[String],
  vendorInvoiceNum: Option[String],
  paymentDate: Option[LocalDate],
  product: OrderItemRow.Product
)

object OrderItemRow {
  case class Product(
    name: String,
    description: String,
    serialNumber: String,
    imageUrl: Option[String],
    productVendorId: Option[Long],
    productVendorName: String,
    category: Option[Product.Category],
    price: Double,
    packageQuantity: Option[Long],
    unit: Option[String]
  )

  object Product {
    case class Category(id: Long, name: String)
  }
}

case class ConsultationRow(
  id: Option[Long],
  data: JsValue,
  dateRequested: DateTime,
  status: ConsultationRow.Status,
  treatmentId: Option[Long],
  staffId: Long,
  amount: Option[Float],
  mode: Option[String],
  closedDate: Option[DateTime],
  customerProfileID: Option[Long],
  customerPaymentProfileId: Option[Long],
  transId: Option[Long],
  transactionCode: Option[String],
  majorType: Option[String],
  consultantId: Option[Long],
  consultArea: Option[String]
)

object ConsultationRow {
  sealed trait Status
  object Status {
    case object Unknown extends Status
    case object Open extends Status
    case object Closed extends Status

    trait ColumnType { self: DatabaseAccess =>
      import driver.api._

      implicit val ConsultationStatusColumnType = MappedColumnType.base[ConsultationRow.Status, String](
        {
          case ConsultationRow.Status.Open => "Open"
          case ConsultationRow.Status.Closed => "Closed"
          case ConsultationRow.Status.Unknown => "Unknown"
        },
        {
          case "Open" => ConsultationRow.Status.Open
          case "Closed" => ConsultationRow.Status.Closed
          case _ => ConsultationRow.Status.Unknown
        }
      )
    }
  }
}

case class SessionRow(
  secret: String,
  userId: Option[Long],
  staffId: Option[Long],
  lastUsedDateTime: DateTime,
  expirationDateTime: DateTime,
  idleTimeoutNanos: Option[Long]
)

case class PasswordChangeTokenRow(
  secret: String,
  userId: Option[Long],
  staffId: Option[Long],
  dateIssued: DateTime
)

object CourseRow {
  sealed trait EducationType

  object EducationType {
    case object DoctorEducation extends EducationType
    case object PatientEducation extends EducationType
    case object ResourceCenter extends EducationType
    case object CIICourse extends EducationType
    case object Unknown extends EducationType

    trait ColumnType {self: DatabaseAccess =>
      import driver.api._

      implicit val educationTypeColumnType = MappedColumnType.base[CourseRow.EducationType, String](
        {
          case CourseRow.EducationType.DoctorEducation => "Doctor"
          case CourseRow.EducationType.PatientEducation => "Patient"
          case CourseRow.EducationType.ResourceCenter => "Resource"
          case CourseRow.EducationType.CIICourse => "CIICourse"
          case _ => "Unknown"
        },
        {
          case "Doctor" => CourseRow.EducationType.DoctorEducation
          case "Patient" => CourseRow.EducationType.PatientEducation
          case "Resource" => CourseRow.EducationType.ResourceCenter
          case "CIICourse" => CourseRow.EducationType.CIICourse
          case _ => CourseRow.EducationType.Unknown
        }
      )
    }
  }
}

case class CourseRow (
  id: Option[Long],
  title: String,
  url: String,
  thumbnailUrl: Option[String],
  courseQuestionnaire: Option[String],
  dateCreated: DateTime,
  category: Option[String],
  courseId: Option[String],
  speaker: Option[String],
  educationType: CourseRow.EducationType
)


case class EmailMappingRow (
  id: Option[Long],
  name: String,
  title: String,
  isConsultation: Boolean,
  defaultEmail: Option[String],
  emailOrder: Option[Long]
)

case class UserEmailMappingRow(
  id: Option[Long],
  userId: Long,
  emailMappingId: Long
)

case class CustomerFavouritesRow(
  id: Option[Long],
  customer: Long,
  product: Long
)

case class PriceChangeHistoryRow(
  id: Option[Long],
  vendorId:Long,
  priceIncreasePercent: Option[Double],
  listPriceIncreasePercent: Option[Double],
  userId: Long,
  dateCreated: DateTime,
  categoryId: Option[Long],
  subCategoryId: Option[Long]
)

case class ToothChartRow(
  id: Option[Long],
  patientId:Long,
  toothId:Int,
  toothConditions:String,
  staffId: Long,
  userId: Long,
  dateUpdated: DateTime,
  existingConditions: Option[String]
)

case class PerioChartRow(
  id: Option[Long],
  patientId:Long,
  rowPosition:String,
  perioType:String,
  dateSelected: LocalDate,
  perioNumbers:JsValue,
  staffId: Long,
  userId: Long,
  providerId: Option[Long]
)


case class PrescriptionRow(
  id: Option[Long],
  medication: String,
  dosage: String,
  count: Option[Int],
  instructions: Option[String],
  refills: Int,
  genericSubstituteOk: Option[Boolean],
  deleted: Boolean,
  category: String,
  indications: String,
  contraindications: String,
  medicationInteraction: String,
  useInOralImplantology: String
)

case class PrescriptionHistoryRow(
  id: Option[Long],
  prescriptionId: Long,
  patientId: Long,
  staffId: Long,
  dateCreated: LocalDate,
  deleted: Boolean,
  note: Option[String]
)

case class ResourceCategoryRow(
  id: Option[Long],
  name: String,
  resourceType: String,
  order: Int,
  parent : Option[Long],
  color: Option[String]
)

case class ResourceSubCategoryRow(
  id: Option[Long],
  categoryId:Long,
  name: String,
  resourceType: String,
  order: Int
)

object EducationRequestRow {
  sealed trait Status

  object Status {
    case object Open extends Status
    case object Closed extends Status

    trait ColumnType {self: DatabaseAccess =>
      import driver.api._

      implicit val educationRequestStatusColumnType = MappedColumnType.base[EducationRequestRow.Status, String](
        {
          case EducationRequestRow.Status.Open=> "Open"
          case EducationRequestRow.Status.Closed=> "Closed"
        },
        {
          case "Open" => EducationRequestRow.Status.Open
          case "Closed" => EducationRequestRow.Status.Closed
        }
      )
    }
  }
}

case class EducationRequestRow(
  id: Option[Long],
  courseId: Long,
  staffId: Long,
  note: Option[String],
  dateCreated: DateTime,
  status: EducationRequestRow.Status,
  returnQuestionnaire: Option[String],
  closedDate: Option[LocalDate]
)

case class ExpiryNotificationRow(
  id: Option[Long],
  staffId: Long,
  expiryDate: DateTime,
  dateCreated: DateTime
)

case class VendorPaymentTrackRow(
  id: Option[Long],
  orderId: Long,
  vendorId: Option[Long],
  paid: Boolean,
  paymentType: Option[String],
  checkNumber: Option[String],
  amountPaid: Option[Double]
)

case class RenewalDetailsRow(
  id: Option[Long],
  staffId: Long,
  subscriptionPeriod: Option[Int],
  plan: Option[Long],
  renewalDate: DateTime,
  prevExpDate: DateTime,
  newExpDate: DateTime,
  renewalType: String,
  accountType: String,
  amountCollected: Option[Long],
  collectedDate: Option[DateTime],
  accessStartDate: Option[DateTime]
)

case class CartRow(
  id: Option[Long],
  userId: Option[Long],
  productId: Option[Long],
  quantity: Option[Long]
)

case class ClinicalNotesRow(
  id: Option[Long],
  staffId: Option[Long],
  patientId: Long,
  note: Option[String],
  dateCreated: DateTime,
  date: Option[LocalDate],
  time: Option[LocalTime],
  provider: Option[Long],
  lock : Option[Boolean],
  assistant1: Option[String],
  assistant2: Option[String],
  procedureCode: Option[String],
  procedureDescription: Option[String],
  bp: Option[String],
  pulse: Option[String],
  spco2: Option[String],
  co2: Option[String],
  ekg: Option[String],
  resp: Option[String],
  temp: Option[String],
  allergies: Option[String],
  medicalConditions: Option[String],
  postSurgicalHistory: Option[String],
  monitors: Option[String],
  asaStatus: Option[String],
  airwayClass: Option[String],
  anesthesia: Option[String],
  diagnosis: Option[String],
  dischargeDate: Option[LocalDate],
  dischargeTime: Option[LocalTime],
  brCompleted: Option[String],
  brMidline: Option[String],
  brLipPostion: Option[String],
  brUpper: Option[String],
  brLower: Option[String],
  impression: Option[String],
  escort: Option[String],
  lapsPhoto: Option[String],
  surgeryPhoto: Option[String],
  balancedOcclusion: Option[String],
  anteriorGuidance: Option[String],
  canineGuidance: Option[String],
  fullDenture: Option[String],
  existingDenture: Option[String],
  newDenture: Option[String],
  pmmaProsthesis: Option[String],
  compositeProsthesis: Option[String],
  prosthesesPhoto: Option[String],
  medicationEntry: Option[String],
  medicationStability: Option[String],
  vitalsPhoto: Option[String],
  preMedicationTotal: Option[String],
  patientTolerate: Option[String],
  suturingMaterials: Option[String],
  suturingTechniques: Option[String],
  reasonForNextAppointment: Option[String],
  procedure: Option[JsValue],
  incision: Option[String],
  boneGraftMaterials: Option[String],
  membranesUsed: Option[String],
  fixationSystemUsed: Option[String],
  assistant3: Option[String],
  verbalPostopIns: Option[String],
  writtenPostopIns: Option[String],
  rxVisit: Option[String],
  incisionMandible: Option[String],
  voiceMemo: Option[String]
)

case class OperatorTableRow(
  id: Option[Long],
  practiceId: Long,
  name: String,
  deleted: Boolean,
  order: Long
)

case class ProviderTableRow(
  id: Option[Long],
  practiceId: Long,
  title: String,
  firstName : String,
  lastName : String,
  role : String,
  deleted: Boolean,
  color: String,
  licenseStateAndNumber: String,
  licenseExpDate: Option[LocalDate],
  malPracticeIns: String,
  malPracticeInsExpDate: Option[LocalDate],
  deaNumber: String,
  deaExpDate : Option[LocalDate],
  npiNumber : String,
  emailAddress: String,
  phoneNumber: String,
  ocscNumber: Option[String],
  ocscExpDate: Option[LocalDate],
  hygienistLicenseNumber: Option[String],
  hygienistLicenseExpDate: Option[LocalDate],
  gender: Option[String],
  bio: Option[String],
  profileImage: Option[String],
  services: Option[String],
  languages: Option[String],
  dob: Option[LocalDate]
)

case class ProcedureCodeRow(
  id: Option[Long],
  procedureCode: String,
  procedureType: String,
  categoryId: Option[Long],
  deleted: Boolean
)

case class WorkingHoursRow(
  id: Option[Long],
  practiceId: Long,
  day: String,
  startTime :LocalTime,
  endTime : LocalTime,
  lunchStartTime : Option[LocalTime],
  lunchEndTime : Option[LocalTime]
)

case class NonWorkingDaysRow(
  id: Option[Long],
  practiceId: Long,
  date : LocalDate
)

case class TreatmentPlanRow(
  id:Option[Long],
  patientId:Long,
  dateCreated:LocalDate,
  dateUpdated:LocalDate,
  planName:String,
  status:String,
  deleted:Boolean,
  createdBy : String
)

case class TreatmentPlanDetailsRow(
  id: Option[Long],
  treatmentPlanId: Long,
  cdcCode: String,
  procedureName: String,
  toothNumber: String,
  status: String,
  datePlanned : LocalDate,
  datePerformed: Option[LocalDate],
  surface: String,
  fee: Float,
  estIns: Float,
  providerId: Option[Long],
  insurancePaidAmt : Option[Float],
  insurancePaidDate : Option[LocalDate],
  patientPaidAmt : Option[Float],
  patientPaidDate : Option[LocalDate],
  insurancePayMethod: String,
  patientPayMethod: String,
  patientId: Option[Long],
  insurance: Option[Long],
  patientBank: Option[String],
  refundAmount: Option[Float],
  refundDate: Option[LocalDate],
  refundPayMethod: Option[String],
  renewalDate: Option[LocalDate],
  familyAnnualMax: Option[Float],
  individualAnnualMax: Option[Float],
  medicalInsuranceName: Option[String],
  prognosis: Option[String],
  priority: Option[String],
  service: Option[String],
  phase: Option[String]
)

case class AppointmentRow(
  id: Option[Long],
  practiceId: Long,
  date : LocalDate,
  startTime : LocalTime,
  endTime : LocalTime,
  patientId : Long,
  operatoryId : Long,
  providerId : Long,
  procedureType : String,
  procedureCode : String,
  anaesthesia : String,
  notes : String,
  status : String,
  amountToBeCollected: Float,
  deleted : Boolean,
  reason : String,
  patientName : String,
  referredBy : String,
  alerts : String,
  insurance : String,
  phoneNumber : String,
  assistantId: Option[String],
  email: Option[String],
  procedure: Option[String],
  creator: Option[String],
  reminderSentDate : Option[LocalDate]
)

case class AppointmentLogRow(
  id: Option[Long],
  appointmentId: Long,
  practiceId: Long,
  date : LocalDate,
  startTime : LocalTime,
  endTime : LocalTime,
  patientId : Long,
  operatoryId : Long,
  providerId : Long,
  procedureType : String,
  procedureCode : String,
  anaesthesia : String,
  notes : String,
  status : String,
  operation : String,
  updatedBy : Long,
  updatedAt : DateTime,
  amountToBeCollected: Float,
  reason : String,
  patientName : String,
  referredBy : String,
  alerts : String,
  insurance : String,
  phoneNumber : String,
  assistantId: Option[String],
  email: Option[String],
  procedure: Option[String],
  creator: Option[String]
)

case class MedicalConditionLogRow(
  id: Option[Long],
  formId: String,
  patientId: Long,
  value: String,
  conditionType: String,
  dateCreated: LocalDate,
  currentlySelected: Boolean
)

case class StateTaxRow(
  id: Option[Long],
  state: String,
  stateName: String,
  saleTax: Float
)

case class AppointmentReasonRow(
  id: Option[Long],
  reason: String
)

case class MeasurementHistoryRow(
  id: Option[Long],
  patientId: Long,
  formId: String,
  systolic: Option[Double],
  diastolic: Option[Double],
  pulse: Option[Double],
  oxygen: Option[Double],
  bodyTemp: Option[Double],
  glucose: Option[Double],
  a1c: Option[Double],
  inr: Option[Double],
  pt: Option[Double],
  dateCreated: DateTime
)

case class SettingsRow(
  id: Option[Long],
  creditCardFee: Option[String]
)

case class RoleRow(
  id: Option[Long],
  role: Option[String]
)

case class OrderLogRow(
  id: Option[Long],
  orderId : Option[Long],
  userAgent: Option[String]
)

case class SaleTaxRow(
  id: Option[Long],
  state: String,
  stateName: String,
  vendorId : Long,
  tax: Float
)

case class StateRow(
  id: Option[Long],
  state: String,
  stateName: String
)



case class CartLogRow(
  id: Option[Long],
  dateCreated : LocalDate,
  userId: Option[Long],
  productId: Option[Long],
  productPrice: Option[Float],
  quantity: Option[Long]
)

case class StaffMembersRow(
  id: Option[Long],
  practiceId: Long,
  firstName: String,
  lastName: String,
  title: String,
  empStartDate: LocalDate,
  empTerminationDate: Option[LocalDate],
  color: String,
  rda: Option[String],
  expDateOfRda: Option[LocalDate],
  deleted: Boolean,
  role: Option[String],
  dob: Option[LocalDate]
)

case class SharedPatientRow(
  id: Option[Long],
  patientId: Long,
  practiceId: Long,
  shareTo: Long,
  sharedBy: Long,
  sharedDate: LocalDate,
  mainSquares: String,
  icons: String,
  accessTo: String,
  accessLevel: String
)

case class ListOptionsRow(
  id: Option[Long],
  listType: String,
  listOptions: String,
  listValue: String
)

case class ProcedureCodeCategoryRow(
  id: Option[Long],
  categoryName : String
)

case class InsuranceCompanyRow(
  id: Option[Long],
  name : String,
  deleted: Boolean,
  companyOrder: Long
)

case class InsuranceFeeRow(
  id: Option[Long],
  practiceId: Long,
  insuranceId: Long,
  procedureCode : String,
  fee: Float
)

case class MedicationEntryRow(
  id: Option[Long],
  notesId: Long,
  medication: String,
  dosage: String
)

case class MedicationStabilityRow(
  id: Option[Long],
  notesId: Long,
  implant: String,
  implantBrand: String,
  diameter: String,
  boneDensity: String,
  length: String,
  ncm: String,
  isqBl: String,
  isqMd: String
)

case class ErrorLogRow(
  id: Option[Long],
  method: String,
  input: String,
  fileName: String,
  errorMessage: String
)

case class ConEdTrackRow(
  id: Option[Long],
  staffId: Long,
  courseId: Long,
  date: LocalDate
  )

  case class ReferralSourceRow(
  id: Option[Long],
  practiceId: Long,
  value: String,
  types: String,
  title: String,
  firstName: String,
  lastName: String,
  archived: Boolean
  )

  case class FamilyRow(
  id: Option[Long],
  guarantorId: Option[Long],
  guarantorFirstName: Option[String],
  guarantorLastName: Option[String],
  guarantorGender: Option[String],
  guarantorEmail: Option[String],
  guarantorPhone: Option[String],
  guarantorDOB: Option[LocalDate],
  guarantorSS: Option[String],
  patientType: Option[String]
  )

case class FamilyMembersRow(
  id: Option[Long],
  familyId: Long,
  patientId: Long
  )

  case class DentalCarrierRow(
  id: Option[Long],
  practiceId: Long,
  name: String,
  address: String,
  city: String,
  state: String,
  zip: String,
  phone: String,
  phone2: String,
  payerId: String,
  claimType: String,
  website: String,
  contactPerson: String,
  insRefNumber: String,
  notes: String,
  feeSchedule: String,
  archived: Boolean
  )

case class McKessonImportLogRow(
  id: Option[Long],
  date: LocalDate,
  offset: Long,
  feedSize: Long,
  status: String
  )

case class SaveLaterRow(
  id: Option[Long],
  staffId: Long,
  productId: Long
)

case class VendorInvoiceRow(
  id: Option[Long],
  vendorId: Long,
  orderId: Long,
  vendorName: String,
  invoiceNum: String,
  paymentType: String,
  amountPaid: Float,
  paymentDate: LocalDate,
  checkNumber: String,
  notes: Option[String],
  invoice: String
  )

case class SubscribeProductsRow(
  id: Option[Long],
  staffId: Long,
  productId: Long,
  frequency: String,
  quantity: Long,
  shippingAddressId: Long,
  startDate: LocalDate,
  unsubscribed: Boolean,
  endDate: Option[LocalDate],
  customerPaymentProfileId: Long,
  lastOrderedDate: LocalDate,
  disabled: Boolean,
  shippingCharge: Float,
  salesTax: Float,
  grandTotal: Float,
  productPrice: Float,
  totalAmountSaved: Float
  )

  case class SubscribeOrderHistoryRow(
  id: Option[Long],
  subscriptionId: Long,
  orderedDate: LocalDate,
  orderId: Long,
  customerProfileId: Long,
  customerPaymentProfileId: Long,
  transactionId: Long,
  transactionCode: String,
  grandTotal: Float
  )

  case class SubscribeOrderFailureHistoryRow(
  id: Option[Long],
  subscriptionId: Long,
  paymentDate: LocalDate,
  customerProfileId: Long,
  customerPaymentProfileId: Long,
  transactionId: Long,
  transactionCode: String,
  grandTotal: Float
  )

case class StaffNotesRow(
  id: Option[Long],
  staffId: String,
  patientId: Long,
  date: LocalDate,
  notes: String,
  time: Option[LocalTime],
  notesFor: Option[String]
  )

case class NotificationsRow(
  id: Option[Long],
  message: String,
  dateCreated: LocalDate,
  deviceId: String,
  status: String
)

case class DevicesRow(
  id: Option[Long],
  staffId: Long,
  deviceDetails: String,
  deviceType: String,
  deviceModel: String,
  deviceToken: String,
  lastLoginDate: LocalDate
)

case class ImplantDataRow(
  id: Option[Long],
  noteId: Long,
  implantSite: String,
  implantBrand: String,
  implantSurface: String,
  implantReference: String,
  implantLot: String,
  diameter: String,
  length: String,
  surgProtocol: String,
  boneDensity: String,
  osteotomySite: String,
  ncm: String,
  isqBl: String,
  isqMd: String,
  implantDepth: String,
  loadingProtocol: String,
  dateOfRemoval: String,
  reasonForRemoval: String,
  finalPA: String,
  recommendedHT: String,
  implantRestored: String,
  patientName: Option[String],
  provider: Option[String],
  date: Option[LocalDate],
  practiceId: Option[Long],
  staffId: Option[Long],
  dob: Option[LocalDate]

)

case class MckessonProductsRow(
  itemId: String,
  units: String
)

case class ConfigTableRow(
  label: String,
  value: String
)

case class PreMadeClinicalNotesRow(
  id: Option[Long],
  practiceId: Long,
  name: String,
  note: String,
  deleted: Boolean
)

case class OrderVendorDetailsRow(
  id: Option[Long],
  orderId: Long,
  vendorId: Long,
  note: String
)

case class TutorialsRow(
  id: Option[Long],
  name: String,
  link: String,
  parent: Option[Long],
  order: Long,
  sectionType: Option[String]
)

case class SaasRow(
  id: Option[Long],
  practiceId: Option[Long],
  saasType: String,
  logo: String,
  name: String,
  subStartDate: LocalDate,
  subExpDate: Option[LocalDate],
  subscriptionType: Option[String],
  subscriptionFee: Option[Float],
  notes: Option[String],
  autoRenew: Option[String],
  documents: Option[String],
  deleted: Boolean
)

case class VendorCategoriesRow(
  id: Option[Long],
  vendorId: Long,
  categories: String
)

case class DocumentRow(
  id: Option[Long],
  patientId: Long,
  documentType: String,
  documents: String
)

case class SaasPaymentRow(
  id: Option[Long],
  saasId: Long,
  practiceId: Long,
  month: String,
  year: Long,
  amount: Float
)

case class EndoChartRow(
  id: Option[Long],
  patientId:Long,
  rowPosition:String,
  dateSelected: LocalDate,
  data: JsValue,
  staffId: Long,
  providerId: Option[Long]
)

case class SaasAdminRow(
  id: Option[Long],
  saasType: String,
  logo: String,
  name: String,
  subStartDate: LocalDate,
  subExpDate: Option[LocalDate],
  subscriptionType: Option[String],
  subscriptionFee: Option[Float],
  notes: Option[String],
  autoRenew: Option[String],
  documents: Option[String],
  deleted: Boolean
)

case class SaasAdminPaymentRow(
  id: Option[Long],
  saasAdminId: Long,
  month: String,
  year: Long,
  amount: Float
)

case class CeHistoryRow(
  id: Option[Long],
  staffId: Long,
  courseId: Long,
  dateCreated: DateTime,
  action: String
)

case class ReferredToRow (
  id: Option[Long],
  practiceId: Long,
  name: String,
  types: String,
  title: String,
  firstName: String,
  lastName: String,
  deleted: Boolean
)

case class CeCoursePurchaseRow(
  id: Option[Long],
  staffId: Long,
  courseId: Long,
  cost: Float,
  purchasedDate: DateTime,
  customerProfileId: Long,
  customerPaymentProfileId: Long,
  transactionId: Long,
  transactionCode: String
)

case class ManualsRow(
  id: Option[Long],
  name: String,
  link: String,
  parent: Option[Long],
  order: Long,
  sectionType: Option[String]
)

case class DefaultFeeRow(
  id: Option[Long],
  procedureCode: String,
  ucrFee: Long,
  privateFee: Long
)

case class AppointmentTreatmentPlanRow(
  id: Option[Long],
  appointmentId: Long,
  treatmentPlanId: Long
)

case class FriendReferralRow(
  id: Option[Long],
  title: String,
  firstName: String,
  lastName: String,
  practiceName: String,
  phone: String,
  email: String,
  note: Option[String],
  staffId: Option[Long]
)

case class OnlineRegInviteTrackRow (
  id: Option[Long],
  patientId: Long,
  sentDate: DateTime
)

case class BoneGraftingDataRow(
  id: Option[Long],
  noteId: Long,
  practiceId: Long,
  staffId: Long,
  providerName: String,
  patientName: String,
  date: Option[LocalDate],
  dob: Option[LocalDate],
  allograftMaterialUsed: String,
  allograftSize: String,
  allograftVolume: String,
  allograftBrand: String,
  xenograftMaterialUsed: String,
  xenograftSize: String,
  xenograftVolume: String,
  xenograftBrand: String,
  alloplastMaterialUsed: String,
  alloplastSize: String,
  alloplastVolume: String,
  alloplastBrand: String,
  autograftMaterialUsed: String,
  autograftSize: String,
  autograftVolume: String,
  membraneUsed: String,
  membraneSize: String,
  membraneBrand: String,
  additives: String,
  additivesVolume: String,
  additivesBrand: String,
  finalPA: String,
  rrpsd: Option[LocalDate],
  failureDate: Option[LocalDate]
  )

case class AppointmentReminderTrackRow (
  id: Option[Long],
  appointmentId: Long,
  dateTime: DateTime,
  action: String,
  status: String,
  note: String,
  reason: String
)

case class ProviderEducationRow (
  providerId: Long,
  degree: String,
  college: String,
  educationYear: Long
)

case class ProviderExperienceRow (
  providerId: Long,
  place: String,
  from: String,
  to: String
)

case class ProviderAwardsRow (
  providerId: Long,
  name: String,
  awardsYear: Long
)

case class SubscriberRow(
  id: Option[Long],
  patientId: Long,
  `type`: String,
  firstName: String,
  lastName: String,
  dob: LocalDate,
  address: String,
  city: String,
  state: String,
  zip: String,
  phone: String,
  ssn: String,
  relationship: String,
  carrierId: Long,
  memberId: String,
  groupNumber: String,
  planName: String,
  employerName: String,
  employerPhone: String
)

case class EligibilityRow(
  id: Option[Long],
  patientId: Long,
  requestDate: Option[LocalDate],
  receivedDate: Option[LocalDate],
  subscriberType: String,
  subscriberChartNumber: String,
  eligibilityResponse: String,
  activeCoverage: String,
  resultCode: String,
  resultDesc: String
)

trait TableDefinitions extends OrderRow.Status.ColumnType with StepRow.State.ColumnType with ConsultationRow.Status.ColumnType with StaffRow.Role.ColumnType with UserRow.Role.ColumnType with CourseRow.EducationType.ColumnType with EducationRequestRow.Status.ColumnType { self: DatabaseAccess =>

  protected val driver: PostgresDriver

  import driver.api._
  import com.github.tototoshi.slick.PostgresJodaSupport._
/*  import slick.driver.H2Driver.api._
  import shapeless._
  import slickless._*/
//  import slick.driver.PostgresDriver.api._
  import shapeless._
  import slickless._

  // Staff

  class StaffTable(tag: Tag) extends Table[StaffRow](tag, "staff") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def email = column[String]("email")
    def password = column[String]("password")
    def role = column[StaffRow.Role]("role")
    def practiceId = column[Long]("practiceId")
    def dateCreated = column[DateTime]("dateCreated")
    def archived = column[Boolean]("archived")
    def udid = column[Option[String]]("udid")
    def udid1 = column[Option[String]]("udid1")
    def canAccessWeb = column[Boolean]("canAccessWeb")
    def deaNumber = column[Option[String]]("deaNumber")
    def npiNumber = column[Option[String]]("npiNumber")
    def phone = column[Option[String]]("phone")

    def country = column[Option[String]]("country")
    def city = column[Option[String]]("city")
    def state = column[Option[String]]("state")
    def zip = column[Option[String]]("zip")
    def address = column[Option[String]]("address")
    def expirationDate = column[DateTime]("expirationDate")
    def plan = column[Option[Long]]("plan")
    def accountType = column[String]("accountType")
    def title = column[String]("title")
    def lastName = column[Option[String]]("lastName")
    def marketingSource = column[Option[String]]("marketingSource")
    def notes = column[Option[String]]("notes")
    def subscriptionPeriod = column[Option[Int]]("subscriptionPeriod")
    def cellPhone = column[Option[String]]("cellPhone")
    def ciiPrograms = column[Option[String]]("ciiPrograms")
    def appSquares = column[Option[String]]("appSquares")
    def custToLeadFlag = column[Option[Boolean]]("custToLeadFlag")
    def photo = column[Option[String]]("photo")
    def amountCollected = column[Option[Long]]("amountCollected")
    def collectedDate = column[Option[DateTime]]("collectedDate")
    def providersFilter = column[Option[String]]("providersFilter")
    def webModules = column[Option[String]]("webModules")
    def prime = column[Option[Boolean]]("prime")
    def authorizeCustId = column[Option[Long]]("authorizeCustId")
    def allowAllCECourses = column[Option[Boolean]]("allowAllCECourses")
    def udid2 = column[Option[String]]("udid2")
    def mckessonId = column[Option[Long]]("mckessonId")
    def agreementAccepted = column[Option[Boolean]]("agreementAccepted")
    def contract = column[Option[String]]("contract")
    def suiteNumber = column[Option[String]]("suiteNumber")
    def dentalLicense = column[Option[String]]("dentalLicense")
    def doseSpotUserId = column[Option[String]]("doseSpotUserId")
    def icoiMember = column[Option[String]]("icoiMember")
    def dateOfBirth = column[Option[LocalDate]]("dateOfBirth")
    def primaryFax = column[Option[String]]("primaryFax")
    def clinicianRoleType = column[Option[String]]("clinicianRoleType")
    def accessStartDate = column[Option[DateTime]]("accessStartDate")
    def ceInfo = column[Boolean]("ceInfo")

    def * = (
      id.? :: name :: email :: password ::
        role :: practiceId :: dateCreated :: udid :: udid1 :: canAccessWeb :: deaNumber :: npiNumber :: phone :: country :: city :: state :: zip :: address :: expirationDate :: plan :: accountType :: title :: lastName :: marketingSource :: notes :: subscriptionPeriod :: cellPhone :: ciiPrograms :: appSquares :: custToLeadFlag :: photo :: amountCollected :: collectedDate :: providersFilter :: webModules :: prime :: authorizeCustId :: allowAllCECourses:: udid2 :: mckessonId :: agreementAccepted :: contract :: suiteNumber :: dentalLicense :: doseSpotUserId :: icoiMember :: dateOfBirth :: primaryFax :: clinicianRoleType :: accessStartDate :: ceInfo :: HNil).mappedWith(Generic[StaffRow])
  }

  val StaffTable = TableQuery[StaffTable]

  // Users

  class UserTable(tag: Tag) extends Table[UserRow](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def email = column[String]("email")
    def password = column[String]("password")
    def phone = column[String]("phone")
    def dateCreated = column[DateTime]("dateCreated")
    def role = column[UserRow.Role]("role")
    def archived = column[Boolean]("archived")
    def * = (id.?, name, email, password, phone, dateCreated, role) <> ((UserRow.apply _).tupled, UserRow.unapply)
  }

  val UserTable = TableQuery[UserTable]

  // Practices
  class PracticeTable(tag: Tag) extends Table[PracticeRow](tag, "practices") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def country = column[String]("country")
    def city = column[String]("city")
    def state = column[String]("state")
    def zip = column[String]("zip")
    def address = column[String]("address")
    def phone = column[String]("phone")
    def dateCreated = column[DateTime]("dateCreated")
    def xvWebURL = column[Option[String]]("xvWebURL")
    def xvWebUserName = column[Option[String]]("xvWebUserName")
    def xvWebPassword = column[Option[String]]("xvWebPassword")
    def archived = column[Boolean]("archived")
    def shortId = column[Option[String]]("shortId")
    def timeZone = column[Option[String]]("timeZone")
    def phoneExt = column[Option[String]]("phoneExt")
    def phone1 = column[Option[String]]("phone1")
    def phoneExt1 = column[Option[String]]("phoneExt1")
    def fax = column[Option[String]]("fax")
    def email = column[Option[String]]("email")
    def taxId = column[Option[Long]]("taxId")
    def insBillingProvider = column[Option[String]]("insBillingProvider")
    def billingLicense = column[Option[String]]("billingLicense")
    def openingDate = column[Option[LocalDate]]("openingDate")
    def officeGroup = column[Option[String]]("officeGroup")
    def ucrFeeSchedule = column[Option[String]]("ucrFeeSchedule")
    def feeSchedule = column[Option[String]]("feeSchedule")
    def officeId = column[Option[String]]("officeId")
    def doseSpotClinicId = column[Option[String]]("doseSpotClinicId")
    def doseSpotClinicKey = column[Option[String]]("doseSpotClinicKey")
    def chartNumberType = column[Option[String]]("chartNumberType")
    def chartNumberSeq = column[Option[Long]]("chartNumberSeq")
    def suite = column[Option[String]]("suite")
    def appointmentReminder = column[Option[Boolean]]("appointmentReminder")
    def appReminderSetting = column[Option[String]]("appReminderSetting")
    def onlineRegReminderFrequency = column[Option[String]]("onlineRegReminderFrequency")
    def onlineRegMaxReminder = column[Option[String]]("onlineRegMaxReminder")
    def onlineRegistrationModule = column[Option[Boolean]]("onlineRegistrationModule") 
    def onlineInviteMessage = column[Option[String]]("onlineInviteMessage")
    def appointmentReminderMessage = column[Option[String]]("appointmentReminderMessage")
    def consentFormMessage = column[Option[String]]("consentFormMessage")
    def amenities = column[Option[String]]("amenities")
    def photos = column[Option[String]]("photos")
    def latitude = column[Option[Double]]("latitude")
    def longitude = column[Option[Double]]("longitude")
    def clinicDescription = column[Option[String]]("clinicDescription")
    def trojanAccountNo = column[Option[Long]]("trojanAccountNo")
    def postopInstructionsMessage = column[Option[String]]("postopInstructionsMessage")
    def recallReminderMessage = column[Option[String]]("recallReminderMessage")

    def * = (id.? :: name :: country :: city :: state :: zip :: address :: phone :: dateCreated :: xvWebURL :: xvWebUserName :: xvWebPassword :: shortId :: timeZone :: phoneExt :: phone1 :: phoneExt1 :: fax :: email :: taxId :: insBillingProvider :: billingLicense :: openingDate :: officeGroup :: ucrFeeSchedule :: feeSchedule :: officeId :: doseSpotClinicId :: doseSpotClinicKey :: chartNumberType :: chartNumberSeq :: suite :: appointmentReminder :: appReminderSetting :: onlineRegReminderFrequency :: onlineRegMaxReminder  :: onlineRegistrationModule :: onlineInviteMessage :: appointmentReminderMessage :: consentFormMessage :: amenities :: photos :: latitude :: longitude :: clinicDescription :: trojanAccountNo :: postopInstructionsMessage :: recallReminderMessage :: HNil).mappedWith(Generic[PracticeRow])
  }
  val PracticeTable = TableQuery[PracticeTable]

  // Shipping
  class ShippingTable(tag: Tag) extends Table[ShippingRow](tag, "shippingAddresses") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[Option[String]]("name")
    def country = column[String]("country")
    def city = column[String]("city")
    def state = column[String]("state")
    def zip = column[String]("zip")
    def address = column[String]("address")
    def phone = column[String]("phone")
    def primary = column[Boolean]("primary")
    def dentist = column[Option[Long]]("dentist")
    def suite = column[Option[String]]("suite")
    def * = (id.?, name, country, city, state, zip, address, phone, primary, dentist,suite) <> (ShippingRow.tupled, ShippingRow.unapply)
  }

  val ShippingTable = TableQuery[ShippingTable]

  // Treatments

  class TreatmentTable(tag: Tag) extends Table[TreatmentRow](tag, "treatments") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[String]("patientId")
    def firstVisit = column[Option[DateTime]]("firstVisit")
    def lastEntry = column[Option[DateTime]]("lastEntry")
    def phoneNumber = column[String]("phoneNumber")
    def emailAddress = column[String]("emailAddress")
    def staffId = column[Long]("staffId")
    def deleted = column[Boolean]("deleted")
    def ctScanFile = column[Option[String]]("ctScanFile")
    def contracts = column[Option[String]]("contracts")
    def xray = column[Option[String]]("xray")
    def scratchPad = column[Option[String]]("scratchPad")
    def scratchPadText = column[Option[String]]("scratchPadText")
    def toothChartUpdatedDate = column[Option[DateTime]]("toothChartUpdatedDate")
    def referralSource = column[Option[String]]("referralSource")
    def treatmentStarted = column[Boolean]("treatmentStarted")
    def recallPeriod = column[Option[String]]("recallPeriod")
    def patientType = column[Option[String]]("patientType")
    def patientStatus = column[Option[String]]("patientStatus")
    def shareToAdmin = column[Option[Boolean]]("shareToAdmin")
    def mainSquares = column[Option[String]]("mainSquares")
    def icons = column[Option[String]]("icons")
    def sharedDate = column[Option[LocalDate]]("sharedDate")
    def sharedBy = column[Option[Long]]("sharedBy")
    def referralSourceId = column[Option[Long]]("referralSourceId")
    def insurance = column[String]("insurance")
    def recallType = column[Option[String]]("recallType")
    def recallCode = column[Option[String]]("recallCode")
    def recallCodeDescription = column[Option[String]]("recallCodeDescription")
    def intervalNumber = column[Option[Long]]("intervalNumber")
    def intervalUnit = column[Option[String]]("intervalUnit")
    def recallFromDate = column[Option[LocalDate]]("recallFromDate")
    def recallOneDayPlus = column[Option[Boolean]]("recallOneDayPlus")
    def recallDueDate = column[Option[LocalDate]]("recallDueDate")
    def recallNote = column[Option[String]]("recallNote")
    def doseSpotPatientId = column[Option[String]]("doseSpotPatientId")
    def firstName = column[Option[String]]("firstName")
    def lastName = column[Option[String]]("lastName")
    def dateOfBirth = column[Option[String]]("dob")
    def providerId = column[Option[Long]]("providerId")
    def practiceId = column[Option[Long]]("practiceId")
    def isFamily = column[Option[Boolean]]("isFamily")
    def isMedicalCondition = column[Option[Boolean]]("isMedicalCondition")
    def alerts = column[Option[String]]("alerts")
    def examImaging = column[Option[String]]("examImaging")
    def referredTo = column[Option[Long]]("referredTo")
    def referredDate = column[Option[DateTime]]("referredDate")
    def missingTooth = column[Option[String]]("missingTooth")
    def edentulousMaxilla = column[Option[Boolean]]("edentulousMaxilla")
    def edentulousMandible = column[Option[Boolean]]("edentulousMandible")
    def allPrimary = column[Option[Boolean]]("allPrimary")
    def primaryTooth = column[Option[String]]("primaryTooth")
    def lastInvitationSent = column[Option[DateTime]]("lastInvitationSent")
    def inviteMaxLimitReached = column[Option[Boolean]]("inviteMaxLimitReached")
    def completedBy = column[Option[String]]("completedBy")
    def completedDate = column[Option[DateTime]]("completedDate")
    def onlineRegistration = column[Option[String]]("onlineRegistration")
    def onlineRegistrationStatus = column[Option[String]]("onlineRegistrationStatus")
    def country = column[Option[String]]("country")
    def recallReminderSentDate = column[Option[LocalDate]]("recallReminderSentDate")

    def * = (id.? :: patientId :: firstVisit :: lastEntry :: phoneNumber :: emailAddress :: staffId :: ctScanFile ::contracts ::xray :: scratchPad :: scratchPadText :: toothChartUpdatedDate :: referralSource :: treatmentStarted :: recallPeriod :: patientType :: patientStatus :: shareToAdmin :: mainSquares :: icons :: sharedDate :: sharedBy :: referralSourceId :: insurance :: recallType :: recallCode :: recallCodeDescription :: intervalNumber :: intervalUnit :: recallFromDate :: recallOneDayPlus :: recallDueDate :: recallNote :: doseSpotPatientId :: firstName :: lastName :: dateOfBirth :: providerId :: practiceId :: isFamily :: isMedicalCondition :: alerts :: examImaging :: referredTo :: referredDate :: missingTooth :: edentulousMaxilla :: edentulousMandible :: allPrimary :: primaryTooth :: lastInvitationSent :: inviteMaxLimitReached :: completedBy :: completedDate :: onlineRegistration :: onlineRegistrationStatus :: country :: recallReminderSentDate :: HNil).mappedWith(Generic[TreatmentRow])
  }

  val TreatmentTable = TableQuery[TreatmentTable]

  // Forms

  class FormTable(tag: Tag) extends Table[FormRow](tag, "forms") {
    def id = column[String]("id")
    def version = column[Int]("version")
    def section = column[String]("section")
    def title = column[String]("title")
    def icon = column[String]("icon")
    def schema = column[JsValue]("schema")
    def print = column[Boolean]("print")
    def listOrder = column[Long]("listOrder")
    def types = column[String]("types")
    def * = (id, version, section, title, icon, schema, print,listOrder,types).shaped <> (
      { case (a, b, c, d, e ,f, g, h, i) => FormRow(a, b, c, d, e ,f, g, h, i) },
      FormRow.unapply
    )
    def pk = primaryKey("pk_forms", (id, version))
  }

  val FormTable = TableQuery[FormTable]

  // Steps

  class StepTable(tag: Tag) extends Table[StepRow](tag, "steps") {
    def value = column[JsValue]("value")
    def state = column[StepRow.State]("state")
    def treatmentId = column[Long]("treatmentId")
    def formId = column[String]("formId")
    def formVersion = column[Int]("formVersion")
    def dateCreated = column[DateTime]("dateCreated")
    def * = (value, state, treatmentId, formId, formVersion, dateCreated) <> ((StepRow.apply _).tupled, StepRow.unapply)
    def pk = primaryKey("pk_steps", (treatmentId, formId, formVersion))
  }

  val StepTable = TableQuery[StepTable]

  // Products

  class ProductTable(tag: Tag) extends Table[ProductRow](tag, "products") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def description = column[Option[String]]("description")
    def serialNumber = column[String]("serialNumber")
    def imageUrl = column[Option[String]]("imageUrl")
    def vendorId = column[Long]("vendorId")
    def categoryId = column[Option[Long]]("categoryId")
    def price = column[Double]("price")
    def quantity = column[Option[Long]]("quantity")
    def deleted = column[Boolean]("deleted")
    def listPrice = column[Option[Double]]("listPrice")
    def document = column[Option[String]]("document")
    def video = column[Option[JsValue]]("video")
    def stockAvailable = column[Option[Boolean]]("stockAvailable")
    def unit = column[Option[String]]("unit")
    def financeEligible = column[Option[Boolean]]("financeEligible")
    def * = (id.?, name, description, serialNumber, imageUrl, vendorId, categoryId, price, quantity, listPrice, document, video,stockAvailable,unit,financeEligible) <> (ProductRow.tupled, ProductRow.unapply)
  }

  val ProductTable = TableQuery[ProductTable]

  // Vendors

  class VendorTable(tag: Tag) extends Table[VendorRow](tag, "vendors") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def logoUrl = column[Option[String]]("logoUrl")
    def email = column[Option[String]]("email")
    def deleted = column[Boolean]("deleted")
    def shippingChargeLimit = column[Option[Double]]("shippingChargeLimit")
    def shippingCharge = column[Option[Double]]("shippingCharge")
    def tax = column[Option[Double]]("tax")
    def creditCardFee = column[Option[Double]]("creditCardFee")
    def group = column[Option[String]]("group")
    def category = column[Option[String]]("category")
    def image = column[Option[String]]("image")
    def groups = column[Option[JsValue]]("groups")

    def * = (id.?, name, email,logoUrl,  deleted, shippingChargeLimit, shippingCharge, tax, creditCardFee, group, category, image, groups) <> (VendorRow.tupled, VendorRow.unapply)
  }

  val VendorTable = TableQuery[VendorTable]

  // Categories

  class CategoryTable(tag: Tag) extends Table[CategoryRow](tag, "categories") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def taxExempted = column[Option[Boolean]]("taxExempted")
    def parent = column[Option[Long]]("parent")
    def order = column[Int]("order")
    def * = (id.?, name, taxExempted, parent, order) <> (CategoryRow.tupled, CategoryRow.unapply)
  }

  val CategoryTable = TableQuery[CategoryTable]

  // Orders

  class OrderTable(tag: Tag) extends Table[OrderRow](tag, "orders") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def status = column[OrderRow.Status]("status")
    def dateCreated = column[DateTime]("dateCreated")
    def staffId = column[Long]("staffId")
    def dateFulfilled = column[Option[DateTime]]("dateFulfilled")
    def approvedBy = column[Option[String]]("approvedBy")
    def name = column[Option[String]]("name")
    def country = column[Option[String]]("country")
    def city = column[Option[String]]("city")
    def state = column[Option[String]]("state")
    def zip = column[Option[String]]("zip")
    def address = column[Option[String]]("address")
    def phone = column[Option[String]]("phone")
    def device = column[Option[String]]("device")
    def suite = column[Option[String]]("suite")
    def paymentType = column[Option[String]]("paymentType")
    def checkNumber = column[Option[String]]("checkNumber")
    def allVendorsPaid = column[Option[Boolean]]("allVendorsPaid")
    def shippingCharge = column[Option[Float]]("shippingCharge")
    def tax = column[Option[Float]]("tax")
    def creditCardFee = column[Option[Float]]("creditCardFee")
    def grandTotal = column[Option[Float]]("grandTotal")
    def totalAmountSaved = column[Option[Float]]("totalAmountSaved")
    def customerProfileID = column[Option[Long]]("customerProfileID")
    def customerPaymentProfileId = column[Option[Long]]("customerPaymentProfileId")
    def transId = column[Option[Long]]("transId")
    def platform = column[Option[String]]("platform")
    def transactionCode = column[Option[String]]("transactionCode")
    def mckessonOrder = column[Option[Boolean]]("mckessonOrder")
    def mckessonStatus = column[Option[String]]("mckessonStatus")
    def paymentCompleted = column[Option[Boolean]]("paymentCompleted")
    def note = column[Option[String]]("note")

    def * = (id.?, status, dateCreated, staffId, dateFulfilled, approvedBy, (name, country, city, state, zip, address, phone, device, suite), checkNumber,shippingCharge,tax,creditCardFee,grandTotal,totalAmountSaved,platform,mckessonOrder,mckessonStatus,note,(paymentType,allVendorsPaid,customerProfileID,customerPaymentProfileId,transId,transactionCode,paymentCompleted)).shaped <> (
      {
        case (id, status, dateCreated, staffId, dateFulfilled, approvedBy, (None, None, None, None, None, None, None, None, None), checkNumber,shippingCharge,tax,creditCardFee,grandTotal,totalAmountSaved,platform,mckessonOrder,mckessonStatus,note,(None, None, None, None, None, None, None)) =>
          OrderRow(id, status, dateCreated, staffId, dateFulfilled, approvedBy, None, checkNumber,shippingCharge,tax,creditCardFee,grandTotal,totalAmountSaved,platform,mckessonOrder,mckessonStatus,note,None)
        case (id, status, dateCreated, staffId, dateFulfilled, approvedBy, (name, country, city, state, zip, address, phone, device, suite), checkNumber,shippingCharge,tax,creditCardFee,grandTotal,totalAmountSaved,platform,mckessonOrder,mckessonStatus,note,(paymentType,allVendorsPaid,customerProfileID,customerPaymentProfileId,transId,transactionCode,paymentCompleted)) =>
          OrderRow(id, status, dateCreated, staffId, dateFulfilled, approvedBy, Some(OrderRow.Address(name, country.getOrElse(""), city.getOrElse(""), state.getOrElse(""), zip.getOrElse(""), address.getOrElse(""), phone.getOrElse(""), device, suite)), checkNumber,shippingCharge,tax,creditCardFee,grandTotal,totalAmountSaved,platform,mckessonOrder,mckessonStatus,note,Some(OrderRow.Payment(paymentType,allVendorsPaid,customerProfileID,customerPaymentProfileId,transId,transactionCode,paymentCompleted)))
      },
      {
        order: OrderRow =>
          val orderAddress = order.address match {
            case Some(address) => (address.name, Some(address.country), Some(address.city), Some(address.state), Some(address.zip), Some(address.address), Some(address.phone), address.device, address.suite)
          }
          val orderPayment = order.payment match {
            case Some(payment) => (payment.paymentType, payment.allVendorsPaid, payment.customerProfileID, payment.customerPaymentProfileId, payment.transId, payment.transactionCode, payment.paymentCompleted)
          }
          Some((order.id, order.status, order.dateCreated, order.staffId, order.dateFulfilled, order.approvedBy, orderAddress,order.checkNumber,order.shippingCharge,order.tax,order.creditCardFee,order.grandTotal,order.totalAmountSaved,order.platform,order.mckessonOrder,order.mckessonStatus,order.note,orderPayment))
      }
    )
  }

  val OrderTable = TableQuery[OrderTable]

  // OrderItems

  class OrderItemTable(tag: Tag) extends Table[OrderItemRow](tag, "orderItems") {
    def amount = column[Int]("amount")
    def productId = column[Long]("productId")
    def orderId = column[Long]("orderId")
    def productName = column[String]("productName")
    def productDescription = column[String]("productDescription")
    def productSerialNumber = column[String]("productSerialNumber")
    def productImageUrl = column[Option[String]]("productImageUrl")
    def productVendorId = column[Option[Long]]("productVendorId")
    def productVendorName = column[String]("productVendorName")
    def productCategoryId = column[Option[Long]]("productCategoryId")
    def productCategoryName = column[Option[String]]("productCategoryName")
    def productPrice = column[Double]("productPrice")
    def packageQuantity = column[Option[Long]]("packageQuantity")
    def overnightShipping = column[Option[Boolean]]("overnightShipping")
    def itemPaid = column[Option[Boolean]]("itemPaid")
    def paymentType = column[Option[String]]("paymentType")
    def checkNumber = column[Option[String]]("checkNumber")
    def amountPaid = column[Option[Double]]("amountPaid")
    def invoice = column[Option[String]]("invoice")
    def unit = column[Option[String]]("unit")
    def vendorInvoiceNum = column[Option[String]]("vendorInvoiceNum")
    def paymentDate = column[Option[LocalDate]]("paymentDate")

    def * = (amount, productId, orderId, overnightShipping,itemPaid, paymentType,checkNumber,amountPaid,invoice,vendorInvoiceNum, paymentDate,(productName, productDescription, productSerialNumber, productImageUrl, productVendorId, productVendorName, productCategoryId, productCategoryName, productPrice, packageQuantity,unit)).shaped <> (
      { case (amount, productId, orderId, overnightShipping,itemPaid, paymentType,checkNumber,amountPaid,invoice, vendorInvoiceNum, paymentDate, (productName, productDescription, productSerialNumber, productImageUrl, productVendorId, productVendorName, productCategoryId, productCategoryName, price, packageQuantity, unit)) =>
        val category = for {
          categoryId <- productCategoryId
          categoryName <- productCategoryName
        } yield {
          OrderItemRow.Product.Category(categoryId, categoryName)
        }
        OrderItemRow(amount, productId, orderId, overnightShipping,itemPaid,paymentType,checkNumber,amountPaid,invoice,vendorInvoiceNum,paymentDate,
        OrderItemRow.Product(productName, productDescription, productSerialNumber, productImageUrl, productVendorId, productVendorName, category, price, packageQuantity,unit))
      },
      { orderItem: OrderItemRow =>
        val product = (
          orderItem.product.name,
          orderItem.product.description,
          orderItem.product.serialNumber,
          orderItem.product.imageUrl,
          orderItem.product.productVendorId,
          orderItem.product.productVendorName,
          orderItem.product.category.map(_.id),
          orderItem.product.category.map(_.name),
          orderItem.product.price,
          orderItem.product.packageQuantity,
          orderItem.product.unit
        )
      Some((orderItem.amount, orderItem.productId, orderItem.orderId, orderItem.overnightShipping, orderItem.itemPaid, orderItem.paymentType, orderItem.checkNumber,orderItem.amountPaid,orderItem.invoice, orderItem.vendorInvoiceNum, orderItem.paymentDate, product))
      }
    )
    def pk = primaryKey("pk_orderItems", (productId, orderId))
  }

  val OrderItemTable = TableQuery[OrderItemTable]

  // Consultations

  class ConsultationTable(tag: Tag) extends Table[ConsultationRow](tag, "consultations") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def data = column[JsValue]("data")
    def dateRequested = column[DateTime]("dateRequested")
    def status = column[ConsultationRow.Status]("status")
    def treatmentId = column[Option[Long]]("treatmentId")
    def staffId = column[Long]("staffId")
    def deleted = column[Boolean]("deleted")
    def amount = column[Option[Float]]("amount")
    def mode = column[Option[String]]("mode")
    def closedDate = column[Option[DateTime]]("closedDate")
    def customerProfileID = column[Option[Long]]("customerProfileID")
    def customerPaymentProfileId = column[Option[Long]]("customerPaymentProfileId")
    def transId = column[Option[Long]]("transId")
    def transactionCode = column[Option[String]]("transactionCode")
    def majorType = column[Option[String]]("majorType")
    def consultantId = column[Option[Long]]("consultantId")
    def consultArea = column[Option[String]]("consultArea")
    def * = (id.?, data, dateRequested, status, treatmentId, staffId, amount, mode,closedDate,customerProfileID,customerPaymentProfileId,transId,transactionCode,majorType,consultantId,consultArea) <> ((ConsultationRow.apply _).tupled, ConsultationRow.unapply)
  }

  val ConsultationTable = TableQuery[ConsultationTable]

  // Sessions

  class SessionTable(tag: Tag) extends Table[SessionRow](tag, "sessions") {
    def secret = column[String]("secret")
    def userId = column[Option[Long]]("userId")
    def staffId = column[Option[Long]]("staffId")
    def lastUsedDateTime = column[DateTime]("lastUsedDateTime")
    def expirationDateTime = column[DateTime]("expirationDateTime")
    def idleTimeoutNanos = column[Option[Long]]("idleTimeoutNanos")
    def * = (secret, userId, staffId, lastUsedDateTime, expirationDateTime, idleTimeoutNanos) <> (SessionRow.tupled, SessionRow.unapply)
  }

  val SessionTable = TableQuery[SessionTable]

  // PasswordChangeTokens

  class PasswordChangeTokenTable(tag: Tag) extends Table[PasswordChangeTokenRow](tag, "passwordChangeTokens") {
    def secret = column[String]("secret")
    def userId = column[Option[Long]]("userId")
    def staffId = column[Option[Long]]("staffId")
    def dateIssued = column[DateTime]("dateIssued")
    def * = (secret, userId, staffId, dateIssued) <> (PasswordChangeTokenRow.tupled, PasswordChangeTokenRow.unapply)
  }

  val PasswordChangeTokenTable = TableQuery[PasswordChangeTokenTable]

  // Courses

  class CoursesTable(tag: Tag) extends Table[CourseRow](tag, "courses") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def url = column[String]("url")
    def thumbnailUrl = column[Option[String]]("thumbnailUrl")
    def courseQuestionnaire = column[Option[String]]("courseQuestionnaire")
    def title = column[String]("title")
    def dateCreated = column[DateTime]("dateCreated")
    def category = column[Option[String]]("category")
    def courseId = column[Option[String]]("courseId")
    def speaker = column[Option[String]]("speaker")
    def educationType = column[EducationType]("educationType")

    def * = (id.?, title, url, thumbnailUrl, courseQuestionnaire, dateCreated, category, courseId, speaker, educationType) <> ((CourseRow.apply _).tupled, CourseRow.unapply)
  }

  val CoursesTable = TableQuery[CoursesTable]

  //Email mapping

  class EmailMappingTable(tag: Tag) extends Table[EmailMappingRow](tag, "emailMapping"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def title = column[String]("title")
    def isConsultation = column[Boolean]("isConsultation")
    def defaultEmail = column[String]("defaultEmail")
    def emailOrder = column[Option[Long]]("emailOrder")

    def * = (id.?, name, title, isConsultation, defaultEmail.?,emailOrder) <> ((EmailMappingRow.apply _).tupled, EmailMappingRow.unapply)
  }

  val EmailMappingTable = TableQuery[EmailMappingTable]

  // User EmailMappings junction table

  class UserEmailMappingTable(tag: Tag) extends Table[UserEmailMappingRow](tag, "userEmailMapping"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Long]("userId")
    def emailMappingId = column[Long]("emailMappingId")

    def * = (id.?, userId, emailMappingId) <> ((UserEmailMappingRow.apply _).tupled, UserEmailMappingRow.unapply)
  }

  val UserEmailMappingTable = TableQuery[UserEmailMappingTable]

  //Customer Favourites

  class CustomerFavouritesTable(tag: Tag) extends Table[CustomerFavouritesRow](tag, "customerFavourites"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def customer = column[Long]("customer")
    def product = column[Long]("product")

    def * = (id.?, customer, product) <> ((CustomerFavouritesRow.apply _).tupled, CustomerFavouritesRow.unapply)
  }
  val CustomerFavouritesTable = TableQuery[CustomerFavouritesTable]


  //Price Change History

  class PriceChangeHistoryTable(tag: Tag) extends Table[PriceChangeHistoryRow](tag, "priceChangeHistory"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def vendorId = column[Long]("vendorId")
    def priceIncreasePercent = column[Option[Double]]("priceIncreasePercent")
    def listPriceIncreasePercent = column[Option[Double]]("listPriceIncreasePercent")
    def userId = column[Long]("userId")
    def dateCreated = column[DateTime]("dateCreated")
    def categoryId = column[Option[Long]]("categoryId")
    def subCategoryId = column[Option[Long]]("subCategoryId")

    def * = (id.?, vendorId, priceIncreasePercent, listPriceIncreasePercent, userId,dateCreated,categoryId,subCategoryId) <> ((PriceChangeHistoryRow.apply _).tupled, PriceChangeHistoryRow.unapply)
  }
  val PriceChangeHistoryTable = TableQuery[PriceChangeHistoryTable]

  
  //ToothChart
  class ToothChartTable(tag: Tag) extends Table[ToothChartRow](tag, "toothChart"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def toothId = column[Int]("toothId")
    def toothConditions = column[String]("toothConditions")
    def staffId = column[Long]("staffId")
    def userId = column[Long]("userId")
    def dateUpdated = column[DateTime]("dateUpdated")
    def existingConditions = column[Option[String]]("existingConditions")

    def * = (id.?, patientId, toothId, toothConditions, staffId,userId,dateUpdated,existingConditions) <> ((ToothChartRow.apply _).tupled, ToothChartRow.unapply)
  }
  val ToothChartTable = TableQuery[ToothChartTable]


  //PerioChart
  class PerioChartTable(tag: Tag) extends Table[PerioChartRow](tag, "perioChart"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def rowPosition = column[String]("rowPosition")
    def perioType = column[String]("perioType")
    def dateSelected = column[LocalDate]("dateSelected")
    def perioNumbers = column[JsValue]("perioNumbers")
    def staffId = column[Long]("staffId")
    def userId = column[Long]("userId")
    def providerId = column[Option[Long]]("providerId")


    def * = (id.?, patientId, rowPosition, perioType, dateSelected, perioNumbers, staffId, userId, providerId) <> ((PerioChartRow.apply _).tupled, PerioChartRow.unapply)
  }
  val PerioChartTable = TableQuery[PerioChartTable]



  // Prescription Table

  class PrescriptionTable(tag: Tag) extends Table[PrescriptionRow](tag, "prescriptions") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def medication = column[String]("medication")
    def dosage = column[String]("dosage")
    def count = column[Option[Int]]("count")
    def instructions = column[Option[String]]("instructions")
    def refills = column[Int]("refills")
    def genericSubstituteOk = column[Option[Boolean]]("genericSubstituteOk")
    def deleted = column[Boolean]("deleted")
    def category = column[String]("category")
    def indications = column[String]("indications")
    def contraindications = column[String]("contraindications")
    def medicationInteraction = column[String]("medicationInteraction")
    def useInOralImplantology = column[String]("useInOralImplantology")


    def * = (id.?, medication, dosage, count, instructions, refills, genericSubstituteOk, deleted, category, indications, contraindications, medicationInteraction, useInOralImplantology) <> (PrescriptionRow.tupled, PrescriptionRow.unapply)
  }

  val PrescriptionTable = TableQuery[PrescriptionTable]


  // PrescriptionHistory Table

  class PrescriptionHistoryTable(tag: Tag) extends Table[PrescriptionHistoryRow](tag, "prescriptionHistory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def prescriptionId = column[Long]("prescriptionId")
    def patientId = column[Long]("patientId")
    def staffId = column[Long]("staffId")
    def dateCreated = column[LocalDate]("dateCreated")
    def deleted = column[Boolean]("deleted")
    def note = column[Option[String]]("note")

    def * = (id.?, prescriptionId, patientId, staffId, dateCreated,deleted, note) <> (PrescriptionHistoryRow.tupled, PrescriptionHistoryRow.unapply)
  }

  val PrescriptionHistoryTable = TableQuery[PrescriptionHistoryTable]

  // Resource Categories Table

  class ResourceCategoryTable(tag: Tag) extends Table[ResourceCategoryRow](tag, "resourceCategories") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def resourceType = column[String]("resourceType")
    def order = column[Int]("order")
    def parent = column[Option[Long]]("parent")
    def color = column[Option[String]]("color")

    def * = (id.?, name, resourceType, order, parent, color) <> (ResourceCategoryRow.tupled, ResourceCategoryRow.unapply)
  }

  val ResourceCategoryTable = TableQuery[ResourceCategoryTable]


  // Resource Sub Categories Table

  class ResourceSubCategoryTable(tag: Tag) extends Table[ResourceSubCategoryRow](tag, "resourceSubCategories") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def categoryId = column[Long]("categoryId")
    def name = column[String]("name")
    def resourceType = column[String]("resourceType")
    def order = column[Int]("order")

    def * = (id.?, categoryId, name, resourceType, order) <> (ResourceSubCategoryRow.tupled, ResourceSubCategoryRow.unapply)
  }

  val ResourceSubCategoryTable = TableQuery[ResourceSubCategoryTable]

  // Education Requests Table

  class EducationRequestTable(tag: Tag) extends Table[EducationRequestRow](tag, "educationRequests") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def courseId = column[Long]("courseId")
    def staffId = column[Long]("staffId")
    def note = column[Option[String]]("note")
    def dateCreated = column[DateTime]("dateCreated")
    def status = column[EducationRequestRow.Status]("status")
    def returnQuestionnaire = column[Option[String]]("returnQuestionnaire")
    def deleted = column[Boolean]("deleted")
    def closedDate = column[Option[LocalDate]]("closedDate")

    def * = (id.?, courseId, staffId, note, dateCreated, status, returnQuestionnaire, closedDate) <> ((EducationRequestRow.apply _).tupled, EducationRequestRow.unapply)
  }

  val EducationRequestTable = TableQuery[EducationRequestTable]


  class ExpiryNotificationTable(tag: Tag) extends Table[ExpiryNotificationRow](tag, "expiryNotifications") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def expiryDate = column[DateTime]("expiryDate")
    def dateCreated = column[DateTime]("dateCreated")

    def * = (id.?, staffId, expiryDate, dateCreated) <> ((ExpiryNotificationRow.apply _).tupled, ExpiryNotificationRow.unapply)
  }

  val ExpiryNotificationTable = TableQuery[ExpiryNotificationTable]

  class VendorPaymentTrackTable(tag: Tag) extends Table[VendorPaymentTrackRow](tag, "vendorPaymentTrack") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def orderId = column[Long]("orderId")
    def vendorId = column[Option[Long]]("vendorId")
    def paid = column[Boolean]("paid")
    def paymentType = column[Option[String]]("paymentType")
    def checkNumber = column[Option[String]]("checkNumber")
    def amountPaid = column[Option[Double]]("amountPaid")

    def * = (id.?, orderId, vendorId, paid,paymentType,checkNumber,amountPaid) <> ((VendorPaymentTrackRow.apply _).tupled, VendorPaymentTrackRow.unapply)
  }

  val VendorPaymentTrackTable = TableQuery[VendorPaymentTrackTable]


  class RenewalDetailsTable(tag: Tag) extends Table[RenewalDetailsRow](tag, "renewalDetails") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def subscriptionPeriod = column[Option[Int]]("subscriptionPeriod")
    def plan = column[Option[Long]]("plan")
    def renewalDate = column[DateTime]("renewalDate")
    def prevExpDate = column[DateTime]("prevExpDate")
    def newExpDate = column[DateTime]("newExpDate")
    def renewalType = column[String]("renewalType")
    def accountType = column[String]("accountType")
    def amountCollected = column[Option[Long]]("amountCollected")
    def collectedDate = column[Option[DateTime]]("collectedDate")
    def accessStartDate = column[Option[DateTime]]("accessStartDate")

    def * = (id.?, staffId, subscriptionPeriod, plan,renewalDate,prevExpDate,newExpDate,renewalType,accountType,amountCollected,collectedDate,accessStartDate) <> ((RenewalDetailsRow.apply _).tupled, RenewalDetailsRow.unapply)
  }

  val RenewalDetailsTable = TableQuery[RenewalDetailsTable]


  class CartTable(tag: Tag) extends Table[CartRow](tag, "cart") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def userId = column[Option[Long]]("userId")
    def productId = column[Option[Long]]("productId")
    def quantity = column[Option[Long]]("quantity")

    def * = (id.?, userId, productId, quantity) <> ((CartRow.apply _).tupled, CartRow.unapply)
  }

  val CartTable = TableQuery[CartTable]

  class ClinicalNotesTable(tag: Tag) extends Table[ClinicalNotesRow](tag, "clinicalNotes") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Option[Long]]("staffId")
    def patientId = column[Long]("patientId")
    def note = column[Option[String]]("note")
    def dateCreated = column[DateTime]("dateCreated")
    def date = column[Option[LocalDate]]("date")
    def time = column[Option[LocalTime]]("time")
    def provider = column[Option[Long]]("provider")
    def lock = column[Option[Boolean]]("lock")
    def assistant1 = column[Option[String]]("assistant1")
    def assistant2 = column[Option[String]]("assistant2")
    def procedureCode = column[Option[String]]("procedureCode")
    def procedureDescription = column[Option[String]]("procedureDescription")
    def bp = column[Option[String]]("bp")
    def pulse = column[Option[String]]("pulse")
    def spco2 = column[Option[String]]("spco2")
    def co2 = column[Option[String]]("co2")
    def ekg = column[Option[String]]("ekg")
    def resp = column[Option[String]]("resp")
    def temp = column[Option[String]]("temp")
    def allergies = column[Option[String]]("allergies")
    def medicalConditions = column[Option[String]]("medicalConditions")
    def postSurgicalHistory = column[Option[String]]("postSurgicalHistory")
    def monitors = column[Option[String]]("monitors")
    def asaStatus = column[Option[String]]("asaStatus")
    def airwayClass = column[Option[String]]("airwayClass")
    def anesthesia = column[Option[String]]("anesthesia")
    def diagnosis = column[Option[String]]("diagnosis")
    def dischargeDate = column[Option[LocalDate]]("dischargeDate")
    def dischargeTime = column[Option[LocalTime]]("dischargeTime")
    def brCompleted = column[Option[String]]("brCompleted")
    def brMidline = column[Option[String]]("brMidline")
    def brLipPostion = column[Option[String]]("brLipPostion")
    def brUpper = column[Option[String]]("brUpper")
    def brLower = column[Option[String]]("brLower")
    def impression = column[Option[String]]("impression")
    def escort = column[Option[String]]("escort")
    def lapsPhoto = column[Option[String]]("lapsPhoto")
    def surgeryPhoto = column[Option[String]]("surgeryPhoto")
    def balancedOcclusion = column[Option[String]]("balancedOcclusion")
    def anteriorGuidance = column[Option[String]]("anteriorGuidance")
    def canineGuidance = column[Option[String]]("canineGuidance")
    def fullDenture = column[Option[String]]("fullDenture")
    def existingDenture = column[Option[String]]("existingDenture")
    def newDenture = column[Option[String]]("newDenture")
    def pmmaProsthesis = column[Option[String]]("pmmaProsthesis")
    def compositeProsthesis = column[Option[String]]("compositeProsthesis")
    def prosthesesPhoto = column[Option[String]]("prosthesesPhoto")
    def medicationEntry = column[Option[String]]("medicationEntry")
    def medicationStability = column[Option[String]]("medicationStability")
    def vitalsPhoto = column[Option[String]]("vitalsPhoto")
    def preMedicationTotal = column[Option[String]]("preMedicationTotal")
    def patientTolerate = column[Option[String]]("patientTolerate")
    def suturingMaterials = column[Option[String]]("suturingMaterials")
    def suturingTechniques = column[Option[String]]("suturingTechniques")
    def reasonForNextAppointment = column[Option[String]]("reasonForNextAppointment")
    def procedure = column[Option[JsValue]]("procedure")
    def incision = column[Option[String]]("incision")
    def boneGraftMaterials = column[Option[String]]("boneGraftMaterials")
    def membranesUsed = column[Option[String]]("membranesUsed")
    def fixationSystemUsed = column[Option[String]]("fixationSystemUsed")
    def assistant3 = column[Option[String]]("assistant3")
    def verbalPostopIns = column[Option[String]]("verbalPostopIns")
    def writtenPostopIns = column[Option[String]]("writtenPostopIns")
    def rxVisit = column[Option[String]]("rxVisit")
    def incisionMandible = column[Option[String]]("incisionMandible")
    def voiceMemo = column[Option[String]]("voiceMemo")

    def * = (id.? :: staffId :: patientId :: note :: dateCreated :: date :: time :: provider :: lock :: assistant1 :: assistant2 :: procedureCode :: procedureDescription :: bp :: pulse :: spco2 :: co2 :: ekg :: resp :: temp :: allergies :: medicalConditions :: postSurgicalHistory :: monitors :: asaStatus :: airwayClass :: anesthesia :: diagnosis :: dischargeDate :: dischargeTime :: brCompleted :: brMidline :: brLipPostion :: brUpper :: brLower :: impression :: escort :: lapsPhoto :: surgeryPhoto :: balancedOcclusion :: anteriorGuidance :: canineGuidance :: fullDenture :: existingDenture :: newDenture :: pmmaProsthesis :: compositeProsthesis :: prosthesesPhoto :: medicationEntry :: medicationStability :: vitalsPhoto :: preMedicationTotal :: patientTolerate :: suturingMaterials :: suturingTechniques :: reasonForNextAppointment :: procedure :: incision :: boneGraftMaterials :: membranesUsed :: fixationSystemUsed :: assistant3 ::verbalPostopIns :: writtenPostopIns :: rxVisit :: incisionMandible :: voiceMemo :: HNil).mappedWith(Generic[ClinicalNotesRow])
  }

  val ClinicalNotesTable = TableQuery[ClinicalNotesTable]


  class OperatorTable(tag: Tag) extends Table[OperatorTableRow](tag, "operatories") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def name = column[String]("name")
    def deleted = column[Boolean]("deleted")
    def order = column[Long]("order")

    def * = (id.?,practiceId,name,deleted,order) <> ((OperatorTableRow.apply _).tupled, OperatorTableRow.unapply)
  }
  val OperatorTable = TableQuery[OperatorTable]


  class ProviderTable(tag: Tag) extends Table[ProviderTableRow](tag, "providers") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def title = column[String]("title")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def role = column[String]("role")
    def deleted = column[Boolean]("deleted")
    def color = column[String]("color")
    def licenseStateAndNumber = column[String]("licenseStateAndNumber")
    def licenseExpDate = column[Option[LocalDate]]("licenseExpDate")
    def malPracticeIns = column[String]("malPracticeIns")
    def malPracticeInsExpDate = column[Option[LocalDate]]("malPracticeInsExpDate")
    def deaNumber = column[String]("deaNumber")
    def deaExpDate = column[Option[LocalDate]]("deaExpDate")
    def npiNumber = column[String]("npiNumber")
    def emailAddress = column[String]("emailAddress")
    def phoneNumber = column[String]("phoneNumber")
    def ocscNumber = column[Option[String]]("ocscNumber")
    def ocscExpDate = column[Option[LocalDate]]("ocscExpDate")
    def hygienistLicenseNumber = column[Option[String]]("hygienistLicenseNumber")
    def hygienistLicenseExpDate = column[Option[LocalDate]]("hygienistLicenseExpDate")
    def gender = column[Option[String]]("gender")
    def bio = column[Option[String]]("bio")
    def profileImage = column[Option[String]]("profileImage")
    def services = column[Option[String]]("services")
    def languages = column[Option[String]]("languages")
    def dob = column[Option[LocalDate]]("dob")

    def * = (id.? :: practiceId :: title :: firstName :: lastName :: role :: deleted :: color :: licenseStateAndNumber :: licenseExpDate :: malPracticeIns :: malPracticeInsExpDate :: deaNumber :: deaExpDate :: npiNumber :: emailAddress :: phoneNumber :: ocscNumber :: ocscExpDate :: hygienistLicenseNumber :: hygienistLicenseExpDate :: gender :: bio :: profileImage :: services :: languages :: dob :: HNil).mappedWith(Generic[ProviderTableRow])
  }
  val ProviderTable = TableQuery[ProviderTable]


  class ProcedureTable(tag: Tag) extends Table[ProcedureCodeRow](tag, "procedureCode") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def procedureCode = column[String]("procedureCode")
    def procedureType = column[String]("procedureType")
    def categoryId = column[Option[Long]]("categoryId")
    def deleted = column[Boolean]("deleted")

    def * = (id.?,procedureCode,procedureType,categoryId,deleted) <> ((ProcedureCodeRow.apply _).tupled, ProcedureCodeRow.unapply)
  }
  val ProcedureTable = TableQuery[ProcedureTable]


  class WorkingHoursTable(tag: Tag) extends Table[WorkingHoursRow](tag, "workingHours") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def day = column[String]("day")
    def startTime = column[LocalTime]("startTime")
    def endTime = column[LocalTime]("endTime")
    def lunchStartTime = column[Option[LocalTime]]("lunchStartTime")
    def lunchEndTime = column[Option[LocalTime]]("lunchEndTime")
    def * = (id.?,practiceId,day,startTime,endTime,lunchStartTime,lunchEndTime) <> ((WorkingHoursRow.apply _).tupled, WorkingHoursRow.unapply)
  }
  val WorkingHoursTable = TableQuery[WorkingHoursTable]

  class NonWorkingDaysTable(tag: Tag) extends Table[NonWorkingDaysRow](tag, "nonWorkingDays") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def date = column[LocalDate]("date")

    def * = (id.?,practiceId,date) <> ((NonWorkingDaysRow.apply _).tupled, NonWorkingDaysRow.unapply)
  }
  val NonWorkingDaysTable = TableQuery[NonWorkingDaysTable]

  //TreatmentPlan
  class TreatmentPlanTable(tag: Tag) extends Table[TreatmentPlanRow](tag, "treatmentPlan") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def dateCreated = column[LocalDate]("dateCreated")
    def dateUpdated = column[LocalDate]("dateUpdated")
    def planName = column[String]("planName")
    def status = column[String]("status")
    def deleted = column[Boolean]("deleted")
    def createdBy = column[String]("createdBy")

    def * = (id.?,patientId,dateCreated,dateUpdated,planName,status,deleted,createdBy) <> ((TreatmentPlanRow.apply _).tupled, TreatmentPlanRow.unapply)
  }
  val TreatmentPlanTable = TableQuery[TreatmentPlanTable]

  //TreatmentPlanDetails
  class TreatmentPlanDetailsTable(tag: Tag) extends Table[TreatmentPlanDetailsRow](tag, "treatmentPlanDetails") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def treatmentPlanId = column[Long]("treatmentPlanId")
    def cdcCode = column[String]("cdcCode")
    def procedureName = column[String]("procedureName")
    def toothNumber = column[String]("toothNumber")
    def status = column[String]("status")
    def datePlanned = column[LocalDate]("datePlanned")
    def datePerformed = column[Option[LocalDate]]("datePerformed")
    def surface = column[String]("surface")
    def fee = column[Float]("fee")
    def estIns = column[Float]("estIns")
    def providerId = column[Option[Long]]("providerId")
    def insurancePaidAmt = column[Option[Float]]("insurancePaidAmt")
    def insurancePaidDate = column[Option[LocalDate]]("insurancePaidDate")
    def patientPaidAmt = column[Option[Float]]("patientPaidAmt")
    def patientPaidDate = column[Option[LocalDate]]("patientPaidDate")
    def insurancePayMethod = column[String]("insurancePayMethod")
    def patientPayMethod = column[String]("patientPayMethod")
    def patientId = column[Option[Long]]("patientId")
    def insurance = column[Option[Long]]("insurance")
    def patientBank = column[Option[String]]("patientBank")
    def refundAmount = column[Option[Float]]("refundAmount")
    def refundDate = column[Option[LocalDate]]("refundDate")
    def refundPayMethod = column[Option[String]]("refundPayMethod")
    def renewalDate = column[Option[LocalDate]]("renewalDate")
    def familyAnnualMax = column[Option[Float]]("familyAnnualMax")
    def individualAnnualMax = column[Option[Float]]("individualAnnualMax")
    def medicalInsuranceName = column[Option[String]]("medicalInsuranceName")
    def prognosis = column[Option[String]]("prognosis")
    def priority = column[Option[String]]("priority")
    def service = column[Option[String]]("service")
    def phase = column[Option[String]]("phase")

    def * = (id.? :: treatmentPlanId :: cdcCode :: procedureName :: toothNumber :: status :: datePlanned :: datePerformed :: surface :: fee :: estIns :: providerId :: insurancePaidAmt :: insurancePaidDate :: patientPaidAmt :: patientPaidDate :: insurancePayMethod :: patientPayMethod :: patientId :: insurance :: patientBank :: refundAmount :: refundDate :: refundPayMethod :: renewalDate :: familyAnnualMax :: individualAnnualMax :: medicalInsuranceName :: prognosis :: priority :: service :: phase :: HNil).mappedWith(Generic[TreatmentPlanDetailsRow])
  }
  val TreatmentPlanDetailsTable = TableQuery[TreatmentPlanDetailsTable]



  class AppointmentTable(tag: Tag) extends Table[AppointmentRow](tag, "appointment") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def date = column[LocalDate]("date")
    def startTime = column[LocalTime]("startTime")
    def endTime = column[LocalTime]("endTime")
    def patientId = column[Long]("patientId")
    def operatoryId = column[Long]("operatoryId")
    def providerId = column[Long]("providerId")
    def procedureType = column[String]("procedureType")
    def procedureCode = column[String]("procedureCode")
    def anaesthesia = column[String]("anaesthesia")
    def notes = column[String]("notes")
    def status = column[String]("status")
    def amountToBeCollected = column[Float]("amountToBeCollected")
    def deleted = column[Boolean]("deleted")
    def reason = column[String]("reason")
    def patientName = column[String]("patientName")
    def referredBy = column[String]("referredBy")
    def alerts = column[String]("alerts")
    def insurance = column[String]("insurance")
    def phoneNumber = column[String]("phoneNumber")
    def assistantId = column[Option[String]]("assistantId")
    def email = column[Option[String]]("email")
    def procedure = column[Option[String]]("procedure")
    def creator = column[Option[String]]("creator")
    def reminderSentDate = column[Option[LocalDate]]("reminderSentDate")

    def * = (id.? :: practiceId :: date :: startTime :: endTime :: patientId :: operatoryId :: providerId :: procedureType :: procedureCode :: anaesthesia :: notes :: status :: amountToBeCollected :: deleted :: reason :: patientName :: referredBy :: alerts :: insurance :: phoneNumber :: assistantId :: email :: procedure :: creator :: reminderSentDate :: HNil).mappedWith(Generic[AppointmentRow])
  }
  val AppointmentTable = TableQuery[AppointmentTable]

  class AppointmentLogTable(tag: Tag) extends Table[AppointmentLogRow](tag, "appointmentLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def appointmentId = column[Long]("appointmentId")
    def practiceId = column[Long]("practiceId")
    def date = column[LocalDate]("date")
    def startTime = column[LocalTime]("startTime")
    def endTime = column[LocalTime]("endTime")
    def patientId = column[Long]("patientId")
    def operatoryId = column[Long]("operatoryId")
    def providerId = column[Long]("providerId")
    def procedureType = column[String]("procedureType")
    def procedureCode = column[String]("procedureCode")
    def anaesthesia = column[String]("anaesthesia")
    def notes = column[String]("notes")
    def status = column[String]("status")
    def operation = column[String]("operation")
    def updatedBy = column[Long]("updatedBy")
    def updatedAt = column[DateTime]("updatedAt")
    def amountToBeCollected = column[Float]("amountToBeCollected")
    def reason = column[String]("reason")
    def patientName = column[String]("patientName")
    def referredBy = column[String]("referredBy")
    def alerts = column[String]("alerts")
    def insurance = column[String]("insurance")
    def phoneNumber = column[String]("phoneNumber")
    def assistantId = column[Option[String]]("assistantId")
    def email = column[Option[String]]("email")
    def procedure = column[Option[String]]("procedure")
    def creator = column[Option[String]]("creator")

def * = (id.? :: appointmentId :: practiceId :: date :: startTime :: endTime :: patientId :: operatoryId :: providerId :: procedureType :: procedureCode :: anaesthesia :: notes :: status :: operation :: updatedBy :: updatedAt :: amountToBeCollected :: reason :: patientName :: referredBy :: alerts :: insurance :: phoneNumber :: assistantId :: email :: procedure :: creator :: HNil).mappedWith(Generic[AppointmentLogRow])

}
  var AppointmentLogTable = TableQuery[AppointmentLogTable]

//Medical Condition Log
class MedicalConditionLogTable(tag: Tag) extends Table[MedicalConditionLogRow](tag, "medicalConditionLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def formId = column[String]("formId")
    def patientId = column[Long]("patientId")
    def value = column[String]("value")
    def conditionType = column[String]("conditionType")
    def dateCreated = column[LocalDate]("dateCreated")
    def currentlySelected = column[Boolean]("currentlySelected")

    def * = (id.?,formId,patientId,value,conditionType,dateCreated,currentlySelected) <> ((MedicalConditionLogRow.apply _).tupled, MedicalConditionLogRow.unapply)
  }
  var MedicalConditionLogTable = TableQuery[MedicalConditionLogTable]

  class MeasurementHistoryTable(tag: Tag) extends Table[MeasurementHistoryRow](tag, "measurementHistory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def formId = column[String]("formId")
    def systolic = column[Option[Double]]("systolic")
    def diastolic = column[Option[Double]]("diastolic")
    def pulse = column[Option[Double]]("pulse")
    def oxygen = column[Option[Double]]("oxygen")
    def bodyTemp = column[Option[Double]]("bodyTemp")
    def glucose = column[Option[Double]]("glucose")
    def a1c = column[Option[Double]]("a1c")
    def inr = column[Option[Double]]("inr")
    def pt = column[Option[Double]]("pt")
    def dateCreated = column[DateTime]("dateCreated")

    def * = (id.?, patientId, formId, systolic, diastolic, pulse, oxygen, bodyTemp, glucose, a1c, inr, pt, dateCreated) <> ((MeasurementHistoryRow.apply _).tupled, MeasurementHistoryRow.unapply)
  }


  val MeasurementHistoryTable = TableQuery[MeasurementHistoryTable]


  class SettingsTable(tag: Tag) extends Table[SettingsRow](tag, "settings") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def creditCardFee = column[Option[String]]("creditCardFee")

    def * = (id.?, creditCardFee) <> ((SettingsRow.apply _).tupled, SettingsRow.unapply)
  }

  val SettingsTable = TableQuery[SettingsTable]

  class StateTaxTable(tag: Tag) extends Table[StateTaxRow](tag, "stateTax") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def state = column[String]("state")
    def stateName = column[String]("stateName")
    def saleTax = column[Float]("saleTax")

    def * = (id.?, state,stateName,saleTax) <> ((StateTaxRow.apply _).tupled, StateTaxRow.unapply)
  }
  val StateTaxTable = TableQuery[StateTaxTable]

  class AppointmentReasonTable(tag: Tag) extends Table[AppointmentReasonRow](tag, "appointmentReason") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def reason = column[String]("reason")

    def * = (id.?, reason) <> ((AppointmentReasonRow.apply _).tupled, AppointmentReasonRow.unapply)
  }
  val AppointmentReasonTable = TableQuery[AppointmentReasonTable]


  class RoleTable(tag: Tag) extends Table[RoleRow](tag, "role") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def role = column[Option[String]]("role")

    def * = (id.?, role) <> ((RoleRow.apply _).tupled, RoleRow.unapply)
  }
  val RoleTable = TableQuery[RoleTable]

  class OrderLogTable(tag: Tag) extends Table[OrderLogRow](tag, "orderLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def orderId = column[Option[Long]]("orderId")
    def userAgent = column[Option[String]]("userAgent")

    def * = (id.?,orderId,userAgent) <> ((OrderLogRow.apply _).tupled, OrderLogRow.unapply)
  }
  val OrderLogTable = TableQuery[OrderLogTable]


  class SaleTaxTable(tag: Tag) extends Table[SaleTaxRow](tag, "saleTax") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def state = column[String]("state")
    def stateName = column[String]("stateName")
    def vendorId = column[Long]("vendorId")
    def tax = column[Float]("tax")

    def * = (id.?, state,stateName,vendorId,tax) <> ((SaleTaxRow.apply _).tupled, SaleTaxRow.unapply)
  }
  val SaleTaxTable = TableQuery[SaleTaxTable]


  class StateTable(tag: Tag) extends Table[StateRow](tag, "states") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def state = column[String]("state")
    def stateName = column[String]("stateName")

    def * = (id.?, state,stateName) <> ((StateRow.apply _).tupled, StateRow.unapply)
  }
  val StateTable = TableQuery[StateTable]

  class ListOptionTable(tag: Tag) extends Table[ListOptionsRow](tag, "listOptions") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def listType = column[String]("listType")
    def listOptions = column[String]("listOption")
    def listValue = column[String]("listValue")

    def * = (id.?, listType,listOptions,listValue) <> ((ListOptionsRow.apply _).tupled, ListOptionsRow.unapply)
  }
  val ListOptionTable = TableQuery[ListOptionTable]

class CartLogTable(tag: Tag) extends Table[CartLogRow](tag, "cartLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def dateCreated = column[LocalDate]("dateCreated")
    def userId = column[Option[Long]]("userId")
    def productId = column[Option[Long]]("productId")
    def productPrice = column[Option[Float]]("productPrice")
    def quantity = column[Option[Long]]("quantity")

    def * = (id.?,dateCreated,userId,productId,productPrice,quantity) <> ((CartLogRow.apply _).tupled, CartLogRow.unapply)
  }
  val CartLogTable = TableQuery[CartLogTable]

class ProcedureCodeCategoryTable(tag: Tag) extends Table[ProcedureCodeCategoryRow](tag, "procedureCodeCategory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def categoryName = column[String]("categoryName")

    def * = (id.?,categoryName) <> ((ProcedureCodeCategoryRow.apply _).tupled, ProcedureCodeCategoryRow.unapply)
  }
  val ProcedureCodeCategoryTable = TableQuery[ProcedureCodeCategoryTable]

class InsuranceCompanyTable(tag: Tag) extends Table[InsuranceCompanyRow](tag, "insuranceCompany") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def deleted = column[Boolean]("deleted")
    def companyOrder = column[Long]("companyOrder")

    def * = (id.?,name,deleted,companyOrder) <> ((InsuranceCompanyRow.apply _).tupled, InsuranceCompanyRow.unapply)
  }
  val InsuranceCompanyTable = TableQuery[InsuranceCompanyTable]

class InsuranceFeeTable(tag: Tag) extends Table[InsuranceFeeRow](tag, "insuranceFee") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def insuranceId = column[Long]("insuranceId")
    def procedureCode = column[String]("procedureCode")
    def fee = column[Float]("fee")

    def * = (id.?,practiceId,insuranceId,procedureCode,fee) <> ((InsuranceFeeRow.apply _).tupled, InsuranceFeeRow.unapply)
  }
  val InsuranceFeeTable = TableQuery[InsuranceFeeTable]

class StaffMembersTable(tag: Tag) extends Table[StaffMembersRow](tag, "staffMembers") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def title = column[String]("title")
    def empStartDate = column[LocalDate]("empStartDate")
    def empTerminationDate = column[Option[LocalDate]]("empTerminationDate")
    def color = column[String]("color")
    def rda = column[Option[String]]("rda")
    def expDateOfRda = column[Option[LocalDate]]("expDateOfRda")
    def deleted = column[Boolean]("deleted")
    def role = column[Option[String]]("role")
    def dob = column[Option[LocalDate]]("dob")

    def * = (id.?,practiceId,firstName,lastName,title,empStartDate,empTerminationDate,color,rda,expDateOfRda,deleted,role,dob) <> ((StaffMembersRow.apply _).tupled, StaffMembersRow.unapply)
  }
  val StaffMembersTable = TableQuery[StaffMembersTable]

  class SharedPatientTable(tag: Tag) extends Table[SharedPatientRow](tag, "sharedPatient") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def practiceId = column[Long]("practiceId")
    def shareTo = column[Long]("shareTo")
    def sharedBy = column[Long]("sharedBy")
    def sharedDate = column[LocalDate]("sharedDate")
    def mainSquares = column[String]("mainSquares")
    def icons = column[String]("icons")
    def accessTo = column[String]("accessTo")
    def accessLevel = column[String]("accessLevel")

    def * = (id.?,patientId,practiceId,shareTo,sharedBy,sharedDate,mainSquares,icons,accessTo,accessLevel) <> ((SharedPatientRow.apply _).tupled, SharedPatientRow.unapply)
  }
  val SharedPatientTable = TableQuery[SharedPatientTable]

  class MedicationEntryTable(tag: Tag) extends Table[MedicationEntryRow](tag, "medicationEntry") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def notesId = column[Long]("notesId")
    def medication = column[String]("medication")
    def dosage = column[String]("dosage")

    def * = (id.?,notesId,medication,dosage) <> ((MedicationEntryRow.apply _).tupled, MedicationEntryRow.unapply)
  }
  val MedicationEntryTable = TableQuery[MedicationEntryTable]


  class MedicationStabilityTable(tag: Tag) extends Table[MedicationStabilityRow](tag, "medicationStability") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def notesId = column[Long]("notesId")
    def implant = column[String]("implant")
    def implantBrand = column[String]("implantBrand")
    def diameter = column[String]("diameter")
    def boneDensity = column[String]("boneDensity")
    def length = column[String]("length")
    def ncm = column[String]("ncm")
    def isqBl = column[String]("isqBl")
    def isqMd = column[String]("isqMd")

    def * = (id.?,notesId,implant,implantBrand,diameter,boneDensity,length,ncm,isqBl,isqMd) <> ((MedicationStabilityRow.apply _).tupled, MedicationStabilityRow.unapply)
  }
  val MedicationStabilityTable = TableQuery[MedicationStabilityTable]


  class ErrorLogTable(tag: Tag) extends Table[ErrorLogRow](tag, "errorLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def router = column[String]("router")
    def input = column[String]("input")
    def fileName = column[String]("fileName")
    def errorMessage = column[String]("errorMessage")

    def * = (id.?,router,input,fileName,errorMessage) <> ((ErrorLogRow.apply _).tupled, ErrorLogRow.unapply)
  }
  val ErrorLogTable = TableQuery[ErrorLogTable]


  class ConEdTrackTable(tag: Tag) extends Table[ConEdTrackRow](tag, "conEdTrack") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def courseId = column[Long]("courseId")
    def date = column[LocalDate]("date")

    def * = (id.?,staffId,courseId,date) <> ((ConEdTrackRow.apply _).tupled, ConEdTrackRow.unapply)
  }
  val ConEdTrackTable = TableQuery[ConEdTrackTable]


  class ReferralSourceTable(tag: Tag) extends Table[ReferralSourceRow](tag, "referralSource") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def value = column[String]("value")
    def types = column[String]("types")
    def title = column[String]("title")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def archived = column[Boolean]("archived")

    def * = (id.?,practiceId,value,types,title,firstName,lastName,archived) <> ((ReferralSourceRow.apply _).tupled, ReferralSourceRow.unapply)
  }
  val ReferralSourceTable = TableQuery[ReferralSourceTable]


  class FamilyTable(tag: Tag) extends Table[FamilyRow](tag, "family") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def guarantorId = column[Option[Long]]("guarantorId")
    def guarantorFirstName = column[Option[String]]("guarantorFirstName")
    def guarantorLastName = column[Option[String]]("guarantorLastName")
    def guarantorGender = column[Option[String]]("guarantorGender")
    def guarantorEmail = column[Option[String]]("guarantorEmail")
    def guarantorPhone = column[Option[String]]("guarantorPhone")
    def guarantorDOB = column[Option[LocalDate]]("guarantorDOB")
    def guarantorSS = column[Option[String]]("guarantorSS")
    def patientType = column[Option[String]]("patientType")

    def * = (id.?,guarantorId,guarantorFirstName,guarantorLastName,guarantorGender,guarantorEmail,guarantorPhone,guarantorDOB,guarantorSS,patientType) <> ((FamilyRow.apply _).tupled, FamilyRow.unapply)
  }
  val FamilyTable = TableQuery[FamilyTable]

  class FamilyMembersTable(tag: Tag) extends Table[FamilyMembersRow](tag, "familyMembers") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def familyId = column[Long]("familyId")
    def patientId = column[Long]("patientId")

    def * = (id.?,familyId,patientId) <> ((FamilyMembersRow.apply _).tupled, FamilyMembersRow.unapply)
  }
  val FamilyMembersTable = TableQuery[FamilyMembersTable]


  class DentalCarrierTable(tag: Tag) extends Table[DentalCarrierRow](tag, "dentalCarrier") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def name = column[String]("name")
    def address = column[String]("address")
    def city = column[String]("city")
    def state = column[String]("state")
    def zip = column[String]("zip")
    def phone = column[String]("phone")
    def phone2 = column[String]("phone2")
    def payerId = column[String]("payerId")
    def claimType = column[String]("claimType")
    def website = column[String]("website")
    def contactPerson = column[String]("contactPerson")
    def insRefNumber = column[String]("insRefNumber")
    def notes = column[String]("notes")
    def feeSchedule = column[String]("feeSchedule")
    def archived = column[Boolean]("archived")

    def * = (id.?,practiceId,name,address,city,state,zip,phone,phone2,payerId,claimType,website,contactPerson,insRefNumber,notes,feeSchedule,archived) <> ((DentalCarrierRow.apply _).tupled, DentalCarrierRow.unapply)
  }
  val DentalCarrierTable = TableQuery[DentalCarrierTable]


class McKessonImportLogTable(tag: Tag) extends Table[McKessonImportLogRow](tag, "mckessonImportLog") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def date = column[LocalDate]("date")
    def offset = column[Long]("offset")
    def feedSize = column[Long]("feedSize")
    def status = column[String]("status")

    def * = (id.?,date,offset,feedSize,status) <> ((McKessonImportLogRow.apply _).tupled, McKessonImportLogRow.unapply)
  }
  val McKessonImportLogTable = TableQuery[McKessonImportLogTable]


  class SaveLaterTable(tag: Tag) extends Table[SaveLaterRow](tag, "saveLater") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def productId = column[Long]("productId")

    def * = (id.?,staffId,productId) <> ((SaveLaterRow.apply _).tupled, SaveLaterRow.unapply)
  }
  val SaveLaterTable = TableQuery[SaveLaterTable]


class VendorInvoiceTable(tag: Tag) extends Table[VendorInvoiceRow](tag, "vendorInvoice") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def vendorId = column[Long]("vendorId")
    def orderId = column[Long]("orderId")
    def vendorName = column[String]("vendorName")
    def invoiceNum = column[String]("invoiceNum")
    def paymentType = column[String]("paymentType")
    def amountPaid = column[Float]("amountPaid")
    def paymentDate = column[LocalDate]("paymentDate")
    def checkNumber = column[String]("checkNumber")
    def notes = column[Option[String]]("notes")
    def invoice = column[String]("invoice")

    def * = (id.?,vendorId,orderId,vendorName,invoiceNum,paymentType,amountPaid,paymentDate,checkNumber,notes,invoice) <> ((VendorInvoiceRow.apply _).tupled, VendorInvoiceRow.unapply)
  }
  val VendorInvoiceTable = TableQuery[VendorInvoiceTable]


  class SubscribeProductsTable(tag: Tag) extends Table[SubscribeProductsRow](tag, "subscribeProducts") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def productId = column[Long]("productId")
    def frequency = column[String]("frequency")
    def quantity = column[Long]("quantity")
    def shippingAddressId = column[Long]("shippingAddressId")
    def startDate = column[LocalDate]("startDate")
    def unsubscribed = column[Boolean]("unsubscribed")
    def endDate = column[Option[LocalDate]]("endDate")
    def customerPaymentProfileId = column[Long]("customerPaymentProfileId")
    def lastOrderedDate = column[LocalDate]("lastOrderedDate")
    def disabled = column[Boolean]("disabled")
    def shippingCharge = column[Float]("shippingCharge")
    def salesTax = column[Float]("salesTax")
    def grandTotal = column[Float]("grandTotal")
    def productPrice = column[Float]("productPrice")
    def totalAmountSaved = column[Float]("totalAmountSaved")

    def * = (id.?,staffId,productId,frequency,quantity,shippingAddressId,startDate,unsubscribed,endDate,customerPaymentProfileId,lastOrderedDate,disabled,shippingCharge,salesTax,grandTotal,productPrice,totalAmountSaved) <> ((SubscribeProductsRow.apply _).tupled, SubscribeProductsRow.unapply)
  }
  val SubscribeProductsTable = TableQuery[SubscribeProductsTable]


class SubscribeOrderHistoryTable(tag: Tag) extends Table[SubscribeOrderHistoryRow](tag, "subscribeOrderHistory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def subscriptionId = column[Long]("subscriptionId")
    def orderedDate = column[LocalDate]("orderedDate")
    def orderId = column[Long]("orderId")
    def customerProfileId = column[Long]("customerProfileId")
    def customerPaymentProfileId = column[Long]("customerPaymentProfileId")
    def transactionId = column[Long]("transactionId")
    def transactionCode = column[String]("transactionCode")
    def grandTotal = column[Float]("grandTotal")

    def * = (id.?,subscriptionId,orderedDate,orderId,customerProfileId,customerPaymentProfileId,transactionId,transactionCode,grandTotal) <> ((SubscribeOrderHistoryRow.apply _).tupled, SubscribeOrderHistoryRow.unapply)
  }
  val SubscribeOrderHistoryTable = TableQuery[SubscribeOrderHistoryTable]


class SubscribeOrderFailureHistoryTable(tag: Tag) extends Table[SubscribeOrderFailureHistoryRow](tag, "subscribeOrderFailureHistory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def subscriptionId = column[Long]("subscriptionId")
    def paymentDate = column[LocalDate]("paymentDate")
    def customerProfileId = column[Long]("customerProfileId")
    def customerPaymentProfileId = column[Long]("customerPaymentProfileId")
    def transactionId = column[Long]("transactionId")
    def transactionCode = column[String]("transactionCode")
    def grandTotal = column[Float]("grandTotal")

    def * = (id.?,subscriptionId,paymentDate,customerProfileId,customerPaymentProfileId,transactionId,transactionCode,grandTotal) <> ((SubscribeOrderFailureHistoryRow.apply _).tupled, SubscribeOrderFailureHistoryRow.unapply)
  }
  val SubscribeOrderFailureHistoryTable = TableQuery[SubscribeOrderFailureHistoryTable]


  class StaffNotesTable(tag: Tag) extends Table[StaffNotesRow](tag, "staffNotes") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[String]("staffId")
    def patientId = column[Long]("patientId")
    def date = column[LocalDate]("date")
    def notes = column[String]("notes")
    def time = column[Option[LocalTime]]("time")
    def notesFor = column[Option[String]]("notesFor")

    def * = (id.?,staffId,patientId,date,notes,time,notesFor) <> ((StaffNotesRow.apply _).tupled, StaffNotesRow.unapply)
  }
  val StaffNotesTable = TableQuery[StaffNotesTable]

  class NotificationsTable(tag: Tag) extends Table[NotificationsRow](tag, "notifications") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def message = column[String]("message")
    def dateCreated = column[LocalDate]("dateCreated")
    def deviceId = column[String]("deviceId")
    def status = column[String]("status")

    def * = (id.?, message,dateCreated,deviceId,status) <> ((NotificationsRow.apply _).tupled, NotificationsRow.unapply)
  }
  val NotificationsTable = TableQuery[NotificationsTable]

  class DeviceTable(tag: Tag) extends Table[DevicesRow](tag, "devices") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def deviceDetails = column[String]("deviceDetails")
    def deviceType = column[String]("deviceType")
    def deviceModel = column[String]("deviceModel")
    def deviceToken = column[String]("deviceToken")
    def lastLoginDate = column[LocalDate]("lastLoginDate")

    def * = (id.?, staffId,deviceDetails,deviceType,deviceModel,deviceToken,lastLoginDate) <> ((DevicesRow.apply _).tupled, DevicesRow.unapply)
  }
  val DeviceTable = TableQuery[DeviceTable]

  class ImplantDataTable(tag: Tag) extends Table[ImplantDataRow](tag, "implantData") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def noteId = column[Long]("noteId")
    def implantSite = column[String]("implantSite")
    def implantBrand = column[String]("implantBrand")
    def implantSurface = column[String]("implantSurface")
    def implantReference = column[String]("implantReference")
    def implantLot = column[String]("implantLot")
    def diameter = column[String]("diameter")
    def length = column[String]("length")
    def surgProtocol = column[String]("surgProtocol")
    def boneDensity = column[String]("boneDensity")
    def osteotomySite = column[String]("osteotomySite")
    def ncm = column[String]("ncm")
    def isqBl = column[String]("isqBl")
    def isqMd = column[String]("isqMd")
    def implantDepth = column[String]("implantDepth")
    def loadingProtocol = column[String]("loadingProtocol")
    def dateOfRemoval = column[String]("dateOfRemoval")
    def reasonForRemoval = column[String]("reasonForRemoval")
    def finalPA = column[String]("finalPA")
    def recommendedHT = column[String]("recommendedHT")
    def implantRestored = column[String]("implantRestored")
    def patientName = column[Option[String]]("patientName")
    def provider = column[Option[String]]("provider")
    def date = column[Option[LocalDate]]("date")
    def practiceId = column[Option[Long]]("practiceId")
    def staffId = column[Option[Long]]("staffId")
    def dob = column[Option[LocalDate]]("dob")

    def * = (id.? :: noteId :: implantSite :: implantBrand :: implantSurface :: implantReference :: implantLot :: diameter :: length :: surgProtocol :: boneDensity :: osteotomySite :: ncm :: isqBl :: isqMd :: implantDepth :: loadingProtocol :: dateOfRemoval :: reasonForRemoval :: finalPA :: recommendedHT :: implantRestored :: patientName :: provider :: date :: practiceId :: staffId :: dob :: HNil).mappedWith(Generic[ImplantDataRow])
  }
  val ImplantDataTable = TableQuery[ImplantDataTable]

  class MckessonProductsTable(tag: Tag) extends Table[MckessonProductsRow](tag, "mckessonProducts") {
    def itemId = column[String]("itemId")
    def units = column[String]("units")

    def * = (itemId,units) <> ((MckessonProductsRow.apply _).tupled, MckessonProductsRow.unapply)
  }
  val MckessonProductsTable = TableQuery[MckessonProductsTable]

  class ConfigTable(tag: Tag) extends Table[ConfigTableRow](tag, "config") {
    def label = column[String]("label")
    def value = column[String]("value")

    def * = (label,value) <> ((ConfigTableRow.apply _).tupled, ConfigTableRow.unapply)
  }
  val ConfigTable = TableQuery[ConfigTable]


  class PreMadeClinicalNotesTable(tag: Tag) extends Table[PreMadeClinicalNotesRow](tag, "preMadeClinicalNotes") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def name = column[String]("name")
    def note = column[String]("note")
    def deleted = column[Boolean]("deleted")

    def * = (id.?,practiceId,name,note,deleted) <> ((PreMadeClinicalNotesRow.apply _).tupled, PreMadeClinicalNotesRow.unapply)
  }
  val PreMadeClinicalNotesTable = TableQuery[PreMadeClinicalNotesTable]

  
  class OrderVendorDetailsTable(tag: Tag) extends Table[OrderVendorDetailsRow](tag, "orderVendorDetails") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def orderId = column[Long]("orderId")
    def vendorId = column[Long]("vendorId")
    def note = column[String]("note")

    def * = (id.?,orderId,vendorId,note) <> ((OrderVendorDetailsRow.apply _).tupled, OrderVendorDetailsRow.unapply)
  }
  val OrderVendorDetailsTable = TableQuery[OrderVendorDetailsTable]


  class TutorialsTable(tag: Tag) extends Table[TutorialsRow](tag, "tutorials") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def link = column[String]("link")
    def parent = column[Option[Long]]("parent")
    def order = column[Long]("order")
    def sectionType = column[Option[String]]("sectionType")

    def * = (id.?,name,link,parent,order,sectionType) <> ((TutorialsRow.apply _).tupled, TutorialsRow.unapply)
  }
  val TutorialsTable = TableQuery[TutorialsTable]


  class SaasTable(tag: Tag) extends Table[SaasRow](tag, "saas") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Option[Long]]("practiceId")
    def saasType = column[String]("saasType")
    def logo = column[String]("logo")
    def name = column[String]("name")
    def subStartDate = column[LocalDate]("subStartDate")
    def subExpDate = column[Option[LocalDate]]("subExpDate")
    def subscriptionType = column[Option[String]]("subscriptionType")
    def subscriptionFee = column[Option[Float]]("subscriptionFee")
    def notes = column[Option[String]]("notes")
    def autoRenew = column[Option[String]]("autoRenew")
    def documents = column[Option[String]]("documents")
    def deleted = column[Boolean]("deleted")

    def * = (id.?,practiceId,saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents,deleted) <> ((SaasRow.apply _).tupled, SaasRow.unapply)
  }
  val SaasTable = TableQuery[SaasTable]


  class VendorCategoriesTable(tag: Tag) extends Table[VendorCategoriesRow](tag, "vendorCategories") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def vendorId = column[Long]("vendorId")
    def categories = column[String]("categories")

    def * = (id.?,vendorId,categories) <> ((VendorCategoriesRow.apply _).tupled, VendorCategoriesRow.unapply)
  }
  val VendorCategoriesTable = TableQuery[VendorCategoriesTable]


  class DocumentTable(tag: Tag) extends Table[DocumentRow](tag, "document") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def documentType = column[String]("documentType")
    def documents = column[String]("documents")

    def * = (id.?,patientId,documentType,documents) <> ((DocumentRow.apply _).tupled, DocumentRow.unapply)
  }
  val DocumentTable = TableQuery[DocumentTable]



  class SaasPaymentTable(tag: Tag) extends Table[SaasPaymentRow](tag, "saasPayment") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def saasId = column[Long]("saasId")
    def practiceId = column[Long]("practiceId")
    def month = column[String]("month")
    def year = column[Long]("year")
    def amount = column[Float]("amount")

    def * = (id.?,saasId,practiceId,month,year,amount) <> ((SaasPaymentRow.apply _).tupled, SaasPaymentRow.unapply)
  }
  val SaasPaymentTable = TableQuery[SaasPaymentTable]

class EndoChartTable(tag: Tag) extends Table[EndoChartRow](tag, "endoChart"){
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def rowPosition = column[String]("rowPosition")
    def dateSelected = column[LocalDate]("dateSelected")
    def data = column[JsValue]("data")
    def staffId = column[Long]("staffId")
    def providerId = column[Option[Long]]("providerId")

    def * = (id.?, patientId, rowPosition, dateSelected, data, staffId, providerId) <> ((EndoChartRow.apply _).tupled, EndoChartRow.unapply)
  }
  val EndoChartTable = TableQuery[EndoChartTable]


  class SaasAdminTable(tag: Tag) extends Table[SaasAdminRow](tag, "saasAdmin") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def saasType = column[String]("saasType")
    def logo = column[String]("logo")
    def name = column[String]("name")
    def subStartDate = column[LocalDate]("subStartDate")
    def subExpDate = column[Option[LocalDate]]("subExpDate")
    def subscriptionType = column[Option[String]]("subscriptionType")
    def subscriptionFee = column[Option[Float]]("subscriptionFee")
    def notes = column[Option[String]]("notes")
    def autoRenew = column[Option[String]]("autoRenew")
    def documents = column[Option[String]]("documents")
    def deleted = column[Boolean]("deleted")

    def * = (id.?,saasType,logo,name,subStartDate,subExpDate,subscriptionType,subscriptionFee,notes,autoRenew,documents,deleted) <> ((SaasAdminRow.apply _).tupled, SaasAdminRow.unapply)
  }
  val SaasAdminTable = TableQuery[SaasAdminTable]

class SaasAdminPaymentTable(tag: Tag) extends Table[SaasAdminPaymentRow](tag, "saasAdminPayment") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def saasAdminId = column[Long]("saasAdminId")
    def month = column[String]("month")
    def year = column[Long]("year")
    def amount = column[Float]("amount")

    def * = (id.?,saasAdminId,month,year,amount) <> ((SaasAdminPaymentRow.apply _).tupled, SaasAdminPaymentRow.unapply)
  }
  val SaasAdminPaymentTable = TableQuery[SaasAdminPaymentTable]


  class CeHistoryTable(tag: Tag) extends Table[CeHistoryRow](tag, "ceHistory") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def courseId = column[Long]("courseId")
    def dateCreated = column[DateTime]("dateCreated")
    def action = column[String]("action")

    def * = (id.?, staffId,courseId, dateCreated, action) <> ((CeHistoryRow.apply _).tupled, CeHistoryRow.unapply)
  }
  val CeHistoryTable = TableQuery[CeHistoryTable]

  class ReferredToTable(tag: Tag) extends Table[ReferredToRow](tag, "referredTo") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def practiceId = column[Long]("practiceId")
    def name = column[String]("name")
    def types = column[String]("types")
    def title = column[String]("title")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def deleted = column[Boolean]("deleted")

    def * = (id.?, practiceId,name,types,title,firstName,lastName,deleted) <> ((ReferredToRow.apply _).tupled, ReferredToRow.unapply)
  }
  val ReferredToTable = TableQuery[ReferredToTable]

  class CeCoursePurchaseTable(tag: Tag) extends Table[CeCoursePurchaseRow](tag, "ceCoursePurchase") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def staffId = column[Long]("staffId")
    def courseId = column[Long]("courseId")
    def cost = column[Float]("cost")
    def purchasedDate = column[DateTime]("purchasedDate")
    def customerProfileId = column[Long]("customerProfileId")
    def customerPaymentProfileId  = column[Long]("customerPaymentProfileId")
    def transactionId = column[Long]("transactionId")
    def transactionCode = column[String]("transactionCode")

    def * = (id.?,staffId,courseId,cost,purchasedDate,customerProfileId,customerPaymentProfileId,transactionId,transactionCode) <> ((CeCoursePurchaseRow.apply _).tupled, CeCoursePurchaseRow.unapply)
  }
  val CeCoursePurchaseTable = TableQuery[CeCoursePurchaseTable]

  class ManualsTable(tag: Tag) extends Table[ManualsRow](tag, "manuals") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def link = column[String]("link")
    def parent = column[Option[Long]]("parent")
    def order = column[Long]("order")
    def sectionType = column[Option[String]]("sectionType")

    def * = (id.?,name,link,parent,order,sectionType) <> ((ManualsRow.apply _).tupled, ManualsRow.unapply)
  }
  val ManualsTable = TableQuery[ManualsTable]

  class DefaultFeeTable(tag: Tag) extends Table[DefaultFeeRow](tag, "defaultFee") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def procedureCode = column[String]("procedureCode")
    def ucrFee = column[Long]("ucr")
    def privateFee = column[Long]("private")

    def * = (id.?,procedureCode,ucrFee,privateFee) <> ((DefaultFeeRow.apply _).tupled, DefaultFeeRow.unapply)
  }
  val DefaultFeeTable = TableQuery[DefaultFeeTable]

  class AppointmentTreatmentPlanTable(tag: Tag) extends Table[AppointmentTreatmentPlanRow](tag, "appointmentTreatmentPlan") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def appointmentId = column[Long]("appointmentId")
    def treatmentPlanId = column[Long]("treatmentPlanId")

    def * = (id.?,appointmentId,treatmentPlanId) <> ((AppointmentTreatmentPlanRow.apply _).tupled, AppointmentTreatmentPlanRow.unapply)
  }
  val AppointmentTreatmentPlanTable = TableQuery[AppointmentTreatmentPlanTable]

  class FriendReferralTable(tag: Tag) extends Table[FriendReferralRow](tag, "friendReferral") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def title = column[String]("title")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def practiceName = column[String]("practiceName")
    def phone = column[String]("phone")
    def email = column[String]("email")
    def note = column[Option[String]]("note")
    def staffId = column[Option[Long]]("staffId")

    def * = (id.?,title,firstName,lastName,practiceName,phone,email,note,staffId) <> ((FriendReferralRow.apply _).tupled, FriendReferralRow.unapply)
  }
  val FriendReferralTable = TableQuery[FriendReferralTable]

  class OnlineRegInviteTrackTable(tag: Tag) extends Table[OnlineRegInviteTrackRow](tag, "onlineRegInviteTrack") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def sentDate = column[DateTime]("sentDate")

    def * = (id.?, patientId,sentDate) <> ((OnlineRegInviteTrackRow.apply _).tupled, OnlineRegInviteTrackRow.unapply)
  }
  val OnlineRegInviteTrackTable = TableQuery[OnlineRegInviteTrackTable]

  class AppointmentReminderTrackTable(tag: Tag) extends Table[AppointmentReminderTrackRow](tag, "appointmentReminderTrack") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def appointmentId = column[Long]("appointmentId")
    def dateTime = column[DateTime]("dateTime")
    def action = column[String]("action")
    def status = column[String]("status")
    def note = column[String]("note")
    def reason = column[String]("reason")

    def * = (id.?, appointmentId,dateTime,action,status,note,reason) <> ((AppointmentReminderTrackRow.apply _).tupled, AppointmentReminderTrackRow.unapply)
  }
  val AppointmentReminderTrackTable = TableQuery[AppointmentReminderTrackTable]


  class BoneGraftingDataTable(tag: Tag) extends Table[BoneGraftingDataRow](tag, "boneGraftingData") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def noteId = column[Long]("noteId")
    def practiceId = column[Long]("practiceId")
    def staffId = column[Long]("staffId")
    def providerName = column[String]("providerName")
    def patientName = column[String]("patientName")
    def date = column[Option[LocalDate]]("date")
    def dob = column[Option[LocalDate]]("dob")
    def allograftMaterialUsed = column[String]("allograftMaterialUsed")
    def allograftSize = column[String]("allograftSize")
    def allograftVolume = column[String]("allograftVolume")
    def allograftBrand = column[String]("allograftBrand")
    def xenograftMaterialUsed = column[String]("xenograftMaterialUsed")
    def xenograftSize = column[String]("xenograftSize")
    def xenograftVolume = column[String]("xenograftVolume")
    def xenograftBrand = column[String]("xenograftBrand")
    def alloplastMaterialUsed = column[String]("alloplastMaterialUsed")
    def alloplastSize = column[String]("alloplastSize")
    def alloplastVolume = column[String]("alloplastVolume")
    def alloplastBrand = column[String]("alloplastBrand")
    def autograftMaterialUsed = column[String]("autograftMaterialUsed")
    def autograftSize = column[String]("autograftSize")
    def autograftVolume = column[String]("autograftVolume")
    def membraneUsed = column[String]("membraneUsed")
    def membraneSize = column[String]("membraneSize")
    def membraneBrand = column[String]("membraneBrand")
    def additives = column[String]("additives")
    def additivesVolume = column[String]("additivesVolume")
    def additivesBrand = column[String]("additivesBrand")
    def finalPA = column[String]("finalPA")
    def rrpsd = column[Option[LocalDate]]("rrpsd")
    def failureDate = column[Option[LocalDate]]("failureDate")

    def * = ( id.? :: noteId :: practiceId :: staffId :: providerName :: patientName :: date :: dob :: allograftMaterialUsed :: allograftSize :: allograftVolume :: allograftBrand :: xenograftMaterialUsed :: xenograftSize :: xenograftVolume :: xenograftBrand :: alloplastMaterialUsed :: alloplastSize :: alloplastVolume :: alloplastBrand :: autograftMaterialUsed :: autograftSize :: autograftVolume :: membraneUsed :: membraneSize :: membraneBrand :: additives :: additivesVolume :: additivesBrand :: finalPA :: rrpsd :: failureDate :: HNil).mappedWith(Generic[BoneGraftingDataRow])
  }
  val BoneGraftingDataTable = TableQuery[BoneGraftingDataTable]


  class ProviderEducationTable(tag: Tag) extends Table[ProviderEducationRow](tag, "providerEducation") {
    def providerId = column[Long]("providerId")
    def degree = column[String]("degree")
    def college = column[String]("college")
    def educationYear = column[Long]("year")

    def * = (providerId,degree,college,educationYear) <> ((ProviderEducationRow.apply _).tupled, ProviderEducationRow.unapply)
  }
  val ProviderEducationTable = TableQuery[ProviderEducationTable]


  class ProviderExperienceTable(tag: Tag) extends Table[ProviderExperienceRow](tag, "providerExperience") {
    def providerId = column[Long]("providerId")
    def place = column[String]("place")
    def from = column[String]("from")
    def to = column[String]("to")

    def * = (providerId,place,from,to) <> ((ProviderExperienceRow.apply _).tupled, ProviderExperienceRow.unapply)
  }
  val ProviderExperienceTable = TableQuery[ProviderExperienceTable]


  class ProviderAwardsTable(tag: Tag) extends Table[ProviderAwardsRow](tag, "providerAwards") {
    def providerId = column[Long]("providerId")
    def name = column[String]("name")
    def awardsYear = column[Long]("year")

    def * = (providerId,name,awardsYear) <> ((ProviderAwardsRow.apply _).tupled, ProviderAwardsRow.unapply)
  }
  val ProviderAwardsTable = TableQuery[ProviderAwardsTable]


  class SubscriberTable(tag: Tag) extends Table[SubscriberRow](tag, "subscriber") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def `type` = column[String]("type")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")
    def dob = column[LocalDate]("dob")
    def address = column[String]("address")
    def city = column[String]("city")
    def state = column[String]("state")
    def zip = column[String]("zip")
    def phone = column[String]("phone")
    def ssn = column[String]("ssn")
    def relationship = column[String]("relationship")
    def carrierId = column[Long]("carrierId")
    def memberId = column[String]("memberId")
    def groupNumber = column[String]("groupNumber")
    def planName = column[String]("planName")
    def employerName = column[String]("employerName")
    def employerPhone = column[String]("employerPhone")

    def * = (id.?, patientId,`type`,firstName,lastName,dob,address,city,state,zip,phone,ssn,relationship,carrierId,memberId,groupNumber,planName,employerName,employerPhone) <> ((SubscriberRow.apply _).tupled, SubscriberRow.unapply)
  }
  val SubscriberTable = TableQuery[SubscriberTable]

  class EligibilityTable(tag: Tag) extends Table[EligibilityRow](tag, "eligibilityRequest") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def patientId = column[Long]("patientId")
    def requestDate = column[Option[LocalDate]]("requestDate")
    def receivedDate = column[Option[LocalDate]]("receivedDate")
    def subscriberType = column[String]("subscriberType")
    def subscriberChartNumber = column[String]("subscriberChartNumber")
    def eligibilityResponse = column[String]("eligibilityResponse")
    def activeCoverage = column[String]("activeCoverage")
    def resultCode = column[String]("resultCode")
    def resultDesc = column[String]("resultDesc")
    // case class EligibilityRow(
  // id: Option[Long],
  // patientId: Option[Long],
  // requestDate: Option[LocalDate],
  // receivedDate: Option[LocalDate],
  // subscriberType: Option[String],
  // subscriberChartNumber: Option[String],
  // eligibilityResponse: Option[String],
  // activeCoverage: Option[String],
  // resultCode: Option[String],
  // resultDesc: Option[String]
// )

    def * = (id.?, patientId,requestDate,receivedDate,subscriberType,subscriberChartNumber,eligibilityResponse,activeCoverage,resultCode,resultDesc) <> ((EligibilityRow.apply _).tupled, EligibilityRow.unapply)
  }
  val EligibilityTable = TableQuery[EligibilityTable]
}

