package models.daos

import co.spicefactory.db.PostgresDriver
import com.github.nscala_time.time.Imports
import models.daos.tables.TableDefinitions
import play.api.Application
import play.api.db.slick._
import play.api.libs.json.{Format, JsValue, Json}

import scala.reflect.ClassTag

trait DatabaseAccess extends HasDatabaseConfig[PostgresDriver] with TableDefinitions with Imports {

  implicit val application: Application

  protected lazy val dbConfig = DatabaseConfigProvider.get[PostgresDriver]

  import driver.api._

  private def jsonColumnType[A : Format : ClassTag](default: A) = MappedColumnType.base[A, JsValue](
    { value => Json.toJson(value) },
    { json => json.asOpt[A].getOrElse(default) }
  )

}
