package models.daos.silhouette

import com.google.inject._
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import models.daos.DatabaseAccess
import play.api.Application

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[DefaultPasswordInfoDAO])
trait PasswordInfoDAO extends DelegableAuthInfoDAO[PasswordInfo]

class DefaultPasswordInfoDAO @Inject() (
  implicit val application: Application,
  implicit val ec: ExecutionContext) extends PasswordInfoDAO with DatabaseAccess {

  import driver.api._

  /**
   * Finds the auth info which is linked with the specified login info.
   *
   * @param loginInfo The linked login info.
   * @return The retrieved auth info or None if no auth info could be retrieved for the given login info.
   */
  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = db.run {
    UserTable.filter(_.email === loginInfo.providerKey).take(1).result.headOption.flatMap[Option[PasswordInfo], NoStream, Nothing] {
      case Some(user) => DBIO.successful(Some(PasswordInfo("bcrypt", user.password, None)))
      case _ =>
        StaffTable.filter(_.email === loginInfo.providerKey).take(1).result.headOption.map {
          case Some(dentist) => Some(PasswordInfo("bcrypt", dentist.password, None))
          case _ => None
        }
    }
  }

  /**
   * Adds new auth info for the given login info.
   *
   * @param loginInfo The login info for which the auth info should be added.
   * @param authInfo The auth info to add.
   * @return The added auth info.
   */
  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    val actions = DBIO.seq(
      UserTable.filter(_.email === loginInfo.providerKey).map(_.password).update(authInfo.password),
      StaffTable.filter(_.email === loginInfo.providerKey).map(_.password).update(authInfo.password)
    )
    db.run(actions).map(_ => authInfo)
  }

  /**
   * Updates the auth info for the given login info.
   *
   * @param loginInfo The login info for which the auth info should be updated.
   * @param authInfo The auth info to update.
   * @return The updated auth info.
   */
  def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = add(loginInfo, authInfo)

  /**
   * Saves the auth info for the given login info.
   *
   * This method either adds the auth info if it doesn't exists or it updates the auth info
   * if it already exists.
   *
   * @param loginInfo The login info for which the auth info should be saved.
   * @param authInfo The auth info to save.
   * @return The saved auth info.
   */
  def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = update(loginInfo, authInfo)

  /**
   * Removes the auth info for the given login info.
   *
   * @param loginInfo The login info for which the auth info should be removed.
   * @return A future to wait for the process to be completed.
   */
  def remove(loginInfo: LoginInfo): Future[Unit] = Future.successful(Unit)

}



