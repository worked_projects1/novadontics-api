package models.daos.silhouette

import java.util.concurrent.TimeUnit

import com.google.inject._
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.repositories.{AuthenticatorRepository => SilhouetteAuthenticatorRepository}
import com.mohiva.play.silhouette.impl.authenticators.BearerTokenAuthenticator
import models.daos.DatabaseAccess
import models.daos.tables.SessionRow
import play.api.Application

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[DefaultAuthenticatorRepository])
trait AuthenticatorRepository extends SilhouetteAuthenticatorRepository[BearerTokenAuthenticator]

@Singleton
class DefaultAuthenticatorRepository @Inject()(
  implicit val application: Application,
  implicit val ec: ExecutionContext) extends AuthenticatorRepository with DatabaseAccess {

  import driver.api._

  override def find(id: String): Future[Option[BearerTokenAuthenticator]] = db.run {
    SessionTable.filter(_.secret === id)
      .joinLeft(UserTable).on(_.userId === _.id)
      .joinLeft(StaffTable).on(_._1.staffId === _.id)
      .result.headOption.map {
        _.flatMap {
          case ((SessionRow(_, _, _, lastUsedDateTime, expirationDateTime, idleTimeoutNanos), Some(user)), _) =>
            Some {
              BearerTokenAuthenticator(
                id,
                LoginInfo("credentials", user.email),
                lastUsedDateTime,
                expirationDateTime,
                idleTimeoutNanos.map(FiniteDuration(_, TimeUnit.NANOSECONDS))
              )
            }
          case ((SessionRow(_, _, _, lastUsedDateTime, expirationDateTime, idleTimeoutNanos), _), Some(dentist)) =>
            Some {
              BearerTokenAuthenticator(
                id,
                LoginInfo("credentials", dentist.email),
                lastUsedDateTime,
                expirationDateTime,
                idleTimeoutNanos.map(FiniteDuration(_, TimeUnit.NANOSECONDS))
              )
            }
          case _ =>
            None
        }
      }
  }

  override def update(authenticator: BearerTokenAuthenticator): Future[BearerTokenAuthenticator] = ???

  override def remove(id: String): Future[Unit] = db.run {
    SessionTable
      .filter(_.secret === id)
      .delete
      .map(_ => ())
  }

  override def add(authenticator: BearerTokenAuthenticator): Future[BearerTokenAuthenticator] = db.run {
    val userQuery = UserTable.filter(_.email === authenticator.loginInfo.providerKey).take(1).result.headOption
    val dentistQuery = StaffTable.filter(_.email === authenticator.loginInfo.providerKey).take(1).result.headOption

    userQuery.flatMap { user =>
      dentistQuery.flatMap { dentist =>
        SessionTable += SessionRow(
          authenticator.id,
          user.flatMap(_.id),
          dentist.flatMap(_.id),
          authenticator.lastUsedDateTime,
          authenticator.expirationDateTime,
          authenticator.idleTimeout.map(_.toNanos)
        )
      }
    }
  }.map(_ => authenticator)

}
