package models.forms

import play.api.libs.json.Json

case class FieldGroup(title: String, info: InfoBlock, fields: List[Field])

object FieldGroup {
  def apply(title: String, info: InfoBlock)(fields: Field*): FieldGroup = apply(title, info, fields.toList)
  implicit val writes = Json.writes[FieldGroup]
}
