package models.forms

import play.api.libs.json.Json

case class ColumnItem(field: Option[Field] = None, group: Option[FieldGroup] = None)

object ColumnItem {
  implicit val writes = Json.writes[ColumnItem]
}
