package models.forms

import play.api.libs.json.Json

case class Row(columns: List[Column])

object Row {
  def apply(columns: Column*): Row = apply(columns.toList)

  implicit val writes = Json.writes[Row]
}
