package models.forms

import play.api.libs.json.Json

case class InfoBlock(text: Option[String] = None, image: Option[String] = None)

object InfoBlock {
  implicit val writes = Json.writes[InfoBlock]
}
