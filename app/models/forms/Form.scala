package models.forms

import play.api.libs.json.Json

case class Form(id: String, title: String, icon: String, slides: List[Slide])

object Form {
  def apply(id: String, title: String, icon: String)(slides: Slide*): Form = apply(id, title, icon, slides.toList)
  implicit val writes = Json.writes[Form]
}
