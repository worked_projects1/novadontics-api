package models.forms

import play.api.libs.json.{Json, Writes}

sealed trait Field

object Field {

  case class Checkbox(name: String, title: String, attributes: Option[Checkbox.Attributes] = None) extends Field
  case class Input(name: String, title: String, info: Option[String] = None, prefix: Option[String] = None, postfix: Option[String] = None) extends Field
  case class Select(name: String, title: String, options: scala.Option[Select.Option] = None) extends Field
  case class Image(name: String, title: String) extends Field
  case class Images(name: String, title: String) extends Field
  case class FaceImages(name: String, title: String) extends Field
  case class PrintButton(title: String, url: String) extends Field
  case class InfoBlock(text: String, image: Option[String] = None) extends Field
  case class Title(text: String) extends Field

  object Checkbox {
    case class Attributes(
      options: Option[Field.Select.Option],
      images: Boolean,
      printTemplate: Option[String],
      info: Option[InfoBlock]
    )

    object Attributes {
      implicit val writes = Json.writes[Attributes]
    }

    implicit val writes = Json.writes[Checkbox]
  }

  object Input {
    implicit val writes = Json.writes[Input]
  }

  object Select {
    case class Option(value: String, title: String)

    object Option {
      implicit val writes: Writes[Option] = Json.writes[Option]
    }

    implicit val writes = Json.writes[Select]
  }

  object Image {
    implicit val writes = Json.writes[Image]
  }

  object Images {
    implicit val writes = Json.writes[Images]
  }

  object FaceImages {
    implicit val writes = Json.writes[FaceImages]
  }

  object PrintButton {
    implicit val writes = Json.writes[PrintButton]
  }

  object InfoBlock {
    implicit val writes = Json.writes[InfoBlock]
  }

  object Title {
    implicit val writes = Json.writes[Title]
  }

  implicit val writes: Writes[Field] = Writes {
    case field: Checkbox => Checkbox.writes.writes(field)
    case field: Input => Input.writes.writes(field)
    case field: Select => Select.writes.writes(field)
    case field: Image => Image.writes.writes(field)
    case field: Images => Images.writes.writes(field)
    case field: FaceImages => FaceImages.writes.writes(field)
    case field: PrintButton => PrintButton.writes.writes(field)
    case field: InfoBlock => InfoBlock.writes.writes(field)
    case field: Title => Title.writes.writes(field)
  }
}
