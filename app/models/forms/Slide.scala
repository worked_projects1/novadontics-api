package models.forms

import play.api.libs.json.Json

case class Slide(title: String, rows: List[Row])

object Slide {
  def apply(title: String)(rows: Row*): Slide = apply(title, rows.toList)
  implicit val writes = Json.writes[Slide]
}
