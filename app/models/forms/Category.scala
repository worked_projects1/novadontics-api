package models.forms

import play.api.libs.json.Json

case class Category(title: String, forms: List[Form])

object Category {
  def apply(title: String)(forms: Form*): Category = apply(title, forms.toList)
  implicit val writes = Json.writes[Category]
}
