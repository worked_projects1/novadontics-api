package models.forms

import play.api.libs.json.Json

case class Column(columnItems: List[ColumnItem])

object Column {
  def apply(columnItems: ColumnItem*): Column = apply(columnItems.toList)
  implicit val writes = Json.writes[Column]
}
