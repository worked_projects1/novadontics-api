package utils.db

import java.sql.{PreparedStatement, ResultSet, Timestamp}

import co.spicefactory.db.PostgresDriver
import com.github.nscala_time.time.Imports._
import org.joda.time.DateTime
import slick.driver.JdbcProfile
import slick.jdbc.{GetResult, PositionedParameters, PositionedResult, SetParameter}

trait FromTypeConverter[A, B] {
  def fromSqlType(a: A): B
}

trait ToTypeConverter[A, B] {

  def toSqlType(b: B): A

  def millisToSqlType(d: { def getTime: Long }): java.sql.Date = {
    import java.util.Calendar
    val cal = Calendar.getInstance()
    cal.setTimeInMillis(d.getTime)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    new java.sql.Date(cal.getTimeInMillis)
  }

}

trait SqlTypeConverter[A, B] extends FromTypeConverter[A, B]
  with ToTypeConverter[A, B]

trait JodaDateTimeSqlTimestampConverter
  extends SqlTypeConverter[String, DateTime] {

  val fromSqlTz = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZZ")
  val fromSql = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")

  def fromSqlType(t: String): DateTime = t match {
    case null => null
    case timestamp if timestamp.length < 24 => fromSql.parseDateTime(timestamp).withZoneRetainFields(DateTimeZone.UTC)
    case timestamp => fromSqlTz.parseDateTime(timestamp)
  }

  def toSqlType(t: DateTime): String = t match {
    case null => null
    case timestamp => timestamp.toString
  }

}

trait JodaGetResult[A, B] {
  self: SqlTypeConverter[A, B] =>

  def next(rs: PositionedResult): A

  def nextOption(rs: PositionedResult): Option[A]

  object getResult extends GetResult[B] {
    def apply(rs: PositionedResult) = fromSqlType(next(rs))
  }

  object getOptionResult extends GetResult[Option[B]] {
    def apply(rs: PositionedResult) = nextOption(rs).map(fromSqlType)
  }
}

trait JodaSetParameter[A, B] {
  self: SqlTypeConverter[A, B] =>

  def set(rs: PositionedParameters, d: A): Unit

  def setOption(rs: PositionedParameters, d: Option[A]): Unit

  object setJodaParameter extends SetParameter[B] {
    def apply(d: B, p: PositionedParameters) {
      set(p, toSqlType(d))
    }
  }

  object setJodaOptionParameter extends SetParameter[Option[B]] {
    def apply(d: Option[B], p: PositionedParameters) {
      setOption(p, d.map(toSqlType))
    }
  }
}

class JodaDateTimeMapper(val driver: JdbcProfile) {

  object TypeMapper extends driver.DriverJdbcType[DateTime]
    with JodaDateTimeSqlTimestampConverter {
    def zero = new DateTime(0L)
    def sqlType = java.sql.Types.TIMESTAMP_WITH_TIMEZONE
    override def setValue(v: DateTime, p: PreparedStatement, idx: Int): Unit =
      p.setTimestamp(idx, new Timestamp(v.getMillis))
    override def getValue(r: ResultSet, idx: Int): DateTime =
      fromSqlType(r.getString(idx))
    override def updateValue(v: DateTime, r: ResultSet, idx: Int): Unit =
      r.updateString(idx, toSqlType(v))
    override def valueToSQLLiteral(value: DateTime) = s"'${toSqlType(value)}'::timestamptz"
  }

  object JodaGetResult extends JodaGetResult[String, DateTime] with JodaDateTimeSqlTimestampConverter {
    def next(rs: PositionedResult): String = rs.nextString()
    def nextOption(rs: PositionedResult): Option[String] = rs.nextStringOption()
  }

  object JodaSetParameter extends JodaSetParameter[String, DateTime] with JodaDateTimeSqlTimestampConverter {
    def set(rs: PositionedParameters, d: String): Unit = rs.setString(d)
    def setOption(rs: PositionedParameters, d: Option[String]): Unit = rs.setStringOption(d)
  }

}

trait JodaSupport {

  val jdbcProfile = PostgresDriver

  protected val dateTimeMapperDelegate = new JodaDateTimeMapper(jdbcProfile)

  implicit val datetimeTypeMapper = dateTimeMapperDelegate.TypeMapper
  implicit val getDatetimeResult = dateTimeMapperDelegate.JodaGetResult.getResult
  implicit val getDatetimeOptionResult = dateTimeMapperDelegate.JodaGetResult.getOptionResult
  implicit val setDatetimeParameter = dateTimeMapperDelegate.JodaSetParameter.setJodaParameter
  implicit val setDatetimeOptionParameter = dateTimeMapperDelegate.JodaSetParameter.setJodaOptionParameter

}
