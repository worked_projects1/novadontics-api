organization := "co.spicefactory"

name := "ds-api"

version := "0.0.1-SNAPSHOT"

packageName in Universal := "ds-api"
sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

lazy val ds = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

scalacOptions in ThisBuild ++= Seq(
  "-encoding", "UTF-8",
  "-deprecation",         // warning and location for usages of deprecated APIs
  "-feature",             // warning and location for usages of features that should be imported explicitly
  "-unchecked",           // additional warnings where generated code depends on assumptions
  "-Xlint",               // recommended additional warnings
  "-Ywarn-adapted-args",  // Warn if an argument list is modified to match the receiver
  "-Ywarn-value-discard", // Warn when non-Unit expression results are unused
  "-Ywarn-inaccessible",
  "-Ywarn-dead-code",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:reflectiveCalls"
)

javacOptions in Compile ++= Seq(
  "-Xlint:unchecked",
  "-Xlint:deprecation"
)

libraryDependencies ++= Seq(
  filters,
  cache,
  ws,
  specs2 % Test,
  "io.github.nremond" %% "pbkdf2-scala" % "0.5",
  "com.typesafe.play" %% "play-json" % "2.5.4",
  "com.typesafe.play" %% "play-slick" % "2.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2",
  "org.postgresql" % "postgresql" % "9.4.1210",
  "com.github.tminglei" %% "slick-pg" % "0.14.4",
  "com.github.tminglei" %% "slick-pg_play-json" % "0.14.4",
  "com.github.tminglei" %% "slick-pg_joda-time" % "0.14.4",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.2.0",
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "com.chuusai" %% "shapeless" % "2.3.1",
  "joda-time" % "joda-time" % "2.9.3",
  "com.github.nscala-time" %% "nscala-time" % "2.12.0",
  "net.codingwell" %% "scala-guice" % "4.0.1",
  "com.mohiva" %% "play-silhouette" % "4.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "4.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "4.0.0",
  "com.mohiva" %% "play-silhouette-testkit" % "4.0.0" % "test",
  "com.iheart" %% "ficus" % "1.2.3",
  "org.julienrf" %% "play-json-derived-codecs" % "3.3",
  "org.scalaz" %% "scalaz-core" % "7.2.3",
  "com.github.julien-truffaut" %% "monocle-core" % "1.2.2",
  "com.github.julien-truffaut" %% "monocle-macro" % "1.2.2",
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.6",
  "com.itextpdf" % "itextpdf" % "5.5.10",
  "com.itextpdf.tool" % "xmlworker" % "5.4.4",
  "com.github.tototoshi" %% "scala-csv" % "1.3.6",
  "org.scala-lang.modules" %% "scala-java8-compat" % "0.7.0",
  "com.amazonaws" % "aws-java-sdk" % "1.11.70",
  "org.scalaj" % "scalaj-http_2.11" % "2.3.0"

//
//  "com.typesafe.slick" %% "slick"     % "3.1.1",
//  "com.chuusai"        %% "shapeless" % "2.3.3"
//  "io.underscore"      %% "slickless" % "0.3.2"
)

resolvers += Resolver.jcenterRepo

resolvers += Resolver.sonatypeRepo("releases")

resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"

resolvers += "Underscore Bintray" at "https://dl.bintray.com/underscoreio/libraries"

resolvers += "Maven Central" at "https://repo1.maven.org/maven2/"

resolvers += Resolver.sonatypeRepo("releases")
resolvers += "Typesafe Public Repo" at "http://repo.typesafe.com/typesafe/releases"
resolvers += Resolver.sonatypeRepo("snapshots")

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
