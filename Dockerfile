FROM anapsix/alpine-java:jre8

COPY target/universal/ds-api /app

EXPOSE 9000

CMD /app/bin/ds-api
