#!/usr/bin/env bash

export BASE_URL=http://ds.spfr.co && \
export ECR_BASE="977016222971.dkr.ecr.us-east-1.amazonaws.com" && \
export ECS_CLUSTER=confbot && \
rm -rf target/universal && \
bin/activator dist && \
unzip target/universal/ds-api.zip -d target/universal && \
docker build -t ds-api . && \
eval $(aws ecr get-login --no-include-email) && \
docker tag "ds-api:latest" "${ECR_BASE}/ds-api:latest" && \
docker push "${ECR_BASE}/ds-api:latest" && \
aws ecs stop-task --cluster ${ECS_CLUSTER} --task $(aws ecs list-tasks --cluster ${ECS_CLUSTER} --service-name ds-api | grep 'task/' | awk -F '[/"]' '{print $3}')
