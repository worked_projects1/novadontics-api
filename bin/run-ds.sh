#!/usr/bin/env bash
echo ">> Lets start docker machine so the Magic can start"
docker-machine create --driver virtualbox ds-dev
docker-machine start ds-dev
eval $(docker-machine env ds-dev)
# eval $(aws ecr get-login)


docker run -d rm --name ds-api --link ds-postgres:postgres.local -e DB_URL=jdbc:postgresql://postgres.local/ds ds-api
