#!/usr/bin/env bash

docker create --name ds-postgres -e POSTGRES_USER=ds -e POSTGRES_PASSWORD=ds -e POSTGRES_DB=ds -p 5432:5432 library/postgres