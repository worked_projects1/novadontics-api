#!/usr/bin/env bash

rm -rf target/universal && bin/activator dist && unzip target/universal/ds-api.zip -d target/universal \
  && docker build -t ds-api .
