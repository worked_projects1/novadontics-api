#!/usr/bin/env bash

PROJECT_ROOT=..
CURRENT_PATH=$(pwd)

source run-postgres.sh

cd ${PROJECT_ROOT}

./bin/activator test

cd ${CURRENT_PATH}

source stop-postgres.sh
