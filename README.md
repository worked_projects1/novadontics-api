![Spicefactory image](https://avatars2.githubusercontent.com/u/13365608?v=3&s=200)

# Novadontics API

Helping dentists across the world. API supports Novadontics Web and iOS apps.

## Domain Overview

* There are two types of identities: **User** and **Dentist**.

* **Users** are Novadontics’ managers/admins/owners, using the Web application.
 
* **Dentists** are Novadontics’ clients, using the iOS application.

* The core entities are: **Product**, **Order**, and **Step**.

## Running in Development

Make sure you have the docker installed and running.

Run the database

```
bin/run-postgres.sh
```

And then run the app

```
bin/run-app.sh
```
