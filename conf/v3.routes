# Routes
# This file defines all api routes (Higher priority routes first)
# ~~~~

# Session

GET           /healthcheck                                            @controllers.v1.HealthCheckController.check

GET           /session                                                @controllers.v1.SessionController.check
PUT           /session                                                @controllers.v1.SessionController.logIn
DELETE        /session                                                @controllers.v1.SessionController.logOut
PUT           /session/signUpRequest                                  @controllers.v1.SessionController.signUpRequest


PUT           /forgot/:email                                          @controllers.v1.PasswordController.forgot(email: String)
GET           /reset/:secret                                          @controllers.v1.PasswordController.check(secret: String)
PUT           /reset/:secret                                          @controllers.v1.PasswordController.reset(secret: String)
PUT           /password                                               @controllers.v1.PasswordController.change

GET           /analytics                                              @controllers.v3.AnalyticsController.read

GET           /categories                                             @controllers.v2.CategoriesController.readAll
PUT           /categories                                             @controllers.v2.CategoriesController.bulkUpdate
PUT           /categories/:id                                         @controllers.v2.CategoriesController.update(id: Long)
DELETE        /categories/:id                                         @controllers.v2.CategoriesController.delete(id: Long)

GET           /products                                               @controllers.v3.ProductsController.readAll
POST          /products                                               @controllers.v3.ProductsController.create
GET           /products/:id                                           @controllers.v3.ProductsController.read(id: Long)
PUT           /products/:id                                           @controllers.v3.ProductsController.update(id: Long)
PUT           /products                                               @controllers.v3.ProductsController.bulkUpdate
DELETE        /products/:id                                           @controllers.v3.ProductsController.delete(id: Long)
GET           /products/generatePdf/:id/pdf                        	  @controllers.v3.ProductsController.generatePdf(id: Long)

POST          /products/:id/favourite                                 @controllers.v2.FavouritesController.add(id: Long)
DELETE        /products/:id/favourite                                 @controllers.v2.FavouritesController.remove(id: Long)

GET           /vendors                                                @controllers.v2.VendorController.readAll
POST          /vendors                                                @controllers.v2.VendorController.create
GET           /vendors/:id                                            @controllers.v2.VendorController.read(id: Long)
PUT           /vendors/:id                                            @controllers.v2.VendorController.update(id: Long)
DELETE        /vendors/:id                                            @controllers.v2.VendorController.archive(id: Long)
PUT           /vendors/:id/unarchive                                  @controllers.v2.VendorController.unarchive(id: Long)

GET           /orders                                                 @controllers.v1.OrdersController.readAll
POST          /orders                                                 @controllers.v1.OrdersController.create
GET           /orders/:id                                             @controllers.v1.OrdersController.read(id: Long)
PUT           /orders/:id                                             @controllers.v1.OrdersController.update(id: Long)
DELETE        /orders/:id                                             @controllers.v1.OrdersController.delete(id: Long)
PUT           /orders/updateVendorPayment/:id                         @controllers.v1.OrdersController.updateVendorPayment(id: Long)

GET           /treatments                                             @controllers.v1.TreatmentsController.readAll
POST          /treatments                                             @controllers.v1.TreatmentsController.create
GET           /treatments/:id                                         @controllers.v1.TreatmentsController.read(id: Long)
PUT           /treatments/:id                                         @controllers.v1.TreatmentsController.update(id: Long)
DELETE        /treatments/:id                                         @controllers.v1.TreatmentsController.delete(id: Long)
GET           /treatments/:id/pdf                                     @controllers.v1.TreatmentsController.pdf(id: Long, step: Option[String])
PUT           /treatments/updateCtScan/:id                            @controllers.v1.TreatmentsController.updateCtScan(id: Long)

GET           /patients                                               @controllers.v1.PatientsController.readAll
GET           /patients/:id                                           @controllers.v1.PatientsController.read(id: Long)

GET           /consultationsFilter/:majorType                         @controllers.v1.ConsultationsController.readAll(majorType: String)
POST          /consultations                                          @controllers.v1.ConsultationsController.create
GET           /consultations/:id                                      @controllers.v1.ConsultationsController.read(id: Long)
PUT           /consultations/:id                                      @controllers.v1.ConsultationsController.update(id: Long)
DELETE        /consultations/:id                                      @controllers.v1.ConsultationsController.delete(id: Long)

GET           /users                                                  @controllers.v1.UsersController.readAll
POST          /users                                                  @controllers.v1.UsersController.create
GET           /users/:id                                              @controllers.v1.UsersController.read(id: Long)
PUT           /users/:id                                              @controllers.v1.UsersController.update(id: Long)
DELETE        /users/:id                                              @controllers.v1.UsersController.delete(id: Long)

GET           /accounts                                               @controllers.v1.StaffController.readAll(practice: Option[Long])
POST          /accounts                                               @controllers.v1.StaffController.create
GET           /accounts/:id                                           @controllers.v1.StaffController.read(id: Long)
PUT           /accounts/:id                                           @controllers.v1.StaffController.update(id: Long)
POST          /accounts/udid                                          @controllers.v1.StaffController.setUDID
DELETE        /accounts/:id                                           @controllers.v1.StaffController.delete(id: Long)
PUT           /accounts/:id/extend                                    @controllers.v1.StaffController.extendExpirationDate(id: Long)
PUT           /accounts/:id/extend1                                   @controllers.v1.StaffController.extendExpirationDate1(id: Long)
DELETE        /accounts/clearUDID/:email                              @controllers.v1.StaffController.clearUDID(email: String)

GET           /renewalDetails/:id                                     @controllers.v1.StaffController.readAllRenewalDetails(id: Long)
PUT           /renewalDetails/:id                                     @controllers.v1.StaffController.updateRenewalDetails(id: Long)

GET           /profile                                                @controllers.v2.ProfileController.read
PUT           /profile                                                @controllers.v2.ProfileController.update

GET           /practices                                              @controllers.v1.PracticeController.readAll
POST          /practices                                              @controllers.v1.PracticeController.create
GET           /practices/:id                                          @controllers.v1.PracticeController.read(id: Long)
PUT           /practices/:id                                          @controllers.v1.PracticeController.update(id: Long)
DELETE        /practices/:id                                          @controllers.v1.PracticeController.delete(id: Long)

GET           /courses/:resource                                      @controllers.v3.CoursesController.readAll(resource: String)
GET           /courses/:resource/:id                                  @controllers.v3.CoursesController.read(resource: String, id: Long)
POST          /courses/:resource                                      @controllers.v3.CoursesController.create(resource: String)
PUT           /courses/:resource/:id                                  @controllers.v3.CoursesController.update(resource: String, id: Long)
DELETE        /courses/:resource/:id                                  @controllers.v3.CoursesController.delete(resource: String, id: Long)

GET           /signature/:name                                        @controllers.v1.UploadsController.signature(name: String)

GET           /emailMappings                                          @controllers.v2.EmailMappingController.readAll
POST          /emailMappings                                          @controllers.v2.EmailMappingController.create
GET           /emailMappings/:id                                      @controllers.v2.EmailMappingController.read(id: Long)
PUT           /emailMappings/:id                                      @controllers.v2.EmailMappingController.update(id: Long)
DELETE        /emailMappings/:id                                      @controllers.v2.EmailMappingController.delete(id: Long)
PUT           /emailMappings/:id/add                                  @controllers.v2.EmailMappingController.addUser(id: Long, userId: Long)
PUT           /emailMappings/:id/rem                                  @controllers.v2.EmailMappingController.removeUser(id: Long, userId: Long)

GET           /shipping                                               @controllers.v2.ShippingController.readAll
POST          /shipping                                               @controllers.v2.ShippingController.create
GET           /shipping/:id                                           @controllers.v2.ShippingController.read(id: Long)
PUT           /shipping/:id                                           @controllers.v2.ShippingController.update(id: Long)
DELETE        /shipping/:id                                           @controllers.v2.ShippingController.delete(id: Long)

GET           /forms                                                  @controllers.v2.FormsController.readForms
GET           /consents                                               @controllers.v2.FormsController.readAllConsents
PUT           /consents/:id                                           @controllers.v2.FormsController.updateConsent(id: String)
POST          /consents                                               @controllers.v2.FormsController.addConsent

GET           /prescriptions                                          @controllers.v2.PrescriptionController.readAll
POST          /prescriptions                                          @controllers.v2.PrescriptionController.create
DELETE        /prescriptions/:id                                      @controllers.v2.PrescriptionController.delete(id: Long)
GET           /prescription/:id/pdf                                   @controllers.v2.PrescriptionController.generatePdf(id: Long, treatment: String, signature: String, timezone: Option[String], providerId: Long)


GET           /resource-categories/:resourceType                      @controllers.v2.ResourceCategoriesController.readAll(resourceType: String)
GET           /resource-categories/:resourceType/:id                  @controllers.v2.ResourceCategoriesController.read(resourceType: String, id: Long)
POST          /resource-categories/:resourceType                      @controllers.v2.ResourceCategoriesController.create(resourceType: String)
PUT           /resource-categories/:resourceType                      @controllers.v2.ResourceCategoriesController.bulkUpdate(resourceType: String)
PUT           /resource-categories/:resourceType/:id                  @controllers.v2.ResourceCategoriesController.update(resourceType: String, id: Long)
DELETE        /resource-categories/:resourceType/:id                  @controllers.v2.ResourceCategoriesController.delete(resourceType: String, id: Long)



GET           /educationRequests                                      @controllers.v2.EducationRequestController.readAll
GET           /educationRequests/:id                                  @controllers.v2.EducationRequestController.read(id: Long)
PUT           /educationRequests/:id                                  @controllers.v2.EducationRequestController.update(id: Long)
DELETE        /educationRequests/:id                                  @controllers.v2.EducationRequestController.delete(id: Long)
POST          /educationRequests                                      @controllers.v2.EducationRequestController.create

GET           /cart                                                   @controllers.v3.CartController.readAll
POST          /cart                                                   @controllers.v3.CartController.update
DELETE        /cart/:id                                               @controllers.v3.CartController.delete(id: Long)

GET           /resource-sub-categories/:resourceType/:categoryId      @controllers.v3.ResourceSubCategoriesController.readAll(resourceType: String, categoryId: Long)
GET           /resource-sub-categories/:resourceType/subCategory/:id  @controllers.v3.ResourceSubCategoriesController.read(resourceType: String, id: Long)
POST          /resource-sub-categories/:resourceType                  @controllers.v3.ResourceSubCategoriesController.create(resourceType: String)
PUT           /resource-sub-categories/:resourceType/:id              @controllers.v3.ResourceSubCategoriesController.update(resourceType: String, id: Long)
DELETE        /resource-sub-categories/:resourceType/:id              @controllers.v3.ResourceSubCategoriesController.delete(resourceType: String, id: Long)


