# --- !Ups
update "emailMapping" set "defaultEmail"='team@novadontics.com' where name='expiryNotifications';
alter table "renewalDetails" add column "accountType" varchar DEFAULT 'Customer';

# --- !Downs
update "emailMapping" set "defaultEmail"='team@novadontics.com' where name='expiryNotifications';
alter table "renewalDetails" drop column "accountType";
