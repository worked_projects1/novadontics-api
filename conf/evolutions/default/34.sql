# --- !Ups
ALTER TABLE "staff" ADD COLUMN "country" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "city" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "state" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "zip" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "address" VARCHAR;
ALTER TABLE "staff" ADD COLUMN "expirationDate" TIMESTAMP;

UPDATE "staff" SET "expirationDate" = "dateCreated" + interval '1 year';

# --- !Downs
ALTER TABLE "staff" DROP COLUMN "country";
ALTER TABLE "staff" DROP COLUMN "city";
ALTER TABLE "staff" DROP COLUMN "state";
ALTER TABLE "staff" DROP COLUMN "zip";
ALTER TABLE "staff" DROP COLUMN "address";
ALTER TABLE "staff" DROP COLUMN "expirationDate";