# --- !Ups

CREATE TABLE "vendors" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "deleted" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id")
);

INSERT INTO "vendors" ("name")
  SELECT DISTINCT "manufacturer" FROM "products";

ALTER TABLE "products"
  ADD COLUMN "vendorId" BIGINT;

UPDATE "products"
  SET "vendorId" = "vendors"."id"
  FROM "vendors"
  WHERE "vendors"."name" = "products"."manufacturer";

ALTER TABLE "products"
  ALTER COLUMN "vendorId" SET NOT NULL,
  ADD FOREIGN KEY ("vendorId") REFERENCES "vendors" ("id");

ALTER TABLE "products"
  DROP COLUMN "manufacturer";

ALTER TABLE "orderItems"
  ADD COLUMN "productVendorId" BIGINT REFERENCES "vendors" ("id"),
  ADD COLUMN "productVendorName" VARCHAR;

UPDATE "orderItems"
  SET "productVendorName" = "productManufacturer";

UPDATE "orderItems"
  SET "productVendorId" = "vendors"."id"
  FROM "vendors"
  WHERE "vendors"."name" = "orderItems"."productVendorName";

ALTER TABLE "orderItems"
  ALTER COLUMN "productVendorName" SET NOT NULL;

ALTER TABLE "orderItems"
  DROP COLUMN "productManufacturer";

# --- !Downs

ALTER TABLE "orderItems"
  ADD COLUMN "productManufacturer" VARCHAR;

UPDATE "orderItems"
  SET "productManufacturer" = "productVendorName";

ALTER TABLE "orderItems"
  ALTER COLUMN "productManufacturer" SET NOT NULL;

ALTER TABLE "orderItems"
  DROP COLUMN "productVendorId",
  DROP COLUMN "productVendorName";

ALTER TABLE "products"
  ADD COLUMN "manufacturer" VARCHAR;

UPDATE "products"
  SET "manufacturer" = "vendors"."name"
  FROM "vendors"
  WHERE "vendors"."id" = "products"."vendorId";

ALTER TABLE "products"
  ALTER COLUMN "manufacturer" SET NOT NULL;

ALTER TABLE "products"
  DROP COLUMN "vendorId";

DROP TABLE "vendors";
