# --- !Ups
ALTER TABLE "courses" ALTER COLUMN "thumbnailUrl" DROP NOT NULL;

INSERT INTO "courses"("title", "url", "dateCreated", "category", "educationType")
  SELECT "title", "url", "dateCreated", "category", 'Resource' FROM "documents";

DROP TABLE "documents";

# --- !Downs
CREATE TABLE "documents" (
  "id" BIGSERIAL NOT NULL,
  "title" VARCHAR NOT NULL,
  "url" VARCHAR NOT NULL,
  "category" VARCHAR,
  "dateCreated" TIMESTAMP NOT NULL,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("category") REFERENCES "resourceCategories" ("name") ON UPDATE CASCADE ON DELETE SET NULL
);

INSERT INTO "documents"("title", "url", "dateCreated", "category")
  SELECT "title", "url", "dateCreated", "category" FROM "courses" WHERE "courses"."educationType" = 'Resource';