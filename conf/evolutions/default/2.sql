# --- !Ups
CREATE TABLE "documents" (
  "id" BIGSERIAL NOT NULL,
  "title" VARCHAR NOT NULL,
  "url" VARCHAR NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,

  PRIMARY KEY ("id")
);

INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('How you can start with the All-on-4 treatment concept', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/1+All+on+4+concept+.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Nobel Esthetics - Procedures manual', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/70190M_NobelEsthetics+Proc+Prod+2010.1+GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Rehabilitation concepts for edentulous patients', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/73176F_Rehabilitation+concepts+for+edentulous+patients_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelReplace and Replace Select Tapered - Procedures manual', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/75434C_NoblRep_Rep+Sel+Tap+Manual+13.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Rescue instrumentation - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/78423A_Rescue+Instr+Product+Overview15.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Rescue instrumentation - Procedures manual', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/79127_Rescue+Manual_15.2_GB_C2.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelActive - Procedures manual', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/79276_NobelActive+Manual+15.2_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Cleaning and sterilization - Guidelines for Nobel Biocare products including MRI information', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/79840_Cleaning_and_sterilization_guidelines_GB_16.1.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Multi-unit Abutment Plus - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/80561B_MUA_plus_product_overview_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Conversion of existing denture into a fixed provisional bridge - direct method - Procedures manual', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/80567_Denture_conversion_manual_16.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Nobel Active - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/80605_NobelActive_Product_Overview_16.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelReplace - Conical Connection - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/80606_NobelReplace_CC_product_overview_16.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelParallel Conical Connection - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/80612_NobelParallel_CC_Product_Overview_16.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Implants with conical connection - Product overview', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/81107_Conical+Connection_implants_product_overview_16.2_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Peri-implantitis : facts and fiction - the 2016 consensus meeting findings - Science First', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/GMT++47809_NB_Peri-Implantitis_Infographic_16.1_GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelProcera FCZ Implant Crown - Handling guidelines for dental laboratories', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/GMT+40522+NobelProcera+FCZ+Implant+Crown+handling+guide+for+labs+GB.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('NobelProcera fixed-removable implant solutions', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/GMT33703_NobelProcera+Implant+Bar+Overdenture+-+Fixed+-+removable_GBFind+here+all+support+material+to+place+orders+with+the+NobelProcera+Services+Center.pdf', CURRENT_TIMESTAMP);
INSERT INTO "documents"("title", "url", "dateCreated") VALUES ('Full-Arch implant-supported same day teeth', 'https://s3.amazonaws.com/ds-static.spfr.co/doc_center/Novadontics%20Full%20Arch%20solutions.pdf', CURRENT_TIMESTAMP);


# --- !Downs
DROP TABLE "documents";