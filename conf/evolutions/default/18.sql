# --- !Ups
CREATE TABLE "shippingAddresses" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "country" VARCHAR NOT NULL,
  "city" VARCHAR NOT NULL,
  "state" VARCHAR NOT NULL,
  "zip" VARCHAR NOT NULL,
  "address" VARCHAR NOT NULL,
  "phone" VARCHAR NOT NULL,
  "dentist" BIGSERIAL,
  "primary" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("dentist") REFERENCES "staff" ("id")
);

ALTER TABLE "orders"
  ADD COLUMN "name" VARCHAR,
  ADD COLUMN "country" VARCHAR,
  ADD COLUMN "city" VARCHAR,
  ADD COLUMN "state" VARCHAR,
  ADD COLUMN "zip" VARCHAR,
  ADD COLUMN "address" VARCHAR,
  ADD COLUMN "phone" VARCHAR;

# --- !Downs
DROP TABLE "shippingAddresses";
ALTER TABLE "orders"
  DROP COLUMN "name",
  DROP COLUMN "country",
  DROP COLUMN "city",
  DROP COLUMN "state",
  DROP COLUMN "zip",
  DROP COLUMN "address",
  DROP COLUMN "phone";