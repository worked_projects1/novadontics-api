# --- !Ups

ALTER TABLE "practices"
ADD COLUMN  "shortId" INT,
ADD COLUMN "timeZone" VARCHAR,
ADD COLUMN  "phoneExt" VARCHAR,
ADD COLUMN  "phone1" VARCHAR,
ADD COLUMN  "phoneExt1" VARCHAR,
ADD COLUMN  "fax" VARCHAR,
ADD COLUMN  "email" VARCHAR,
ADD COLUMN  "taxId" INT,
ADD COLUMN  "insBillingProvider" VARCHAR,
ADD COLUMN  "billingLicense" VARCHAR,
ADD COLUMN  "openingDate" DATE,
ADD COLUMN  "officeGroup" VARCHAR,
ADD COLUMN  "ucrFeeSchedule" VARCHAR,
ADD COLUMN  "feeSchedule" VARCHAR,
ADD COLUMN  "officeId" VARCHAR;

ALTER TABLE "vendors" ADD COLUMN "image" VARCHAR;

CREATE TABLE "saveLater"(
"id" BIGSERIAL NOT NULL,
"staffId" INT,
"productId" INT,
PRIMARY KEY ("id"));

ALTER TABLE "staff" ADD COLUMN "agreementAccepted" BOOLEAN DEFAULT FALSE;

ALTER TABLE "vendors" ADD COLUMN "groups" JSONB DEFAULT '[]';

CREATE TABLE "vendorInvoice"(
"id" BIGSERIAL NOT NULL,
"vendorId" INT,
"orderId" INT,
"vendorName" VARCHAR,
"invoiceNum" VARCHAR,
"paymentType" VARCHAR,
"amountPaid" INT,
"paymentDate" DATE,
"checkNumber" VARCHAR,
"invoice" VARCHAR,
PRIMARY KEY ("id"));

DELETE from "listOptions" where "listValue" IN('clinicalNotes','treatmentPlan');

UPDATE "listOptions" set "listOption" = 'Tooth chart / Tx plan / Notes' where "listValue" = 'toothChart'

ALTER TABLE "orders" ADD COLUMN "paymentMethod" VARCHAR;

ALTER TABLE "consultations"
ADD COLUMN  "customerProfileID" BIGINT,
ADD COLUMN "customerPaymentProfileId" BIGINT,
ADD COLUMN  "transId" BIGINT,
ADD COLUMN  "transactionCode" VARCHAR;

ALTER TABLE "staff" ADD COLUMN "contract" VARCHAR;

ALTER TABLE practices ALTER COLUMN "shortId" TYPE VARCHAR;

UPDATE forms set schema = '[{"rows":[{"columns":[{"columnItems":[{"group":{"title":"Decisions for the Maxilla","fields":[{"name":"c4b-16","type":"Checkbox","title":"Is Immediate Loading Recommended? (Based on the HU value only)","attributes":{"note":false,"options":[{"title":"Recommended (HU average is 250 or more)","value":"Recommended"},{"title":"Not Recommended (HU average is below 250)","value":"Not Recommended"}]}},{"name":"c4b-17c","type":"Checkbox","title":"Natural Bone Loss","attributes":{"note":true,"options":[{"title":"None (Consider C & B)","value":"None (Consider C & B)"},{"title":"Minimal (1-2 mm. Consider C & B)","value":"Minimal (1-2 mm. Consider C & B)"},{"title":"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)","value":"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)"},{"title":"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)","value":"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-18b","type":"Checkbox","title":"Can Alveoloplasty Be Performed?","attributes":{"note":true,"options":[{"title":"No (Only C & B option is available to the patient)","value":"No (Only C & B option is available to the patient)"},{"title":"Yes up to 2 mm (Consider C & B or Locator overdenture)","value":"Yes up to 2 mm (Consider C & B or Locator overdenture)"},{"title":"Yes up to 5 mm or more (Consider Hybrid or Locator options)","value":"Yes up to 5 mm or more (Consider Hybrid or Locator options)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-19","type":"Checkbox","title":"Is Posterior Placement Currently Possible?","attributes":{"note":true,"options":[{"title":"No (Consider all on 4 or locator overdenture)","value":"No (Consider all on 4 or locator overdenture)"},{"title":"Yes (all on 5. Adequate for certain mandibles)","value":"Yes (all on 5. Adequate for certain mandibles)"},{"title":"Yes (all on 6. Recommended for the Maxilla or the mandible)","value":"Yes (all on 6. Recommended for the Maxilla or the mandible)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-20a","type":"Checkbox","title":"Do You Recommend Additional Implant Placement After Bone Grafting?","attributes":{"note":true,"options":[{"title":"No","value":"No"},{"title":"Yes (After Sinus elevation, block grafting or PSP for the maxilla)","value":"Yes (After Sinus elevation, block grafting or PSP for the maxilla)"},{"title":"Yes (After block grafting or PSP for the mandible)","value":"Yes (After block grafting or PSP for the mandible)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-20","type":"Checkbox","title":"Best Implant configuration","attributes":{"note":true,"options":[{"title":"2 for Locators","value":"2 for Locators"},{"title":"4 for Locators","value":"4 for Locators"},{"title":"All on 4","value":"All on 4"},{"title":"All on 5","value":"All on 5"},{"title":"All on 6","value":"All on 6"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-21","type":"Checkbox","title":"Type of Temporary Prosthesis Recommended","attributes":{"note":true,"options":[{"title":"Converted dentures","value":"Converted dentures"},{"title":"CAD/CAM PMMA","value":"C&B (FP3/With pink)"},{"title":"CAD/CAM Composite","value":"CAD/CAM Composite"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-22","type":"Checkbox","title":"Type of Definitive Prosthesis Recommended","attributes":{"note":true,"options":[{"title":"C & B (No pink)","value":"C & B (No pink))"},{"title":"C & B (With pink)","value":"C & B (With pink)"},{"title":"Hybrid Acrylic","value":"Hybrid Acrylic"},{"title":"Hybrid Zirconia","value":"Hybrid zirconia"},{"title":"Marius Bridge","value":"Marius Bridge"},{"title":"Locator Overdenture","value":"Locator overdenture"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-23","type":"Checkbox","title":"Is Guided Surgery Recommended?","attributes":{"note":true,"options":[{"title":"No","value":"No"},{"title":"Yes (With bone reduction guide)","value":"Yes (With bone reduction guide)"},{"title":"Yes (Without bone reduction guide)","value":"Yes (Without bone reduction guide)"},{"title":"Other, see notes","value":"other"}]}}]}}]},{"columnItems":[{"group":{"title":"","fields":[{"name":"c4b-16b","type":"Checkbox","title":"Incisor Display When Lips Are in Repose","attributes":{"note":true,"options":[{"title":"-3","value":"-3"},{"title":"-2","value":"-2"},{"title":"-1","value":"-1"},{"title":"0","value":"0"},{"title":"+1","value":"+1"},{"title":"+2","value":"+2"},{"title":"+3","value":"+3"},{"title":"+4","value":"+4"},{"title":"+5","value":"+5"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17","type":"Checkbox","title":"Gum in Exaggerated Smile","attributes":{"note":true,"options":[{"title":"No Gummy Smile","value":"No Gummy Smile"},{"title":"1mm of gummy smile","value":"1mm of gummy smile"},{"title":"2mm of gummy smile","value":"2mm of gummy smile"},{"title":"3mm of gummy smile","value":"3mm of gummy smile"},{"title":"4mm of gummy smile","value":"4mm of gummy smile"},{"title":"5mm of gummy smile","value":"5mm of gummy smile"},{"title":"6mm of gummy smile","value":"6mm of gummy smile"},{"title":"7mm of gummy smile","value":"7mm of gummy smile"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17a","type":"Checkbox","title":"Lip Support","attributes":{"note":true,"options":[{"title":"Not needed","value":"No needed"},{"title":"Needed","value":"Needed"},{"title":"Not sure (must perform flangless try-in)","value":"Not sure (must perform flangless try-in)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17b","type":"Checkbox","title":"Attempt to Change the Skeletal Classification","attributes":{"note":true,"options":[{"title":"Needed","value":"Needed"},{"title":"Not needed","value":"No needed"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-18","type":"Checkbox","title":"Condition of Natural Soft Tissue Papillae","attributes":{"note":true,"options":[{"title":"Exists (Better outcome with C & B if selected)","value":"Exist (Better outcome with C & B if selected)"},{"title":"Does not exist (Higher risk with C & B. Consider Hybrid)","value":"Does not exist (Higher risk with C & B. Consider Hybrid)"},{"title":"Other, see notes","value":"other"}]}}]}}]}]},{"columns":[{"columnItems":[{"group":{"title":"","fields":[]}}]}]},{"columns":[{"columnItems":[{"group":{"title":"Bone Volume","fields":[{"name":"2b4","type":"Input","title":"Incisal Edge of Anterior Teeth to Nasal Floor","postfix":"mm"},{"name":"2b5","type":"Input","title":"Crestal Ridge to Nasal Floor","postfix":"mm"},{"name":"2b6","type":"Select","title":"Amount of Natural Bone Loss","options":[{"title":"1 mm","value":"1 mm"},{"title":"2 mm","value":"2 mm"},{"title":"3 mm","value":"3 mm"},{"title":"4 mm","value":"4 mm"},{"title":"5 mm","value":"5 mm"},{"title":"6 mm","value":"6 mm"},{"title":"7 mm","value":"7 mm"},{"title":"8 mm","value":"8 mm"},{"title":"9 mm","value":"9 mm"},{"title":"10 mm","value":"10 mm"},{"title":"10+ mm","value":"10+ mm"}]},{"name":"2b7","type":"Input","title":"Anterior - Available Bony Height Above Roots","postfix":"mm"},{"name":"2b8","type":"Input","title":"Posterior Right - Available Bony Height Above Roots","postfix":"mm"},{"name":"2b9","type":"Input","title":"Posterior Left- Available Bony Height Above Roots","postfix":"mm"},{"name":"2b10","type":"Select","title":"Alveoplasty beyond 12 mm (12 mm is the minimum needed for 10-mm implants)","options":[{"title":"Possible","value":"Possible"},{"title":"Not Possible","value":"Not Possible"}]},{"name":"2b11","type":"Input","title":"Recommended Implant Location (in locations of teeth #)"}]}}]},{"columnItems":[{"group":{"title":"Average Bone Density","fields":[{"name":"2b1","type":"Input","title":"Anterior","postfix":"HU VALUE"},{"name":"2b2","type":"Input","title":"Posterior Right","postfix":"HU VALUE"},{"name":"2b3","type":"Input","title":"Posterior Left","postfix":"HU VALUE"}]}}]}]},{"columns":[{"columnItems":[{"field":{"text":"<br/><br/><br/><br/>","type":"InfoBlock"}}]}]}],"title":"Full Arch Implant Exam - Maxilla"},{"rows":[{"columns":[{"columnItems":[{"group":{"title":"Decisions for the Mandible","fields":[{"name":"c4b-16_2","type":"Checkbox","title":"Is Immediate Loading Recommended? (Based on the HU value only)","attributes":{"note":false,"options":[{"title":"Recommended (HU average is 250 or more)","value":"Recommended"},{"title":"Not Recommended (HU average is below 250)","value":"Not Recommended"}]}},{"name":"c4b-17c_2","type":"Checkbox","title":"Natural Bone Loss","attributes":{"note":true,"options":[{"title":"None (Consider C & B)","value":"None (Consider C & B)"},{"title":"Minimal (1-2 mm. Consider C & B)","value":"Minimal (1-2 mm. Consider C & B)"},{"title":"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)","value":"Moderate (3-7 mm. Consider Hybrid, Marius or Locator options)"},{"title":"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)","value":"Severe (Over 7 mm. Consider Hybrid, Marius or Locator options)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-18b_2","type":"Checkbox","title":"Can Alveoloplasty Be Performed?","attributes":{"note":true,"options":[{"title":"No (Only C & B option is available to the patient)","value":"No (Only C & B option is available to the patient)"},{"title":"Yes up to 2 mm (Consider C & B or Locator overdenture)","value":"Yes up to 2 mm (Consider C & B or Locator overdenture)"},{"title":"Yes up to 5 mm or more (Consider Hybrid or Locator options)","value":"Yes up to 5 mm or more (Consider Hybrid or Locator options)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-19_2","type":"Checkbox","title":"Is Posterior Placement Currently Possible?","attributes":{"note":true,"options":[{"title":"No (Consider all on 4 or locator overdenture)","value":"No (Consider all on 4 or locator overdenture)"},{"title":"Yes (all on 5. Adequate for certain mandibles)","value":"Yes (all on 5. Adequate for certain mandibles)"},{"title":"Yes (all on 6. Recommended for the Maxilla or the mandible)","value":"Yes (all on 6. Recommended for the Maxilla or the mandible)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-20a_2","type":"Checkbox","title":"Do You Recommend Additional Implant Placement After Bone Grafting?","attributes":{"note":true,"options":[{"title":"No","value":"No"},{"title":"Yes (After Sinus elevation, block grafting or PSP for the maxilla)","value":"Yes (After Sinus elevation, block grafting or PSP for the maxilla)"},{"title":"Yes (After block grafting or PSP for the mandible)","value":"Yes (After block grafting or PSP for the mandible)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-20_2","type":"Checkbox","title":"Best Implant configuration","attributes":{"note":true,"options":[{"title":"2 for Locators","value":"2 for Locators"},{"title":"4 for Locators","value":"4 for Locators"},{"title":"All on 4","value":"All on 4"},{"title":"All on 5","value":"All on 5"},{"title":"All on 6","value":"All on 6"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-21_2","type":"Checkbox","title":"Type of Temporary Prosthesis Recommended","attributes":{"note":true,"options":[{"title":"Converted dentures","value":"Converted dentures"},{"title":"CAD/CAM PMMA","value":"C&B (FP3/With pink)"},{"title":"CAD/CAM Composite","value":"CAD/CAM Composite"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-22_2","type":"Checkbox","title":"Type of Definitive Prostheses Recommended","attributes":{"note":true,"options":[{"title":"C & B (No pink)","value":"C & B (No pink))"},{"title":"C & B (With pink)","value":"C & B (With pink)"},{"title":"Hybrid Acrylic","value":"Hybrid Acrylic"},{"title":"Hybrid Zirconia","value":"Hybrid zirconia"},{"title":"Marius Bridge","value":"Marius Bridge"},{"title":"Locator Overdenture","value":"Locator overdenture"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-23_2","type":"Checkbox","title":"Is Guided Surgery Recommended?","attributes":{"note":true,"options":[{"title":"No","value":"No"},{"title":"Yes (With bone reduction guide)","value":"Yes (With bone reduction guide)"},{"title":"Yes (Without bone reduction guide)","value":"Yes (Without bone reduction guide)"},{"title":"Other, see notes","value":"other"}]}}]}}]},{"columnItems":[{"group":{"title":"","fields":[{"name":"c4b-16b_2","type":"Checkbox","title":"Incisor Display When Lips Are in Repose","attributes":{"note":true,"options":[{"title":"-3","value":"-3"},{"title":"-2","value":"-2"},{"title":"-1","value":"-1"},{"title":"0","value":"0"},{"title":"+1","value":"+1"},{"title":"+2","value":"+2"},{"title":"+3","value":"+3"},{"title":"+4","value":"+4"},{"title":"+5","value":"+5"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17_2","type":"Checkbox","title":"Gum in Exaggerated Smile","attributes":{"note":true,"options":[{"title":"No Gummy Smile","value":"No Gummy Smile"},{"title":"1mm of gummy smile","value":"1mm of gummy smile"},{"title":"2mm of gummy smile","value":"2mm of gummy smile"},{"title":"3mm of gummy smile","value":"3mm of gummy smile"},{"title":"4mm of gummy smile","value":"4mm of gummy smile"},{"title":"5mm of gummy smile","value":"5mm of gummy smile"},{"title":"6mm of gummy smile","value":"6mm of gummy smile"},{"title":"7mm of gummy smile","value":"7mm of gummy smile"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17a_2","type":"Checkbox","title":"Lip Support","attributes":{"note":true,"options":[{"title":"Not needed","value":"No needed"},{"title":"Needed","value":"Needed"},{"title":"Not sure (must perform flangless try-in)","value":"Not sure (must perform flangless try-in)"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-17b_2","type":"Checkbox","title":"Attempt to Change the Skeletal Classification","attributes":{"note":true,"options":[{"title":"Needed","value":"Needed"},{"title":"Not needed","value":"No needed"},{"title":"Other, see notes","value":"other"}]}},{"name":"c4b-18_2","type":"Checkbox","title":"Condition of Natural Soft Tissue Papillae","attributes":{"note":true,"options":[{"title":"Exists (Better outcome with C & B if selected)","value":"Exist (Better outcome with C & B if selected)"},{"title":"Does not exist (Higher risk with C & B Consider Hybrid)","value":"Does not exist (Higher risk with C & B. Consider Hybrid)"},{"title":"Other, see notes","value":"other"}]}}]}}]}]},{"columns":[{"columnItems":[{"group":{"title":"","fields":[]}}]}]},{"columns":[{"columnItems":[{"group":{"title":"Bone Volume","fields":[{"name":"2b4b","type":"Input","title":"Incisal Edge of Anterior Teeth to Inferior Border of the Mandible","postfix":"mm"},{"name":"2b5b","type":"Input","title":"Crestal Ridge to Inferior Border of the Mandible","postfix":"mm"},{"name":"2b6b","type":"Select","title":"Amount of Natural Bone Loss","options":[{"title":"1 mm","value":"1 mm"},{"title":"2 mm","value":"2 mm"},{"title":"3 mm","value":"3 mm"},{"title":"4 mm","value":"4 mm"},{"title":"5 mm","value":"5 mm"},{"title":"6 mm","value":"6 mm"},{"title":"7 mm","value":"7 mm"},{"title":"8 mm","value":"8 mm"},{"title":"9 mm","value":"9 mm"},{"title":"10 mm","value":"10 mm"},{"title":"10+ mm","value":"10+ mm"}]},{"name":"2b7b","type":"Input","title":"Anterior - Available Bony Height Under Roots","postfix":"mm"},{"name":"2b8b","type":"Input","title":"Posterior Right - Available Bony Height Under Roots","postfix":"mm"},{"name":"2b9b","type":"Input","title":"Posterior Left- Available Bony Height Under Roots","postfix":"mm"},{"name":"2b10b","type":"Select","title":"Alveoplasty beyond 12 mm (12 mm is the minimum needed for 10-mm implants)","options":[{"title":"Possible","value":"Possible"},{"title":"Not Possible","value":"Not Possible"}]},{"name":"2b11b","type":"Input","title":"Recommended Implant Location (in locations of teeth #)"}]}}]},{"columnItems":[{"group":{"title":"Average Bone Density","fields":[{"name":"2b1b","type":"Input","title":"Anterior","postfix":"HU VALUE"},{"name":"2b2b","type":"Input","title":"Posterior Right","postfix":"HU VALUE"},{"name":"2b3b","type":"Input","title":"Posterior Left","postfix":"HU VALUE"}]}}]}]},{"columns":[{"columnItems":[{"field":{"text":"<br/><br/><br/><br/>","type":"InfoBlock"}}]}]}],"title":"Full Arch Implant Exam - Mandible"},{"rows":[{"columns":[{"columnItems":[{"field":{"name":"clinicalexam","type":"Checkbox","title":"Clinical Exam is done","attributes":{"info":{"image":"https://s3.amazonaws.com/static.novadonticsllc.com/PDF/Novadontics%20Full%20Arch%20solutions.pdf"},"note":false,"images":false}}},{"field":{"name":"c4_3_2","type":"Input","title":"Recommended temporary prosthesis","required":false}},{"field":{"name":"c4_3_3","type":"Input","title":"Recommended permanent prosthesis","required":false}},{"field":{"name":"c4_3_4","type":"Input","title":"Recommended implant configuration","required":false}},{"field":{"name":"c4_3_5","type":"Input","title":"Loading protocol (immediate Vs. delayed)","required":false}}]}]},{"columns":[{"columnItems":[{"field":{"text":"<br/><br/><br/><br/>","type":"InfoBlock"}}]}]}],"title":"Implant Exam Recommendations"}]' where id = '1D';

update forms set types='checkList',"listOrder"=21,"checkList"=true where id='1F';

INSERT INTO "listOptions"("listType","listOption","listValue") values('implantBrand','NOBEL BIOCARE NobelParallel Conical Connection','NOBEL BIOCARE NobelParallel Conical Connection');


# --- !Downs

ALTER TABLE "practices"
DROP COLUMN  "shortId",
DROP COLUMN "timeZone",
DROP COLUMN  "phoneExt",
DROP COLUMN  "phone1",
DROP COLUMN  "phoneExt1",
DROP COLUMN  "fax",
DROP COLUMN  "email",
DROP COLUMN  "taxId",
DROP COLUMN  "insBillingProvider",
DROP COLUMN  "billingLicense",
DROP COLUMN  "openingDate",
DROP COLUMN  "officeGroup",
DROP COLUMN  "ucrFeeSchedule",
DROP COLUMN  "feeSchedule",
DROP COLUMN  "officeId";

ALTER TABLE "vendors" DROP COLUMN "image";

DROP TABLE "saveLater";

ALTER TABLE "staff" DROP COLUMN "agreementAccepted";

ALTER TABLE "vendors" DROP COLUMN "groups";

DROP TABLE "vendorInvoice";

ALTER TABLE "orders" DROP COLUMN "paymentMethod";

ALTER TABLE "consultations"
DROP COLUMN  "customerProfileID",
DROP COLUMN "customerPaymentProfileId",
DROP COLUMN  "transId",
DROP COLUMN  "transactionCode";

ALTER TABLE "staff" DROP COLUMN "contract";
