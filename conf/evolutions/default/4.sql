# --- !Ups
ALTER TABLE "documents"
  ADD COLUMN "category" VARCHAR NOT NULL DEFAULT 'Other';

# --- !Downs
ALTER TABLE "documents"
  DROP COLUMN "category";
