# --- !Ups
CREATE TABLE "prescriptions" (
  "id" BIGSERIAL NOT NULL,
  "medication" VARCHAR NOT NULL,
  "dosage" VARCHAR NOT NULL,
  "count" INT,
  "instructions" VARCHAR,
  "refills" INT NOT NULL DEFAULT 0,
  "genericSubstituteOk" BOOLEAN,
  "deleted" BOOLEAN NOT NULL DEFAULT FALSE
);

# --- !Downs
DROP TABLE "prescriptions";