# --- !Ups
ALTER TABLE "consultations" ALTER COLUMN "treatmentId" DROP NOT NULL;

# --- !Downs
ALTER TABLE "consultations" ALTER COLUMN "treatmentId" SET NOT NULL;