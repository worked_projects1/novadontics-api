# --- !Ups

INSERT INTO "listOptions"("listType","listOption","listValue") values
('implantBrand','A.B. DENTAL DEVICES LTD. Conical Implant','A.B. DENTAL DEVICES LTD. Conical Implant'),
('implantBrand','A.B. DENTAL DEVICES LTD. Internal Hex Implant','A.B. DENTAL DEVICES LTD. Internal Hex Implant'),
('implantBrand','A.B. DENTAL DEVICES LTD. Screw Type Implant','A.B. DENTAL DEVICES LTD. Screw Type Implant'),
('implantBrand','A.B. DENTAL DEVICES LTD. Trapeze Implant','A.B. DENTAL DEVICES LTD. Trapeze Implant'),
('implantBrand','ADDENTA Bifasico','ADDENTA Bifasico'),
('implantBrand','ADVAN S.R.L. GTB','ADVAN S.R.L. GTB'),
('implantBrand','ADVAN S.R.L. ONE','ADVAN S.R.L. ONE'),
('implantBrand','ADVAN S.R.L. Tzero','ADVAN S.R.L. Tzero'),
('implantBrand','ALPHATECH','ALPHATECH'),
('implantBrand','AON IMPLANTS CYROTH OsteoPore CC','AON IMPLANTS CYROTH OsteoPore CC'),
('implantBrand','AON IMPLANTS IS-FOUR OsteoPore CC','AON IMPLANTS IS-FOUR OsteoPore CC'),
('implantBrand','BIODENTA Bone Level Tapered','BIODENTA Bone Level Tapered'),
('implantBrand','BIOHORIZONS IMPLANTS Tapered IM','BIOHORIZONS IMPLANTS Tapered IM'),
('implantBrand','BIOHORIZONS IMPLANTS Tapered Plus','BIOHORIZONS IMPLANTS Tapered Plus'),
('implantBrand','BIOHORIZONS IMPLANTS Tapered Pro','BIOHORIZONS IMPLANTS Tapered Pro'),
('implantBrand','BIOHORIZONS IMPLANTS Tapered Shor','BIOHORIZONS IMPLANTS Tapered Shor'),
('implantBrand','BLUE SKY BIO BIO | Conus 12','BLUE SKY BIO BIO | Conus 12'),
('implantBrand','BLUE SKY BIO BIO | Internal Hex','BLUE SKY BIO BIO | Internal Hex'),
('implantBrand','BLUE SKY BIO BIO | Max NP','BLUE SKY BIO BIO | Max NP'),
('implantBrand','BLUE SKY BIO BIO | One Stage','BLUE SKY BIO BIO | Quattro'),
('implantBrand','BLUE SKY BIO BIO | Trilobe','BLUE SKY BIO BIO | Trilobe'),
('implantBrand','BREDENT MEDICAL copaSKY','BREDENT MEDICAL copaSKY'),
('implantBrand','BREDENT MEDICAL mini2SKY','BREDENT MEDICAL mini2SKY'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Plus','BTI BIOTECHNOLOGY INSTITUTE Externa Plus'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Tiny','BTI BIOTECHNOLOGY INSTITUTE Externa Tiny'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Universal','BTI BIOTECHNOLOGY INSTITUTE Externa Universal'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna 3.0','BTI BIOTECHNOLOGY INSTITUTE Interna 3.0'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Core','BTI BIOTECHNOLOGY INSTITUTE Interna Core'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Plus','BTI BIOTECHNOLOGY INSTITUTE Interna Plus'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Universal','BTI BIOTECHNOLOGY INSTITUTE Interna Universal'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Wide','BTI BIOTECHNOLOGY INSTITUTE Interna Wide'),
('implantBrand','CAMLOG CAMLOG PROGRESSIVE-LINE Promote plus snap-in','CAMLOG CAMLOG PROGRESSIVE-LINE Promote plus snap-in'),
('implantBrand','CAMLOG CAMLOG PROGRESSIVE-LINE Promote plus snap-in','CAMLOG CAMLOG PROGRESSIVE-LINE Promote plus snap-in'),
('implantBrand','CAMLOG CAMLOG SCREW-LINE Promote plus former-packaging','CAMLOG CAMLOG SCREW-LINE Promote plus former-packaging'),
('implantBrand','CAMLOG CAMLOG SCREW-LINE Promote plus snap-in','CAMLOG CAMLOG SCREW-LINE Promote plus snap-in'),
('implantBrand','CAMLOG CAMLOG SCREW-LINE Promote snap-in','CAMLOG CAMLOG SCREW-LINE Promote snap-in'),
('implantBrand','CAMLOG CERALOG® Hexalobe implant M10','CAMLOG CERALOG® Hexalobe implant M10'),
('implantBrand','CAMLOG CERALOG® Hexalobe implant M12','CAMLOG CERALOG® Hexalobe implant M12'),
('implantBrand','CAMLOG CERALOG® Hexalobe implant M8','CAMLOG CERALOG® Hexalobe implant M8'),
('implantBrand','CAMLOG CONELOG PROGRESSIVE-LINE Promote plus snap-in','CAMLOG CONELOG PROGRESSIVE-LINE Promote plus snap-in'),
('implantBrand','CAMLOG CONELOG PROGRESSIVE-LINE Promote plus guide/srew-mounted','CAMLOG CONELOG PROGRESSIVE-LINE Promote plus guide/srew-mounted'),
('implantBrand','CAMLOG CONELOG SCREW-LINE Promote plus former-packaging','CAMLOG CONELOG SCREW-LINE Promote plus former-packaging'),
('implantBrand','CAMLOG CONELOG SCREW-LINE Promote plus snap-in','CAMLOG CONELOG SCREW-LINE Promote plus snap-in'),
('implantBrand','CAMLOG Guide System CAMLOG SCREW-LINE Promote plus','CAMLOG Guide System CAMLOG SCREW-LINE Promote plus'),
('implantBrand','CAMLOG Guide System CONELOG SCREW-LINE Promote plus','CAMLOG Guide System CONELOG SCREW-LINE Promote plus'),
('implantBrand','CHAMPIONS IMPLANTS GMBH Champions® (R)Evolution','CHAMPIONS IMPLANTS GMBH Champions® (R)Evolution'),
('implantBrand','COMPRESS','COMPRESS'),
('implantBrand','COWELLMEDI INNO External Implant S.L.A(Pre-Mount)','COWELLMEDI INNO External Implant S.L.A(Pre-Mount)'),
('implantBrand','COWELLMEDI INNO Internal Implant S.L.A Cuff 1.8mm(No-Mount)','COWELLMEDI INNO Internal Implant S.L.A Cuff 1.8mm(No-Mount)'),
('implantBrand','COWELLMEDI INNO Submerged Narrow Implant S.L.A(No-Mount)','COWELLMEDI INNO Submerged Narrow Implant S.L.A(No-Mount)'),
('implantBrand','COWELLMEDI INNO Submerged Regular Implant S.L.A(No-Mount)','COWELLMEDI INNO Submerged Regular Implant S.L.A(No-Mount)'),
('implantBrand','COWELLMEDI INNO Submerged Short Implant (Pre-Mount)','COWELLMEDI INNO Submerged Short Implant (Pre-Mount)'),
('implantBrand','DENTAL TECH Implassic FTP','DENTAL TECH Implassic FTP'),
('implantBrand','DENTEGRIS SLS-Straight','DENTEGRIS SLS-Straight'),
('implantBrand','DENTEGRIS SL-Tapered','DENTEGRIS SL-Tapered'),
('implantBrand','DENTEGRIS Soft-Bone','DENTEGRIS Soft-Bone'),
('implantBrand','DENTIN IMPLANTS TECHNOLOGIES CLASSIC','DENTIN IMPLANTS TECHNOLOGIES CLASSIC'),
('implantBrand','DENTIN IMPLANTS TECHNOLOGIES PRESTIGE','DENTIN IMPLANTS TECHNOLOGIES PRESTIGE'),
('implantBrand','DENTIN IMPLANTS TECHNOLOGIES RAPID','DENTIN IMPLANTS TECHNOLOGIES RAPID'),
('implantBrand','DENTIUM SimpleLine II','DENTIUM SimpleLine II'),
('implantBrand','DR. IHDE DENTAL AG Hexacone','DR. IHDE DENTAL AG Hexacone'),
('implantBrand','DR. IHDE DENTAL AG Hexacone 6+2','DR. IHDE DENTAL AG Hexacone 6+2'),
('implantBrand','DR. IHDE DENTAL AG Hexacone 6+2 with wide thread part','DR. IHDE DENTAL AG Hexacone 6+2 with wide thread part'),
('implantBrand','DR. IHDE DENTAL AG Hexacone with wide thread part','DR. IHDE DENTAL AG Hexacone with wide thread part'),
('implantBrand','DTI','DTI'),
('implantBrand','GEASS Way Extra','GEASS Way Extra'),
('implantBrand','GEASS Way miX','GEASS Way miX'),
('implantBrand','GEASS Way Rock','GEASS Way Rock'),
('implantBrand','GLOBAL D Inkone','GLOBAL D Inkone'),
('implantBrand','GLOBAL D Krestal','GLOBAL D Krestal'),
('implantBrand','GLOBAL D Progress','GLOBAL D Progress'),
('implantBrand','GLOBAL D T-quest','GLOBAL D T-quest'),
('implantBrand','GLOBAL D Twinkone','GLOBAL D Twinkone'),
('implantBrand','HIOSSEN HGII','HIOSSEN HGII'),
('implantBrand','IMPLACIL DE BORTOLI IMPLANTE UN III CONE MORSE AR','IMPLACIL DE BORTOLI IMPLANTE UN III CONE MORSE AR'),
('implantBrand','IMPLACIL DE BORTOLI Universal III HI','IMPLACIL DE BORTOLI Universal III HI'),
('implantBrand','IMPLANT DIRECT InterActive Implant','IMPLANT DIRECT InterActive Implant'),
('implantBrand','IMPLANT DIRECT Legacy2 Implant HA','IMPLANT DIRECT Legacy2 Implant HA'),
('implantBrand','IMPLANT DIRECT RePlant Implant','IMPLANT DIRECT RePlant Implant'),
('implantBrand','IMPLANT DIRECT simplyInterActive Implant','IMPLANT DIRECT simplyInterActive Implant'),
('implantBrand','IMPLANT DIRECT simplyLegacy2 Implant','IMPLANT DIRECT simplyLegacy2 Implant'),
('implantBrand','IMPLANT DIRECT simplyLegacy3 Implant','IMPLANT DIRECT simplyLegacy3 Implant'),
('implantBrand','IMPLANT DIRECT simplyRePlant Implant','IMPLANT DIRECT simplyRePlant Implant'),
('implantBrand','IMPLANT DIRECT SwishPlus Implant','IMPLANT DIRECT SwishPlus Implant'),
('implantBrand','IMPLANT DIRECT simplyRePlant Implant','IMPLANT DIRECT simplyRePlant Implant'),
('implantBrand','IMPLANT DIRECT SwishPlus Implant','IMPLANT DIRECT SwishPlus Implant'),
('implantBrand','IMPLANTS DIFFUSION INTERNATIONAL (IDI) IDBIO Hexagonal Connection Implant','IMPLANTS DIFFUSION INTERNATIONAL (IDI) IDBIO Hexagonal Connection Implant'),
('implantBrand','IMPLANTS DIFFUSION INTERNATIONAL (IDI) IDCAM Implants Morse Taper','IMPLANTS DIFFUSION INTERNATIONAL (IDI) IDCAM Implants Morse Taper'),
('implantBrand','IMPLANTSWISS Bone Level','IMPLANTSWISS Bone Level'),
('implantBrand','IMPLANTSWISS Mini Type','IMPLANTSWISS Mini Type'),
('implantBrand','IMPLANTSWISS Tissue Level','IMPLANTSWISS Tissue Level'),
('implantBrand','KEYSTONE DENTAL I-HEXMRT','KEYSTONE DENTAL I-HEXMRT'),
('implantBrand','KEYSTONE DENTAL TiLobeMAXX® Implantt','KEYSTONE DENTAL TiLobeMAXX® Implantt'),
('implantBrand','MEDENTIKA MICROCONE','MEDENTIKA MICROCONE'),
('implantBrand','MEDENTIKA PROCONE','MEDENTIKA PROCONE'),
('implantBrand','MEDENTIKA QUATTROCONE','MEDENTIKA QUATTROCONE'),
('implantBrand','MEDENTIS ICX Active Master','MEDENTIS ICX Active Master'),
('implantBrand','MEDENTIS ICX Active Master 3.3','MEDENTIS ICX Active Master 3.3'),
('implantBrand','MEDENTIS ICX Active Master Narrow 3.3 Tissue Line','MEDENTIS ICX Active Master Narrow 3.3 Tissue Line'),
('implantBrand','MEDENTIS ICX Active Master Tissue Line','MEDENTIS ICX Active Master Tissue Line'),
('implantBrand','MEDENTIS ICX Narrow 3.3 Tissue Line','MEDENTIS ICX Narrow 3.3 Tissue Line'),
('implantBrand','MEDENTIS ICX Premium','MEDENTIS ICX Premium'),
('implantBrand','MEDENTIS ICX Premium 3.3','MEDENTIS ICX Premium 3.3'),
('implantBrand','MEDENTIS ICX Premium Single','MEDENTIS ICX Premium Single'),
('implantBrand','MEDENTIS ICX Premium Tissue Line','MEDENTIS ICX Premium Tissue Line'),
('implantBrand','MEDENTIS ICX-mini','MEDENTIS ICX-mini'),
('implantBrand','MEDENTIS ICX-mini machined','MEDENTIS ICX-mini machined'),
('implantBrand','MEDIGMA TECHNOLOGIES FixTite-S','MEDIGMA TECHNOLOGIES FixTite-S'),
('implantBrand','MGM IMPLANT Fixture Diameter Ø3.8','MGM IMPLANT Fixture Diameter Ø3.8'),
('implantBrand','MGM IMPLANT Fixture Diameter Ø4.3','MGM IMPLANT Fixture Diameter Ø4.3'),
('implantBrand','NOBEL BIOCARE NobelPearl Tapered','NOBEL BIOCARE NobelPearl Tapered'),
('implantBrand','NUCLEOSS Nucleoss T3','NUCLEOSS Nucleoss T3'),
('implantBrand','NUCLEOSS NucleOSS T6 Standard','NUCLEOSS NucleOSS T6 Standard'),
('implantBrand','NUCLEOSS NucleOSS T6 Torq','NUCLEOSS NucleOSS T6 Torq'),
('implantBrand','OSSEOLINK','OSSEOLINK'),
('implantBrand','OSSTEM KS III BA','OSSTEM KS III BA'),
('implantBrand','OXY IMPLANT Classic Int','OXY IMPLANT Classic Int'),
('implantBrand','OXY IMPLANT Fixo Mini - 0 degree','OXY IMPLANT Fixo Mini - 0 degree'),
('implantBrand','OXY IMPLANT K1','OXY IMPLANT K1'),
('implantBrand','OXY IMPLANT MD Ext','OXY IMPLANT MD Ext'),
('implantBrand','OXY IMPLANT MD Int','OXY IMPLANT MD Int'),
('implantBrand','OXY IMPLANT MD Kone','OXY IMPLANT MD Kone'),
('implantBrand','OXY IMPLANT Piesse Int - micro','OXY IMPLANT Piesse Int - micro'),
('implantBrand','OXY IMPLANT Piesse Int - normo','OXY IMPLANT Piesse Int - normo'),
('implantBrand','OXY IMPLANT PSK micro','OXY IMPLANT PSK micro'),
('implantBrand','OXY IMPLANT PSK normo','OXY IMPLANT PSK nom)o'),
('implantBrand','PALTOP Advanced Classic','PALTOP Advanced Classic'),
('implantBrand','PALTOP Advanced Plus','PALTOP Advanced Plus'),
('implantBrand','PALTOP Dynamic','PALTOP Dynamic'),
('implantBrand','PALTOP PAI','PALTOP PAI'),
('implantBrand','PALTOP PAI Total Coverage','PALTOP PAI Total Coverage'),
('implantBrand','PALTOP PCA','PALTOP PCA'),
('implantBrand','REVIOS','REVIOS'),
('implantBrand','SIC INVENT SICace','SIC INVENT SICace'),
('implantBrand','SIC INVENT SICmax','SIC INVENT SICmax'),
('implantBrand','SIC INVENT SICtapered','SIC INVENT SICtapered'),
('implantBrand','SIC INVENT SICvantage max','SIC INVENT SICvantage max'),
('implantBrand','SIC INVENT SICvantage tapered','SIC INVENT SICvantage tapered'),
('implantBrand','SOUTHERN IMPLANTS Deep Conical','SOUTHERN IMPLANTS Deep Conical'),
('implantBrand','STRAUMANN Tapered Effect Implants Titanium SLA','STRAUMANN Tapered Effect Implants Titanium SLA'),
('implantBrand','SURGIKOR Fixation Conical Connection','SURGIKOR Fixation Conical Connection'),
('implantBrand','SURGIKOR Solution Conical Connection','SURGIKOR Solution Conical Connection'),
('implantBrand','SWEDEN & MARTINA Premium One','SWEDEN & MARTINA Premium One'),
('implantBrand','SWEDEN & MARTINA Premium Straight & SP','SWEDEN & MARTINA Premium Straight & SP'),
('implantBrand','SWISS DENTAL SOLUTIONS AG SDS1.1','SWISS DENTAL SOLUTIONS AG SDS1.1'),
('implantBrand','SWISS DENTAL SOLUTIONS AG SDS2.0','SWISS DENTAL SOLUTIONS AG SDS2.0'),
('implantBrand','THOMMEN MEDICAL ELEMENT implant RC INICELL','THOMMEN MEDICAL ELEMENT implant RC INICELL'),
('implantBrand','THOMMEN MEDICAL AG CONTACT implant','THOMMEN MEDICAL AG CONTACT implant'),
('implantBrand','THOMMEN MEDICAL AG CONTACT implant MC INICELL','THOMMEN MEDICAL AG CONTACT implant MC INICELL'),
('implantBrand','THOMMEN MEDICAL AG CONTACT implant RC INICELL','THOMMEN MEDICAL AG CONTACT implant RC INICELL'),
('implantBrand','THOMMEN MEDICAL AG ELEMENT implant','THOMMEN MEDICAL AG ELEMENT implant'),
('implantBrand','THOMMEN MEDICAL AG ELEMENT implant MC INICELL','THOMMEN MEDICAL AG ELEMENT implant MC INICELL'),
('implantBrand','THOMMEN MEDICAL AG ELEMENT implant RC INICELL','THOMMEN MEDICAL AG ELEMENT implant RC INICELL'),
('implantBrand','TIDAL SPIRAL','TIDAL SPIRAL'),
('implantBrand','URIS OMNI FIxture','URIS OMNI FIxture'),
('implantBrand','URIS OMNI Tapered FIxture','URIS OMNI Tapered FIxture'),
('implantBrand','WEGO','WEGO'),
('implantBrand','WIELAND IMPLANT SYSTEM','WIELAND IMPLANT SYSTEM'),
('implantBrand','ZERAMEX XT','ZERAMEX XT'),
('implantBrand','ZIACOM Galaxy','ZIACOM Galaxy'),
('implantBrand','ZIACOM ZV2','ZIACOM ZV2'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Multi-IM Abutment','BTI BIOTECHNOLOGY INSTITUTE Externa Multi-IM Abutment'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Multi-IM Abutment Expanded','BTI BIOTECHNOLOGY INSTITUTE Externa Multi-IM Abutment Expanded'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Externa Unit Abutment','BTI BIOTECHNOLOGY INSTITUTE Externa Unit Abutment'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Multi-IM Abutment','BTI BIOTECHNOLOGY INSTITUTE Interna Multi-IM Abutment'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Multi-IM Abutment Expanded','BTI BIOTECHNOLOGY INSTITUTE Interna Multi-IM Abutment Expanded'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Unit Abutment','BTI BIOTECHNOLOGY INSTITUTE Interna Unit Abutment'),
('implantBrand','BTI BIOTECHNOLOGY INSTITUTE Interna Unit Abutment Expanded','BTI BIOTECHNOLOGY INSTITUTE Interna Unit Abutment Expanded'),
('implantBrand','CAMLOG Comfour Bar abutment','CAMLOG Comfour Bar abutment'),
('implantBrand','NOBEL BIOCARE On1 Base CC','NOBEL BIOCARE On1 Base CC');

-- Update list options

UPDATE "listOptions" set "listOption" = 'ADIN DENTAL IMPLANT SYSTEM CloseFit™', "listValue" = 'ADIN DENTAL IMPLANT SYSTEM CloseFit™' where "listOption" = 'ADIN DENTAL IMPLANT SYSTEM Touareg CloseFit';

UPDATE "listOptions" set "listOption" = 'ADIN DENTAL IMPLANT SYSTEM swell™', "listValue" = 'ADIN DENTAL IMPLANT SYSTEM swell™' where "listOption" = 'ADIN DENTAL IMPLANT SYSTEM swell';

UPDATE "listOptions" set "listOption" = 'ADIN DENTAL IMPLANT SYSTEM Touareg™-S', "listValue" = 'ADIN DENTAL IMPLANT SYSTEM Touareg™-S' where "listOption" = 'ADIN DENTAL IMPLANT SYSTEM Touareg-S';

UPDATE "listOptions" set "listOption" = 'BIOHORIZONS IMPLANTS External', "listValue" = 'BIOHORIZONS IMPLANTS External' where "listOption" = 'BIOHORIZONS External';

UPDATE "listOptions" set "listOption" = 'BIOHORIZONS IMPLANTS Tapered Internal', "listValue" = 'BIOHORIZONS IMPLANTS Tapered Internal' where "listOption" = 'BIOHORIZONS Tapered Internal';

UPDATE "listOptions" set "listOption" = 'BRAIN BASE CORPORATION MYTIS Arrow Implant', "listValue" = 'BRAIN BASE CORPORATION MYTIS Arrow Implant' where "listOption" = 'BRAINBASE Mytis Arrow Implant';

UPDATE "listOptions" set "listOption" = 'BTI BIOTECHNOLOGY INSTITUTE Tiny', "listValue" = 'BTI BIOTECHNOLOGY INSTITUTE Tiny ' where "listOption" = 'BTI BIOTECHNOLOGY Tiny';

UPDATE "listOptions" set "listOption" = 'CAMLOG iSy', "listValue" = 'CAMLOG iSy' where "listOption" = 'CAMLOG iSy (direct to implant) / CAMLOG iSy (to implant base)';

UPDATE "listOptions" set "listOption" = 'COCHLEAR Baha BIA 300', "listValue" = 'COCHLEAR Baha BIA 300' where "listOption" = 'COCHLEAR Baha BA 300,BIA 300';

UPDATE "listOptions" set "listOption" = 'DENTEGRIS S&T', "listValue" = 'DENTEGRIS S&T' where "listOption" = 'DENTEGRIS Straight &Tapered';

UPDATE "listOptions" set "listOption" = 'GC IMPLANT Aadva - Standard Implant', "listValue" = 'GC IMPLANT Aadva - Standard Implant' where "listOption" = 'GC IMPLANT Aadva';

UPDATE "listOptions" set "listOption" = 'IMPLANT DIRECT Legacy1 Implant SBM', "listValue" = 'IMPLANT DIRECT Legacy1 Implant SBM' where "listOption" = 'IMPLANT DIRECT Legacy1';

UPDATE "listOptions" set "listOption" = 'IMPLANT DIRECT Legacy2 Implant SBM', "listValue" = 'IMPLANT DIRECT Legacy2 Implant SBM' where "listOption" = 'IMPLANT DIRECT Legacy2';

UPDATE "listOptions" set "listOption" = 'IMPLANT DIRECT Legacy3 Implant SBM', "listValue" = 'IMPLANT DIRECT Legacy3 Implant SBM' where "listOption" = 'IMPLANT DIRECT Legacy3';

UPDATE "listOptions" set "listOption" = 'IMPLANT DIRECT Legacy4 Implant SBM', "listValue" = 'IMPLANT DIRECT Legacy4 Implant SBM' where "listOption" = 'IMPLANT DIRECT Legacy4';

UPDATE "listOptions" set "listOption" = 'PALTOP DIVA by Paltop', "listValue" = 'PALTOP DIVA by Paltop' where "listOption" = 'PALTOP PALTOP';

UPDATE "listOptions" set "listOption" = 'PRODENT ITALIA PRIME CONOMET TS', "listValue" = 'PRODENT ITALIA PRIME CONOMET TS' where "listOption" = 'PRODENT PRIME CONOMET TS';

UPDATE "listOptions" set "listOption" = 'TRI DENTAL IMPLANTS TRI-Octa', "listValue" = 'TRI DENTAL IMPLANTS TRI-Octa' where "listOption" = 'TRI DENTAL IMPLANTS TRI-Octa 4.8';

-- Implant Brand Update

UPDATE "implantData" set "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM CloseFit™' where "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM Touareg CloseFit';

UPDATE "implantData" set "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM swell™' where "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM swell';

UPDATE "implantData" set "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM Touareg™-S' where "implantBrand" = 'ADIN DENTAL IMPLANT SYSTEM Touareg-S';

UPDATE "implantData" set "implantBrand" = 'BIOHORIZONS IMPLANTS External' where "implantBrand" = 'BIOHORIZONS External';

UPDATE "implantData" set "implantBrand" = 'BIOHORIZONS IMPLANTS Tapered Internal' where "implantBrand" = 'BIOHORIZONS Tapered Internal';

UPDATE "implantData" set "implantBrand" = 'BRAIN BASE CORPORATION MYTIS Arrow Implant' where "implantBrand" = 'BRAINBASE Mytis Arrow Implant';

UPDATE "implantData" set "implantBrand" = 'BTI BIOTECHNOLOGY INSTITUTE Tiny ' where "implantBrand" = 'BTI BIOTECHNOLOGY Tiny';

UPDATE "implantData" set "implantBrand" = 'CAMLOG iSy' where "implantBrand" = 'CAMLOG iSy (direct to implant) / CAMLOG iSy (to implant base)';

UPDATE "implantData" set "implantBrand" = 'COCHLEAR Baha BIA 300' where "implantBrand" = 'COCHLEAR Baha BA 300,BIA 300';

UPDATE "implantData" set "implantBrand" = 'DENTEGRIS S&T' where "implantBrand" = 'DENTEGRIS Straight &Tapered';

UPDATE "implantData" set "implantBrand" = 'GC IMPLANT Aadva - Standard Implant' where "implantBrand" = 'GC IMPLANT Aadva';

UPDATE "implantData" set "implantBrand" = 'GC IMPLANT Aadva - Tapered Implant' where "implantBrand" = 'GC IMPLANT Aadva';

UPDATE "implantData" set "implantBrand" = 'IMPLANT DIRECT Legacy1 Implant SBM' where "implantBrand" = 'IMPLANT DIRECT Legacy1';

UPDATE "implantData" set "implantBrand" = 'IMPLANT DIRECT Legacy2 Implant SBM' where "implantBrand" = 'IMPLANT DIRECT Legacy2';

UPDATE "implantData" set "implantBrand" = 'IMPLANT DIRECT Legacy3 Implant SBM' where "implantBrand" = 'IMPLANT DIRECT Legacy3';

UPDATE "implantData" set "implantBrand" = 'IMPLANT DIRECT Legacy4 Implant SBM' where "implantBrand" = 'IMPLANT DIRECT Legacy4';

UPDATE "implantData" set "implantBrand" = 'PALTOP DIVA by Paltop' where "implantBrand" = 'PALTOP PALTOP';

UPDATE "implantData" set "implantBrand" = 'PRODENT ITALIA PRIME CONOMET TS' where "implantBrand" = 'PRODENT PRIME CONOMET TS';

UPDATE "implantData" set "implantBrand" = 'TRI DENTAL IMPLANTS TRI-Octa' where "implantBrand" = 'TRI DENTAL IMPLANTS TRI-Octa 4.8';