# --- !Ups

CREATE TABLE "clinicalNotes" (
  "id" BIGSERIAL NOT NULL,
  "staffId" BIGINT NOT NULL,
  "patientId" BIGINT NOT NULL,
  "note" VARCHAR DEFAULT '',
  "dateCreated" TIMESTAMP NOT NULL,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("staffId") REFERENCES "staff" ("id") ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY ("patientId") REFERENCES "treatments" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE "orderItems" 
	ADD COLUMN "amountPaid" FLOAT;

ALTER TABLE "treatments"
    ADD COLUMN "contracts" VARCHAR DEFAULT '',
    ADD COLUMN "scratchPad" VARCHAR DEFAULT '',
    ADD COLUMN "scratchPadText" VARCHAR DEFAULT '',
    ADD COLUMN "xray" VARCHAR DEFAULT '',
    ADD COLUMN "toothChartUpdatedDate" TIMESTAMP;
    
    
    ALTER TABLE "staff"
        ADD COLUMN "appSquares" VARCHAR DEFAULT '',
        ADD COLUMN "udid1" VARCHAR DEFAULT '';


CREATE TABLE "priceChangeHistory" (
  "id" BIGSERIAL NOT NULL,
  "vendorId" BIGINT, 
  "priceIncreasePercent" FLOAT,
  "listPriceIncreasePercent" FLOAT,
  "userId" BIGINT,
  "dateCreated" TIMESTAMP NOT NULL
);

CREATE TABLE "measurementHistory" (
  "id" BIGSERIAL NOT NULL,
  "patientId" BIGINT, 
  "formId" VARCHAR  DEFAULT '', 
  "systolic" FLOAT,
  "diastolic" FLOAT,
  "pulse" FLOAT,
  "oxygen" FLOAT,  
  "dateCreated" TIMESTAMP NOT NULL
);

# --- !Downs

DROP TABLE "clinicalNotes";

ALTER TABLE "orderItems" 
	DROP COLUMN "amountPaid";

ALTER TABLE "treatments"
	DROP COLUMN "contracts",
  DROP COLUMN "scratchPad",
  DROP COLUMN "scratchPadText";
  

ALTER TABLE "staff"
    DROP COLUMN "appSquares";

DROP TABLE "priceChangeHistory";

DROP TABLE "measurementHistory";




