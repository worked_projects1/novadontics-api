# --- !Ups
update staff set "marketingSource"='Colleague Referral' where "marketingSource"='Colleague';
update staff set "subscriptionPeriod"="subscriptionPeriod"*12 where "subscriptionPeriod" is not null;
update "renewalDetails" set "subscriptionPeriod"="subscriptionPeriod"*12 where "subscriptionPeriod" is not null;
update staff set "subscriptionPeriod"=12 where "subscriptionPeriod" is null;
update "renewalDetails" set "subscriptionPeriod"=12 where "subscriptionPeriod" is null;

# --- !Downs
update staff set "marketingSource"='Colleague' where "marketingSource"='Colleague Referral';
update staff set "subscriptionPeriod"="subscriptionPeriod"/12 where "subscriptionPeriod" is not null;
update "renewalDetails" set "subscriptionPeriod"="subscriptionPeriod"/12 where "subscriptionPeriod" is not null;
