# --- !Ups
ALTER TABLE "vendors"
    ADD COLUMN "logoUrl" VARCHAR;

ALTER TABLE "products"
    ALTER COLUMN "description" DROP NOT NULL;

# --- !Downs
ALTER TABLE "vendors"
    DROP COLUMN "logoUrl";
