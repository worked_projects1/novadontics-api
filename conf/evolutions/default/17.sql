# --- !Ups
UPDATE "videos" SET "category" = 'Beginning Level Live Patient Surgical Videos' WHERE "category" = 'Live-Patient Demonstration Videos';
UPDATE "videos" SET "category" = 'Lecture Series Videos' WHERE "category" = 'Lecture Series';
UPDATE "documents" SET "category" = 'Novadontics Documents and Forms' WHERE "category" = 'Novadontics';
UPDATE "documents" SET "category" = 'Vendors Brochures and Manuals' WHERE "category" = 'Nobel';

# --- !Downs
UPDATE "videos" SET "category" = 'Live-Patient Demonstration Videos' WHERE "category" = 'Beginning Level Live Patient Surgical Videos';
UPDATE "videos" SET "category" = 'Lecture Series' WHERE "category" = 'Lecture Series Videos';
UPDATE "documents" SET "category" = 'Novadontics' WHERE "category" = 'Novadontics Documents and Forms';
UPDATE "documents" SET "category" = 'Nobel' WHERE "category" = 'Vendors Brochures and Manuals';