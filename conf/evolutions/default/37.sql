# --- !Ups
ALTER TABLE staff 
	ADD COLUMN "plan" BIGINT, 
	ADD COLUMN "accountType" VARCHAR DEFAULT 'Customer', 
	ADD COLUMN "title" VARCHAR DEFAULT '',
	ADD COLUMN "lastName" VARCHAR DEFAULT '',
	ADD COLUMN "marketingSource" VARCHAR DEFAULT '',
	ADD COLUMN "notes" VARCHAR DEFAULT '',
	ADD COLUMN "subscriptionPeriod" INT;

ALTER TABLE orders 
	ADD COLUMN "paymentType" VARCHAR DEFAULT '',
	ADD COLUMN "checkNumber" VARCHAR DEFAULT '',
	ADD COLUMN "allVendorsPaid" BOOLEAN DEFAULT FALSE;

ALTER TABLE "orderItems" 
	ADD COLUMN "itemPaid" BOOLEAN DEFAULT FALSE,
	ADD COLUMN "paymentType" VARCHAR DEFAULT '',
	ADD COLUMN "checkNumber" VARCHAR DEFAULT '';


ALTER TABLE consultations 
	ADD COLUMN "amount" FLOAT, 
	ADD COLUMN "mode" VARCHAR DEFAULT '';

CREATE TABLE "renewalDetails" (
  "id" BIGSERIAL NOT NULL,
  "staffId" BIGINT,
  "subscriptionPeriod" INT,
  "plan" BIGINT,
  "renewalDate" TIMESTAMP,
  "prevExpDate" TIMESTAMP,
  "newExpDate" TIMESTAMP,
  "renewalType" VARCHAR DEFAULT '',
  PRIMARY KEY ("id")
);

INSERT INTO "renewalDetails"("staffId","subscriptionPeriod","plan","renewalDate","prevExpDate","newExpDate","renewalType")
 (SELECT id, "subscriptionPeriod", plan, "dateCreated", "dateCreated", "expirationDate", 'new'
    FROM staff);

# --- !Downs
ALTER TABLE staff 
	DROP COLUMN "plan", 
	DROP COLUMN "accountType", 
	DROP COLUMN "title",
	DROP COLUMN "lastName",
	DROP COLUMN "marketingSource",
	DROP COLUMN "notes",
	DROP COLUMN "subscriptionPeriod";

ALTER TABLE orders 
	DROP COLUMN "paymentType",
	DROP COLUMN "checkNumber",
	DROP COLUMN "allVendorsPaid";

ALTER TABLE "orderItems" 
	DROP COLUMN "itemPaid",
	DROP COLUMN "paymentType",
	DROP COLUMN "checkNumber";


ALTER TABLE consultations 
	DROP COLUMN "amount", 
	DROP COLUMN "mode";

DROP TABLE "renewalDetails";


