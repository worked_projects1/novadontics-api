# --- !Ups

CREATE TABLE "users" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "email" VARCHAR NOT NULL,
  "password" VARCHAR NOT NULL,
  "phone" VARCHAR NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  "archived" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id")
);

CREATE TABLE "dentists" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "email" VARCHAR NOT NULL,
  "password" VARCHAR NOT NULL,
  "practice" VARCHAR NOT NULL,
  "country" VARCHAR NOT NULL,
  "city" VARCHAR NOT NULL,
  "state" VARCHAR NOT NULL,
  "zip" VARCHAR NOT NULL,
  "address" VARCHAR NOT NULL,
  "phone" VARCHAR NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  "archived" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id")
);

CREATE TABLE "treatments" (
  "id" BIGSERIAL NOT NULL,
  "patientId" VARCHAR NOT NULL,
  "firstVisit" TIMESTAMP NOT NULL,
  "lastEntry" TIMESTAMP NOT NULL,
  "phoneNumber" VARCHAR NOT NULL,
  "emailAddress" VARCHAR NOT NULL,
  "dentistId" BIGINT NOT NULL,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("dentistId") REFERENCES "dentists" ("id")
);

CREATE TABLE "forms" (
  "id" VARCHAR(3) NOT NULL,
  "version" INT NOT NULL,
  "section" VARCHAR NOT NULL,
  "title" VARCHAR NOT NULL,
  "icon" VARCHAR NOT NULL,
  "schema" JSONB NOT NULL,

  PRIMARY KEY ("id", "version")
);

CREATE TABLE "steps" (
  "value" JSONB NOT NULL,
  "state" VARCHAR NOT NULL,
  "treatmentId" BIGINT NOT NULL,
  "formId" VARCHAR(3) NOT NULL,
  "formVersion" INT NOT NULL,
  "dateCreated" TIMESTAMP NOT NULL,

  PRIMARY KEY ("treatmentId", "formId", "formVersion", "dateCreated"),
  FOREIGN KEY ("treatmentId") REFERENCES "treatments" ("id"),
  FOREIGN KEY ("formId", "formVersion") REFERENCES "forms" ("id", "version")
);

CREATE TABLE "categories" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "parent" BIGINT NULL,
  "order" INT NOT NULL,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("parent") REFERENCES "categories" ("id") ON DELETE CASCADE
);

CREATE TABLE "products" (
  "id" BIGSERIAL NOT NULL,
  "name" VARCHAR NOT NULL,
  "description" VARCHAR NOT NULL,
  "serialNumber" VARCHAR NOT NULL,
  "imageUrl" VARCHAR NULL,
  "manufacturer" VARCHAR NOT NULL,
  "categoryId" BIGINT NULL,
  "price" FLOAT8 NOT NULL,
  "deleted" BOOLEAN NOT NULL DEFAULT FALSE,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("categoryId") REFERENCES "categories" ("id") ON DELETE SET NULL
);

CREATE TABLE "orders" (
  "id" BIGSERIAL NOT NULL,
  "status" VARCHAR NULL,
  "dateCreated" TIMESTAMP NOT NULL,
  "dentistId" BIGINT NOT NULL,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("dentistId") REFERENCES "dentists" ("id")
);

CREATE TABLE "orderItems" (
  "amount" INT NOT NULL,
  "productId" BIGINT NOT NULL,
  "orderId" BIGINT NOT NULL,
  "productName" VARCHAR NOT NULL,
  "productDescription" VARCHAR NOT NULL,
  "productSerialNumber" VARCHAR NOT NULL,
  "productImageUrl" VARCHAR NULL,
  "productManufacturer" VARCHAR NOT NULL,
  "productCategoryId" BIGINT NULL,
  "productCategoryName" VARCHAR NULL,
  "productPrice" FLOAT8 NOT NULL,

  PRIMARY KEY ("productId", "orderId"),
  FOREIGN KEY ("productId") REFERENCES "products" ("id"),
  FOREIGN KEY ("orderId") REFERENCES "orders" ("id")
);

CREATE TABLE "consultations" (
  "id" BIGSERIAL NOT NULL,
  "data" JSONB NOT NULL,
  "dateRequested" TIMESTAMP NOT NULL,
  "status" VARCHAR NOT NULL,
  "treatmentId" BIGINT NOT NULL,
  "dentistId" BIGINT NOT NULL,

  PRIMARY KEY ("id"),
  FOREIGN KEY ("treatmentId") REFERENCES "treatments" ("id"),
  FOREIGN KEY ("dentistId") REFERENCES "dentists" ("id")
);

CREATE TABLE "sessions" (
  "secret" VARCHAR NOT NULL,
  "userId" BIGINT NULL,
  "dentistId" BIGINT NULL,
  "lastUsedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL,
  "expirationDateTime" TIMESTAMP WITH TIME ZONE NOT NULL,
  "idleTimeoutNanos" BIGINT NULL,

  PRIMARY KEY ("secret"),
  FOREIGN KEY ("userId") REFERENCES "users" ("id"),
  FOREIGN KEY ("dentistId") REFERENCES "dentists" ("id")
);

CREATE TABLE "passwordChangeTokens" (
  "secret" CHAR(12) NOT NULL,
  "userId" BIGINT NULL,
  "dentistId" BIGINT NULL,
  "dateIssued" TIMESTAMP NOT NULL,

  PRIMARY KEY ("secret"),
  FOREIGN KEY ("userId") REFERENCES "users" ("id") ON DELETE CASCADE,
  FOREIGN KEY ("dentistId") REFERENCES "dentists" ("id") ON DELETE CASCADE
);

# --- !Downs

DROP TABLE "passwordChangeTokens";
DROP TABLE "sessions";
DROP TABLE "consultations";
DROP TABLE "orderItems";
DROP TABLE "orders";
DROP TABLE "products";
DROP TABLE "categories";
DROP TABLE "steps";
DROP TABLE "forms";
DROP TABLE "treatments";
DROP TABLE "dentists";
DROP TABLE "users";
