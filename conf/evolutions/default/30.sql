# --- !Ups
ALTER TABLE "videos" ADD COLUMN "speaker" VARCHAR;

-- One of [Doctor, Patient]
ALTER TABLE "videos" ADD COLUMN "educationType" VARCHAR NOT NULL DEFAULT 'Doctor';
ALTER TABLE "videos" RENAME TO "courses";

-- One of [Open, Closed]
ALTER TABLE "educationRequests" ADD COLUMN "status" VARCHAR NOT NULL DEFAULT 'Open';

ALTER TABLE "consultations" ADD COLUMN "deleted" BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE "treatments" ADD COLUMN "deleted" BOOLEAN NOT NULL DEFAULT FALSE;

# --- !Downs
ALTER TABLE "courses" RENAME TO "videos";
ALTER TABLE "videos" DROP COLUMN "speaker";
ALTER TABLE "videos" DROP COLUMN "educationType";
ALTER TABLE "educationRequests" DROP COLUMN "status";
ALTER TABLE "consultations" DROP COLUMN "deleted";
ALTER TABLE "treatments" DROP COLUMN "deleted";