# --- !Ups
ALTER TABLE "users" ADD COLUMN "role" VARCHAR NOT NULL DEFAULT 'Admin';

# --- !Downs
ALTER TABLE "users" DROP COLUMN "role";