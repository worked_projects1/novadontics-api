# --- !Ups
ALTER TABLE "staff"
  ADD COLUMN "canAccessWeb" BOOLEAN NOT NULL DEFAULT TRUE;


# --- !Downs
ALTER TABLE "staff"
  DROP COLUMN "canAccessWeb";
